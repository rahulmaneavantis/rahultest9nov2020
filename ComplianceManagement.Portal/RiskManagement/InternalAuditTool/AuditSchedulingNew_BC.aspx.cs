﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditSchedulingNew_BC : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            int BranchId = -1;
            if (!IsPostBack)
            {
                BindSchedulingType();
                BindFinancialYear();
                BindFinancialYearFilter();
                BindLegalEntityDataPop();
                BindMainGrid();
                bindPageNumber();
                //GetPageDisplaySummary();
                BindLegalEntityData();
                BindVerticalIDPOPup(BranchId);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditScheduling.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindMainGrid();
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public void BindVerticalIDPOPup(int? BranchId)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var details = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalListPopup.DataTextField = "VerticalName";
                ddlVerticalListPopup.DataValueField = "VerticalsId";
                ddlVerticalListPopup.Items.Clear();
                ddlVerticalListPopup.DataSource = details;
                ddlVerticalListPopup.DataBind();
                ddlVerticalListPopup.Items.Insert(0, new ListItem("Select Vertical", "-1"));

                //ddlVerticalBranch.Items.Clear();
                //ddlVerticalBranch.DataTextField = "VerticalName";
                //ddlVerticalBranch.DataValueField = "VerticalsId";
                //ddlVerticalBranch.DataSource = details;
                //ddlVerticalBranch.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindVerticalID(int? BranchId)
        {
            if (BranchId != null)
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlVerticalID.DataTextField = "VerticalName";
                ddlVerticalID.DataValueField = "VerticalsId";
                ddlVerticalID.Items.Clear();
                ddlVerticalID.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(BranchId);
                ddlVerticalID.DataBind();
                ddlVerticalID.Items.Insert(0, new ListItem("Select Vertical", "-1"));
                
            }
        }

      
        public void BindFinancialYearFilter()
        {
            ddlFilterFinancialYear.DataTextField = "Name";
            ddlFilterFinancialYear.DataValueField = "ID";
            ddlFilterFinancialYear.Items.Clear();
            ddlFilterFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancialYear.DataBind();
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            cleardatasource();
            ForceCloseFilterBranchesTreeView();
            BindSchedulingType();
        }
        protected void ddlFilterFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {            
            BindMainGrid();
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView1", "$(\"#divLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                int Branchid = -1;
                int verticalId = -1;

                if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                {
                    if (ddlLegalEntityPop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                {
                    if (ddlSubEntity1Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                {
                    if (ddlSubEntity2Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                {
                    if (ddlSubEntity3Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                {
                    if (ddlSubEntity4Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                {
                    if (ddlVerticalListPopup.SelectedValue != "-1")
                    {
                        verticalId = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                    }
                }

                if (verticalId != -1)
                {
                    if (flag == "A")
                    {
                        #region Annualy
                        cleardatasource();
                        if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                string financialyear = ddlFinancialYear.SelectedItem.Text;
                                string[] a = financialyear.Split('-');
                                string aaa = a[0];
                                string bbb = a[1];
                                string f1 = aaa + "-" + bbb;
                                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                List<SP_Annualy_Result> r = new List<SP_Annualy_Result>();
                                r = ProcessManagement.GetSPAnnualyDisplay(Branchid, f1, verticalId);

                                if (r.Count > 0)
                                    r = r.OrderBy(entry => entry.Name).ToList();

                                grdAnnually.DataSource = r;/*ProcessManagement.GetSPAnnualyDisplay(Branchid, f1, verticalId);*/
                                grdAnnually.DataBind();                               

                                DataTable dt = new DataTable();
                                dt = (grdAnnually.DataSource as List<SP_Annualy_Result>).ToDataTable();
                                DataTable dt1 = new DataTable();
                                dt1.Clear();
                                DataRow dr1 = null;
                                dt1.Columns.Add("Name");
                                dt1.Columns.Add("Annualy1");
                                dt1.Columns.Add("Annualy2");
                                dt1.Columns.Add("Annualy3");
                                dt1.Columns.Add("ID", typeof(long));
                                dr1 = dt1.NewRow();

                                dr1["Name"] = "";
                                dr1["Annualy1"] = f1;
                                dr1["Annualy2"] = f2;
                                dr1["Annualy3"] = f3;
                                dr1["ID"] = 0;
                                dt1.Rows.Add(dr1);
                                dt1.Merge(dt);

                                grdAnnually.DataSource = dt1;
                                grdAnnually.DataBind();
                            }
                        }
                        #endregion
                    }
                    else if (flag == "H")
                    {
                        #region Half Yearly
                        cleardatasource();

                        if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                string financialyear = ddlFinancialYear.SelectedItem.Text;
                                string[] a = financialyear.Split('-');
                                string aaa = a[0];
                                string bbb = a[1];
                                string f1 = aaa + "-" + bbb;
                                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                List<SP_HalfYearly_Result> r = new List<SP_HalfYearly_Result>();
                                r = ProcessManagement.GetSPHalfYearlyDisplay(Branchid, f1, verticalId);

                                if (r.Count > 0)
                                    r = r.OrderBy(entry => entry.Name).ToList();

                                grdHalfYearly.DataSource = r; /* ProcessManagement.GetSPHalfYearlyDisplay(Branchid, f1, verticalId);*/
                                grdHalfYearly.DataBind();                               

                                DataTable dt = new DataTable();
                                dt = (grdHalfYearly.DataSource as List<SP_HalfYearly_Result>).ToDataTable();
                                DataTable dt1 = new DataTable();
                                dt1.Clear();
                                DataRow dr1 = null;

                                dt1.Columns.Add("Name");
                                dt1.Columns.Add("Halfyearly1");
                                dt1.Columns.Add("Halfyearly2");
                                dt1.Columns.Add("Halfyearly3");
                                dt1.Columns.Add("Halfyearly4");
                                dt1.Columns.Add("Halfyearly5");
                                dt1.Columns.Add("Halfyearly6");
                                dt1.Columns.Add("ID", typeof(long));
                                dr1 = dt1.NewRow();

                                dr1["Name"] = "Process";
                                dr1["Halfyearly1"] = "";
                                dr1["Halfyearly2"] = f1;
                                dr1["Halfyearly3"] = "";
                                dr1["Halfyearly4"] = f2;
                                dr1["Halfyearly5"] = "";
                                dr1["Halfyearly6"] = f3;
                                dr1["ID"] = 0;
                                dt1.Rows.Add(dr1);
                                dt1.Merge(dt);

                                grdHalfYearly.DataSource = dt1;
                                grdHalfYearly.DataBind();
                            }
                        }
                        #endregion
                    }
                    else if (flag == "Q")
                    {
                        #region Quarterly
                        cleardatasource();

                        if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                string financialyear = ddlFinancialYear.SelectedItem.Text;
                                string[] a = financialyear.Split('-');
                                string aaa = a[0];
                                string bbb = a[1];
                                string f1 = aaa + "-" + bbb;
                                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                List<SP_Quarterly_Result> r = new List<SP_Quarterly_Result>();
                                r = ProcessManagement.GetSPQuarterwiseDisplay(Branchid, f1, verticalId);

                                if (r.Count > 0)
                                    r = r.OrderBy(entry => entry.Name).ToList();

                                grdQuarterly.DataSource = r;/*ProcessManagement.GetSPQuarterwiseDisplay(Branchid, f1, verticalId);*/
                                grdQuarterly.DataBind();                                

                                DataTable dt = new DataTable();
                                dt = (grdQuarterly.DataSource as List<SP_Quarterly_Result>).ToDataTable();
                                DataTable dt1 = new DataTable();
                                dt1.Clear();
                                DataRow dr1 = null;

                                dt1.Columns.Add("Name");
                                dt1.Columns.Add("Quarter1");
                                dt1.Columns.Add("Quarter2");
                                dt1.Columns.Add("Quarter3");
                                dt1.Columns.Add("Quarter4");
                                dt1.Columns.Add("Quarter5");
                                dt1.Columns.Add("Quarter6");
                                dt1.Columns.Add("Quarter7");
                                dt1.Columns.Add("Quarter8");
                                dt1.Columns.Add("Quarter9");
                                dt1.Columns.Add("Quarter10");
                                dt1.Columns.Add("Quarter11");
                                dt1.Columns.Add("Quarter12");
                                dt1.Columns.Add("ID", typeof(long));
                                dr1 = dt1.NewRow();

                                dr1["Name"] = "Process";
                                dr1["Quarter1"] = "";
                                dr1["Quarter2"] = "";
                                dr1["Quarter3"] = "";
                                dr1["Quarter4"] = f1;
                                dr1["Quarter5"] = "";
                                dr1["Quarter6"] = "";
                                dr1["Quarter7"] = "";
                                dr1["Quarter8"] = f2;
                                dr1["Quarter9"] = "";
                                dr1["Quarter10"] = "";
                                dr1["Quarter11"] = "";
                                dr1["Quarter12"] = f3;
                                dr1["ID"] = 0;
                                dt1.Rows.Add(dr1);
                                dt1.Merge(dt);

                                grdQuarterly.DataSource = dt1;
                                grdQuarterly.DataBind();
                            }
                        }
                        #endregion
                    }
                    else if (flag == "M")
                    {
                        #region Monthly
                        cleardatasource();

                        if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                        {
                            if (ddlFinancialYear.SelectedValue != "-1")
                            {
                                string financialyear = ddlFinancialYear.SelectedItem.Text;
                                string[] a = financialyear.Split('-');
                                string aaa = a[0];
                                string bbb = a[1];
                                string f1 = aaa + "-" + bbb;
                                string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                List<SP_Monthly_Result> r = new List<SP_Monthly_Result>();
                                r = ProcessManagement.GetSPMonthlyDisplay(Branchid, f1, verticalId);

                                if (r.Count > 0)
                                    r = r.OrderBy(entry => entry.Name).ToList();

                                grdMonthly.DataSource = r; /*ProcessManagement.GetSPMonthlyDisplay(Branchid, f1, verticalId);*/
                                grdMonthly.DataBind();                               

                                DataTable dt = new DataTable();
                                dt = (grdMonthly.DataSource as List<SP_Monthly_Result>).ToDataTable();
                                DataTable dt1 = new DataTable();
                                dt1.Clear();
                                DataRow dr1 = null;

                                dt1.Columns.Add("Name");
                                dt1.Columns.Add("Monthly1");
                                dt1.Columns.Add("Monthly2");
                                dt1.Columns.Add("Monthly3");
                                dt1.Columns.Add("Monthly4");
                                dt1.Columns.Add("Monthly5");
                                dt1.Columns.Add("Monthly6");
                                dt1.Columns.Add("Monthly7");
                                dt1.Columns.Add("Monthly8");
                                dt1.Columns.Add("Monthly9");
                                dt1.Columns.Add("Monthly10");
                                dt1.Columns.Add("Monthly11");
                                dt1.Columns.Add("Monthly12");
                                dt1.Columns.Add("Monthly13");
                                dt1.Columns.Add("Monthly14");
                                dt1.Columns.Add("Monthly15");
                                dt1.Columns.Add("Monthly16");
                                dt1.Columns.Add("Monthly17");
                                dt1.Columns.Add("Monthly18");
                                dt1.Columns.Add("Monthly19");
                                dt1.Columns.Add("Monthly20");
                                dt1.Columns.Add("Monthly21");
                                dt1.Columns.Add("Monthly22");
                                dt1.Columns.Add("Monthly23");
                                dt1.Columns.Add("Monthly24");
                                dt1.Columns.Add("Monthly25");
                                dt1.Columns.Add("Monthly26");
                                dt1.Columns.Add("Monthly27");
                                dt1.Columns.Add("Monthly28");
                                dt1.Columns.Add("Monthly29");
                                dt1.Columns.Add("Monthly30");
                                dt1.Columns.Add("Monthly31");
                                dt1.Columns.Add("Monthly32");
                                dt1.Columns.Add("Monthly33");
                                dt1.Columns.Add("Monthly34");
                                dt1.Columns.Add("Monthly35");
                                dt1.Columns.Add("Monthly36");
                                dt1.Columns.Add("ID", typeof(long));
                                dr1 = dt1.NewRow();

                                dr1["Name"] = "Process";
                                dr1["Monthly1"] = "";
                                dr1["Monthly2"] = "";
                                dr1["Monthly3"] = "";
                                dr1["Monthly4"] = "";
                                dr1["Monthly5"] = "";
                                dr1["Monthly6"] = "";
                                dr1["Monthly7"] = "";
                                dr1["Monthly8"] = "";
                                dr1["Monthly9"] = "";
                                dr1["Monthly10"] = "";
                                dr1["Monthly11"] = "";
                                dr1["Monthly12"] = f1;
                                dr1["Monthly13"] = "";
                                dr1["Monthly14"] = "";
                                dr1["Monthly15"] = "";
                                dr1["Monthly16"] = "";
                                dr1["Monthly17"] = "";
                                dr1["Monthly18"] = "";
                                dr1["Monthly19"] = "";
                                dr1["Monthly20"] = "";
                                dr1["Monthly21"] = "";
                                dr1["Monthly22"] = "";
                                dr1["Monthly23"] = "";
                                dr1["Monthly24"] = f2;
                                dr1["Monthly25"] = "";
                                dr1["Monthly26"] = "";
                                dr1["Monthly27"] = "";
                                dr1["Monthly28"] = "";
                                dr1["Monthly29"] = "";
                                dr1["Monthly30"] = "";
                                dr1["Monthly31"] = "";
                                dr1["Monthly32"] = "";
                                dr1["Monthly33"] = "";
                                dr1["Monthly34"] = "";
                                dr1["Monthly35"] = "";
                                dr1["Monthly36"] = f3;
                                dr1["ID"] = 0;
                                dt1.Rows.Add(dr1);
                                dt1.Merge(dt);

                                grdMonthly.DataSource = dt1;
                                grdMonthly.DataBind();
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (count == 1)
                        {
                            #region Phase1
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;
                                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                    List<SP_Phase1_Result> r = new List<SP_Phase1_Result>();
                                    r = ProcessManagement.GetSP_Phase1Display(Branchid, f1, verticalId);

                                    if (r.Count > 0)
                                        r = r.OrderBy(entry => entry.Name).ToList();

                                    grdphase1.DataSource = r; /*ProcessManagement.GetSP_Phase1Display(Branchid, f1, verticalId);*/
                                    grdphase1.DataBind();                                    

                                    DataTable dt = new DataTable();
                                    dt = (grdphase1.DataSource as List<SP_Phase1_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("Name");
                                    dt1.Columns.Add("Phase1");
                                    dt1.Columns.Add("Phase2");
                                    dt1.Columns.Add("Phase3");
                                    dt1.Columns.Add("ID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["Name"] = "Process";
                                    dr1["Phase1"] = f1;
                                    dr1["Phase2"] = f2;
                                    dr1["Phase3"] = f3;
                                    dr1["ID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdphase1.DataSource = dt1;
                                    grdphase1.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (count == 2)
                        {
                            #region Phase2
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;
                                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                    List<SP_Phase2_Result> r = new List<SP_Phase2_Result>();
                                    r = ProcessManagement.GetSP_Phase2Display(Branchid, f1, verticalId);

                                    if (r.Count > 0)
                                        r = r.OrderBy(entry => entry.Name).ToList();

                                    grdphase2.DataSource = r;/*ProcessManagement.GetSP_Phase2Display(Branchid, f1, verticalId);*/
                                    grdphase2.DataBind();                                   

                                    DataTable dt = new DataTable();
                                    dt = (grdphase2.DataSource as List<SP_Phase2_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("Name");
                                    dt1.Columns.Add("Phase1");
                                    dt1.Columns.Add("Phase2");
                                    dt1.Columns.Add("Phase3");
                                    dt1.Columns.Add("Phase4");
                                    dt1.Columns.Add("Phase5");
                                    dt1.Columns.Add("Phase6");
                                    dt1.Columns.Add("ID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["Name"] = "Process";
                                    dr1["Phase1"] = "";
                                    dr1["Phase2"] = f1;
                                    dr1["Phase3"] = "";
                                    dr1["Phase4"] = f2;
                                    dr1["Phase5"] = "";
                                    dr1["Phase6"] = f3;
                                    dr1["ID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdphase2.DataSource = dt1;
                                    grdphase2.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (count == 3)
                        {
                            #region Phase3
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;
                                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                    List<SP_Phase3_Result> r = new List<SP_Phase3_Result>();
                                    r = ProcessManagement.GetSP_Phase3Display(Branchid, f1, verticalId);
                                    if (r.Count > 0)
                                        r = r.OrderBy(entry => entry.Name).ToList();

                                    grdphase3.DataSource = r; /*ProcessManagement.GetSP_Phase3Display(Branchid, f1, verticalId);*/
                                    grdphase3.DataBind();
                                    
                                    DataTable dt = new DataTable();
                                    dt = (grdphase3.DataSource as List<SP_Phase3_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("Name");
                                    dt1.Columns.Add("Phase1");
                                    dt1.Columns.Add("Phase2");
                                    dt1.Columns.Add("Phase3");
                                    dt1.Columns.Add("Phase4");
                                    dt1.Columns.Add("Phase5");
                                    dt1.Columns.Add("Phase6");
                                    dt1.Columns.Add("Phase7");
                                    dt1.Columns.Add("Phase8");
                                    dt1.Columns.Add("Phase9");
                                    dt1.Columns.Add("ID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["Name"] = "Process";
                                    dr1["Phase1"] = "";
                                    dr1["Phase2"] = "";
                                    dr1["Phase3"] = f1;
                                    dr1["Phase4"] = "";
                                    dr1["Phase5"] = "";
                                    dr1["Phase6"] = f2;
                                    dr1["Phase7"] = "";
                                    dr1["Phase8"] = "";
                                    dr1["Phase9"] = f3;
                                    dr1["ID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdphase3.DataSource = dt1;
                                    grdphase3.DataBind();
                                }
                            }
                            #endregion
                        }
                        else if (count == 4)
                        {
                            #region Phase4
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;
                                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                    List<SP_Phase4_Result> r = new List<SP_Phase4_Result>();
                                    r = ProcessManagement.GetSP_Phase4Display(Branchid, f1, verticalId);
                                    if (r.Count > 0)
                                        r = r.OrderBy(entry => entry.Name).ToList();

                                    grdphase4.DataSource = r;/*ProcessManagement.GetSP_Phase4Display(Branchid, f1, verticalId);*/
                                    grdphase4.DataBind();
                                   
                                    DataTable dt = new DataTable();
                                    dt = (grdphase4.DataSource as List<SP_Phase4_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("Name");
                                    dt1.Columns.Add("Phase1");
                                    dt1.Columns.Add("Phase2");
                                    dt1.Columns.Add("Phase3");
                                    dt1.Columns.Add("Phase4");
                                    dt1.Columns.Add("Phase5");
                                    dt1.Columns.Add("Phase6");
                                    dt1.Columns.Add("Phase7");
                                    dt1.Columns.Add("Phase8");
                                    dt1.Columns.Add("Phase9");
                                    dt1.Columns.Add("Phase10");
                                    dt1.Columns.Add("Phase11");
                                    dt1.Columns.Add("Phase12");
                                    dt1.Columns.Add("ID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["Name"] = "Process";
                                    dr1["Phase1"] = "";
                                    dr1["Phase2"] = "";
                                    dr1["Phase3"] = "";
                                    dr1["Phase4"] = f1;
                                    dr1["Phase5"] = "";
                                    dr1["Phase6"] = "";
                                    dr1["Phase7"] = "";
                                    dr1["Phase8"] = f2;
                                    dr1["Phase9"] = "";
                                    dr1["Phase10"] = "";
                                    dr1["Phase11"] = "";
                                    dr1["Phase12"] = f3;
                                    dr1["ID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdphase4.DataSource = dt1;
                                    grdphase4.DataBind();
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Phase5
                            cleardatasource();

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                                    string[] a = financialyear.Split('-');
                                    string aaa = a[0];
                                    string bbb = a[1];
                                    string f1 = aaa + "-" + bbb;
                                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                                    List<SP_Phase5_Result> r = new List<SP_Phase5_Result>();
                                    r = ProcessManagement.GetSP_Phase5Display(Branchid, f1, verticalId);
                                    if (r.Count > 0)
                                        r = r.OrderBy(entry => entry.Name).ToList();

                                    grdphase5.DataSource = r;/*ProcessManagement.GetSP_Phase5Display(Branchid, f1, verticalId);*/
                                    grdphase5.DataBind();
                                   
                                    DataTable dt = new DataTable();
                                    dt = (grdphase5.DataSource as List<SP_Phase5_Result>).ToDataTable();
                                    DataTable dt1 = new DataTable();
                                    dt1.Clear();
                                    DataRow dr1 = null;
                                    dt1.Columns.Add("Name");
                                    dt1.Columns.Add("Phase1");
                                    dt1.Columns.Add("Phase2");
                                    dt1.Columns.Add("Phase3");
                                    dt1.Columns.Add("Phase4");
                                    dt1.Columns.Add("Phase5");
                                    dt1.Columns.Add("Phase6");
                                    dt1.Columns.Add("Phase7");
                                    dt1.Columns.Add("Phase8");
                                    dt1.Columns.Add("Phase9");
                                    dt1.Columns.Add("Phase10");
                                    dt1.Columns.Add("Phase11");
                                    dt1.Columns.Add("Phase12");
                                    dt1.Columns.Add("Phase13");
                                    dt1.Columns.Add("Phase14");
                                    dt1.Columns.Add("Phase15");
                                    dt1.Columns.Add("ID", typeof(long));
                                    dr1 = dt1.NewRow();

                                    dr1["Name"] = "Process";
                                    dr1["Phase1"] = "";
                                    dr1["Phase2"] = "";
                                    dr1["Phase3"] = "";
                                    dr1["Phase4"] = "";
                                    dr1["Phase5"] = f1;
                                    dr1["Phase6"] = "";
                                    dr1["Phase7"] = "";
                                    dr1["Phase8"] = "";
                                    dr1["Phase9"] = "";
                                    dr1["Phase10"] = f2;
                                    dr1["Phase11"] = "";
                                    dr1["Phase12"] = "";
                                    dr1["Phase13"] = "";
                                    dr1["Phase14"] = "";
                                    dr1["Phase15"] = f3;
                                    dr1["ID"] = 0;
                                    dt1.Rows.Add(dr1);
                                    dt1.Merge(dt);

                                    grdphase5.DataSource = dt1;
                                    grdphase5.DataBind();
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Vertical";
                    return;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
        }
        protected void upCompliancesList_Load(object sender, EventArgs e)
        {
        }
        public void BindSchedulingType()
        {
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.Items.Clear();
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingType();
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }
        public void EnableDisable(int noofphases)
        {
            if (ddlFinancialYear.SelectedItem.Text != "Financial Year")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    pnlAnnually.Visible = true;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = true;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = true;
                    pnlMonthly.Visible = false;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    pnlAnnually.Visible = false;
                    pnlHalfYearly.Visible = false;
                    pnlQuarterly.Visible = false;
                    pnlMonthly.Visible = true;
                    pnlphase1.Visible = false;
                    pnlphase2.Visible = false;
                    pnlphase3.Visible = false;
                    pnlphase4.Visible = false;
                    pnlphase5.Visible = false;
                    Divnophase.Visible = false;
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    if (noofphases == 1)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = true;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 2)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = true;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 3)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = true;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 4)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = true;
                        pnlphase5.Visible = false;
                        Divnophase.Visible = true;
                    }
                    else if (noofphases == 5)
                    {
                        pnlAnnually.Visible = false;
                        pnlHalfYearly.Visible = false;
                        pnlQuarterly.Visible = false;
                        pnlMonthly.Visible = false;
                        pnlphase1.Visible = false;
                        pnlphase2.Visible = false;
                        pnlphase3.Visible = false;
                        pnlphase4.Visible = false;
                        pnlphase5.Visible = true;
                        Divnophase.Visible = true;
                    }
                }
                else
                {
                    Divnophase.Visible = false;
                }
            }
        }

        public string ShowVerticalName(int customerid,int verticalid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.CustomerID== customerid
                             && row.ID == verticalid                             
                             select row.VerticalName).FirstOrDefault();

                processnonprocess = query;
            }
            return processnonprocess;
        }

        public string ShowCustomerBranchName(long id)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.Status == 1
                             && row.IsDeleted == false
                             && row.ID == id
                             select row).FirstOrDefault();

                processnonprocess = query.Name;
            }
            return processnonprocess.Trim(',');
        }
        public string ShowStatus(string ISAHQMP)
        {
            string processnonprocess = "";
            if (Convert.ToString(ISAHQMP) == "A")
            {
                processnonprocess = "Annually";
            }
            else if (Convert.ToString(ISAHQMP) == "H")
            {
                processnonprocess = "Half Yearly";
            }
            else if (Convert.ToString(ISAHQMP) == "Q")
            {
                processnonprocess = "Quarterly";
            }
            else if (Convert.ToString(ISAHQMP) == "M")
            {
                processnonprocess = "Monthly";
            }
            else if (Convert.ToString(ISAHQMP) == "P")
            {
                processnonprocess = "Phase";
            }

            return processnonprocess.Trim(',');
        }
        public string ShowProcessName(long Processid)
        {
            string processnonprocess = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.IsDeleted == false
                             && row.Id == Processid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    processnonprocess = query.Name;
                }
            }
            return processnonprocess.Trim(',');
        }
        public void cleardatasource()
        {
            grdAnnually.DataSource = null;
            grdAnnually.DataBind();
            grdHalfYearly.DataSource = null;
            grdHalfYearly.DataBind();
            grdQuarterly.DataSource = null;
            grdQuarterly.DataBind();
            grdMonthly.DataSource = null;
            grdMonthly.DataBind();
            grdphase1.DataSource = null;
            grdphase1.DataBind();
            grdphase2.DataSource = null;
            grdphase2.DataBind();
            grdphase3.DataSource = null;
            grdphase3.DataBind();
            grdphase4.DataSource = null;
            grdphase4.DataBind();
            grdphase5.DataSource = null;
            grdphase5.DataBind();
        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSchedulingType.SelectedValue))
            {
                if (ddlSchedulingType.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                    {
                        if (ddlFinancialYear.SelectedValue != "-1")
                        {
                            if (ddlSchedulingType.SelectedItem.Text == "Annually")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("A", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("H", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("Q", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                            {
                                EnableDisable(0);
                                BindAuditSchedule("M", 0);
                            }
                            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                            {
                                //EnableDisable(0);
                                if (ddlSchedulingType.SelectedItem.Text == "Phase")
                                {
                                    Divnophase.Visible = true;
                                }
                                else
                                {
                                    Divnophase.Visible = false;
                                }
                                cleardatasource();
                            }
                            ForceCloseFilterBranchesTreeView();
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Financial Year";
                        }
                    }
                }
                else
                {

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Scheduling Type";
                    cleardatasource();
                }
            }
        }
        protected void txtNoOfPhases_TextChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                {
                    EnableDisable(Convert.ToInt32(txtNoOfPhases.Text));
                    BindAuditSchedule("P", Convert.ToInt32(txtNoOfPhases.Text));

                }
            }
        }
        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                ddlFinancialYear.SelectedValue = "-1";
                ddlSchedulingType.SelectedValue = "-1";
                ddlLegalEntityPop.SelectedValue = "-1";
                ddlSubEntity1Pop.SelectedValue = "-1";
                ddlSubEntity2Pop.SelectedValue = "-1";
                ddlSubEntity3Pop.SelectedValue = "-1";
                ddlSubEntity4Pop.SelectedValue = "-1";
                txtStartDatePop.Text = string.Empty;
                txtEndDatePop.Text = string.Empty;
                ddlFinancialYear.SelectedValue = "-1";
                ddlSchedulingType.SelectedValue = "-1";
                cleardatasource();
                EnableDisable(0);
                Divnophase.Visible = false;
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;
                txtNoOfPhases.Text = string.Empty;

                ForceCloseFilterBranchesTreeView();
                upComplianceDetails.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divAuditSchedulingDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //public void bindGridDataSchedule(int customerid, int customerbranchid, string FinancialYear)
        //{
        //    try
        //    {
        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            if (customerid != -1 && customerbranchid != -1 && !string.IsNullOrEmpty(FinancialYear))
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
        //                             && row.CustomerBranchId == customerbranchid && row.FinancialYear == FinancialYear
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,
        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //            else if (customerid != -1 && customerbranchid != -1)
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
        //                             && row.CustomerBranchId == customerbranchid
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,

        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //            else if (customerid != -1 && !string.IsNullOrEmpty(FinancialYear))
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
        //                             && row.FinancialYear == FinancialYear
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,

        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //            else if (customerbranchid != -1 && !string.IsNullOrEmpty(FinancialYear))
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false
        //                             && row.CustomerBranchId == customerbranchid && row.FinancialYear == FinancialYear
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,

        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //            else if (customerid != -1)
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerid
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,
        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //            else if (customerbranchid != -1)
        //            {
        //                var query = (from row in entities.InternalAuditSchedulings
        //                             join MCB in entities.mst_CustomerBranch
        //                             on row.CustomerBranchId equals MCB.ID
        //                             where row.TermStatus == true && row.IsDeleted == false
        //                             && row.CustomerBranchId == customerbranchid
        //                             select new InternalAuditSchedulingClass()
        //                             {
        //                                 CustomerId = MCB.CustomerID,
        //                                 FinancialYear = row.FinancialYear,
        //                                 CustomerBranchId = row.CustomerBranchId,
        //                                 ISAHQMP = row.ISAHQMP,
        //                                 TermName = row.TermName,
        //                                 PhaseCount = row.PhaseCount,
        //                             }).Distinct().ToList();
        //                grdAuditScheduling.DataSource = null;
        //                Session["TotalRows"] = query.Count;
        //                grdAuditScheduling.DataBind();
        //                if (query != null)
        //                {
        //                    grdAuditScheduling.DataSource = query;
        //                    Session["TotalRows"] = query.Count;
        //                    grdAuditScheduling.DataBind();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {


            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindMainGrid()
        {
            string FinancialYear = string.Empty;
            int CustomerBranchId = -1;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            int VerticalID = -1;
            if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
            {
                if (ddlVerticalID.SelectedValue != "-1")
                {
                    VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                }
            }
            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(customerID, CustomerBranchId);
            var Branchlistloop = Branchlist.ToList();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                             join row1 in entities.mst_Vertical
                             on row.VerticalID equals row1.ID
                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerID
                             select new AuditSchedulingNewbind()
                             {
                                 CustomerId = MCB.CustomerID,
                                 FinancialYear = row.FinancialYear,
                                 CustomerBranchId = row.CustomerBranchId,
                                 CustomerBranchName= MCB.Name,
                                 ISAHQMP = row.ISAHQMP,
                                 TermName = row.TermName,
                                 PhaseCount = row.PhaseCount,
                                 VerticalName = row1.VerticalName,
                                 VerticalID = row1.ID,
                                 StartDate=row.StartDate,
                                 EndDate=row.EndDate,
                             }).Distinct().ToList();


                if (Branchlist.Count>0)
                {
                    query = query.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                }
                if (FinancialYear != "")
                {
                    query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                if (VerticalID != -1)
                {
                    query = query.Where(entry => entry.VerticalID == VerticalID).ToList();
                }

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.CustomerBranchName).ThenBy(entry=>entry.VerticalName).ToList();

                grdAuditScheduling.DataSource = null;             
                grdAuditScheduling.DataBind();

                if (query != null)
                {
                    grdAuditScheduling.DataSource = query;
                    Session["TotalRows"] = query.Count;
                    grdAuditScheduling.DataBind();
                }
            }
        }

        public void BindMainGridEdit(long branchid)
        {
            string FinancialYear = string.Empty;
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (!string.IsNullOrEmpty(ddlFilterFinancialYear.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterFinancialYear.SelectedValue) != -1)
                {
                    FinancialYear = Convert.ToString(ddlFilterFinancialYear.SelectedItem.Text);
                }
            }
            int VerticalID = -1;
            if (!string.IsNullOrEmpty(ddlVerticalID.SelectedValue))
            {
                if (ddlVerticalID.SelectedValue != "-1")
                {
                    VerticalID = Convert.ToInt32(ddlVerticalID.SelectedValue);
                }
            }
            Branchlist.Clear();
            var bracnhes = GetAllHierarchy(customerID, branchid);
            var Branchlistloop = Branchlist.ToList();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             join MCB in entities.mst_CustomerBranch
                             on row.CustomerBranchId equals MCB.ID
                             join row1 in entities.mst_Vertical
                             on row.VerticalID equals row1.ID
                             where row.TermStatus == true && row.IsDeleted == false && MCB.CustomerID == customerID
                             select new AuditSchedulingNewbind()
                             {
                                 CustomerId = MCB.CustomerID,
                                 FinancialYear = row.FinancialYear,
                                 CustomerBranchId = row.CustomerBranchId,
                                 ISAHQMP = row.ISAHQMP,
                                 TermName = row.TermName,
                                 PhaseCount = row.PhaseCount,
                                 VerticalName = row1.VerticalName,
                                 VerticalID = row1.ID
                             }).Distinct().ToList();


                if (Branchlist.Count > 0)
                {
                    query = query.Where(entry => Branchlist.Contains(entry.CustomerBranchId)).ToList();
                }
                if (FinancialYear != "")
                {
                    query = query.Where(entry => entry.FinancialYear == FinancialYear).ToList();
                }
                if (VerticalID != -1)
                {
                    query = query.Where(entry => entry.VerticalID == VerticalID).ToList();
                }

                grdAuditScheduling.DataSource = null;
                Session["TotalRows"] = query.Count;
                grdAuditScheduling.DataBind();
                if (query != null)
                {
                    grdAuditScheduling.DataSource = query;
                    Session["TotalRows"] = query.Count;
                    grdAuditScheduling.DataBind();
                }
            }

        }
        public void BindGridForNextpreviouse()
        {
            BindMainGrid();
        }

        protected void btnClose_Click(object sender, EventArgs e) 
        {            
            BindMainGrid();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        int verticalId = -1;
                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        int CustomerBranchId = -1;
                        if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                        {
                            if (ddlLegalEntityPop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                        {
                            if (ddlSubEntity1Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                        {
                            if (ddlSubEntity2Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                        {
                            if (ddlSubEntity3Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                        {
                            if (ddlSubEntity4Pop.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                        {
                            if (ddlVerticalListPopup.SelectedValue != "-1")
                            {
                                verticalId = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                            }
                        }



                        string financialyear = ddlFinancialYear.SelectedItem.Text;
                        string[] fsplit = financialyear.Split('-');
                        string fyear = fsplit[0];
                        string syear = fsplit[1];
                        string f1 = fyear + "-" + syear;
                        string f2 = Convert.ToInt32(fyear) + 1 + "-" + (Convert.ToInt32(syear) + 1);
                        string f3 = (Convert.ToInt32(fyear) + 1) + 1 + "-" + ((Convert.ToInt32(syear) + 1) + 1);

                        if (verticalId != -1)
                        {

                            #region Annually Save
                            if (ddlSchedulingType.SelectedItem.Text == "Annually")
                            {
                                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                int processid = -1;
                                for (int i = 0; i < grdAnnually.Rows.Count; i++)
                                {
                                    GridViewRow row = grdAnnually.Rows[i];
                                    Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                    if (!string.IsNullOrEmpty(lblProcessId.Text))
                                    {

                                        processid = Convert.ToInt32(lblProcessId.Text);
                                        CheckBox bf = (CheckBox) row.FindControl("chkAnnualy1");
                                        CheckBox bf1 = (CheckBox) row.FindControl("chkAnnualy2");
                                        CheckBox bf2 = (CheckBox) row.FindControl("chkAnnualy3");
                                        if (bf.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Annually"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Annually";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "A";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf1.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Annually"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Annually";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "A";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf2.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Annually"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Annually";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "A";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (InternalauditschedulingList.Count != 0)
                                {
                                    UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                    // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                }
                            }
                            #endregion

                            #region Half Yearly Save
                            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                            {
                                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                int processid = -1;
                                for (int i = 0; i < grdHalfYearly.Rows.Count; i++)
                                {
                                    GridViewRow row = grdHalfYearly.Rows[i];
                                    Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                    if (!string.IsNullOrEmpty(lblProcessId.Text))
                                    {
                                        processid = Convert.ToInt32(lblProcessId.Text);
                                        CheckBox bf = (CheckBox) row.FindControl("chkHalfyearly1");
                                        CheckBox bf1 = (CheckBox) row.FindControl("chkHalfyearly2");
                                        CheckBox bf2 = (CheckBox) row.FindControl("chkHalfyearly3");
                                        CheckBox bf3 = (CheckBox) row.FindControl("chkHalfyearly4");
                                        CheckBox bf4 = (CheckBox) row.FindControl("chkHalfyearly5");
                                        CheckBox bf5 = (CheckBox) row.FindControl("chkHalfyearly6");
                                        //first financial Year
                                        if (bf.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Apr-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Apr-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf1.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Oct-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Oct-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        //}//Second financial Year
                                        if (bf2.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Apr-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Apr-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf3.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Oct-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Oct-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        //}//Third financial Year
                                        if (bf4.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Apr-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Apr-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf5.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Oct-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Oct-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "H";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                    }
                                }
                                if (InternalauditschedulingList.Count != 0)
                                {
                                    UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                }
                            }
                            #endregion

                            #region Quarterly Save
                            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                            {
                                int processid = -1;
                                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                for (int i = 0; i < grdQuarterly.Rows.Count; i++)
                                {
                                    GridViewRow row = grdQuarterly.Rows[i];
                                    Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                    if (!string.IsNullOrEmpty(lblProcessId.Text))
                                    {
                                        processid = Convert.ToInt32(lblProcessId.Text);
                                        //first financial Year
                                        CheckBox bf1 = (CheckBox) row.FindControl("chkQuarter1");
                                        CheckBox bf2 = (CheckBox) row.FindControl("chkQuarter2");
                                        CheckBox bf3 = (CheckBox) row.FindControl("chkQuarter3");
                                        CheckBox bf4 = (CheckBox) row.FindControl("chkQuarter4");
                                        //Second financial Year
                                        CheckBox bf5 = (CheckBox) row.FindControl("chkQuarter5");
                                        CheckBox bf6 = (CheckBox) row.FindControl("chkQuarter6");
                                        CheckBox bf7 = (CheckBox) row.FindControl("chkQuarter7");
                                        CheckBox bf8 = (CheckBox) row.FindControl("chkQuarter8");
                                        //Third financial Year
                                        CheckBox bf9 = (CheckBox) row.FindControl("chkQuarter9");
                                        CheckBox bf10 = (CheckBox) row.FindControl("chkQuarter10");
                                        CheckBox bf11 = (CheckBox) row.FindControl("chkQuarter11");
                                        CheckBox bf12 = (CheckBox) row.FindControl("chkQuarter12");
                                        //first financial Year
                                        if (bf1.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Apr-Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Apr-Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf2.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Jul-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Jul-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf3.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Oct-Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Oct-Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf4.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Jan-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Jan-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        //Second financial Year                          
                                        if (bf5.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Apr-Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Apr-Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf6.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Jul-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Jul-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf7.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Oct-Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Oct-Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf8.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Jan-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Jan-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        //Third financial Year
                                        if (bf9.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Apr-Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Apr-Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf10.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Jul-Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Jul-Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf11.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Oct-Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Oct-Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf12.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Jan-Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Jan-Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "Q";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (InternalauditschedulingList.Count != 0)
                                {
                                    UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                }
                            }
                            #endregion

                            #region Monthly Save
                            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                            {
                                int processid = -1;
                                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                for (int i = 0; i < grdMonthly.Rows.Count; i++)
                                {

                                    GridViewRow row = grdMonthly.Rows[i];
                                    Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                    if (!string.IsNullOrEmpty(lblProcessId.Text))
                                    {
                                        processid = Convert.ToInt32(lblProcessId.Text);
                                        //first financial Year
                                        CheckBox bf1 = (CheckBox)row.FindControl("chkMonthly1");
                                        CheckBox bf2 = (CheckBox)row.FindControl("chkMonthly2");
                                        CheckBox bf3 = (CheckBox)row.FindControl("chkMonthly3");
                                        CheckBox bf4 = (CheckBox)row.FindControl("chkMonthly4");
                                        CheckBox bf5 = (CheckBox)row.FindControl("chkMonthly5");
                                        CheckBox bf6 = (CheckBox)row.FindControl("chkMonthly6");
                                        CheckBox bf7 = (CheckBox)row.FindControl("chkMonthly7");
                                        CheckBox bf8 = (CheckBox)row.FindControl("chkMonthly8");
                                        CheckBox bf9 = (CheckBox)row.FindControl("chkMonthly9");
                                        CheckBox bf10 = (CheckBox)row.FindControl("chkMonthly10");
                                        CheckBox bf11 = (CheckBox)row.FindControl("chkMonthly11");
                                        CheckBox bf12 = (CheckBox)row.FindControl("chkMonthly12");
                                        //Second financial Year
                                        CheckBox bf13 = (CheckBox)row.FindControl("chkMonthly13");
                                        CheckBox bf14 = (CheckBox)row.FindControl("chkMonthly14");
                                        CheckBox bf15 = (CheckBox)row.FindControl("chkMonthly15");
                                        CheckBox bf16 = (CheckBox)row.FindControl("chkMonthly16");
                                        CheckBox bf17 = (CheckBox)row.FindControl("chkMonthly17");
                                        CheckBox bf18 = (CheckBox)row.FindControl("chkMonthly18");
                                        CheckBox bf19 = (CheckBox)row.FindControl("chkMonthly19");
                                        CheckBox bf20 = (CheckBox)row.FindControl("chkMonthly20");
                                        CheckBox bf21 = (CheckBox)row.FindControl("chkMonthly21");
                                        CheckBox bf22 = (CheckBox)row.FindControl("chkMonthly22");
                                        CheckBox bf23 = (CheckBox)row.FindControl("chkMonthly23");
                                        CheckBox bf24 = (CheckBox)row.FindControl("chkMonthly24");
                                        //Third financial Year
                                        CheckBox bf25 = (CheckBox)row.FindControl("chkMonthly25");
                                        CheckBox bf26 = (CheckBox)row.FindControl("chkMonthly26");
                                        CheckBox bf27 = (CheckBox)row.FindControl("chkMonthly27");
                                        CheckBox bf28 = (CheckBox)row.FindControl("chkMonthly28");
                                        CheckBox bf29 = (CheckBox)row.FindControl("chkMonthly29");
                                        CheckBox bf30 = (CheckBox)row.FindControl("chkMonthly30");
                                        CheckBox bf31 = (CheckBox)row.FindControl("chkMonthly31");
                                        CheckBox bf32 = (CheckBox)row.FindControl("chkMonthly32");
                                        CheckBox bf33 = (CheckBox)row.FindControl("chkMonthly33");
                                        CheckBox bf34 = (CheckBox)row.FindControl("chkMonthly34");
                                        CheckBox bf35 = (CheckBox)row.FindControl("chkMonthly35");
                                        CheckBox bf36 = (CheckBox)row.FindControl("chkMonthly36");
                                        //first financial Year
                                        if (bf1.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Apr"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Apr";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf2.Checked)
                                        {

                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "May"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "May";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf3.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf4.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Jul"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Jul";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf5.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Aug"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Aug";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf6.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf7.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Oct"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Oct";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf8.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Nov"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Nov";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf9.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf10.Checked)
                                        {

                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Jan"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Jan";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf11.Checked)
                                        {

                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Feb"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Feb";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf12.Checked)
                                        {

                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f1;
                                                Internalauditscheduling.TermName = "Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        //Second financial Year 
                                        if (bf13.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Apr"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Apr";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                        if (bf14.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "May"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "May";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf15.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf16.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Jul"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Jul";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf17.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Aug"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Aug";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf18.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf19.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Oct"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Oct";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf20.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Nov"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Nov";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf21.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf22.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Jan"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Jan";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf23.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Feb"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Feb";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf24.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f2;
                                                Internalauditscheduling.TermName = "Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        //Third financial Year

                                        if (bf25.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Apr"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Apr";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf26.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "May"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "May";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf27.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Jun"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Jun";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf28.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Jul"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Jul";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf29.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Aug"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Aug";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf30.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Sep"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Sep";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf31.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Oct"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Oct";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf32.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Nov"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Nov";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf33.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Dec"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Dec";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf34.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Jan"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Jan";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf35.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Feb"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Feb";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }

                                        if (bf36.Checked)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Mar"))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                Internalauditscheduling.FinancialYear = f3;
                                                Internalauditscheduling.TermName = "Mar";
                                                Internalauditscheduling.TermStatus = true;
                                                Internalauditscheduling.Process = processid;
                                                Internalauditscheduling.ISAHQMP = "M";
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                if (processid != 0)
                                                {
                                                    InternalauditschedulingList.Add(Internalauditscheduling);
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (InternalauditschedulingList.Count != 0)
                                {
                                    UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                }
                            }
                            #endregion

                            #region Phase Save
                            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                            {
                                int processid = -1;
                                int noofphases = -1;
                                if (!string.IsNullOrEmpty(txtNoOfPhases.Text))
                                {
                                    noofphases = (Convert.ToInt32(txtNoOfPhases.Text));
                                }
                                #region Phase 1
                                if (noofphases == 1)
                                {
                                    List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                    for (int i = 0; i < grdphase1.Rows.Count; i++)
                                    {
                                        GridViewRow row = grdphase1.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            CheckBox bf = (CheckBox)row.FindControl("chkPhase1");
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkPhase2");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkPhase3");

                                            if (bf.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf1.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf2.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (InternalauditschedulingList.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                    }
                                }
                                #endregion
                                #region Phase 2
                                else if (noofphases == 2)
                                {
                                    List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                    for (int i = 0; i < grdphase2.Rows.Count; i++)
                                    {

                                        GridViewRow row = grdphase2.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                            CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                            CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                            if (bf1.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }
                                            if (bf2.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Second Finacial Year
                                            if (bf3.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf4.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Third Financial Year
                                            if (bf5.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf6.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (InternalauditschedulingList.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                    }
                                }
                                #endregion
                                #region Phase 3
                                else if (noofphases == 3)
                                {
                                    List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                    for (int i = 0; i < grdphase3.Rows.Count; i++)
                                    {

                                        GridViewRow row = grdphase3.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                            CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                            CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                            CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                            CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                            CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                            if (bf1.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf2.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf3.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Second Finacial Year                               
                                            if (bf4.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf5.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf6.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Third Financial Year                                
                                            if (bf7.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf8.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf9.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (InternalauditschedulingList.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                    }
                                }
                                #endregion
                                #region Phase 4
                                else if (noofphases == 4)
                                {
                                    List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                    for (int i = 0; i < grdphase4.Rows.Count; i++)
                                    {

                                        GridViewRow row = grdphase4.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                            CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                            CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                            CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                            CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                            CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                            CheckBox bf10 = (CheckBox)row.FindControl("chkPhase10");
                                            CheckBox bf11 = (CheckBox)row.FindControl("chkPhase11");
                                            CheckBox bf12 = (CheckBox)row.FindControl("chkPhase12");
                                            if (bf1.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf2.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf3.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf4.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Second Finacial Year                               
                                            if (bf5.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf6.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf7.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase3",noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf8.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Third Financial Year                                
                                            if (bf9.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf10.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf11.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf12.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                        }
                                    }
                                    if (InternalauditschedulingList.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                    }
                                }
                                #endregion
                                #region Phase 5
                                else if (noofphases == 5)
                                {
                                    List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                    for (int i = 0; i < grdphase5.Rows.Count; i++)
                                    {

                                        GridViewRow row = grdphase5.Rows[i];
                                        Label lblProcessId = (Label)row.FindControl("lblProcessID");
                                        if (!string.IsNullOrEmpty(lblProcessId.Text))
                                        {
                                            processid = Convert.ToInt32(lblProcessId.Text);
                                            CheckBox bf1 = (CheckBox)row.FindControl("chkPhase1");
                                            CheckBox bf2 = (CheckBox)row.FindControl("chkPhase2");
                                            CheckBox bf3 = (CheckBox)row.FindControl("chkPhase3");
                                            CheckBox bf4 = (CheckBox)row.FindControl("chkPhase4");
                                            CheckBox bf5 = (CheckBox)row.FindControl("chkPhase5");
                                            CheckBox bf6 = (CheckBox)row.FindControl("chkPhase6");
                                            CheckBox bf7 = (CheckBox)row.FindControl("chkPhase7");
                                            CheckBox bf8 = (CheckBox)row.FindControl("chkPhase8");
                                            CheckBox bf9 = (CheckBox)row.FindControl("chkPhase9");
                                            CheckBox bf10 = (CheckBox)row.FindControl("chkPhase10");
                                            CheckBox bf11 = (CheckBox)row.FindControl("chkPhase11");
                                            CheckBox bf12 = (CheckBox)row.FindControl("chkPhase12");
                                            CheckBox bf13 = (CheckBox)row.FindControl("chkPhase10");
                                            CheckBox bf14 = (CheckBox)row.FindControl("chkPhase11");
                                            CheckBox bf15 = (CheckBox)row.FindControl("chkPhase12");
                                            if (bf1.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf2.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf3.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf4.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf5.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f1, "Phase5",noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f1;
                                                    Internalauditscheduling.TermName = "Phase5";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Second Finacial Year                               
                                            if (bf6.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf7.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf8.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf9.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf10.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f2, "Phase5", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f2;
                                                    Internalauditscheduling.TermName = "Phase5";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            //Third Financial Year                                
                                            if (bf11.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase1", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase1";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf12.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase2", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase2";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf13.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase3", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase3";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf14.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase4", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase4";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                            if (bf15.Checked)
                                            {
                                                if (UserManagementRisk.GetPreviousAuditScheduling(CustomerBranchId, verticalId, f3, "Phase5", noofphases))
                                                {
                                                    InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    Internalauditscheduling.CustomerBranchId = Convert.ToInt32(CustomerBranchId);
                                                    Internalauditscheduling.FinancialYear = f3;
                                                    Internalauditscheduling.TermName = "Phase5";
                                                    Internalauditscheduling.TermStatus = true;
                                                    Internalauditscheduling.Process = processid;
                                                    Internalauditscheduling.ISAHQMP = "P";
                                                    Internalauditscheduling.PhaseCount = noofphases;
                                                    Internalauditscheduling.StartDate = Convert.ToDateTime(txtStartDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.EndDate = Convert.ToDateTime(txtEndDatePop.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    Internalauditscheduling.VerticalID = Convert.ToInt32(verticalId);
                                                    if (processid != 0)
                                                    {
                                                        InternalauditschedulingList.Add(Internalauditscheduling);
                                                    }
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Same audit is already scheduled for current selection.";
                                                    break;
                                                }
                                            }

                                        }
                                    }
                                    if (InternalauditschedulingList.Count != 0)
                                    {
                                        UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Audit Scheduling Record Save Successfully.";
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "ApplyToconfirm()", true);
                                    }
                                }
                                #endregion
                            }
                            #endregion

                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Vertical";
                        }
                        BindMainGrid();
                        BindGridpopData();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Financial Year";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                //int ID = -1;
                for (int i = 0; i < grdAuditScheduleStartEndDate.Rows.Count; i++)
                {
                    GridViewRow row = grdAuditScheduleStartEndDate.Rows[i];
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtStartDate = (TextBox)row.FindControl("txtExpectedStartDate");
                    TextBox txtEndDate = (TextBox)row.FindControl("txtExpectedEndDate");

                    if (txtStartDate.Text != "" && txtEndDate.Text != "")
                    {
                        if (!string.IsNullOrEmpty(lblID.Text))
                        {
                            DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                            Internalauditscheduling.Id = Convert.ToInt32(lblID.Text);
                            Internalauditscheduling.StartDate = GetDate(a.ToString("dd/MM/yyyy"));
                            Internalauditscheduling.EndDate = GetDate(b.ToString("dd/MM/yyyy"));
                            ProcessManagement.UpdateInternalAuditorScheduling(Internalauditscheduling);
                            cvDuplicateEntry23.IsValid = false;
                            cvDuplicateEntry23.ErrorMessage = "Data updated successfully.";
                        }
                    }
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry23.IsValid = false;
                cvDuplicateEntry23.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool IsValidDateFormat(string dateFormat)
        {
            try
            {
                String dts = DateTime.Now.ToString(dateFormat);
                DateTime.ParseExact(dts, dateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        #region Annually       
        protected void grdAnnually_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 1;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 1;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkAnnualy1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkAnnualy2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkAnnualy3");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Annually";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Annually12 = new CheckBox();
                                chk_Annually12.Text = "Annually";
                                chk_Annually12.ID = "Annually1_Id";
                                chk_Annually12.Attributes.Add("OnClick", "return func();");
                                cell1.Controls.Add(chk_Annually12);


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Annually";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Annually13 = new CheckBox();
                                chk_Annually13.Text = "Annually";
                                chk_Annually13.ID = "Annually2_Id";
                                chk_Annually13.Attributes.Add("OnClick", "return func1();");
                                cell2.Controls.Add(chk_Annually13);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Annually";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Annually14 = new CheckBox();
                                chk_Annually14.Text = "Annually";
                                chk_Annually14.ID = "Annually3_Id";
                                chk_Annually14.Attributes.Add("OnClick", "return func2();");
                                cell3.Controls.Add(chk_Annually14);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdAnnually_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAnnually.PageIndex = e.NewPageIndex;
            BindAuditSchedule("A", 0);
        }
        #endregion
        #region Haly Yearly
        protected void grdHalfYearly_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        GridViewRow gvRow = e.Row;
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 2;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 2;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell1 = e.Row.Cells[5];
                            otherCell1.Visible = false;
                            TableCell otherCell2 = e.Row.Cells[6];
                            otherCell2.Visible = false;
                            TableCell otherCell3 = e.Row.Cells[7];
                            otherCell3.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkHalfyearly1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkHalfyearly2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkHalfyearly3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkHalfyearly4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkHalfyearly5");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkHalfyearly6");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;



                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr-Sep";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Half1 = new CheckBox();
                                chk_Half1.Text = "Apr-Sep";
                                chk_Half1.ID = "Half1_Id";
                                chk_Half1.Attributes.Add("OnClick", "return funHalf1();");
                                cell1.Controls.Add(chk_Half1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Oct-Mar";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Half2 = new CheckBox();
                                chk_Half2.Text = "Oct-Mar";
                                chk_Half2.ID = "Half2_Id";
                                chk_Half2.Attributes.Add("OnClick", "return funHalf2();");
                                cell2.Controls.Add(chk_Half2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Apr-Sep";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Half3 = new CheckBox();
                                chk_Half3.Text = "Apr-Sep";
                                chk_Half3.ID = "Half3_Id";
                                chk_Half3.Attributes.Add("OnClick", "return funHalf3();");
                                cell3.Controls.Add(chk_Half3);

                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Oct-Mar";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Half4 = new CheckBox();
                                chk_Half4.Text = "Oct-Mar";
                                chk_Half4.ID = "Half4_Id";
                                chk_Half4.Attributes.Add("OnClick", "return funHalf4();");
                                cell4.Controls.Add(chk_Half4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Apr-Sep";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Half5 = new CheckBox();
                                chk_Half5.Text = "Apr-Sep";
                                chk_Half5.ID = "Half5_Id";
                                chk_Half5.Attributes.Add("OnClick", "return funHalf5();");
                                cell5.Controls.Add(chk_Half5);

                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Oct-Mar";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Half6 = new CheckBox();
                                chk_Half6.Text = "Oct-Mar";
                                chk_Half6.ID = "Half6_Id";
                                chk_Half6.Attributes.Add("OnClick", "return funHalf6();");
                                cell6.Controls.Add(chk_Half6);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        protected void grdHalfYearly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHalfYearly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("H", 0);
        }
        #endregion
        #region Quarterly
        protected void grdQuarterly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 4;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 4;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[11];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[12];
                            otherCell12.Visible = false;

                            TableCell otherCell3 = e.Row.Cells[13];
                            otherCell3.Visible = false;

                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            //first financial Year
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkQuarter1");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkQuarter2");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkQuarter3");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkQuarter4");
                            //Second financial Year
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkQuarter5");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkQuarter6");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkQuarter7");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkQuarter8");
                            //Third financial Year
                            CheckBox bf9 = (CheckBox)e.Row.FindControl("chkQuarter9");
                            CheckBox bf10 = (CheckBox)e.Row.FindControl("chkQuarter10");
                            CheckBox bf11 = (CheckBox)e.Row.FindControl("chkQuarter11");
                            CheckBox bf12 = (CheckBox)e.Row.FindControl("chkQuarter12");

                            if (lblProcessID.Text == "0")
                            {
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;
                                bf12.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr-Jun";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly1 = new CheckBox();
                                chk_Quarterly1.Text = "Apr-Jun";
                                chk_Quarterly1.ID = "Quarterly1_Id";
                                chk_Quarterly1.Attributes.Add("OnClick", "return funcQuarterly1();");
                                cell1.Controls.Add(chk_Quarterly1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Jul-Sep";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly2 = new CheckBox();
                                chk_Quarterly2.Text = "Jul-Sep";
                                chk_Quarterly2.ID = "Quarterly2_Id";
                                chk_Quarterly2.Attributes.Add("OnClick", "return funcQuarterly2();");
                                cell2.Controls.Add(chk_Quarterly2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Oct-Dec";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Quarterly3 = new CheckBox();
                                chk_Quarterly3.Text = "Oct-Dec";
                                chk_Quarterly3.ID = "Quarterly3_Id";
                                chk_Quarterly3.Attributes.Add("OnClick", "return funcQuarterly3();");
                                cell3.Controls.Add(chk_Quarterly3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Jan-Mar";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly4 = new CheckBox();
                                chk_Quarterly4.Text = "Jan-Mar";
                                chk_Quarterly4.ID = "Quarterly4_Id";
                                chk_Quarterly4.Attributes.Add("OnClick", "return funcQuarterly4();");
                                cell4.Controls.Add(chk_Quarterly4);


                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Apr-Jun";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly5 = new CheckBox();
                                chk_Quarterly5.Text = "Apr-Jun";
                                chk_Quarterly5.ID = "Quarterly5_Id";
                                chk_Quarterly5.Attributes.Add("OnClick", "return funcQuarterly5();");
                                cell5.Controls.Add(chk_Quarterly5);


                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Jul-Sep";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly6 = new CheckBox();
                                chk_Quarterly6.Text = "Jul-Sep";
                                chk_Quarterly6.ID = "Quarterly6_Id";
                                chk_Quarterly6.Attributes.Add("OnClick", "return funcQuarterly6();");
                                cell6.Controls.Add(chk_Quarterly6);


                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Oct-Dec";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly7 = new CheckBox();
                                chk_Quarterly7.Text = "Oct-Dec";
                                chk_Quarterly7.ID = "Quarterly7_Id";
                                chk_Quarterly7.Attributes.Add("OnClick", "return funcQuarterly7();");
                                cell7.Controls.Add(chk_Quarterly7);

                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Jan-Mar";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly8 = new CheckBox();
                                chk_Quarterly8.Text = "Jan-Mar";
                                chk_Quarterly8.ID = "Quarterly8_Id";
                                chk_Quarterly8.Attributes.Add("OnClick", "return funcQuarterly8();");
                                cell8.Controls.Add(chk_Quarterly8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Apr-Jun";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly9 = new CheckBox();
                                chk_Quarterly9.Text = "Apr-Jun";
                                chk_Quarterly9.ID = "Quarterly9_Id";
                                chk_Quarterly9.Attributes.Add("OnClick", "return funcQuarterly9();");
                                cell9.Controls.Add(chk_Quarterly9);

                                TableCell cell10 = e.Row.Cells[11];
                                cell10.ColumnSpan = 1;
                                //cell10.Text = "Jul-Sep";
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly10 = new CheckBox();
                                chk_Quarterly10.Text = "Jul-Sep";
                                chk_Quarterly10.ID = "Quarterly10_Id";
                                chk_Quarterly10.Attributes.Add("OnClick", "return funcQuarterly10();");
                                cell10.Controls.Add(chk_Quarterly10);

                                TableCell cell11 = e.Row.Cells[12];
                                cell11.ColumnSpan = 1;
                                //cell11.Text = "Oct-Dec";
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly11 = new CheckBox();
                                chk_Quarterly11.Text = "Oct-Dec";
                                chk_Quarterly11.ID = "Quarterly11_Id";
                                chk_Quarterly11.Attributes.Add("OnClick", "return funcQuarterly11();");
                                cell11.Controls.Add(chk_Quarterly11);

                                TableCell cell12 = e.Row.Cells[13];
                                cell12.ColumnSpan = 1;
                                cell12.Text = "Jan-Mar";
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Quarterly12 = new CheckBox();
                                chk_Quarterly12.Text = "Jan-Mar";
                                chk_Quarterly12.ID = "Quarterly12_Id";
                                chk_Quarterly12.Attributes.Add("OnClick", "return funcQuarterly12();");
                                cell12.Controls.Add(chk_Quarterly12);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdQuarterly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdQuarterly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("Q", 0);

        }
        #endregion
        #region Monthly
        protected void grdMonthly_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {

                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 12;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 12;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 12;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[11];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[12];
                            otherCell12.Visible = false;

                            TableCell otherCell13 = e.Row.Cells[13];
                            otherCell13.Visible = false;

                            TableCell otherCell14 = e.Row.Cells[14];
                            otherCell14.Visible = false;

                            TableCell otherCell15 = e.Row.Cells[15];
                            otherCell15.Visible = false;

                            TableCell otherCell16 = e.Row.Cells[16];
                            otherCell16.Visible = false;

                            TableCell otherCell17 = e.Row.Cells[17];
                            otherCell17.Visible = false;

                            TableCell otherCell18 = e.Row.Cells[18];
                            otherCell18.Visible = false;

                            TableCell otherCell19 = e.Row.Cells[19];
                            otherCell19.Visible = false;

                            TableCell otherCell20 = e.Row.Cells[20];
                            otherCell20.Visible = false;

                            TableCell otherCell21 = e.Row.Cells[21];
                            otherCell21.Visible = false;

                            TableCell otherCell22 = e.Row.Cells[22];
                            otherCell22.Visible = false;

                            TableCell otherCell23 = e.Row.Cells[23];
                            otherCell23.Visible = false;

                            TableCell otherCell24 = e.Row.Cells[24];
                            otherCell24.Visible = false;

                            TableCell otherCell25 = e.Row.Cells[25];
                            otherCell25.Visible = false;

                            TableCell otherCell26 = e.Row.Cells[26];
                            otherCell26.Visible = false;

                            TableCell otherCell27 = e.Row.Cells[27];
                            otherCell27.Visible = false;

                            TableCell otherCell28 = e.Row.Cells[28];
                            otherCell28.Visible = false;

                            TableCell otherCell29 = e.Row.Cells[29];
                            otherCell29.Visible = false;

                            TableCell otherCell30 = e.Row.Cells[30];
                            otherCell30.Visible = false;


                            TableCell otherCell31 = e.Row.Cells[31];
                            otherCell31.Visible = false;

                            TableCell otherCell32 = e.Row.Cells[32];
                            otherCell32.Visible = false;

                            TableCell otherCell33 = e.Row.Cells[33];
                            otherCell33.Visible = false;

                            TableCell otherCell34 = e.Row.Cells[34];
                            otherCell34.Visible = false;

                            TableCell otherCell35 = e.Row.Cells[35];
                            otherCell35.Visible = false;

                            TableCell otherCell36 = e.Row.Cells[36];
                            otherCell36.Visible = false;

                            TableCell otherCell37 = e.Row.Cells[37];
                            otherCell37.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            //first financial Year
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkMonthly1");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkMonthly2");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkMonthly3");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkMonthly4");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkMonthly5");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkMonthly6");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkMonthly7");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkMonthly8");
                            CheckBox bf9 = (CheckBox)e.Row.FindControl("chkMonthly9");
                            CheckBox bf10 = (CheckBox)e.Row.FindControl("chkMonthly10");
                            CheckBox bf11 = (CheckBox)e.Row.FindControl("chkMonthly11");
                            CheckBox bf12 = (CheckBox)e.Row.FindControl("chkMonthly12");
                            //Second financial Year
                            CheckBox bf13 = (CheckBox)e.Row.FindControl("chkMonthly13");
                            CheckBox bf14 = (CheckBox)e.Row.FindControl("chkMonthly14");
                            CheckBox bf15 = (CheckBox)e.Row.FindControl("chkMonthly15");
                            CheckBox bf16 = (CheckBox)e.Row.FindControl("chkMonthly16");
                            CheckBox bf17 = (CheckBox)e.Row.FindControl("chkMonthly17");
                            CheckBox bf18 = (CheckBox)e.Row.FindControl("chkMonthly18");
                            CheckBox bf19 = (CheckBox)e.Row.FindControl("chkMonthly19");
                            CheckBox bf20 = (CheckBox)e.Row.FindControl("chkMonthly20");
                            CheckBox bf21 = (CheckBox)e.Row.FindControl("chkMonthly21");
                            CheckBox bf22 = (CheckBox)e.Row.FindControl("chkMonthly22");
                            CheckBox bf23 = (CheckBox)e.Row.FindControl("chkMonthly23");
                            CheckBox bf24 = (CheckBox)e.Row.FindControl("chkMonthly24");
                            //Third financial Year
                            CheckBox bf25 = (CheckBox)e.Row.FindControl("chkMonthly25");
                            CheckBox bf26 = (CheckBox)e.Row.FindControl("chkMonthly26");
                            CheckBox bf27 = (CheckBox)e.Row.FindControl("chkMonthly27");
                            CheckBox bf28 = (CheckBox)e.Row.FindControl("chkMonthly28");
                            CheckBox bf29 = (CheckBox)e.Row.FindControl("chkMonthly29");
                            CheckBox bf30 = (CheckBox)e.Row.FindControl("chkMonthly30");
                            CheckBox bf31 = (CheckBox)e.Row.FindControl("chkMonthly31");
                            CheckBox bf32 = (CheckBox)e.Row.FindControl("chkMonthly32");
                            CheckBox bf33 = (CheckBox)e.Row.FindControl("chkMonthly33");
                            CheckBox bf34 = (CheckBox)e.Row.FindControl("chkMonthly34");
                            CheckBox bf35 = (CheckBox)e.Row.FindControl("chkMonthly35");
                            CheckBox bf36 = (CheckBox)e.Row.FindControl("chkMonthly36");

                            if (lblProcessID.Text == "0")
                            {
                                //first financial Year
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;
                                bf12.Visible = false;
                                //Second financial Year
                                bf13.Visible = false;
                                bf14.Visible = false;
                                bf15.Visible = false;
                                bf16.Visible = false;
                                bf17.Visible = false;
                                bf18.Visible = false;
                                bf19.Visible = false;
                                bf20.Visible = false;
                                bf21.Visible = false;
                                bf22.Visible = false;
                                bf23.Visible = false;
                                bf24.Visible = false;
                                //Third financial Year
                                bf25.Visible = false;
                                bf26.Visible = false;
                                bf27.Visible = false;
                                bf28.Visible = false;
                                bf29.Visible = false;
                                bf30.Visible = false;
                                bf31.Visible = false;
                                bf32.Visible = false;
                                bf33.Visible = false;
                                bf34.Visible = false;
                                bf35.Visible = false;
                                bf36.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly1 = new CheckBox();
                                chk_grdMonthly1.Text = "Apr";
                                chk_grdMonthly1.ID = "grdMonthly1_Id";
                                chk_grdMonthly1.Attributes.Add("OnClick", "return funMonthly1();");
                                cell1.Controls.Add(chk_grdMonthly1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "May";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly2 = new CheckBox();
                                chk_grdMonthly2.Text = "May";
                                chk_grdMonthly2.ID = "grdMonthly2_Id";
                                chk_grdMonthly2.Attributes.Add("OnClick", "return funMonthly2();");
                                cell2.Controls.Add(chk_grdMonthly2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Jun";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_grdMonthly3 = new CheckBox();
                                chk_grdMonthly3.Text = "Jun";
                                chk_grdMonthly3.ID = "grdMonthly3_Id";
                                chk_grdMonthly3.Attributes.Add("OnClick", "return funMonthly3();");
                                cell3.Controls.Add(chk_grdMonthly3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Jul";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly4 = new CheckBox();
                                chk_grdMonthly4.Text = "Jul";
                                chk_grdMonthly4.ID = "grdMonthly4_Id";
                                chk_grdMonthly4.Attributes.Add("OnClick", "return funMonthly4();");
                                cell4.Controls.Add(chk_grdMonthly4);


                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Aug";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly5 = new CheckBox();
                                chk_grdMonthly5.Text = "Aug";
                                chk_grdMonthly5.ID = "grdMonthly5_Id";
                                chk_grdMonthly5.Attributes.Add("OnClick", "return funMonthly5();");
                                cell5.Controls.Add(chk_grdMonthly5);


                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Sep";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly6 = new CheckBox();
                                chk_grdMonthly6.Text = "Sep";
                                chk_grdMonthly6.ID = "grdMonthly6_Id";
                                chk_grdMonthly6.Attributes.Add("OnClick", "return funMonthly6();");
                                cell6.Controls.Add(chk_grdMonthly6);

                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Oct";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly7 = new CheckBox();
                                chk_grdMonthly7.Text = "Oct";
                                chk_grdMonthly7.ID = "grdMonthly7_Id";
                                chk_grdMonthly7.Attributes.Add("OnClick", "return funMonthly7();");
                                cell7.Controls.Add(chk_grdMonthly7);

                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Nov";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly8 = new CheckBox();
                                chk_grdMonthly8.Text = "Nov";
                                chk_grdMonthly8.ID = "grdMonthly8_Id";
                                chk_grdMonthly8.Attributes.Add("OnClick", "return funMonthly8();");
                                cell8.Controls.Add(chk_grdMonthly8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Dec";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly9 = new CheckBox();
                                chk_grdMonthly9.Text = "Dec";
                                chk_grdMonthly9.ID = "grdMonthly9_Id";
                                chk_grdMonthly9.Attributes.Add("OnClick", "return funMonthly9();");
                                cell9.Controls.Add(chk_grdMonthly9);

                                TableCell cell10 = e.Row.Cells[11];
                                cell10.ColumnSpan = 1;
                                //cell10.Text = "Jan";
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly10 = new CheckBox();
                                chk_grdMonthly10.Text = "Jan";
                                chk_grdMonthly10.ID = "grdMonthly10_Id";
                                chk_grdMonthly10.Attributes.Add("OnClick", "return funMonthly10();");
                                cell10.Controls.Add(chk_grdMonthly10);

                                TableCell cell11 = e.Row.Cells[12];
                                cell11.ColumnSpan = 1;
                                //cell11.Text = "Feb";
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly11 = new CheckBox();
                                chk_grdMonthly11.Text = "Feb";
                                chk_grdMonthly11.ID = "grdMonthly11_Id";
                                chk_grdMonthly11.Attributes.Add("OnClick", "return funMonthly11();");
                                cell11.Controls.Add(chk_grdMonthly11);

                                TableCell cell12 = e.Row.Cells[13];
                                cell12.ColumnSpan = 1;
                                //cell12.Text = "Mar";
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly12 = new CheckBox();
                                chk_grdMonthly12.Text = "Mar";
                                chk_grdMonthly12.ID = "grdMonthly12_Id";
                                chk_grdMonthly12.Attributes.Add("OnClick", "return funMonthly12();");
                                cell12.Controls.Add(chk_grdMonthly12);

                                TableCell cell13 = e.Row.Cells[14];
                                cell13.ColumnSpan = 1;
                                //cell13.Text = "Apr";
                                cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell13.BorderColor = Color.White;
                                cell13.ForeColor = Color.White;
                                cell13.Font.Bold = true;
                                cell13.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly13 = new CheckBox();
                                chk_grdMonthly13.Text = "Apr";
                                chk_grdMonthly13.ID = "grdMonthly13_Id";
                                chk_grdMonthly13.Attributes.Add("OnClick", "return funMonthly13();");
                                cell13.Controls.Add(chk_grdMonthly13);

                                TableCell cell14 = e.Row.Cells[15];
                                cell14.ColumnSpan = 1;
                                //cell14.Text = "May";
                                cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell14.BorderColor = Color.White;
                                cell14.ForeColor = Color.White;
                                cell14.Font.Bold = true;
                                cell14.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly14 = new CheckBox();
                                chk_grdMonthly14.Text = "May";
                                chk_grdMonthly14.ID = "grdMonthly14_Id";
                                chk_grdMonthly14.Attributes.Add("OnClick", "return funMonthly14();");
                                cell14.Controls.Add(chk_grdMonthly14);


                                TableCell cell15 = e.Row.Cells[16];
                                cell15.ColumnSpan = 1;
                                //cell15.Text = "Jun";
                                cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell15.BorderColor = Color.White;
                                cell15.ForeColor = Color.White;
                                cell15.Font.Bold = true;
                                cell15.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly15 = new CheckBox();
                                chk_grdMonthly15.Text = "Jun";
                                chk_grdMonthly15.ID = "grdMonthly15_Id";
                                chk_grdMonthly15.Attributes.Add("OnClick", "return funMonthly15();");
                                cell15.Controls.Add(chk_grdMonthly15);


                                TableCell cell16 = e.Row.Cells[17];
                                cell16.ColumnSpan = 1;
                                //cell16.Text = "Jul";
                                cell16.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell16.BorderColor = Color.White;
                                cell16.ForeColor = Color.White;
                                cell16.Font.Bold = true;
                                cell16.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly16 = new CheckBox();
                                chk_grdMonthly16.Text = "Jul";
                                chk_grdMonthly16.ID = "grdMonthly16_Id";
                                chk_grdMonthly16.Attributes.Add("OnClick", "return funMonthly16();");
                                cell16.Controls.Add(chk_grdMonthly16);


                                TableCell cell17 = e.Row.Cells[18];
                                cell17.ColumnSpan = 1;
                                //cell17.Text = "Aug";
                                cell17.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell17.BorderColor = Color.White;
                                cell17.ForeColor = Color.White;
                                cell17.Font.Bold = true;
                                cell17.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly17 = new CheckBox();
                                chk_grdMonthly17.Text = "Aug";
                                chk_grdMonthly17.ID = "grdMonthly17_Id";
                                chk_grdMonthly17.Attributes.Add("OnClick", "return funMonthly17();");
                                cell17.Controls.Add(chk_grdMonthly17);

                                TableCell cell18 = e.Row.Cells[19];
                                cell18.ColumnSpan = 1;
                                //cell18.Text = "Sep";
                                cell18.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell18.BorderColor = Color.White;
                                cell18.ForeColor = Color.White;
                                cell18.Font.Bold = true;
                                cell18.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly18 = new CheckBox();
                                chk_grdMonthly18.Text = "Sep";
                                chk_grdMonthly18.ID = "grdMonthly18_Id";
                                chk_grdMonthly18.Attributes.Add("OnClick", "return funMonthly18();");
                                cell18.Controls.Add(chk_grdMonthly18);

                                TableCell cell19 = e.Row.Cells[20];
                                cell19.ColumnSpan = 1;
                                //cell19.Text = "Oct";
                                cell19.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell19.BorderColor = Color.White;
                                cell19.ForeColor = Color.White;
                                cell19.Font.Bold = true;
                                cell19.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly19 = new CheckBox();
                                chk_grdMonthly19.Text = "Oct";
                                chk_grdMonthly19.ID = "grdMonthly19_Id";
                                chk_grdMonthly19.Attributes.Add("OnClick", "return funMonthly19();");
                                cell19.Controls.Add(chk_grdMonthly19);

                                TableCell cell20 = e.Row.Cells[21];
                                cell20.ColumnSpan = 1;
                                //cell20.Text = "Nov";
                                cell20.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell20.BorderColor = Color.White;
                                cell20.ForeColor = Color.White;
                                cell20.Font.Bold = true;
                                cell20.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly20 = new CheckBox();
                                chk_grdMonthly20.Text = "Nov";
                                chk_grdMonthly20.ID = "grdMonthly20_Id";
                                chk_grdMonthly20.Attributes.Add("OnClick", "return funMonthly20();");
                                cell20.Controls.Add(chk_grdMonthly20);

                                TableCell cell21 = e.Row.Cells[22];
                                cell21.ColumnSpan = 1;
                                //cell21.Text = "Dec";
                                cell21.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell21.BorderColor = Color.White;
                                cell21.ForeColor = Color.White;
                                cell21.Font.Bold = true;
                                cell21.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly21 = new CheckBox();
                                chk_grdMonthly21.Text = "Dec";
                                chk_grdMonthly21.ID = "grdMonthly21_Id";
                                chk_grdMonthly21.Attributes.Add("OnClick", "return funMonthly21();");
                                cell21.Controls.Add(chk_grdMonthly21);

                                TableCell cell22 = e.Row.Cells[23];
                                cell22.ColumnSpan = 1;
                                //cell22.Text = "Jan";
                                cell22.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell22.BorderColor = Color.White;
                                cell22.ForeColor = Color.White;
                                cell22.Font.Bold = true;
                                cell22.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly22 = new CheckBox();
                                chk_grdMonthly22.Text = "Jan";
                                chk_grdMonthly22.ID = "grdMonthly22_Id";
                                chk_grdMonthly22.Attributes.Add("OnClick", "return funMonthly22();");
                                cell22.Controls.Add(chk_grdMonthly22);


                                TableCell cell23 = e.Row.Cells[24];
                                cell23.ColumnSpan = 1;
                                //cell23.Text = "Feb";
                                cell23.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell23.BorderColor = Color.White;
                                cell23.ForeColor = Color.White;
                                cell23.Font.Bold = true;
                                cell23.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly23 = new CheckBox();
                                chk_grdMonthly23.Text = "Feb";
                                chk_grdMonthly23.ID = "grdMonthly23_Id";
                                chk_grdMonthly23.Attributes.Add("OnClick", "return funMonthly23();");
                                cell23.Controls.Add(chk_grdMonthly23);


                                TableCell cell24 = e.Row.Cells[25];
                                cell24.ColumnSpan = 1;
                                //cell24.Text = "Mar";
                                cell24.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell24.BorderColor = Color.White;
                                cell24.ForeColor = Color.White;
                                cell24.Font.Bold = true;
                                cell24.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly24 = new CheckBox();
                                chk_grdMonthly24.Text = "Mar";
                                chk_grdMonthly24.ID = "grdMonthly24_Id";
                                chk_grdMonthly24.Attributes.Add("OnClick", "return funMonthly24();");
                                cell24.Controls.Add(chk_grdMonthly24);


                                TableCell cell25 = e.Row.Cells[26];
                                cell25.ColumnSpan = 1;
                                //cell25.Text = "Apr";
                                cell25.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell25.BorderColor = Color.White;
                                cell25.ForeColor = Color.White;
                                cell25.Font.Bold = true;
                                cell25.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly25 = new CheckBox();
                                chk_grdMonthly25.Text = "Apr";
                                chk_grdMonthly25.ID = "grdMonthly25_Id";
                                chk_grdMonthly25.Attributes.Add("OnClick", "return funMonthly25();");
                                cell25.Controls.Add(chk_grdMonthly25);

                                TableCell cell26 = e.Row.Cells[27];
                                cell26.ColumnSpan = 1;
                                cell26.Text = "May";
                                cell26.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell26.BorderColor = Color.White;
                                cell26.ForeColor = Color.White;
                                cell26.Font.Bold = true;
                                cell26.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly26 = new CheckBox();
                                chk_grdMonthly26.Text = "May";
                                chk_grdMonthly26.ID = "grdMonthly26_Id";
                                chk_grdMonthly26.Attributes.Add("OnClick", "return funMonthly26();");
                                cell26.Controls.Add(chk_grdMonthly26);

                                TableCell cell27 = e.Row.Cells[28];
                                cell27.ColumnSpan = 1;
                                //cell27.Text = "Jun";
                                cell27.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell27.BorderColor = Color.White;
                                cell27.ForeColor = Color.White;
                                cell27.Font.Bold = true;
                                cell27.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly27 = new CheckBox();
                                chk_grdMonthly27.Text = "Jun";
                                chk_grdMonthly27.ID = "grdMonthly27_Id";
                                chk_grdMonthly27.Attributes.Add("OnClick", "return funMonthly27();");
                                cell27.Controls.Add(chk_grdMonthly27);

                                TableCell cell28 = e.Row.Cells[29];
                                cell28.ColumnSpan = 1;
                                //cell28.Text = "Jul";
                                cell28.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell28.BorderColor = Color.White;
                                cell28.ForeColor = Color.White;
                                cell28.Font.Bold = true;
                                cell28.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly28 = new CheckBox();
                                chk_grdMonthly28.Text = "Jul";
                                chk_grdMonthly28.ID = "grdMonthly28_Id";
                                chk_grdMonthly28.Attributes.Add("OnClick", "return funMonthly28();");
                                cell28.Controls.Add(chk_grdMonthly28);

                                TableCell cell29 = e.Row.Cells[30];
                                cell29.ColumnSpan = 1;
                                //cell29.Text = "Aug";
                                cell29.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell29.BorderColor = Color.White;
                                cell29.ForeColor = Color.White;
                                cell29.Font.Bold = true;
                                cell29.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly29 = new CheckBox();
                                chk_grdMonthly29.Text = "Aug";
                                chk_grdMonthly29.ID = "grdMonthly29_Id";
                                chk_grdMonthly29.Attributes.Add("OnClick", "return funMonthly29();");
                                cell29.Controls.Add(chk_grdMonthly29);

                                TableCell cell30 = e.Row.Cells[31];
                                cell30.ColumnSpan = 1;
                                //cell30.Text = "Sep";
                                cell30.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell30.BorderColor = Color.White;
                                cell30.ForeColor = Color.White;
                                cell30.Font.Bold = true;
                                cell30.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly30 = new CheckBox();
                                chk_grdMonthly30.Text = "Sep";
                                chk_grdMonthly30.ID = "grdMonthly30_Id";
                                chk_grdMonthly30.Attributes.Add("OnClick", "return funMonthly30();");
                                cell30.Controls.Add(chk_grdMonthly30);

                                TableCell cell31 = e.Row.Cells[32];
                                cell31.ColumnSpan = 1;
                                //cell31.Text = "Oct";
                                cell31.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell31.BorderColor = Color.White;
                                cell31.ForeColor = Color.White;
                                cell31.Font.Bold = true;
                                cell31.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly31 = new CheckBox();
                                chk_grdMonthly31.Text = "Oct";
                                chk_grdMonthly31.ID = "grdMonthly31_Id";
                                chk_grdMonthly31.Attributes.Add("OnClick", "return funMonthly31();");
                                cell31.Controls.Add(chk_grdMonthly31);

                                TableCell cell32 = e.Row.Cells[33];
                                cell32.ColumnSpan = 1;
                                //cell32.Text = "Nov";
                                cell32.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell32.BorderColor = Color.White;
                                cell32.ForeColor = Color.White;
                                cell32.Font.Bold = true;
                                cell32.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly32 = new CheckBox();
                                chk_grdMonthly32.Text = "Nov";
                                chk_grdMonthly32.ID = "grdMonthly32_Id";
                                chk_grdMonthly32.Attributes.Add("OnClick", "return funMonthly32();");
                                cell32.Controls.Add(chk_grdMonthly32);

                                TableCell cell33 = e.Row.Cells[34];
                                cell33.ColumnSpan = 1;
                                //cell33.Text = "Dec";
                                cell33.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell33.BorderColor = Color.White;
                                cell33.ForeColor = Color.White;
                                cell33.Font.Bold = true;
                                cell33.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly33 = new CheckBox();
                                chk_grdMonthly33.Text = "Dec";
                                chk_grdMonthly33.ID = "grdMonthly33_Id";
                                chk_grdMonthly33.Attributes.Add("OnClick", "return funMonthly33();");
                                cell33.Controls.Add(chk_grdMonthly33);

                                TableCell cell34 = e.Row.Cells[35];
                                cell34.ColumnSpan = 1;
                                //cell34.Text = "Jan";
                                cell34.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell34.BorderColor = Color.White;
                                cell34.ForeColor = Color.White;
                                cell34.Font.Bold = true;
                                cell34.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly34 = new CheckBox();
                                chk_grdMonthly34.Text = "Jan";
                                chk_grdMonthly34.ID = "grdMonthly34_Id";
                                chk_grdMonthly34.Attributes.Add("OnClick", "return funMonthly34();");
                                cell34.Controls.Add(chk_grdMonthly34);

                                TableCell cell35 = e.Row.Cells[36];
                                cell35.ColumnSpan = 1;
                                //cell35.Text = "Feb";
                                cell35.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell35.BorderColor = Color.White;
                                cell35.ForeColor = Color.White;
                                cell35.Font.Bold = true;
                                cell35.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly35 = new CheckBox();
                                chk_grdMonthly35.Text = "Feb";
                                chk_grdMonthly35.ID = "grdMonthly35_Id";
                                chk_grdMonthly35.Attributes.Add("OnClick", "return funMonthly35();");
                                cell35.Controls.Add(chk_grdMonthly35);

                                TableCell cell36 = e.Row.Cells[37];
                                cell36.ColumnSpan = 1;
                                //cell36.Text = "Mar";
                                cell36.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell36.BorderColor = Color.White;
                                cell36.ForeColor = Color.White;
                                cell36.Font.Bold = true;
                                cell36.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_grdMonthly36 = new CheckBox();
                                chk_grdMonthly36.Text = "Mar";
                                chk_grdMonthly36.ID = "grdMonthly36_Id";
                                chk_grdMonthly36.Attributes.Add("OnClick", "return funMonthly36();");
                                cell36.Controls.Add(chk_grdMonthly36);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        protected void grdMonthly_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMonthly.PageIndex = e.NewPageIndex;
            BindAuditSchedule("M", 0);

        }
        #endregion
        #region Phase1
        protected void grdphase1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 1;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 1;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase1 = new CheckBox();
                                chk_Phase1.Text = "Phase1";
                                chk_Phase1.ID = "Phase1_Phase1_Id";
                                chk_Phase1.Attributes.Add("OnClick", "return funcphase1();");
                                cell1.Controls.Add(chk_Phase1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2 = new CheckBox();
                                chk_Phase2.Text = "Phase2";
                                chk_Phase2.ID = "Phase1_Phase2_Id";
                                chk_Phase2.Attributes.Add("OnClick", "return funcphase2();");
                                cell2.Controls.Add(chk_Phase2);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3 = new CheckBox();
                                chk_Phase3.Text = "Phase3";
                                chk_Phase3.ID = "Phase1_Phase3_Id";
                                chk_Phase3.Attributes.Add("OnClick", "return funcphase3();");
                                cell3.Controls.Add(chk_Phase3);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase1.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 1);

        }
        #endregion
        #region Phase 2
        protected void grdphase2_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;


                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 2;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 2;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell1 = e.Row.Cells[5];
                            otherCell1.Visible = false;

                            TableCell otherCell2 = e.Row.Cells[6];
                            otherCell2.Visible = false;

                            TableCell otherCell3 = e.Row.Cells[7];
                            otherCell3.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkPhase5");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkPhase6");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2_Phase1 = new CheckBox();
                                chk_Phase2_Phase1.Text = "Phase1";
                                chk_Phase2_Phase1.ID = "Phase2_Phase1_Id";
                                chk_Phase2_Phase1.Attributes.Add("OnClick", "return funcphase21();");
                                cell1.Controls.Add(chk_Phase2_Phase1);


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2_Phase2 = new CheckBox();
                                chk_Phase2_Phase2.Text = "Phase2";
                                chk_Phase2_Phase2.ID = "Phase2_Phase2_Id";
                                chk_Phase2_Phase2.Attributes.Add("OnClick", "return funcphase22();");
                                cell2.Controls.Add(chk_Phase2_Phase2);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase1";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase2_Phase3 = new CheckBox();
                                chk_Phase2_Phase3.Text = "Phase1";
                                chk_Phase2_Phase3.ID = "Phase2_Phase3_Id";
                                chk_Phase2_Phase3.Attributes.Add("OnClick", "return funcphase23();");
                                cell3.Controls.Add(chk_Phase2_Phase3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase2";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase2_Phase4 = new CheckBox();
                                chk_Phase2_Phase4.Text = "Phase2";
                                chk_Phase2_Phase4.ID = "Phase2_Phase4_Id";
                                chk_Phase2_Phase4.Attributes.Add("OnClick", "return funcphase24();");
                                cell4.Controls.Add(chk_Phase2_Phase4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Phase1";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase2_Phase5 = new CheckBox();
                                chk_Phase2_Phase5.Text = "Phase1";
                                chk_Phase2_Phase5.ID = "Phase2_Phase5_Id";
                                chk_Phase2_Phase5.Attributes.Add("OnClick", "return funcphase25();");
                                cell5.Controls.Add(chk_Phase2_Phase5);


                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Phase2";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase2_Phase6 = new CheckBox();
                                chk_Phase2_Phase6.Text = "Phase2";
                                chk_Phase2_Phase6.ID = "Phase2_Phase6_Id";
                                chk_Phase2_Phase6.Attributes.Add("OnClick", "return funcphase26();");
                                cell6.Controls.Add(chk_Phase2_Phase6);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase2.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 2);
        }
        #endregion
        #region Phase 3
        protected void grdphase3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 3;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 3;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 3;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkPhase5");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkPhase6");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkPhase7");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkPhase8");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkPhase9");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;


                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase1 = new CheckBox();
                                chk_Phase3_Phase1.Text = "Phase1";
                                chk_Phase3_Phase1.ID = "Phase3_Phase1_Id";
                                chk_Phase3_Phase1.Attributes.Add("OnClick", "return funcphase31();");
                                cell1.Controls.Add(chk_Phase3_Phase1);


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase2 = new CheckBox();
                                chk_Phase3_Phase2.Text = "Phase2";
                                chk_Phase3_Phase2.ID = "Phase3_Phase2_Id";
                                chk_Phase3_Phase2.Attributes.Add("OnClick", "return funcphase32();");
                                cell2.Controls.Add(chk_Phase3_Phase2);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase3_Phase3 = new CheckBox();
                                chk_Phase3_Phase3.Text = "Phase3";
                                chk_Phase3_Phase3.ID = "Phase3_Phase3_Id";
                                chk_Phase3_Phase3.Attributes.Add("OnClick", "return funcphase33();");
                                cell3.Controls.Add(chk_Phase3_Phase3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase1";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase4 = new CheckBox();
                                chk_Phase3_Phase4.Text = "Phase1";
                                chk_Phase3_Phase4.ID = "Phase3_Phase4_Id";
                                chk_Phase3_Phase4.Attributes.Add("OnClick", "return funcphase34();");
                                cell4.Controls.Add(chk_Phase3_Phase4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Phase2";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase5 = new CheckBox();
                                chk_Phase3_Phase5.Text = "Phase2";
                                chk_Phase3_Phase5.ID = "Phase3_Phase5_Id";
                                chk_Phase3_Phase5.Attributes.Add("OnClick", "return funcphase35();");
                                cell5.Controls.Add(chk_Phase3_Phase5);

                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Phase3";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase6 = new CheckBox();
                                chk_Phase3_Phase6.Text = "Phase3";
                                chk_Phase3_Phase6.ID = "Phase3_Phase6_Id";
                                chk_Phase3_Phase6.Attributes.Add("OnClick", "return funcphase36();");
                                cell6.Controls.Add(chk_Phase3_Phase6);


                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Phase1";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase7 = new CheckBox();
                                chk_Phase3_Phase7.Text = "Phase1";
                                chk_Phase3_Phase7.ID = "Phase3_Phase7_Id";
                                chk_Phase3_Phase7.Attributes.Add("OnClick", "return funcphase37();");
                                cell7.Controls.Add(chk_Phase3_Phase7);


                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Phase2";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase8 = new CheckBox();
                                chk_Phase3_Phase8.Text = "Phase2";
                                chk_Phase3_Phase8.ID = "Phase3_Phase8_Id";
                                chk_Phase3_Phase8.Attributes.Add("OnClick", "return funcphase38();");
                                cell8.Controls.Add(chk_Phase3_Phase8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Phase3";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase3_Phase9 = new CheckBox();
                                chk_Phase3_Phase9.Text = "Phase3";
                                chk_Phase3_Phase9.ID = "Phase3_Phase9_Id";
                                chk_Phase3_Phase9.Attributes.Add("OnClick", "return funcphase39();");
                                cell9.Controls.Add(chk_Phase3_Phase9);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        protected void grdphase3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase3.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 3);
        }
        #endregion
        #region Phase 4
        protected void grdphase4_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 4;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 4;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[11];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[12];
                            otherCell12.Visible = false;

                            TableCell otherCell13 = e.Row.Cells[13];
                            otherCell13.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkPhase5");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkPhase6");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkPhase7");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkPhase8");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkPhase9");
                            CheckBox bf9 = (CheckBox)e.Row.FindControl("chkPhase10");
                            CheckBox bf10 = (CheckBox)e.Row.FindControl("chkPhase11");
                            CheckBox bf11 = (CheckBox)e.Row.FindControl("chkPhase12");
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase1 = new CheckBox();
                                chk_Phase4_Phase1.Text = "Phase1";
                                chk_Phase4_Phase1.ID = "Phase4_Phase1_Id";
                                chk_Phase4_Phase1.Attributes.Add("OnClick", "return funcphase41();");
                                cell1.Controls.Add(chk_Phase4_Phase1);


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase2 = new CheckBox();
                                chk_Phase4_Phase2.Text = "Phase2";
                                chk_Phase4_Phase2.ID = "Phase4_Phase2_Id";
                                chk_Phase4_Phase2.Attributes.Add("OnClick", "return funcphase42();");
                                cell2.Controls.Add(chk_Phase4_Phase2);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase4_Phase3 = new CheckBox();
                                chk_Phase4_Phase3.Text = "Phase3";
                                chk_Phase4_Phase3.ID = "Phase4_Phase3_Id";
                                chk_Phase4_Phase3.Attributes.Add("OnClick", "return funcphase43();");
                                cell3.Controls.Add(chk_Phase4_Phase3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase4";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase4 = new CheckBox();
                                chk_Phase4_Phase4.Text = "Phase4";
                                chk_Phase4_Phase4.ID = "Phase4_Phase4_Id";
                                chk_Phase4_Phase4.Attributes.Add("OnClick", "return funcphase44();");
                                cell4.Controls.Add(chk_Phase4_Phase4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Phase1";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase5 = new CheckBox();
                                chk_Phase4_Phase5.Text = "Phase1";
                                chk_Phase4_Phase5.ID = "Phase4_Phase5_Id";
                                chk_Phase4_Phase5.Attributes.Add("OnClick", "return funcphase45();");
                                cell5.Controls.Add(chk_Phase4_Phase5);

                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Phase2";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase6 = new CheckBox();
                                chk_Phase4_Phase6.Text = "Phase2";
                                chk_Phase4_Phase6.ID = "Phase4_Phase6_Id";
                                chk_Phase4_Phase6.Attributes.Add("OnClick", "return funcphase46();");
                                cell6.Controls.Add(chk_Phase4_Phase6);


                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Phase3";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase7 = new CheckBox();
                                chk_Phase4_Phase7.Text = "Phase3";
                                chk_Phase4_Phase7.ID = "Phase4_Phase7_Id";
                                chk_Phase4_Phase7.Attributes.Add("OnClick", "return funcphase47();");
                                cell7.Controls.Add(chk_Phase4_Phase7);

                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Phase4";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase8 = new CheckBox();
                                chk_Phase4_Phase8.Text = "Phase4";
                                chk_Phase4_Phase8.ID = "Phase4_Phase8_Id";
                                chk_Phase4_Phase8.Attributes.Add("OnClick", "return funcphase48();");
                                cell8.Controls.Add(chk_Phase4_Phase8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Phase1";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase9 = new CheckBox();
                                chk_Phase4_Phase9.Text = "Phase1";
                                chk_Phase4_Phase9.ID = "Phase4_Phase9_Id";
                                chk_Phase4_Phase9.Attributes.Add("OnClick", "return funcphase49();");
                                cell9.Controls.Add(chk_Phase4_Phase9);

                                TableCell cell10 = e.Row.Cells[11];
                                cell10.ColumnSpan = 1;
                                //cell10.Text = "Phase2";
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase10 = new CheckBox();
                                chk_Phase4_Phase10.Text = "Phase2";
                                chk_Phase4_Phase10.ID = "Phase4_Phase10_Id";
                                chk_Phase4_Phase10.Attributes.Add("OnClick", "return funcphase410();");
                                cell10.Controls.Add(chk_Phase4_Phase10);


                                TableCell cell11 = e.Row.Cells[12];
                                cell11.ColumnSpan = 1;
                                //cell11.Text = "Phase3";
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase11 = new CheckBox();
                                chk_Phase4_Phase11.Text = "Phase3";
                                chk_Phase4_Phase11.ID = "Phase4_Phase11_Id";
                                chk_Phase4_Phase11.Attributes.Add("OnClick", "return funcphase411();");
                                cell11.Controls.Add(chk_Phase4_Phase11);

                                TableCell cell12 = e.Row.Cells[13];
                                cell12.ColumnSpan = 1;
                                //cell12.Text = "Phase4";
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase4_Phase12 = new CheckBox();
                                chk_Phase4_Phase12.Text = "Phase4";
                                chk_Phase4_Phase12.ID = "Phase4_Phase12_Id";
                                chk_Phase4_Phase12.Attributes.Add("OnClick", "return funcphase412();");
                                cell12.Controls.Add(chk_Phase4_Phase12);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase4.PageIndex = e.NewPageIndex;

            BindAuditSchedule("P", 4);
        }
        #endregion
        #region Phase 5
        protected void grdphase5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;
                if (ddlFinancialYear.SelectedItem.Text != " Select Financial Year ")
                {
                    string financialyear = ddlFinancialYear.SelectedItem.Text;
                    string[] a = financialyear.Split('-');
                    string aaa = a[0];
                    string bbb = a[1];
                    string f1 = aaa + "-" + bbb;
                    string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                    string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);
                    if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 5;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell2 = e.Row.Cells[3];
                            cell2.ColumnSpan = 5;
                            cell2.Text = f2;
                            cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell2.BorderColor = Color.White;
                            cell2.Font.Bold = true;
                            cell2.ForeColor = Color.White;
                            cell2.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell3 = e.Row.Cells[4];
                            cell3.ColumnSpan = 5;
                            cell3.Text = f3;
                            cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell3.BorderColor = Color.White;
                            cell3.Font.Bold = true;
                            cell3.ForeColor = Color.White;
                            cell3.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[11];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[12];
                            otherCell12.Visible = false;

                            TableCell otherCell13 = e.Row.Cells[13];
                            otherCell13.Visible = false;

                            TableCell otherCell14 = e.Row.Cells[14];
                            otherCell14.Visible = false;

                            TableCell otherCell15 = e.Row.Cells[15];
                            otherCell15.Visible = false;

                            TableCell otherCell16 = e.Row.Cells[16];
                            otherCell16.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox)e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox)e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox)e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox)e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox)e.Row.FindControl("chkPhase5");
                            CheckBox bf5 = (CheckBox)e.Row.FindControl("chkPhase6");
                            CheckBox bf6 = (CheckBox)e.Row.FindControl("chkPhase7");
                            CheckBox bf7 = (CheckBox)e.Row.FindControl("chkPhase8");
                            CheckBox bf8 = (CheckBox)e.Row.FindControl("chkPhase9");
                            CheckBox bf9 = (CheckBox)e.Row.FindControl("chkPhase10");
                            CheckBox bf10 = (CheckBox)e.Row.FindControl("chkPhase11");
                            CheckBox bf11 = (CheckBox)e.Row.FindControl("chkPhase12");

                            CheckBox bf12 = (CheckBox)e.Row.FindControl("chkPhase13");
                            CheckBox bf13 = (CheckBox)e.Row.FindControl("chkPhase14");
                            CheckBox bf14 = (CheckBox)e.Row.FindControl("chkPhase15");

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;
                                bf12.Visible = false;
                                bf13.Visible = false;
                                bf14.Visible = false;


                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase1 = new CheckBox();
                                chk_Phase5_Phase1.Text = "Phase1";
                                chk_Phase5_Phase1.ID = "Phase5_Phase1_Id";
                                chk_Phase5_Phase1.Attributes.Add("OnClick", "return funcphase51();");
                                cell1.Controls.Add(chk_Phase5_Phase1);


                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase2 = new CheckBox();
                                chk_Phase5_Phase2.Text = "Phase2";
                                chk_Phase5_Phase2.ID = "Phase5_Phase2_Id";
                                chk_Phase5_Phase2.Attributes.Add("OnClick", "return funcphase52();");
                                cell2.Controls.Add(chk_Phase5_Phase2);


                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox chk_Phase5_Phase3 = new CheckBox();
                                chk_Phase5_Phase3.Text = "Phase3";
                                chk_Phase5_Phase3.ID = "Phase5_Phase3_Id";
                                chk_Phase5_Phase3.Attributes.Add("OnClick", "return funcphase53();");
                                cell3.Controls.Add(chk_Phase5_Phase3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase4";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase4 = new CheckBox();
                                chk_Phase5_Phase4.Text = "Phase4";
                                chk_Phase5_Phase4.ID = "Phase5_Phase4_Id";
                                chk_Phase5_Phase4.Attributes.Add("OnClick", "return funcphase54();");
                                cell4.Controls.Add(chk_Phase5_Phase4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Phase5";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase5 = new CheckBox();
                                chk_Phase5_Phase5.Text = "Phase5";
                                chk_Phase5_Phase5.ID = "Phase5_Phase5_Id";
                                chk_Phase5_Phase5.Attributes.Add("OnClick", "return funcphase55();");
                                cell5.Controls.Add(chk_Phase5_Phase5);

                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Phase1";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase6 = new CheckBox();
                                chk_Phase5_Phase6.Text = "Phase1";
                                chk_Phase5_Phase6.ID = "Phase5_Phase6_Id";
                                chk_Phase5_Phase6.Attributes.Add("OnClick", "return funcphase56();");
                                cell6.Controls.Add(chk_Phase5_Phase6);

                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Phase2";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase7 = new CheckBox();
                                chk_Phase5_Phase7.Text = "Phase2";
                                chk_Phase5_Phase7.ID = "Phase5_Phase7_Id";
                                chk_Phase5_Phase7.Attributes.Add("OnClick", "return funcphase57();");
                                cell7.Controls.Add(chk_Phase5_Phase7);

                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Phase3";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase8 = new CheckBox();
                                chk_Phase5_Phase8.Text = "Phase3";
                                chk_Phase5_Phase8.ID = "Phase5_Phase8_Id";
                                chk_Phase5_Phase8.Attributes.Add("OnClick", "return funcphase58();");
                                cell8.Controls.Add(chk_Phase5_Phase8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Phase4";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase9 = new CheckBox();
                                chk_Phase5_Phase9.Text = "Phase4";
                                chk_Phase5_Phase9.ID = "Phase5_Phase9_Id";
                                chk_Phase5_Phase9.Attributes.Add("OnClick", "return funcphase59();");
                                cell9.Controls.Add(chk_Phase5_Phase9);

                                TableCell cell10 = e.Row.Cells[11];
                                cell10.ColumnSpan = 1;
                                //cell10.Text = "Phase5";
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase10 = new CheckBox();
                                chk_Phase5_Phase10.Text = "Phase5";
                                chk_Phase5_Phase10.ID = "Phase5_Phase10_Id";
                                chk_Phase5_Phase10.Attributes.Add("OnClick", "return funcphase510();");
                                cell10.Controls.Add(chk_Phase5_Phase10);


                                TableCell cell11 = e.Row.Cells[12];
                                cell11.ColumnSpan = 1;
                                //cell11.Text = "Phase1";
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase11 = new CheckBox();
                                chk_Phase5_Phase11.Text = "Phase1";
                                chk_Phase5_Phase11.ID = "Phase5_Phase11_Id";
                                chk_Phase5_Phase11.Attributes.Add("OnClick", "return funcphase511();");
                                cell11.Controls.Add(chk_Phase5_Phase11);

                                TableCell cell12 = e.Row.Cells[13];
                                cell12.ColumnSpan = 1;
                                //cell12.Text = "Phase2";
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase12 = new CheckBox();
                                chk_Phase5_Phase12.Text = "Phase2";
                                chk_Phase5_Phase12.ID = "Phase5_Phase12_Id";
                                chk_Phase5_Phase12.Attributes.Add("OnClick", "return funcphase512();");
                                cell12.Controls.Add(chk_Phase5_Phase12);


                                TableCell cell13 = e.Row.Cells[14];
                                cell13.ColumnSpan = 1;
                                //cell13.Text = "Phase3";
                                cell13.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell13.BorderColor = Color.White;
                                cell13.ForeColor = Color.White;
                                cell13.Font.Bold = true;
                                cell13.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase13 = new CheckBox();
                                chk_Phase5_Phase13.Text = "Phase3";
                                chk_Phase5_Phase13.ID = "Phase5_Phase13_Id";
                                chk_Phase5_Phase13.Attributes.Add("OnClick", "return funcphase513();");
                                cell13.Controls.Add(chk_Phase5_Phase13);

                                TableCell cell14 = e.Row.Cells[15];
                                cell14.ColumnSpan = 1;
                                //cell14.Text = "Phase4";
                                cell14.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell14.BorderColor = Color.White;
                                cell14.ForeColor = Color.White;
                                cell14.Font.Bold = true;
                                cell14.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase14 = new CheckBox();
                                chk_Phase5_Phase14.Text = "Phase4";
                                chk_Phase5_Phase14.ID = "Phase5_Phase14_Id";
                                chk_Phase5_Phase14.Attributes.Add("OnClick", "return funcphase514();");
                                cell14.Controls.Add(chk_Phase5_Phase14);

                                TableCell cell15 = e.Row.Cells[16];
                                cell15.ColumnSpan = 1;
                                //cell15.Text = "Phase5";
                                cell15.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell15.BorderColor = Color.White;
                                cell15.ForeColor = Color.White;
                                cell15.Font.Bold = true;
                                cell15.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox chk_Phase5_Phase15 = new CheckBox();
                                chk_Phase5_Phase15.Text = "Phase5";
                                chk_Phase5_Phase15.ID = "Phase5_Phase15_Id";
                                chk_Phase5_Phase15.Attributes.Add("OnClick", "return funcphase515();");
                                cell15.Controls.Add(chk_Phase5_Phase15);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void grdphase5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase5.PageIndex = e.NewPageIndex;
            BindAuditSchedule("P", 5);
        }
        #endregion
        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {

        }
        protected bool ViewSchedule(long? EventID, object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {

                if (EventID != null)
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {
                        return true;

                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void grdAuditScheduling_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void grdAuditScheduling_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdAuditScheduling_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            try
            {


                if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[4]);
                        }
                        OpenScheduleInformation(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid);
                    }
                }
                if (e.CommandName.Equals("DELETE_SCHEDULE"))
                {
                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                            lblEDITLocation.Text = ShowCustomerBranchName(CustomerBranchId);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[4]);
                        }
                        DeleteInternalAuditScheduling(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid);
                        BindMainGrid();
                        //cvDuplicateEntry50.IsValid = false;
                        //cvDuplicateEntry50.ErrorMessage = "Schedule Deleted Successfully..!";
                    }
                }
                if (e.CommandName.Equals("EDIT_SCHEDULE"))
                {

                    string isAHQMP = string.Empty;
                    string FinancialYear = string.Empty;
                    string StartDate = string.Empty;
                    string EndDate = string.Empty;
                    int CustomerBranchId = -1;
                    int Verticalid = -1;
                    string Termname = string.Empty;
                    int PhaseCount = 0;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(commandArgs[0]))
                        {
                            isAHQMP = Convert.ToString(commandArgs[0]);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[1]))
                        {
                            FinancialYear = Convert.ToString(commandArgs[1]);
                            lblEDITFinancialyear.Text = FinancialYear;
                        }
                        if (!string.IsNullOrEmpty(commandArgs[2]))
                        {
                            CustomerBranchId = Convert.ToInt32(commandArgs[2]);
                            lblEDITLocation.Text = ShowCustomerBranchName(CustomerBranchId);
                            lblEDITLocationID.Text = Convert.ToString(CustomerBranchId);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[3]))
                        {
                            Termname = Convert.ToString(commandArgs[3]);
                            lblEDITTermName.Text = Termname;
                        }
                        if (!string.IsNullOrEmpty(commandArgs[4]))
                        {
                            PhaseCount = Convert.ToInt32(commandArgs[4]);
                            lblEDITPhaseCount.Text = Convert.ToString(PhaseCount);
                        }
                        if (!string.IsNullOrEmpty(commandArgs[5]))
                        {
                            Verticalid = Convert.ToInt32(commandArgs[5]);
                            int customerID = -1;
                            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            lblEDITVerticalID.Text = Convert.ToString(Verticalid);
                            lblVerticalName.Text = ShowVerticalName(customerID, Verticalid);
                        }
                        GridViewRow gvr = (GridViewRow) (((LinkButton) e.CommandSource).NamingContainer);
                        int rowIndex = gvr.RowIndex;

                        //Reference the GridView Row.
                        GridViewRow row = grdAuditScheduling.Rows[rowIndex];
                        Label lblEDITGridStartDate = row.FindControl("lblEDITGridStartDate") as Label;
                        //Fetch value of StartDate.
                        if (lblEDITGridStartDate !=null)
                        {
                            StartDate = (row.FindControl("lblEDITGridStartDate") as Label).Text;
                            lblEDITStartDate.Text = Convert.ToString(StartDate);
                        }
                        Label lblEDITGridEndDate = row.FindControl("lblEDITGridEndDate") as Label;
                        if (lblEDITGridEndDate != null)
                        {
                            //Fetch value of StartDate.
                            EndDate = (row.FindControl("lblEDITGridEndDate") as Label).Text;
                            lblEDITEndDate.Text = Convert.ToString(EndDate);
                        }
                        
                      

                        ViewState["EDITISAHQMP"] = null;
                        ViewState["EDITFinancialYear"] = null;
                        ViewState["EDITCustomerBranchId"] = null;
                        ViewState["EDITTermname"] = null;
                        ViewState["EDITVerticalId"] = null;
                        ViewState["EDITISAHQMP"] = isAHQMP;
                        ViewState["EDITFinancialYear"] = FinancialYear;
                        ViewState["EDITCustomerBranchId"] = CustomerBranchId;
                        ViewState["EDITTermname"] = Termname;
                        ViewState["EDITVerticalId"] = Verticalid;
                        if (isAHQMP == "A")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("A");
                            EnableDisableEDIT(0, "A");
                            BindAuditScheduleEDIT("A", 0, Convert.ToInt32(CustomerBranchId), Verticalid);
                        }
                        else if (isAHQMP == "H")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("H");
                            EnableDisableEDIT(0, "H");
                            BindAuditScheduleEDIT("H", 0, Convert.ToInt32(CustomerBranchId), Verticalid);
                        }
                        else if (isAHQMP == "Q")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("Q");
                            EnableDisableEDIT(0, "Q");
                            BindAuditScheduleEDIT("Q", 0, Convert.ToInt32(CustomerBranchId), Verticalid);
                        }
                        else if (isAHQMP == "M")
                        {
                            lblEDITSchedulingType.Text = ShowStatus("M");
                            EnableDisableEDIT(0, "M");
                            BindAuditScheduleEDIT("M", 0, Convert.ToInt32(CustomerBranchId), Verticalid);
                        }
                        else
                        {
                            if (PhaseCount == 1)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                EnableDisableEDIT(1, "P");
                                BindAuditScheduleEDIT("P", 1, Convert.ToInt32(CustomerBranchId), Verticalid);
                            }
                            else if (PhaseCount == 2)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                EnableDisableEDIT(2, "P");
                                BindAuditScheduleEDIT("P", 2, Convert.ToInt32(CustomerBranchId), Verticalid);
                            }
                            else if (PhaseCount == 3)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                EnableDisableEDIT(3, "P");
                                BindAuditScheduleEDIT("P", 3, Convert.ToInt32(CustomerBranchId), Verticalid);
                            }
                            else if (PhaseCount == 4)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                EnableDisableEDIT(4, "P");
                                BindAuditScheduleEDIT("P", 4, Convert.ToInt32(CustomerBranchId), Verticalid);
                            }
                            else if (PhaseCount == 5)
                            {
                                lblEDITSchedulingType.Text = ShowStatus("P");
                                EnableDisableEDIT(5, "P");
                                BindAuditScheduleEDIT("P", 5, Convert.ToInt32(CustomerBranchId), Verticalid);
                            }
                        }
                        upEDITComplianceDetails.Update();                       
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool CheckProcessAsignedOrNotExists(string FinancialYear, string ForPeriod, long CustomerBranchId,int Verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             join row1 in entities.InternalAuditScheduleOns
                             on row.ID equals row1.InternalAuditInstance
                             where row.ProcessId == row1.ProcessId && row1.FinancialYear == FinancialYear
                             && row1.ForMonth == ForPeriod && row.CustomerBranchID == CustomerBranchId && row.VerticalID==Verticalid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool DeleteInternalAuditScheduling(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname,int Verticalid)
        {

            try
            {
                bool isdeletesucess = false;
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    if (CheckProcessAsignedOrNotExists(FinancialYear, Termname, CustomerBranchId, Verticalid) == false)
                    {
                        var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                                     where row.FinancialYear == FinancialYear && row.IsDeleted == false
                                                     && row.ISAHQMP == isAHQMP && row.CustomerBranchId == CustomerBranchId
                                                     && row.TermName == Termname && row.VerticalID == Verticalid
                                                     select row.Id).ToList();
                        AuditorMastertoDelete.ForEach(entry =>
                       {
                           InternalAuditScheduling prevmappedids = (from row in entities.InternalAuditSchedulings
                                                                    where row.Id == entry
                                                                    select row).FirstOrDefault();
                           prevmappedids.IsDeleted = true;
                           entities.SaveChanges();
                       });
                        isdeletesucess = true;
                    }
                    else
                    {                        
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Audit already Kickoff. So, Audit Schedule can not be Edit/Delete...!";
                        isdeletesucess = false;
                    }
                    return isdeletesucess;
                }
            }
            catch (Exception ex)
            {
               
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
     
        public bool DeleteInternalAuditSchedulingWithCount(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname, int phasecount,int VerticalId)
        {
            try
            {
                bool isdeletesucess = false;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (CheckProcessAsignedOrNotExists(FinancialYear, Termname, CustomerBranchId, VerticalId) == false)
                    {
                        var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                                     where row.FinancialYear == FinancialYear && row.IsDeleted == false
                                                     && row.ISAHQMP == isAHQMP && row.CustomerBranchId == CustomerBranchId
                                                     && row.PhaseCount == phasecount && row.VerticalID == VerticalId
                                                     select row.Id).ToList();
                        AuditorMastertoDelete.ForEach(entry =>
                        {
                            InternalAuditScheduling prevmappedids = (from row in entities.InternalAuditSchedulings
                                                                     where row.Id == entry
                                                                     select row).FirstOrDefault();
                            prevmappedids.IsDeleted = true;
                            entities.SaveChanges();
                        });
                        isdeletesucess = true;
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Audit already kickedoff,so Schedule cannot be edit ..!";
                        isdeletesucess = false;
                    }
                    return isdeletesucess;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected void grdAuditScheduling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        private void OpenScheduleInformation(string isAHQMP, string FinancialYear, int CustomerBranchId, string Termname,int Verticalid)
        {
            try
            {

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<AuditExpectedStartEndDate_Result> a = new List<AuditExpectedStartEndDate_Result>();
                    a = ProcessManagement.GetAuditExpectedStartEndDate_ResultProcedure(isAHQMP, FinancialYear, CustomerBranchId, Termname, Verticalid,-1).ToList();
                    var remindersummary = a.OrderBy(entry => entry.ProcessName).ToList();
                    grdAuditScheduleStartEndDate.DataSource = null;
                    grdAuditScheduleStartEndDate.DataBind();
                    grdAuditScheduleStartEndDate.DataSource = remindersummary;
                    grdAuditScheduleStartEndDate.DataBind();
                    upComplianceScheduleDialog.Update();
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region EDIT Record
        public List<SchedulingReport_Result> GetSchedulingReport_ResultProcedure(int branchid, int ProcessId, string FinancialYear, int verticalID)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.SchedulingReport(branchid, ProcessId, FinancialYear, verticalID).ToList();
                return complianceReminders;
            }

        }
        public void cleardatasourceEDIT()
        {
            grdAnnuallyEDIT.DataSource = null;
            grdAnnuallyEDIT.DataBind();
            grdHalfYearlyEDIT.DataSource = null;
            grdHalfYearlyEDIT.DataBind();
            grdQuarterlyEDIT.DataSource = null;
            grdQuarterlyEDIT.DataBind();
            grdMonthlyEDIT.DataSource = null;
            grdMonthlyEDIT.DataBind();
            grdphase1EDIT.DataSource = null;
            grdphase1EDIT.DataBind();
            grdphase2EDIT.DataSource = null;
            grdphase2EDIT.DataBind();
            grdphase3EDIT.DataSource = null;
            grdphase3EDIT.DataBind();
            grdphase4EDIT.DataSource = null;
            grdphase4EDIT.DataBind();
            grdphase5EDIT.DataSource = null;
            grdphase5EDIT.DataBind();
        }
        public void EnableDisableEDIT(int noofphases, string Flag)
        {

            if (Flag == "A")
            {
                pnlAnnuallyEDIT.Visible = true;
                pnlHalfYearlyEDIT.Visible = false;
                pnlQuarterlyEDIT.Visible = false;
                pnlMonthlyEDIT.Visible = false;
                pnlphase1EDIT.Visible = false;
                pnlphase2EDIT.Visible = false;
                pnlphase3EDIT.Visible = false;
                pnlphase4EDIT.Visible = false;
                pnlphase5EDIT.Visible = false;

            }
            else if (Flag == "H")
            {
                pnlAnnuallyEDIT.Visible = false;
                pnlHalfYearlyEDIT.Visible = true;
                pnlQuarterlyEDIT.Visible = false;
                pnlMonthlyEDIT.Visible = false;
                pnlphase1EDIT.Visible = false;
                pnlphase2EDIT.Visible = false;
                pnlphase3EDIT.Visible = false;
                pnlphase4EDIT.Visible = false;
                pnlphase5EDIT.Visible = false;
            }
            else if (Flag == "Q")
            {
                pnlAnnuallyEDIT.Visible = false;
                pnlHalfYearlyEDIT.Visible = false;
                pnlQuarterlyEDIT.Visible = true;
                pnlMonthlyEDIT.Visible = false;
                pnlphase1EDIT.Visible = false;
                pnlphase2EDIT.Visible = false;
                pnlphase3EDIT.Visible = false;
                pnlphase4EDIT.Visible = false;
                pnlphase5EDIT.Visible = false;
            }
            else if (Flag == "M")
            {
                pnlAnnuallyEDIT.Visible = false;
                pnlHalfYearlyEDIT.Visible = false;
                pnlQuarterlyEDIT.Visible = false;
                pnlMonthlyEDIT.Visible = true;
                pnlphase1EDIT.Visible = false;
                pnlphase2EDIT.Visible = false;
                pnlphase3EDIT.Visible = false;
                pnlphase4EDIT.Visible = false;
                pnlphase5EDIT.Visible = false;
            }
            else if (Flag == "P")
            {
                if (noofphases == 1)
                {
                    pnlAnnuallyEDIT.Visible = false;
                    pnlHalfYearlyEDIT.Visible = false;
                    pnlQuarterlyEDIT.Visible = false;
                    pnlMonthlyEDIT.Visible = false;
                    pnlphase1EDIT.Visible = true;
                    pnlphase2EDIT.Visible = false;
                    pnlphase3EDIT.Visible = false;
                    pnlphase4EDIT.Visible = false;
                }
                else if (noofphases == 2)
                {
                    pnlAnnuallyEDIT.Visible = false;
                    pnlHalfYearlyEDIT.Visible = false;
                    pnlQuarterlyEDIT.Visible = false;
                    pnlMonthlyEDIT.Visible = false;
                    pnlphase1EDIT.Visible = false;
                    pnlphase2EDIT.Visible = true;
                    pnlphase3EDIT.Visible = false;
                    pnlphase4EDIT.Visible = false;
                    pnlphase5EDIT.Visible = false;
                }
                else if (noofphases == 3)
                {
                    pnlAnnuallyEDIT.Visible = false;
                    pnlHalfYearlyEDIT.Visible = false;
                    pnlQuarterlyEDIT.Visible = false;
                    pnlMonthlyEDIT.Visible = false;
                    pnlphase1EDIT.Visible = false;
                    pnlphase2EDIT.Visible = false;
                    pnlphase3EDIT.Visible = true;
                    pnlphase4EDIT.Visible = false;
                    pnlphase5EDIT.Visible = false;
                }
                else if (noofphases == 4)
                {
                    pnlAnnuallyEDIT.Visible = false;
                    pnlHalfYearlyEDIT.Visible = false;
                    pnlQuarterlyEDIT.Visible = false;
                    pnlMonthlyEDIT.Visible = false;
                    pnlphase1EDIT.Visible = false;
                    pnlphase2EDIT.Visible = false;
                    pnlphase3EDIT.Visible = false;
                    pnlphase4EDIT.Visible = true;
                    pnlphase5EDIT.Visible = false;
                }
                else if (noofphases == 5)
                {
                    pnlAnnuallyEDIT.Visible = false;
                    pnlHalfYearlyEDIT.Visible = false;
                    pnlQuarterlyEDIT.Visible = false;
                    pnlMonthlyEDIT.Visible = false;
                    pnlphase1EDIT.Visible = false;
                    pnlphase2EDIT.Visible = false;
                    pnlphase3EDIT.Visible = false;
                    pnlphase4EDIT.Visible = false;
                    pnlphase5EDIT.Visible = true;
                }
            }
        }
        public static List<SP_AnnualyReport_Result> GetSPAnnualyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_AnnualyReport(Branchid, Financialyear, Verticalid, SchedulingType,-1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_HalfYearlyReport_Result> GetSPHalfYearlyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_HalfYearlyReport(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_QuarterlyReport_Result> GetSPQuarterwiseDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_QuarterlyReport(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_MonthlyReport_Result> GetSPMonthlyDisplay(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_MonthlyReport(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase1Report_Result> GetSP_Phase1Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase1Report(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase2Report_Result> GetSP_Phase2Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase2Report(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase3Report_Result> GetSP_Phase3Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase3Report(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase4Report_Result> GetSP_Phase4Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase4Report(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public static List<SP_Phase5Report_Result> GetSP_Phase5Display(int Branchid, string Financialyear, int Verticalid, string SchedulingType)
        {
            //date = date.Date;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditdsplay = entities.SP_Phase5Report(Branchid, Financialyear, Verticalid, SchedulingType, -1).ToList();
                return auditdsplay;
            }
        }
        public void BindAuditScheduleEDIT(string flag, int count, int Branchid,int Verticalid)
        {
            try
            {               
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITFinancialYear"])))
                {
                    string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                    if (flag == "A")
                    {
                        #region Annualy
                        cleardatasourceEDIT();

                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_AnnualyReport_Result> r = new List<SP_AnnualyReport_Result>();
                        r = GetSPAnnualyDisplay(Branchid, f1, Verticalid,"A");

                        if (r.Count > 0)
                            r = r.OrderBy(entry => entry.Name).ToList();

                        grdAnnuallyEDIT.DataSource = r; /*GetSPAnnualyDisplay(Branchid, "", Verticalid);*/
                        grdAnnuallyEDIT.DataBind();                        

                        DataTable dt = new DataTable();
                        dt = (grdAnnuallyEDIT.DataSource as List<SP_AnnualyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Annualy1");
                        //dt1.Columns.Add("Annualy2");
                        //dt1.Columns.Add("Annualy3");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "";
                        dr1["Annualy1"] = f1;
                        //dr1["Annualy2"] = f2;
                        //dr1["Annualy3"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdAnnuallyEDIT.DataSource = dt1;
                        grdAnnuallyEDIT.DataBind();

                        #endregion
                    }
                    else if (flag == "H")
                    {
                        #region Half Yearly
                        cleardatasourceEDIT();
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_HalfYearlyReport_Result> r = new List<SP_HalfYearlyReport_Result>();
                        r = GetSPHalfYearlyDisplay(Branchid, f1, Verticalid, "H");

                        if (r.Count > 0)
                            r = r.OrderBy(entry => entry.Name).ToList();

                        grdHalfYearlyEDIT.DataSource = r; /* GetSPHalfYearlyDisplay(Branchid, "", Verticalid);*/
                        grdHalfYearlyEDIT.DataBind();
                       
                        DataTable dt = new DataTable();
                        dt = (grdHalfYearlyEDIT.DataSource as List<SP_HalfYearlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Halfyearly1");
                        dt1.Columns.Add("Halfyearly2");
                        //dt1.Columns.Add("Halfyearly3");
                        //dt1.Columns.Add("Halfyearly4");
                        //dt1.Columns.Add("Halfyearly5");
                        //dt1.Columns.Add("Halfyearly6");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Halfyearly1"] = "";
                        dr1["Halfyearly2"] = f1;
                        //dr1["Halfyearly3"] = "";
                        //dr1["Halfyearly4"] = f2;
                        //dr1["Halfyearly5"] = "";
                        //dr1["Halfyearly6"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdHalfYearlyEDIT.DataSource = dt1;
                        grdHalfYearlyEDIT.DataBind();

                        #endregion
                    }
                    else if (flag == "Q")
                    {
                        #region Quarterly
                        cleardatasourceEDIT();

                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_QuarterlyReport_Result> r = new List<SP_QuarterlyReport_Result>();
                        r = GetSPQuarterwiseDisplay(Branchid, f1, Verticalid, "Q");

                        if (r.Count > 0)
                            r = r.OrderBy(entry => entry.Name).ToList();

                        grdQuarterlyEDIT.DataSource = r; /*GetSPQuarterwiseDisplay(Branchid, "", Verticalid);*/
                        grdQuarterlyEDIT.DataBind();
                       
                        DataTable dt = new DataTable();
                        dt = (grdQuarterlyEDIT.DataSource as List<SP_QuarterlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Quarter1");
                        dt1.Columns.Add("Quarter2");
                        dt1.Columns.Add("Quarter3");
                        dt1.Columns.Add("Quarter4");
                        //dt1.Columns.Add("Quarter5");
                        //dt1.Columns.Add("Quarter6");
                        //dt1.Columns.Add("Quarter7");
                        //dt1.Columns.Add("Quarter8");
                        //dt1.Columns.Add("Quarter9");
                        //dt1.Columns.Add("Quarter10");
                        //dt1.Columns.Add("Quarter11");
                        //dt1.Columns.Add("Quarter12");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Quarter1"] = "";
                        dr1["Quarter2"] = "";
                        dr1["Quarter3"] = "";
                        dr1["Quarter4"] = f1;
                        //dr1["Quarter5"] = "";
                        //dr1["Quarter6"] = "";
                        //dr1["Quarter7"] = "";
                        //dr1["Quarter8"] = f2;
                        //dr1["Quarter9"] = "";
                        //dr1["Quarter10"] = "";
                        //dr1["Quarter11"] = "";
                        //dr1["Quarter12"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);

                        grdQuarterlyEDIT.DataSource = dt1;
                        grdQuarterlyEDIT.DataBind();

                        #endregion
                    }
                    else if (flag == "M")
                    {
                        #region Monthly
                        cleardatasourceEDIT();
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                        //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                        //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                        List<SP_MonthlyReport_Result> r = new List<SP_MonthlyReport_Result>();
                        r = GetSPMonthlyDisplay(Branchid, f1, Verticalid, "M");

                        if (r.Count > 0)
                            r = r.OrderBy(entry => entry.Name).ToList();

                        grdMonthlyEDIT.DataSource = r;/*GetSPMonthlyDisplay(Branchid, "", Verticalid);*/
                        grdMonthlyEDIT.DataBind();
                        
                        DataTable dt = new DataTable();
                        dt = (grdMonthlyEDIT.DataSource as List<SP_MonthlyReport_Result>).ToDataTable();
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        DataRow dr1 = null;

                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("Monthly1");
                        dt1.Columns.Add("Monthly2");
                        dt1.Columns.Add("Monthly3");
                        dt1.Columns.Add("Monthly4");
                        dt1.Columns.Add("Monthly5");
                        dt1.Columns.Add("Monthly6");
                        dt1.Columns.Add("Monthly7");
                        dt1.Columns.Add("Monthly8");
                        dt1.Columns.Add("Monthly9");
                        dt1.Columns.Add("Monthly10");
                        dt1.Columns.Add("Monthly11");
                        dt1.Columns.Add("Monthly12");
                        //dt1.Columns.Add("Monthly13");
                        //dt1.Columns.Add("Monthly14");
                        //dt1.Columns.Add("Monthly15");
                        //dt1.Columns.Add("Monthly16");
                        //dt1.Columns.Add("Monthly17");
                        //dt1.Columns.Add("Monthly18");
                        //dt1.Columns.Add("Monthly19");
                        //dt1.Columns.Add("Monthly20");
                        //dt1.Columns.Add("Monthly21");
                        //dt1.Columns.Add("Monthly22");
                        //dt1.Columns.Add("Monthly23");
                        //dt1.Columns.Add("Monthly24");
                        //dt1.Columns.Add("Monthly25");
                        //dt1.Columns.Add("Monthly26");
                        //dt1.Columns.Add("Monthly27");
                        //dt1.Columns.Add("Monthly28");
                        //dt1.Columns.Add("Monthly29");
                        //dt1.Columns.Add("Monthly30");
                        //dt1.Columns.Add("Monthly31");
                        //dt1.Columns.Add("Monthly32");
                        //dt1.Columns.Add("Monthly33");
                        //dt1.Columns.Add("Monthly34");
                        //dt1.Columns.Add("Monthly35");
                        //dt1.Columns.Add("Monthly36");
                        dt1.Columns.Add("ID", typeof(long));
                        dr1 = dt1.NewRow();

                        dr1["Name"] = "Process";
                        dr1["Monthly1"] = "";
                        dr1["Monthly2"] = "";
                        dr1["Monthly3"] = "";
                        dr1["Monthly4"] = "";
                        dr1["Monthly5"] = "";
                        dr1["Monthly6"] = "";
                        dr1["Monthly7"] = "";
                        dr1["Monthly8"] = "";
                        dr1["Monthly9"] = "";
                        dr1["Monthly10"] = "";
                        dr1["Monthly11"] = "";
                        dr1["Monthly12"] = f1;
                        //dr1["Monthly13"] = "";
                        //dr1["Monthly14"] = "";
                        //dr1["Monthly15"] = "";
                        //dr1["Monthly16"] = "";
                        //dr1["Monthly17"] = "";
                        //dr1["Monthly18"] = "";
                        //dr1["Monthly19"] = "";
                        //dr1["Monthly20"] = "";
                        //dr1["Monthly21"] = "";
                        //dr1["Monthly22"] = "";
                        //dr1["Monthly23"] = "";
                        //dr1["Monthly24"] = f2;
                        //dr1["Monthly25"] = "";
                        //dr1["Monthly26"] = "";
                        //dr1["Monthly27"] = "";
                        //dr1["Monthly28"] = "";
                        //dr1["Monthly29"] = "";
                        //dr1["Monthly30"] = "";
                        //dr1["Monthly31"] = "";
                        //dr1["Monthly32"] = "";
                        //dr1["Monthly33"] = "";
                        //dr1["Monthly34"] = "";
                        //dr1["Monthly35"] = "";
                        //dr1["Monthly36"] = f3;
                        dr1["ID"] = 0;
                        dt1.Rows.Add(dr1);
                        dt1.Merge(dt);
                        grdMonthlyEDIT.DataSource = dt1;
                        grdMonthlyEDIT.DataBind();

                        #endregion
                    }
                    else
                    {
                        if (count == 1)
                        {
                            #region Phase1
                            cleardatasourceEDIT();
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase1Report_Result> r = new List<SP_Phase1Report_Result>();
                            r = GetSP_Phase1Display(Branchid, f1, Verticalid, "P");

                            if (r.Count > 0)
                                r = r.OrderBy(entry => entry.Name).ToList();

                            grdphase1EDIT.DataSource = r; /*GetSP_Phase1Display(Branchid, "", Verticalid);*/
                            grdphase1EDIT.DataBind();
                            
                            DataTable dt = new DataTable();
                            dt = (grdphase1EDIT.DataSource as List<SP_Phase1Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            //dt1.Columns.Add("Phase2");
                            //dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = f1;
                            //dr1["Phase2"] = f2;
                            //dr1["Phase3"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase1EDIT.DataSource = dt1;
                            grdphase1EDIT.DataBind();

                            #endregion
                        }
                        else if (count == 2)
                        {
                            #region Phase2
                            cleardatasourceEDIT();
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase2Report_Result> r = new List<SP_Phase2Report_Result>();
                            r = GetSP_Phase2Display(Branchid, f1, Verticalid, "P");

                            if (r.Count > 0)
                                r = r.OrderBy(entry => entry.Name).ToList();

                            grdphase2EDIT.DataSource = r; /*GetSP_Phase2Display(Branchid, "", Verticalid);*/
                            grdphase2EDIT.DataBind();
                           
                            DataTable dt = new DataTable();
                            dt = (grdphase2EDIT.DataSource as List<SP_Phase2Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            //dt1.Columns.Add("Phase3");
                            //dt1.Columns.Add("Phase4");
                            //dt1.Columns.Add("Phase5");
                            //dt1.Columns.Add("Phase6");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = f1;
                            //dr1["Phase3"] = "";
                            //dr1["Phase4"] = f2;
                            //dr1["Phase5"] = "";
                            //dr1["Phase6"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase2EDIT.DataSource = dt1;
                            grdphase2EDIT.DataBind();

                            #endregion
                        }
                        else if (count == 3)
                        {
                            #region Phase3
                            cleardatasourceEDIT();
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase3Report_Result> r = new List<SP_Phase3Report_Result>();
                            r = GetSP_Phase3Display(Branchid, f1, Verticalid, "P");

                            if (r.Count > 0)
                                r = r.OrderBy(entry => entry.Name).ToList();

                            grdphase3EDIT.DataSource = r;/*GetSP_Phase3Display(Branchid, "", Verticalid);*/
                            grdphase3EDIT.DataBind();
                            
                            DataTable dt = new DataTable();
                            dt = (grdphase3EDIT.DataSource as List<SP_Phase3Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            //dt1.Columns.Add("Phase4");
                            //dt1.Columns.Add("Phase5");
                            //dt1.Columns.Add("Phase6");
                            //dt1.Columns.Add("Phase7");
                            //dt1.Columns.Add("Phase8");
                            //dt1.Columns.Add("Phase9");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = f1;
                            //dr1["Phase4"] = "";
                            //dr1["Phase5"] = "";
                            //dr1["Phase6"] = f2;
                            //dr1["Phase7"] = "";
                            //dr1["Phase8"] = "";
                            //dr1["Phase9"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase3EDIT.DataSource = dt1;
                            grdphase3EDIT.DataBind();

                            #endregion
                        }
                        else if (count == 4)
                        {
                            #region Phase4
                            cleardatasourceEDIT();
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase4Report_Result> r = new List<SP_Phase4Report_Result>();
                            r = GetSP_Phase4Display(Branchid, f1, Verticalid, "P");

                            if (r.Count > 0)
                                r = r.OrderBy(entry => entry.Name).ToList();

                            grdphase4EDIT.DataSource = r; /*GetSP_Phase4Display(Branchid, "", Verticalid);*/
                            grdphase4EDIT.DataBind();
                            
                            DataTable dt = new DataTable();
                            dt = (grdphase4EDIT.DataSource as List<SP_Phase4Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            //dt1.Columns.Add("Phase5");
                            //dt1.Columns.Add("Phase6");
                            //dt1.Columns.Add("Phase7");
                            //dt1.Columns.Add("Phase8");
                            //dt1.Columns.Add("Phase9");
                            //dt1.Columns.Add("Phase10");
                            //dt1.Columns.Add("Phase11");
                            //dt1.Columns.Add("Phase12");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = f1;
                            //dr1["Phase5"] = "";
                            //dr1["Phase6"] = "";
                            //dr1["Phase7"] = "";
                            //dr1["Phase8"] = f2;
                            //dr1["Phase9"] = "";
                            //dr1["Phase10"] = "";
                            //dr1["Phase11"] = "";
                            //dr1["Phase12"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase4EDIT.DataSource = dt1;
                            grdphase4EDIT.DataBind();

                            #endregion
                        }
                        else
                        {
                            #region Phase5
                            cleardatasourceEDIT();
                            string[] a = financialyear.Split('-');
                            string aaa = a[0];
                            string bbb = a[1];
                            string f1 = aaa + "-" + bbb;
                            //string f2 = Convert.ToInt32(aaa) + 1 + "-" + (Convert.ToInt32(bbb) + 1);
                            //string f3 = (Convert.ToInt32(aaa) + 1) + 1 + "-" + ((Convert.ToInt32(bbb) + 1) + 1);

                            List<SP_Phase5Report_Result> r = new List<SP_Phase5Report_Result>();
                            r = GetSP_Phase5Display(Branchid, f1, Verticalid, "P");

                            if (r.Count > 0)
                                r = r.OrderBy(entry => entry.Name).ToList();

                            grdphase5EDIT.DataSource = r; /*GetSP_Phase5Display(Branchid, "", Verticalid);*/
                            grdphase5EDIT.DataBind();
                            
                            DataTable dt = new DataTable();
                            dt = (grdphase5EDIT.DataSource as List<SP_Phase5Report_Result>).ToDataTable();
                            DataTable dt1 = new DataTable();
                            dt1.Clear();
                            DataRow dr1 = null;
                            dt1.Columns.Add("Name");
                            dt1.Columns.Add("Phase1");
                            dt1.Columns.Add("Phase2");
                            dt1.Columns.Add("Phase3");
                            dt1.Columns.Add("Phase4");
                            dt1.Columns.Add("Phase5");
                            //dt1.Columns.Add("Phase6");
                            //dt1.Columns.Add("Phase7");
                            //dt1.Columns.Add("Phase8");
                            //dt1.Columns.Add("Phase9");
                            //dt1.Columns.Add("Phase10");
                            //dt1.Columns.Add("Phase11");
                            //dt1.Columns.Add("Phase12");
                            //dt1.Columns.Add("Phase13");
                            //dt1.Columns.Add("Phase14");
                            //dt1.Columns.Add("Phase15");
                            dt1.Columns.Add("ID", typeof(long));
                            dr1 = dt1.NewRow();

                            dr1["Name"] = "Process";
                            dr1["Phase1"] = "";
                            dr1["Phase2"] = "";
                            dr1["Phase3"] = "";
                            dr1["Phase4"] = "";
                            dr1["Phase5"] = f1;
                            //dr1["Phase6"] = "";
                            //dr1["Phase7"] = "";
                            //dr1["Phase8"] = "";
                            //dr1["Phase9"] = "";
                            //dr1["Phase10"] = f2;
                            //dr1["Phase11"] = "";
                            //dr1["Phase12"] = "";
                            //dr1["Phase13"] = "";
                            //dr1["Phase14"] = "";
                            //dr1["Phase15"] = f3;
                            dr1["ID"] = 0;
                            dt1.Rows.Add(dr1);
                            dt1.Merge(dt);

                            grdphase5EDIT.DataSource = dt1;
                            grdphase5EDIT.DataBind();

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Annually
        protected void grdAnnuallyEDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {               
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;
                       
                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkAnnualy1");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Annually")
                                        {
                                            bf.Checked = true;
                                        }
                                    }
                                }                                
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                               
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Annually";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Annually12 = new CheckBox();
                                Editchk_Annually12.Text = "Annually";
                                Editchk_Annually12.ID = "EditAnnually1_Id";
                                Editchk_Annually12.Attributes.Add("OnClick", "return Editfunc();");
                                cell1.Controls.Add(Editchk_Annually12);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdAnnuallyEDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAnnually.PageIndex = e.NewPageIndex;

            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("A", 0, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }

        #endregion

        #region Haly Yearly
        protected void grdHalfYearlyEDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkHalfyearly1");
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkHalfyearly2");                            

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Apr-Sep")
                                        {
                                            bf.Checked = true;
                                        }
                                        else if (termName == "Oct-Mar")
                                        {
                                            bf1.Checked = true;
                                        }
                                    }
                                }                               
                               
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;                                

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr-Sep";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Half1 = new CheckBox();
                                Editchk_Half1.Text = "Apr-Sep";
                                Editchk_Half1.ID = "EditHalf1_Id";
                                Editchk_Half1.Attributes.Add("OnClick", "return funHalfEdit1();");
                                cell1.Controls.Add(Editchk_Half1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Oct-Mar";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Half2 = new CheckBox();
                                Editchk_Half2.Text = "Oct-Mar";
                                Editchk_Half2.ID = "EditHalf2_Id";
                                Editchk_Half2.Attributes.Add("OnClick", "return funHalfEdit2();");
                                cell2.Controls.Add(Editchk_Half2);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }

        }
        protected void grdHalfYearlyEDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHalfYearly.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("H", 0, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }

        #endregion

        #region Quarterly
        protected void grdQuarterlyEDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {                
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] !=null)
                {
                    if (ViewState["EDITVerticalId"] !=null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                        

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;

                            TableCell otherCell4 = e.Row.Cells[4];
                            otherCell4.Visible = false;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;
                        }
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            //first financial Year
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkQuarter1");
                            CheckBox bf2 = (CheckBox) e.Row.FindControl("chkQuarter2");
                            CheckBox bf3 = (CheckBox) e.Row.FindControl("chkQuarter3");
                            CheckBox bf4 = (CheckBox) e.Row.FindControl("chkQuarter4");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Apr-Jun")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Jul-Sep")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Oct-Dec")
                                        {
                                            bf3.Checked = true;
                                        }
                                        else if (termName == "Jan-Mar")
                                        {
                                            bf4.Checked = true;
                                        }
                                    }
                                }                                
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;                               

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr-Jun";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox EditEditchk_Quarterly1 = new CheckBox();
                                EditEditchk_Quarterly1.Text = "Apr-Jun";
                                EditEditchk_Quarterly1.ID = "EditQuarterly1_Id";
                                EditEditchk_Quarterly1.Attributes.Add("OnClick", "return EditfuncQuarterly1();");
                                cell1.Controls.Add(EditEditchk_Quarterly1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Jul-Sep";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox EditEditchk_Quarterly2 = new CheckBox();
                                EditEditchk_Quarterly2.Text = "Jul-Sep";
                                EditEditchk_Quarterly2.ID = "EditQuarterly2_Id";
                                EditEditchk_Quarterly2.Attributes.Add("OnClick", "return EditfuncQuarterly2();");
                                cell2.Controls.Add(EditEditchk_Quarterly2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Oct-Dec";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Quarterly3 = new CheckBox();
                                Editchk_Quarterly3.Text = "Oct-Dec";
                                Editchk_Quarterly3.ID = "EditQuarterly3_Id";
                                Editchk_Quarterly3.Attributes.Add("OnClick", "return EditfuncQuarterly3();");
                                cell3.Controls.Add(Editchk_Quarterly3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Jan-Mar";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_Quarterly4 = new CheckBox();
                                Editchk_Quarterly4.Text = "Jan-Mar";
                                Editchk_Quarterly4.ID = "EditQuarterly4_Id";
                                Editchk_Quarterly4.Attributes.Add("OnClick", "return EditfuncQuarterly4();");
                                cell4.Controls.Add(Editchk_Quarterly4);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdQuarterlyEDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdQuarterly.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("Q", 0, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }
        #endregion

        #region Monthly
        protected void grdMonthlyEDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {               
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                       

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 12;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;

                            TableCell otherCell4 = e.Row.Cells[4];
                            otherCell4.Visible = false;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;

                            TableCell otherCell7 = e.Row.Cells[7];
                            otherCell7.Visible = false;

                            TableCell otherCell8 = e.Row.Cells[8];
                            otherCell8.Visible = false;

                            TableCell otherCell9 = e.Row.Cells[9];
                            otherCell9.Visible = false;

                            TableCell otherCell10 = e.Row.Cells[10];
                            otherCell10.Visible = false;

                            TableCell otherCell11 = e.Row.Cells[11];
                            otherCell11.Visible = false;

                            TableCell otherCell12 = e.Row.Cells[12];
                            otherCell12.Visible = false;

                            TableCell otherCell13 = e.Row.Cells[13];
                            otherCell13.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            e.Row.Cells[0].CssClass = "locked";
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            //first financial Year
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkMonthly1");
                            CheckBox bf2 = (CheckBox) e.Row.FindControl("chkMonthly2");
                            CheckBox bf3 = (CheckBox) e.Row.FindControl("chkMonthly3");
                            CheckBox bf4 = (CheckBox) e.Row.FindControl("chkMonthly4");
                            CheckBox bf5 = (CheckBox) e.Row.FindControl("chkMonthly5");
                            CheckBox bf6 = (CheckBox) e.Row.FindControl("chkMonthly6");
                            CheckBox bf7 = (CheckBox) e.Row.FindControl("chkMonthly7");
                            CheckBox bf8 = (CheckBox) e.Row.FindControl("chkMonthly8");
                            CheckBox bf9 = (CheckBox) e.Row.FindControl("chkMonthly9");
                            CheckBox bf10 = (CheckBox) e.Row.FindControl("chkMonthly10");
                            CheckBox bf11 = (CheckBox) e.Row.FindControl("chkMonthly11");
                            CheckBox bf12 = (CheckBox) e.Row.FindControl("chkMonthly12");                           
                          
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Apr")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "May")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Jun")
                                        {
                                            bf3.Checked = true;
                                        }
                                        else if (termName == "Jul")
                                        {
                                            bf4.Checked = true;
                                        }
                                        else if (termName == "Aug")
                                        {
                                            bf5.Checked = true;
                                        }
                                        else if (termName == "Sep")
                                        {
                                            bf6.Checked = true;
                                        }
                                        else if (termName == "Oct")
                                        {
                                            bf7.Checked = true;
                                        }
                                        else if (termName == "Nov")
                                        {
                                            bf8.Checked = true;
                                        }
                                        else if (termName == "Dec")
                                        {
                                            bf9.Checked = true;
                                        }
                                        else if (termName == "Jan")
                                        {
                                            bf10.Checked = true;
                                        }
                                        else if (termName == "Feb")
                                        {
                                            bf11.Checked = true;
                                        }
                                        else if (termName == "Mar")
                                        {
                                            bf12.Checked = true;
                                        }
                                    }
                                }  
                            }
                            if (lblProcessID.Text == "0")
                            {
                                //first financial Year
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                                bf5.Visible = false;
                                bf6.Visible = false;
                                bf7.Visible = false;
                                bf8.Visible = false;
                                bf9.Visible = false;
                                bf10.Visible = false;
                                bf11.Visible = false;
                                bf12.Visible = false;                               

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Apr";
                                cell1.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox EditEditchk_grdMonthly1 = new CheckBox();
                                EditEditchk_grdMonthly1.Text = "Apr";
                                EditEditchk_grdMonthly1.ID = "EditgrdMonthly1_Id";
                                EditEditchk_grdMonthly1.Attributes.Add("OnClick", "return funMonthlyEdit1();");
                                cell1.Controls.Add(EditEditchk_grdMonthly1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "May";
                                cell2.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox EditEditchk_grdMonthly2 = new CheckBox();
                                EditEditchk_grdMonthly2.Text = "May";
                                EditEditchk_grdMonthly2.ID = "EditgrdMonthly2_Id";
                                EditEditchk_grdMonthly2.Attributes.Add("OnClick", "return funMonthlyEdit2();");
                                cell2.Controls.Add(EditEditchk_grdMonthly2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Jun";
                                cell3.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_grdMonthly3 = new CheckBox();
                                Editchk_grdMonthly3.Text = "Jun";
                                Editchk_grdMonthly3.ID = "EditgrdMonthly3_Id";
                                Editchk_grdMonthly3.Attributes.Add("OnClick", "return funMonthlyEdit3();");
                                cell3.Controls.Add(Editchk_grdMonthly3);


                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Jul";
                                cell4.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly4 = new CheckBox();
                                Editchk_grdMonthly4.Text = "Jul";
                                Editchk_grdMonthly4.ID = "EditgrdMonthly4_Id";
                                Editchk_grdMonthly4.Attributes.Add("OnClick", "return funMonthlyEdit4();");
                                cell4.Controls.Add(Editchk_grdMonthly4);


                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Aug";
                                cell5.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly5 = new CheckBox();
                                Editchk_grdMonthly5.Text = "Aug";
                                Editchk_grdMonthly5.ID = "EditgrdMonthly5_Id";
                                Editchk_grdMonthly5.Attributes.Add("OnClick", "return funMonthlyEdit5();");
                                cell5.Controls.Add(Editchk_grdMonthly5);


                                TableCell cell6 = e.Row.Cells[7];
                                cell6.ColumnSpan = 1;
                                //cell6.Text = "Sep";
                                cell6.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell6.BorderColor = Color.White;
                                cell6.ForeColor = Color.White;
                                cell6.Font.Bold = true;
                                cell6.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly6 = new CheckBox();
                                Editchk_grdMonthly6.Text = "Sep";
                                Editchk_grdMonthly6.ID = "EditgrdMonthly6_Id";
                                Editchk_grdMonthly6.Attributes.Add("OnClick", "return funMonthlyEdit6();");
                                cell6.Controls.Add(Editchk_grdMonthly6);

                                TableCell cell7 = e.Row.Cells[8];
                                cell7.ColumnSpan = 1;
                                //cell7.Text = "Oct";
                                cell7.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell7.BorderColor = Color.White;
                                cell7.ForeColor = Color.White;
                                cell7.Font.Bold = true;
                                cell7.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly7 = new CheckBox();
                                Editchk_grdMonthly7.Text = "Oct";
                                Editchk_grdMonthly7.ID = "EditgrdMonthly7_Id";
                                Editchk_grdMonthly7.Attributes.Add("OnClick", "return funMonthlyEdit7();");
                                cell7.Controls.Add(Editchk_grdMonthly7);

                                TableCell cell8 = e.Row.Cells[9];
                                cell8.ColumnSpan = 1;
                                //cell8.Text = "Nov";
                                cell8.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell8.BorderColor = Color.White;
                                cell8.ForeColor = Color.White;
                                cell8.Font.Bold = true;
                                cell8.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly8 = new CheckBox();
                                Editchk_grdMonthly8.Text = "Nov";
                                Editchk_grdMonthly8.ID = "EditgrdMonthly8_Id";
                                Editchk_grdMonthly8.Attributes.Add("OnClick", "return funMonthlyEdit8();");
                                cell8.Controls.Add(Editchk_grdMonthly8);

                                TableCell cell9 = e.Row.Cells[10];
                                cell9.ColumnSpan = 1;
                                //cell9.Text = "Dec";
                                cell9.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell9.BorderColor = Color.White;
                                cell9.ForeColor = Color.White;
                                cell9.Font.Bold = true;
                                cell9.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly9 = new CheckBox();
                                Editchk_grdMonthly9.Text = "Dec";
                                Editchk_grdMonthly9.ID = "EditgrdMonthly9_Id";
                                Editchk_grdMonthly9.Attributes.Add("OnClick", "return funMonthlyEdit9();");
                                cell9.Controls.Add(Editchk_grdMonthly9);

                                TableCell cell10 = e.Row.Cells[11];
                                cell10.ColumnSpan = 1;
                                //cell10.Text = "Jan";
                                cell10.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell10.BorderColor = Color.White;
                                cell10.ForeColor = Color.White;
                                cell10.Font.Bold = true;
                                cell10.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly10 = new CheckBox();
                                Editchk_grdMonthly10.Text = "Jan";
                                Editchk_grdMonthly10.ID = "EditgrdMonthly10_Id";
                                Editchk_grdMonthly10.Attributes.Add("OnClick", "return funMonthlyEdit10();");
                                cell10.Controls.Add(Editchk_grdMonthly10);

                                TableCell cell11 = e.Row.Cells[12];
                                cell11.ColumnSpan = 1;
                                //cell11.Text = "Feb";
                                cell11.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell11.BorderColor = Color.White;
                                cell11.ForeColor = Color.White;
                                cell11.Font.Bold = true;
                                cell11.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly11 = new CheckBox();
                                Editchk_grdMonthly11.Text = "Feb";
                                Editchk_grdMonthly11.ID = "EditgrdMonthly11_Id";
                                Editchk_grdMonthly11.Attributes.Add("OnClick", "return funMonthlyEdit11();");
                                cell11.Controls.Add(Editchk_grdMonthly11);

                                TableCell cell12 = e.Row.Cells[13];
                                cell12.ColumnSpan = 1;
                                //cell12.Text = "Mar";
                                cell12.BackColor = System.Drawing.Color.FromArgb(66, 151, 215);
                                cell12.BorderColor = Color.White;
                                cell12.ForeColor = Color.White;
                                cell12.Font.Bold = true;
                                cell12.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_grdMonthly12 = new CheckBox();
                                Editchk_grdMonthly12.Text = "Mar";
                                Editchk_grdMonthly12.ID = "EditgrdMonthly12_Id";
                                Editchk_grdMonthly12.Attributes.Add("OnClick", "return funMonthlyEdit12();");
                                cell12.Controls.Add(Editchk_grdMonthly12);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdMonthlyEDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMonthly.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("M", 0, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }

        #endregion

        #region Phase1
        protected void grdphase1EDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {               
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                       

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 1;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                    }
                                }  
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase1 = new CheckBox();
                                Editchk_Phase1.Text = "Phase1";
                                Editchk_Phase1.ID = "EditPhase1_Phase1_Id";
                                Editchk_Phase1.Attributes.Add("OnClick", "return Editfuncphase1();");
                                cell1.Controls.Add(Editchk_Phase1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdphase1EDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase1.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("P", 1, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }

        #endregion

        #region Phase 2
        protected void grdphase2EDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {              
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                       

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 2;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                        else if (termName == "Phase2")
                                        {
                                            bf1.Checked = true;
                                        }
                                    }
                                }
                            }

                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;                                

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase2_Phase1 = new CheckBox();
                                Editchk_Phase2_Phase1.Text = "Phase1";
                                Editchk_Phase2_Phase1.ID = "EditPhase2_Phase1_Id";
                                Editchk_Phase2_Phase1.Attributes.Add("OnClick", "return Editfuncphase21();");
                                cell1.Controls.Add(Editchk_Phase2_Phase1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase2_Phase2 = new CheckBox();
                                Editchk_Phase2_Phase2.Text = "Phase2";
                                Editchk_Phase2_Phase2.ID = "EditPhase2_Phase2_Id";
                                Editchk_Phase2_Phase2.Attributes.Add("OnClick", "return Editfuncphase22();");
                                cell2.Controls.Add(Editchk_Phase2_Phase2);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdphase2EDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase2.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("P", 2, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }

        #endregion

        #region Phase 3
        protected void grdphase3EDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                        

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 3;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;

                            TableCell otherCell4 = e.Row.Cells[4];
                            otherCell4.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                        else if (termName == "Phase2")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Phase3")
                                        {
                                            bf2.Checked = true;
                                        }
                                    }
                                }                               
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false; 

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase3_Phase1 = new CheckBox();
                                Editchk_Phase3_Phase1.Text = "Phase1";
                                Editchk_Phase3_Phase1.ID = "EditPhase3_Phase1_Id";
                                Editchk_Phase3_Phase1.Attributes.Add("OnClick", "return Editfuncphase31();");
                                cell1.Controls.Add(Editchk_Phase3_Phase1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase3_Phase2 = new CheckBox();
                                Editchk_Phase3_Phase2.Text = "Phase2";
                                Editchk_Phase3_Phase2.ID = "EditPhase3_Phase2_Id";
                                Editchk_Phase3_Phase2.Attributes.Add("OnClick", "return Editfuncphase32();");
                                cell2.Controls.Add(Editchk_Phase3_Phase2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase3_Phase3 = new CheckBox();
                                Editchk_Phase3_Phase3.Text = "Phase3";
                                Editchk_Phase3_Phase3.ID = "EditPhase3_Phase3_Id";
                                Editchk_Phase3_Phase3.Attributes.Add("OnClick", "return Editfuncphase33();");
                                cell3.Controls.Add(Editchk_Phase3_Phase3);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdphase3EDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase3.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("P", 3, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }
        #endregion

        #region Phase 4
        protected void grdphase4EDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {               
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                       

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 4;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;

                            TableCell otherCell4 = e.Row.Cells[4];
                            otherCell4.Visible = false;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");                           

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                        else if (termName == "Phase2")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Phase3")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Phase4")
                                        {
                                            bf3.Checked = true;
                                        }
                                    }
                                }
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;

                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase4_Phase1 = new CheckBox();
                                Editchk_Phase4_Phase1.Text = "Phase1";
                                Editchk_Phase4_Phase1.ID = "EditPhase4_Phase1_Id";
                                Editchk_Phase4_Phase1.Attributes.Add("OnClick", "return Editfuncphase41Eidt();");
                                cell1.Controls.Add(Editchk_Phase4_Phase1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase4_Phase2 = new CheckBox();
                                Editchk_Phase4_Phase2.Text = "Phase2";
                                Editchk_Phase4_Phase2.ID = "EditPhase4_Phase2_Id";
                                Editchk_Phase4_Phase2.Attributes.Add("OnClick", "return Editfuncphase42Edit();");
                                cell2.Controls.Add(Editchk_Phase4_Phase2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase4_Phase3 = new CheckBox();
                                Editchk_Phase4_Phase3.Text = "Phase3";
                                Editchk_Phase4_Phase3.ID = "EditPhase4_Phase3_Id";
                                Editchk_Phase4_Phase3.Attributes.Add("OnClick", "return Editfuncphase43();");
                                cell3.Controls.Add(Editchk_Phase4_Phase3);

                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase4";
                                cell4.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_Phase4_Phase4 = new CheckBox();
                                Editchk_Phase4_Phase4.Text = "Phase4";
                                Editchk_Phase4_Phase4.ID = "EditPhase4_Phase4_Id";
                                Editchk_Phase4_Phase4.Attributes.Add("OnClick", "return Editfuncphase44Edit();");
                                cell4.Controls.Add(Editchk_Phase4_Phase4);                              
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdphase4EDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase4.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("P", 4, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }
        #endregion

        #region Phase 5
        protected void grdphase5EDIT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {                
                GridViewRow gvRow = e.Row;

                if (ViewState["EDITFinancialYear"] != null)
                {
                    if (ViewState["EDITVerticalId"] != null)
                    {
                        string financialyear = Convert.ToString(ViewState["EDITFinancialYear"]);
                        string[] a = financialyear.Split('-');
                        string aaa = a[0];
                        string bbb = a[1];
                        string f1 = aaa + "-" + bbb;                      

                        if (gvRow.RowType == DataControlRowType.Header)
                        {
                            TableCell cell = e.Row.Cells[1];
                            cell.ColumnSpan = 1;
                            cell.Text = "Process";
                            cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell.BorderColor = Color.White;
                            cell.Font.Bold = true;
                            cell.ForeColor = Color.White;
                            cell.HorizontalAlign = HorizontalAlign.Left;

                            TableCell cell1 = e.Row.Cells[2];
                            cell1.ColumnSpan = 5;
                            cell1.Text = f1;
                            cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                            cell1.BorderColor = Color.White;
                            cell1.Font.Bold = true;
                            cell1.ForeColor = Color.White;
                            cell1.HorizontalAlign = HorizontalAlign.Left;

                            TableCell otherCell3 = e.Row.Cells[3];
                            otherCell3.Visible = false;

                            TableCell otherCell4 = e.Row.Cells[4];
                            otherCell4.Visible = false;

                            TableCell otherCell5 = e.Row.Cells[5];
                            otherCell5.Visible = false;

                            TableCell otherCell6 = e.Row.Cells[6];
                            otherCell6.Visible = false;
                        }

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            Label lblProcessID = e.Row.FindControl("lblProcessID") as Label;
                            CheckBox bf = (CheckBox) e.Row.FindControl("chkPhase1");
                            CheckBox bf1 = (CheckBox) e.Row.FindControl("chkPhase2");
                            CheckBox bf2 = (CheckBox) e.Row.FindControl("chkPhase3");
                            CheckBox bf3 = (CheckBox) e.Row.FindControl("chkPhase4");
                            CheckBox bf4 = (CheckBox) e.Row.FindControl("chkPhase5");

                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
                            {
                                List<SchedulingReport_Result> schFirstresult = new List<SchedulingReport_Result>();
                                schFirstresult = GetSchedulingReport_ResultProcedure(Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(lblProcessID.Text), f1, Convert.ToInt32(ViewState["EDITVerticalId"])).ToList();
                                var remindersummary = schFirstresult.OrderBy(entry => entry.TermName).ToList();
                                string termName = string.Empty;
                                if (remindersummary.Count > 0)
                                {
                                    foreach (var row in remindersummary)
                                    {
                                        termName = row.TermName;
                                        if (termName == "Phase1")
                                        {
                                            bf.Checked = true;
                                        }
                                        else if (termName == "Phase2")
                                        {
                                            bf1.Checked = true;
                                        }
                                        else if (termName == "Phase3")
                                        {
                                            bf2.Checked = true;
                                        }
                                        else if (termName == "Phase4")
                                        {
                                            bf3.Checked = true;
                                        }
                                        else if (termName == "Phase5")
                                        {
                                            bf4.Checked = true;
                                        }
                                    }
                                }
                            }
                            if (lblProcessID.Text == "0")
                            {
                                bf.Visible = false;
                                bf1.Visible = false;
                                bf2.Visible = false;
                                bf3.Visible = false;
                                bf4.Visible = false;
                               
                                TableCell cell = e.Row.Cells[1];
                                cell.ColumnSpan = 1;
                                cell.Text = "Process";
                                cell.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell.BorderColor = Color.White;
                                cell.ForeColor = Color.White;
                                cell.Font.Bold = true;
                                cell.HorizontalAlign = HorizontalAlign.Left;

                                TableCell cell1 = e.Row.Cells[2];
                                cell1.ColumnSpan = 1;
                                //cell1.Text = "Phase1";
                                cell1.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell1.BorderColor = Color.White;
                                cell1.ForeColor = Color.White;
                                cell1.Font.Bold = true;
                                cell1.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase5_Phase1 = new CheckBox();
                                Editchk_Phase5_Phase1.Text = "Phase1";
                                Editchk_Phase5_Phase1.ID = "EditPhase5_Phase1_Id";
                                Editchk_Phase5_Phase1.Attributes.Add("OnClick", "return Editfuncphase51();");
                                cell1.Controls.Add(Editchk_Phase5_Phase1);

                                TableCell cell2 = e.Row.Cells[3];
                                cell2.ColumnSpan = 1;
                                //cell2.Text = "Phase2";
                                cell2.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell2.BorderColor = Color.White;
                                cell2.ForeColor = Color.White;
                                cell2.Font.Bold = true;
                                cell2.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase5_Phase2 = new CheckBox();
                                Editchk_Phase5_Phase2.Text = "Phase2";
                                Editchk_Phase5_Phase2.ID = "EditPhase5_Phase2_Id";
                                Editchk_Phase5_Phase2.Attributes.Add("OnClick", "return Editfuncphase52();");
                                cell2.Controls.Add(Editchk_Phase5_Phase2);

                                TableCell cell3 = e.Row.Cells[4];
                                cell3.ColumnSpan = 1;
                                //cell3.Text = "Phase3";
                                cell3.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell3.BorderColor = Color.White;
                                cell3.ForeColor = Color.White;
                                cell3.Font.Bold = true;
                                cell3.HorizontalAlign = HorizontalAlign.Left;
                                CheckBox Editchk_Phase5_Phase3 = new CheckBox();
                                Editchk_Phase5_Phase3.Text = "Phase3";
                                Editchk_Phase5_Phase3.ID = "EditPhase5_Phase3_Id";
                                Editchk_Phase5_Phase3.Attributes.Add("OnClick", "return Editfuncphase53();");
                                cell3.Controls.Add(Editchk_Phase5_Phase3);

                                TableCell cell4 = e.Row.Cells[5];
                                cell4.ColumnSpan = 1;
                                //cell4.Text = "Phase4";
                                cell4.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell4.BorderColor = Color.White;
                                cell4.ForeColor = Color.White;
                                cell4.Font.Bold = true;
                                cell4.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_Phase5_Phase4 = new CheckBox();
                                Editchk_Phase5_Phase4.Text = "Phase4";
                                Editchk_Phase5_Phase4.ID = "EditPhase5_Phase4_Id";
                                Editchk_Phase5_Phase4.Attributes.Add("OnClick", "return Editfuncphase54();");
                                cell4.Controls.Add(Editchk_Phase5_Phase4);

                                TableCell cell5 = e.Row.Cells[6];
                                cell5.ColumnSpan = 1;
                                //cell5.Text = "Phase5";
                                cell5.BackColor = System.Drawing.Color.FromArgb(238, 238, 238);
                                cell5.BorderColor = Color.White;
                                cell5.ForeColor = Color.White;
                                cell5.Font.Bold = true;
                                cell5.HorizontalAlign = HorizontalAlign.Center;
                                CheckBox Editchk_Phase5_Phase5 = new CheckBox();
                                Editchk_Phase5_Phase5.Text = "Phase5";
                                Editchk_Phase5_Phase5.ID = "EditPhase5_Phase5_Id";
                                Editchk_Phase5_Phase5.Attributes.Add("OnClick", "return Editfuncphase55();");
                                cell5.Controls.Add(Editchk_Phase5_Phase5);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void grdphase5EDIT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdphase5.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITCustomerBranchId"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["EDITVerticalId"])))
                {
                    BindAuditScheduleEDIT("P", 5, Convert.ToInt32(ViewState["EDITCustomerBranchId"]), Convert.ToInt32(ViewState["EDITVerticalId"]));
                }
            }
        }
        #endregion

        protected void btnEDITSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblEDITFinancialyear.Text.Trim() != "")
                {
                    if (lblEDITLocationID.Text.Trim() != "")
                    {
                        if (lblEDITVerticalID.Text.Trim() != "")
                        {
                            if (lblEDITStartDate.Text.Trim() != "")
                            {
                                if (lblEDITEndDate.Text.Trim() != "")
                                {
                                    bool successrahulf1 = false;
                                    //bool successrahulf2 = false;
                                    //bool successrahulf3 = false;
                                    string financialyear = lblEDITFinancialyear.Text;
                                    string[] fsplit = financialyear.Split('-');
                                    string fyear = fsplit[0];
                                    string syear = fsplit[1];
                                    string f1 = fyear + "-" + syear;                                    

                                    #region Annually Save
                                    if (lblEDITSchedulingType.Text == "Annually")
                                    {
                                        successrahulf1 = DeleteInternalAuditScheduling("A", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf2 = DeleteInternalAuditScheduling("A", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf3 = DeleteInternalAuditScheduling("A", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        
                                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        int processid = -1;

                                        for (int i = 0; i < grdAnnuallyEDIT.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdAnnuallyEDIT.Rows[i];
                                            Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                CheckBox bf = (CheckBox) row.FindControl("chkAnnualy1");
                                                //CheckBox bf1 = (CheckBox) row.FindControl("chkAnnualy2");
                                                //CheckBox bf2 = (CheckBox) row.FindControl("chkAnnualy3");
                                                if (successrahulf1)
                                                {
                                                    if (bf.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Annually";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "A";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                }
                                                //if (successrahulf2)
                                                //{
                                                //    if (bf1.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Annually";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "A";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                                //if (successrahulf3)
                                                //{
                                                //    if (bf2.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Annually";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "A";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                            }
                                        }
                                        if (InternalauditschedulingList.Count != 0)
                                        {
                                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                        }
                                        BindAuditScheduleEDIT("A", 0, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                    }
                                    #endregion

                                    #region Half Yearly Save
                                    else if (lblEDITSchedulingType.Text == "Half Yearly")
                                    {
                                        successrahulf1 = DeleteInternalAuditScheduling("H", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf2 = DeleteInternalAuditScheduling("H", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf3 = DeleteInternalAuditScheduling("H", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        int processid = -1;
                                        for (int i = 0; i < grdHalfYearlyEDIT.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdHalfYearlyEDIT.Rows[i];
                                            Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                CheckBox bf = (CheckBox) row.FindControl("chkHalfyearly1");
                                                CheckBox bf1 = (CheckBox) row.FindControl("chkHalfyearly2");
                                                //CheckBox bf2 = (CheckBox) row.FindControl("chkHalfyearly3");
                                                //CheckBox bf3 = (CheckBox) row.FindControl("chkHalfyearly4");
                                                //CheckBox bf4 = (CheckBox) row.FindControl("chkHalfyearly5");
                                                //CheckBox bf5 = (CheckBox) row.FindControl("chkHalfyearly6");
                                                //first financial Year
                                                if (successrahulf1)
                                                {
                                                    if (bf.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Apr-Sep";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "H";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf1.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Oct-Mar";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "H";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                }
                                                //if (successrahulf2)
                                                //{
                                                //    //Second financial Year
                                                //    if (bf2.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Apr-Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "H";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf3.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Oct-Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "H";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                                //if (successrahulf3)
                                                //{
                                                //    //Third financial Year
                                                //    if (bf4.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Apr-Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "H";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf5.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Oct-Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "H";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                            }
                                        }
                                        if (InternalauditschedulingList.Count != 0)
                                        {
                                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                        }
                                        BindAuditScheduleEDIT("H", 0, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));


                                    }
                                    #endregion

                                    #region Quarterly Save
                                    else if (lblEDITSchedulingType.Text == "Quarterly")
                                    {
                                        successrahulf1 = DeleteInternalAuditScheduling("Q", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf2 = DeleteInternalAuditScheduling("Q", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf3 = DeleteInternalAuditScheduling("Q", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        int processid = -1;
                                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        for (int i = 0; i < grdQuarterlyEDIT.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdQuarterlyEDIT.Rows[i];
                                            Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                //first financial Year
                                                CheckBox bf1 = (CheckBox) row.FindControl("chkQuarter1");
                                                CheckBox bf2 = (CheckBox) row.FindControl("chkQuarter2");
                                                CheckBox bf3 = (CheckBox) row.FindControl("chkQuarter3");
                                                CheckBox bf4 = (CheckBox) row.FindControl("chkQuarter4");
                                                ////Second financial Year
                                                //CheckBox bf5 = (CheckBox) row.FindControl("chkQuarter5");
                                                //CheckBox bf6 = (CheckBox) row.FindControl("chkQuarter6");
                                                //CheckBox bf7 = (CheckBox) row.FindControl("chkQuarter7");
                                                //CheckBox bf8 = (CheckBox) row.FindControl("chkQuarter8");
                                                ////Third financial Year
                                                //CheckBox bf9 = (CheckBox) row.FindControl("chkQuarter9");
                                                //CheckBox bf10 = (CheckBox) row.FindControl("chkQuarter10");
                                                //CheckBox bf11 = (CheckBox) row.FindControl("chkQuarter11");
                                                //CheckBox bf12 = (CheckBox) row.FindControl("chkQuarter12");

                                                //first financial Year

                                                if (successrahulf1)
                                                {
                                                    if (bf1.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Apr-Jun";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                    if (bf2.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Jul-Sep";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                    if (bf3.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Oct-Dec";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                    if (bf4.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Jan-Mar";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "Q";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                }
                                                //if (successrahulf2)
                                                //{
                                                //    //Second financial Year                          
                                                //    if (bf5.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Apr-Jun";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf6.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Jul-Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf7.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Oct-Dec";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf8.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Jan-Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                                //if (successrahulf3)
                                                //{
                                                //    //Third financial Year
                                                //    if (bf9.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Apr-Jun";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf10.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Jul-Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf11.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Oct-Dec";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf12.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Jan-Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "Q";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                            }
                                        }
                                        if (InternalauditschedulingList.Count != 0)
                                        {
                                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                        }
                                        BindAuditScheduleEDIT("Q", 0, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                    }
                                    #endregion

                                    #region Monthly Save
                                    else if (lblEDITSchedulingType.Text == "Monthly")
                                    {
                                        successrahulf1= DeleteInternalAuditScheduling("M", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf2= DeleteInternalAuditScheduling("M", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        //successrahulf3= DeleteInternalAuditScheduling("M", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, Convert.ToInt32(lblEDITVerticalID.Text));
                                        int processid = -1;
                                        List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                        for (int i = 0; i < grdMonthlyEDIT.Rows.Count; i++)
                                        {
                                            GridViewRow row = grdMonthlyEDIT.Rows[i];
                                            Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                            if (!string.IsNullOrEmpty(lblProcessId.Text))
                                            {
                                                processid = Convert.ToInt32(lblProcessId.Text);
                                                //first financial Year
                                                CheckBox bf1 = (CheckBox) row.FindControl("chkMonthly1");
                                                CheckBox bf2 = (CheckBox) row.FindControl("chkMonthly2");
                                                CheckBox bf3 = (CheckBox) row.FindControl("chkMonthly3");
                                                CheckBox bf4 = (CheckBox) row.FindControl("chkMonthly4");
                                                CheckBox bf5 = (CheckBox) row.FindControl("chkMonthly5");
                                                CheckBox bf6 = (CheckBox) row.FindControl("chkMonthly6");
                                                CheckBox bf7 = (CheckBox) row.FindControl("chkMonthly7");
                                                CheckBox bf8 = (CheckBox) row.FindControl("chkMonthly8");
                                                CheckBox bf9 = (CheckBox) row.FindControl("chkMonthly9");
                                                CheckBox bf10 = (CheckBox) row.FindControl("chkMonthly10");
                                                CheckBox bf11 = (CheckBox) row.FindControl("chkMonthly11");
                                                CheckBox bf12 = (CheckBox) row.FindControl("chkMonthly12");
                                                ////Second financial Year
                                                //CheckBox bf13 = (CheckBox) row.FindControl("chkMonthly13");
                                                //CheckBox bf14 = (CheckBox) row.FindControl("chkMonthly14");
                                                //CheckBox bf15 = (CheckBox) row.FindControl("chkMonthly15");
                                                //CheckBox bf16 = (CheckBox) row.FindControl("chkMonthly16");
                                                //CheckBox bf17 = (CheckBox) row.FindControl("chkMonthly17");
                                                //CheckBox bf18 = (CheckBox) row.FindControl("chkMonthly18");
                                                //CheckBox bf19 = (CheckBox) row.FindControl("chkMonthly19");
                                                //CheckBox bf20 = (CheckBox) row.FindControl("chkMonthly20");
                                                //CheckBox bf21 = (CheckBox) row.FindControl("chkMonthly21");
                                                //CheckBox bf22 = (CheckBox) row.FindControl("chkMonthly22");
                                                //CheckBox bf23 = (CheckBox) row.FindControl("chkMonthly23");
                                                //CheckBox bf24 = (CheckBox) row.FindControl("chkMonthly24");
                                                ////Third financial Year
                                                //CheckBox bf25 = (CheckBox) row.FindControl("chkMonthly25");
                                                //CheckBox bf26 = (CheckBox) row.FindControl("chkMonthly26");
                                                //CheckBox bf27 = (CheckBox) row.FindControl("chkMonthly27");
                                                //CheckBox bf28 = (CheckBox) row.FindControl("chkMonthly28");
                                                //CheckBox bf29 = (CheckBox) row.FindControl("chkMonthly29");
                                                //CheckBox bf30 = (CheckBox) row.FindControl("chkMonthly30");
                                                //CheckBox bf31 = (CheckBox) row.FindControl("chkMonthly31");
                                                //CheckBox bf32 = (CheckBox) row.FindControl("chkMonthly32");
                                                //CheckBox bf33 = (CheckBox) row.FindControl("chkMonthly33");
                                                //CheckBox bf34 = (CheckBox) row.FindControl("chkMonthly34");
                                                //CheckBox bf35 = (CheckBox) row.FindControl("chkMonthly35");
                                                //CheckBox bf36 = (CheckBox) row.FindControl("chkMonthly36");
                                                //first financial Year
                                                if (successrahulf1)
                                                {
                                                    if (bf1.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Apr";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf2.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "May";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf3.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Jun";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf4.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Jul";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf5.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Aug";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf6.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Sep";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf7.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Oct";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf8.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Nov";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf9.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Dec";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf10.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Jan";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf11.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Feb";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }

                                                    if (bf12.Checked)
                                                    {
                                                        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                        Internalauditscheduling.FinancialYear = f1;
                                                        Internalauditscheduling.TermName = "Mar";
                                                        Internalauditscheduling.TermStatus = true;
                                                        Internalauditscheduling.Process = processid;
                                                        Internalauditscheduling.ISAHQMP = "M";
                                                        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                        if (processid != 0)
                                                        {
                                                            InternalauditschedulingList.Add(Internalauditscheduling);
                                                        }
                                                    }
                                                }

                                                //Second financial Year 
                                                //if (successrahulf2)
                                                //{
                                                //    if (bf13.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Apr";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //    if (bf14.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "May";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf15.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Jun";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf16.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Jul";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf17.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Aug";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf18.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf19.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Oct";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf20.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Nov";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf21.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Dec";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf22.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Jan";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf23.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Feb";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf24.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f2;
                                                //        Internalauditscheduling.TermName = "Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}

                                                ////Third financial Year
                                                //if (successrahulf3)
                                                //{
                                                //    if (bf25.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Apr";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf26.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "May";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf27.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Jun";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf28.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Jul";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf29.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Aug";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf30.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Sep";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf31.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Oct";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf32.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Nov";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf33.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Dec";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf34.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Jan";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf35.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Feb";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }

                                                //    if (bf36.Checked)
                                                //    {
                                                //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                //        Internalauditscheduling.FinancialYear = f3;
                                                //        Internalauditscheduling.TermName = "Mar";
                                                //        Internalauditscheduling.TermStatus = true;
                                                //        Internalauditscheduling.Process = processid;
                                                //        Internalauditscheduling.ISAHQMP = "M";
                                                //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                //        if (processid != 0)
                                                //        {
                                                //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                //        }
                                                //    }
                                                //}
                                            }
                                        }
                                        if (InternalauditschedulingList.Count != 0)
                                        {
                                            UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                            cvDuplicateEntry1.IsValid = false;
                                            cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                        }
                                        BindAuditScheduleEDIT("M", 0, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                    }
                                    #endregion

                                    #region Phase Save
                                    else if (lblEDITSchedulingType.Text == "Phase")
                                    {
                                        int processid = -1;
                                        int noofphases = -1;
                                        if (!string.IsNullOrEmpty(lblEDITPhaseCount.Text))
                                        {
                                            noofphases = (Convert.ToInt32(lblEDITPhaseCount.Text));
                                        }
                                        #region Phase 1
                                        if (noofphases == 1)
                                        {
                                            successrahulf1=DeleteInternalAuditSchedulingWithCount("P", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf2=DeleteInternalAuditSchedulingWithCount("P", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf3=DeleteInternalAuditSchedulingWithCount("P", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase1EDIT.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase1EDIT.Rows[i];
                                                Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    CheckBox bf = (CheckBox) row.FindControl("chkPhase1");
                                                    //CheckBox bf1 = (CheckBox) row.FindControl("chkPhase2");
                                                    //CheckBox bf2 = (CheckBox) row.FindControl("chkPhase3");
                                                    if (successrahulf1)
                                                    {
                                                        if (bf.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase1";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }
                                                    }
                                                    //if (successrahulf2)
                                                    //{
                                                    //    if (bf1.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                    //if (successrahulf3)
                                                    //{
                                                    //    if (bf2.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                }
                                            }
                                            if (InternalauditschedulingList.Count != 0)
                                            {
                                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                            }
                                            BindAuditScheduleEDIT("P", noofphases, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                        }
                                        #endregion
                                        #region Phase 2
                                        else if (noofphases == 2)
                                        {
                                            successrahulf1 = DeleteInternalAuditSchedulingWithCount("P", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf2 = DeleteInternalAuditSchedulingWithCount("P", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf3 = DeleteInternalAuditSchedulingWithCount("P", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase2EDIT.Rows.Count; i++)
                                            {
                                                GridViewRow row = grdphase2EDIT.Rows[i];
                                                Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    CheckBox bf1 = (CheckBox) row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox) row.FindControl("chkPhase2");
                                                    //CheckBox bf3 = (CheckBox) row.FindControl("chkPhase3");
                                                    //CheckBox bf4 = (CheckBox) row.FindControl("chkPhase4");
                                                    //CheckBox bf5 = (CheckBox) row.FindControl("chkPhase5");
                                                    //CheckBox bf6 = (CheckBox) row.FindControl("chkPhase6");
                                                    if (successrahulf1)
                                                    {
                                                        if (bf1.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase1";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf2.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase2";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }
                                                    }
                                                    //Second Finacial Year
                                                    //if (successrahulf2)
                                                    //{
                                                    //    if (bf3.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf4.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                    ////Third Financial Year
                                                    //if (successrahulf3)
                                                    //{
                                                    //    if (bf5.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf6.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                }
                                            }
                                            if (InternalauditschedulingList.Count != 0)
                                            {
                                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                            }
                                            BindAuditScheduleEDIT("P", noofphases, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                        }
                                        #endregion
                                        #region Phase 3
                                        else if (noofphases == 3)
                                        {
                                            successrahulf1 = DeleteInternalAuditSchedulingWithCount("P", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf2 = DeleteInternalAuditSchedulingWithCount("P", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf3 = DeleteInternalAuditSchedulingWithCount("P", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase3EDIT.Rows.Count; i++)
                                            {

                                                GridViewRow row = grdphase3EDIT.Rows[i];
                                                Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    CheckBox bf1 = (CheckBox) row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox) row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox) row.FindControl("chkPhase3");
                                                    //CheckBox bf4 = (CheckBox) row.FindControl("chkPhase4");
                                                    //CheckBox bf5 = (CheckBox) row.FindControl("chkPhase5");
                                                    //CheckBox bf6 = (CheckBox) row.FindControl("chkPhase6");
                                                    //CheckBox bf7 = (CheckBox) row.FindControl("chkPhase7");
                                                    //CheckBox bf8 = (CheckBox) row.FindControl("chkPhase8");
                                                    //CheckBox bf9 = (CheckBox) row.FindControl("chkPhase9");
                                                    if (successrahulf1)
                                                    {
                                                        if (bf1.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase1";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf2.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase2";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf3.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase3";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }
                                                    }
                                                    //Second Finacial Year    
                                                    //if (successrahulf2)
                                                    //{
                                                    //    if (bf4.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf5.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf6.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                    ////Third Financial Year  
                                                    //if (successrahulf3)
                                                    //{
                                                    //    if (bf7.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf8.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf9.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                }
                                            }
                                            if (InternalauditschedulingList.Count != 0)
                                            {
                                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                            }
                                            BindAuditScheduleEDIT("P", noofphases, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                        }
                                        #endregion
                                        #region Phase 4
                                        else if (noofphases == 4)
                                        {
                                            successrahulf1 = DeleteInternalAuditSchedulingWithCount("P", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf2 = DeleteInternalAuditSchedulingWithCount("P", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf3 = DeleteInternalAuditSchedulingWithCount("P", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase4EDIT.Rows.Count; i++)
                                            {

                                                GridViewRow row = grdphase4EDIT.Rows[i];
                                                Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    CheckBox bf1 = (CheckBox) row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox) row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox) row.FindControl("chkPhase3");
                                                    CheckBox bf4 = (CheckBox) row.FindControl("chkPhase4");
                                                    //CheckBox bf5 = (CheckBox) row.FindControl("chkPhase5");
                                                    //CheckBox bf6 = (CheckBox) row.FindControl("chkPhase6");
                                                    //CheckBox bf7 = (CheckBox) row.FindControl("chkPhase7");
                                                    //CheckBox bf8 = (CheckBox) row.FindControl("chkPhase8");
                                                    //CheckBox bf9 = (CheckBox) row.FindControl("chkPhase9");
                                                    //CheckBox bf10 = (CheckBox) row.FindControl("chkPhase10");
                                                    //CheckBox bf11 = (CheckBox) row.FindControl("chkPhase11");
                                                    //CheckBox bf12 = (CheckBox) row.FindControl("chkPhase12");
                                                    if (successrahulf1)
                                                    {
                                                        if (bf1.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase1";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf2.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase2";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf3.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase3";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf4.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase4";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }
                                                    }
                                                    //if (successrahulf2)
                                                    //{
                                                    //    //Second Finacial Year                               
                                                    //    if (bf5.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf6.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf7.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf8.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase4";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}

                                                    ////Third Financial Year  
                                                    //if (successrahulf3)
                                                    //{
                                                    //    if (bf9.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf10.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf11.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf12.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase4";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                }
                                            }
                                            if (InternalauditschedulingList.Count != 0)
                                            {
                                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated Successfully.";
                                            }
                                            BindAuditScheduleEDIT("P", noofphases, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                        }
                                        #endregion
                                        #region Phase 5
                                        else if (noofphases == 5)
                                        {
                                            successrahulf1 = DeleteInternalAuditSchedulingWithCount("P", f1, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf2 = DeleteInternalAuditSchedulingWithCount("P", f2, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));
                                            //successrahulf3 = DeleteInternalAuditSchedulingWithCount("P", f3, Convert.ToInt32(lblEDITLocationID.Text), lblEDITTermName.Text, noofphases, Convert.ToInt32(lblEDITVerticalID.Text));

                                            List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                                            for (int i = 0; i < grdphase5EDIT.Rows.Count; i++)
                                            {

                                                GridViewRow row = grdphase5EDIT.Rows[i];
                                                Label lblProcessId = (Label) row.FindControl("lblProcessID");
                                                if (!string.IsNullOrEmpty(lblProcessId.Text))
                                                {
                                                    processid = Convert.ToInt32(lblProcessId.Text);
                                                    CheckBox bf1 = (CheckBox) row.FindControl("chkPhase1");
                                                    CheckBox bf2 = (CheckBox) row.FindControl("chkPhase2");
                                                    CheckBox bf3 = (CheckBox) row.FindControl("chkPhase3");
                                                    CheckBox bf4 = (CheckBox) row.FindControl("chkPhase4");
                                                    CheckBox bf5 = (CheckBox) row.FindControl("chkPhase5");
                                                    //CheckBox bf6 = (CheckBox) row.FindControl("chkPhase6");
                                                    //CheckBox bf7 = (CheckBox) row.FindControl("chkPhase7");
                                                    //CheckBox bf8 = (CheckBox) row.FindControl("chkPhase8");
                                                    //CheckBox bf9 = (CheckBox) row.FindControl("chkPhase9");
                                                    //CheckBox bf10 = (CheckBox) row.FindControl("chkPhase10");
                                                    //CheckBox bf11 = (CheckBox) row.FindControl("chkPhase11");
                                                    //CheckBox bf12 = (CheckBox) row.FindControl("chkPhase12");
                                                    //CheckBox bf13 = (CheckBox) row.FindControl("chkPhase10");
                                                    //CheckBox bf14 = (CheckBox) row.FindControl("chkPhase11");
                                                    //CheckBox bf15 = (CheckBox) row.FindControl("chkPhase12");
                                                    if (successrahulf1)
                                                    {
                                                        if (bf1.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase1";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf2.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase2";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf3.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase3";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf4.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase4";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }

                                                        if (bf5.Checked)
                                                        {
                                                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                            Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                            Internalauditscheduling.FinancialYear = f1;
                                                            Internalauditscheduling.TermName = "Phase5";
                                                            Internalauditscheduling.TermStatus = true;
                                                            Internalauditscheduling.Process = processid;
                                                            Internalauditscheduling.ISAHQMP = "P";
                                                            Internalauditscheduling.PhaseCount = noofphases;
                                                            Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                            Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                            if (processid != 0)
                                                            {
                                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                                            }
                                                        }
                                                    }
                                                    //if (successrahulf2)
                                                    //{
                                                    //    //Second Finacial Year                               
                                                    //    if (bf6.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf7.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf8.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf9.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase4";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf10.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f2;
                                                    //        Internalauditscheduling.TermName = "Phase5";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}
                                                    //if (successrahulf3)
                                                    //{
                                                    //    //Third Financial Year                                
                                                    //    if (bf11.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase1";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf12.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase2";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf13.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase3";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf14.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase4";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }

                                                    //    if (bf15.Checked)
                                                    //    {
                                                    //        InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                    //        Internalauditscheduling.CustomerBranchId = Convert.ToInt32(lblEDITLocationID.Text);
                                                    //        Internalauditscheduling.FinancialYear = f3;
                                                    //        Internalauditscheduling.TermName = "Phase5";
                                                    //        Internalauditscheduling.TermStatus = true;
                                                    //        Internalauditscheduling.Process = processid;
                                                    //        Internalauditscheduling.ISAHQMP = "P";
                                                    //        Internalauditscheduling.PhaseCount = noofphases;
                                                    //        Internalauditscheduling.VerticalID = Convert.ToInt32(lblEDITVerticalID.Text);
                                                    //        Internalauditscheduling.StartDate = Convert.ToDateTime(lblEDITStartDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        Internalauditscheduling.EndDate = Convert.ToDateTime(lblEDITEndDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                    //        if (processid != 0)
                                                    //        {
                                                    //            InternalauditschedulingList.Add(Internalauditscheduling);
                                                    //        }
                                                    //    }
                                                    //}

                                                }
                                            }
                                            if (InternalauditschedulingList.Count != 0)
                                            {
                                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                                cvDuplicateEntry1.IsValid = false;
                                                cvDuplicateEntry1.ErrorMessage = "Audit Schedule Updated successfully.";
                                            }
                                            BindAuditScheduleEDIT("P", noofphases, Convert.ToInt32(lblEDITLocationID.Text), Convert.ToInt32(lblEDITVerticalID.Text));
                                        }
                                        #endregion
                                    }
                                    #endregion

                                    BindMainGrid();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditScheduling.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                BindMainGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditScheduling.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry50.IsValid = false;
                cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {               
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditScheduling.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }              
        //        BindGridForNextpreviouse();
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry50.IsValid = false;
        //        cvDuplicateEntry50.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        ///// <summary>
        ///// Added for next button in gridview.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdAuditScheduling.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditScheduling.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        //BindMainGrid(-1, -1, "");
        //        BindGridForNextpreviouse();
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}


        /// <summary>
        /// Added for Get Total Page Count in grdview Total page row count.
        /// </summary>
        /// <returns></returns>
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}



        ////////////////////////////Sushant Code///////////////

        #region Dropdown code Start
        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindLegalEntityDataPop()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntityPop.DataTextField = "Name";
            ddlLegalEntityPop.DataValueField = "ID";
            ddlLegalEntityPop.Items.Clear();
            ddlLegalEntityPop.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntityPop.DataBind();
            ddlLegalEntityPop.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityDataPop(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }




        #endregion End Code
        protected void ddlVerticalID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }

                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                    BindVerticalID(Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
                ddlPageSize_SelectedIndexChanged(sender, e);
            }
        }

        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    BindVerticalID(Convert.ToInt32(ddlSubEntity4.SelectedValue));
                    ddlPageSize_SelectedIndexChanged(sender, e);
                }
            }
        }

        protected void btnSaveMainGrid_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                //int ID = -1;
                for (int i = 0; i < grdAuditScheduling.Rows.Count; i++)
                {
                    GridViewRow row = grdAuditScheduling.Rows[i];
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtStartDate = (TextBox)row.FindControl("txtStartDateGrid");
                    TextBox txtEndDate = (TextBox)row.FindControl("txtEndDateGrid");

                    if (txtStartDate.Text != "" && txtEndDate.Text != "")
                    {
                        if (!string.IsNullOrEmpty(lblID.Text))
                        {
                            DateTime a = DateTime.ParseExact(txtStartDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime b = DateTime.ParseExact(txtEndDate.Text.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                            Internalauditscheduling.Id = Convert.ToInt32(lblID.Text);
                            Internalauditscheduling.StartDate = GetDate(a.ToString("dd/MM/yyyy"));
                            Internalauditscheduling.EndDate = GetDate(b.ToString("dd/MM/yyyy"));
                            ProcessManagement.UpdateInternalAuditorScheduling(Internalauditscheduling);
                        }
                    }
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public void BindddlBranchApplyto()
        {

            int Branchid = -1;
            if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
            {
                if (ddlLegalEntityPop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
            {
                if (ddlSubEntity1Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
            {
                if (ddlSubEntity2Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
            {
                if (ddlSubEntity3Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
            {
                if (ddlSubEntity4Pop.SelectedValue != "-1")
                {
                    Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                }
            }

            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlBranchList.Items.Clear();
            var details= AuditKickOff_NewDetails.FillSubEntityDataScheduleingApplyTO(Branchid, customerID);

            ddlBranchList.DataSource = details;
            ddlBranchList.DataTextField = "Name";
            ddlBranchList.DataValueField = "ID";
            ddlBranchList.DataBind();
            updateApplyToPopUp.Update();

        }

        protected void btnSaveApplyto_Click(object sender, EventArgs e)
        {
            try
            {
                List<InternalAuditScheduling> InternalauditschedulingList = new List<InternalAuditScheduling>();
                int Branchid = -1;
                int VerticalID = -1;
                bool SaveSuccess = false;
                if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
                {
                    if (ddlLegalEntityPop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlLegalEntityPop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
                {
                    if (ddlSubEntity1Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity1Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
                {
                    if (ddlSubEntity2Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity2Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
                {
                    if (ddlSubEntity3Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity3Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
                {
                    if (ddlSubEntity4Pop.SelectedValue != "-1")
                    {
                        Branchid = Convert.ToInt32(ddlSubEntity4Pop.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlVerticalListPopup.SelectedValue))
                {
                    if (ddlVerticalListPopup.SelectedValue != "-1")
                    {
                        VerticalID = Convert.ToInt32(ddlVerticalListPopup.SelectedValue);
                    }
                }
                if (VerticalID != -1)
                {
                    var schedulingList = UserManagementRisk.GetDataFromPopUpSave(Branchid, VerticalID);
                    if (schedulingList.Count > 0)
                    {
                        foreach (var item in schedulingList)
                        {
                            InternalauditschedulingList.Clear();
                            for (int i = 0; i < ddlBranchList.Items.Count; i++)
                            {
                                if (ddlBranchList.Items[i].Selected == true)
                                {
                                    var verticalIDList = UserManagementRisk.FillVerticalListFromRiskActTrasa(Convert.ToInt32(ddlBranchList.Items[i].Value), item.VerticalID);
                                    if (verticalIDList.Count > 0)
                                    {
                                        foreach (var Items in verticalIDList)
                                        {
                                            if (UserManagementRisk.GetPreviousAuditScheduling(Convert.ToInt32(ddlBranchList.Items[i].Value),Convert.ToInt32(Items.VerticalsId), item.FinancialYear, item.TermName))
                                            {
                                                InternalAuditScheduling Internalauditscheduling = new InternalAuditScheduling();
                                                Internalauditscheduling.CustomerBranchId = Convert.ToInt32(ddlBranchList.Items[i].Value);
                                                Internalauditscheduling.FinancialYear = item.FinancialYear;
                                                Internalauditscheduling.TermName = item.TermName;
                                                Internalauditscheduling.TermStatus = item.TermStatus;
                                                Internalauditscheduling.Process = item.Process;
                                                Internalauditscheduling.ISAHQMP = item.ISAHQMP;
                                                Internalauditscheduling.StartDate = Convert.ToDateTime(item.StartDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.EndDate = Convert.ToDateTime(item.EndDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                                                Internalauditscheduling.VerticalID = (int) Items.VerticalsId;

                                                InternalauditschedulingList.Add(Internalauditscheduling);
                                            }
                                        }
                                    }                                 
                                }
                            }
                            if (InternalauditschedulingList.Count != 0)
                            {
                                UserManagementRisk.AddDetailsInternalAuditSchedulingTable(InternalauditschedulingList);
                                SaveSuccess = true;
                            }
                        }

                        if (SaveSuccess)
                        {
                            cvDuplicateEntryApplyPop.IsValid = false;
                            cvDuplicateEntryApplyPop.ErrorMessage = "Audit Schedule Save Successfully.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller1()", true);
                            ddlBranchList.ClearSelection();
                        }
                    }
                    else
                    {
                        cvDuplicateEntryApplyPop.IsValid = false;
                        cvDuplicateEntryApplyPop.ErrorMessage = "No Previous Audit Schedule Record found for current selection.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryApplyPop.IsValid = false;
                cvDuplicateEntryApplyPop.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// //////////////////////Pop Up code Sushant//////////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLegalEntityPop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntityPop.SelectedValue))
            {
                if (ddlLegalEntityPop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity1Pop, Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(ddlLegalEntityPop.SelectedValue));
                }

                if (ddlSubEntity2Pop.Items.Count > 0)
                    ddlSubEntity2Pop.Items.Clear();

                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();

               
                BindGridpopData();                
            }
        }

        protected void ddlSubEntity1Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1Pop.SelectedValue))
            {
                if (ddlSubEntity1Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity2Pop, Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(ddlSubEntity1Pop.SelectedValue));
                }
                if (ddlSubEntity3Pop.Items.Count > 0)
                    ddlSubEntity3Pop.Items.Clear();

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();


                BindGridpopData();
                //  BindddlBranchApplyto(ddlSubEntity1Pop);
            }
        }

        protected void ddlSubEntity2Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2Pop.SelectedValue))
            {
                if (ddlSubEntity2Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity3Pop, Convert.ToInt32(ddlSubEntity2Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(ddlSubEntity2Pop.SelectedValue));
                }

                if (ddlSubEntity4Pop.Items.Count > 0)
                    ddlSubEntity4Pop.Items.Clear();
                BindGridpopData();
                //  BindddlBranchApplyto(ddlSubEntity2Pop);
            }
        }

        protected void ddlSubEntity3Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3Pop.SelectedValue))
            {
                if (ddlSubEntity3Pop.SelectedValue != "-1")
                {
                    BindSubEntityDataPop(ddlSubEntity4Pop, Convert.ToInt32(ddlSubEntity3Pop.SelectedValue));
                    BindVerticalIDPOPup(Convert.ToInt32(ddlSubEntity3Pop.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4Pop.Items.Count > 0)
                        ddlSubEntity4Pop.Items.Clear();
                }

                BindGridpopData();
            }
        }

        protected void ddlSubEntity4Pop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity4Pop.SelectedValue))
            {
                BindGridpopData();
                BindVerticalIDPOPup(Convert.ToInt32(ddlSubEntity4Pop.SelectedValue));
            }   
        }
        public void BindGridpopData()
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    if (ddlSchedulingType.SelectedItem.Text == "Annually")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("A", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("H", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("Q", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                    {
                        EnableDisable(0);
                        BindAuditSchedule("M", 0);
                    }
                    else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                    {
                        //EnableDisable(0);
                        if (ddlSchedulingType.SelectedItem.Text == "Phase")
                        {
                            Divnophase.Visible = true;
                        }
                        else
                        {
                            Divnophase.Visible = false;
                        }
                        cleardatasource();
                    }
                }
            }
            else
            {
                cleardatasource();
            }
        }
        protected void btnapply_Click(object sender, EventArgs e)
        {
            BindddlBranchApplyto();
        }
    }
}