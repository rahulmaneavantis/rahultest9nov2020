﻿<%@ Page Title="Testing Status Summary" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="InternalAuditStatusSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.InternalAuditStatusSummary" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #aBackChk:hover {
            color: blue;
            text-decoration: underline;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Workspace/ Internal Audit Status');
        });
    </script>
    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upAuditStatusSummary" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                                                                                     
                            <div class="panel-body">

                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                ValidationGroup="AuditValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="AuditValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                            </div>
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5"/>
                                    <asp:ListItem Text="10"/>
                                    <asp:ListItem Text="20" Selected="True" />
                                    <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>                                      
                                                                    
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen ID="ddlProcess" runat="server" DataPlaceHolder="Process" AutoPostBack="true" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                    OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                   
                                </div>

                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                     
                                    <asp:DropDownListChosen ID="ddlSubProcess" runat="server" DataPlaceHolder="Sub Process"  AutoPostBack="true" 
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                    OnSelectedIndexChanged="ddlSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                       
                                </div>  

                                 <div class="col-md-3 colpadding0" style="margin-top: 5px; text-align:right;"> 
                                 <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary"  OnClick="btnBack_Click"/>                                                                                                                                                                             
                                 </div>                                                    
                            </div>   
                                                                                           
                            <div class="clearfix"></div>                                                                                      
                            <div class="clearfix"></div>                                
                            <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdAuditStatus"
                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                            PageSize="20" AllowPaging="true" AutoPostBack="true"
                            OnRowCommand="grdAuditStatus_RowCommand"  OnPageIndexChanging="grdAuditStatus_PageIndexChanging"
                            CssClass="table" GridLines="None" Width="100%" AllowSorting="true"                                    
                            ShowFooter="true" >
                            <Columns>
                                <asp:TemplateField  HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Process" ItemStyle-Width="250px" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblProcess" runat="server" Text='<%#Eval("Process") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Process") %>' />
                                        </div>
                                    </ItemTemplate>                           
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub Process" ItemStyle-Width="250px" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblSubProcess" runat="server" Text='<%#Eval("SubProcess") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Process") %>' />
                                        </div>
                                    </ItemTemplate>                            
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                                    <ItemTemplate>                                                 
                                        <asp:LinkButton ID="lblRiskTotal" runat="server" Text='<%# Eval("Total") %>' CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>                            
                                </asp:TemplateField>                                         
                                <asp:TemplateField HeaderText="Open" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblRiskNotDone" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Open;"+ Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("RoleID") %>' Text='<%# Eval("Open") %>' CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>                            
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Submitted" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblRiskCompleted" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Submitted;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("RoleID") %>' Text='<%# Eval("Submited") %>' CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>                           
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Review Comment" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblRiskReviewComment" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "ReviewComment;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("RoleID") %>' Text='<%# Eval("ReviewComment") %>' CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>                            
                                </asp:TemplateField>                                        
                                <asp:TemplateField HeaderText="Closed" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblRiskClosed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# "Closed;" + Eval("InstanceID")+";"+ Eval("ScheduleOnID")+";"+Eval("ProcessId")+";"+Eval("SubProcessId")+";"+Eval("CustomerBranchID")+";"+Eval("RoleID") %>' Text='<%# Eval("Closed") %>' ></asp:LinkButton>
                                    </ItemTemplate>                            
                                </asp:TemplateField> 
                            </Columns>
                            <RowStyle CssClass="clsROWgrid"/>
                            <HeaderStyle CssClass="clsheadergrid" />   
                             <PagerSettings Visible="false" />                   
                            <PagerTemplate>                          
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate> 
                            </asp:GridView>
                             <div style="float: right;">
                              <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                  class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                              </asp:DropDownListChosen>  
                            </div>

                           
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="float:right;">
                                    <div class="table-paging" style="margin-bottom: 10px;">                                        
                                            <div class="table-paging-text" style="float:right;">
                                                <p>Page                                         
                                                </p>
                                            </div>                                        
                                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>                                                                                                     
                          </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
