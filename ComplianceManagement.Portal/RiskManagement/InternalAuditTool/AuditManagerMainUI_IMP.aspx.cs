﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class AuditManagerMainUI_IMP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        if (!String.IsNullOrEmpty(Request.QueryString["SID"]))
                            if (!String.IsNullOrEmpty(Request.QueryString["CustBranchID"]))
                                if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
                                    if (!String.IsNullOrEmpty(Request.QueryString["Period"]))
                                        if (!String.IsNullOrEmpty(Request.QueryString["VID"]))
                                        {
                                            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
                                            {
                                                int chkRole = Convert.ToInt32(Request.QueryString["RoleID"]);
                                                ViewState["RoleID"] = Request.QueryString["RoleID"];
                                                int CBranchID = Convert.ToInt32(Request.QueryString["CustBranchID"]);
                                                ViewState["Arguments"] = Request.QueryString["Status"] + ";" + Request.QueryString["ID"] + ";" + Request.QueryString["SID"] + ";" + Request.QueryString["CustBranchID"] + ";" + Request.QueryString["FY"] + ";" + Request.QueryString["Period"] + ";" + Request.QueryString["VID"] + ";" + Request.QueryString["AuditID"];
                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                                ViewState["VerticalID"] = Request.QueryString["VID"];
                                                bindPageNumber();
                                                int customerID = -1;
                                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                if (!string.IsNullOrEmpty(Request.QueryString["SID"]))
                                                {
                                                    string statusidasd = Request.QueryString["Status"].ToString();
                                                    int statusFlag = 0;
                                                    if (statusidasd.Equals("NotDone"))
                                                    {
                                                        statusFlag = 1;
                                                    }
                                                    else if (statusidasd.Equals("Submitted"))
                                                    {
                                                        statusFlag = 2;
                                                    }
                                                    else if (statusidasd.Equals("TeamReview"))
                                                    {
                                                        statusFlag = 4;
                                                    }
                                                    else if (statusidasd.Equals("AuditeeReview"))
                                                    {
                                                        statusFlag = 6;
                                                    }
                                                    else if (statusidasd.Equals("FinalReview"))
                                                    {
                                                        statusFlag = 5;
                                                    }
                                                    else if (statusidasd.Equals("Closed"))
                                                    {
                                                        statusFlag = 3;
                                                    }

                                                    if (statusFlag != 0)
                                                    {
                                                        ddlFilterStatus.ClearSelection();
                                                        ddlFilterStatus.Items.FindByValue(statusFlag.ToString()).Selected = true;
                                                    }
                                                    bool checkPRFlag = false;
                                                    checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(Convert.ToInt32(customerID), Convert.ToInt32(CBranchID));
                                                    var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                    var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);
                                                    if (checkPRFlag == true)
                                                    {
                                                        bool checkPRRiskActivationFlag = false;
                                                        checkPRRiskActivationFlag = CustomerManagementRisk.CheckPersonResponsibleFlowApplicable(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CBranchID);

                                                        if (litrole.Contains(4)) // If Audit Manager IS Reviewer
                                                        {
                                                            if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                            {
                                                                lbkSave.Enabled = false;
                                                                ddlSaveStatus.Visible = false;
                                                            }
                                                            else
                                                            {
                                                                lbkSave.Enabled = true;
                                                                ddlSaveStatus.Visible = true;
                                                            }
                                                        }
                                                        //if Audit Manager Is PersonResponsible
                                                        else if ((AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH") && checkPRRiskActivationFlag)
                                                        {
                                                            if (statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                            {
                                                                lbkSave.Enabled = false;
                                                                ddlSaveStatus.Visible = false;
                                                            }
                                                            else
                                                            {
                                                                lbkSave.Enabled = true;
                                                                ddlSaveStatus.Visible = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                            {
                                                                lbkSave.Enabled = false;
                                                                ddlSaveStatus.Visible = false;
                                                            }
                                                            else
                                                            {
                                                                lbkSave.Enabled = true;
                                                                ddlSaveStatus.Visible = true;
                                                            }
                                                        }
                                                        BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                    }
                                                    else
                                                    {
                                                        if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("Submitted") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                                                        {
                                                            lbkSave.Enabled = false;
                                                            ddlSaveStatus.Visible = false;
                                                        }
                                                        else
                                                        {
                                                            lbkSave.Enabled = true;
                                                            ddlSaveStatus.Visible = true;
                                                        }

                                                        BindStatusList(5, litrole.ToList(), AuditHeadOrManager);
                                                    }

                                                    #region Previous Code
                                                    //if (chkRole == 3)
                                                    //{
                                                    //    if (statusidasd.Equals("Submitted") || statusidasd.Equals("Closed") || statusidasd.Equals("FinalReview") || statusidasd.Equals("AuditeeReview"))
                                                    //    {
                                                    //        lbkSave.Enabled = false;
                                                    //        ddlSaveStatus.Visible = false;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        lbkSave.Enabled = true;
                                                    //        ddlSaveStatus.Visible = false;
                                                    //    }
                                                    //}
                                                    //if (chkRole == 4)
                                                    //{
                                                    //    bool flagStatus = false;
                                                    //    if (statusidasd.Equals("NotDone") || statusidasd.Equals("Closed"))
                                                    //    {
                                                    //        lbkSave.Enabled = false;
                                                    //        ddlSaveStatus.Visible = false;
                                                    //        flagStatus = false;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        lbkSave.Enabled = true;
                                                    //        ddlSaveStatus.Visible = true;
                                                    //        flagStatus = true;
                                                    //    }

                                                    //    //int customerID = -1;
                                                    //    if (flagStatus == true)
                                                    //    {
                                                    //        bool checkPRFlag = false;
                                                    //        checkPRFlag = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(customerID, CBranchID);

                                                    //        var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                    //        var litrole = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                                                    //        if (checkPRFlag == true)
                                                    //        {
                                                    //            if (!(statusidasd.Equals("Closed")))
                                                    //            {
                                                    //                BindStatusList(7, litrole.ToList(), AuditHeadOrManager);
                                                    //            }
                                                    //        }
                                                    //        else
                                                    //        {
                                                    //            if (!(statusidasd.Equals("Closed")))
                                                    //            {
                                                    //                BindStatusList(4, litrole.ToList(), AuditHeadOrManager);
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}

                                                    #endregion
                                                }
                                            }
                                        }
            }
        }
        public static ImplementationAuditResult GetImplementationAuditResultbyID(long AuditScheduleOnID, int ResultID, long CustomerBranchId, int Verticalid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.ImplementationAuditResults
                                     where row.AuditScheduleOnID == AuditScheduleOnID && row.ResultID == ResultID
                                     && row.CustomerBranchId == CustomerBranchId && row.VerticalID == Verticalid
                                     && row.AuditID == AuditID
                                     select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                return MstRiskResult;
            }
        }
        public string ShowManagementResponse(int ResultID, int VerticalId, long ScheduledOnID, long CustomerBranchId)
        {
            long AuditID = -1;
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt64(Request.QueryString["AuditID"]);
            }
            string ManagementResponse = string.Empty;
            var MstRiskResult = GetImplementationAuditResultbyID(ScheduledOnID, Convert.ToInt32(ResultID), CustomerBranchId, VerticalId, AuditID);
            if (MstRiskResult != null)
            {
                ManagementResponse = MstRiskResult.ManagementResponse;
            }
            return ManagementResponse;
        }
        private void BindStatusList(int statusID, List<int> rolelist, string isAuditmanager)
        {
            try
            {
                ddlSaveStatus.Items.Clear();
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();

                if (statusID == 7)
                {
                    if (ddlFilterStatus.SelectedValue == "6")
                    {
                        if (isAuditmanager == "AM" || isAuditmanager == "AH")
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4)
                                {
                                }
                                else
                                {
                                    ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                        else
                        {
                            foreach (InternalAuditStatu st in allowedStatusList)
                            {
                                if (st.ID == 4 || st.ID == 6)
                                {
                                }
                                else
                                {
                                    ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }

                    }
                    else if (ddlFilterStatus.SelectedValue == "5")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((st.ID == 4) || (st.ID == 6))
                            {
                            }
                            else
                            {
                                ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else if (ddlFilterStatus.SelectedValue == "4")
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 5))
                            {

                                ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {

                                ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));

                            }
                            else
                            {

                                if (!(st.ID == 5))
                                {
                                    ddlSaveStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                }
                            }
                        }
                    }
                }//Personresponsible End
                else
                {
                    foreach (InternalAuditStatu st in allowedStatusList)
                    {
                        if (!(st.ID == 6))
                        {
                            ddlSaveStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverageIMP.PageIndex = chkSelectedPage - 1;

            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindDetailView(ViewState["Arguments"].ToString(), "P");

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string financialyear = string.Empty;
            string url4 = "";
            long AuditID = -1;
            if (!String.IsNullOrEmpty(Request.QueryString["AuditID"]))
            {
                AuditID = Convert.ToInt64(Request.QueryString["AuditID"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["returnUrl1"]))
            {
                url4 = "&returnUrl1=" + Request.QueryString["returnUrl1"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["FY"]))
            {
                financialyear = Convert.ToString(Request.QueryString["FY"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["DrilDown"]))
            {
                if (Convert.ToString(Request.QueryString["DrilDown"]) == "Yes")
                {
                    Response.Redirect("~/RiskManagement/AuditTool/AuditManagerStatusUI.aspx?Status=Open&Type=Implementation");
                }
            }
            else
            {
                Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManagerIMPStatusSummary.aspx?" + url4 + "&AuditID=" + AuditID+ "&FY=" + financialyear);
            }
        }



        protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                String Args = btn.CommandArgument.ToString();

                if (Args != "")
                {
                    string[] arg = Args.ToString().Split(',');

                    if (arg[4] == "")
                        arg[4] = "1";

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialog(" + arg[0] + "," + arg[1] + ",'" + arg[2] + "','" + arg[3] + "'," + arg[4] + "," + arg[5] + "," + arg[6] + "," + arg[7] + ");", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowSampleDocumentName(string DownloadFileName)
        {
            string processnonprocess = "";
            if (!String.IsNullOrEmpty(DownloadFileName))
            {
                processnonprocess = "Download";
            }
            else
            {
                processnonprocess = "File Not Uploded";
            }
            return processnonprocess;
        }

        public bool ATBDVisibleorNot(int? StatusID)
        {
            if (ViewState["roleid"] != null)
            {
                if (StatusID == null && Convert.ToInt32(ViewState["roleid"]) == 4)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }

        private void BindDetailView(string Arguments, String IFlag)
        {
            try
            {
                int BranchID = -1;
                string financialyear = String.Empty;
                string period = String.Empty;
                string filter = String.Empty;
                int VerticalID = -1;
                int ScheduledonID = -1;
                long AuditID = -1;

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string[] arg = Arguments.ToString().Split(';');

                if (arg.Length > 0)
                {
                    ScheduledonID = Convert.ToInt32(arg[2]);
                    BranchID = Convert.ToInt32(arg[3]);
                    financialyear = Convert.ToString(arg[4]);
                    period = Convert.ToString(arg[5]);
                    VerticalID = Convert.ToInt32(arg[6]);
                    AuditID = Convert.ToInt64(arg[7]);
                    List<int> statusIds = new List<int>();
                    List<int?> statusNullableIds = new List<int?>();

                    filter = arg[0].Trim();

                    if (arg[0].Trim().Equals("NotDone"))
                    {
                        statusIds.Add(-1);
                    }
                    else if (arg[0].Trim().Equals("Submited"))
                    {
                        statusIds.Add(2);
                    }
                    else if (arg[0].Trim().Equals("TeamReview"))
                    {
                        statusIds.Add(4);
                    }
                    else if (arg[0].Trim().Equals("Closed"))
                    {
                        statusIds.Add(3);
                    }
                    else if (arg[0].Trim().Equals("AuditeeReview"))
                    {
                        statusIds.Add(6);
                    }
                    else if (arg[0].Trim().Equals("FinalReview"))
                    {
                        statusIds.Add(5);
                    }
                    else
                    {
                        statusIds.Add(1);
                        statusIds.Add(4);
                        statusIds.Add(2);
                        statusIds.Add(3);
                        statusIds.Add(5);
                        statusIds.Add(6);
                    }

                    if (IFlag == "P")
                    {
                        var detailView = InternalControlManagementDashboardRisk.GetAuditManagerDetailIMP(customerID, BranchID, VerticalID, financialyear, period, ScheduledonID, statusIds, statusNullableIds, filter, AuditID);
                        Session["TotalRows"] = detailView.Count;
                        grdSummaryDetailsAuditCoverageIMP.DataSource = detailView;
                        grdSummaryDetailsAuditCoverageIMP.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}

                //Reload the Grid
                BindDetailView(ViewState["Arguments"].ToString(), "P");
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverageIMP.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;

                if (Flag == "OS")
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        //Reload the Grid
        //        BindDetailView(ViewState["Arguments"].ToString(), "P");

        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void grdSummaryDetailsAuditCoverageIMP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var details = RiskCategoryManagement.FillUsers(customerID);

                string statusidasd = Request.QueryString["Status"].ToString();

                if (statusidasd.Equals("AuditeeReview") || statusidasd.Equals("NotDone") || statusidasd.Equals("Closed") || statusidasd.Equals("TeamReview"))
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = false;
                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            TextBox tbxMgmResponce = (TextBox)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = false;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = false;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = false;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;


                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = false;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                    }
                }
                else
                {
                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        chkBx.Enabled = true;

                        Label lblResultID = (Label)rw.FindControl("lblResultID");
                        Label lblVerticalId = (Label)rw.FindControl("lblVerticalId");
                        Label lblScheduledOnID = (Label)rw.FindControl("lblScheduledOnID");
                        Label lblCustomerBranchID = (Label)rw.FindControl("lblCustomerBranchID");
                        Label lblAuditID = (Label)rw.FindControl("lblAuditID");
                        var implementationdetails = GetImplementationAuditResultbyID(Convert.ToInt32(lblScheduledOnID.Text), Convert.ToInt32(lblResultID.Text), Convert.ToInt32(lblCustomerBranchID.Text), Convert.ToInt32(lblVerticalId.Text), Convert.ToInt64(lblAuditID.Text));
                        if (implementationdetails != null)
                        {
                            TextBox tbxMgmResponce = (TextBox)rw.FindControl("tbxMgmResponce");
                            if (implementationdetails.ManagementResponse != "")
                            {
                                tbxMgmResponce.Text = implementationdetails.ManagementResponse;
                            }
                            tbxMgmResponce.Enabled = true;

                            DropDownList ddlPersonResponsible = (DropDownList)rw.FindControl("ddlPersonResponsible");
                            ddlPersonResponsible.Enabled = true;
                            if (ddlPersonResponsible != null)
                            {
                                ddlPersonResponsible.DataTextField = "Name";
                                ddlPersonResponsible.DataValueField = "ID";
                                ddlPersonResponsible.DataSource = details;
                                ddlPersonResponsible.DataBind();
                                ddlPersonResponsible.Items.Insert(0, new ListItem("Select Person Responsible", "-1"));
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.PersonResponsible)))
                            {
                                ddlPersonResponsible.SelectedValue = Convert.ToString(implementationdetails.PersonResponsible);
                            }

                            TextBox txtStartDate = (TextBox)rw.FindControl("txtStartDate");
                            txtStartDate.Enabled = true;
                            txtStartDate.Text = implementationdetails.TimeLine != null ? implementationdetails.TimeLine.Value.ToString("dd-MM-yyyy") : null;

                            DropDownList ddlIMPStatus = (DropDownList)rw.FindControl("ddlIMPStatus");
                            ddlIMPStatus.Enabled = true;
                            if (!string.IsNullOrEmpty(Convert.ToString(implementationdetails.ImplementationStatus)))
                            {
                                ddlIMPStatus.SelectedValue = Convert.ToString(implementationdetails.ImplementationStatus);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void lbkSave_Click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {
                    string ManagementResponse = string.Empty;
                    string TimeLineValue = null;
                    string PersonResponsible = "-1";
                    string FinalStatusIMP = "-1";

                    foreach (GridViewRow rw in grdSummaryDetailsAuditCoverageIMP.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        if (chkBx != null && chkBx.Checked)
                        {
                            long roleid = 4;
                            DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            ManagementResponse = ((TextBox)rw.FindControl("tbxMgmResponce")).Text;
                            TimeLineValue = ((TextBox)rw.FindControl("txtStartDate")).Text;
                            PersonResponsible = ((DropDownList)rw.FindControl("ddlPersonResponsible")).SelectedValue;
                            FinalStatusIMP = ((DropDownList)rw.FindControl("ddlIMPStatus")).SelectedValue;

                            if (FinalStatusIMP != "" && FinalStatusIMP != "-1")
                            {
                                LinkButton chklnkbutton = (LinkButton)rw.FindControl("btnChangeStatus");
                                String Args = chklnkbutton.CommandArgument.ToString();
                                string[] arg = Args.ToString().Split(',');

                                long ResultID = Convert.ToInt32(arg[0]);
                                long CustomerbranchID = Convert.ToInt32(arg[1]);
                                string FinancialYear = arg[2].ToString();
                                string ForMonth = arg[3].ToString();
                                long VerticalID = Convert.ToInt32(arg[5]);
                                long Scheduledonid = Convert.ToInt32(arg[6]);
                                long AuditID = Convert.ToInt32(arg[7]);
                                ViewState["resultID"] = null;
                                ViewState["resultID"] = ResultID;
                                var getImpAuditScheduleOnByDetails = RiskCategoryManagement.GetAuditImpementationScheduleOnDetailsNew(FinancialYear, ForMonth, Convert.ToInt32(CustomerbranchID), Convert.ToInt32(VerticalID), Convert.ToInt32(Scheduledonid), AuditID);
                                if (getImpAuditScheduleOnByDetails != null)
                                {
                                    if (ddlSaveStatus.SelectedValue != "")
                                    {
                                        int StatusID = 0;
                                        if (ddlSaveStatus.Visible)
                                            StatusID = Convert.ToInt32(ddlSaveStatus.SelectedValue);
                                        else
                                            StatusID = Convert.ToInt32(ComplianceManagement.Business.RiskCategoryManagement.GetClosedTransaction(Convert.ToInt32(getImpAuditScheduleOnByDetails.Id)).AuditStatusID);

                                        #region
                                        ImplementationAuditResult MstRiskResult = new ImplementationAuditResult()
                                        {
                                            AuditScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = getImpAuditScheduleOnByDetails.FinancialYear,
                                            ForPeriod = getImpAuditScheduleOnByDetails.ForMonth,
                                            CustomerBranchId = CustomerbranchID,
                                            IsDeleted = false,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };
                                        AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                                        {
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                            ImplementationScheduleOnID = getImpAuditScheduleOnByDetails.Id,
                                            FinancialYear = FinancialYear,
                                            CustomerBranchId = CustomerbranchID,
                                            ImplementationInstance = getImpAuditScheduleOnByDetails.ImplementationInstance,
                                            ForPeriod = ForMonth,
                                            ResultID = ResultID,
                                            RoleID = roleid,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VerticalID = (int?)VerticalID,
                                            ProcessId = getImpAuditScheduleOnByDetails.ProcessId,
                                            AuditID = AuditID,
                                        };

                                        if (ManagementResponse.Trim() == "")
                                            MstRiskResult.ManagementResponse = "";
                                        else
                                            MstRiskResult.ManagementResponse = ManagementResponse.Trim();

                                        if (PersonResponsible == "-1")
                                        {
                                            MstRiskResult.PersonResponsible = null;
                                            transaction.PersonResponsible = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                            transaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                                        }
                                        if (FinalStatusIMP == "-1")
                                        {
                                            MstRiskResult.ImplementationStatus = null;
                                            transaction.ImplementationStatusId = null;
                                        }
                                        else
                                        {
                                            MstRiskResult.ImplementationStatus = Convert.ToInt32(FinalStatusIMP);
                                            transaction.ImplementationStatusId = Convert.ToInt32(FinalStatusIMP);
                                        }
                                        DateTime dt = new DateTime();
                                        if (!string.IsNullOrEmpty(TimeLineValue.Trim()))
                                        {
                                            dt = DateTime.ParseExact(TimeLineValue.Trim(), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            MstRiskResult.TimeLine = dt.Date;
                                        }
                                        else
                                            MstRiskResult.TimeLine = null;

                                        string remark = string.Empty;
                                        if (ddlSaveStatus.SelectedItem.Text == "Closed")
                                        {
                                            transaction.StatusId = 3;
                                            MstRiskResult.Status = 3;
                                            remark = "Implementation Step Closed.";
                                        }
                                        else if (ddlSaveStatus.SelectedItem.Text == "Team Review")
                                        {
                                            transaction.StatusId = 4;
                                            MstRiskResult.Status = 4;
                                            remark = "Implementation Step Under Team Review.";
                                        }
                                        else if (ddlSaveStatus.SelectedItem.Text == "Final Review")
                                        {
                                            transaction.StatusId = 5;
                                            MstRiskResult.Status = 5;
                                            remark = "Implementation Step Under Final Review.";
                                        }
                                        else if (ddlSaveStatus.SelectedItem.Text == "Auditee Review")
                                        {
                                            transaction.StatusId = 6;
                                            MstRiskResult.Status = 6;
                                            remark = "Implementation Step Under Auditee Review.";
                                        }
                                        transaction.Remarks = remark.Trim();
                                        #endregion

                                        var MstRiskResultchk = RiskCategoryManagement.GetImplementationAuditResultIMPlementationID(Convert.ToInt32(ResultID), Convert.ToInt32(VerticalID), Convert.ToInt32(getImpAuditScheduleOnByDetails.Id), AuditID);
                                        if (MstRiskResultchk != null)
                                        {
                                            #region
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ProcessWalkthrough))
                                            {
                                                MstRiskResult.ProcessWalkthrough = null;
                                            }
                                            else
                                            {
                                                MstRiskResult.ProcessWalkthrough = MstRiskResultchk.ProcessWalkthrough;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.ActualWorkDone))
                                            {
                                                MstRiskResult.ActualWorkDone = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.ActualWorkDone = MstRiskResultchk.ActualWorkDone;
                                            }

                                            if (string.IsNullOrEmpty(MstRiskResultchk.Population))
                                            {
                                                MstRiskResult.Population = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Population = MstRiskResultchk.Population;
                                            }
                                            if (string.IsNullOrEmpty(MstRiskResultchk.Sample))
                                            {
                                                MstRiskResult.Sample = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.Sample = MstRiskResultchk.Sample;
                                            }

                                            if (string.IsNullOrEmpty(MstRiskResultchk.FixRemark))
                                            {
                                                MstRiskResult.FixRemark = "";
                                            }
                                            else
                                            {
                                                MstRiskResult.FixRemark = MstRiskResultchk.FixRemark;
                                            }
                                            #endregion
                                        }
                                        
                                        bool Success1 = false;
                                        bool Success2 = false;
                                        bool isvalidate = true;
                                        string msg = string.Empty;
                                        if (string.IsNullOrEmpty(MstRiskResult.ManagementResponse))
                                        {
                                            isvalidate = false;
                                            msg += "Management Response,";
                                        }
                                        if (MstRiskResult.PersonResponsible == null)
                                        {
                                            isvalidate = false;
                                            msg += "Person Responsible,";
                                        }
                                        if (MstRiskResult.TimeLine == null)
                                        {
                                            isvalidate = false;
                                            msg += "Time Line";
                                        }
                                        if (MstRiskResult.ImplementationStatus == null)
                                        {
                                            isvalidate = false;
                                            msg += "Implementation Status";
                                        }
                                        if (isvalidate)
                                        {
                                            #region
                                            if (RiskCategoryManagement.ImplementationAuditResultExists(MstRiskResult))
                                            {
                                                if (!string.IsNullOrEmpty(ddlFilterStatus.SelectedValue))
                                                {
                                                    if (ddlFilterStatus.SelectedValue == "5")
                                                    {
                                                        if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                                        {
                                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResultResult(MstRiskResult, 5);
                                                        }
                                                        else
                                                        {
                                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.CreatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                                        {
                                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                                        }
                                                        else
                                                        {
                                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                            MstRiskResult.CreatedOn = DateTime.Now;
                                                            Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (RiskCategoryManagement.ImplementationAuditResultExistsWithStatus(MstRiskResult))
                                                    {
                                                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        MstRiskResult.UpdatedOn = DateTime.Now;
                                                        Success1 = RiskCategoryManagement.UpdateImplementationAuditResult(MstRiskResult);
                                                    }
                                                    else
                                                    {
                                                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                        MstRiskResult.CreatedOn = DateTime.Now;
                                                        Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateImplementationAuditResult(MstRiskResult);
                                            }
                                            if (RiskCategoryManagement.AuditImplementationTransactionExists(transaction))
                                            {
                                                if (Convert.ToInt32(ddlFilterStatus.SelectedValue) == 4 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 5 || Convert.ToInt32(ddlFilterStatus.SelectedValue) == 6)
                                                {
                                                    transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.CreatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                                }
                                                else
                                                {
                                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    transaction.UpdatedOn = DateTime.Now;
                                                    Success2 = RiskCategoryManagement.UpdateAuditImplementationTransactionTxnStatusReviewer(transaction);
                                                }
                                            }
                                            else
                                            {
                                                transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                transaction.CreatedOn = DateTime.Now;
                                                Success2 = RiskCategoryManagement.CreateAuditImplementationTransaction(transaction);
                                            }
                                            if (Success1 == true && Success2 == true)
                                            {
                                                #region
                                                if (FinalStatusIMP == "3")
                                                {
                                                    RiskCategoryManagement.UpdateImplementationAuditResultISACTIVE(Convert.ToInt32(ViewState["resultID"]), AuditID);
                                                    RiskCategoryManagement.UpdateAuditImplementationTransactionISACTIVE(Convert.ToInt32(ViewState["resultID"]), AuditID);
                                                    cvDuplicateEntry.ErrorMessage = "Implementation Steps Closed Successfully";
                                                }
                                                else
                                                {
                                                    if (ddlSaveStatus.SelectedItem.Text == "Closed")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Implementation Steps Closed Successfully";
                                                    }
                                                    else if (ddlSaveStatus.SelectedItem.Text == "Team Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Implementation Steps Submitted Successfully";
                                                    }
                                                    else if (ddlSaveStatus.SelectedItem.Text == "Final Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Implementation Steps Submitted Successfully";
                                                    }
                                                    else if (ddlSaveStatus.SelectedItem.Text == "Auditee Review")
                                                    {
                                                        cvDuplicateEntry.ErrorMessage = "Implementation Steps Submitted Successfully";
                                                    }
                                                }
                                                #endregion
                                                BindDetailView(ViewState["Arguments"].ToString(), "P");
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Please Provide " + msg.Trim(',') + " Before Save";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Select Status";
                                    }
                                }//getImpAuditScheduleOnByDetails
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Implementation Status";
                                break;
                            }
                        }
                    } //For Each Loop End
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}