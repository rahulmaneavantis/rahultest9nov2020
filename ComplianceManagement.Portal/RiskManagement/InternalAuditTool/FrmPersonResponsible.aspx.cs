﻿using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class FrmPersonResponsible : System.Web.UI.Page
    {             
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowProcessGrid(sender, e);

                //BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                GetPageDisplaySummary();
            }         
        }

        public void BindGrid(int Userid)
        {
            if (ViewState["ActiveFlag"] != null)
            {
                if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                {
                    var records = Business.DashboardManagementRisk.GetFileDataPersonResponsibleObservationStatus(Userid);
                    grdSummaryDetailsAuditCoverage.DataSource = records;
                    Session["TotalRows"] = records.Count;
                    grdSummaryDetailsAuditCoverage.DataBind();                    
                }
                else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                {
                    var records = Business.DashboardManagementRisk.GetFileDataPersonResponsibleObservationIMPelementationStatus(Userid);
                    grdSummaryDetailsAuditCoverageIMP.DataSource = records;
                    Session["TotalRows"] = records.Count;
                    grdSummaryDetailsAuditCoverageIMP.DataBind();                    
                }
            }             
        }

        protected void Previous_Click(object sender, ImageClickEventArgs e)
        {
            try
            {              
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {         
                    if(ViewState["ActiveFlag"]!=null)
                    {
                        SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }                    
                }
                
                //Reload the Grid
                BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void Next_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (ViewState["ActiveFlag"] != null)
                {
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo + 1).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }

                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }

                    //Reload the Grid
                    BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSummaryDetailsAuditCoverage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSummaryDetailsAuditCoverage.PageIndex = e.NewPageIndex;
            grdSummaryDetailsAuditCoverage.DataSource = Business.DashboardManagementRisk.GetFileDataPersonResponsibleObservationStatus(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            grdSummaryDetailsAuditCoverage.DataBind();   
        }

        protected void grdSummaryDetailsAuditCoverageIMP_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSummaryDetailsAuditCoverageIMP.PageIndex = e.NewPageIndex;
            grdSummaryDetailsAuditCoverageIMP.DataSource = Business.DashboardManagementRisk.GetFileDataPersonResponsibleObservationIMPelementationStatus(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            grdSummaryDetailsAuditCoverageIMP.DataBind();
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int atbdid = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    atbdid = Convert.ToInt32(commandArgs[0]);
                    ViewState["ATBDID"] = null;
                    ViewState["ATBDID"] = atbdid;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[4]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + atbdid + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "',"+ VerticalId + ");", true);                
                BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        {
            try
            {
                int atbdid = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    atbdid = Convert.ToInt32(commandArgs[0]);
                    ViewState["ResultID"] = null;
                    ViewState["ResultID"] = atbdid;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[4]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowImpDialog(" + atbdid + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "'," + VerticalId + ");", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }   
     
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ActiveFlag"] != null)
                {
                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo <= GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();

                        if (Convert.ToString(ViewState["ActiveFlag"]) == "P")
                        {
                            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                        else if (Convert.ToString(ViewState["ActiveFlag"]) == "I")
                        {
                            grdSummaryDetailsAuditCoverageIMP.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                            grdSummaryDetailsAuditCoverageIMP.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                        }
                    }

                    //Reload the Grid
                    BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }      

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["ActiveFlag"] = "I";
            ProcessGrid.Visible = false;
            ImplementationGrid.Visible = true;
           

            BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["ActiveFlag"] = "P";
            ProcessGrid.Visible = true;
            ImplementationGrid.Visible = false;
            BindGrid(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
        }
    }
}