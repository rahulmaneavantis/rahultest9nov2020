﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class frmAuditFrequency : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindCustomer();
                BindFilterCustomer();
                BindAuditorList(-1);
                GetPageDisplaySummary();
            }
        }
        
        private void BindFilterCustomer()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlFilterLocation.DataTextField = "Name";
                ddlFilterLocation.DataValueField = "ID";
                ddlFilterLocation.Items.Clear();
                ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlFilterLocation.DataBind();
                ddlFilterLocation.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                BindAuditorList(Convert.ToInt32(ddlFilterLocation.SelectedValue));
            }
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        public string GetStatusName(int rating)
        {
            string processnonprocess = "";
            if (rating != -1)
            {
                if (rating ==1)
                {
                    processnonprocess="High";
                }
                else if (rating == 2)
                {
                    processnonprocess="Medium";
                }
                else
                {
                    processnonprocess = "Low";
                }
            }
            return processnonprocess;
        }
        public string GetCustomerBranchName(long branchid)
        {
            string processnonprocess = "";
            if (branchid != -1)
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    int a = Convert.ToInt32(branchid);
                    mst_CustomerBranch mstcustomerbranch = (from row in entities.mst_CustomerBranch
                                                            where row.ID == a && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                    processnonprocess = mstcustomerbranch.Name;
                }
            }
            return processnonprocess;
        }

        
        private void BindAuditorList(int Branchid)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var AuditorMasterList = ProcessManagement.GetAllAuditFrequency(customerID, Branchid);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLocation.DataTextField = "Name";
            ddlLocation.DataValueField = "ID";
            ddlLocation.Items.Clear();
            ddlLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem(" Select Location ", "-1"));
        }
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Mst_AuditFrequency Mstauditfrequency = new Mst_AuditFrequency()
                {
                    Customerbranchid = Convert.ToInt32(ddlLocation.SelectedValue),
                    Rating = Convert.ToInt32(ddlRating.SelectedValue),
                    Frequency = Convert.ToString(ddlAuditFrequency.SelectedItem.Text),
                    IsActive = false
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    Mstauditfrequency.ID = Convert.ToInt32(ViewState["ObservationID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.AuditFrequencyExists(Mstauditfrequency.Rating, Mstauditfrequency.Customerbranchid))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Audit Frequency Already Exists";
                        return;
                    }
                    else
                    {
                        ProcessManagement.CreateAuditFrequency(Mstauditfrequency);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Audit Frequency Add Succssfully";
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    ProcessManagement.UpdateAuditFrequency(Mstauditfrequency);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Audit Frequency Updated Succssfully";
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAuditFrequencyDialog\").dialog('close')", true);
                //BindAuditorList(Convert.ToInt32(ddlLocation.SelectedValue));
                BindAuditorList(Convert.ToInt32(ddlFilterLocation.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlRating.SelectedValue = "-1";
                ddlAuditFrequency.SelectedValue = "-1";
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditFrequencyDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //var AuditorMasterList = ProcessManagement.GetAllAuditFrequencyList();
                //if (direction == SortDirection.Ascending)
                //{
                //    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //    ViewState["SortOrder"] = "Asc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //else
                //{
                //    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //    ViewState["SortOrder"] = "Desc";
                //    ViewState["SortExpression"] = e.SortExpression.ToString();
                //}
                //foreach (DataControlField field in grdAuditor.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdAuditor.Columns.IndexOf(field);
                //    }
                //}
                //grdAuditor.DataSource = AuditorMasterList;
                //grdAuditor.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
               //int ObservationID = Convert.ToInt32(e.CommandArgument);
                int ObservationID = -1;
                int CustomerBranchId = -1;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (commandArgs.Length > 1)
                {
                    if (!string.IsNullOrEmpty(commandArgs[0]))
                    {
                        ObservationID = Convert.ToInt32(commandArgs[0]);
                    }
                    if (!string.IsNullOrEmpty(commandArgs[1]))
                    {
                        CustomerBranchId = Convert.ToInt32(commandArgs[1]);
                    }
                }
                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ObservationID"] = ObservationID;
                    ViewState["BranchID"] = null;

                    Mst_AuditFrequency RPD = ProcessManagement.Mst_AuditFrequencyGetByID(ObservationID);
                    if (!string.IsNullOrEmpty(Convert.ToString(RPD.Customerbranchid)))
                    {
                        ddlLocation.SelectedValue = Convert.ToString(RPD.Customerbranchid);
                        ViewState["BranchID"] = RPD.Customerbranchid;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(RPD.Rating)))
                    {
                        ddlRating.SelectedValue = Convert.ToString(RPD.Rating);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(RPD.Frequency)))
                    {
                        ddlAuditFrequency.SelectedItem.Text = Convert.ToString(RPD.Frequency);
                    }
                    //txtFName.Text = RPD.Name;
                    upPromotor.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditFrequencyDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_PROMOTER"))
                {

                    ProcessManagement.DeleteAuditFrequency(ObservationID);
                    int CustomerBranchIdd = -1;
                    BindAuditorList(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                    //upPromotor.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAuditor.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(ViewState["BranchID"].ToString()))
                {
                    BindAuditorList(Convert.ToInt32(ViewState["BranchID"]));
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdAuditor_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    //if (e.Row.RowType == DataControlRowType.Header)
        //    //{
        //    //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //    //    if (sortColumnIndex != -1)
        //    //    {
        //    //        AddSortImage(sortColumnIndex, e.Row);
        //    //    }
        //    //}
        //}
        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}


        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void ddlRating_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindGrid();
                BindAuditorList(-1);
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindGrid();
                BindAuditorList(-1);
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                //BindGrid();
                BindAuditorList(-1);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }        
    }
}