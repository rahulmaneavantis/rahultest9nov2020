﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool
{
    public partial class InternalAuditStatusUI : System.Web.UI.Page
    {
        int Statusid;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        public static List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        protected string customerName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            roles = CustomerManagementRisk.GetAssignedRolesICFR(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                // CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //added by sagar on 14-07-2020
                if (!String.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    GetCustomerNameFromQueryStringId(Request.QueryString["CustomerID"]);
                }
                BindCustomerList();
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;
                    }
                }
                BindFinancialYear();
                BindLegalEntityData();
                if (!string.IsNullOrEmpty(Request.QueryString["RoleID"]))
                {
                    if (Request.QueryString["RoleID"] == "3")
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (Request.QueryString["RoleID"] == "4")
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdInternalAudits.PageIndex = chkSelectedPage - 1;
            grdInternalAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData("P");
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));

            if (ddlFinancialYear.SelectedValue == "-1" || ddlFinancialYear.SelectedValue == "")
            {
                if (ddlFinancialYear.Items.Count > 0)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.Items.FindByText(DateTimeExtensions.GetCurrentFinancialYear(DateTime.Now.Date)).Selected = true;
                }
            }
        }

        public void BindLegalEntityData()
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityDataICFR(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else
            {
                BindData("N");
            }
            bindPageNumber();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdInternalAudits.DataSource = null;
                grdInternalAudits.DataBind();
                BindData("P");
                bindPageNumber();
            }
            else
            {
                grdInternalAudits.DataSource = null;
                grdInternalAudits.DataBind();
                BindData("N");
                bindPageNumber();
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");

            bindPageNumber();
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                BindData("P");
            else
                BindData("N");
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdInternalAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //Reload the Grid
                BindData("P");
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdInternalAudits.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindData(string Flag)
        {
            try
            {
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                #region added by sagar on 15-07-2020
                if (!String.IsNullOrEmpty(Request.QueryString["CustomerID"]) && CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(Request.QueryString["CustomerID"]);
                } 
                #endregion
                int roleid = -1;
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ViewState["Status"].ToString()))
                {
                    Statusid = Convert.ToInt32(ViewState["Status"]);
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }
                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();
                if (PerformerFlag)
                    roleid = 3;

                else if (ReviewerFlag)
                    roleid = 4;


                if (Statusid == 1)
                {
                    var result = DashboardManagementRisk.GetInternalOpenAudits(Branchlist.ToList(), FinYear, Period, Portal.Common.AuthenticationHelper.UserID, roleid);
                    var AuditLists = result.Where(c => c.CustomerID == CustomerId).Distinct().ToList();
                    grdInternalAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdInternalAudits.DataBind();
                }
                else if (Statusid == 3)
                {
                    var result = DashboardManagementRisk.GetInternalClosedAudits(Branchlist.ToList(), FinYear, Period, Portal.Common.AuthenticationHelper.UserID, roleid);
                    var AuditLists = result.Where(c => c.CustomerID == CustomerId).Distinct().ToList();
                    grdInternalAudits.DataSource = AuditLists;
                    Session["TotalRows"] = AuditLists.Count;
                    grdInternalAudits.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdInternalAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (PerformerFlag)
                {
                    e.Row.Cells[5].Text = "Performer";

                }
                else if (ReviewerFlag)
                {

                    e.Row.Cells[5].Text = "Reviewer";
                }
            }
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{

            //String GridStatus = e.Row.Cells[6].Text;

            //Label lblPerformer = (Label)e.Row.FindControl("lblPerformer");
            ////Label lblReviewer = (Label)e.Row.FindControl("lblReviewer");               
            //if (PerformerFlag)
            //{
            //    lblPerformer.Visible = true;
            //    //lblReviewer.Visible = false;
            //    e.Row.Cells[6].Visible = false;
            //    e.Row.Cells[5].Visible = true; 
            //}
            //else if (ReviewerFlag)
            //{
            //    lblPerformer.Visible = false;
            //    //lblReviewer.Visible = true;
            //    e.Row.Cells[6].Visible = true;
            //    e.Row.Cells[5].Visible = false;
            //}   

            //Label chklabl = (Label)e.Row.FindControl("lblProcess");
            //string chk = chklabl.Text.ToString();
            //if (chk.Equals("Total"))
            //{
            //    e.Row.Font.Bold = true;
            //    e.Row.Cells[0].Text = "";
            //}
            //}
        }

        protected void grdInternalAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                        string ForMonth = Convert.ToString(commandArgs[1]);
                        string FinancialYear = Convert.ToString(commandArgs[2]);
                        int RoleID = Convert.ToInt32(commandArgs[3]);
                        int UserID = Convert.ToInt32(commandArgs[4]);
                        int CustomerID = Convert.ToInt32(commandArgs[5]);
                        var AuditInstanceIDs = UserManagementRisk.GetAuditInstanceID(CustomerBranchID, FinancialYear, ForMonth, RoleID, UserID);
                        if (AuditInstanceIDs.Count > 0)
                        {
                            Session["ListOfAuditInstance"] = null;
                            Session["ListOfAuditInstance"] = AuditInstanceIDs;

                            string url = HttpContext.Current.Request.Url.AbsolutePath;
                            if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                            {
                                Response.Redirect("~/RiskManagement/InternalAuditTool/InternalAuditStatusSummary.aspx?Role=" + RoleID + "&Period=" + ForMonth + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&CustomerID=" + CustomerID, false);
                            }
                            else
                            {
                                Response.Redirect("~/RiskManagement/InternalAuditTool/InternalAuditStatusSummary.aspx?Role=" + RoleID + "&Period=" + ForMonth + "&ReturnUrl1=" + "&CustomerID=" + CustomerID, false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected bool CheckEnable(String Role, String SDate, String EDate)
        {
            if (Role != "Performer")
                return false;
            else if ((SDate == "" || EDate == "") && Role == "Performer")
                return true;
            else if (SDate != "" && EDate != "")
                return false;
            else
                return true;
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            BindData("P");
            bindPageNumber();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            BindData("P");
            bindPageNumber();
        }
        protected void upAuditSummaryDetails_Load(object sender, EventArgs e)
        {
        }

        // need to review code from sushant
        private void BindCustomerList()
        {
            long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            var customerList = RiskCategoryManagement.GetAllMappedCustomerForIFCPerformer(userId);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Customer", "-1"));
                if (!string.IsNullOrEmpty(customerName))
                {
                    ddlCustomer.SelectedItem.Text = customerName;
                }
                else
                {
                    ddlCustomer.SelectedIndex = 2;
                }
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();


            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    BindLegalEntityData();
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindData("P");
                    }
                    else
                    {
                        BindData("N");
                    }

                }
                else
                {
                    ddlLegalEntity.DataSource = null;
                    ddlLegalEntity.DataBind();
                    ddlLegalEntity.Items.Clear();

                    ddlSubEntity1.DataSource = null;
                    ddlSubEntity1.DataBind();
                    ddlSubEntity1.Items.Clear();

                    ddlSubEntity2.DataSource = null;
                    ddlSubEntity2.DataBind();
                    ddlSubEntity2.Items.Clear();

                    ddlSubEntity3.DataSource = null;
                    ddlSubEntity3.DataBind();
                    ddlSubEntity3.Items.Clear();

                    ddlFilterLocation.DataSource = null;
                    ddlFilterLocation.DataBind();
                    ddlFilterLocation.Items.Clear();

                    grdInternalAudits.DataSource = null;
                    grdInternalAudits.DataBind();

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Customer.";
                }
            }
        }


        // added by sagar on 14-07-2020
        private void GetCustomerNameFromQueryStringId(string customerIdFromQueryString)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int custId = Convert.ToInt32(customerIdFromQueryString);
                long userId = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                var customerList = RiskCategoryManagement.GetAllMappedCustomerForIFCPerformer(userId);
                var name = customerList.Where(c => c.CustomerId == custId).FirstOrDefault();
                customerName = name.CustomerName;
            }
        }
    }
}