﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class docviewerPath : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    string filepath = Request.QueryString["docurl"].ToString();
                    if (filepath != "undefined")
                    {
                        doccontrol.Document = Request.QueryString["docurl"].ToString();
                    }
                    else
                    {
                        //lblMessage.Text = "Sorry File not find";
                    }
                }
            }
        }
    }
}