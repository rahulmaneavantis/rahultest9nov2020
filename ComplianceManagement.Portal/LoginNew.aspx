﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.LoginNew" %>

<!DOCTYPE html>

<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="style/CSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="style/CSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="style/CSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="CSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="style/CSS/style.css" rel="stylesheet" />
    <link href="style/CSS/style-responsive.css" rel="stylesheet" />



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
    <div class="container">
        <form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">
            <asp:Panel ID="Panel2" runat="server" DefaultButton="Submit">
                <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <img src="Images/avantil-logo.png" />
                            </p>
                        </div>
                        <div class="login-wrap">
                            <asp:CustomValidator ID="cvLogin" EnableClientScript="False" runat="server" Display="None" />
                            <asp:ValidationSummary ID="vsLogin" runat="server" CssClass="vdsummary" Style="margin-left: 10px; color: Red" />
                            <h1>Sign in</h1>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon_profile"></i></span>
                                <%--<input type="text" id="txtemail" class="form-control" runat="server" placeholder="Username" autofocus="" />--%>
                                <asp:TextBox ID="txtemail" CssClass="form-control" runat="server" placeholder="Username" />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon_lock"></i></span>
                                <%-- <input type="password" class="form-control" placeholder="Password">--%>
                                <asp:TextBox ID="txtpass" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password" />
                            </div>
                            <asp:LinkButton ID="lbtResetPassword" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                                Text="Forgot your password?" OnClick="lbtResetPassword_Click" CssClass="pull-left"></asp:LinkButton>
                            <%--  <span class="pull-left"><a href="login-resetpassword.html">Forgot your password?</a></span>--%>
                            <div class="clearfix"></div>
                        
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="login-form-captcha">
                                    <%-- <asp:TextBox ID="txtcode" CssClass="form-control" runat="server" placeholder="Enter Code"></asp:TextBox>--%>
                                    <div class="g-recaptcha" data-sitekey="6LdHaw0UAAAAABbVAmqGINbZdkH9GANgNr_uMISc"></div>
                                    <cc1:GoogleReCaptcha ID="ctrlGoogleReCaptcha" runat="server" PublicKey="6LdHaw0UAAAAABbVAmqGINbZdkH9GANgNr_uMISc" PrivateKey="6LdHaw0UAAAAACUSQK2kgIy8RaczRGb6ws5uHn1g" />
                                </div>
                                <asp:Panel ID="Panel1" runat="server">
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clearfix"></div>
                  
                            <asp:Button ID="Submit" CssClass="btn btn-primary btn-lg btn-block" Text="Sign in" runat="server" OnClick="Submit_Click"></asp:Button>
                    </div>
                        <%-- <button class="btn btn-primary btn-lg btn-block" type="submit">Sign in</button>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="js/bootstrap.min.js"></script>
    <script type='text/javascript' src="js/google1113_jquery.min.js"></script>
</body>
</html>
