﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class PracticeManagement : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;
        protected bool user_Audit;
        protected bool PageAutherzation;
        protected bool DeptHead = false;
        protected bool showLicenseManagement = false;

        protected bool showCADMNTOManagement;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                customerid =Convert.ToInt32(AuthenticationHelper.CustomerID);
                userid = AuthenticationHelper.UserID;              
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                showCADMNTOManagement = UserManagement.CheckIsServiceProviderID(AuthenticationHelper.UserID);
                if (Session["LastLoginTime"] != null)
                {
                    LastLoginDate = Session["LastLoginTime"].ToString();
                }
                if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                {
                    if (AuthenticationHelper.UserID != -1)
                    {                        
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                        if (cname != null)
                        {
                            CustomerName = cname;
                        }
                    }
                }                             
                user_Roles = AuthenticationHelper.Role;               
                if (LoggedUser.ImagePath != null)
                {
                    ProfilePic.Src = LoggedUser.ImagePath;
                    ProfilePicTop.Src = LoggedUser.ImagePath;
                    ProfilePicSide.Src = LoggedUser.ImagePath;
                }
                else
                {
                    ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upDivLocation_Load(object sender, EventArgs e)
        {            
        }
    }
}