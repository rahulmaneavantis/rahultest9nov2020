﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/EntityUserAssignmentAPI")]
    public class EntityUserAssignmentAPIController : ApiController
    {
        IEntityuserAssignment obj;
        public EntityUserAssignmentAPIController(IEntityuserAssignment obj)
        {
            this.obj = obj;
        }

        [Route("GetEntityAssignment")]
        [HttpGet]
        public DataSourceResult GetEntityAssignment([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetEntityAssignment(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetUser")]
        [HttpGet]
        public IEnumerable<UserforDropdown> GetUser(string Role,int CustomerId)
        {
            return obj.GetUser(Role, CustomerId);
        }

        [Route("GetUserforTask")]
        [HttpGet]
        public IEnumerable<UserforDropdown> GetUserforTask(string Role,int CustomerId,int UserId)
        {
            return obj.GetUserforTask(Role, CustomerId, UserId);
        }

        [Route("DeleteEntityuserAssignment")]
        [HttpDelete]
        public HttpResponseMessage DeleteEntityuserAssignment(int Id,int EntityId)
        {

            HttpResponseMessage response = null;
            if (Id > 0)
            {
                bool result = obj.DeleteEntityAssignment(Id, EntityId);
                if(result)
                {
                   response = Request.CreateResponse(HttpStatusCode.OK); 
                }
                else
                {
                     response = Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }
    }
}
