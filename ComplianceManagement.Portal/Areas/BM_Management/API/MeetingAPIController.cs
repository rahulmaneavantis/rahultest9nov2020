﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/MeetingAPI")]
    public class MeetingAPIController : ApiController
    {
        IMeeting_Service obj;
        public MeetingAPIController(IMeeting_Service obj)
        {
            this.obj = obj;
        }

        [Route("GetCompletedMeetings")]
        [HttpGet]
        public DataSourceResult GetCompletedMeetings([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int entityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetCompletedMeetings(entityId, customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData == null ? 0 : gridData.Count
            };

            return result;
        }

        [HttpGet]
        [Route("GetAvailabilityResponse")]
        public DataSourceResult GetAvailabilityResponse([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long MeetingID)
        {
            //int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            //var gridData = obj.GetAvailabilityResponse(MeetingID, customerId);
            //var result = new DataSourceResult()
            //{
            //    Data = gridData,
            //    Total = gridData.Count
            //};
            //return result;
            return null;
        }

        [Route("GetDraftCommitteeMeeting")]
        [HttpGet]
        public IEnumerable<PreCommiitteeDraftMeetingVM> GetDraftCommitteeMeeting(int PreCommitteeId, int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return obj.GetDraftCommitteeMeeting(PreCommitteeId, EntityId, customerId);
        }

        [Route("GetMeetingType")]
        [HttpGet]
        public IEnumerable<CommitteeCompVM> GetMeetingType(string Type)
        {
            var result = obj.GetAllMeetingType(Type);
            return result;
        }

        [Route("MeetingNoticeAgendaLog")]
        [HttpGet]
        public DataSourceResult GetMeetingsNoticeLog([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int meetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetNoticeAgendaLog(meetingId, customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData == null ? 0 : gridData.Count
            };

            return result;
        }

        [Route("MeetingNoticeAgendaLogDetails")]
        [HttpGet]
        public HttpResponseMessage GetAgendaItemInfo(long id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response = Request.CreateResponse(HttpStatusCode.OK, obj.GetNoticeAgendaLogDetails(id));
            return response;
        }

        [HttpGet]
        [Route("Agenda_SummitResponse")]
        public DataSourceResult Agenda_SummitResponse([DataSourceRequest] DataSourceRequest request, long MeetingId, string ViewType)
        {
            //return PartialView("_AgendaResponseSummite");
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            List<Agenda_SummitResponse> gridData = new List<BM_ManegmentServices.VM.Agenda_SummitResponse>();
            int? userId = null;
            string role = AuthenticationHelper.Role;
            int? RoleId;
            if(role=="CS")
            {
                RoleId = 20;
            }
            else
            {
                RoleId = 0;
            }
            //if (ViewType == "Participant")
            //{
                userId = Convert.ToInt32(AuthenticationHelper.UserID);
                
            //}
            gridData = obj.GetAgendaResponse(MeetingId, customerId, userId, RoleId);

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;

        }

        [HttpGet]
        [Route("Agenda_Document")]
        public DataSourceResult Agenda_Document([DataSourceRequest] DataSourceRequest request, long AgendaMappingID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            List<VM_AgendaDocument> gridData = new List<VM_AgendaDocument>();

            gridData = obj.GetAgendaDocument(AgendaMappingID, customerId);

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
