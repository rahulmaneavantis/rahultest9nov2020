﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/NotesAPI")]
    public class NotesAPIController : ApiController
    {
        INote _objinote;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        public NotesAPIController(INote obj)
        {
            _objinote = obj;
        }


        [Route("GetNotes")]
        [HttpGet]
        public DataSourceResult GetEntity([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = _objinote.GetAllNotes(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateNotes")]
        [HttpPost]
        public IHttpActionResult CreateNotes(VMNotes _objnotes)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string result = string.Empty;
            if (ModelState.IsValid)
            {
                 result = _objinote.AddNewNotes(_objnotes, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(result);

        }

        [Route("UpdateNotes")]
        [HttpPut]
        public IHttpActionResult UpdateNotes(VMNotes _objnotes)
        {
            string result = string.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objnotes = _objinote.UpdateNotes(_objnotes);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(result);

        }

        [Route("DeleteNotes")]
        [HttpDelete]
        public IHttpActionResult DeleteNotes(VMNotes _objnotes)
        {
            string result = string.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                 result = _objinote.DeleteNotes(_objnotes, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(result);

        }


        [Route("GetDropDownforNotesType")]
        [HttpGet]
        public IEnumerable<BM_NotesTypes> GetDroupDownforEntity()
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var item = _objinote.GetAllNotesType(customerId);
            if (item == null)
            {
                item = new List<BM_NotesTypes>();
            }
            // item.Add(new EntityMasterVM() { Id = -1, EntityName = "Other", Entity_Type = 0 });
            return item;
            //return objEntityMaster.GetCompanyType();
        }
    }
}
