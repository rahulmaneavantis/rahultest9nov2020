﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BM_ManegmentServices.Services.Masters;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CircularResolutionAPI")]
    public class CircularResolutionAPIController : ApiController
    {
        ICircularResolution obj;
        public CircularResolutionAPIController(ICircularResolution obj)
        {
            this.obj = obj;
        }

        [Route("GetAllCirculars")]
        [HttpGet]
        public DataSourceResult GetAll_Circulars([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetAll_CircularResolution(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
