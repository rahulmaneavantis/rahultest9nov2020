﻿using BM_ManegmentServices.Services.Masters;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/DesignationMasterAPI")]
    public class DesignationMasterAPIController : ApiController
    {
        IDesignationMaster obj;

        public DesignationMasterAPIController(IDesignationMaster obj)
        {
            this.obj = obj;
        }

        [Route("GetDesignationeDetails")]
        [HttpGet]
        public DataSourceResult GetDesignationeDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            var gridData = obj.GetDesignationData();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("DeleteDesignationData")]
        [HttpDelete]
        public HttpResponseMessage DeleteDesignationData(int Id)//, int UserID
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                bool result = obj.DeleteDesignationData(Id);//,UserID
            }

            return response;
        }


        [Route("GetDirectors_DesignationDropdown")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.Director_DesignationVM> GetDirectors_DesignationDropdown()
        {
            var data = obj.GetDirectorDesignation();
            return data;
        }

        [Route("GetDesignationForDirector")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.VMDesignation> GetDesignationForDirector(string insertSelect)
        {
            return obj.GetDesignationForDirector(insertSelect);
        }
        [Route("GetDesignation")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.VMDesignation> GetDesignation(bool isDirector, bool isMNGT, string insertSelect)
        {
            return obj.GetDesignation(isDirector, isMNGT, insertSelect);
        }
        [Route("GetDirectorTypeOfMembership")]
        [HttpGet]
        public IEnumerable<BM_ManegmentServices.VM.DirectorTypeOfMembershipVM> GetDirectorTypeOfMembership(int designationId, string insertSelect)
        {
            return obj.GetDirectorTypeOfMembership(designationId, insertSelect);
        }
    }
}