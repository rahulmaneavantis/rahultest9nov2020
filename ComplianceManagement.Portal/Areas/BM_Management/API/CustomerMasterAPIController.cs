﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CustomerMasterAPI")]
    public class CustomerMasterAPIController : ApiController
    {
        [Route("GetAllMyCustomers")]        
        [HttpGet]
        public List<object> GetAll_MyCustomers(int userID, int custID, int SPID, int distID, string roleCode, bool showSelf)
        {            
            return SecretarialManagement.GetAll_MyCustomers(userID, custID, SPID, distID, roleCode, showSelf);
        }

        [Route("GetAllMyCustomerEntities")]
        [HttpGet]
        public List<BM_SP_GetMyCustomerEntities_Result> GetAll_MyCustomerEntities(int userID, int custID, int SPID, int distID, string roleCode, bool showSelf)
        {
            return SecretarialManagement.GetAll_MyCustomerEntities(userID, custID, SPID, distID, roleCode, showSelf);
        }

        [Route("GetEntityConfigurationCustomerWise")]
        [HttpGet]
        public List<BM_SP_EntityConfigurationCustomerWise_Result> GetEntityConfigurationCustomerWise(int custID)
        {
            return SecretarialManagement.GetEntityConfigurationCustomerWise(custID);
        }

        [Route("SaveSettings")]
        [HttpPost]
        public HttpResponseMessage Save_Settings([FromBody]List<VM_EntityTypeSetting> lstSettings)
        {
            try
            {
                bool SaveSuccess = false;

                if (lstSettings.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var customers = lstSettings.Select(row => row.CustID).Distinct();

                        int customerID = 0;
                        if (customers.Count() == 1)
                            customerID = customers.FirstOrDefault();

                        if (customerID != 0)
                        {
                            SaveSuccess = SecretarialManagement.DeActive_EntityConfigurationCustomerWise(customerID);

                            if (SaveSuccess)
                            {
                                lstSettings.ForEach(eachRecord =>
                                {
                                    SaveSuccess = SecretarialManagement.CreateUpdate_EntityTypeSetting(eachRecord.CustID, eachRecord.EntityTypeID, eachRecord.Value, eachRecord.UserID);
                                });                              
                            }                                
                        }
                    }
                }

                if (!SaveSuccess)
                {
                    var r = Request.CreateResponse(HttpStatusCode.OK, "Error");
                    return r;
                }

                var response = Request.CreateResponse(HttpStatusCode.OK, "Success");
                return response;
            }
            catch (Exception ex)
            {                
                var r = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
                return r;
            }
        }

        [Route("DeleteCustomer")]
        [HttpGet]
        public bool DeleteCustomer(int custID)
        {
            return SecretarialManagement.DeleteCustomer(custID);
        }

        [Route("GetAllUserCustomerMapping")]
        [HttpGet]
        public List<BM_SP_GetAllUserCustomerMapping_Result> GetAllUserCustomerMapping(int distID, int prodID)
        {
            return SecretarialManagement.GetAll_UserCustomerMapping(distID, prodID);
        }

        [Route("DeleteUserCustomerMapping")]
        [HttpGet]
        public bool DeleteUserCustomerMapping(int recordID)
        {
            //return SecretarialManagement.Delete_UserCustomerMapping(recordID);
            return UserCustomerMappingManagement.Delete_UserCustomerMapping(recordID);
        }
    }
}
