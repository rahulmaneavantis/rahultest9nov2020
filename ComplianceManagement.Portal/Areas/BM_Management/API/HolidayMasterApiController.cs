﻿using BM_ManegmentServices.Services.Masters;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/HolidayMasterApi")]
    public class HolidayMasterApiController : ApiController
    {
        IHoliday obj;

        public HolidayMasterApiController(IHoliday obj)
        {
            this.obj = obj;
        }

        [Route("GetHolidayList")]
        [HttpGet]
        public DataSourceResult GetHolidayList([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {

            var gridData = obj.GetHolidayList();

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
