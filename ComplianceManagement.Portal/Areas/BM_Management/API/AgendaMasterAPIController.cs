﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AgendaMasterAPI")]
    public class AgendaMasterAPIController : ApiController
    {
        IAgendaMaster objIAgendaMaster;

        public AgendaMasterAPIController(IAgendaMaster obj)
        {
            objIAgendaMaster = obj;
        }

        [Route("GetAllAgenda")]
        [HttpGet]
        public DataSourceResult GetAllAgenda([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var role = AuthenticationHelper.Role;

            var gridData = objIAgendaMaster.GetAll(customerId, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("LastUpdatedOn", System.ComponentModel.ListSortDirection.Descending));
            }

            return result;
        }

        [Route("GetAgendaCustomerWise")]
        [HttpGet]
        public DataSourceResult GetAgendaCustomerWise([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var role = AuthenticationHelper.Role;

            var gridData = objIAgendaMaster.GetAll_CustomerEntityTypeWise(customerId, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("LastUpdatedOn", System.ComponentModel.ListSortDirection.Descending));
            }

            return result;
        }

        [Route("GetAllSubAgenda")]
        [HttpGet]
        public DataSourceResult GetAllSubAgenda(long ParentId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIAgendaMaster.GetAllSubAgenda(ParentId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetAllSubAgendasNew")]
        [HttpGet]
        public DataSourceResult GetAllSubAgendasNew(long ParentId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIAgendaMaster.GetAllSubAgendasNew(ParentId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetAllPreCommittee")]
        [HttpGet]
        public DataSourceResult GetAllPreCommittee(long ParentId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objIAgendaMaster.GetAllSubAgendas(ParentId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetSubAgendaList")]
        [HttpGet]
        public IEnumerable<SubAgendaMasterVM> GetSubAgendaList(int MeetingTypeId, long Parent_AgendaId)
        {
            return objIAgendaMaster.GetAllSubAgendaNew(MeetingTypeId, Parent_AgendaId);
        }

        [Route("CreateAgenda")]
        [HttpPost]
        public HttpResponseMessage CreateAgenda(AgendaMasterVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            //if (ModelState.IsValid)
            //{
            //    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            //    obj.UserId = UserID;
            //    var result = objIAgendaMaster.Create(obj);
            //    response = Request.CreateResponse(HttpStatusCode.OK, result);
            //}
            //else
            //{
            //    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "validation failed");
            //}
            return response;
        }

        [Route("CreateAgendaNew")]
        [HttpPost]
        public HttpResponseMessage CreateAgendaNew(AgendaMasterVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            //if(obj.IsVoting == true && obj.IsNoting == true)
            //{
            //    ModelState.AddModelError("Voting","Please select voting or noting");
            //}
            
            //if (ModelState.IsValid)
            //{
            //    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            //    obj.UserId = UserID;
            //    var result = objIAgendaMaster.Create(obj);
            //    response = Request.CreateResponse(HttpStatusCode.OK, result);
            //}
            //else
            //{
            //    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            //}
            return response;
        }

        [Route("DeleteAgenda")]
        [HttpDelete]
        public HttpResponseMessage DeleteAgenda(int Id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                string result = objIAgendaMaster.Delete(Id, UserID);
            }

            return response;
        }

        [Route("UpdateAgenda")]
        [HttpPut]
        public HttpResponseMessage UpdateAgenda(AgendaMasterVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            //if (ModelState.IsValid)
            //{
            //    int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            //    obj.UserId = UserID;
            //    var result = objIAgendaMaster.Update(obj);
            //    response = Request.CreateResponse(HttpStatusCode.OK, result);
            //}
            //else
            //{
            //    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            //}

            return response;
        }

        [Route("GetAgendaFrequency")]
        [HttpGet]
        public IEnumerable<AgendaFrequencyVM> GetAgendaFrequency()
        {
            return objIAgendaMaster.GetAgendaFrequency();
        }


        [Route("GetUIForms")]
        [HttpGet]
        public IEnumerable<UIFormsVM> GetUIForms()
        {
            return objIAgendaMaster.GetUIForms();
        }

        #region Agenda Information
        [Route("Information")]
        [HttpGet]
        public HttpResponseMessage GetAgendaItemInfo(long id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response = Request.CreateResponse(HttpStatusCode.OK, objIAgendaMaster.GetInfomativeText(id));
            return response;
        }
        #endregion

        #region Agenda Item for Meeting Module

        [Route("GetAgendaItemsForMeeting")]
        [HttpGet]
        public IEnumerable<SubAgendaMasterVM> GetAgendaItems(int EntityId, int MeetingTypeId)
        {
            return objIAgendaMaster.GetAgendaItems(EntityId, MeetingTypeId);
        }

        [Route("GetAgendaItemsGridForMeeting")]
        [HttpGet]
        public DataSourceResult GetAgendaItemsGridForMeeting([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,
            long MeetingId, string Part, string MeetingCircular, bool? showPendingAgenda)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            if(showPendingAgenda == true)
            {
                var gridData = objIAgendaMaster.GetPendingAgendaItemsSelectList(MeetingId, Part, customerId).ToList();
                var result = new DataSourceResult()
                {
                    Data = gridData,
                    Total = gridData == null ? 0 :gridData.Count
                };
                return result;
            }
            else
            {
                var gridData = objIAgendaMaster.GetAgendaItemsSelectList(MeetingId, Part, customerId).ToList();
                var result = new DataSourceResult()
                {
                    Data = gridData,
                    Total = gridData == null ? 0 : gridData.Count
                };
                return result;
            }
        }

        [Route("GetDetailsOfOpenAgendaItem")]
        [HttpGet]
        public DataSourceResult GetDetailsOfOpenAgendaItem([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,
            long MappingID)
        {
            var gridData = objIAgendaMaster.GetDetailsOfOpenAgendaItem(MappingID).ToList();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData == null ? 0 : gridData.Count
            };
            return result;
        }

        [Route("GetMeetingMultiStageAgendaItemHistory")]
        [HttpGet]
        public DataSourceResult GetMeetingMultiStageAgendaItemHistory([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,
            long StartMeetingId, long StartAgendaId, int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var gridData = objIAgendaMaster.GetMeetingMultiStageAgendaItemHistory(StartMeetingId, StartAgendaId, EntityId , customerId).ToList();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData == null ? 0 : gridData.Count
            };

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("SequenceNo", System.ComponentModel.ListSortDirection.Ascending));
                request.Sorts.Add(new SortDescriptor("MeetingAgendaMappingID", System.ComponentModel.ListSortDirection.Ascending));
            }

            return result;
        }


        [Route("GetOpenAgendaItemsCount")]
        [HttpGet]
        public HttpResponseMessage GetAgendaItemInfo(long MeetingId, string Part)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var gridData = objIAgendaMaster.GetPendingAgendaItemsSelectList(MeetingId, Part, customerId).ToList();

            if(gridData == null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, new { Total = 0});
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.OK, new { Total = gridData.Count });
            }
            
            return response;
        }

        #endregion

        [Route("DeleteAgendaComplianceMapping")]
        [HttpDelete]
        public HttpResponseMessage DeleteAgendaComplianceMapping(int Id)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                bool result = objIAgendaMaster.DeleteAgendaComplianceMapping(Id);
            }

            return response;
        }

    }
}