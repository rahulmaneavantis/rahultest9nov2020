﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/DirectorRelativeAPIController")]
    public class DirectorRelativeAPIController : ApiController
    {
        IDirectorRelative objDirectorRelative;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public DirectorRelativeAPIController(IDirectorRelative obj)
        {
            objDirectorRelative = obj;
        }

        [Route("GetRelative")]
        [HttpGet]
        public DataSourceResult GetRelative([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int Director_Id)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objDirectorRelative.GetAllRelatives(Director_Id);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("AddRelative")]
        [HttpPost]
        public HttpResponseMessage AddRelative(DirectorRelativesVM obj)
        {
            string Message = String.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.CreatedBy = userID;
                obj = objDirectorRelative.AddRelatives(obj);

                if(obj.Message.Error == true)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, obj.Message.Message);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, obj.Message.Message);
                }   
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return response;

        }

        [Route("UpdateRelative")]
        [HttpPut]
        public HttpResponseMessage UpdateRelative(DirectorRelativesVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.UpdatedBy = userID;
                obj = objDirectorRelative.UpdateRelatives(obj);

                response = Request.CreateResponse(HttpStatusCode.OK, "Update Successfully");
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            return response;
        }

        [Route("DeleteRelative")]
        [HttpPut]
        public HttpResponseMessage DeleteRelative(DirectorRelativesVM obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            obj.UpdatedBy = userID;
            obj = objDirectorRelative.Delete(obj);

            response = Request.CreateResponse(HttpStatusCode.OK, obj.Message.Message);

            return response;
        }

        [Route("GetRelation")]
        [HttpGet]
        public IEnumerable<RelationVM> GetRelation(long? Director_Id)
        {
            if(Director_Id == null)
            {
                return objDirectorRelative.GetRelation();
            }
            else
            {
                return objDirectorRelative.GetRelation((long)Director_Id);
            } 
        }
    }

}
