﻿
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CapitalMasterAPI")]
    public class CapitalMasterAPIController : ApiController
    {

        ICapitalMaster obj;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public CapitalMasterAPIController(ICapitalMaster obj)
        {
            this.obj = obj;
        }
        [Route("GetLLpCapital")]
        [HttpGet]
        public DataSourceResult GetLLpCapital([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, string EntityId)
        {
            int Entity_Id = Convert.ToInt32(EntityId);
            var gridData = obj.GetCapitalLLPList(Entity_Id);

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateLLPCapital")]
        [HttpPost]
        public IHttpActionResult CreateLLPCapital(VMllpCapital _objllp, string EntityId)
        {
            int Entity_Id = 0;


            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                if (EntityId != null)
                {
                    Entity_Id = Convert.ToInt32(EntityId);
                    var result = obj.AddLLpCapitalMaster(_objllp, CustomerId);
                }

            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateLLPCapital")]
        [HttpPost]
        public IHttpActionResult UpdateLLPCapital(VMllpCapital _objllp,string EntityId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                if (EntityId != null)
                {
                    int Entity_Id = Convert.ToInt32(EntityId);
                   var result = obj.UpdateLLpCapitalMaster(_objllp, CustomerId);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("GetBoadyCorporateCapital")]
        [HttpGet]
        public DataSourceResult GetBoadyCorporateCapital([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
            var gridData = obj.GetCapitalBodyCorporateList(EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateBodyCorporateCapital")]
        [HttpPost]
        public IHttpActionResult CreateBodyCorporateCapital(VMBodyCorporate _objbodyCorporate,int EntityId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                var result = obj.AddBodyCorporateCapitalMaster(_objbodyCorporate, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("UpdateBodyCorporateCapital")]
        [HttpPost]
        public IHttpActionResult UpdateBodyCorporateCapital(VMBodyCorporate _objbodyCorporate, int EntityId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                string result = obj.UpdatebodyCorporateCapitalMaster(_objbodyCorporate, CustomerId);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Ok();

        }

        [Route("GetDesignatedPartnerName")]
        [HttpGet]
        public IEnumerable<VMllpCapital> GetDesignatedPartnerName(int EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return obj.GetAllDINNumber(customerId,EntityId);
        }


        [Route("GetEntityShareCapitaldetails")]
        [HttpGet]
        public DataSourceResult GetEntityShareCapitaldetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId)
        {
           
            var gridData = obj.GetEntityShareCapitaldetails(EntityId);

            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

    }
}
