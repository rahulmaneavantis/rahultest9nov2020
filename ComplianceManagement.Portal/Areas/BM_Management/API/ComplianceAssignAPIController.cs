﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ComplianceAssignAPI")]
    public class ComplianceAssignAPIController : ApiController
    {
        ICompliance_Service objCompliance_Service;
        public ComplianceAssignAPIController(ICompliance_Service obj)
        {
            this.objCompliance_Service = obj;
        }

        [Route("StatutoryCompliance")]
        [HttpGet]
        public DataSourceResult StatutoryCompliance_Read([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,int EntityID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var data = objCompliance_Service.GetStatutoryComplianceList(EntityID, customerId);
            var result = new DataSourceResult()
            {
                Data = data,
                Total = data == null ? 0 : data.Count()
            };
            return result;
        }

        [Route("StatutoryActs")]
        [HttpGet]
        public IEnumerable<ActVM> StatutoryActs_Read(int EntityID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return objCompliance_Service.GetStatutoryActList(EntityID, customerId);
        }

        [Route("GetSecretarialTags")]
        [HttpGet]
        public IEnumerable<SecretarialTag_VM> GetSecretarialTags()
        {            
            return objCompliance_Service.GetSecretarialTagList();
        }

    }
}
