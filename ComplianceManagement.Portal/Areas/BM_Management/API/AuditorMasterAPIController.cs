﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/AuditorMasterAPI")]
    public class AuditorMasterAPIController : ApiController
    {
        IAuditorMaster obj;
        public AuditorMasterAPIController(IAuditorMaster obj)
        {
            this.obj = obj;
        }

        [Route("GetAuditorDetails")]
        [HttpGet]
        public DataSourceResult GetAuditorDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetAuditorDetails(customerId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetAuditorInternalDetails")]
        [HttpGet]
        public DataSourceResult GetAuditorInternalDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetInternalAuditor(customerId,EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetStatutoryAuditor")]
        [HttpGet]
        public DataSourceResult GetStatutoryAuditor([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetStatutoryAuditor(customerId,EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        // getSecreterialAuditor

        [Route("GetSecreterialAuditor")]
        [HttpGet]
        public DataSourceResult GetSecreterialAuditor([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.getSecreterialAuditor(customerId,EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetCostAuditor")]
        [HttpGet]
        public DataSourceResult GetCostAuditor([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.getCostAuditor(customerId,EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetDropDownEntityName")]
        [HttpGet]
        public IEnumerable<BM_EntityMaster> GetDropDownforEntityType()
        {
            return obj.GetDropDownforEntityType();
        }
        [Route("GetAuditorwhoresigned")]
        [HttpGet]
        public IEnumerable<BM_Auditor_Master> GetAuditorwhoresigned(long EntityId)
        {
            return obj.GetAuditorwhoresigned(EntityId);
        }
        [Route("GetDropDownNatureofAppointment")]
        [HttpGet]
        public IEnumerable<BM_Nature_of_Appointment> GetDropDownNatureofAppointment()
        {
            return obj.GetDropDownNatureofAppointment();
        }

        [Route("GetDropDownAuditors")]
        [HttpGet]
        public IEnumerable<VMAuditorforDropdown> GetDropDownAuditors(int CriteriaId,int Type, int EntityId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return obj.GetDropDownAuditors(CriteriaId,Type, EntityId, CustomerId);
        }


        [Route("GetDropDownAuditors_Type")]
        [HttpGet]
        public IEnumerable<BM_AuditorType> GetDropDownAuditors_Type()
        {
            return obj.GetDropDownAuditors_Type();
        }


        [Route("GetAuditorsByType")]
        [HttpGet]
        public IEnumerable<VMAuditorforDropdown> GetAuditorsByType(int type)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return obj.GetAuditorsByType(type, customerId);
        }

        [Route("GetAuditorListForApptCess")]
        [HttpGet]
        public IEnumerable<VMAuditorforDropdown> GetAuditorListForApptCess(int entityId, string auditorType, long? statutoryMappingId)
        {
            return obj.GetAuditorListForApptCess(entityId, auditorType, statutoryMappingId);
        }

        [Route("DeleteAuditor")]
        [HttpDelete]
        public HttpResponseMessage DeleteAuditor(int Id,int CustomerId,int UserId)
        {
            string result = string.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (Id > 0)
            {
                result = obj.DeleteAuditor(Id, CustomerId, UserId);
            }
            response.Content = new StringContent(result);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return response;
        }
    }
}
