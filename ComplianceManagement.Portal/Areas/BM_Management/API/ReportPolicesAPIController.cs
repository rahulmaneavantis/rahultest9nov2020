﻿using BM_ManegmentServices.Services.Reports;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/ReportPolicesAPI")]
    public class ReportPolicesAPIController : ApiController
    {
        IReportPolices obj;
        public ReportPolicesAPIController(IReportPolices obj)
        {
            this.obj = obj;
        }

        [Route("Reports")]
        [HttpGet]
        public DataSourceResult Reports([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long EntityId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = obj.GetReportsDetails(customerId, userID, role, EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
