﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/RegisterAPI")]
    public class RegisterAPIController : ApiController
    {
        IRegister obj;
        public RegisterAPIController(IRegister obj)
        {
            this.obj = obj;
        }

        [Route("GetRegisterList")]
        [HttpGet]
        public DataSourceResult GetRegisterList([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request)
        {
            //int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetRegisterList();
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }


        [Route("GetEntityType")]
        [HttpGet]
        public IEnumerable<EntityTypeVM> GetDroupDownforEntityTypeVM()
        {
            return obj.GetEntityType();
        }

        //[Route("GetRegisterList")]
        //[HttpGet]
        //public IEnumerable<RegistersLists> GetDroupDownfor(long EntityId)
        //{
        //    return obj.GetRegisterList(EntityId);
        //}

        [Route("RegistersEntityWise")]
        [HttpGet]
        public DataSourceResult RegistersEntityWise([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long EntityId)
        {
            //int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = obj.GetRegisterList(EntityId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }
    }
}
