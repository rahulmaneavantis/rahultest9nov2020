﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/CommitteeMasterNewAPIController")]
    public class CommitteeMasterNewAPIController : ApiController
    {
        IDirectorMaster objDirectorMaster;
        ICommitteeMaster objICommitteeMaster;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public CommitteeMasterNewAPIController(IDirectorMaster obj, ICommitteeMaster objICom)
        {
            objDirectorMaster = obj;
            objICommitteeMaster = objICom;
        }

        [Route("AddCommitee")]
        [HttpPost]
        public IHttpActionResult AddCommitee(CommitteeMasterVM _obj)
        {
            string Message = String.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                Message = objDirectorMaster.AddMember(_obj, userID, CustomerId);
         
            }
            else
            {
                response= Request.CreateResponse(HttpStatusCode.BadRequest,"Validation Fail");
            }
            return Json(Message);
        }

        [Route("UpdateCommittee")]
        [HttpPut]
        public IHttpActionResult UpdateCommittee(CommitteeMasterVM _obj)
        {
            string Message = String.Empty;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                DetailsOfCommiteePosition d = new DetailsOfCommiteePosition()
                {
                    Id = _obj.Id,
                    Entity_Id_Committee = _obj.Entity_Id,
                    Committee_Id = _obj.Committee_Id,
                    Director_ID = _obj.Director_Id,
                    Designation_Id = _obj.Designation_Id,
                    Committee_Type = _obj.Committee_Type,

                    UpdatedBy = userID,
                    UpdatedOn = DateTime.Now
                };

                var hasError = false;
                var data = objDirectorMaster.CheckChairpersonExists(d, _obj.Director_Id, out hasError);

                if (hasError)
                {
                    Message = data;
                }
                else if (string.IsNullOrEmpty(data))
                {
                    hasError = false;
                    var result = objDirectorMaster.CreateDetailsOfCommitteePosition(d, _obj.Director_Id, out hasError);

                    if (hasError)
                    {
                        Message = data;
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Message);
                    }
                    else if (result)
                    {
                        Message = "Update Successfully";
                        response = Request.CreateErrorResponse(HttpStatusCode.OK, Message);
                    }
                    else
                    {
                        Message = data;
                        response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
                    }
                }
                else
                {
                    Message = data;
                    response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, Message);
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(Message);

        }

        [Route("DeleteCommitteeDtls")]
        [HttpDelete]
        public IHttpActionResult DeleteCommitteeDtls(CommitteeMasterVM _obj)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                bool hasError = false;
                var result = objDirectorMaster.DeleteDetailsOfCommitteePosition(_obj.Id, userID, out hasError);

                if (hasError)
                {
                    Message = "Server Error Occure";
                }
                else if (result)
                {
                    Message = "true";
                }
                else
                {
                    Message = "Something wents wrong";
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }
            return Json(Message);

        }

        [Route("GetCommitteeMasterNew")]
        [HttpGet]
        public DataSourceResult GetCommitteeMasterNew([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int EntityId, int CommitteeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var gridData = objDirectorMaster.GetCommitteeMasterNew(customerId, EntityId, CommitteeId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        public IHttpActionResult CheckIsValidCommittee(int EntityId, int CommitteeId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            return Json(objICommitteeMaster.CheckIsValidCommittee(CommitteeId, EntityId, customerId));
        }

        #region Committee Director Details
        [Route("GetCommitteeDtls")]
        [HttpGet]
        public DataSourceResult GetCommitteeDtls([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int DirectorId)
        {

            var gridData = objDirectorMaster.GetCommitteeMasterdtls(CustomerId, DirectorId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("CreateCommitteeDtls")]
        [HttpPost]
        public HttpResponseMessage CreateCommitteeDtls(CommitteeMasterVM _objcommity)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcommity = objDirectorMaster.CreateCommitteeDtls(CustomerId, _objcommity);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }

        [Route("UpdateCommitteeDtls")]
        [HttpPut]
        public HttpResponseMessage UpdateCommitteeDtls(CommitteeMasterVM _objcommity, int DirectorId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            if (ModelState.IsValid)
            {
                _objcommity = objDirectorMaster.UpdateCommitteeDtls(CustomerId, _objcommity);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Validation Failed");
            }

            return response;
        }
        #endregion
        //GetDirectorPosition

        [Route("GetDirectorPosition")]
        [HttpGet]
        public IEnumerable<BM_DirectorPosition> GetDirectorPosition()
        {
            return objDirectorMaster.GetDirectorPosition();
        }
    }

}
