﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.API
{
    [RoutePrefix("api/FormAPI")]
    public class FormAPIController : ApiController
    {
        IFormTemplets obj;
        public FormAPIController(IFormTemplets obj)
        {
            this.obj = obj;
        }

        [Route("GetFormTempletDetails")]
        [HttpGet]
        public DataSourceResult GetFormTempletDetails([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, int customerId, string role)
        {
            var gridData = obj.GetFormTempletDetails(customerId, role);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetFormListForComplianceMapping")]
        [HttpGet]
        public DataSourceResult GetFormListForComplianceMapping([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request, long ComplianceId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = obj.GetFormTempletDetails(ComplianceId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetMeetingFormTemplate")]
        [HttpGet]
        public DataSourceResult GetMeetingFormTemplate([ModelBinder(typeof(WebApiDataSourceRequestModelBinder))] DataSourceRequest request,long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;

            var gridData = obj.GetMeetingFormTemplate(MeetingId);
            var result = new DataSourceResult()
            {
                Data = gridData,
                Total = gridData.Count
            };
            return result;
        }

        [Route("GetMasterForFormTemplet")]
        [HttpGet]
        public IEnumerable<MasterDetails> GetMasterForFormTemplet()
        {
            return obj.GetMasterForm();
        }

        [Route("GetTempletFields")]
        [HttpGet]
        public IEnumerable<MasterFormates> GetDropDownforCustomer(long MasterId)
        {
            return obj.GetTemplateFormate(MasterId);
        }
    }
}