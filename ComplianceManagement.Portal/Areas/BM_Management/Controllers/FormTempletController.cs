﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class FormTempletController : Controller
    {
        IFormTemplets obj;
        public FormTempletController(IFormTemplets obj)
        {
            this.obj = obj;
        }
        // GET: BM_Management/FormTemplet
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Form()
        {
            return View();
        }

        public ActionResult OpenFormTemplet(long FormId)
        {
           
            FormTemplateDetails objformtemplet = new FormTemplateDetails();
            if (FormId>0)
            {
                objformtemplet = obj.getFormFormateById(FormId);
            }
            return PartialView("_FormTemplet", objformtemplet);
        }
     
        [HttpPost]
        public ActionResult SaveFormDetailsData(FormTemplateDetails objformtemplate)
        {
            if(ModelState.IsValid)
            {

                if (objformtemplate.FormFormat != null)
                {
                    objformtemplate.FormFormat = HttpUtility.HtmlDecode(objformtemplate.FormFormat);
                    ModelState.Clear();
                }
                if (objformtemplate.FormId==0)
                {
                    
                    objformtemplate = obj.AddFormTemplet(objformtemplate);
                }
                else
                {
                    objformtemplate = obj.UpdateFormTemplet(objformtemplate);
                }
            }
            return PartialView("_FormTemplet", objformtemplate);
        }


        public ActionResult OpenPreviewForm(long FormId)
        {
            VM_FormTemplet objFormTemplet = new VM_FormTemplet();
            if (FormId>0)
            {
                objFormTemplet = obj.getFileFormateforPreview(FormId);
            }
            return PartialView("_PreviewForm", objFormTemplet);
        }
        
    }
}