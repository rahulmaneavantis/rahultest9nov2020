﻿using BM_ManegmentServices;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyReportsController : Controller
    {
        // GET: BM_Management/MyReports
        public ActionResult Index()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View();               
            }
            else
            {
                return View("StatusReportCS");
            }            
        }

        public ActionResult AttedanceReports()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("AttendanceReport");
            }
            else
            {
                return View("AttendanceReportCS");
            }            
        }
    }
}