﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Dynamic;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CommitteeCompositionController : Controller
    {
        ICommitteeComp objService;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();

        public CommitteeCompositionController(ICommitteeComp obj)
        {
            objService = obj;
        }
        // GET: BM_Management/CommitteeComposition
        public ActionResult Index()
        {
            int? CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            dynamic mymodel = new ExpandoObject();
            mymodel.Customer_Id = CustomerId;
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
            if (authRecord != null)
            {
                mymodel.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee Composition") select x.Addview).FirstOrDefault();
                mymodel.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee Composition") select x.Editview).FirstOrDefault();
                mymodel.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Committee Composition") select x.DeleteView).FirstOrDefault();
            }
            else
            {
                mymodel.CanAdd = true;
                mymodel.CanEdit = true;
                mymodel.CanDelete = true;

            }
            return View(mymodel);
        }

        public ActionResult Committee()
        {
            dynamic mymodel = new ExpandoObject();
            mymodel.Customer_Id = null;

            return View("Index", mymodel);
        }

        public ActionResult GetAllCommittee([DataSourceRequest]DataSourceRequest request, int ? CustomerId)
        {
            //var committee = objService.GetAllCommitteeComp(CustomerId);
            var committee = objService.GetAllCommitteeCompForMaster(CustomerId);
            return Json(committee.ToDataSourceResult(request));
        }

        public ActionResult GetAllCommitteeOld([DataSourceRequest]DataSourceRequest request)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var committee = objService.GetAllCommitteeComp(CustomerId);
            return Json(committee.ToDataSourceResult(request));
        }

        public ActionResult GetAllCommitteeNew([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var committee = objService.GetAllCommitteeComp(CustomerId).ToList();
                return Json(new { CommitteeCompVM = committee }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult Create(int? Customer_Id)
        {
            return PartialView("Create", new CommitteeCompVM() { Id =0 ,Customer_Id = Customer_Id, IsCommittee =  true, CompositionType = "C" });
        }
        public ActionResult Edit(int id, int? Customer_Id)
        {
            var committee = objService.GetCommitteeComp(id);
            return PartialView("Create", committee);
        }
        public ActionResult Rules(int id, int? Customer_Id, int? EntityId)
        {
            var committee = objService.GetCommitteeComp(id);

            //Get Default data for Entity Type = 1
            if (committee != null)
            {
                var asPerStandard = objService.CheckAsPerStandard(id, Customer_Id, EntityId);
                committee.Customer_Id = Customer_Id;
                

                CommitteeCompRulesForVM model = new CommitteeCompRulesForVM() { CommitteeId = committee.Id, Customer_Id = Customer_Id,  Entity_Type = 1 };
                //Create empty model
                model.MinimumDirector = new Composition() { RuleId = 0, CommitteeId = committee.Id, Customer_Id = Customer_Id,  Entity_Type = 1 };
                model.Quorum = new Composition() { RuleId = 0, CommitteeId = committee.Id, Customer_Id = Customer_Id, Entity_Type = 1 };

                if (asPerStandard == true && Customer_Id != null)
                {
                    Customer_Id = null;
                }

                MeetingDetails objMeetingDetails = objService.GetMeetingRule(committee.Id, 1, Customer_Id, EntityId);
                if (objMeetingDetails == null)
                {
                    objMeetingDetails = new MeetingDetails();
                }
                model.Meeting = objMeetingDetails;

                GetAllRules(committee.Id, 1, Customer_Id, EntityId);

                
                committee.AsPerStandard = asPerStandard;
                model.AsPerStandard = asPerStandard;

                ViewBag.CommitteeCompRulesForVM_model = model;
            }
            //End

            return PartialView("_EditRules", committee);
        }
        [NonAction]
        public void GetAllRules(int CommitteeId, int EntityType, int? CustomerId, int? EntityId)
        {
            //var lstMinimumDirector = objService.GetAllComposition(CommitteeId, EntityType, "D");

            #region Generate Rules
            //foreach (var item in lstMinimumDirector)
            //{
            //    if(item.DesignationCount > 0 && item.DesignationId == null && item.DesignationNumerator == null && item.DesignationDenominator == null)
            //    {
            //        item.Rule = "Minimum <b>"+ item.DesignationCount +"</b> Director";
            //    }
            //    else if (item.DesignationCount > 0 && item.DesignationId >0 && item.DesignationNumerator == null && item.DesignationDenominator == null)
            //    {
            //        if(item.DesignationCount == 1)
            //        {
            //            item.Rule = "At least <b>" + item.DesignationCount + " " + item.Designation + "</b> Director";
            //        }
            //        else
            //        {
            //            item.Rule = "Minimum <b>" + item.DesignationCount + " " + item.Designation + "</b> Director";
            //        }

            //    }
            //    else if (item.DesignationCount == null && item.DesignationId > 0 && item.DesignationNumerator > 0 && item.DesignationDenominator >0 )
            //    {
            //        item.Rule = "<b>" + item.Designation + "</b> Director " +
            //            "<br>More than "+item.DesignationNumerator +"/"+ item.DesignationDenominator +" of total strength";
            //    }
            //    else if (item.DesignationCount >0 && item.DesignationId ==null && item.DesignationNumerator > 0 && item.DesignationDenominator > 0)
            //    {
            //        item.Rule = "<b>" + item.DesignationCount + "</b> member "+
            //            " or "+ item.DesignationNumerator + "/" + item.DesignationDenominator + " of the member whichever is higher.";
            //    }
            //    else if (item.DesignationCount > 0 && item.DesignationId >0 && item.DesignationNumerator > 0 && item.DesignationDenominator > 0)
            //    {
            //        item.Rule = "<b>" + item.Designation + "</b> Director " + 
            //            "<br><b>" + item.DesignationCount + "</b> " +
            //            " or <b>" + item.DesignationNumerator + "/" + item.DesignationDenominator + "</b> whichever is higher.";
            //    }
            //    else
            //    {
            //        item.Rule = "";
            //    }
            //}

            #endregion

            ViewBag.lstMinimumDirector = objService.GetAllComposition(CommitteeId, EntityType, "D", CustomerId, EntityId);
            ViewBag.lstQuorum = objService.GetAllComposition(CommitteeId, EntityType, "Q", CustomerId, EntityId);
        }
        public ActionResult RulesFor(int Committee_Id, int EntityType, int? CustomerId, int? EntityId, string command_name)
        {
            var asPerStandard = objService.CheckAsPerStandard(Committee_Id, CustomerId, EntityId);

            CommitteeCompRulesForVM model = new CommitteeCompRulesForVM() { CommitteeId = Committee_Id, Entity_Type = EntityType };
            
            //Create empty model
            model.MinimumDirector = new Composition() { RuleId =0, CommitteeId = Committee_Id, Entity_Type = EntityType, Customer_Id = CustomerId, Entity_Id = EntityId };
            model.Quorum = new Composition() { RuleId = 0, CommitteeId = Committee_Id, Entity_Type = EntityType, Customer_Id = CustomerId , Entity_Id = EntityId };

            if (command_name == "CreateNew")
            {
                model.AsPerStandard = false;
                asPerStandard = false;
            }
            else if (command_name == "Copy")
            {
                model.AsPerStandard = false;
                asPerStandard = false;
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                objService.CopyRules(Committee_Id, (int) CustomerId, (int) EntityId, UserID);
            }
            else
            {
                model.AsPerStandard = asPerStandard;
            }

            if (asPerStandard == true && CustomerId != null)
            {
                CustomerId = null;
            }
            MeetingDetails objMeetingDetails = objService.GetMeetingRule(Committee_Id, EntityType, CustomerId, EntityId);
            if(objMeetingDetails == null)
            {
                objMeetingDetails = new MeetingDetails() { CommitteeId = Committee_Id,Entity_Type = EntityType, Customer_Id = CustomerId, Entity_Id =EntityId };
            }

            objMeetingDetails.Customer_Id = CustomerId;
            objMeetingDetails.Entity_Id = EntityId;

            model.Meeting = objMeetingDetails;

            GetAllRules(Committee_Id, EntityType, CustomerId, EntityId);

            return PartialView("_CommitteeRulesFor", model);
        }

        [HttpPost]
        public ActionResult EditMinimumDirector(int Rule_Id, int Committee_Id, string command_name)
        {
            var model = objService.GetComposition(Rule_Id, Committee_Id);

            if(command_name == "deleteRule")
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                var success = objService.DeleteRule(Rule_Id, Committee_Id, UserID);

                model.RuleId = 0;
                model.DesignationCount = null;
                model.DesignationId = null;
                model.DesignationNumerator = null;
                model.DesignationDenominator = null;
                model.DesignationIdQuorum = null;
            }

            ModelState.Clear();
            GetAllRules(model.CommitteeId, model.Entity_Type, model.Customer_Id, model.Entity_Id);

            string view = "_Rule_MinimumDirector";
            if (model.RuleType.Trim() == "Q")
            {
                view = "_Rule_Quorum";
                model.DesignationIdQuorum = model.DesignationId;
            }
            return PartialView(view, model);
        }

        [HttpPost]
        public ActionResult SaveUpdateMinimumDirector(Composition obj , string command_name)
        {
            string view = "_Rule_MinimumDirector";
            if (obj.RuleType == "Q")
            {
                view = "_Rule_Quorum";
                obj.DesignationId = obj.DesignationIdQuorum;
            }
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (obj.RuleId >0)
            {
                obj.UpdatedBy = UserID;
                obj.UpdatedOn = DateTime.Now;
                obj = objService.UpdateRule(obj);
            }
            else
            {
                obj.CreatedBy = UserID;
                obj.CreatedOn = DateTime.Now;
                obj = objService.CreateRule(obj);
            }
            ModelState.Clear();

            GetAllRules(obj.CommitteeId, obj.Entity_Type, obj.Customer_Id, obj.Entity_Id);

            if(obj.Message !=null)
            {
                if(obj.Message.Success)
                {
                    obj.RuleId = 0;
                    obj.DesignationCount = null;
                    obj.DesignationId  = null;
                    obj.DesignationNumerator = null;
                    obj.DesignationDenominator = null;
                    obj.DesignationIdQuorum = null;
                }
            }
            return PartialView(view, obj);
        }
        [HttpPost]
        public ActionResult SaveUpdateMeeting(MeetingDetails obj, string command_name)
        {
            if(command_name == "btnNoofMeeting")
            {
                ModelState.Clear();
                if(obj.lstMeetingSchedule == null)
                {
                    obj.lstMeetingSchedule = new List<MeetingSchedule>() { };
                }

                if (obj.MinMeeting < obj.lstMeetingSchedule.Count)
                {
                    List<MeetingSchedule> lstTemp = new List<MeetingSchedule>();
                    for (int i = 0; i < obj.MinMeeting; i++)
                    {
                        lstTemp.Add(obj.lstMeetingSchedule[i]);
                    }
                    obj.lstMeetingSchedule = lstTemp;
                }
                else
                {
                    for (int i = obj.lstMeetingSchedule.Count; i < obj.MinMeeting; i++)
                    {
                        obj.lstMeetingSchedule.Add(new MeetingSchedule() { MeetingScheduleId =0, CommitteeId= obj.CommitteeId , Entity_Type= obj.Entity_Type , Customer_Id = obj.Customer_Id, Entity_Id = obj.Entity_Id});
                    }
                }
            }
            else
            {
                ModelState.Clear();
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.CreatedBy = UserID;
                obj.UpdatedBy = UserID;
                if(obj.lstMeetingSchedule !=null)
                {
                    foreach (var item in obj.lstMeetingSchedule)
                    {
                        item.CreatedBy = UserID;
                        item.UpdatedBy = UserID;
                    }
                }
                objService.CreateOrUpdate(obj);
            }
            return PartialView("_Rule_Meeting", obj);
        }
        [HttpPost]
        public ActionResult Save(CommitteeCompVM obj)
        {
            if(ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (obj.Id > 0)
                {
                    obj.UpdatedBy = UserID;
                    obj.UpdatedOn = DateTime.Now;
                    var committee = objService.Update(obj);
                    ModelState.Clear();
                    return PartialView("Create", committee);
                }
                else
                {
                    obj.CreatedBy = UserID;
                    obj.CreatedOn = DateTime.Now;
                    var committee = objService.CreateOrUpdate(obj);
                    ModelState.Clear();
                    return PartialView("Create", committee);
                }
            }
            else
            {
                return PartialView("Create", obj);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var success = objService.Delete(id);
            var error = "true";
            return Json(String.Format("'Success':'{0}','Error':'{0}'", success,error));
        }

        [HttpPost]
        public ActionResult DeleteRule(int id, int CommitteeId)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var success = objService.DeleteRule(id, CommitteeId, UserID);
            var error = "true";
            return Json(String.Format("'Success':'{0}','Error':'{0}'", success, error));
        }

    }
}