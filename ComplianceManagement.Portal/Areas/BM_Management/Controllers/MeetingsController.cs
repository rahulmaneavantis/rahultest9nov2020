﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;

using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.InteractiveForms;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using System.Reflection;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Compliance;
using System.Windows.Media;
using Telerik.Windows.Documents.Spreadsheet.Theming;
using System.Windows;
using System.Net.Mail;
using BM_ManegmentServices.Services.Setting;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingsController : Controller
    {
        IMeeting_Service objIMeeting_Service;
        IMeeting_Compliances objIMeeting_Compliances;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IFileData_Service objIFileData_Service;
        IMainMeeting objIMainMeeting;
        IMeetingInvitee objIMeetingInvitee;
        ICompliance_Service objICompliance_Service;
        ISettingService objISettingService;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();

        #region Controller Constructor
        public MeetingsController(IMeeting_Service objMeeting_Service, IMeeting_Compliances objMeeting_Compliance, IComplianceTransaction_Service objComplianceTransaction_Service, IFileData_Service objFileData_Service, IMainMeeting objMainMeeting, IMeetingInvitee objMeetingInvitee, ICompliance_Service objCompliance_Service, ISettingService objSettingService)
        {
            objIMeeting_Service = objMeeting_Service;
            objIMeeting_Compliances = objMeeting_Compliance;
            objIComplianceTransaction_Service = objComplianceTransaction_Service;
            objIFileData_Service = objFileData_Service;
            objIMainMeeting = objMainMeeting;
            objIMeetingInvitee = objMeetingInvitee;
            objICompliance_Service = objCompliance_Service;
            objISettingService = objSettingService;
        }
        #endregion        

        #region Get Meetings
        public ActionResult Meetings(string stage)
        {
            var model = new MeetingFilter() { Stage = stage };
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
            
            if (authRecord != null)
            {
                model.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("My Meetings") select x.Addview).FirstOrDefault();
                model.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("My Meetings") select x.Editview).FirstOrDefault();
            }


            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("Meetings_Director");
            }
            else
            {
                return View(model);
            }
        }
        public ActionResult Past()
        {
            return View("PastMeetings");
        }
        public ActionResult Meetings_Read([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var role = AuthenticationHelper.Role;
            return Json(objIMeeting_Service.GetMeetings(customerId, userID, role).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult MeetingsForDirector_Read([DataSourceRequest] DataSourceRequest request)
        {
            int UserId = AuthenticationHelper.UserID;
            return Json(objIMeeting_Service.GetMeetingsForParticipant(UserId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadTodaysMeeting([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string role = AuthenticationHelper.Role;
            int UserId = AuthenticationHelper.UserID;
            return Json(objIMeeting_Service.GetTodaysMeetingDirectorwise(customerId, UserId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create/Edit
        public ActionResult Meeting_Create()
        {
            return PartialView("Meeting_Add", new Meeting_NewVM() { MeetingID = 0, IsShorter = false });
        }
        public ActionResult Meeting_Edit(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

            var model = objIMeeting_Service.GetMeetingDetails(customerId, MeetingId);

            if (model != null)
            {
                if (model.Type.Trim() == "M" && model.IsSeekAvailability == true)
                {
                    LoadSeekAvailability(model.MeetingID);
                }

                if (model.Type.Trim() == "C")
                {
                    model.CircularMeetingDetails.CircularMeetingId = model.MeetingID;
                    model.CircularMeetingDetails.EntityId = model.Entityt_Id;
                    model.CircularMeetingDetails.CircularNumber = model.MeetingSrNo;
                    model.CircularMeetingDetails.MeetingTypeId = model.MeetingTypeId;
                    model.CircularMeetingDetails.DueDate = model.CircularMeetingDetails.DueDate;
                    model.CircularMeetingDetails.CircularDate = model.CircularMeetingDetails.CircularDate;
                    model.CircularMeetingDetails.CircularDueTime = model.CircularMeetingDetails.CircularDueTime;
                    LoadCircularAgendaData(model.MeetingID);
                }

                if(model.IsVirtualMeeting == true)
                {
                    model.AttendanceNew = objIMainMeeting.checkforQuorum(model.MeetingID, customerId, model.Entityt_Id);
                    model.AttendanceNew.startstopeDateVM = objIMainMeeting.getMeetingstartstopDate(model.MeetingID);
                    model.AttendanceNew.MeetingId = MeetingId;
                }
            }
            ModelState.Clear();
            return PartialView("Meeting_Edit", model);
        }
        [HttpPost]
        public ActionResult CreateMeeting(Meeting_NewVM obj)
        {
            if(ModelState.IsValid)
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.CustomerId = customerId;
                obj.UserId = UserID;
                obj.MeetingSrNo = null;
                obj = objIMeeting_Service.Create(obj);

                if (obj.Success)
                {
                    return Meeting_Edit(obj.MeetingID);
                }
                else
                {
                    return PartialView("Meeting_Add", obj);
                }
            }
            else
            {
                return PartialView("Meeting_Add", obj);
            }
        }
        [HttpPost]
        public ActionResult EditMeeting(MeetingVM obj)
        {
            if(ModelState.IsValid)
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                obj.UserId = UserID;
                obj = objIMeeting_Service.Update(obj);
                ModelState.Clear();
            }
            
            if (obj.Type.Trim() == "M")
            {
                return PartialView("_EditMeetings", obj);
            }
            else
            {
                return PartialView("_EditCircular", obj);
            }
        }
        [HttpPost]
        public ActionResult RefreshMeeting(MeetingVM obj)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            obj = objIMeeting_Service.GetMeetingDetails(customerId, obj.MeetingID);
            ModelState.Clear();
            return PartialView("_EditMeetings", obj);
        }
        #endregion

        #region Delete Meeting
        public PartialViewResult GetDetailsCanMeetingDelete(long MeetingId)
        {
            var model = objIMeeting_Service.GetDetailsMeetingCanDelete(MeetingId);
            return PartialView("_DeleteMeeting", model);
        }
        [HttpPost]
        public PartialViewResult DeleteMeeting(DeleteMeetingVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            model = objIMeeting_Service.DeleteMeeting(model, userID);
            return PartialView("_DeleteMeeting", model);
        }
        #endregion

        #region Postpond
        [HttpPost]
        public ActionResult Postpond(long meetingId)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIMeeting_Service.Postpond(meetingId, userId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Meeting Participants
        public ActionResult CheckParticipantScheduledMeetings(long meetingId)
        {
            var result = objIMeeting_Service.GetParticipantOtherScheduledMeetings(meetingId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Pending Availability
        public ActionResult Availability()
        {
            return View();
        }
        public ActionResult GetPendingAvailability([DataSourceRequest] DataSourceRequest request)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            return Json(objIMeeting_Service.GetPedingAvailability(UserID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Seek Availability
        [HttpPost]
        public ActionResult UpdateAvailabilityDueDate(long MeetingId, string date_)
        {
            var result = false;
            if (!String.IsNullOrEmpty(date_.Trim()))
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                DateTime dt = Convert.ToDateTime(date_);
                result = objIMeeting_Service.UpdateAvailabilityDueDate(MeetingId, dt, UserID);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public void LoadSeekAvailability(long ParentID)
        {
            ViewBag.DetailsOfSeekAvailability_Grid = objIMeeting_Service.GetAllSeekAvailability(ParentID);
        }

        public PartialViewResult GetSeekAvailability(long id)
        {
            var model = objIMeeting_Service.GetSeekAvailability(id);
            model.ShowCtrls = true;
            LoadSeekAvailability(model.MeetingID);
            LoadCircularAgendaData(model.MeetingID);
            return PartialView("_EditAvailability", model);
        }

        public PartialViewResult GetSeekAvailabilityNew(long ParentId)
        {
            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = ParentId, ShowCtrls = true };
            LoadSeekAvailability(ParentId);
            return PartialView("_EditAvailability", model);
        }


        public PartialViewResult CreateUpdateSeekAvailabilityNew(long id, long meetingId)
        {
            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meetingId};
            if (id > 0 )
            {
                model = objIMeeting_Service.GetSeekAvailability(id);
            }
            
            model.ShowCtrls = true;
            //LoadSeekAvailability(model.MeetingID);
            return PartialView("_EditAvailability_AddUpdate", model);
        }

        [HttpPost]
        public PartialViewResult CreateUpdateSeekAvailabilityNew(MeetingAvailabilityVM obj, string command_name)
        {
            #region Create/Update Seek Availability
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.MeetingAvailabilityId == 0)
                {
                    obj = objIMeeting_Service.Create(obj);
                }
                else
                {
                    obj = objIMeeting_Service.Update(obj);
                }
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();

            return PartialView("_EditAvailability_AddUpdate", obj);
            #endregion
        }

        [HttpPost]
        public PartialViewResult CreateUpdateSeekAvailability(MeetingAvailabilityVM obj, string command_name)
        {
            #region Create/Update Seek Availability
            if (ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.UserId = UserId;
                if (obj.MeetingAvailabilityId == 0)
                {
                    obj = objIMeeting_Service.Create(obj);
                }
                else
                {
                    obj = objIMeeting_Service.Update(obj);
                }
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation fails.";
            }
            ModelState.Clear();

            if (obj.Success)
            {
                obj = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = obj.MeetingID, Message = obj.Message, Success = obj.Success, Error = obj.Error };
            }
            else
            {
                obj.ShowCtrls = true;
            }

            LoadSeekAvailability(obj.MeetingID);
            return PartialView("_EditAvailability", obj);
            #endregion
        }

        public PartialViewResult SeekAvailabilityRefresh(long MeetingId, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);

            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meeting.MeetingID, ShowCtrls = false, IsSeekAvailabilitySent = meeting.IsSeekAvailabilitySent };
            model.CanMarkSeekAvailability_ = meeting.CanMarkSeekAvailability;
            model.CanReSendSeekAvailability_ = meeting.CanReSendSeekAvailability;

            if (ViewType == "Participant")
            {
                model.Participant_View = true;
                model.Participant_UserId = userID;
            }

            model.Participant_UserId = userID;

            LoadSeekAvailability(MeetingId);
            return PartialView("_EditAvailability", model);
        }

        [HttpPost]
        public PartialViewResult ReSendSeekAvailability(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            var meeting = objIMeeting_Service.ForceEditSeekAvailability(customerId, MeetingId);

            var model = new MeetingAvailabilityVM() { MeetingAvailabilityId = 0, MeetingID = meeting.MeetingID, ShowCtrls = true, IsSeekAvailabilitySent = meeting.IsSeekAvailabilitySent };
            model.CanMarkSeekAvailability_ = meeting.CanMarkSeekAvailability;
            model.CanReSendSeekAvailability_ = meeting.CanReSendSeekAvailability;
            model.RefreshMailFormat = true;

            model.Participant_UserId = userID;

            LoadSeekAvailability(MeetingId);
            return PartialView("_EditAvailability", model);
        }

        public PartialViewResult MarkSeekAvailability_Participant(long MeetingId)
        {
            MarkSeekAvailabilityVM model = new MarkSeekAvailabilityVM() { MeetingID = MeetingId, ViewType = "Participant" };
            return PartialView("_EditAvailability_Mark", model);
        }

        public PartialViewResult MarkSeekAvailability(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);

            MarkSeekAvailabilityVM model = new MarkSeekAvailabilityVM() { MeetingID = MeetingId };
            if (meeting != null)
            {
                model.CanMarkAvailability_ = meeting.CanMarkSeekAvailability;
            }
            return PartialView("_EditAvailability_Mark", model);
        }
        #endregion

        #region Seek Availability Mail
        [HttpPost]
        public ActionResult EditAvailabilityMail(MeetingAvailabilityMailVM obj, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            obj.UserId = UserID;
            if (command_name == "send")
            {

                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "";
                obj = objIMeeting_Service.SendAvailabilityMail(obj, out mailFormat);
                var accessToDirector = objISettingService.GetAccessToDirector(customerID);
                if (accessToDirector)
                {
                    var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.AvailabilityMailID);
                    if (lstParticipants != null)
                    {
                        foreach (var item in lstParticipants)
                        {
                            string Mailbody = mailFormat.Replace("{{ Name of the Director }}", item.ParticipantName);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, "Seek Availability", Mailbody);
                        }
                    }
                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateAvailabilityMail(obj);
            }

            return PartialView("_EditAvailabilityFormat", obj);
        }

        [HttpPost]
        public ActionResult RefreshAvailabilityMail(long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var meeting = objIMeeting_Service.GetMeeting(customerId, MeetingId);
            var model = new MeetingAvailabilityMailVM();
            if (meeting != null)
            {
                if (meeting.AvailabilityMailFormat != null)
                {
                    model = meeting.AvailabilityMailFormat;
                }
            }
            return PartialView("_EditAvailabilityFormat", model);
        }

        [HttpPost]
        public ActionResult AvailabilityMailPreview(MeetingAvailabilityMailVM obj, string command_name)
        {

            obj = objIMeeting_Service.PreviewAvailabilityMail(obj);

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Seek Availability Response

        [HttpGet]
        public ActionResult GetAvailabilityResponse([DataSourceRequest] DataSourceRequest request, long MeetingID, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = null;
            if (ViewType == "Participant")
            {
                userId = Convert.ToInt32(AuthenticationHelper.UserID);
            }
            var gridData = objIMeeting_Service.GetAvailabilityResponse(MeetingID, customerId, userId);

            return Json(gridData.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAvailabilityResponse([DataSourceRequest] DataSourceRequest request,
           [Bind(Prefix = "models")]IEnumerable<AvailabilityResponse_ResultVM> availabilityResponse)
        {
            if (availabilityResponse != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in availabilityResponse)
                {
                    var result = objIMeeting_Service.UpdateAvailabilityResponse(item, UserId);
                    item.AvailabilityResponseID = result.AvailabilityResponseID;
                }
            }
            return Json(availabilityResponse.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult GetAvailabilityResponseChartData(long MeetingID, long MeetingAvailabilityId)
        {
            var model = objIMeeting_Service.GetAvailabilityResponseChartData(MeetingID, MeetingAvailabilityId);
            return Json(model);
        }
        public PartialViewResult SelectSeekAvailability(long meetingID, long meetingAvailabilityId)
        {
            var model = objIMeeting_Service.GetSeekAvailability(meetingAvailabilityId);
            return PartialView("Availability_Click", model);
        }
        [HttpPost]
        public PartialViewResult SetMeetingDateFromAvailability(MeetingAvailabilityVM obj, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SelectSeekAvailability(obj, UserID);
            ModelState.Clear();
            return PartialView("Availability_Click", model);
        }
        #endregion

        #region Meeting Agenda
        //Returns List of Agenda Item 
        public PartialViewResult AgendaItem(long MeetingId)
        {
            return PartialView("_EditAgendaItems", MeetingId);
        }

        //Returns Agenda Items for select
        public PartialViewResult AgendaItemSelect(long MeetingId, string Part, string MeetingCircular, bool ? showPendingAgenda)
        {
            if (String.IsNullOrEmpty(Part))
            {
                Part = "B";
            }

            if (Part != "A" && Part != "B" && Part != "C")
            {
                Part = "B";
            }

            if(showPendingAgenda== null)
            {
                showPendingAgenda = false;
            }
            dynamic expando = new ExpandoObject();
            var agendaItemSelectModel = expando as IDictionary<string, object>;

            agendaItemSelectModel.Add("MeetingId", MeetingId);
            agendaItemSelectModel.Add("Part", Part);
            agendaItemSelectModel.Add("MeetingCircular", MeetingCircular);

            agendaItemSelectModel.Add("showPendingAgenda", showPendingAgenda);

            return PartialView("_EditAgendaItemsSelect", agendaItemSelectModel);
        }

        public PartialViewResult MultistageAgendaItemHistory(long StartMeetingId, long StartAgendaId, int EntityId)
        {
            dynamic expando = new ExpandoObject();
            var agendaItemSelectModel = expando as IDictionary<string, object>;

            agendaItemSelectModel.Add("StartMeetingId", StartMeetingId);
            agendaItemSelectModel.Add("StartAgendaId", StartAgendaId);
            agendaItemSelectModel.Add("EntityIdForMultistage", EntityId);
            agendaItemSelectModel.Add("showPendingAgenda", true);

            return PartialView("_MultistageAgendaItemsHistory", agendaItemSelectModel);
        }
        //Add Agenda Items
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult save_checkedAgendaItems([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AgendaItemSelect> agendaItems)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = new List<AgendaComplianceMappingPendingVM>();
            bool flag = false;
            bool addFlag = true;
            if (agendaItems != null && ModelState.IsValid)
            {
                foreach (var agendaItem in agendaItems)
                {
                    addFlag = true;
                    if(agendaItem.IsCheked)
                    {
                        var agenda = objIMeeting_Service.CheckComplianceAssignedOrNot(agendaItem.BM_AgendaMasterId, agendaItem.Meeting_Id, CustomerID);
                        if(agenda != null)
                        {
                            if(agenda.Count > 0)
                            {
                                addFlag = false;
                                result.Add(agenda.FirstOrDefault());
                            }
                        }

                        if(addFlag)
                        {
                            objIMeeting_Service.AddAgendaItem(agendaItem, UserID, CustomerID);
                            flag = true;
                        }
                    }
                }

                if(flag)
                {
                    var agenda = agendaItems.First();

                    objIMeeting_Service.SetDefaultItemNumbers(agenda.Meeting_Id);

                    objIMeeting_Compliances.GenerateScheduleOn(agenda.Meeting_Id, SecretarialConst.ComplianceMappingType.AGENDA, CustomerID, UserID);

                    #region Compliance Activation before Meeting
                    objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(agenda.Meeting_Id, UserID);
                    #endregion

                    objIMeeting_Service.ReOpenVirtualMeeting(agenda.Meeting_Id, UserID);
                }
            }

            return Json(result.ToDataSourceResult(request, ModelState));
        }

        public ActionResult MeetingAgenda_Read([DataSourceRequest] DataSourceRequest request, long MeetingId, string Part, string Type)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("SrNo", System.ComponentModel.ListSortDirection.Ascending));
            }

            return Json(objIMeeting_Service.GetMeetingAgenda(MeetingId, Part, Type).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //Save Agenda Item Order
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MeetingAgenda_SaveOrder([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<MeetingAgendaMappingVM> agendaItems)
        {
            if (agendaItems != null && ModelState.IsValid)
            {
                int SrNo = 0;
                foreach (var agendaItem in agendaItems)
                {
                    SrNo++;
                    objIMeeting_Service.SaveOrderAgendaItem(agendaItem, SrNo);
                    agendaItem.SrNo = SrNo;
                }

                if(SrNo > 0)
                {
                    var meetingId = agendaItems.FirstOrDefault().Meeting_Id;
                    objIMeeting_Service.SetDefaultItemNumbers(meetingId);
                }
            }
            return Json(agendaItems.ToDataSourceResult(request, ModelState));
        }

        //Agenda Item Format Methods
        public PartialViewResult AgendaItemFormat_Read(long MeetingAgendaMappingID)
        {
            var model = objIMeeting_Service.GetAgendaItem(MeetingAgendaMappingID);
            return PartialView("_EditAgendaItemFormat", model);
        }

        public PartialViewResult AgendaItemFormat_Save(MeetingAgendaMappingVM obj)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                obj = objIMeeting_Service.UpdateAgendaItem(obj, UserID);
            }
            else
            {
                obj.Error = true;
                obj.Message = "Validation Fails.";
            }
            ModelState.Clear();
            return PartialView("_EditAgendaItemFormat", obj);
        }
        //End
        public PartialViewResult AgendaPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.PreviewAgenda(MeetingId, CustomerId);
            return PartialView("_Viewer_Agenda", model);
        }
        public ActionResult DownloadAgendaDocumnet(long MeetingId)
        {
            try
            {
                var fileName = GenerateAgendaDocumnet(MeetingId, 0);
                return File(fileName, "application/force-download", Path.GetFileName(fileName));
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }

        public FileDataVM GetNew_NoticeFileData(long meetingId, string noticeMail)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + meetingId;
                string fileName =  "Notice - "+meeting.MeetingTypeName + " "+meeting.TypeName + " [ "+meeting.MeetingSrNo +" ] "+ meeting.FY_CY+".docx";

                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = GetNoticeDocument(noticeMail);
                Byte[] bytes = provider.Export(document);

                result.FileName = fileName;
                result.FilePath = path;
                result.FileData = bytes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public RadFlowDocument GetNoticeDocument(string noticeMail)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                #region Document header
                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                editor.MoveToParagraphStart(paragraph);
                #endregion

                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };

                RadFlowDocument htmlDocument = htmlProvider.Import(noticeMail);
                editor.InsertDocument(htmlDocument);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public FileDataVM NewAgendaFileData(long meetingId)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + meetingId;
                string fileName = "Agenda - " + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".docx";

                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = CreateRadFixedDocumentNew(meetingId, customerId);
                Byte[] bytes = provider.Export(document);

                result.FileName = fileName;
                result.FilePath = path;
                result.FileData = bytes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public string GenerateAgendaDocumnet(long MeetingId, long NoticeLogId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, MeetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + MeetingId;
                string str = "Agenda_" + meeting.TypeName + "_" + meeting.MeetingTypeName + "_[" + meeting.MeetingSrNo + "]_" + meeting.FY_CY + ".docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                if (NoticeLogId > 0)
                {
                    fileName = Path.Combine(Server.MapPath(path), "Agenda_" + NoticeLogId + ".docx");
                }

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateRadFixedDocumentNew(MeetingId, customerId);

                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public RadFlowDocument CreateRadFixedDocumentNew(long meetingId, int customerId)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                var agenda = objIMeeting_Service.PreviewAgenda(meetingId, customerId);
                var agendaItems = agenda.lstAgendaItems;

                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 
                

                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                editor.MoveToParagraphStart(paragraph);

                //var r = editor.InsertLine(meeting.EntityName);
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 24;

                //r = editor.InsertText(meeting.EntityAddressLine1 + Environment.NewLine + meeting.EntityAddressLine2);
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 14;

                //r = editor.InsertText(Environment.NewLine + "AGENDA");
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 24;

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var run1 = editor.InsertLine(agenda.EntityName);
                run1.FontWeight = System.Windows.FontWeights.Bold;
                run1.FontSize = 24;

                var run2 = editor.InsertLine(agenda.EntityAddressLine1);
                run2.FontWeight = System.Windows.FontWeights.Bold;
                run2.FontSize = 14;

                var run21 = editor.InsertLine(agenda.EntityAddressLine2);
                run21.FontWeight = System.Windows.FontWeights.Bold;
                run21.FontSize = 14;

                var runCIN = editor.InsertLine(agenda.EntityCIN_LLPIN);
                run21.FontWeight = System.Windows.FontWeights.Bold;
                run21.FontSize = 12;

                //var run3 = editor.InsertLine("AGENDA");
                //run3.FontWeight = System.Windows.FontWeights.Bold;
                //run3.FontSize = 18;

                //editor.InsertSection();
                paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Center;
                //editor.MoveToParagraphStart(paragraph);

                editor.InsertLine("");
                var r = editor.InsertLine("AGENDA FOR THE "+ agenda.MeetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + agenda.MeetingTypeName.ToUpper() +" MEETING");
                r.FontWeight = System.Windows.FontWeights.Bold;
                r.FontSize = 18;

                #region Table
                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;

                if (agenda.MeetingDate !=null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd, yyyy");
                    mDay = date.ToString("dddd");
                }
                if(!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }


                var table = editor.InsertTable(4, 3);

                table.PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 100);
                table.Alignment = Alignment.Center;

                table.Rows[0].Cells[0].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 18);
                table.Rows[0].Cells[1].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 2);
                table.Rows[0].Cells[2].PreferredWidth = new TableWidthUnit(TableWidthUnitType.Percent, 80);
                Border tableBorderAll = new Border(0.5, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                table.Borders = new TableBorders(tableBorderAll);

                var cellRun = table.Rows[0].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DATE");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[0].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[0].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mDate);
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                //Day
                cellRun = table.Rows[1].Cells[0].Blocks.AddParagraph().Inlines.AddRun("DAY");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[1].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[1].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mDay);
                cellRun.FontWeight = System.Windows.FontWeights.Bold;
                //End

                //Time
                cellRun = table.Rows[2].Cells[0].Blocks.AddParagraph().Inlines.AddRun("TIME");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[2].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[2].Cells[2].Blocks.AddParagraph().Inlines.AddRun(mTime);
                cellRun.FontWeight = System.Windows.FontWeights.Bold;
                //End

                //Venue
                cellRun = table.Rows[3].Cells[0].Blocks.AddParagraph().Inlines.AddRun("VENUE");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[3].Cells[1].Blocks.AddParagraph().Inlines.AddRun(":");
                cellRun.FontWeight = System.Windows.FontWeights.Bold;

                cellRun = table.Rows[3].Cells[2].Blocks.AddParagraph().Inlines.AddRun(agenda.MeetingVenue);
                cellRun.FontWeight = System.Windows.FontWeights.Bold;
                //End
                #endregion

                editor.InsertLine("");
                r = editor.InsertLine("Items of Agenda");
                r.FontWeight = System.Windows.FontWeights.Bold;
                r.FontSize = 14;
                
                editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                #endregion

                Telerik.Windows.Documents.Flow.Model.Fields.FieldInfo tocField = editor.InsertField("TOC \\o" + " \"1-1\" " + "\\h \\z \\u", "");
                tocField.IsDirty = true;

                Border b = new Border(1, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                Border b_other = new Border(0, BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                h.CharacterProperties.FontSize.LocalValue = 16;
                h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;


                //editor.InsertBreak(BreakType.PageBreak);

                //editor.InsertSection();
                //section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Center;
                //editor.MoveToParagraphStart(paragraph);

                //r = editor.InsertLine("AGENDA");
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 24;

                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };

                if (agendaItems != null)
                {
                    var SrNo = 0;
                    foreach (var item in agendaItems)
                    {
                        SrNo++;
                        //string AgendaItem = "<h1>" + SrNo +". "+ item.AgendaItemText + "</h1><p>" + item.AgendaFormat +"</p>";
                        string AgendaItem = "<h1>" + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper()) + "</h1>";
                        section = editor.InsertSection();
                        paragraph = section.Blocks.AddParagraph();
                        paragraph.TextAlignment = Alignment.Right;

                        
                        //paragraph.Borders.SetBottom(b);
                        //paragraph.Properties.Borders.LocalValue.SetBottom(b);

                        editor.MoveToParagraphStart(paragraph);

                        var r1 = editor.InsertLine("Item No. " + SrNo);
                        r1.FontWeight = System.Windows.FontWeights.Bold;
                        r1.FontStyle = System.Windows.FontStyles.Italic;
                        r1.Underline.Pattern = UnderlinePattern.Single;

                        r1.FontSize = 24;

                        RadFlowDocument htmlDocument = htmlProvider.Import(AgendaItem);
                        editor.InsertDocument(htmlDocument, insertOptions);

                        AgendaItem = "<p>" + item.AgendaFormat + "</p>";
                        AgendaItem = item.AgendaFormat == null ? " " : item.AgendaFormat;
                        htmlDocument = htmlProvider.Import(AgendaItem);

                        editor.InsertDocument(htmlDocument, insertOptions);
                    }
                }
                //paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Left;
                //r = editor.InsertLine("By Order of the Board");
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 20;

                //paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Left;
                //r = editor.InsertLine("(Name of Chairman)");
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 18;

                //paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Left;
                //r = editor.InsertLine("Chairman");
                //r.FontWeight = System.Windows.FontWeights.Bold;
                //r.FontSize = 18;

                //Footer
                //Footer defaultFooter = section.Footers.Default;
                //Paragraph para = defaultFooter.Blocks.AddParagraph();

                //editor.MoveToParagraphStart(para);

                //editor.InsertText("Page ");
                //editor.InsertField("PAGE", "1");
                //editor.InsertText(" of ");
                //editor.InsertField("NUMPAGES", "1");
                //End Footer
                tocField.UpdateField();
                //doc.UpdateFields();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }
        #endregion

        #region Delete Agenda Item
        public PartialViewResult GetDeleteAgendaItem(long MeetingId, long MeetingAgendaMappingId)
        {
            var model = objIMeeting_Service.GetAllowAgendaDelete(MeetingId, MeetingAgendaMappingId, false);
            return PartialView("_DeleteAgendaItem", model);
        }
        [HttpPost]
        public PartialViewResult DeleteAgendaItem(DeleteAgendaVM model)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            model = objIMeeting_Service.DeleteAgendaItem(model, userId);
            return PartialView("_DeleteAgendaItem", model);
        }
        #endregion

        #region Meeting Pre Committee Agenda
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CheckPendingPreCommitteeAgendaItems(long MeetingId)
        {
            var PreCommittePendingAgenda = objIMeeting_Service.GetPendingPreCommittee(MeetingId);

            return Json(PreCommittePendingAgenda, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPendingPreCommitteeAgendaItems(long MeetingId)
        {
            var PreCommittePendingAgenda = objIMeeting_Service.GetPendingPreCommittee(MeetingId);
            return PartialView("_PreCommitteeAgendaItems", PreCommittePendingAgenda);
        }
        [HttpPost]
        public ActionResult SavePreCommitteeAgendaItems(int MeetingTypeID, long SourceMeetingID, long SourceMeetingAgendaMappingID, long? PostAgendaID, long TargetMeetingID)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objIMeeting_Service.SavePreCommitteeMeetingID(MeetingTypeID, SourceMeetingID, SourceMeetingAgendaMappingID, PostAgendaID, TargetMeetingID, UserID);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Meeting Agenda Item Data Fields Template Generation
        public PartialViewResult AgendaItemTemplate_Read(long MeetingAgendaMappingID)
        {
            var model = objIMeeting_Service.GetAgendaItemTemplate(MeetingAgendaMappingID);
            return PartialView("_EditAgendaItemsTemplate", model);
        }
        public PartialViewResult AgendaItemTemplate_Save(MeetingAgendaTemplateVM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SaveUpdateAgendaItemTemplateList(obj, userID);
            ModelState.Clear();
            return PartialView("_EditAgendaItemsTemplate", model);
        }
        #endregion

        #region Meeting New Agenda Item Create/Update
        [HttpGet]
        public ActionResult CreateNewAgendaItem(long MeetingId, string Part)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.CreateNewAgendaItem(MeetingId, CustomerID);

            return PartialView("_AddNewAgendaItem", model);
        }

        [HttpPost]
        public ActionResult SaveUpdateNewAgendaItem(AgendaMasterVM obj)
        {
            var model = new AgendaMasterVM();
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            if (obj.BM_AgendaMasterId == 0)
            {
                model = objIMeeting_Service.CreateNewAgendaItem(obj, CustomerID);
            }
            else
            {
                model = objIMeeting_Service.Update(obj);
            }
            ModelState.Clear();
            return PartialView("_AddNewAgendaItem", model);
        }
        #endregion       

        #region Meeting Agenda change Items No
        public PartialViewResult ChangeItemNumber(long id)
        {
            return PartialView("_EditAgendaItemNumber", id);
        }
        public ActionResult CheckAgendaItemNumberIsValid(long id)
        {
            var result = objIMeeting_Service.CheckAgendaItemNumberIsValid(id);
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAgendaItemsForChangeNumber([DataSourceRequest] DataSourceRequest request, long id)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ItemNo", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Service.GetAgendaItemForChangeNumber(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangeItemNumber([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ChangeAgendaItemNumberVM> agendaItems)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            if (agendaItems != null && ModelState.IsValid)
            {
                objIMeeting_Service.SaveAgendaItemNumber(agendaItems, userId);
            }
            return Json(agendaItems.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Meeting Notice Mail
        [HttpPost]
        public ActionResult EditNoticeMail(MeetingNoticeMailVM obj, string command_name)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

            obj.UserId = UserID;
            if (command_name == "send")
            {
                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "";

                long? fileId = null;
                obj = objIMeeting_Service.SendNoticeMail(obj, out mailFormat);

                if(obj.Success)
                {
                    if(obj.IsVirtualMeeting_ == true)
                    {
                        #region Generate Compliance document on Notice Send
                        if (obj.TemplateType == "AN" || obj.TemplateType == "N")
                        {
                            #region Update Schedule on Dates
                            objIMeeting_Compliances.CalculateScheduleDate(obj.NoticeMailID, obj.UserId);
                            #endregion

                            #region Compliance Activation before Meeting
                            objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(obj.NoticeMailID, UserID);
                            #endregion

                            var objFileData = GetNew_NoticeFileData(obj.NoticeMailID, mailFormat);
                            //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice");
                            var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice", false, 0, DateTime.Now, objFileData, UserID);
                            #region Commented on 19 Aug 2020
                            //long? complianceScheduleOnID = null, transactionID = null;
                            //if (objFileData != null)
                            //{
                            //    int fileVersion = 0;
                            //    if (lstClosedTransactions != null)
                            //    {
                            //        if (lstClosedTransactions.Count > 0)
                            //        {
                            //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                            //        }
                            //    }

                            //    fileVersion++;
                            //    objFileData.Version = Convert.ToString(fileVersion);
                            //    objFileData = objIFileData_Service.Save(objFileData, UserID);

                            //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            //    }
                            //}
                            #endregion
                        }
                        #endregion

                        #region Send Mail
                        var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.NoticeMailID);
                        if (lstParticipants != null)
                        {
                            obj.IsSummit = true;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Generate Agenda Document
                        FileDataVM objFileData_Agenda = null;
                        if (obj.TemplateType == "AN" || obj.TemplateType == "A")
                        {
                            objFileData_Agenda = NewAgendaFileData(obj.NoticeMailID);

                            if (objFileData_Agenda != null)
                            {
                                int fileVersion = 0;
                                fileVersion++;

                                objFileData_Agenda.Version = Convert.ToString(fileVersion);
                                objFileData_Agenda = objIFileData_Service.Save(objFileData_Agenda, UserID);

                                fileId = objFileData_Agenda.FileID;
                            }
                        }
                        #endregion

                        #region Generate Compliance document on Notice Send
                        if (obj.TemplateType == "AN" || obj.TemplateType == "N")
                        {
                            #region Update Schedule on Dates
                            objIMeeting_Compliances.CalculateScheduleDate(obj.NoticeMailID, obj.UserId);
                            #endregion

                            #region Compliance Activation before Meeting
                            objIComplianceTransaction_Service.CreateTransactionOnNoticeSend(obj.NoticeMailID, UserID);
                            #endregion

                            var objFileData = GetNew_NoticeFileData(obj.NoticeMailID, mailFormat);
                            //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice");
                            var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(obj.NoticeMailID, "Notice", false, 0, DateTime.Now, objFileData, UserID);

                            #region Commented on 19 Aug 2020
                            //long? complianceScheduleOnID = null, transactionID = null;
                            //if (objFileData != null)
                            //{
                            //    int fileVersion = 0;
                            //    if (lstClosedTransactions != null)
                            //    {
                            //        if (lstClosedTransactions.Count > 0)
                            //        {
                            //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                            //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                            //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                            //        }
                            //    }

                            //    fileVersion++;
                            //    objFileData.Version = Convert.ToString(fileVersion);
                            //    objFileData = objIFileData_Service.Save(objFileData, UserID);

                            //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            //    }

                            //    if (objFileData_Agenda != null && complianceScheduleOnID > 0)
                            //    {
                            //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData_Agenda.FileID, 1);
                            //    }
                            //}
                            #endregion
                        }
                        #endregion

                        long MeetingNoticeLogID = 0;
                        if (obj.TemplateType == "AN" || obj.TemplateType == "A" || obj.TemplateType == "N")
                        {
                            //objIMeeting_Service.CreateNoticeLog(obj.NoticeMailID, obj.TemplateType, obj.NoticeMail, fileId, UserID);
                            MeetingNoticeLogID = objIMeeting_Service.CreateNoticeLog(obj.NoticeMailID, obj.TemplateType, mailFormat, fileId, UserID);
                        }

                        #region Send Mail
                        var lstParticipants = objIMeeting_Service.GetMeetingParticipants(obj.NoticeMailID);
                        if (lstParticipants != null)
                        {
                            var accessToDirector = objISettingService.GetAccessToDirector(CustomerID);
                            if(accessToDirector)
                            {
                                foreach (var item in lstParticipants)
                                {
                                    string Mailbody = mailFormat.Replace("{{ Name of the Director }}", item.ParticipantName);
                                    if (objFileData_Agenda != null)
                                    {
                                        List<Attachment> attachment = new List<Attachment>();
                                        attachment.Add(new Attachment(new MemoryStream(objFileData_Agenda.FileData), objFileData_Agenda.FileName));

                                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, "Notice", Mailbody, attachment);
                                    }
                                    else
                                    {
                                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, "Notice", Mailbody);
                                    }

                                    if (MeetingNoticeLogID > 0)
                                    {
                                        objIMeeting_Service.CreateNoticeTransaction(MeetingNoticeLogID, item.MeetingParticipantId, DateTime.Now);
                                    }
                                }
                            }
                            obj.IsSummit = true;
                        }
                        #endregion
                    }

                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateNoticeMail(obj);
            }
            ModelState.Clear();
            return PartialView("_NoticeFormat", obj);
        }

        [HttpPost]
        public ActionResult NoticeMailPreview(MeetingNoticeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.PreviewNoticeMail(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NoticeAgendaTemplate(MeetingNoticeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.NoticeAgendaTemplate(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult NoticeAgendaLog(long meetingId)
        {
            return PartialView("_MeetingNoticeAgendaLog", meetingId);
        }

        public ActionResult DownloadNoticeDocument(long meetingId)
        {
            try
            {
                var noticeMail = objIMeeting_Service.GetNoticeFormatByMeetingId(meetingId);
                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = GetNoticeDocument(noticeMail);
                Byte[] bytes = provider.Export(document);

                return File(bytes, "application/force-download", "Notice.docx");
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }
        #endregion

        #region Invitee Mail
        [HttpPost]
        public ActionResult EditInviteeMail(MeetingInviteeMailVM obj, int userid, string command_name)
        {
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (command_name == "send")
            {
                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string mailFormat = "";
                
                var lstInvitees = objIMeetingInvitee.GetAll(obj.InviteeMailID, true);

                if(lstInvitees !=null)
                {
                    foreach (var item in lstInvitees)
                    {
                        obj = objIMeeting_Service.SendInviteeMail(obj, out mailFormat);
                        if(!string.IsNullOrEmpty(mailFormat))
                        {
                            var table = "<Table border='1' style='width: 80%;text-align: center;margin-left: 50px;'>" +
                                        "<tr>" +
                                        "<th>SrNo</th>" +
                                        "<th>Agenda List</th>" +
                                        "</tr>";
                            int srNo = 0;
                            foreach (var agendaItem in item.lstAgendaItems)
                            {
                                srNo++;
                                table += "<tr>" +
                                        "<td>" + srNo + "</td>" +
                                        "<td>" + agendaItem.AgendaHeading + "</td>" +
                                        "</tr>";
                            }
                            table += "</Table>";

                            string Mailbody = mailFormat.Replace("{{ AgendaList }}", table);
                            //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { item.Email }), null, null, "Seek Availability", Mailbody);
                        }
                    }
                }
            }
            else
            {
                obj = objIMeeting_Service.UpdateInviteeMail(obj, UserID);
            }
            return PartialView("_inviteeMailFormat", obj);
        }
        [HttpPost]
        public ActionResult InviteeMailPreview(MeetingInviteeMailVM obj, string command_name)
        {
            obj = objIMeeting_Service.PreviewInviteeMail(obj);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Circular
        [NonAction]
        public void LoadCircularAgendaData(long ParentID)
        {
            ViewBag.DetailsOfAgenda_Grid = objIMeeting_Service.GetAllAgendaDetails(ParentID);
        }
        [HttpPost]
        public ActionResult GetAgendaResponseChartData(long MeetingID, long AgendaId, long MappingId)
        {
            int UserId = AuthenticationHelper.UserID;
            var model = objIMeeting_Service.GetAgendaResponseChartData(MeetingID, AgendaId, MappingId, UserId);
            return Json(model);
        }

        public ActionResult Agenda_SummitResponse([DataSourceRequest] DataSourceRequest request, long MeetingId, string ViewType)
        {
            Agenda_SummitResponse obj = new Agenda_SummitResponse();
            obj.MeetingId = MeetingId;
            obj.ViewType = "";
            return PartialView("_AgendaResponseSummite", obj);
        }

        [HttpGet]
        public ActionResult GetAgendaResponse([DataSourceRequest] DataSourceRequest request, long MeetingID, string ViewType)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int? userId = null;
            string Role = null;
            int? RoleId = null;
            //if (ViewType == "Participant")
            //{
            userId = Convert.ToInt32(AuthenticationHelper.UserID);
            Role = AuthenticationHelper.Role;
            if (Role == "CS")
            {
                RoleId = 20;
            }
            else
            {
                RoleId = 0;
            }
            //}
            var gridData = objIMeeting_Service.GetAgendaResponse(MeetingID, customerId, userId, RoleId);
            LoadCircularAgendaData(MeetingID);

            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("Castingvote", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(gridData.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Agenda Documents
        [HttpPost]
        public ActionResult UpdateAgendaResponse([DataSourceRequest] DataSourceRequest request,
        [Bind(Prefix = "models")]IEnumerable<Agenda_SummitResponse> agendaResponse)
        {
            if (agendaResponse != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var item in agendaResponse)
                {
                    var result = objIMeeting_Service.UpdateAgendaResponse(item, UserId);
                    //item.AvailabilityResponseID = result.AvailabilityResponseID;
                }
            }
            return Json(agendaResponse.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult UploadAgendaFile(VM_AgendaDocument _vmagendaDocument)
        {
            if (ModelState.IsValid)
            {
                bool Savesuccess = objIMeeting_Service.AgendaFileUpload(_vmagendaDocument);
                if (Savesuccess)
                {
                    _vmagendaDocument.Success = true;
                    _vmagendaDocument.Message = "File Upload successfully";
                }
                else
                {
                    _vmagendaDocument.Error = true;
                    _vmagendaDocument.Message = "Some error occured";
                }
            }
            return PartialView("_AgendaFileUpload", _vmagendaDocument);
        }

        [HttpGet]
        public ActionResult ReloadPieChart(long MeetingId)
        {
            LoadCircularAgendaData(MeetingId);
            return PartialView("_CircularAgendaResponsesPiachart", MeetingId);
        }

        public ActionResult AgendaDocumentDownload(long Id)
        {
            bool downloadFile = objIMeeting_Service.agendaDocumentDownload(Id);
            return View();
        }

        public ActionResult AgendaDocumentsList(long MeetingAgendaMappingID)
        {
            VM_AgendaDocument _vm_agendadocument = new VM_AgendaDocument();
            _vm_agendadocument.MeetingAggendaMappingId = MeetingAgendaMappingID;
            return PartialView("_Agenda_wize_Document", _vm_agendadocument);
        }

        public ActionResult AgendaDocuments(long MeetingAgendaMappingID)
        {
            VM_AgendaDocument _vm_agendadocument = new VM_AgendaDocument();
            _vm_agendadocument.MeetingAggendaMappingId = MeetingAgendaMappingID;
            return PartialView("_Agenda_Documents", _vm_agendadocument);
        }
        #endregion

        #region Circular Voting Noting
        public ActionResult MeetingVootingNoting(long MeetingId)
        {
            MeetingAttendance_VM _meetingattendance = new MeetingAttendance_VM();
            _meetingattendance.MeetingId = MeetingId;
            return PartialView("_Meeting", _meetingattendance);
        }
        #endregion

        #region Minutes of Meeting
        public PartialViewResult MOMPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if(model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Minutes", model);
        }

        public PartialViewResult MOMPreviewNew(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Draft", model);
        }

        //Used in virtual meeting
        public PartialViewResult MinutesPreview(long MeetingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.PreviewMOM(MeetingId, CustomerId);
            if (model != null)
            {
                model.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(MeetingId, CustomerId, UserID);
                model.meetingMinutesDetails = objIMeeting_Service.GetMinutesDetails(MeetingId);
            }
            return PartialView("_Viewer_Minutes", model);
        }

        public ActionResult DownloadMinutesDocumnet(long MeetingId)
        {
            try
            {
                var fileName = GenerateMinutesDocument(MeetingId, 0);
                return File(fileName, "application/force-download", Path.GetFileName(fileName));
            }
            catch (Exception e)
            {
                return PartialView("_FileNotFound");
            }
        }

        public string GenerateMinutesDocument(long MeetingId, long NoticeLogId)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, MeetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + MeetingId;
                string str = "Minutes_" + meeting.TypeName + "_" + meeting.MeetingTypeName + "_[" + meeting.MeetingSrNo + "]_" + meeting.FY_CY + ".docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                if (NoticeLogId > 0)
                {
                    fileName = Path.Combine(Server.MapPath(path), "Minutes_" + NoticeLogId + ".docx");
                }

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateMinutesDocument(MeetingId, customerId, string.Empty);

                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public RadFlowDocument CreateMinutesDocument(long meetingId, int customerId, string generateFor)
        {
            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);
            try
            {
                var agenda = objIMeeting_Service.PreviewMOM(meetingId, customerId);
                var agendaItems = agenda.lstAgendaItems;
                if (agenda != null)
                {
                    agenda.lstMeetingAttendance = objIMainMeeting.GetPerticipenforAttendenceCS(meetingId, customerId, null);
                }


                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                Border b = new Border(1, BorderStyle.Single, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(10, 0, 0)), false, false, 1);
                var h = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(1));
                Border b_other = new Border(0, BorderStyle.None, new Telerik.Windows.Documents.Spreadsheet.Model.ThemableColor(System.Windows.Media.Color.FromRgb(0, 0, 0)), false, false, 0);
                h.ParagraphProperties.Borders.LocalValue = new ParagraphBorders(b_other, b_other, b_other, b);

                h.CharacterProperties.FontSize.LocalValue = 15;
                h.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;



                var h2 = doc.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.GetHeadingStyleIdByIndex(2));
                h2.CharacterProperties.FontSize.LocalValue = 15;
                h2.ParagraphProperties.TextAlignment.LocalValue = Alignment.Justified;
                h2.CharacterProperties.FontWeight.LocalValue = FontWeights.Bold;

                #region Document header

                var section = editor.InsertSection();
                var paragraph = section.Blocks.AddParagraph();
                paragraph.TextAlignment = Alignment.Left;
                editor.MoveToParagraphStart(paragraph);

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Justified;
               
                #region Table
                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;

                if (agenda.MeetingDate != null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd, yyyy");
                    mDay = date.ToString("dddd");
                }
                if (!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }

                #endregion

                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();

                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };

                var meetingTitle = "<h1>"+"MINUTES OF THE " + agenda.MeetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "")+ " MEETING OF THE " + agenda.MeetingTypeName.ToUpper() + " OF " + agenda.EntityName.ToUpper() + " HELD ON " + mDate.ToUpper() + ", AT " + mTime.ToUpper() + " AT " + agenda.MeetingVenue.ToUpper() + "</h1>";
                RadFlowDocument htmlDocument = htmlProvider.Import(meetingTitle);
                editor.InsertDocument(htmlDocument, insertOptions);


                var run1 = editor.InsertLine("");
                //run1.FontSize = 12;

                if (agenda.lstMeetingAttendance.Where(k=>k.Attendance == "P").Count() > 0)
                {
                    run1 = editor.InsertLine("PRESENT");
                    run1.FontWeight = System.Windows.FontWeights.Bold;
                    run1.FontSize = 15;

                    foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "P"))
                    {
                        var run2 = editor.InsertLine(item.MeetingParticipant_Name + "   " + item.Designation);
                        run2.FontWeight = System.Windows.FontWeights.Bold;
                        run2.FontSize = 15;
                    }

                    run1 = editor.InsertLine("");
                    //run1.FontSize = 12;

                    var run21 = editor.InsertLine("The meeting commenced at "+ agenda.MeetingStartTime);
                    run21.FontSize = 14;

                    run1 = editor.InsertLine("");
                    //run1.FontSize = 12;
                    var runCIN = editor.InsertLine("Chairman");
                    runCIN.FontWeight = System.Windows.FontWeights.Bold;
                    runCIN.FontSize = 14;

                    run1 = editor.InsertLine("");
                    //run1.FontSize = 12;

                    if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position ==1).Count() > 0)
                    {
                        var chairman = agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && k.Position == 1).FirstOrDefault();
                        runCIN = editor.InsertLine(chairman.MeetingParticipant_Name + " presided as the Chairman of the meeting.");
                        runCIN.FontSize = 14;
                    }
                }
                //paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Center;

                editor.ParagraphFormatting.SpacingAfter.LocalValue = 6;
                editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Left;

                #endregion

                bool anyOtherBusinessFlag = false;
                if (agendaItems != null)
                {
                    var SrNo = 0;
                    foreach (var item in agendaItems)
                    {
                        if(!string.IsNullOrEmpty(item.AgendaItemText))
                        {
                            SrNo++;

                            if(item.PartId == 3)
                            {
                                if(anyOtherBusinessFlag == false)
                                {
                                    var anyOtherHeading = "<h2>Any other business item</h2>";

                                    paragraph = section.Blocks.AddParagraph();
                                    editor.MoveToParagraphStart(paragraph);

                                    htmlDocument = htmlProvider.Import(anyOtherHeading);
                                    editor.InsertDocument(htmlDocument, insertOptions);


                                    var anyOtherText = "<p>The Chairman informed the board with the consultation of majority of members present at the meeting to consider the following agenda</p>";
                                    htmlDocument = htmlProvider.Import(anyOtherText);

                                    editor.InsertDocument(htmlDocument, insertOptions);

                                    anyOtherBusinessFlag = true;
                                }
                            }
                            string AgendaItem = "<h2>" + SrNo + ". " + item.AgendaItemText + "</h2>";
                            paragraph = section.Blocks.AddParagraph();

                            editor.MoveToParagraphStart(paragraph);

                            htmlDocument = htmlProvider.Import(AgendaItem);
                            editor.InsertDocument(htmlDocument, insertOptions);

                            AgendaItem = string.IsNullOrEmpty(item.AgendaFormat) ? "" : item.AgendaFormat ;
                            htmlDocument = htmlProvider.Import(AgendaItem);

                            editor.InsertDocument(htmlDocument, insertOptions);
                        }
                    }
                }

                var voteForThanks = "<h2>Votes of Thanks</h2>";
                paragraph = section.Blocks.AddParagraph();

                editor.MoveToParagraphStart(paragraph);

                htmlDocument = htmlProvider.Import(voteForThanks);
                editor.InsertDocument(htmlDocument, insertOptions);

                var voteForThanksBody = "<p>There being no other matter to be transacted, the meeting concluded at "+agenda.MeetingEndTime+ ". with a vote of thanks to the Chair.</p>";
                htmlDocument = htmlProvider.Import(voteForThanksBody);

                htmlDocument = htmlProvider.Import(voteForThanksBody);
                editor.InsertDocument(htmlDocument, insertOptions);

                var signTable = "<p>" +
                    "<table style ='width:100%'>" +
                         "<tr>" +
                             "<td style = 'width:50%' > Place </td>" +
                              "<td style = 'width:50%' ><b> Chairman </b></td>" +
                           "</tr>" +
                           "<tr>" +
                               "<td> Date of entering minutes </td>" +
                                  "<td> Date of signing minutes </td> " +
                                 "</tr > " +
                             "</table> " +
                    "</p>";

                if (generateFor != "Draft Minutes")
                {
                    signTable = "<p>" +
                    "<table style ='width:100%'>";
                    var minutesDetails = objIMeeting_Service.GetMinutesDetails(meetingId);

                    if(minutesDetails !=null)
                    {
                        if(!string.IsNullOrEmpty(minutesDetails.Place))
                        {
                            signTable += "<tr>" +
                             "<td style = 'width:50%' > Place : "+ minutesDetails.Place+"</td>" +
                              "<td style = 'width:50%' ><b> Chairman </b></td>" +
                           "</tr>";
                        }
                        else
                        {
                            signTable += "<tr>" +
                            "<td style = 'width:50%' > Place </td>" +
                             "<td style = 'width:50%' ><b> Chairman </b></td>" +
                          "</tr>";
                        }
                        signTable += "<tr>";
                        if(minutesDetails.DateOfEntering !=null)
                        {
                            signTable += "<td> Date of entering minutes : "+Convert.ToDateTime(minutesDetails.DateOfEntering).ToString("MMM dd, yyyy") +"</td>";
                        }
                        else
                        {
                            signTable += "<td> Date of entering minutes </td> ";
                        }

                        if (minutesDetails.DateOfSigning != null)
                        {
                            signTable += "<td> Date of signing minutes : " + Convert.ToDateTime(minutesDetails.DateOfSigning).ToString("MMM dd, yyyy") + "</td>";
                        }
                        else
                        {
                            signTable += "<td> Date of signing minutes </td> ";
                        }

                        signTable += "</tr>";
                    }
                    else
                    {
                        signTable += "<tr>" +
                             "<td style = 'width:50%' > Place </td>" +
                              "<td style = 'width:50%' ><b> Chairman </b></td>" +
                           "</tr>" +
                           "<tr>" +
                               "<td> Date of entering minutes </td>" +
                                  "<td> Date of signing minutes </td>" +
                                 "</tr> ";
                    }
                    signTable += "</table></p>";
                }

                htmlDocument = htmlProvider.Import(signTable);
                editor.InsertDocument(htmlDocument, insertOptions);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }

        public FileDataVM NewDraftMinutesFileData(long meetingId, string generateFor)
        {
            var result = new FileDataVM();
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var meeting = objIMeeting_Service.GetMeetingForDocumentName(customerId, meetingId);

                string path = "~/Areas/BM_Management/Documents/" + customerId + "/" + meeting.Entityt_Id + "/Meeting/" + meetingId;
                string fileName = generateFor + " - " + meeting.MeetingTypeName + " " + meeting.TypeName + " [ " + meeting.MeetingSrNo + " ] " + meeting.FY_CY + ".docx";

                DocxFormatProvider provider = new DocxFormatProvider();
                RadFlowDocument document = CreateMinutesDocument(meetingId, customerId, generateFor);
                Byte[] bytes = provider.Export(document);

                result.FileName = fileName;
                result.FilePath = path;
                result.FileData = bytes;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public PartialViewResult Edit_Minutes(long MeetingId, long MappingId)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIMeeting_Service.GetMOM(MeetingId, MappingId, CustomerId);
            return PartialView("_MOM_Edit", model);
        }

        public PartialViewResult View_Minutes(long MeetingId, long MappingId)
        {
            var model = objIMeeting_Service.PreviewMOM(MappingId);
            return PartialView("_MOM_View", model);
        }
        [HttpPost]
        public PartialViewResult Save_Minutes(MeetingAgendaMappingMOMVM model)
        {
            if(ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                model = objIMeeting_Service.SaveMOM(model, UserID);
                ModelState.Clear();
            }
            else
            {
                model.Error = true;
                model.Message = "Validation Fails.";
            }
            
            return PartialView("_MOM_Edit", model);
        }

        [HttpPost]
        public JsonResult Get_Minutes(long MeetingId, long MappingId)
        {
            return Json( new { result = objIMainMeeting.GetAgendabyIDforMainContent(MappingId) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScheduleOnIDForMeetingEvent(long meetingId, string meetingEvent)
        {
            var scheduleOnID = objICompliance_Service.GetScheduleOnIDForMeetingEvent(meetingId, meetingEvent);
            return Json(new { result = scheduleOnID }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Minutes Details (date of entering etc)
        public PartialViewResult GetMinutesDetails(long meetingId)
        {
            var model = objIMeeting_Service.GetMinutesDetails(meetingId);

            if (model == null)
            {
                model = new MeetingMinutesDetailsVM() { MOM_MeetingId = meetingId};
            }
            return PartialView("_MOM_Details", model);
        }
        [HttpPost]
        public PartialViewResult SaveMinutesDetails(MeetingMinutesDetailsVM model,string commandName)
        {
            if(ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if(commandName == "circulateDraft")
                {
                    model = objIMeeting_Service.CirculateMinutes(model, userID);

                    if(model.Success == true)
                    {
                        #region Generate Compliance document on circulate Draft
                        //#region Update Schedule on Dates
                        //objIMeeting_Compliances.CalculateScheduleDate(model.MOM_MeetingId, userID);
                        //#endregion

                        var objFileData = NewDraftMinutesFileData(model.MOM_MeetingId, "Draft Minutes");
                        //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "CirculateDraft");
                        var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "CirculateDraft", true, 0, DateTime.Now, objFileData, userID);

                        #region Commented on 19 Aug 2020
                        //long? complianceScheduleOnID = null, transactionID = null;
                        //if (objFileData != null)
                        //{
                        //    int fileVersion = 0;
                        //    if (lstClosedTransactions != null)
                        //    {
                        //        if (lstClosedTransactions.Count > 0)
                        //        {
                        //            complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                        //            transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                        //            fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                        //        }
                        //    }

                        //    fileVersion++;
                        //    objFileData.Version = Convert.ToString(fileVersion);
                        //    objFileData = objIFileData_Service.Save(objFileData, userID);

                        //    if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                        //    {
                        //        objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                        //    }
                        //}
                        #endregion

                        #endregion
                    }
                }
                else if (commandName == "finalizeDraft")
                {
                    model = objIMeeting_Service.FinalizedMinutes(model, userID);

                    if (model.Success == true)
                    {
                        #region Generate Compliance document on Finalized

                        var objFileData = NewDraftMinutesFileData(model.MOM_MeetingId, "Minutes");

                        //var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "FinalizedMinutes");
                        var lstClosedTransactions = objIComplianceTransaction_Service.CreateTransactionOnMeetingEvent(model.MOM_MeetingId, "FinalizedMinutes", false, 0, DateTime.Now, objFileData, userID);

                        #region Commented on 19 Aug 2020
                        long? complianceScheduleOnID = null, transactionID = null;
                        if (objFileData != null)
                        {
                            int fileVersion = 0;
                            if (lstClosedTransactions != null)
                            {
                                if (lstClosedTransactions.Count > 0)
                                {
                                    complianceScheduleOnID = lstClosedTransactions.FirstOrDefault().ComplianceScheduleOnID;
                                    transactionID = lstClosedTransactions.FirstOrDefault().TransactionID;

                                    fileVersion = objIFileData_Service.GetFileVersion((long)complianceScheduleOnID, objFileData.FileName);
                                }
                            }

                            fileVersion++;
                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && complianceScheduleOnID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping((long)transactionID, (long)complianceScheduleOnID, objFileData.FileID, 1);
                            }
                        }
                        #endregion

                        #endregion
                    }
                }
                else
                {
                    model = objIMeeting_Service.SaveMinutesDetails(model, userID);
                }
            }
            return PartialView("_MOM_Details", model);
        }
        //Used in virtual meeting
        [HttpPost]
        public PartialViewResult SaveMinutesDetailsVirtual(MeetingMinutesDetailsVM model, string commandName)
        {
            if (ModelState.IsValid)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                if (commandName == "finalizeDraft")
                {
                    //var objFileDataDraft = NewDraftMinutesFileData(model.MOM_MeetingId, "Draft Minutes");
                    var objFileDataMinutes = NewDraftMinutesFileData(model.MOM_MeetingId, "Minutes");
                    model = objIMeeting_Service.FinalizedMinutesOfVirtualMeeting(model, objFileDataMinutes, userID);
                }
                else
                {
                    model = objIMeeting_Service.SaveMinutesDetails(model, userID);
                }
            }
            return PartialView("_MOM_DetailsVirtual", model);
        }
        //
        #endregion
        public ActionResult Meetings_Read_Participants([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            return null;//Json(objIMeeting_Service.GetMeetingParticipants(MeetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Meeting Details
        public PartialViewResult SigningAuthority(long id, int entityId)
        {
            var model = objIMeeting_Service.GetMeetingOtherDetails(id, entityId);
            return PartialView("_SigningAuthority", model);
        }
        [HttpPost]
        public PartialViewResult SaveMeetingOtherDetails(MeetingDetailsVM obj)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            var model = objIMeeting_Service.SaveMeetingOtherDetails(obj, userID);
            return PartialView("_SigningAuthority", model);
        }
        #endregion
    }
}