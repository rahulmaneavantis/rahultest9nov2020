﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Drawing;
using System.Dynamic;
//using GleamTech.DocumentUltimate;
//using GleamTech.DocumentUltimate.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class TemplateController : Controller
    {
        IAgendaMasterTemplate objIAgendaMasterTemplate;
        public TemplateController(IAgendaMasterTemplate obj)
        {
            objIAgendaMasterTemplate = obj;
        }
        public ActionResult Editing_Custom(long? AgendaMasterId)
        {
            if(AgendaMasterId ==null)
            {
                AgendaMasterId = 0;
            }
            PopulateCategories();

            dynamic expando = new ExpandoObject();
            var model = expando as IDictionary<string, object>;
            model.Add("AgendaMasterId", AgendaMasterId);
            var parentId = objIAgendaMasterTemplate.GetParentId(AgendaMasterId);
            model.Add("PostAgendaId", parentId);

            return PartialView("Editing_Custom", model);
        }

        [HttpPost]
        public ActionResult ImportTemplates(long? AgendaMasterId)
        {
            var result = objIAgendaMasterTemplate.ImportTemplates(AgendaMasterId);
            return Json(new { Templates = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Editing_Custom1(long? AgendaMasterId)
        {
            if (AgendaMasterId == null)
            {
                AgendaMasterId = 0;
            }
            PopulateCategories();
            return View((long)AgendaMasterId);
        }
        public ActionResult EditingCustom_Read([DataSourceRequest] DataSourceRequest request, long? AgendaMasterId)
        {
            if(AgendaMasterId == null)
            {
                AgendaMasterId = 0;
            }
            return Json(objIAgendaMasterTemplate.GetAll((long)AgendaMasterId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Update([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<TemplateViewModel> products)
        {
            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    product.UserId = UserId;
                    product.TemplateName = product.TemplateName.ToUpper();
                    objIAgendaMasterTemplate.Update(product);
                }
            }

            return Json(products.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Create([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<TemplateViewModel> products)
        {
            var results = new List<TemplateViewModel>();

            if (products != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                foreach (var product in products)
                {
                    product.UserId = UserId;
                    product.TemplateName = product.TemplateName.ToUpper();
                    objIAgendaMasterTemplate.Create(product);

                    results.Add(product);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Destroy([DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")]IEnumerable<TemplateViewModel> products)
        {
            foreach (var product in products)
            {
                objIAgendaMasterTemplate.Delete(product);
            }
            return Json(products.ToDataSourceResult(request, ModelState));
        }


        private void PopulateCategories()
        {
            var categories = objIAgendaMasterTemplate.GetCategory();

            ViewData["categories"] = categories;
            ViewData["defaultCategory"] = categories.First();
        }
    }
}