﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http.ModelBinding;
using Kendo.Mvc.UI;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CircularResolutionListController : Controller
    {   
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddEditNewCircular(int ID)
        {
            VM_CircularResolution vmnewMeeting = new VM_CircularResolution();
            if (ID > 0)
            {
                CircularResolution obj = new CircularResolution();
                vmnewMeeting = obj.GetCircularDetailByID(ID);
            }
            return PartialView("_AddEditCircular", vmnewMeeting);
        }

        public ActionResult CircularResolutionList()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveCircular(VM_CircularResolution _objCircular)
        {
            if (ModelState.IsValid)
            {
                _objCircular.customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                _objCircular.loggedinUserID = Convert.ToInt32(AuthenticationHelper.UserID);

                CircularResolution obj = new CircularResolution();
                _objCircular = obj.CreateUpdate_CircularResolution(_objCircular);
            }
            return PartialView("_AddEditCircular", _objCircular);
        }
    }
}