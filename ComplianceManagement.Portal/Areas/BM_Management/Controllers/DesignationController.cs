﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class DesignationController : Controller
    {
        // GET: BM_Management/Designation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int id)
        {
            IDesignationMaster objIDesignation = new DesignationMaster();
            VMDesignation objVMDesignation = new VMDesignation();
            if (id > 0)
            {
                objVMDesignation.ID = id;
                VMDesignation result = objIDesignation.GetDesignationMaster(id);
                return PartialView("Create", result);
            }
            else
            {
                return PartialView("Create", objVMDesignation);
            }
        }

        public ActionResult SaveUpdateDesignationDetails(VMDesignation ObjVMDesignation)
        {
            if (ModelState.IsValid)
            {
                IDesignationMaster ObjIDesignationMaster = new DesignationMaster();
                if (ObjVMDesignation.ID > 0)
                {
                    ObjVMDesignation.UpdatedBy = AuthenticationHelper.UserID;
                    ObjVMDesignation.UpdatedOn = DateTime.Now;
                    ObjVMDesignation.IsDeleted = false;
                    VMDesignation ObjResult = ObjIDesignationMaster.SaveUpdateDesignationDetials(ObjVMDesignation);
                    return PartialView("Create", ObjVMDesignation);
                }
                else
                {
                    ObjVMDesignation.CreatedBy = AuthenticationHelper.UserID;
                    ObjVMDesignation.CreatedOn = DateTime.Now;
                    ObjVMDesignation.IsDeleted = false;
                    VMDesignation ObjResult = ObjIDesignationMaster.SaveUpdateDesignationDetials(ObjVMDesignation);
                    return PartialView("Create", ObjResult);
                }
            }
            else
            {
                ObjVMDesignation.ErrorMessage = "Something went wrong...!";
                return PartialView("Create", ObjVMDesignation);
            }
        }
    }
}