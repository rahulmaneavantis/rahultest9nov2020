﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingComplianceMappingController : Controller
    {
        IMeetingComplianceMapping obj;
        public MeetingComplianceMappingController(IMeetingComplianceMapping obj)
        {
            this.obj = obj;
        }


        // GET: BM_Management/MeetingComplianceMapping
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListofMeetingType()
        {
            PopulateCategories();
            return View();
        }

        public ActionResult OpenComplianceListForMapping(int MeetingTypeId,int EntityTypeId)
        {
            VM_MeetingCamplianceMapping objmapingcompliance = new VM_MeetingCamplianceMapping();
            objmapingcompliance.MeetingTypeId = MeetingTypeId;
            objmapingcompliance.EntityType = EntityTypeId;
            PopulateCategories();
            return PartialView("_ComplianceListForMeetingMapping", objmapingcompliance);
        }

        public ActionResult MeetingMappingDetails([DataSourceRequest] DataSourceRequest request, int EntityType,long MeetingTypeID)
        {
            PopulateCategories();
            List<AgendaComplianceEditorVM> objagendacompliance = new List<AgendaComplianceEditorVM>();
            if (EntityType > 0 && MeetingTypeID>0)
            {

                objagendacompliance = obj.GetComplianceMappingDetails(EntityType, MeetingTypeID);
            }
            return Json(objagendacompliance.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult save_checkedAgendaComplianceApping([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VMCompliences> complianceListmapping)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool flag = false;
            if (complianceListmapping != null && ModelState.IsValid)
            {
                foreach (var complianceItem in complianceListmapping)
                {
                    if (complianceItem.IsCheked)
                    {
                        obj.AddMeetingItem(complianceItem, UserID, CustomerID);

                        flag = true;
                    }
                }
            }

            return Json(complianceListmapping.ToDataSourceResult(request, ModelState));
        }

        private void PopulateCategories()
        {
            var listDayorHoure = new List<DaysOrHoursViewModel>
            {
             new DaysOrHoursViewModel {DaysOrHoursName="Select",DaysOrHours="" },
             new DaysOrHoursViewModel {DaysOrHoursName="Day(s)",DaysOrHours="D" },
             new DaysOrHoursViewModel {DaysOrHoursName="Hour(s)",DaysOrHours="H" },
             new DaysOrHoursViewModel {DaysOrHoursName="Minute(s)",DaysOrHours="M" }
            };
            ViewData["listDayorHoureData"] = listDayorHoure;
            //ViewBag.listDayorHoureData = listDayorHoure;


            var listBeforeAfter = new List<BeforeAfterViewModel>
         {
          new BeforeAfterViewModel {BeforeAfterName="Select",BeforeAfter="" },
          new BeforeAfterViewModel {BeforeAfterName="After",BeforeAfter="A" },
          new BeforeAfterViewModel {BeforeAfterName="Before",BeforeAfter="B" }

         };
            ViewData["BeforeAfterData"] = listBeforeAfter;

            var listDayType = new List<DayTypeViewModel>
            {
          new DayTypeViewModel {DayTypeName="Select",DayType="" },
          new DayTypeViewModel {DayTypeName="Clear Working Day(s)",DayType="W" },
          new DayTypeViewModel {DayTypeName="Normal Day(s)",DayType="C" },
            };
            ViewData["listDayType"] = listDayType;
            //ViewBag.listDayType = listDayType;

            var listMeetingLevel = new List<MeetingViewModel>
            {
              new MeetingViewModel {MeetingLevelName="Select",MeetingLevelId="" },
              new MeetingViewModel {MeetingLevelName="Notice",MeetingLevelId="Notice" },
              new MeetingViewModel {MeetingLevelName="CirculateDraft",MeetingLevelId="CirculateDraft" },
              new MeetingViewModel {MeetingLevelName="FinalizedMinutes",MeetingLevelId="FinalizedMinutes" },
            };
            ViewData["listMeetingLevel"] = listMeetingLevel;
        }

        public JsonResult BindDayHours(string dayhoursID)
        {
            return Json(obj.GetHoureMinutesDay(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult BindDayType(string DayTypeNames)
        {
           return Json(obj.getDayType(DayTypeNames), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ComplianceDetails_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AgendaComplianceEditorVM> complianceListDetails)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool flag = false;
            if (complianceListDetails != null && ModelState.IsValid)
            {
                foreach (var complianceItem in complianceListDetails)
                {
                    obj.AddMeetingMappingDetails(complianceItem, UserID, CustomerID);

                    flag = true;
                }
            }

            return Json(complianceListDetails.ToDataSourceResult(request, ModelState));
        }
    }
}