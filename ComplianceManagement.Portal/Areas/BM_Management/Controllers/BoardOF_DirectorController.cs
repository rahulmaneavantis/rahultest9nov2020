﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using static BM_ManegmentServices.VM.VMBoard_Director;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class BoardOF_DirectorController : Controller
    {
        // GET: BM_Management/BoardOF_Director

        IBoard_DirectorMaster obj;
        ICommitteeComp objService;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

        public BoardOF_DirectorController(IBoard_DirectorMaster obj, ICommitteeComp objService)
        {
            this.obj = obj;
            this.objService = objService;
        }
        public ActionResult Index()
        {
            VMBoard_Director objboard_director = new VMBoard_Director();
            objboard_director.DirectorAge = new List<Director_Age>();
            objboard_director.Directors = new List<Director_Rule>();
            objboard_director.MeetingDetails = new MeetingDetails();
            objboard_director.Quorum = new Composition();

            return View(objboard_director);
        }
        public ActionResult RulesFor(int EntityType, string command_name)
        {
            VMBoard_Director objboard_director = new VMBoard_Director() { EntityType = EntityType };
            //MeetingDetails objmeetingdetails = new MeetingDetails() { Entity_Type = EntityType };
            objboard_director.MeetingDetails = new MeetingDetails() { Entity_Type = EntityType };
            objboard_director.Quorum = new Composition() { Entity_Type = EntityType };
            GetAllRules(10, EntityType, CustomerId, 0);
            var _objgetage = obj.GetAllDirectorAge(CustomerId, EntityType);
            if (_objgetage.Count > 0)
            {
                objboard_director.DirectorAge = new List<Director_Age>();
                objboard_director.DirectorAge.AddRange(_objgetage);
            }
            else
            {
                objboard_director.DirectorAge = new List<Director_Age>();
            }

            var _objgetDirDis = obj.GetAllDirectorDiscription(CustomerId, EntityType);
            if (_objgetDirDis.Count > 0)
            {
                objboard_director.Directors = new List<Director_Rule>();
                objboard_director.Directors.AddRange(_objgetDirDis);
            }
            else
            {
                objboard_director.Directors = new List<Director_Rule>();
            }
            objboard_director.MeetingDetails = objService.GetMeetingRule(10, EntityType, null, 0);
            if (objboard_director.MeetingDetails == null)
            {
                objboard_director.MeetingDetails = new MeetingDetails() { Entity_Type = EntityType, CommitteeId = 10 };
            }

            objboard_director.EntityType = EntityType;
            return PartialView("_partialViewForRule", objboard_director);
        }

        [HttpPost]
        public ActionResult SaveUpdateDirector_Rules(VMBoard_Director _objBoardDirector, string command_name)
        {
            string ReturnPartialView = "_PartialViewDirector_Details";
            if (_objBoardDirector.RoleType == "DD")
            {
                ReturnPartialView = "_PartialViewDirector_Details";
                _objBoardDirector.Designation_dirId = _objBoardDirector.Designation_dirId;
            }

            if (_objBoardDirector.BDId > 0)
            {

                _objBoardDirector = obj.Update_DirectorsRules(_objBoardDirector);


            }
            else
            {
                _objBoardDirector = obj.Create_DirectorsRules(_objBoardDirector, CustomerId);
            }
            ModelState.Clear();
            if (_objBoardDirector.Messages.Success == true)
            {
                _objBoardDirector.BDId = 0;
                _objBoardDirector.Max = null;
                _objBoardDirector.Min = null;
            }

            _objBoardDirector.Directors = obj.GetAllDirectorDiscription(CustomerId, _objBoardDirector.EntityType);


            return PartialView(ReturnPartialView, _objBoardDirector);
        }

        public ActionResult SaveUpdateDirector_Age(VMBoard_Director _objBoardDirector, string command_name)
        {
            string ReturnPartialView = "_PartialViewAge";
            if (_objBoardDirector.RoleType == "A")
            {
                ReturnPartialView = "_PartialViewAge";
                _objBoardDirector.Designation_dirId = _objBoardDirector.Designation_dirId;
            }

            if (_objBoardDirector.BDId > 0)
            {
                _objBoardDirector = obj.Update_DirectorAge(_objBoardDirector, CustomerId);
            }
            else
            {
                _objBoardDirector = obj.Create_DirectorAge(_objBoardDirector, CustomerId);
            }
            ModelState.Clear();

            _objBoardDirector.DirectorAge = obj.GetAllDirectorAge(CustomerId, _objBoardDirector.EntityType);

            if (_objBoardDirector.Messages.Success)
            {
                _objBoardDirector.BDId = 0;
                _objBoardDirector.Max = null;
                _objBoardDirector.Min = null;
            }

            return PartialView(ReturnPartialView, _objBoardDirector);
        }

        [HttpPost]
        public ActionResult EditMinimumDirector(int Id, int Entity_Type, string command_name)
        {
            string _resltView = string.Empty;
            VMBoard_Director modal = new VMBoard_Director();

            if (command_name == "deleteRule")
            {

            }
            else if (command_name == "Directorage")
            {
                _resltView = "_PartialViewAge";
                modal = obj.GetDirectorAge(Id, Entity_Type);
            }
            else if (command_name == null)
            {
                modal = obj.GetDirector(Id, Entity_Type, 0);
                _resltView = "_PartialViewDirector_Details";
            }

            ModelState.Clear();
            modal.Directors = obj.GetAllDirectorDiscription(CustomerId, Entity_Type);
            modal.DirectorAge = obj.GetAllDirectorAge(CustomerId, Entity_Type);
            return PartialView(_resltView, modal);
        }

        [HttpPost]
        public ActionResult SaveUpdateMeeting(MeetingDetails obj, string command_name)
        {
            obj.Customer_Id = null;
            if (command_name == "btnNoofMeeting")
            {
                ModelState.Clear();
                if (obj.lstMeetingSchedule == null)
                {
                    obj.lstMeetingSchedule = new List<MeetingSchedule>() { };
                }

                if (obj.MinMeeting < obj.lstMeetingSchedule.Count)
                {
                    List<MeetingSchedule> lstTemp = new List<MeetingSchedule>();
                    for (int i = 0; i < obj.MinMeeting; i++)
                    {
                        lstTemp.Add(obj.lstMeetingSchedule[i]);
                    }
                    obj.lstMeetingSchedule = lstTemp;
                }
                else
                {
                    for (int i = obj.lstMeetingSchedule.Count; i < obj.MinMeeting; i++)
                    {
                        obj.lstMeetingSchedule.Add(new MeetingSchedule() { MeetingScheduleId = 0, CommitteeId = 10, Entity_Type = obj.Entity_Type, Customer_Id = obj.Customer_Id, Entity_Id = obj.Entity_Id });
                    }
                }
            }
            else
            {
                ModelState.Clear();
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                obj.CreatedBy = UserID;
                obj.UpdatedBy = UserID;
                if (obj.lstMeetingSchedule != null)
                {
                    foreach (var item in obj.lstMeetingSchedule)
                    {
                        item.CreatedBy = UserID;
                        item.UpdatedBy = UserID;
                    }
                }
                objService.CreateOrUpdate(obj);
            }
            return PartialView("_Rule_Meeting", obj);
        }

        [NonAction]
        public void GetAllRules(int MeetingtypeId, int EntityType, int? CustomerId, int? EntityId)
        {
            ViewBag.lstQuorum = objService.GetAllComposition(MeetingtypeId, EntityType, "Q", null, EntityId);
        }


        [HttpPost]
        public ActionResult SaveUpdateMinimumDirector(Composition obj, string command_name)
        {
            string view = "_Rule_MinimumDirector";
            if (obj.RuleType == "Q")
            {
                view = "_Rule_Quorum";
                obj.DesignationId = obj.DesignationIdQuorum;
                obj.CommitteeId = 10;
            }
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            if (obj.RuleId > 0)
            {
                obj.UpdatedBy = UserID;
                obj.UpdatedOn = DateTime.Now;
                obj = objService.UpdateRule(obj);
            }
            else
            {
                obj.CreatedBy = UserID;
                obj.CreatedOn = DateTime.Now;
                obj = objService.CreateRule(obj);
            }
            ModelState.Clear();

            GetAllRules(10, obj.Entity_Type, obj.Customer_Id, obj.Entity_Id);

            if (obj.Message != null)
            {
                if (obj.Message.Success)
                {
                    obj.RuleId = 0;
                    obj.DesignationCount = null;
                    obj.DesignationId = null;
                    obj.DesignationNumerator = null;
                    obj.DesignationDenominator = null;
                    obj.DesignationIdQuorum = null;
                }
            }
            return PartialView(view, obj);
        }

        [HttpPost]
        public ActionResult EditMinimumDirectorforQuorum(int Rule_Id, int Committee_Id, string command_name)
        {
            var model = objService.GetComposition(Rule_Id, Committee_Id);

            if (command_name == "deleteRule")
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                var success = objService.DeleteRule(Rule_Id, Committee_Id, UserID);

                model.RuleId = 0;
                model.DesignationCount = null;
                model.DesignationId = null;
                model.DesignationNumerator = null;
                model.DesignationDenominator = null;
                model.DesignationIdQuorum = null;
            }

            ModelState.Clear();
            GetAllRules(model.CommitteeId, model.Entity_Type, model.Customer_Id, model.Entity_Id);

            if (model.RuleType.Trim() == "Q")
            {
             
                model.DesignationIdQuorum = model.DesignationId;
            }
            return PartialView("_Rule_Quorum", model);
        }


        public ActionResult GetBoardofDirectorRule(int EntityId)
        {
            int EntityType = 0;
            List<Director_Rule> _objgerrule = new List<Director_Rule>();
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            if (EntityId > 0)
            {
                EntityType = obj.GetEntityType(EntityId, customerId);

                _objgerrule = obj.GetAllDirectorDiscription(customerId, EntityType);
            }
            return Json(_objgerrule);
        }
    }
}