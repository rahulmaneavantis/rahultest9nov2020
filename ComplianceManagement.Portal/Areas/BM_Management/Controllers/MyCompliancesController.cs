﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyCompliancesController : Controller
    {
        ICompliance_Service objICompliance_Service;
        ICompliance objICompliance;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IFileData_Service objIFileData_Service;
        IMeeting_Compliances objIMeeting_Compliances;

        public MyCompliancesController(ICompliance_Service objCompliance_Service, IMeeting_Compliances obj, ICompliance objCompliance, IComplianceTransaction_Service objComplianceTransaction_Service, IFileData_Service objFileData_Service)
        {
            objICompliance_Service = objCompliance_Service;
            objIMeeting_Compliances = obj;
            objICompliance = objCompliance;
            objIComplianceTransaction_Service = objComplianceTransaction_Service;
            objIFileData_Service = objFileData_Service;
        }

        #region My Compliances
        public ActionResult Closed()
        {
            return MyCompliances("Closed");
        }
        public ActionResult Upcoming()
        {
            return MyCompliances("Upcoming");
        }
        public ActionResult Overdue()
        {
            return MyCompliances("Overdue");
        }
        public ActionResult MyCompliances(string status)
        {
            var model = new MyCompliancesVM();

            if (AuthenticationHelper.Role == SecretarialConst.Roles.HDCS)
            {
                model.RoleId = 4;
            }
            else
            {
                model.RoleId = 3;
            }

            model.StatusFilter = status;

            switch (status)
            {
                case "Overdue":
                case "Upcoming":
                    model.StatusId = 1;
                    break;
                case "Closed":
                    model.StatusId = 4;
                    break;
                default:
                    model.StatusId = 1;
                    break;
            }
            return View("MyCompliances", model);
        }

        [HttpPost]
        public ActionResult GetMyCompliances(MyCompliancesVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICompliance_Service.GetMyCompliances(model, userID, customerID);

            if(result !=null)
            {
                var lstMeeting = result.Select(r => new { r.EntityId, r.RoleID, r.MeetingID, r.MeetingTitle }).Distinct().ToList();
                var lstDirector = result.Where(k=>k.DirectorId >0).Select(r => new { r.DirectorId, r.CompanyName}).Distinct().ToList();
                return Json(new { MyCompliances = result, Meetings = lstMeeting, Directors = lstDirector }, JsonRequestBehavior.AllowGet);
            }
            return Json( new { MyCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMyCompliances_Director(MyCompliancesVM model)
        {
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

            var result = objICompliance_Service.GetMyCompliances(userID, customerID, model.StatusId, model.RoleId);

            if (result != null)
            {
                var lstMeeting = result.Select(r => new { r.EntityId, r.RoleID, r.MeetingID, r.MeetingTitle }).Distinct().ToList();
                var lstDirector = result.Where(k => k.DirectorId > 0).Select(r => new { r.DirectorId, r.DirectorName }).Distinct().ToList();
                return Json(new { MyCompliances = result, Meetings = lstMeeting, Directors = lstDirector }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { MyCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Schedule -> Meeting Level compliances
        public ActionResult MeetingComplianceSchedule(long meetingId)
        {
            dynamic model = new ExpandoObject();
            model.MeetingID = meetingId;
            return PartialView("_ComplianceSchedule", model);
        }
        public ActionResult ComplianceSchedule_Read([DataSourceRequest] DataSourceRequest request, long meetingId)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ScheduleOn", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Compliances.GetMeetingComplianceSchedule(meetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ScheduleDetails
        public PartialViewResult ScheduleDetails(long id, int entityID, int? roleID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            //var model = objIMeeting_Compliances.GetScheduleDetails(id, entityID, customerId);
            var model = objIMeeting_Compliances.GetScheduleDetailsNew(id);
            if (model != null)
            {
                model.RoleID = roleID;
                loadComplianceStaus(model.ScheduleOnID, userID, roleID);
            }

            return PartialView("_ComplianceScheduleDetails", model);
        }

        public ActionResult ComplianceSchedule_Documents([DataSourceRequest] DataSourceRequest request, long id)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("FileID", System.ComponentModel.ListSortDirection.Descending));
            }
            return Json(objIMeeting_Compliances.GetComplianceDocuments(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Compliance status update
        [HttpPost]
        public PartialViewResult ComplianceStatusUpdate(ComplianceTransactionVM model, HttpPostedFileBase[] complianceDoc, HttpPostedFileBase[] workingDoc, string btnRejectStatus)
        {
            if (ModelState.IsValid)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                model.CreatedBy = userID;
                model.CreatedByText = AuthenticationHelper.User;

                if(model.RoleID == SecretarialConst.RoleID.REVIEWER && btnRejectStatus == "Reject")
                {
                    model.StatusId = 8;
                }
                model = objIComplianceTransaction_Service.CreateTransaction(model);

                #region File Data -> complianceDoc
                if (complianceDoc != null)
                {
                    string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                    foreach (var item in complianceDoc)
                    {
                        if (item.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            item.InputStream.CopyTo(target);

                            int fileVersion = 0;

                            var objFileData = new FileDataVM()
                            {
                                FileData = target.ToArray(),
                                FileName = item.FileName,
                                FilePath = path
                            };

                            fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                            fileVersion++;

                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && model.TransactionID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 1);
                            }
                        }
                    }
                }
                #endregion

                #region File Data -> workingDoc
                if (workingDoc != null)
                {
                    string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                    foreach (var item in workingDoc)
                    {
                        if (item.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            item.InputStream.CopyTo(target);

                            int fileVersion = 0;

                            var objFileData = new FileDataVM()
                            {
                                FileData = target.ToArray(),
                                FileName = item.FileName,
                                FilePath = path
                            };

                            fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                            fileVersion++;

                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && model.TransactionID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 2);
                            }
                        }
                    }
                }
                #endregion

                loadComplianceStaus(model.ComplianceScheduleOnID, userID, model.RoleID);
            }
            return PartialView("_ComplianceStatusUpdate", model);
        }

        public void loadComplianceStaus(long ComplianceScheduleOnID, int userID, int? roleID)
        {
            if(roleID == null)
            {
                ViewBag.ComplianceUpdateStatus = objICompliance.GetComplianceUpdateStatus(ComplianceScheduleOnID, userID);
            }
            else
            {
                ViewBag.ComplianceUpdateStatus = objICompliance.GetComplianceUpdateStatus(ComplianceScheduleOnID, userID, roleID);
            }
        }

        #endregion


        #region Director-My Compliances

        public ActionResult DirectorMyCompliances(string status)
        {
            var model = new MyCompliancesVM();
            model.RoleId = 4;
           
            model.StatusFilter = status;

            switch (status)
            {
                case "Overdue":
                case "Upcoming":
                    model.StatusId = 1;
                    break;
                case "Closed":
                    model.StatusId = 4;
                    break;
                default:
                    model.StatusId = 1;
                    break;
            }

            return View("DirectorMyCompliances", model);
        }

        #endregion
    }
}