﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    //[ValidateAntiForgeryToken]
    public class EntityMasterController : Controller
    {
        IEntityMaster objEntityMaster;
        ICommitteeMaster objICommitteeMaster;

        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();

        public EntityMasterController(IEntityMaster obj, ICommitteeMaster objCommitteeMaster)
        {
            objEntityMaster = obj;
            objICommitteeMaster = objCommitteeMaster;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Entity Creation Added by Ruchi some Modification done on 12th of March Related Avacom Entity Details
        //[ValidateAntiForgeryToken]
        [PageViewFilter]
        public ActionResult Getview(int Entity_Id)
        {
            if (Entity_Id == 1 || Entity_Id == 2 || Entity_Id == 0 || Entity_Id == 9)
            {
                return PartialView("_PublicPrivatePartialView", new Pravate_PublicVM() { Entity_Type = Entity_Id, CustomerBranchId = 0 });
            }
            else if (Entity_Id == 10)
            {
                return PartialView("_PublicPrivatePartialView", new Pravate_PublicVM() { Entity_Type = Entity_Id, CustomerBranchId = 0, IS_Listed = true });
            }
            else if (Entity_Id == 3)
            {
                return PartialView("_LLPPartialView", new LLPVM() { Entity_Type = Entity_Id, CustomerBranchId = 0 });
            }
            else if (Entity_Id == 4)
            {
                return PartialView("_TrustPartialView", new TrustVM() { Entity_Type = Entity_Id, CustomerBranchId = 0 });
            }
            else if (Entity_Id == 5)
            {
                return PartialView("_BodyCorporatePartialView", new BodyCorporateVM() { Entity_Type = Entity_Id, CustomerBranchId = 0 });
            }
            else
            {
                return PartialView("_PublicPrivatePartialView", new Pravate_PublicVM() { Entity_Type = Entity_Id, CustomerBranchId = 0 });
            }

        }

        [PageViewFilter]
        public ActionResult GetEntityDtls()
        {

            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;
            EntityMasterVM obj = new EntityMasterVM();
            if (authRecord != null)
            {
                obj.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Entity") select x.Addview).FirstOrDefault();
                obj.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Entity") select x.Editview).FirstOrDefault();
            }
            return View(obj);
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult Create(Pravate_PublicVM _objentity)
        {
            try
            {              
                if (ModelState.IsValid)
                {
                    if (_objentity.Id == 0)
                    {
                        _objentity = objEntityMaster.CreatePublicPrivate(_objentity, CustomerId,UserId);
                    }
                    else
                    {
                        _objentity = objEntityMaster.UpdatePublicPrivateEntity(_objentity, CustomerId, UserId);
                    }
                }
                else
                {
                    _objentity.errorMessage = true;
                    _objentity.successErrorMessage ="Validation Issues occured";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objentity.errorMessage = true;
                _objentity.successErrorMessage = "Server error occured";
            }
            return PartialView("_PublicPrivatePartialView", _objentity);
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateLLP(LLPVM _objentity)
        {
            try
            {
                string Message = "";
                if (ModelState.IsValid)
                {
                    #region Create Entity 

                    #endregion

                    if (_objentity.Id == 0)
                    {

                        _objentity = objEntityMaster.CreateLLP(_objentity, CustomerId,UserId);

                    }
                    else
                    {
                        _objentity = objEntityMaster.UpdateLLP(_objentity, CustomerId, UserId);
                    }

                }

                return PartialView("_LLPPartialView", _objentity);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_LLPPartialView", _objentity);
            }

        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateTrust(TrustVM _objentity)
        {
            try
            {
                string Message = "";
                if (ModelState.IsValid)
                {
                    #region Create Entity 


                    #endregion

                    if (_objentity.Id == 0)
                    {

                        _objentity = objEntityMaster.Createtrusts(_objentity, CustomerId,UserId);

                    }
                    else
                    {

                        _objentity = objEntityMaster.Updatetrusts(_objentity, CustomerId,UserId);

                    }
                }

                return PartialView("_TrustPartialView", _objentity);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_TrustPartialView", _objentity);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateBodyCorporate(BodyCorporateVM _objentity)
        {
            try
            {
              
                if (ModelState.IsValid)
                {
                    if (_objentity.Id == 0)
                    {
                        _objentity = objEntityMaster.Createbody_Corporate(_objentity, CustomerId,UserId);

                    }

                    else
                    {
                        _objentity = objEntityMaster.UpdateBody_Corporate(_objentity, CustomerId,UserId);
                    }
                }
                return PartialView("_BodyCorporatePartialView", _objentity);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_BodyCorporatePartialView", _objentity);
            }
        }

        [PageViewFilter]
        public ActionResult Edit(int id, int EntityId)
        {
            EntityMasterVM model = new EntityMasterVM();
            try
            {
                if (id > 0 && EntityId > 0)
                {
                    model.Id = id;
                    model.Entity_Type = EntityId;

                    if (EntityId == 3)
                    {
                        model.LLPVM = new LLPVM();
                        model.LLPVM = objEntityMaster.GetLLPDtls(id, EntityId);
                    }
                    else if (EntityId == 4)
                    {
                        model.TrustVM = new TrustVM();
                        model.TrustVM = objEntityMaster.GetTustDtls(id, EntityId);
                    }
                    else if (EntityId == 5)
                    {
                        model.BodyCorporateVM = new BodyCorporateVM();
                        model.BodyCorporateVM = objEntityMaster.GetBodyCorporate(id, EntityId);
                    }
                    else
                    {
                        model.Pravate_PublicVM = new Pravate_PublicVM();
                        model.Pravate_PublicVM = objEntityMaster.GetprivatepublicDtls(id, EntityId);
                    }

                    return PartialView("_AddEntityMaster", model);

                }
                else
                {
                    model.Id = 0;
                    //model.Entity_Type = 1;
                    return PartialView("_AddEntityMaster", model);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_AddEntityMaster", model);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public ActionResult CreateEntitys(EntityMasterVM _objCreateEntity)
        {

            return View("GetEntityDtls");
        }
        [HttpGet]
        [PageViewFilter]
        public ActionResult DeleteEntity(int Id)
        {
            if (ModelState.IsValid)
            {
                // int Id = Convert.ToInt32(id);
                bool result;
                if (Id > 0)
                {
                    result = objEntityMaster.DeleteEntityInfo(Id, CustomerId);
                }


            }
            return View("GetEntityDtls");
        }

        public int GetCompanyType()
        {
            return 0;
        }
        #endregion

        #region Added by Amit Grid Icon's Reponces
        [PageViewFilter]
        public PartialViewResult Applicability(int Entity_Id)
        {
            var capital = objEntityMaster.GetCapital(Entity_Id, CustomerId);
            if (capital <= 0)
            {
                var model = new BM_Rule() { Entity_Id = Entity_Id, Rules = new List<Rules>() };
                model.Messages = new BM_ManegmentServices.VM.Response() { Error = true, Message = "Please fill Capital Details" };

                return PartialView("Applicability", model);
            }
            else
            {
                var model = objEntityMaster.GetBM_Applicability(Entity_Id, capital);
                //if (model.Entity_Type == 10)
                //{
                //    if (model.Rules.Where(k => k.SrNo > 0).Any())
                //    {
                //        //model.SMEListed = true;
                //    }
                //}
                return PartialView("Applicability", model);
            }
        }

        [HttpPost]
        [PageViewFilter]
        public PartialViewResult SaveApplicability(BM_Rule objBM_Rule, string command_name, string rbtnSME)
        {
            if (command_name == "SME" && rbtnSME == "True")
            {
                ModelState.Clear();
                var model = objEntityMaster.GetBM_Applicability(objBM_Rule);
                model.SMEListed = true;

                if (model.Rules != null)
                {
                    model.Rules.ForEach(k => k.Answer = true);
                }

                return PartialView("Applicability", model);
            }

            if (ModelState.IsValid == false)
            {
                return PartialView("Applicability", objBM_Rule);
            }

            if (command_name == "save")
            {
                ModelState.Clear();
                var result = objEntityMaster.UpdateBM_Applicability(objBM_Rule);
                var model = objEntityMaster.GetBM_Applicability(objBM_Rule);
                if (result == true)
                {
                    model.Messages = new BM_ManegmentServices.VM.Response() { Success = true, Message = "Saved Successfully" };
                }
                else
                {
                    model.Messages = new BM_ManegmentServices.VM.Response() { Error = true, Message = "Something wents wrong" };
                }

                return PartialView("Applicability", model);
            }
            else
            {
                ModelState.Clear();
                var model = objEntityMaster.GetBM_Applicability(objBM_Rule);
                model.SMEListed = objBM_Rule.SMEListed;
                return PartialView("Applicability", model);
            }
        }

        [PageViewFilter]
        public PartialViewResult GetDirectorForEntity(int Entity_Id)
        {
            return PartialView("_GetDirector", Entity_Id);
        }

        [PageViewFilter]
        public PartialViewResult GetCommitteeForEntity(int Entity_Id)
        {
            ViewBag.MandatoryCommittee = objICommitteeMaster.getMandatoryCommittee(Entity_Id);
            return PartialView("_GetCommitteeForEntity", Entity_Id);
        }
        #endregion

        #region Customer Branch Entity
        [NonAction]
        public CustomerBranch CreateCustomerBranch(CustomerBranch customerBranch, int Entity_Type, bool IsListed, out string Message)
        {
            Message = "";
            customerBranch.LegalEntityTypeID = 1;
            customerBranch.Status = 1;
            customerBranch.Landline = "0";
            customerBranch.Mobile = "0";
            customerBranch.Type = 1;

            customerBranch.ContactPerson = " ";

            #region set Company Type
            customerBranch.ComType = null;

            if (IsListed == true)
            {
                customerBranch.ComType = 3;
            }
            else if (Entity_Type == 1 || Entity_Type == 9)
            {
                customerBranch.ComType = 2;
            }
            else if (Entity_Type == 2)
            {
                customerBranch.ComType = 1;
            }
            else
            {
                customerBranch.ComType = 4;
            }
            #endregion

            #region set Legal Entity type

            if (Entity_Type == 1)
            {
                customerBranch.LegalEntityTypeID = 6; //Private Limited Company
                customerBranch.LegalEntityTypeID = null;
            }
            else if (Entity_Type == 3)
            {
                customerBranch.LegalEntityTypeID = 4; //Limited Liability Partnership(LLP)
            }
            else if (Entity_Type == 2 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 2 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 10 && IsListed == true)
            {
                customerBranch.LegalEntityTypeID = 2; //Public Limited Company (Listed)
            }
            else if (Entity_Type == 10 && IsListed == false)
            {
                customerBranch.LegalEntityTypeID = 3; //Public Limited Company (Unlisted)
            }
            else if (Entity_Type == 7)
            {
                customerBranch.LegalEntityTypeID = 1; //Partnership
            }
            else if (Entity_Type == 6)
            {
                customerBranch.LegalEntityTypeID = 5; //Proprietorship
            }
            else if (Entity_Type == 8)
            {
                customerBranch.LegalEntityTypeID = 8; //One Person Company (O.P.C.)
            }
            else
            {
                customerBranch.LegalEntityTypeID = null;
            }
            #endregion

            #region Create Entity

            if (CustomerBranchManagement.Exists(customerBranch, customerBranch.CustomerID))
            {
                Message = "Customer branch name already exists.";
                //cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                //cvDuplicateEntry.IsValid = false;
                return customerBranch;
            }

            com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
            {
                ID = customerBranch.ID,
                Name = customerBranch.Name,
                Type = customerBranch.Type,
                ComType = customerBranch.ComType,
                AddressLine1 = customerBranch.AddressLine1,
                AddressLine2 = customerBranch.AddressLine2,
                StateID = customerBranch.StateID,
                CityID = customerBranch.CityID,
                Others = customerBranch.Others,
                PinCode = customerBranch.PinCode,
                ContactPerson = customerBranch.ContactPerson,
                Landline = customerBranch.Landline,
                Mobile = customerBranch.Mobile,
                EmailID = customerBranch.EmailID,
                CustomerID = customerBranch.CustomerID,
                //ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                Status = customerBranch.Status
            };

            if (CustomerBranchManagement.Exists1(customerBranch1, customerBranch1.CustomerID))
            {
                Message = "Customer branch name already exists.";
                //cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                //cvDuplicateEntry.IsValid = false;
                return customerBranch;
            }

            if (customerBranch.ID <= 0)
            {
                int BranchId = CustomerBranchManagement.Create(customerBranch);
                customerBranch.ID = BranchId;

                CustomerBranchManagement.Create1(customerBranch1);

                string ReplyEmailAddressName = "Avantis";
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                    //.Replace("@NewCustomer", AuthenticationHelper.CustomerID)
                                    .Replace("@BranchName", customerBranch.Name)
                                    .Replace("@LoginUser", AuthenticationHelper.User)
                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    .Replace("@From", ReplyEmailAddressName)
                                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                ;

                string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer branch added.", message);

            }
            else
            {
                CustomerBranchManagement.UpdateFromSecretarial(customerBranch);

                CustomerBranchManagementRisk.Update1FromSecretarial(customerBranch1);
            }

            #endregion

            return customerBranch;
        }
        #endregion
       
        #region ShareHolding Questions Added By Ruchi
        [HttpPost]
        public ActionResult SaveQuestionsForShareHolder(VM_QuestionForShareholding objsharesholder)
        {
            if (objsharesholder.EntityId > 0)
            {
                objsharesholder = objEntityMaster.SaveSharesDetails(objsharesholder);
            }
            return PartialView("_PopForShareholder", objsharesholder);
        }

        [PageViewFilter]
        public ActionResult openradiobutton(int EntityId)
        {
            VM_QuestionForShareholding objQuestionForShareholding = new VM_QuestionForShareholding();
            objQuestionForShareholding.EntityId = EntityId;
            return PartialView("_PopForShareholder", objQuestionForShareholding);
        }
        #endregion

        #region In case of Listed
        public ActionResult EditingInline_Read([DataSourceRequest] DataSourceRequest request, int EntityID)
        {
            return Json(objEntityMaster.readListedStockExchangeData(EntityID).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Create([DataSourceRequest] DataSourceRequest request, ListofstockExchange objlistofstockexc)
        {
            if (objlistofstockexc != null && ModelState.IsValid)
            {
                objEntityMaster.Create(objlistofstockexc);
            }

            return Json(new[] { objlistofstockexc }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Update([DataSourceRequest] DataSourceRequest request, ListofstockExchange objlistofstockexc)
        {
            if (objlistofstockexc != null && ModelState.IsValid)
            {
                objEntityMaster.Update(objlistofstockexc);
            }

            return Json(new[] { objlistofstockexc }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingInline_Destroy([DataSourceRequest] DataSourceRequest request, ListofstockExchange objlistofstockexc)
        {
            if (objlistofstockexc != null)
            {
                objEntityMaster.Destroy(objlistofstockexc);
            }

            return Json(new[] { objlistofstockexc }.ToDataSourceResult(request, ModelState));
        }
        #endregion


        #region Upload MGT7 Form added by Ruchi
        public ActionResult UploadFileopenPopUP()
        {
            VMEntityFileUpload obj = new VMEntityFileUpload();
            return PartialView("_EntityUpload", obj);
        }

        [HttpPost]
        public ActionResult UploadEXcelorEForm(VMEntityFileUpload objfileupload)
        {
            if (ModelState.IsValid)
            {
                objfileupload.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                objfileupload.UserId= Convert.ToInt32(AuthenticationHelper.UserID);
                objfileupload = objEntityMaster.ImportDatafrom_MGT7(objfileupload);
            }
            return PartialView("_EntityUpload", objfileupload);
        }
        #endregion

        #region Association Companies Added By Ruchi on 11th of March
        [HttpGet]
        //public ActionResult EditingCustom_Read([DataSourceRequest] DataSourceRequest request,long EntityID,int CustomerID)
        //{
        //    BindAssociateType();
        //    return Json(objEntityMaster.GetSubEntityDetails(EntityID, CustomerID).ToDataSourceResult(request),JsonRequestBehavior.AllowGet);
        //}

        public ActionResult EditingCustom_Read([DataSourceRequest] DataSourceRequest request, long EntityID, int CustomerID)
        {
            BindAssociateType();
            return Json(objEntityMaster.GetSubEntityDetails(EntityID, CustomerID).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult OpenEntitySubType(long EntityId, int CustomerID, string CIN)
        {
            SubCompanyType objsubCompanyType = new SubCompanyType();
            objsubCompanyType.EntityID = EntityId;
            objsubCompanyType.CustomerId = CustomerID;
            objsubCompanyType.ParentCIN = CIN;
            objsubCompanyType.AssociateCompany = new AssosiateType();
            BindAssociateType();
            return PartialView("SubCompany", objsubCompanyType);
        }

        private void BindAssociateType()
        {
            SubCompanyType objsubCompanyType = new SubCompanyType();
            var _objassosiate = new List<AssosiateType>();
            _objassosiate = objEntityMaster.GetAllEntityMaster();
            ViewData["AssosiateType"] = _objassosiate;
            ViewData["defaultAssosiateType"] = _objassosiate.First();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSubEntityDetails([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubCompanyType> subcompanyType)
        {
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int i = 1;

            bool flag = false;
            if (subcompanyType != null)
            {
                foreach (var items in subcompanyType)
                {
                    items.CustomerId = CustomerId;
                   
                    //items.Compliancesrno = i;
                    objEntityMaster.addCompanySubType(items);

                    flag = true;
                    i++;
                }
            }
            BindAssociateType();
            return Json(subcompanyType.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSubEntityDetails([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubCompanyType> subcompanyType)
        {

            bool flag = false;
            if (subcompanyType != null)
            {
                foreach (var items in subcompanyType)
                {
                    objEntityMaster.DeleteSubEntityMapping(items);
                    flag = true;
                }
            }
            BindAssociateType();
            return Json(subcompanyType.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Import entity from AVACOM
        public PartialViewResult ImportFromAVACOM()
        {
            return PartialView("_ImportFromAVACOM");
        }
        public ActionResult GetEntityListForImport([DataSourceRequest] DataSourceRequest request, int customerId)
        {
            return Json(objEntityMaster.GetEntityListForImport(customerId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ImportEntities([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ParentEntitySelectVM> model)
        {
            if(ModelState.IsValid && model != null)
            {
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                objEntityMaster.ImportEntityFromAVACOM(model, userID);
            }
            return Json(model.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }

}
