﻿using BM_ManegmentServices;
using BM_ManegmentServices.Services.Dashboard;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    
    public class DashboardController : Controller
    {

        IDashboardService objIDashboardService;
        public DashboardController(IDashboardService obj1)
        {
            objIDashboardService = obj1;
        }

        public ActionResult Index()
        {
            //CustomerManagement objcustom = new CustomerManagement();
            int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            string CustomerName = CustomerManagement.CustomerGetByIDName(CustomerId);
            if (CustomerName != null)
            {
                Session["CustomerName"] = CustomerName;
            }

            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("Dashboard");
            }
            else if ((AuthenticationHelper.Role == SecretarialConst.Roles.HDCS || AuthenticationHelper.Role == SecretarialConst.Roles.CS) && AuthenticationHelper.ProductApplicableLogin == "DFM")
            {
                return View("ClientDashboard");
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.HDCS || AuthenticationHelper.Role == SecretarialConst.Roles.CS)
            {
                return View("Dashboard_CS");
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.DADMN 
                || AuthenticationHelper.Role == SecretarialConst.Roles.CSMGR 
                || AuthenticationHelper.Role == SecretarialConst.Roles.CEXCT)
            {
                return View("FirmDashboard");
            }
            else
            {
                return View("Index");
            }
        }

        public ActionResult Dashboard()
        {
            if (AuthenticationHelper.Role == SecretarialConst.Roles.DRCTR)
            {
                return View("Dashboard");
            }
            else if ((AuthenticationHelper.Role == SecretarialConst.Roles.HDCS || AuthenticationHelper.Role == SecretarialConst.Roles.CS) && AuthenticationHelper.ProductApplicableLogin == "DFM")
            {
                return View("ClientDashboard");
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.HDCS || AuthenticationHelper.Role == SecretarialConst.Roles.CS)
            {
                return View("Dashboard_CS");
            }
            else if (AuthenticationHelper.Role == SecretarialConst.Roles.DADMN
                || AuthenticationHelper.Role == SecretarialConst.Roles.CSMGR
                || AuthenticationHelper.Role == SecretarialConst.Roles.CEXCT)
            {
                return View("FirmDashboard");
            }
            else
            {
                return View("Index");
            }
        }

        public ActionResult ClientDashboard()
        {
            return View("ClientDashboard");            
        }

        public ActionResult UpcomingMeetings()
        {
            return PartialView("_DashboardUpcomingMeetings");
        }

        [HttpPost]
        public ActionResult CountsDetails(int roleId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            objIDashboardService.MyCompliances_Director(customerId, userId, roleId);

            return Json(objIDashboardService.MyCompliances_Director(customerId, userId, roleId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CountsDetails1(int roleId)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);

            objIDashboardService.MyCompliances_Director(customerId, userId, roleId);

            return Json(objIDashboardService.MyCompliances_Director(customerId, userId, roleId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SelectandProceed(int custID, string type)
        {
            ProductMappingStructure _obj = new ProductMappingStructure();

            string role = string.Empty;
            string prodApplLogin = string.Empty;

            if (AuthenticationHelper.Role == "DADMN" || AuthenticationHelper.Role == "CSMGR") {
                role = "HDCS"; prodApplLogin = "DFM";
            }
            else if (AuthenticationHelper.Role == "CEXCT") {
                role = "CS"; prodApplLogin = "DFM";
            }

            _obj.ReAuthenticate_User(custID, role, prodApplLogin);

            string CustomerName = CustomerManagement.CustomerGetByIDName(custID);
            if (CustomerName != null)
            {
                Session["CustomerName"] = CustomerName;
            }

            string returnURL = "/BM_Management/Dashboard/ClientDashboard";
            if(!string.IsNullOrEmpty(type))
            {
                if (type.Trim().Equals("G"))
                    returnURL = "/BM_Management/Dashboard/ClientDashboard";
                else if (type.Trim().Equals("E"))
                    returnURL = "/BM_Management/EntityMaster/GetEntityDtls";
            }

            return Json(new { redirectToUrl = returnURL });
        }       
    }
}