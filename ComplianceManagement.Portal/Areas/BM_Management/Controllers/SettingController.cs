﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Drawing;
using BM_ManegmentServices.Services.Setting;
using com.VirtuosoITech.ComplianceManagement.Business;
//using GleamTech.DocumentUltimate;
//using GleamTech.DocumentUltimate.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class SettingController : Controller
    {
        IEmailService objIEmailService;
        ITemplateservice objITemplateservice;

        public SettingController(IEmailService obj, ITemplateservice obj1)
        {
            objIEmailService = obj;
            objITemplateservice = obj1;
        }
        public ActionResult Mail()
        {
            return View();
        }

        public ActionResult GetMailSetting([DataSourceRequest] DataSourceRequest request)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            string role = AuthenticationHelper.Role;

            return Json(objIEmailService.GetAll(customerId, userId, role).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestMail(long id)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIEmailService.Get(id, customerID);
            if(model == null)
            {
                model = new TestMailVM();
            }
            model.Body = objITemplateservice.TestMailTemplate();

            return PartialView(model);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TestMail(TestMailVM model)
        {
            if (ModelState.IsValid)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var result = EmailService.TestMail(model.FromMail, new List<String>(new String[] { model.To }), null, null, model.Subject, model.Body, new List<System.Net.Mail.Attachment>(), customerID, (int)model.EntityID);
                model.Message = result;
                model.Error = true;
                ModelState.Clear();
            }
            return PartialView(model);
        }
        
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Put)]
        public ActionResult SaveMailSetting(MailSettingVM obj)
        {
            if (obj != null && ModelState.IsValid)
            {
                int UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                obj.CustomerID = CustomerID;
                objIEmailService.Save(obj, UserId);
            }
            return Json(obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingCustom_Destroy(MailSettingVM obj)
        {
            return Json(obj);
        }

    }
}