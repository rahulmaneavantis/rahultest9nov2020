﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class Meeting_CompliancesController : Controller
    {
        IMeeting_Compliances objIMeeting_Compliances;
        ICompliance objICompliance;
        IComplianceTransaction_Service objIComplianceTransaction_Service;
        IFileData_Service objIFileData_Service;
        public Meeting_CompliancesController(IMeeting_Compliances obj, ICompliance objCompliance,IComplianceTransaction_Service objComplianceTransaction_Service, IFileData_Service objFileData_Service)
        {
            objIMeeting_Compliances = obj;
            objICompliance = objCompliance;
            objIComplianceTransaction_Service = objComplianceTransaction_Service;
            objIFileData_Service = objFileData_Service;
        }
        // GET: BM_Management/Meeting_Compliances/Compliances
        public ActionResult Compliances(long agendaId, long meetingId, long meetingAgendaMappingId)
        {
            dynamic model = new ExpandoObject();
            model.AgendaID = agendaId;
            model.MeetingID = meetingId;
            model.MeetingAgendaMappingId = meetingAgendaMappingId;

            return PartialView("Compliances", model);
        }
        public ActionResult Compliances_Read([DataSourceRequest] DataSourceRequest request, long meetingId, long meetingAgendaMappingId)
        {
            return Json(objIMeeting_Compliances.GetCompliances(meetingAgendaMappingId, meetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Schedule
        public ActionResult MeetingComplianceSchedule(long meetingId)
        {
            dynamic model = new ExpandoObject();
            model.MeetingID = meetingId;
            return PartialView("_ComplianceSchedule", model);
        }
        public ActionResult ComplianceSchedule_Read([DataSourceRequest] DataSourceRequest request, long meetingId)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ScheduleOn", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Compliances.GetMeetingComplianceSchedule(meetingId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ComplianceSchedule_Read_MeetingLevel([DataSourceRequest] DataSourceRequest request, long meetingId, string mappingType)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("ScheduleOn", System.ComponentModel.ListSortDirection.Ascending));
            }
            return Json(objIMeeting_Compliances.GetMeetingComplianceSchedule(meetingId, mappingType).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ScheduleDetails(long id, int entityID)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userID = Convert.ToInt32(AuthenticationHelper.UserID);

            var model = objIMeeting_Compliances.GetScheduleDetails(id, entityID, customerId);
            loadComplianceStaus(model.ScheduleOnID, userID);

            return PartialView("_ComplianceScheduleDetails",model);
        }

        public ActionResult ComplianceSchedule_Documents([DataSourceRequest] DataSourceRequest request, long id)
        {
            if (request.Sorts.Count == 0)
            {
                request.Sorts.Add(new SortDescriptor("FileID", System.ComponentModel.ListSortDirection.Descending));
            }
            return Json(objIMeeting_Compliances.GetComplianceDocuments(id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Compliance status update
        [HttpPost]
        public PartialViewResult ComplianceStatusUpdate(ComplianceTransactionVM model, HttpPostedFileBase[] complianceDoc, HttpPostedFileBase[] workingDoc)
        {
            if(ModelState.IsValid)
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userID = Convert.ToInt32(AuthenticationHelper.UserID);
                model.CreatedBy = userID;
                model.CreatedByText = AuthenticationHelper.User;

                model = objIComplianceTransaction_Service.CreateTransaction(model);

                #region File Data -> complianceDoc
                if (complianceDoc != null)
                {
                    string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                    foreach (var item in complianceDoc)
                    {
                        if (item.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            item.InputStream.CopyTo(target);

                            int fileVersion = 0;

                            var objFileData = new FileDataVM()
                            {
                                FileData = target.ToArray(),
                                FileName = item.FileName,
                                FilePath = path
                            };

                            fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                            fileVersion++;

                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && model.TransactionID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 1);
                            }
                        }
                    }
                }
                #endregion

                #region File Data -> workingDoc
                if (workingDoc != null)
                {
                    string path = "~/Areas/BM_Management/Documents/" + customerID + "/" + model.EntityID_ + "/Meeting/" + model.MeetingID_;
                    foreach (var item in workingDoc)
                    {
                        if (item.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            item.InputStream.CopyTo(target);

                            int fileVersion = 0;

                            var objFileData = new FileDataVM()
                            {
                                FileData = target.ToArray(),
                                FileName = item.FileName,
                                FilePath = path
                            };

                            fileVersion = objIFileData_Service.GetFileVersion(model.ComplianceScheduleOnID, objFileData.FileName);
                            fileVersion++;

                            objFileData.Version = Convert.ToString(fileVersion);
                            objFileData = objIFileData_Service.Save(objFileData, userID);

                            if (objFileData.FileID > 0 && model.TransactionID > 0)
                            {
                                objIFileData_Service.CreateFileDataMapping(model.TransactionID, model.ComplianceScheduleOnID, objFileData.FileID, 2);
                            }
                        }
                    }
                }
                #endregion

                loadComplianceStaus(model.ComplianceScheduleOnID, userID);
            }
            
            return PartialView("_ComplianceStatusUpdate", model);
        }

        public void loadComplianceStaus(long ComplianceScheduleOnID, int userID)
        {
            ViewBag.ComplianceUpdateStatus = objICompliance.GetComplianceUpdateStatus(ComplianceScheduleOnID, userID);
        }
        #endregion
    }
}