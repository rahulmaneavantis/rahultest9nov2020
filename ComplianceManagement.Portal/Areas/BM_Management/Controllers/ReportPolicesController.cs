﻿using BM_ManegmentServices.Services.Reports;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ReportPolicesController : Controller
    {
        IReportPolices obj;
        public ReportPolicesController(IReportPolices obj)
        {
            this.obj = obj;
        }
        // GET: BM_Management/ReportPolices
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }


        public ActionResult UploadReportview()
        {
            ReportVM objreportvm = new ReportVM();
            return PartialView("_UploadReports", objreportvm);
        }

        public ActionResult UploadReport(ReportVM objreport)
        {
            if(objreport!=null && ModelState.IsValid)
            {
                objreport = obj.uploadFile(objreport);
            }
            return PartialView("_UploadReports", objreport);
        }
    }
}