﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{

    public class AgendaMasterCompliancesController : Controller
    {
        IAgendaCompliance objIAgendaCompliance;
        public AgendaMasterCompliancesController(IAgendaCompliance obj)
        {
            objIAgendaCompliance = obj;
        }

        public ActionResult Editing_Compliances(long? AgendaMasterId)
        {
            string Role = AuthenticationHelper.Role;
            int? CustomerId;
            if(Role=="CSIMP")
            {
                CustomerId = null;
            }
            else
            {
                CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            if (AgendaMasterId == null)
            {
                AgendaMasterId = 0;
            }
            PopulateCategories();
            var checkAgendaCompliaceMapping = objIAgendaCompliance.CheckAgendaCompliance(AgendaMasterId);
            var IsOpenOnComplianceCompletion = objIAgendaCompliance.IsOnComplianceCompletion(AgendaMasterId);
            if (CustomerId == null)
            {
                if (checkAgendaCompliaceMapping.Count > 0)
                {
                    VMCompliences obj = new VMCompliences();
                    obj.AgendaId = AgendaMasterId;
                    obj.New = false;
                    obj.OnComplianceCompletion = IsOpenOnComplianceCompletion;
                    return PartialView("_ComplianceMappingandDetails", obj);
                }
                else
                {
                    VMCompliences obj = new VMCompliences();
                    obj.AgendaId = AgendaMasterId;
                    obj.New = true;
                    obj.OnComplianceCompletion = IsOpenOnComplianceCompletion;
                    return PartialView("_ComplianceMappingandDetails", obj);
                }
            }
            else
            {
                VMCompliences obj = new VMCompliences();
                obj.AgendaId = AgendaMasterId;
               
                obj.OnComplianceCompletion = IsOpenOnComplianceCompletion;
                return PartialView("_ComplianceMappingDetails", obj.AgendaId);
            }

        }

        public ActionResult EditingCustom_Read([DataSourceRequest] DataSourceRequest request, long? AgendaMasterId)
        {
            if (AgendaMasterId == null)
            {
                AgendaMasterId = 0;
            }
            return Json(objIAgendaCompliance.GetAll((long)AgendaMasterId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(long id)
        {
            var model = new VMCompliences() { AgendaId = id, ID = 0, New = true };
            PopulateCategories();
            //return PartialView("_AddEditComplianceMapping", model);
            return PartialView("_ComplianceMappingandDetails", model);
        }
        public ActionResult GetMapping(long agendaMasterId, long id)
        {
            var model = objIAgendaCompliance.Get(agendaMasterId, id);
            PopulateCategories();
            return PartialView("_ComplianceDetails", agendaMasterId);
        }
        [HttpPost]
        public ActionResult Save(AgendaComplianceVM model)
        {
            if (ModelState.IsValid)
            {
                if (model.Agenda_ComplianceId == 0)
                {
                    model = objIAgendaCompliance.Create(model);
                }
                else
                {
                    model = objIAgendaCompliance.Update(model);
                }
                ModelState.Clear();
            }

            return PartialView("_AddEditComplianceMapping", model);
        }

        #region Save Agenda MappingCompliance //Added by Ruchi on 16 of December 2019
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult save_checkedAgendaComplianceApping([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VMCompliences> complianceListmapping)
        {
            int? CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            string Role = AuthenticationHelper.Role;
            if(Role== "CSIMP")
            {
                CustomerID = null;
            }
            else
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            bool flag = false;
            if (complianceListmapping != null && ModelState.IsValid)
            {
                foreach (var complianceItem in complianceListmapping)
                {
                    if (complianceItem.IsCheked)
                    {
                        objIAgendaCompliance.AddAgendaItem(complianceItem, UserID, CustomerID);

                        flag = true;
                    }
                }
            }

            return Json(complianceListmapping.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Add Agenda Compliance Mapping Details
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ComplianceDetails_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AgendaComplianceEditorVM> complianceListDetails)
        {
            int i = 1;
            int? CustomerID = null;
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            bool flag = false;
            string Role = AuthenticationHelper.Role;
            if (Role == "CSIMP")
            {
                CustomerID = null;
            }
            else
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            if (complianceListDetails != null)
            {
                foreach (var complianceItem in complianceListDetails)
                {
                    complianceItem.Compliancesrno = i;
                    objIAgendaCompliance.AddAgendaDetails(complianceItem, UserID, CustomerID);

                    flag = true;
                    i++;
                }

                if(flag)
                {
                    if(complianceListDetails.Count() > 0)
                    {
                        objIAgendaCompliance.SetHasCompliance(complianceListDetails.FirstOrDefault().AgendaMasterId);
                    }
                }
            }

            return Json(complianceListDetails.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Get Agenda Compliance Mapping Details
        //[HttpGet]
        public ActionResult AgendaMappingDetails([DataSourceRequest] DataSourceRequest request, long AgendaMasterId)
        {
            PopulateCategories();
            int? CustomerId = null;
            string Role = AuthenticationHelper.Role;
            if(Role== "CSIMP")
            {
                CustomerId= null;
            }
            else
            {
                CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            List<AgendaComplianceEditorVM> objagendacompliance = new List<AgendaComplianceEditorVM>();
            if (AgendaMasterId > 0)
            {

                objagendacompliance = objIAgendaCompliance.GetAgendaMappingDetails(AgendaMasterId, CustomerId);
            }
            return Json(objagendacompliance.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Drop down for Mapping Details
        private void PopulateCategories()
        {
            var listDayorHoure = new List<DaysOrHoursViewModel>
            {
            
             new DaysOrHoursViewModel {DaysOrHoursName="Day(s)",DaysOrHours="D" },
             new DaysOrHoursViewModel {DaysOrHoursName="Hour(s)",DaysOrHours="H" },
             new DaysOrHoursViewModel {DaysOrHoursName="Minute(s)",DaysOrHours="M" }
            };
           ViewData["listDayorHoureData"] = listDayorHoure;
            //ViewBag.listDayorHoureData = listDayorHoure;


            var listBeforeAfter = new List<BeforeAfterViewModel>
         {
         
          new BeforeAfterViewModel {BeforeAfterName="After",BeforeAfter="A" },
          new BeforeAfterViewModel {BeforeAfterName="Before",BeforeAfter="B" }

         };
            ViewData["BeforeAfterData"] = listBeforeAfter;

           var listDayType = new List<DayTypeViewModel>
            {
                 new DayTypeViewModel {DayTypeName="Clear Working Day(s)",DayType="W" },
          new DayTypeViewModel {DayTypeName="Normal Day(s)",DayType="C" },
            };
            ViewData["listDayType"] = listDayType;

            var listtriggeron = new List<TriggeronViewdata>
            {
                 new TriggeronViewdata {TriggerOnName="On Approve",Triggeron="A" },
                 new TriggeronViewdata {TriggerOnName="On DisApprove",Triggeron="D" },
                 new TriggeronViewdata {TriggerOnName="Both",Triggeron="B" },
            };
            ViewData["listTriggeron"] = listtriggeron;

            var listType = new List<TypeViewModel>
            {
                 new TypeViewModel {TypeName="Single",Type="S" },
                 new TypeViewModel {TypeName="Combined",Type="C" },
            };
            ViewData["listType"] = listType;
        }

        public JsonResult BindDayHours(string dayhoursID)
        {
            return Json(objIAgendaCompliance.GetHoureMinutesDay(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult BindDayType(string DayTypeNames)
        {
            return Json(objIAgendaCompliance.getDayType(DayTypeNames), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delete Agenda Compliance Mapping Details
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteComplianceAgendaMapping([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<AgendaComplianceEditorVM> complianceListDetails)
        {

            int? CustomerID = null;
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            string Role = AuthenticationHelper.Role;
            if (Role== "CSIMP")
            {
                CustomerID = null;
            }
            else
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
                bool flag = false;
            if (complianceListDetails != null)
            {
                foreach (var complianceItem in complianceListDetails)
                {
                    objIAgendaCompliance.DeleteAgendaComplianceMapping(complianceItem, UserID, CustomerID, Role);
                    flag = true;
                }

                if (flag)
                {
                    if (complianceListDetails.Count() > 0)
                    {
                        objIAgendaCompliance.SetHasCompliance(complianceListDetails.FirstOrDefault().AgendaMasterId);
                    }
                }
            }

            return Json(complianceListDetails.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }

}