﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CountryCityStateMasterController : Controller
    {
        // GET: BM_Management/CountryCityStateMaster
        ICountryStateCityMaster obj;

        public CountryCityStateMasterController(ICountryStateCityMaster obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [PageViewFilter]
        public ActionResult GetCountry()
        {
            return View();
        }

        [HttpGet]
        [PageViewFilter]
        public ActionResult GetState()
        {
            return View();
        }

        [HttpGet]
        [PageViewFilter]
        public ActionResult GetCity()
        {
            return View();
        }


        public ActionResult Editcountry(int Id)
        {
            VMCountry model = new VMCountry();
            try
            {
                if (Id > 0)
                {
                    model = obj.GetCountryById(Id);
                    return PartialView("_AddEditCountryfor", model);
                }
                else
                {
                    return PartialView("_AddEditCountryfor", model);
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_AddEditCountryfor", model);
            }
        }

        public ActionResult EditState(int Id)
        {
            VMState model = new VMState();
            try
            {
                if (Id > 0)
                {
                    model = obj.GetSateById(Id);
                    return PartialView("_PartialviewforState", model);
                }
                else
                {
                    return PartialView("_PartialviewforState", model);
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_PartialviewforState", model);
            }
        }


        [HttpPost]
        public ActionResult Savestate(VMState _objstate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_objstate.SIdPublic > 0)
                    {
                        _objstate = obj.EditState(_objstate);
                        return PartialView("_PartialviewforState", _objstate);
                    }
                    else
                    {
                        _objstate = obj.AddState(_objstate);
                        return PartialView("_PartialviewforState", _objstate);
                    }
                }
                else
                {
                    return PartialView("_PartialviewforState", _objstate);
                }
            }
            catch (Exception ex)
            {
                return PartialView("_PartialviewforState", _objstate);
            }
        }

        [HttpPost]
        public ActionResult SaveCountry(VMCountry _objcountry)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_objcountry.CountryId > 0)
                    {
                        _objcountry = obj.EditCountry(_objcountry);
                        return PartialView("_AddEditCountryfor", _objcountry);
                    }
                    else
                    {
                        _objcountry = obj.AddCountry(_objcountry);
                        return PartialView("_AddEditCountryfor", _objcountry);
                    }
                }
                else
                {
                    return PartialView("_AddEditCountryfor", _objcountry);
                }
            }
            catch (Exception ex)
            {
                return PartialView("_AddEditCountryfor", _objcountry);
            }
        }


        public ActionResult EditCity(int Id)
        {
            VMCity model = new VMCity();
            try
            {
                if (Id > 0)
                {
                    model = obj.GetCityById(Id);
                    return PartialView("_partialviewforCity", model);
                }
                else
                {
                    return PartialView("_partialviewforCity", model);
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return PartialView("_partialviewforCity", model);
            }
        }

        [HttpPost]
        public ActionResult SaveCity(VMCity _objcity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_objcity.Reg_cityId > 0)
                    {
                        _objcity = obj.EditCity(_objcity);
                        return PartialView("_partialviewforCity", _objcity);
                    }
                    else
                    {
                        _objcity = obj.AddCity(_objcity);
                        return PartialView("_partialviewforCity", _objcity);
                    }
                }
                else
                {
                    return PartialView("_partialviewforCity", _objcity);
                }
            }
            catch (Exception ex)
            {
                return PartialView("_partialviewforCity", _objcity);
            }
        }
    }
}