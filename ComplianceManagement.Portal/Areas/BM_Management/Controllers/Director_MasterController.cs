﻿using BM_ManegmentServices;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Setting;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.Services.Forms;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class Director_MasterController : Controller
    {
        IDirectorMaster objService;
        IKMP_Master _objkmp;
        IForms_Service objform;
        ISettingService objISettingService;
        int CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public Director_MasterController(IDirectorMaster obj, IKMP_Master _objkmp, ISettingService objSettingService)
        {
            objService = obj;
            this._objkmp = _objkmp;
            objISettingService = objSettingService;
        }
        // GET: BM_Management/Director_Master
        public ActionResult Index()
        {
            Director _objdirector = new Director();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                _objdirector.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Director") select x.Addview).FirstOrDefault();
                _objdirector.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Director") select x.Editview).FirstOrDefault();
                _objdirector.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("Director") select x.DeleteView).FirstOrDefault();
            }
            return View(_objdirector);
        }

        public ActionResult DirectorMaster1()
        {
            return View();
        }

        public ViewResult BlankEditorRow()
        {
            return View("_DirectorsRealtiveEditor", new Relatives());
        }

        public PartialViewResult CreateDirector()
        {
            return PartialView(new Director_MasterVM());
        }

        public string UploadFile(HttpPostedFileBase file, string path, string file_Prefix)
        {
            var result = "";
            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    string _file_Name = file_Prefix + Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));

                    file.SaveAs(_path);
                    result = _file_Name;
                }
            }
            return result;
        }

        public PartialViewResult Create(string ViewName)
        {
            Director_MasterVM obj = new Director_MasterVM() { EducationalQualification = -1, Occupation = -1, AreaOfOccupation = -1 };
            obj.ViewName = ViewName;
            return PartialView("CreateDirector", obj);
        }
        [HttpPost]
        public PartialViewResult Save(Director_MasterVM obj)
        {
            obj.Response = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid == false)
            {
                obj.Response.Error = true;
                obj.Response.Message = "Something went wrong";
                return PartialView("CreateDirector", obj);
            }

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            BM_DirectorMaster obj_BM_DirectorMaster = new BM_DirectorMaster();

            #region Assign Values
            
            obj_BM_DirectorMaster.DIN = obj.DIN;

            obj_BM_DirectorMaster.Salutation = obj.Salutation;
            obj_BM_DirectorMaster.FirstName = obj.FirstName;
            obj_BM_DirectorMaster.MiddleName = obj.MiddleName;
            obj_BM_DirectorMaster.LastName = string.IsNullOrEmpty(obj.LastName) ? "" : obj.LastName;

            obj_BM_DirectorMaster.DOB = (DateTime)obj.DateOfBirth;
            obj_BM_DirectorMaster.Gender = (int)obj.Gender;
            obj_BM_DirectorMaster.MobileNo = obj.MobileNo;
            obj_BM_DirectorMaster.EmailId_Personal = obj.EmailId;
            obj_BM_DirectorMaster.EmailId_Official = obj.EmailId_Official;

            obj_BM_DirectorMaster.FSalutations = obj.FSalutations;
            obj_BM_DirectorMaster.Father = obj.FatherFirstName;
            obj_BM_DirectorMaster.FatherMiddleName = obj.FatherMiddleName;
            obj_BM_DirectorMaster.FatherLastName = obj.FatherLastName;
                        
            obj_BM_DirectorMaster.Mother = obj.MotherName;

            #region Residential Address

            obj_BM_DirectorMaster.Permenant_Address_Line1 = obj.Permenant_Address_Line1;
            obj_BM_DirectorMaster.Permenant_Address_Line2 = obj.Permenant_Address_Line2;
            obj_BM_DirectorMaster.Permenant_StateId = obj.Permenant_StateId;
            obj_BM_DirectorMaster.Permenant_CityId = (int)obj.Permenant_CityId;
            obj_BM_DirectorMaster.Permenant_PINCode = obj.Permenant_PINCode;

            obj_BM_DirectorMaster.IsSameAddress = obj.IsSameAddress;

            obj_BM_DirectorMaster.Present_Address_Line1 = obj.Present_Address_Line1;
            obj_BM_DirectorMaster.Present_Address_Line2 = obj.Present_Address_Line2;
            obj_BM_DirectorMaster.Present_StateId = (int)obj.Present_StateId;
            obj_BM_DirectorMaster.Present_CityId = (int)obj.Present_CityId;
            obj_BM_DirectorMaster.Present_PINCode = obj.Present_PINCode;
            
            #endregion

            obj_BM_DirectorMaster.EducationalQualification = obj.EducationalQualification;
            obj_BM_DirectorMaster.OtherQualification = obj.OtherQualification;
            obj_BM_DirectorMaster.Occupation = obj.Occupation;
            obj_BM_DirectorMaster.AreaOfOccupation = obj.AreaOfOccupation;
            obj_BM_DirectorMaster.OtherOccupation = obj.OtherOccupation;
            obj_BM_DirectorMaster.ResidentInIndia = obj.ResidentInIndia;
            obj_BM_DirectorMaster.Nationality = (int)obj.Nationality;
            obj_BM_DirectorMaster.PAN = obj.PAN;
            obj_BM_DirectorMaster.Aadhaar = obj.Adhaar;
            obj_BM_DirectorMaster.PassportNo = obj.PassportNo;
            //obj_BM_DirectorMaster.PAN_Doc = obj.PAN_Doc;
            //obj_BM_DirectorMaster.Aadhar_Doc = obj.Aadhar_Doc;
            //obj_BM_DirectorMaster.Passport_Doc = obj.Passport_Doc;
            //obj_BM_DirectorMaster.Photo_Doc = obj.Photo_Doc;
            obj_BM_DirectorMaster.DSC_ExpiryDate = obj.DESC_ExpiryDate;

            obj_BM_DirectorMaster.Customer_Id = customerId;
            obj_BM_DirectorMaster.Is_Deleted = false;

            obj_BM_DirectorMaster.CreatedBy = AuthenticationHelper.UserID;
            obj_BM_DirectorMaster.CreatedOn = DateTime.Now;
            // obj_BM_DirectorMaster.IsCAmember = obj.IsCAmember;
            obj_BM_DirectorMaster.CS_MemberNo = obj.CS_Membersip;

            //obj_BM_DirectorMaster.IsDirecor = true;
            obj_BM_DirectorMaster.IsActive = true;
            
            #endregion

            if (!string.IsNullOrEmpty(obj_BM_DirectorMaster.DIN))
            {
                if (objService.CheckDIN(customerId, obj_BM_DirectorMaster.Id, obj_BM_DirectorMaster.DIN))
                {
                    obj.Response.Success = false;
                    obj.Response.Message = "DIN already exists";
                    obj.Response.Error = true;
                    return PartialView("CreateDirector", obj);
                }
            }

            #region add User
           
            //Create User with Director Role
            bool createUserOfDirector = true;
            bool accessToDirector = objISettingService.GetAccessToDirector(customerId);

            //Create User with Director Role
            UserVM user = new UserVM();
            user.UserID = 0;

            if (createUserOfDirector)
            {
                user.Email = obj_BM_DirectorMaster.EmailId_Official;
                user.FirstName = obj_BM_DirectorMaster.FirstName;
                user.LastName = obj_BM_DirectorMaster.LastName;
                user.CustomerID = customerId;
                user.ContactNumber = obj_BM_DirectorMaster.MobileNo;
                user.Address = obj_BM_DirectorMaster.Present_Address_Line1;

                var tempUserId = objService.GetUserIdForCreateNewDirector(obj.EmailId_Official, customerId);
                if (tempUserId > 0)
                {
                    user.UserID = (long)tempUserId;
                }

                if (user.UserID == 0)
                {
                    user = CreateUser(user, false, false);
                }
                else
                {
                    user = UpdateUser(user, obj.EmailId_Official, false);
                }
            }
            //End
            #endregion

            bool hasError = false;
            obj_BM_DirectorMaster.UserID = user.UserID;
            var result = objService.Create(obj_BM_DirectorMaster, out hasError);

            if (result == true)
            {
                if (obj.Photo_Doc != null)
                {
                    obj_BM_DirectorMaster.Photo_Doc = UploadFile(obj.Photo_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Photo_");
                    obj.Photo_Doc_Name = obj_BM_DirectorMaster.Photo_Doc;
                }

                if (obj.PAN_Doc != null)
                {
                    obj_BM_DirectorMaster.PAN_Doc = UploadFile(obj.PAN_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "PAN_");
                    obj.PAN_Doc_Name = obj_BM_DirectorMaster.PAN_Doc;
                }

                if (obj.Aadhaar_Doc != null)
                {
                    obj_BM_DirectorMaster.Aadhar_Doc = UploadFile(obj.Aadhaar_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Aadhaar_");
                    obj.Aadhaar_Doc_Name = obj_BM_DirectorMaster.Aadhar_Doc;
                }

                if (obj.Passport_Doc != null)
                {
                    obj_BM_DirectorMaster.Passport_Doc = UploadFile(obj.Passport_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Passport_");
                    obj.Passport_Doc_Name = obj_BM_DirectorMaster.Passport_Doc;
                }

                objService.Update_DirectorDoc(obj_BM_DirectorMaster);

                ModelState.Clear();

                return Edit(obj_BM_DirectorMaster.Id, "New", obj.ViewName);
            }
            else
            {
                obj.Response.Error = true;
                obj.Response.Message = hasError ? "Server Error Occure" : "Something wents wrong";
                return PartialView("CreateDirector", obj);
            }
        }

        public PartialViewResult Edit(long id, string isNew, string viewName)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            Director obj = new Director();
            //added by Ruchi on 20th Feb 2020 for Security of Director
            obj._objSecurities = new Director_MasterKMPUploadSecurity { KMP_ID = id };

            var obj_BM_DirectorMasterVM = objService.GetDirectorVM(customerId, id);
            if (obj_BM_DirectorMasterVM != null)
            {
                obj.objDirector_MasterVM = obj_BM_DirectorMasterVM;

                if (!string.IsNullOrEmpty(isNew))
                {
                    if (isNew == "New")
                    {
                        obj.objDirector_MasterVM.Response = new BM_ManegmentServices.VM.Response() { Success = true, Message = "Save successfully." };
                    }
                }
            }
            else
            {
                obj.objDirector_MasterVM.Response = new BM_ManegmentServices.VM.Response() { Error = true, Message = "Something wents wrong" };
            }

            if (viewName == "DirectorAppointment")
            {
                return PartialView("_DirectorDetails", obj.objDirector_MasterVM);
            }

            else
            {

                return PartialView("EditDirector", obj);
            }
        }


        [HttpPost]
        public PartialViewResult EditShareDetails(ShareHolding obj, string command_name)
        {
            ModelState.Clear();
            if (command_name == "btnNoofDetails")
            {
                if (obj.lstShareHoldingDetails == null)
                {
                    obj.lstShareHoldingDetails = new List<ShareHoldingDetails>();
                }

                if (obj.Count < obj.lstShareHoldingDetails.Count)
                {
                    List<ShareHoldingDetails> lstTemp = new List<ShareHoldingDetails>();
                    for (int i = 0; i < obj.Count; i++)
                    {
                        lstTemp.Add(obj.lstShareHoldingDetails[i]);
                    }
                    obj.lstShareHoldingDetails = lstTemp;
                }
                else
                {
                    for (int i = obj.lstShareHoldingDetails.Count; i < obj.Count; i++)
                    {
                        obj.lstShareHoldingDetails.Add(new ShareHoldingDetails());
                    }
                }
            }
            else
            {
                if (obj.lstShareHoldingDetails.Count > 0)
                {
                }
            }
            return PartialView("Share", obj);
        }

        [HttpPost]
        public ActionResult UpdatetypeofchangesDirectorDetails(BM_ManegmentServices.VM.Director_MasterVM obj)
        {
            BM_DirectorTypeOfChanges _objcheckChangetype = new BM_DirectorTypeOfChanges();
            if (ModelState.IsValid == false)
            {
                // IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return PartialView("_DirectorDetails", obj);
            }
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            #region Check Edit type
            _objcheckChangetype = objService.CheckIsChangeDone(obj.ID);

            #endregion
            #region Change by Ruchi on 27th of March
            //BM_DirectorMaster obj_BM_Directordetails = objService.GetDirector(customerId, obj.ID);

            if (_objcheckChangetype != null)
            {
                BM_DirectorTypeChangesMapping obj_BM_DirectorMaster = new BM_DirectorTypeChangesMapping();
                string Email_Old = obj_BM_DirectorMaster.EmailId_Official;
                #region Assign Values
                obj_BM_DirectorMaster.DirectorId = obj.ID;
                obj_BM_DirectorMaster.TypeChangeMaapingID = _objcheckChangetype.Id;
                if (_objcheckChangetype.Name_of_Director)
                {
                    obj_BM_DirectorMaster.Salutation = obj.Salutation;
                    obj_BM_DirectorMaster.FirstName = obj.FirstName;
                    obj_BM_DirectorMaster.MiddleName = obj.MiddleName;
                    obj_BM_DirectorMaster.LastName = obj.LastName;
                }
                if (_objcheckChangetype.DateofBirth)
                {
                    obj_BM_DirectorMaster.DOB = obj.DateOfBirth;
                }
                if (_objcheckChangetype.Gender)
                {
                    obj_BM_DirectorMaster.Gender = obj.Gender;
                }
                if (_objcheckChangetype.Mobile)
                {
                    obj_BM_DirectorMaster.MobileNo = obj.MobileNo;
                }
                if (_objcheckChangetype.EmailID)
                {
                    obj_BM_DirectorMaster.EmailId_Personal = obj.EmailId;
                    obj_BM_DirectorMaster.EmailId_Official = obj.EmailId_Official;
                }
                #region Father's Name
                if (_objcheckChangetype.FatherName)
                {
                    obj_BM_DirectorMaster.FatherFirstName = obj.FatherFirstName;
                    obj_BM_DirectorMaster.FatherMiddleName = obj.FatherMiddleName;
                    obj_BM_DirectorMaster.FatherLastName = obj.FatherLastName;
                }
                #endregion

                #region Residential Address
                if (_objcheckChangetype.Parmanent_Address)
                {
                    obj_BM_DirectorMaster.Permenant_Address_Line1 = obj.Permenant_Address_Line1;
                    obj_BM_DirectorMaster.Permenant_Address_Line2 = obj.Permenant_Address_Line2;
                    obj_BM_DirectorMaster.Permenant_StateId = obj.Permenant_StateId;
                    obj_BM_DirectorMaster.Permenant_CityId = obj.Permenant_CityId;
                    obj_BM_DirectorMaster.Permenant_PINCode = obj.Permenant_PINCode;
                }
                obj_BM_DirectorMaster.IsSameAddress = obj.IsSameAddress;
                if (_objcheckChangetype.Present_Address)
                {
                    obj_BM_DirectorMaster.Present_Address_Line1 = obj.Present_Address_Line1;
                    obj_BM_DirectorMaster.Present_Address_Line2 = obj.Present_Address_Line2;
                    obj_BM_DirectorMaster.Present_StateId = obj.Present_StateId;
                    obj_BM_DirectorMaster.Present_CityId = obj.Present_CityId;
                    obj_BM_DirectorMaster.Present_PINCode = obj.Present_PINCode;
                }
                #endregion

                obj_BM_DirectorMaster.EducationalQualification = obj.EducationalQualification;
                obj_BM_DirectorMaster.OtherQualification = obj.OtherQualification;
                obj_BM_DirectorMaster.Occupation = obj.Occupation;
                obj_BM_DirectorMaster.AreaOfOccupation = obj.AreaOfOccupation;
                obj_BM_DirectorMaster.OtherOccupation = obj.OtherOccupation;
               // obj_BM_DirectorMaster.MotherName= obj.MotherName;
                if (_objcheckChangetype.Rasidential_Status)
                {
                    obj_BM_DirectorMaster.ResidentInIndia = obj.ResidentInIndia;
                }
                if (_objcheckChangetype.Nationality)
                {
                    obj_BM_DirectorMaster.Nationality = obj.Nationality;
                }
                if (_objcheckChangetype.PAN)
                {
                    obj_BM_DirectorMaster.PAN = obj.PAN;
                }
                if (_objcheckChangetype.AadharNumber)
                {
                    obj_BM_DirectorMaster.Aadhaar = obj.Adhaar;
                }

                obj_BM_DirectorMaster.PassportNo = obj.PassportNo;
                obj_BM_DirectorMaster.IsActive = true;

                obj_BM_DirectorMaster.Is_Deleted = false;
                obj_BM_DirectorMaster.DSC_ExpiryDate = obj.DESC_ExpiryDate;

                obj_BM_DirectorMaster.UpdatedBy = AuthenticationHelper.UserID;
                obj_BM_DirectorMaster.UpdatedOn = DateTime.Now;
                #endregion

                obj.Response = new BM_ManegmentServices.VM.Response();



                var hasError = false;
                long directorID = objService.Update_DirectorDetails(obj_BM_DirectorMaster, out hasError);

                #region added by Ruchi for update photo,pan and passport Documents
                if (_objcheckChangetype.PhotoGraph_of_Director)
                {
                    if (obj.Photo_Doc != null)
                    {
                        obj_BM_DirectorMaster.Photo_Doc = UploadFile(obj.Photo_Doc, "~/Areas/BM_Management/Documents/Director/" + directorID, "Photo_");
                        obj.Photo_Doc_Name = obj_BM_DirectorMaster.Photo_Doc;
                    }
                }

                if (obj.PAN_Doc != null)
                {
                    obj_BM_DirectorMaster.PAN_Doc = UploadFile(obj.PAN_Doc, "~/Areas/BM_Management/Documents/Director/" + directorID, "PAN_");
                    obj.PAN_Doc_Name = obj_BM_DirectorMaster.PAN_Doc;
                }

                if (obj.Aadhaar_Doc != null)
                {
                    obj_BM_DirectorMaster.Aadhar_Doc = UploadFile(obj.Aadhaar_Doc, "~/Areas/BM_Management/Documents/Director/" + directorID, "Aadhaar_");
                    obj.Aadhaar_Doc_Name = obj_BM_DirectorMaster.Aadhar_Doc;
                }

                if (obj.Passport_Doc != null)
                {
                    obj_BM_DirectorMaster.Passport_Doc = UploadFile(obj.Passport_Doc, "~/Areas/BM_Management/Documents/Director/" + directorID, "Passport_");
                    obj.Passport_Doc_Name = obj_BM_DirectorMaster.Passport_Doc;
                }
                obj_BM_DirectorMaster.Id = directorID;
                bool uploadsuccess = objService.Update_DirectorDocInfo(obj_BM_DirectorMaster);
                #endregion



                if (hasError)
                {
                    obj.Response.Error = true;
                    obj.Response.Message = "Something went wrong";
                }

                #endregion Change by Ruchi on 27th of March
                //End

                obj.Response.Success = true;
                obj.Response.Message = "Updated successfully";
            }
            else
            {
                obj.Response.Error = true;
                obj.Response.Message = "Something went wrong";
            }

            ModelState.Clear();
            obj.IsDetailsOfIntrest = true;
            return PartialView("_DirectorDetails", obj);
        }

        #region Updated by Ruchi on 21st April 2020

        [HttpPost]
        public ActionResult UPDATEDirectorDetails(BM_ManegmentServices.VM.Director_MasterVM obj)
        {
            if (ModelState.IsValid == false)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return PartialView("_DirectorDetails", obj);
            }

            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

            #region Change by Ruchi on 21st of April
            //BM_DirectorMaster obj_BM_Directordetails = objService.GetDirector(customerId, obj.ID);

            BM_DirectorMaster obj_BM_DirectorMaster = new BM_DirectorMaster();
            //string Email_Old = obj_BM_DirectorMaster.EmailId_Official;
            
            obj_BM_DirectorMaster.Salutation = obj.Salutation;
            obj_BM_DirectorMaster.FirstName = obj.FirstName;
            obj_BM_DirectorMaster.MiddleName = obj.MiddleName;
            obj_BM_DirectorMaster.LastName = obj.LastName;
            obj_BM_DirectorMaster.DIN = obj.DIN;
            obj_BM_DirectorMaster.DOB = (DateTime)obj.DateOfBirth;
            obj_BM_DirectorMaster.Gender = (int)obj.Gender;

            obj_BM_DirectorMaster.MobileNo = obj.MobileNo;

            obj_BM_DirectorMaster.EmailId_Personal = obj.EmailId;
            obj_BM_DirectorMaster.EmailId_Official = obj.EmailId_Official;

            #region Father's Name
            obj_BM_DirectorMaster.FSalutations = obj.FSalutations;
            obj_BM_DirectorMaster.Father = obj.FatherFirstName;
            obj_BM_DirectorMaster.FatherMiddleName = obj.FatherMiddleName;
            obj_BM_DirectorMaster.FatherLastName = obj.FatherLastName;
            
            #endregion

            #region Mother's Name
            obj_BM_DirectorMaster.Mother = obj.MotherName;
            #endregion

            #region Residential Address
            obj_BM_DirectorMaster.Id = obj.ID;
            obj_BM_DirectorMaster.Permenant_Address_Line1 = obj.Permenant_Address_Line1;
            obj_BM_DirectorMaster.Permenant_Address_Line2 = obj.Permenant_Address_Line2;
            obj_BM_DirectorMaster.Permenant_StateId = obj.Permenant_StateId;
            obj_BM_DirectorMaster.Permenant_CityId = (int)obj.Permenant_CityId;
            obj_BM_DirectorMaster.Permenant_PINCode = obj.Permenant_PINCode;

            obj_BM_DirectorMaster.IsSameAddress = obj.IsSameAddress;

            obj_BM_DirectorMaster.Present_Address_Line1 = obj.Present_Address_Line1;
            obj_BM_DirectorMaster.Present_Address_Line2 = obj.Present_Address_Line2;
            obj_BM_DirectorMaster.Present_StateId = obj.Present_StateId > 0 ? (int)obj.Present_StateId : 0;
            obj_BM_DirectorMaster.Present_CityId = obj.Present_CityId > 0 ? (int)obj.Present_CityId : 0;
            obj_BM_DirectorMaster.Present_PINCode = obj.Present_PINCode;

            #endregion

            obj_BM_DirectorMaster.EducationalQualification = obj.EducationalQualification;
            obj_BM_DirectorMaster.OtherQualification = obj.OtherQualification;
            obj_BM_DirectorMaster.Occupation = obj.Occupation;
            obj_BM_DirectorMaster.AreaOfOccupation = obj.AreaOfOccupation;
            obj_BM_DirectorMaster.OtherOccupation = obj.OtherOccupation;

            obj_BM_DirectorMaster.ResidentInIndia = obj.ResidentInIndia;
            obj_BM_DirectorMaster.Nationality = obj.Nationality > 0 ? (int)obj.Nationality : 0;
            obj_BM_DirectorMaster.PAN = obj.PAN;
            obj_BM_DirectorMaster.Aadhaar = obj.Adhaar;
            obj_BM_DirectorMaster.PassportNo = obj.PassportNo;
            obj_BM_DirectorMaster.DSC_ExpiryDate = obj.DESC_ExpiryDate;

            obj_BM_DirectorMaster.Customer_Id = customerId;
            
            obj_BM_DirectorMaster.IsDirecor = true;
            obj_BM_DirectorMaster.Is_Deleted = false;
            
            obj_BM_DirectorMaster.UpdatedBy = AuthenticationHelper.UserID;
            obj_BM_DirectorMaster.UpdatedOn = DateTime.Now;
            
            #region added by Ruchi for update photo,pan and passport Documents
            if (obj.Photo_Doc != null)
            {
                obj_BM_DirectorMaster.Photo_Doc = UploadFile(obj.Photo_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Photo_");
                obj.Photo_Doc_Name = obj_BM_DirectorMaster.Photo_Doc;
            }

            if (obj.PAN_Doc != null)
            {
                obj_BM_DirectorMaster.PAN_Doc = UploadFile(obj.PAN_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "PAN_");
                obj.PAN_Doc_Name = obj_BM_DirectorMaster.PAN_Doc;
            }

            if (obj.Aadhaar_Doc != null)
            {
                obj_BM_DirectorMaster.Aadhar_Doc = UploadFile(obj.Aadhaar_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Aadhaar_");
                obj.Aadhaar_Doc_Name = obj_BM_DirectorMaster.Aadhar_Doc;
            }

            if (obj.Passport_Doc != null)
            {
                obj_BM_DirectorMaster.Passport_Doc = UploadFile(obj.Passport_Doc, "~/Areas/BM_Management/Documents/Director/" + obj_BM_DirectorMaster.Id, "Passport_");
                obj.Passport_Doc_Name = obj_BM_DirectorMaster.Passport_Doc;
            }

            objService.Update_DirectorDoc(obj_BM_DirectorMaster);
            #endregion
            
            obj.Response = new BM_ManegmentServices.VM.Response();

            //Update User
            UserVM user = new UserVM();
            user.Email = obj_BM_DirectorMaster.EmailId_Official;
            user.FirstName = obj_BM_DirectorMaster.FirstName;
            user.LastName = obj_BM_DirectorMaster.LastName;
            user.CustomerID = customerId;
            user.ContactNumber = obj_BM_DirectorMaster.MobileNo;
            user.Address = obj_BM_DirectorMaster.Present_Address_Line1;

            obj_BM_DirectorMaster.UserID = objService.GetUserId(obj_BM_DirectorMaster.Id);
            string Email_Old = objService.GetEmail(obj_BM_DirectorMaster.Id);

            if (obj_BM_DirectorMaster.UserID.HasValue && !string.IsNullOrEmpty(Email_Old))
            {
                user.UserID = Convert.ToInt64(obj_BM_DirectorMaster.UserID);

                var createUserOfDirector = true;
                var accessToDirector = objISettingService.GetAccessToDirector(customerId);
                //var createUserOfDirector = false;
                //string IS_ICSI_MODE = ConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"].ToString();

                //if (IS_ICSI_MODE != "1")
                //{
                //    createUserOfDirector = objISettingService.CheckCreateUserOfDirectorId(customerId);
                //}
                if (createUserOfDirector == true)
                {
                    user = UpdateUser(user, Email_Old, accessToDirector);
                }
                else
                {
                    user.Message = new Response() { Success = true };
                }

                if (user.Message.Success)
                {
                    var hasError = false;
                    bool success = objService.Update_Director(obj_BM_DirectorMaster, out hasError);

                    if (hasError)
                    {
                        obj.Response.Error = true;
                        obj.Response.Message = "Server Error Occur";
                    }
                    if (success)
                    {
                        obj.Response.Success = true;
                        obj.Response.Message = "Updated successfully";
                    }
                    else
                    {
                        obj.Response.Error = true;
                        obj.Response.Message = "Something went wrong";
                    }
                }
                else
                {
                    obj.Response.Error = user.Message.Error;
                    obj.Response.Message = user.Message.Message;
                }
            }
            #endregion Change by Ruchi on 27th of March
            //End

            ModelState.Clear();

            return PartialView("_DirectorDetails", obj);
        }
        #endregion Updated by Ruchi on 21st April 2020

        [HttpPost]
        public PartialViewResult EditDirectorsRelatives(BM_ManegmentServices.VM.Directors_Relatives obj)
        {
            bool hasError = false;
            bool hasErrorSon = false;
            bool hasErrorDaughter = false;
            bool hasErrorBrother = false;
            bool hasErrorSister = false;

            bool success = false;
            if (ModelState.IsValid == false)
            {
                return PartialView("_DirectorsRelatives", obj);
            }

            success = objService.UpdateRelative(obj.Director_ID, obj.HUF, obj.Spouse, obj.Father, obj.Mother, out hasError);

            //Get temp Relative list for delete.....
            var lstRelativeTemp = objService.GetAllRelativeId(obj.Director_ID);
            var lstRelativeNew = new List<long>();

            if (obj.lstSon != null)
            {
                objService.CreateRelative_Son(obj.lstSon, obj.Director_ID, out hasErrorSon);
                foreach (var item in obj.lstSon)
                {
                    lstRelativeNew.Add(item.Id);
                }
            }
            else
            {
                obj.lstSon = new List<Relatives>();
            }

            if (obj.lstDaughter != null)
            {
                objService.CreateRelative_Daughter(obj.lstDaughter, obj.Director_ID, out hasErrorDaughter);
                foreach (var item in obj.lstDaughter)
                {
                    lstRelativeNew.Add(item.Id);
                }
            }
            else
            {
                obj.lstDaughter = new List<Relatives>();
            }

            if (obj.lstBrother != null)
            {
                objService.CreateRelative_Brother(obj.lstBrother, obj.Director_ID, out hasErrorBrother);
                foreach (var item in obj.lstBrother)
                {
                    lstRelativeNew.Add(item.Id);
                }
            }
            else
            {
                obj.lstBrother = new List<Relatives>();
            }

            if (obj.lstSister != null)
            {
                objService.CreateRelative_Sister(obj.lstSister, obj.Director_ID, out hasErrorSister);
                foreach (var item in obj.lstSister)
                {
                    lstRelativeNew.Add(item.Id);
                }
            }
            else
            {
                obj.lstSister = new List<Relatives>();
            }

            #region Delete relatives
            //var lstRelative = objService.GetAllRelativeId(obj.Director_ID);
            foreach (var item in lstRelativeTemp)
            {
                var flag = false;
                foreach (var rel in lstRelativeNew)
                {
                    if (rel == item.Id)
                    {
                        flag = true;
                    }
                }

                if (flag == false)
                {
                    objService.DeleteRelative(item.Id, obj.Director_ID);
                }
            }
            #endregion

            obj.Response = new BM_ManegmentServices.VM.Response();
            if (hasError || hasErrorSon || hasErrorDaughter || hasErrorBrother || hasErrorSister)
            {
                obj.Response.Error = true;
                obj.Response.Message = "Server Error Occure";
            }
            else if (success)
            {
                obj.Response.Success = true;
                obj.Response.Message = "Updated successfully";
            }
            else
            {
                obj.Response.Error = true;
                obj.Response.Message = "Something wents wrong";
            }

            ModelState.Clear();
            return PartialView("_DirectorsRelatives", obj);
        }

        public ActionResult Download(long id, string ImageName)
        {
            try
            {
                string _path = Path.Combine(Server.MapPath("~/Areas/BM_Management/Documents/Director/" + id), ImageName);
                if (System.IO.File.Exists(_path))
                {
                    return File(_path, "application/force-download", Path.GetFileName(_path));
                }
                else
                {
                    return PartialView("_FileNotFound");
                }
            }
            catch (Exception ex)
            {
                return PartialView("_FileNotFound");
            }
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            bool hasError = false;
            var success = objService.Delete(id, out hasError);
            return Json(String.Format("'Success':'{0}','Error':'{1}'", success, hasError));
        }

        [NonAction]
        public UserVM CreateUser(UserVM obj, bool accessToDirector = true, bool isActive = true)
        {
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;
                string passwordText = Util.CreateRandomPassword(10);
                obj.Password = passwordText;

                string Mailbody = getEmailMessage(obj, obj.Password, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                    IUser objIUser = new BM_ManegmentServices.Services.Masters.User();
                    obj.SecretarialRoleId = objIUser.GetRoleIdFromCode(SecretarialConst.Roles.DRCTR);

                    obj.accessToDirector = accessToDirector;
                    var result = objIUser.Create(obj, SenderEmailAddress, isActive);

                    if (result.Message.Success && accessToDirector)
                    {
                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Account Created", Mailbody);
                    }

                    obj.Message.Success = result.Message.Success;
                    obj.Message.Message = result.Message.Message;
                    obj.Message.Error = result.Message.Error;
                }
            }
            else
            {
                obj.Message.Error = true;
            }
            return obj;
        }

        [NonAction]
        public UserVM UpdateUser(UserVM obj, string Email_Old, bool accessToDirector = true)
        {
            obj.Message = new BM_ManegmentServices.VM.Response();
            if (ModelState.IsValid)
            {
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                obj.Message = new Response();

                obj.CreatedBy = UserID;
                obj.CustomerID = CustomerID;
                obj.CreatedByText = AuthenticationHelper.User;

                var hasError = false;

                string Mailbody = SendNotificationEmailChanged(obj, out hasError);
                if (hasError == true)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something went wrong, Please try again";
                }
                else
                {
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    IUser objIUser = new BM_ManegmentServices.Services.Masters.User();
                    var tempUser = objIUser.GetUser(CustomerID, Email_Old);

                    if (tempUser != null)
                    {
                        obj.UserID = tempUser.UserID;
                        obj.SecretarialRoleId = objIUser.GetRoleIdFromCode(SecretarialConst.Roles.DRCTR);
                        var result = objIUser.Update(obj, SenderEmailAddress, out Email_Old);

                        if (result.Message.Success)
                        {
                            if (Email_Old != result.Email && accessToDirector)
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { obj.Email }), null, null, "AVACOM Email Changed", Mailbody);

                            obj.Message.Success = true;
                        }
                        else
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = result.Message.Message;
                        }
                    }
                    else
                    {
                        obj.Message.Error = true;
                        obj.Message.Message = "Something went wrong, Please try again";
                    }
                }
            }
            else
            {
                obj.Message.Error = true;
                obj.Message.Message = "Something went wrong, Please try again";
            }
            return obj;
        }

        private string getEmailMessage(UserVM user, string passwordText, out bool hasError)
        {
            try
            {
                hasError = false;
                int customerID = -1;
                string ReplyEmailAddressName = "";
                int customerId = Convert.ToInt32(AuthenticationHelper.UserID);

                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                //cvUserPopup.ErrorMessage = "Something went wrong, Please try again";
                return null;
            }
        }

        private string SendNotificationEmailChanged(UserVM user, out bool hasError)
        {
            hasError = false;
            try
            {
                user.Message = new Response();
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(AuthenticationHelper.Role).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(AuthenticationHelper.Role).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    //customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).CustomerID ?? 0;
                    customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                hasError = true;
                user.Message.Error = true;
                user.Message.Message = "Something went wrong, Please try again";

            }
            return null;
        }

        public ActionResult CreateSecurity(long KmpID, long SecurityId)
        {
            Director_Master_KMPCompanySecurity objkmpsecuritydetails = new Director_Master_KMPCompanySecurity();
            objkmpsecuritydetails.KMP_Id = KmpID;
            if (KmpID > 0 && SecurityId > 0)
            {
                objkmpsecuritydetails = _objkmp.GetsecuritybyId(KmpID, SecurityId);
                objkmpsecuritydetails.Response = new BM_ManegmentServices.VM.Response();
            }

            return PartialView("_PartialDirectorCompanyDtls", objkmpsecuritydetails);
        }

        public ActionResult SecurityFileUpload(long DirectorId)
        {
            Director_MasterKMPUploadSecurity obj = new Director_MasterKMPUploadSecurity();
            obj.KMP_ID = DirectorId;
            return PartialView("_SecurityUpload", obj);
        }
        [HttpPost]
        public ActionResult UploadDirector_Securities(Director_MasterKMPUploadSecurity _objSecurities)
        {
            if (_objSecurities.File != null && _objSecurities.Id > 0)
            {
                if (_objSecurities.File.ContentLength > 0)
                {
                    int UserId = AuthenticationHelper.UserID;

                    string path = "~/Areas/BM_Management/Documents/Director_SecritiesDetails/" + "/" + UserId + "/";
                    string _file_Name = Path.GetFileName(_objSecurities.File.FileName);
                    string _path = Path.Combine(Server.MapPath(path), _file_Name);
                    bool exists = System.IO.Directory.Exists(Server.MapPath(path));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(path));

                    _objSecurities.File.SaveAs(_path);
                    FileInfo excelfile = new FileInfo(_path);
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = DirectorSheetsExitsts(xlWorkbook, "SecurityDetails");
                            if (flag == true)
                            {

                                SecurityDetails(xlWorkbook, _objSecurities.Id, _objSecurities.Current_Date);
                            }
                            else
                            {
                                ViewBag.Error = "No Data Found in Excel Document or Sheet Name must be 'DirectorSecurity'.";
                            }
                        }
                    }
                }
            }
            return PartialView("_SecurityUpload");
        }

        private bool DirectorSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("SecurityDetails"))
                    {
                        if (sheet.Name.Trim().Equals("SecurityDetails"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ViewBag.ErrorMessage = "Server error occured'.";
            }
            return false;
        }

        private void SecurityDetails(ExcelPackage xlWorkbook, long KMPID, string Current_date)
        {
            List<string> errorMessage = new List<string>();
            try
            {
                bool saveSuccess = false;
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["SecurityDetails"];
                if (xlWorksheet != null)
                {

                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    string CompanyName = string.Empty;
                    int Securities_Number = -1;
                    string Securities_Description = string.Empty;
                    long Securities_NominalValue = -1;
                    DateTime? acquisition_Date;
                    decimal acquisitionsecurities_PricePaid = -1;
                    long Other_acquisition_considerationpaid = -1;
                    DateTime? Disposal_Date;
                    decimal Disposal_RecivedPrice = -1;
                    decimal Other_considerationRecivedPrice = -1;
                    //string acquisition_securities_Mode = string.Empty;
                    long CBal_SecuritiesNO = -1;
                    string ModeofAcquisition = string.Empty;
                    string ModeofHolding = string.Empty;
                    string Any_encumbrance_or_pledged = string.Empty;


                    #region Validations
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        CompanyName = string.Empty;
                        Securities_Number = -1;
                        Securities_Description = string.Empty;
                        Securities_NominalValue = -1;
                        acquisition_Date = null;
                        acquisitionsecurities_PricePaid = -1;
                        Other_acquisition_considerationpaid = -1;
                        Disposal_Date = null;
                        Disposal_RecivedPrice = -1;
                        Other_considerationRecivedPrice = -1;
                        //acquisition_securities_Mode = string.Empty;
                        CBal_SecuritiesNO = -1;
                        ModeofAcquisition = string.Empty;
                        ModeofHolding = string.Empty;
                        Any_encumbrance_or_pledged = string.Empty;

                        #region 1 CompanyName
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                        {
                            CompanyName = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            if (string.IsNullOrEmpty(CompanyName))
                            {
                                errorMessage.Add("Please Check the CompanyName can not be empty at row - " + i + "");
                            }
                        }
                        #endregion

                        #region 2 Securities Number
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                        {
                            Securities_Number = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(Securities_Number)))
                        {
                            errorMessage.Add("Please Check the Securities Number can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region 3 Securities Description
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                        {
                            Securities_Description = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Securities_Description))
                        {
                            errorMessage.Add("Required Securities Description at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 4 Securities NominalValue
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                        {
                            Securities_NominalValue = Convert.ToInt64((xlWorksheet.Cells[i, 4].Text).Trim());
                        }
                        if (Securities_NominalValue <= 0)
                        {
                            errorMessage.Add("Required Securities NominalValue at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 5 Acquisition_Date
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                        {
                            acquisition_Date = Convert.ToDateTime(xlWorksheet.Cells[i, 5].Text);
                        }
                        if (acquisition_Date == null)
                        {
                            errorMessage.Add("Required Acquisition_Date at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region  6 Acquisition Securities PricePaid
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                        {
                            acquisitionsecurities_PricePaid = Convert.ToUInt32(xlWorksheet.Cells[i, 6].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Convert.ToString(acquisitionsecurities_PricePaid)))
                        {
                            errorMessage.Add("Required Acquisition Securities PricePaid at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 7 Other Acquisition Consideration Paid
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                        {
                            Other_acquisition_considerationpaid = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                        }
                        //if (Other_acquisition_considerationpaid <= 0)
                        //{
                        //    errorMessage.Add("Required Other Acquisition Consideration Paid at row number - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 8 Disposal Date                        
                        if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                        {
                            Disposal_Date = (Convert.ToDateTime(Convert.ToString(xlWorksheet.Cells[i, 8].Text)));
                        }
                        //if (string.IsNullOrEmpty(Disposal_Date))
                        //{
                        //    errorMessage.Add("Required Disposal Date  at row - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 9 Disposal RecivedPrice
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                        {
                            Disposal_RecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text.Trim());
                        }
                        //if (Disposal_RecivedPrice < 0)
                        //{
                        //    errorMessage.Add("Required Disposal Recived Price at row number - " + (count + 1) + "");
                        //}
                        #endregion

                        #region 10 Other_considerationRecivedPrice   
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                        {
                            Other_considerationRecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim());
                        }
                        //if (Other_considerationRecivedPrice < 0)
                        //{
                        //    errorMessage.Add("Required Other consideration Recived Price at row number - " + (count + 1) + "");
                        //}
                        #endregion



                        #region 11 Mode
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                        {
                            ModeofAcquisition = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(ModeofAcquisition))
                        {
                            errorMessage.Add("Required Mode of Security at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 12 CBal_SecuritiesNO
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                        {
                            CBal_SecuritiesNO = Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(Convert.ToString(CBal_SecuritiesNO)))
                        {
                            errorMessage.Add("Required Cumulative balance and number of securities held after each transaction at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 13 Mode
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                        {
                            ModeofHolding = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                        }
                        if (String.IsNullOrEmpty(ModeofAcquisition))
                        {
                            errorMessage.Add("Required Mode of Security at row number - " + (count + 1) + "");
                        }
                        #endregion

                        #region 14 Any_encumbrance_or_pledged
                        if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim())))
                        {
                            Any_encumbrance_or_pledged = Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim());
                        }

                        #endregion

                    }

                    #endregion

                    if (errorMessage.Count <= 0)
                    {
                        #region Save

                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;
                            CompanyName = string.Empty;
                            Securities_Number = -1;
                            Securities_Description = string.Empty;
                            Securities_NominalValue = -1;
                            acquisition_Date = null;
                            acquisitionsecurities_PricePaid = -1;
                            Other_acquisition_considerationpaid = -1;
                            Disposal_Date = null;
                            Disposal_RecivedPrice = -1;
                            Other_considerationRecivedPrice = -1;
                            //acquisition_securities_Mode = string.Empty;
                            CBal_SecuritiesNO = -1;
                            ModeofAcquisition = string.Empty;
                            ModeofHolding = string.Empty;
                            Any_encumbrance_or_pledged = string.Empty;

                            #region 1 CompanyName
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 1].Text.Trim())))
                            {
                                CompanyName = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                            }
                            #endregion

                            #region 2 Securities_Number
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 2].Text.Trim())))
                            {
                                Securities_Number = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region 3 Securities_Description
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim())))
                            {
                                Securities_Description = Convert.ToString(xlWorksheet.Cells[i, 3].Text.Trim());
                            }
                            #endregion

                            #region 4 Securities_NominalValue
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim()))
                            {
                                Securities_NominalValue = Convert.ToInt64((xlWorksheet.Cells[i, 4].Text).Trim());
                            }
                            #endregion

                            #region 5 Acquisition Date
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim()))
                            {
                                acquisition_Date = Convert.ToDateTime(xlWorksheet.Cells[i, 5].Text);
                            }
                            #endregion

                            #region  6 Acquisition Securities PricePaid
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 6].Text.Trim())))
                            {
                                acquisitionsecurities_PricePaid = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text.Trim());
                            }
                            #endregion

                            #region 7 Other Acquisition Considerationpaid
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 7].Text.Trim())))
                            {
                                Other_acquisition_considerationpaid = Convert.ToInt64((xlWorksheet.Cells[i, 7].Text.Trim()));
                            }
                            #endregion

                            #region 8 Disposal_Date                       
                            if (!String.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim())))
                            {
                                Disposal_Date = Convert.ToDateTime(Convert.ToString(xlWorksheet.Cells[i, 8].Text.Trim()));
                            }
                            #endregion

                            #region 9 Disposal Recived Price
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 9].Text.Trim())))
                            {
                                Disposal_RecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 9].Text.Trim());
                            }
                            #endregion

                            #region 10 Other_considerationRecivedPrice   
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 10].Text.Trim())))
                            {
                                Other_considerationRecivedPrice = Convert.ToInt32(xlWorksheet.Cells[i, 10].Text.Trim());
                            }
                            #endregion

                            //#region 11 acquisition_securities_Mode
                            //if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                            //{
                            //    acquisition_securities_Mode = Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim());
                            //}
                            //#endregion

                            #region 11 CBal_SecuritiesNO
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 11].Text.Trim())))
                            {
                                CBal_SecuritiesNO = Convert.ToInt32(xlWorksheet.Cells[i, 11].Text.Trim());
                            }
                            #endregion

                            #region 12 Mode
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim())))
                            {
                                ModeofAcquisition = Convert.ToString(xlWorksheet.Cells[i, 12].Text.Trim());
                            }
                            #endregion

                            #region 13 Mode
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim())))
                            {
                                ModeofHolding = Convert.ToString(xlWorksheet.Cells[i, 13].Text.Trim());
                            }
                            #endregion

                            #region 14 Any_encumbrance_or_pledged
                            if (!string.IsNullOrEmpty(Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim())))
                            {
                                Any_encumbrance_or_pledged = Convert.ToString(xlWorksheet.Cells[i, 14].Text.Trim());
                            }
                            #endregion

                            BM_SecurityDetail Obj_Kmpsecurity = new BM_SecurityDetail();
                            if (KMPID > 0)
                                Obj_Kmpsecurity.Director_KmpId = Convert.ToInt32(KMPID);
                            Obj_Kmpsecurity.CustomerId = CustomerId;
                            Obj_Kmpsecurity.CompanyName = CompanyName;
                            Obj_Kmpsecurity.securities_No = Securities_Number;
                            Obj_Kmpsecurity.Dis_of_securities = Securities_Description;
                            Obj_Kmpsecurity.Nominam_valSecurities = Securities_NominalValue;
                            if (acquisition_Date != null)
                                Obj_Kmpsecurity.acquisition_Date = acquisition_Date;
                            Obj_Kmpsecurity.acquisition_pricePaid = acquisitionsecurities_PricePaid;
                            Obj_Kmpsecurity.Other_ConsiPaid_acquisition = Other_acquisition_considerationpaid;
                            Obj_Kmpsecurity.disposal_RescPrice = Disposal_RecivedPrice;
                            Obj_Kmpsecurity.otherconsideration_disposal_RescPrice = Other_considerationRecivedPrice;
                            if (Disposal_Date != null)
                                Obj_Kmpsecurity.Date_disposal = Disposal_Date;
                            Obj_Kmpsecurity.Price_Received_disposal = Disposal_RecivedPrice;
                            Obj_Kmpsecurity.Cbal_No_securities_afterTransaction = CBal_SecuritiesNO;
                            Obj_Kmpsecurity.Mode_of_acquisitionSecurities = ModeofAcquisition;
                            Obj_Kmpsecurity.DematerializedorPhysical = ModeofHolding;
                            Obj_Kmpsecurity.Securities_encumbrance = Any_encumbrance_or_pledged;

                            saveSuccess = objService.CreateDirector_Security(Obj_Kmpsecurity);
                            if (!saveSuccess)
                            {
                                ViewBag.Error = "Director Security Data allready exist";
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (errorMessage.Count > 0)
                        {
                            ViewBag.ErrorMessage = errorMessage;
                        }
                    }

                    if (saveSuccess)
                    {
                        ViewBag.SuccessMessage = "Director Securities Details Uploaded Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                errorMessage.Add("" + ex.Message + "");
                ViewBag.Error = errorMessage;
            }
        }

        public ActionResult DownloadSampleFormateforDirectorSecurity()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Areas\\Document\\SampleDocument\\DirectorSecurities\\";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "DirectorSecurity.xlsx");
            string fileName = "DirectorSecurity.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult SaveDirectorSecurity(Director_Master_KMPCompanySecurity _obj_KMP)
        {
            if (ModelState.IsValid)
            {

                if (_obj_KMP.Id_sec == 0)
                {
                    var objkempdtls = _objkmp.CreateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    return PartialView("_PartialDirectorCompanyDtls", objkempdtls);
                }
                else
                {
                    var objkempdtls = _objkmp.UpdateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    return PartialView("_PartialDirectorCompanyDtls", objkempdtls);
                }

            }
            else
            {
                return PartialView("_PartialKMPCompanyDtls", _obj_KMP);
            }
        }

        public ActionResult SaveKMP_CompanySecurity(Director_Master_KMPCompanySecurity _obj_KMP)
        {
            if (ModelState.IsValid)
            {

                if (_obj_KMP.Id_sec == 0)
                {
                    _obj_KMP = _objkmp.CreateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    //return PartialView("_PartialDirectorCompanyDtls", objkempdtls);
                }
                else
                {
                    _obj_KMP = _objkmp.UpdateKMP_CompanySecurity(_obj_KMP, CustomerId);
                    // return PartialView("_PartialDirectorCompanyDtls", objkempdtls);
                }

            }

            //return PartialView("_PartialDirectorCompanyDtls", _obj_KMP);
            return Json(_obj_KMP, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadFileopenPopUP()
        {
            VMEntityFileUpload objfileupload = new VMEntityFileUpload();
            return PartialView("UploadDirectorDetails", objfileupload);
        }

        public ActionResult ImportDIR3(VMEntityFileUpload objfileupload)
        {
            if (ModelState.IsValid)
            {
                objfileupload.CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                objfileupload.UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                objfileupload = objService.ImportDataFormDIR3(objfileupload);
            }
            return PartialView("UploadDirectorDetails", objfileupload);
        }

        public ActionResult OpenOneditDirector(long DirectorID)
        {
            OnEditingDirector _objonEditingDirector = new OnEditingDirector();

            _objonEditingDirector = objService.checkIscheckedornot(DirectorID);
            if (_objonEditingDirector == null)
            {
                _objonEditingDirector = new OnEditingDirector();
                _objonEditingDirector.DirectorID = DirectorID;
            }
            _objonEditingDirector.DirectorID = DirectorID;
            return PartialView("_OnEditChange", _objonEditingDirector);
        }

        [HttpPost]
        public ActionResult DirectorTypeChanges(OnEditingDirector _objonEditingDirector)
        {
            if (ModelState.IsValid)
            {
                _objonEditingDirector.UserId = AuthenticationHelper.UserID;
                _objonEditingDirector = objService.SaveDirectorTypeChanges(_objonEditingDirector);

            }
            return PartialView("_OnEditChange", _objonEditingDirector);
        }


        public ActionResult GetDitectorChangeType(long DirectorId)
        {

            OnEditingDirector _objonEditing = new OnEditingDirector();
            if (DirectorId > 0)
            {
                _objonEditing = objService.getDirectorTypeChanges(DirectorId);
            }

            return Json(_objonEditing, JsonRequestBehavior.AllowGet);
        }

        #region Resignation of director
        public ActionResult ResignationOfDirector(int detailsOfInterestId)
        {
            var model = new Director_ResignationVM();
            model = objService.ResignationOfDirector(detailsOfInterestId);
            return PartialView("_ResignationOfDirector", model);
        }

        [HttpPost]
        public ActionResult SaveResignationOfDirector(Director_ResignationVM model)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(AuthenticationHelper.UserID);
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                model = objService.SaveResignationOfDirector(model, userId);
            }
            return PartialView("_ResignationOfDirector", model);
        }

        [HttpPost]
        public ActionResult RevertResignationOfDirector(long id)
        {
            int userId = Convert.ToInt32(AuthenticationHelper.UserID);
            var result = objService.RevertResignationOfDirector(id, userId);
            var model = objService.ResignationOfDirector(id);
            if(result.Error)
            {
                model.Error = true;
                model.Message = result.Message;
            }
            else
            {
                model.Error = true;
                model.Message = result.Message;
            }

            return PartialView("_ResignationOfDirector", model);
        }
        #endregion

    }
}