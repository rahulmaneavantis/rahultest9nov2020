﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class NotesController : Controller
    {
        // GET: BM_Management/Notes
        INote obj;
        public NotesController(INote obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetNotes([DataSourceRequest] DataSourceRequest request, long MeetingId, long AgendaId)
        {
            int userId = AuthenticationHelper.UserID;
            int CustomerId = (int)AuthenticationHelper.CustomerID;
            var getNotes = obj.GetsNotes(userId, CustomerId, MeetingId, AgendaId);
            return Json(getNotes.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SharedNotesList(long MeetingId)
        {
            VMNotes obj = new VMNotes();
            obj.MeetingId = MeetingId;
            return PartialView("_SharedNotesList", obj);
        }

        [HttpGet]
        public ActionResult GetSharedNotes([DataSourceRequest] DataSourceRequest request, long MeetingId)
        {
            List<VMNotes> objnotes = new List<VMNotes>();
            int userId = AuthenticationHelper.UserID;
            int CustomerId = (int)AuthenticationHelper.CustomerID;
            objnotes = obj.GetsSharedNotes(userId, CustomerId, MeetingId);
            return Json(objnotes.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
      
        [PageViewFilter]
        public ActionResult MyNotes(long MeetingId,long AgendaId)
        {
            VMNotes objvmnotes = new VMNotes();
            objvmnotes.MeetingId = MeetingId;
            objvmnotes.AgendaId = AgendaId;
            return PartialView("_NotesList",objvmnotes);
        }

        [PageViewFilter]
        [HttpGet]
        public ActionResult AddNotes(long Id,long MeetingId,long AgendaId,string NotesTitle)
        {
            VMNotes _objvmnotes = new VMNotes();
            _objvmnotes.MeetingId = MeetingId;
            _objvmnotes.AgendaId = AgendaId;
            _objvmnotes.Title = NotesTitle;
          if(Id>0)
            {
                _objvmnotes = obj.GetNotesbyId(Id, MeetingId, AgendaId);
            }
            return PartialView("_AddEditNotes", _objvmnotes);
        }

        

        [HttpPost]
    public ActionResult CreateNotes(VMNotes objnotes)
        {
            if(ModelState.IsValid)
            {
                
                objnotes.UserId = AuthenticationHelper.UserID;
                objnotes.CustomerId = (int)AuthenticationHelper.CustomerID;
                if (objnotes.Id == 0)
                {
                    objnotes = obj.CreateNotes(objnotes);
                }
                else
                {
                    objnotes = obj.UpdateNotes(objnotes);
                }
            }
            return PartialView("_AddEditNotes", objnotes);
        }


    }
}