﻿using BM_ManegmentServices.Services.Compliance;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BM_ManegmentServices;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class ComplianceAssignController : Controller
    {
        //ICompliance_ServiceSecretarial objCompliance_Service;
        //public ComplianceAssignController(ICompliance_ServiceSecretarial obj)
        //{
        //    this.objCompliance_Service = obj;
        //}

        ICompliance_Service objCompliance_Service;
        IDirectorCompliances objIDirectorCompliances;
        IEntityMaster objIEntityMaster;
        public ComplianceAssignController(ICompliance_Service obj, IDirectorCompliances objDirectorCompliances, IEntityMaster objEntityMaster)
        {
            this.objCompliance_Service = obj;
            objIDirectorCompliances = objDirectorCompliances;
            objIEntityMaster = objEntityMaster;
        }

        public ActionResult Director()
        {
            var model = SecretarialConst.ComplianceMappingType.DIRECTOR;

            //objIDirectorCompliances.GenerateDirectorScheduleOn(3, "TypeOfChange", DateTime.Now, 1, "Admin");
            return View("ComplianceAssign_Director", (object)model);
        }

        #region Agenda/Meeting Level View
        public ActionResult Agenda()
        {
            var model = SecretarialConst.ComplianceMappingType.AGENDA;
            return View("ComplianceAssign", (object)model);
        }
        public ActionResult Meeting()
        {
            var model = SecretarialConst.ComplianceMappingType.MEETING;
            return View("ComplianceAssign", (object)model);
        }
        #endregion

        #region Pending Compliances
        [HttpPost]
        public ActionResult GetAgendaComplianceList(int EntityID)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetAgendaComplianceList(EntityID, CustomerID);
            if (result != null)
            {
                var AgendaItems = result.Select(r => new { r.AgendaId, r.AgendaItem, r.MeetingTypeId, r.MeetingTypeName, r.IsAgendaSelect }).Distinct().ToList();
                var Compliances = result.Select(r => new { r.ComplianceId, r.Description, r.IsComplianceSelect, r.PerformerID, r.ReviewerID, r.Performer, r.Reviewer }).Distinct().ToList();
                var AgendaCompliances = result.Select(r => new { r.AgendaId, r.AgendaItem, r.ComplianceId, r.Description, r.IsComplianceSelect, r.PerformerID, r.ReviewerID, r.EntityID, r.Performer, r.Reviewer }).ToList();

                return Json(new { AgendaItems = AgendaItems, Compliances = Compliances, AgendaCompliances = AgendaCompliances }, JsonRequestBehavior.AllowGet);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMeetingComplianceList(int EntityID)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetMeetingComplianceList(EntityID, CustomerID);
            if (result != null)
            {
                var Compliances = result.Select(r => new { r.ComplianceId, r.Description, r.IsComplianceSelect, r.PerformerID, r.ReviewerID, r.Performer, r.Reviewer }).Distinct().ToList();
                var MeetingCompliances = result.Select(r => new { r.ComplianceId, r.Description, r.MeetingTypeId, r.MeetingTypeName, r.IsComplianceSelect }).ToList();

                return Json(new { Compliances = Compliances }, JsonRequestBehavior.AllowGet);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStatutoryComplianceList(int EntityID)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetStatutoryComplianceList(EntityID, CustomerID);
            //if (result != null)
            //{
            //    var Acts = result.Select(r => new { r.ActId, r.ActName }).Distinct().ToList();
            //    var Compliances = result.Select(r => new { r.ActId, r.ActName, r.ComplianceId, r.ShortDescription, r.Description, r.Section, r.Risk, r.IsComplianceSelect, r.PerformerID, r.ReviewerID, r.Performer, r.Reviewer }).ToList();
            //    var StatutoryCompliances = result.Select(r => new { r.ActId, r.ComplianceId, r.Description, r.IsComplianceSelect, r.PerformerID, r.ReviewerID, r.EntityID, r.Performer, r.Reviewer }).ToList();

            //    return Json(new { Acts = Acts, Compliances = Compliances }, JsonRequestBehavior.AllowGet);
            //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StatutoryCompliance_Read([DataSourceRequest] DataSourceRequest request, int EntityID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetStatutoryComplianceList(EntityID, customerID);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Assigned Compliances
        public ActionResult AssignedCompliances_Read([DataSourceRequest] DataSourceRequest request, int EntityID, string MappingType)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetAssignedCompliances(EntityID, customerID, MappingType);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportAssignedCompliances(int EntityID, string MappingType)
        {
            using (ExcelPackage excelPackge = new ExcelPackage())
            {
                try
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    #region Create DataTable
                    System.Data.DataTable dt = new System.Data.DataTable();
                    var result = objCompliance_Service.GetAssignedCompliances(EntityID, customerID, MappingType);
                    if(result!=null)
                    {
                        dt.Columns.Add("SrNo", typeof(Int64));
                        dt.Columns.Add("ComplianceID", typeof(Int64));
                        dt.Columns.Add("Compliance", typeof(string));
                        dt.Columns.Add("Performer", typeof(string));
                        dt.Columns.Add("Reviewer", typeof(string));

                        Int64 SrNo = 0;
                        foreach (var item in result)
                        {
                            SrNo++;
                            dt.Rows.Add(SrNo, item.ComplianceId, item.Description, item.Performer.FullName, item.Reviewer.FullName);
                        }
                    }
                    #endregion

                    ExcelWorksheet sheet = excelPackge.Workbook.Worksheets.Add("Assigned Compliances");

                    int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var EntityName = objIEntityMaster.GetEntityName(EntityID, CustomerID);

                    #region Header
                    ExcelRange range = sheet.Cells[1, 1, 1, 5];
                    range.Merge = true;
                    range.Value = "Assigned Compliance List";
                    range.Style.Font.Size = 14;
                    range.Style.Font.Bold = true;
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    range = sheet.Cells[2, 1, 2, 5];
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Value = "Entity Name : " + EntityName;
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    #endregion

                    #region Columns
                    sheet.Cells[3, 1].Value = "SrNo";
                    sheet.Cells[3, 1].Style.Font.Bold = true;
                    sheet.Cells[3, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[3, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    sheet.Cells[3, 2].Value = "Compliance ID";
                    sheet.Cells[3, 2].Style.Font.Bold = true;
                    sheet.Cells[3, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[3, 2].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    sheet.Cells[3, 3].Value = "Compliance";
                    sheet.Cells[3, 3].Style.Font.Bold = true;
                    sheet.Cells[3, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[3, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    sheet.Cells[3, 3].Style.WrapText = true;

                    sheet.Cells[3, 4].Value = "Performer";
                    sheet.Cells[3, 4].Style.Font.Bold = true;
                    sheet.Cells[3, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[3, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    sheet.Cells[3, 5].Value = "Reviewer";
                    sheet.Cells[3, 5].Style.Font.Bold = true;
                    sheet.Cells[3, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[3, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    #endregion

                    if (dt.Rows.Count > 0)
                    {
                        sheet.Cells[4,1].LoadFromDataTable(dt, false);

                        for (int i = 1; i <= dt.Rows.Count; i++)
                        {
                            sheet.Cells[i+ 3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells[i + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells[i + 3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells[i + 3, 3].Style.WrapText = true;

                            sheet.Cells[i + 3, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            sheet.Cells[i + 3, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            sheet.Cells[i + 3, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            sheet.Cells[i + 3, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            sheet.Cells[i + 3, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }
                    } 

                    range = sheet.Cells[dt.Rows.Count +4, 1, dt.Rows.Count + 4, 5];
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Value = "Date : "+DateTime.Now.Date.ToString("dd/MMM/yyyy");
                    range.Style.Font.Size = 9;
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;


                    range = sheet.Cells[1, 1, dt.Rows.Count + 4, 5];

                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    sheet.Column(1).AutoFit();
                    sheet.Column(2).AutoFit();
                    sheet.Column(3).Width = 60;
                    sheet.Column(4).AutoFit();
                    sheet.Column(5).AutoFit();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                byte[] data = excelPackge.GetAsByteArray();
                return File(data, "application/octet-stream","AssignedCompliance_"+DateTime.Now+".xlsx");
            }
        }
        #endregion

        #region Compliance Assignment
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgendaComplianceList_SaveJson(AgendaComplianceAssingVM agendaItems)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            string userName = AuthenticationHelper.User;
            agendaItems.CustomerID = CustomerID;
            agendaItems.UserID = UserID;
            agendaItems.UserName = userName;

            if (agendaItems.MappingType == "S")
                objCompliance_Service.CreateComplianceInstance_Statutory(agendaItems);
            else
                objCompliance_Service.CreateComplianceInstance(agendaItems);

            return Json(agendaItems, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Compliance Re-Assignment
        [HttpPost]
        public ActionResult GetAssignedCompliancesDetails(int EntityID, string MappingType, int UserID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetAssignedCompliancesDetails(EntityID, customerID, MappingType, UserID);

            return Json(new { AssignedCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ComplianceReAssignList_SaveJson(ComplianceReAssingmentVM items)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            items.CustomerID = CustomerID;
            items.UpdatedBy = UserID;

            objCompliance_Service.ComplianceReAssignment(items);
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Compliance Assignment Upload

        [HttpPost]
        public ActionResult UploadAComplianceAssignment(UploadComplianceAssignmentVM obj)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

            if (obj != null)
            {
                obj.isUploaded = true;
                HttpPostedFileBase file = obj.File;
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    var lstCompliance = new List<UploadComplianceVM>();
                    var isValid = true;

                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        ExcelWorkbook e;

                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            int entityID = 0;

                            int.TryParse(workSheet.Cells[rowIterator, 1].Value.ToString(), out entityID);

                            long complianceId = 0;

                            long.TryParse(workSheet.Cells[rowIterator, 3].Value.ToString(), out complianceId);

                            string performerMail = workSheet.Cells[rowIterator, 5].Value.ToString();
                            string reviewerMail = workSheet.Cells[rowIterator, 6].Value.ToString();
                            string scheduleOn = workSheet.Cells[rowIterator, 7].Value.ToString();

                            var complaince = objCompliance_Service.CreateInstance(CustomerID, entityID, complianceId, performerMail, reviewerMail, scheduleOn, UserID);

                            if (!complaince.IsValid)
                            {
                                isValid = false;
                            }

                            lstCompliance.Add(complaince);
                        }

                        if (isValid)
                        {
                            objCompliance_Service.CreateInstance(lstCompliance, CustomerID, UserID);
                            obj.Success = true;

                            obj.Message = lstCompliance.Count + " record uploaded successfully.";
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = "Validation fails.";
                            obj.lstCompliance = lstCompliance;
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Please Upload File";
                }
            }
            else
            {
                obj.Error = true;
                obj.Message = "Please Upload File";
            }

            return PartialView("_UploadComplianceAssignment", obj);
        }
        #endregion

        #region Director Related Compliance

        [HttpPost]
        public ActionResult GetDirectorComplianceList(long directorId)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetDirectorComplianceList(directorId, CustomerID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgendaComplianceList_SaveJsonForDirector(AgendaComplianceAssingVM agendaItems)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            agendaItems.CustomerID = CustomerID;
            agendaItems.UserID = UserID;

            objCompliance_Service.CreateComplianceInstanceForDirector(agendaItems);
            return Json(agendaItems, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AssignedCompliancesDirector_Read([DataSourceRequest] DataSourceRequest request, long directorId)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetAssignedCompliancesDirector(directorId, customerID);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult GetAssignedCompliancesDetailsDirector(long directorId, string MappingType, int UserID)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var result = objCompliance_Service.GetAssignedCompliancesDetailsForDirector(directorId, customerID, UserID);

            return Json(new { AssignedCompliances = result }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ComplianceReAssignListForDirector_SaveJson(ComplianceReAssingmentVM items)
        {
            int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
            items.CustomerID = CustomerID;
            items.UpdatedBy = UserID;

            objCompliance_Service.ComplianceReAssignmentForDirector(items);
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StatutoryNew()
        {
            var model = SecretarialConst.ComplianceMappingType.STATUTORY;
            return View("ComplianceAssignStatutory", (object)model);
        }

        public ActionResult Statutory()
        {
            var model = SecretarialConst.ComplianceMappingType.STATUTORY;
            return View("ComplianceAssign", (object)model);
        }
    }
}