﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Profile;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Windows.Media;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;
using Telerik.Windows.Documents.Flow.Model.Fields;
using Telerik.Windows.Documents.Flow.Model.Styles;

using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.InteractiveForms;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;

using Telerik.Windows.Documents.Spreadsheet.Theming;



namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MyProfileController : Controller
    {
        IMyProfile objprofile;
        IDirectorMaster objService;
        public MyProfileController(IMyProfile objprofile, IDirectorMaster objService)
        {
            this.objprofile = objprofile;
            this.objService = objService;
        }
        int userID = AuthenticationHelper.UserID;
        int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        // GET: BM_Management/MyProfile
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult  DirectorProfile()
        {
            Director obDirector = new Director();
           
            obDirector.objDirector_MasterVM =objprofile.GetDirectorDetails(userID, CustomerID);
            obDirector.objDirector_MasterVM.Response = new BM_ManegmentServices.VM.Response();

            return View(obDirector);
        }

        public ActionResult BODList()
        {
          
            return View();
        }

        public ActionResult CommiteeList()
        {
          
            return View();
        }

        public ActionResult MeetingsForDirector()
        {

           
            return View();
        }

        public PartialViewResult GetDirectorForEntity(int Entity_Id)
        {
           
            return PartialView("_GetDirector", Entity_Id);
        }

        public ActionResult TypeofChanges()
        {
         
            return View();
        }


        public ActionResult UpdateDirectorDetails(Director_MasterVM obj,string submitButton)
        {
            Director_MasterVM obDirector = new Director_MasterVM();
            if (submitButton == "Approved")
            {
                obj.Approve_disapprove = "Approve";
            }
            else if(submitButton == "Reject")
            {
                obj.Approve_disapprove = "Reject";
            }
            else if(submitButton=="Save")
            {
                obj.Approve_disapprove = "Save";
            }
            obj.UserID = userID;
            obDirector = objprofile.updateDirectorProfile(obj);
     
            //return PartialView("_DirectorPersonalProfile",obDirector);
            return Json(new { obj});
        }


        public ActionResult OpenOneditDirector(long DirectorID)
        {
            OnEditingDirector _objonEditingDirector = new OnEditingDirector();
            _objonEditingDirector.DirectorID = DirectorID;
            _objonEditingDirector = objService.checkIscheckedornot(DirectorID);
            if (_objonEditingDirector == null)
            {
                _objonEditingDirector = new OnEditingDirector();
                _objonEditingDirector.DirectorID = DirectorID;
            }
            _objonEditingDirector.DirectorID = DirectorID;

          
            return PartialView("_OnEditChange", _objonEditingDirector);
        }

        [HttpPost]
        public ActionResult DirectorTypeChanges(OnEditingDirector _objonEditingDirector)
        {
            if (ModelState.IsValid)
            {
                _objonEditingDirector.UserId = AuthenticationHelper.UserID;
                _objonEditingDirector = objService.SaveDirectorTypeChanges(_objonEditingDirector);

            }
        
             return PartialView("_OnEditChange", _objonEditingDirector);
        }

        public ActionResult Edit(long id)
        {
            Director_MasterVM obj = new Director_MasterVM();
            obj.ID = id;
            obj = objService.GetDirectorHistory(id);
            
            return PartialView("_DirectorHistoryEdit", obj);
        }

        public ActionResult DirectorHistory()
        {
            return View();
        }

        public ActionResult OpenOneditAssignment(long ID)
        {
          
            OnEditingDirector objedit = new OnEditingDirector();
            objedit = objprofile.getOnEditChanges(ID);
          
            return PartialView("_DirectorUserAssignment", objedit);
        }
        [HttpPost]
        public ActionResult SaveDirectorTypeOfChange(OnEditingDirector obj)
        {
            obj = objprofile.SaveDirectorTypeOfChange(obj);

            return PartialView("_DirectorUserAssignment", obj);
        }

        public ActionResult OpenDirectorRegignation(long? ID, long? EntityId)
        {
            ResignationofDirector objResignation = new ResignationofDirector();
            objResignation.DirectorID = ID;
            objResignation.EntityID = EntityId;
            objResignation = objprofile.GetDetailsofIntrest(ID, EntityId);
            if(objResignation==null)
            {
                objResignation = new ResignationofDirector();
                objResignation.DirectorID = ID;
                objResignation.EntityID = EntityId;
            }
            return PartialView("_Resign", objResignation);
        }

        public ActionResult UpdateDirectorResignation(ResignationofDirector _objResignation)
        {
            _objResignation = objService.DirectorResignation(_objResignation);
            return PartialView("_Resign", _objResignation);
        }

        public ActionResult DownloadResignForm(long DirectorID, long EntityID)
        {
            string generatectc = GenerateDirectorRegignationDocument(DirectorID, EntityID);
            return File(generatectc, "application/force-download", Path.GetFileName(generatectc));
        }
        public string GenerateDirectorRegignationDocument(long DirectorID, long EntityID)
        {
            try
            {
                int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string getFileFormate = objService.GetDirectorResignation(DirectorID, EntityID);
                string path = "~/Areas/BM_Management/Documents/" + customerId + "/MeetingForm/" + DirectorID + "/" + EntityID;
                string str = "Resignation Letter.docx";
                string fileName = Path.Combine(Server.MapPath(path), str);

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                using (Stream output = System.IO.File.OpenWrite(fileName))
                {
                    DocxFormatProvider provider = new DocxFormatProvider();
                    RadFlowDocument document = CreateAttendenceDocument(DirectorID, EntityID);

                    provider.Export(document, output);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public RadFlowDocument CreateAttendenceDocument(long DirectorID, long EntityID)
        {
            string getFileFormate = objService.GetDirectorResignation(DirectorID, EntityID);

            RadFlowDocument doc = new RadFlowDocument();
            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(doc);

            try
            {
                ThemeFontScheme fontScheme = new ThemeFontScheme(
               "Mine",
               "Bookman Old Style",   // Major 
               "Bookman Old Style");          // Minor 


                ThemeColorScheme colorScheme = new ThemeColorScheme(
                "Mine",
                Colors.Black,     // background 1 
                Colors.Blue,      // text 1 
                Colors.Brown,     // background 2 
                Colors.Cyan,      // text 2 
                Colors.Black,    //DarkGray,  // accent 1 
                Colors.Gray,      // accent 2 
                Colors.Green,     // accent 3 
                Colors.LightGray, // accent 4 
                Colors.Magenta,   // accent 5 
                Colors.Orange,    // accent 6 
                Colors.Purple,    // hyperlink 
                Colors.Red);      // followedHyperlink 

                DocumentTheme theme = new DocumentTheme("Mine", colorScheme, fontScheme);
                doc.Theme = theme;

                #region Document header

                //var section = editor.InsertSection();
                //var paragraph = section.Blocks.AddParagraph();
                //paragraph.TextAlignment = Alignment.Center;
                //editor.MoveToParagraphStart(paragraph);

                //editor.ParagraphFormatting.SpacingAfter.LocalValue = 0;
                //editor.ParagraphFormatting.TextAlignment.LocalValue = Alignment.Center;
                var insertOptions = new InsertDocumentOptions
                {
                    ConflictingStylesResolutionMode = ConflictingStylesResolutionMode.UseTargetStyle
                };
                HtmlFormatProvider htmlProvider = new HtmlFormatProvider();
                RadFlowDocument htmlDocument = htmlProvider.Import(getFileFormate);
                editor.InsertDocument(htmlDocument);
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return doc;
        }


        public ActionResult OpenDirectorRegignationview(long DirectorID, long EntityID)
        {
            string generatectc = objService.GetDirectorResignation(DirectorID, EntityID);
            return PartialView("_PreviewForm", generatectc);
        }

        public ActionResult Checkchngedbydirectorornot()
        {
            bool IschangedbyDirector = objService.checkchangebydirectorornot(userID);
            return Json(IschangedbyDirector,JsonRequestBehavior.AllowGet);
        }

        public ActionResult canceleditChangebyDir(long DirectorId)
        {
            bool canceleditData = false;
            if (DirectorId > 0)
            {
                canceleditData = objprofile.Canceleditchange(DirectorId, userID);
            }
            return Json(canceleditData, JsonRequestBehavior.AllowGet);
        }
    }
}