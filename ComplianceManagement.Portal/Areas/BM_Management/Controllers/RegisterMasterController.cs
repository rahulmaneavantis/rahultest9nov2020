﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class RegisterMasterController : Controller
    {
        IRegister obj;
        // GET: BM_Management/RegisterMaster

        public RegisterMasterController(IRegister obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult PopForAddEditRegister(int Id)
        {
            VMRegisters _objregister = new VMRegisters();

            if (Id > 0)
            {
                _objregister = obj.getRegisterbyId(Id);
            }
            return PartialView("_AddEditRegister", _objregister);
        }

        public ActionResult Add_EditRegister(VMRegisters _objregister)
        {
            if (ModelState.IsValid)
            {
                if (_objregister.Id == 0)
                {
                    _objregister = obj.AddRegister(_objregister);
                }
                else
                {
                    _objregister = obj.UpdateRegister(_objregister);
                }

            }
            return PartialView("_AddEditRegister", _objregister);

        }
    }
}