﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Reports;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class CompanyProfileController : Controller
    {
        IReportPolices obj;
        IDirectorMaster objService;
        public CompanyProfileController(IReportPolices obj, IDirectorMaster objService)
        {
            this.obj = obj;
            this.objService = objService;
        }
        // GET: BM_Management/CompanyProfile
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompanyProfile()
        {
            return View();
        }

        public ActionResult DownloadRecords(long Id)
        {
            if (Id > 0)
            {
                bool downloadfile = obj.Downloadrecords(Id);
            }
            return Json("");
        }

        public ActionResult ViewRecords(long Id)
        {
            string view = obj.Viewdocument(Id);
            return Json(view);
        }

        public ActionResult DirectorsDetails(long Id)
        {
            int customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            Director_MasterVM obj = new Director_MasterVM();

            obj = objService.GetManegmentDetails(customerId, Id);

            return PartialView("_DirectorMaster", obj);
        }
        public ActionResult DirectorProfile()
        {
            return View();
        }
    }
}