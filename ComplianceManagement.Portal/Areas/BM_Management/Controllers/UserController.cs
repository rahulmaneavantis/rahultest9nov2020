﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class UserController : Controller
    {
        IUser objIUser;
        String key = "Authenticate" + AuthenticationHelper.UserID;
        List<VM_pageAuthentication> authRecord = new List<VM_pageAuthentication>();
        public UserController(IUser objUser)
        {
            objIUser = objUser;
        }

        // GET: BM_Management/User
        public ActionResult UserList()
        {
            var customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            var model = objIUser.GetProductByCustomerId(customerId);
            return View(model);
        }

        public ActionResult User()
        {
            UserVM objUserVM = new UserVM();
            authRecord = HttpContext.Cache.Get(key + "authrecord") as List<VM_pageAuthentication>;

            if (authRecord != null)
            {
                objUserVM.CanAdd = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.Addview).FirstOrDefault();
                objUserVM.CanEdit = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.Editview).FirstOrDefault();
                objUserVM.CanDelete = (from x in authRecord where x.UserId == AuthenticationHelper.UserID && x.PageName.Equals("User") select x.DeleteView).FirstOrDefault();
            }
            return View(objUserVM);
        }

        public ActionResult UserCustomerMapping()
        {
            return View();
        }
    }
}