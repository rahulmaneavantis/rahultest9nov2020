﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas.BM_Management.Controllers
{
    public class MeetingDashbordController : Controller
    {
        // GET: BM_Management/MeetingDashbord

        IMeetingModule obj;

        public MeetingDashbordController(IMeetingModule obj)
        {
            this.obj = obj;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MeetingDashboard()
        {
            return View();
        }

        public ActionResult NewMeetingModule()
        {
            return View();
        }

        public ActionResult EditNewMeeting(int Id)
        {
            VM_NewMeetingModule vmnewMeeting = new VM_NewMeetingModule();
            if(Id>0)
            {
                vmnewMeeting = obj.getMettingbyId(Id);
            }
            return PartialView("_AddEditfornewMeeting", vmnewMeeting);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddEditMeeting(VM_NewMeetingModule _objvmmeeting)
        {
            if(ModelState.IsValid)
            {
                _objvmmeeting.customerId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (_objvmmeeting.Id==0)
                {
                    
                    _objvmmeeting = obj.AddMeeting(_objvmmeeting);
                }
               else
                {
                    _objvmmeeting = obj.UpdateMeeting(_objvmmeeting);
                }
            }
            return PartialView("_AddEditfornewMeeting", _objvmmeeting);
        }
    }
}