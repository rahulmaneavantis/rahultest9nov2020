﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Text;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial
{
    public partial class AddEditEntityWiseConfigurations : System.Web.UI.Page
    {
        protected static int selectedCustomerID;
        protected static int loggedInUserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            loggedInUserID = AuthenticationHelper.UserID;
            if (!IsPostBack)
            {                
                if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                {
                    selectedCustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                    hdnCustID.Value = selectedCustomerID.ToString();
                }
            }
        }        

    }
}