﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterDocViewer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Areas_Secretarial.RegisterDocViewer" %>

<!DOCTYPE html>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <table style="width:100%">
            <tr>
                <%--<td style="width:20%; vertical-align:top;">
                    <asp:Label ID="lblFileName" runat="server" Text="" ></asp:Label>
                </td>--%>
                <td style="width:100%">
                    <div>
                        <GleamTech:DocumentViewer runat="server" Width="100%" ID="doccontrol" FullViewport="false" searchControl="false" SidePaneVisible="false" Height="560px"
                            DownloadAsPdfEnabled="false" DisableHeaderIncludes="false" ClientLoad="documentViewerLoad" />
                    </div>   
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    function documentViewerLoad(sender, e) { 
        var documentViewer = sender; //sender parameter will be the DocumentViewer instance

        documentViewer.setZoomLevel(1); // Set zoom to 100%
    }
</script>

