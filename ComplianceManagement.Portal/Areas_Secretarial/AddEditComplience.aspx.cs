﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AreaSecreterial
{
    public partial class AddEditComplience : System.Web.UI.Page
    {
        int ComplianceId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";

                BindCategories();
                BindTypes();
                BindActs();
                BindDueDates();
                BindFrequencies();
                BindDueDays();
                BindFilterFrequencies();
                BindNatureOfCompliance();
                //BindCompliancesNew();
                BindEvents();
                BindIndustry();
                BindEntityType();
                //BindLegalEntityType();
                txtIndustry.Attributes.Add("readonly", "readonly");
                txtEntityType.Attributes.Add("readonly", "readonly");
                string Id = Request.QueryString["ID"];

                if (Id != null)
                {
                    ComplianceId = Convert.ToInt32(Id);
                    BindCompliencesonEdit(ComplianceId);
                    ViewState["Mode"] = 1;
                }
                else
                {
                    ViewState["Mode"] = 0;
                }


                //grdCompliances.Attributes.Add("style", "word-break:break-all;word-wrap:break-word");
            }
        }

        private void BindCompliencesonEdit(int complianceId)
        {
            try
            {
                //if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                //{
                divSampleForm.Visible = false;
                grdSampleForm.DataSource = null;
                grdSampleForm.DataBind();

                hdnFile.Value = "";

                lblErrorMassage.Text = "";
                int complianceID = complianceId;
                ViewState["GComplianceID"] = null;
                //ViewState["ComplianceID"] = complianceID;
             
                var compliance = Business.ComplianceManagement.GetByID(complianceID);
                var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                hdnFile.Value = Convert.ToString(complianceID);

                var vGetIndustryMappedIDs = Business.ComplianceManagement.GetIndustryMappedID(complianceID);

                //foreach (RepeaterItem aItem in rptIndustry.Items)
                //{
                //    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                //    chkIndustry.Checked = false;
                //    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                //    for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                //    {
                //        if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                //        {
                //            chkIndustry.Checked = true;
                //        }

                //    }
                //    if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                //    {
                //        IndustrySelectAll.Checked = true;

                //    }
                //    else
                //    {
                //        IndustrySelectAll.Checked = false;

                //    }
                //}

                //var vGetLegalEntityTypeMappedID = Business.ComplianceManagement.GetLegalEntityTypeMappedID(complianceID);
                //foreach (RepeaterItem aItem in rptEntityType.Items)
                //{
                //    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                //    chkEntityType.Checked = false;
                    //CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                    //for (int i = 0; i <= vGetLegalEntityTypeMappedID.Count - 1; i++)
                    //{
                    //    if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetLegalEntityTypeMappedID[i].ToString())
                    //    {
                    //        chkEntityType.Checked = true;
                    //    }

                    //}
                    //if ((rptEntityType.Items.Count) == (vGetLegalEntityTypeMappedID.Count))
                    //{
                    //    EntityTypeSelectAll.Checked = true;

                    //}
                    //else
                    //{
                    //    EntityTypeSelectAll.Checked = false;

                    //}


                //}

                //txtIndustry.Text = "< Select >";
                //txtEntityType.Text = "< Select >";

                ViewState["Mode"] = 1;
                ViewState["ComplianceID"] = complianceID;
                ViewState["GComplianceID"] = complianceID;

                txtShortForm.Text = compliance.ShortForm;
                ddlAct.SelectedValue = compliance.ActID.ToString();
                txtShortDescription.Text = compliance.ShortDescription;
                tbxDescription.Text = compliance.Description;
                tbxSections.Text = compliance.Sections;
                chkDocument.Checked = compliance.UploadDocument ?? false;
                ddlComplianceType.SelectedValue = compliance.ComplianceType.ToString();
                if (compliance.StartDate != null)
                {
                    txtStartDate.Text = Convert.ToString(compliance.StartDate);
                }
                if (compliance.ComplianceType == 1)
                {
                    divActionable.Visible = true;
                }
                else
                {
                    divActionable.Visible = false;
                }
                rdoComplianceVisible.Checked = compliance.ComplinceVisible ?? false;
                if (rdoComplianceVisible.Checked == false)
                {
                    rdoComplianceVisible.Checked = false;
                    rdoNotComplianceVisible.Checked = true;

                    divChecklist.Visible = false;
                    divOneTime.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                }
                else
                {
                    rdoComplianceVisible.Checked = true;
                    rdoNotComplianceVisible.Checked = false;
                }
                ddlComplianceType_SelectedIndexChanged(null, null);

                var compliancetag = Business.ComplianceManagement.GetComplianceDatatagMapping(complianceID);
                List<string> CIntertagfixvalue = new List<string>();

                foreach (var tag in compliancetag)
                {
                    string tag1 = tag.FileTag;
                    CIntertagfixvalue.Add(tag1);
                }

                string nameOfString = (string.Join(",", CIntertagfixvalue.Select(x => x.ToString()).ToArray()));
                txtCompliancetag.Text = nameOfString;
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                if (complianceForm.Count > 0)
                {
                    divSampleForm.Visible = true;
                    grdSampleForm.DataSource = complianceForm;
                    grdSampleForm.DataBind();
                }
                else
                {
                    divSampleForm.Visible = false;
                }
                tbxRequiredForms.Text = compliance.RequiredForms;
                txtSampleFormLink.Text = compliance.SampleFormLink;
                ddlRiskType.SelectedValue = compliance.RiskType.ToString();
                if (Convert.ToString(compliance.PenaltyDescription) != null)
                {
                    txtPenaltyDescription.Text = compliance.PenaltyDescription.ToString();
                }
                if (Convert.ToString(compliance.ReferenceMaterialText) != null)
                {
                    txtReferenceMaterial.Text = compliance.ReferenceMaterialText.ToString();
                }


                ddlComplianceType_SelectedIndexChanged(null, null);

                //if (complianceForm != null)
                //{
                //    lblSampleForm.Text = complianceForm.Name;
                //}
                //else
                //{
                //    lblSampleForm.Text = "< Not selected >";
                //}

                if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2)//function based or time based
                {
                    ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();

                    ddlNatureOfCompliance_SelectedIndexChanged(null, null);

                    if (compliance.ComplianceSubTypeID == null)
                    {
                        ddlComplianceSubType.SelectedValue = "-1";
                    }
                    else
                    {
                        ddlComplianceSubType.SelectedValue = compliance.ComplianceSubTypeID.ToString();

                    }
                    if (compliance.EventID != null)
                    {
                        chkEventBased.Checked = true;
                        divEvent.Visible = true;
                        divComplianceDueDays.Visible = true;
                        divNonEvents.Visible = false;
                        if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                        {
                            ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                            ddlEvents_SelectedIndexChanged(null, null);

                            if (compliance.SubEventID != null)
                            {
                                divSubEventmode.Visible = true;
                                SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                tbxSubEvent.Text = subevent.Name;
                                SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                            }
                        }


                        if (compliance.DueDate != null)
                        {
                            txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                        }
                        if (compliance.EventComplianceType != null)
                        {
                            rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                        }
                    }
                    else
                    {
                        chkEventBased.Checked = false;
                        divEvent.Visible = false;
                        divComplianceDueDays.Visible = false;
                        divNonEvents.Visible = true;
                        ddlEvents.SelectedIndex = 0;
                        txtEventDueDate.Text = string.Empty;
                        ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();

                        if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                        {
                            vivDueDate.Visible = false;
                        }
                        else
                        {
                            vivDueDate.Visible = true;
                        }

                        vivWeekDueDays.Visible = false;
                        if (ddlFrequency.SelectedValue == "8")
                        {
                            ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                            vivWeekDueDays.Visible = true;
                        }
                        else
                        {
                            vivWeekDueDays.Visible = false;
                        }

                        if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                        {
                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                        }
                        else
                        {
                            if (compliance.DueDate != null && compliance.DueDate != -1)
                            {
                                if (compliance.DueDate <= 31)
                                {
                                    ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                }
                                else
                                {
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                            }

                        }
                        if (compliance.ComplianceType == 2)
                        {
                            if (compliance.SubComplianceType == 0)
                            {
                                divNonEvents.Visible = false;
                                divFrequency.Visible = false;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = false;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }
                            else if (compliance.SubComplianceType == 1)
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = false;
                                rgexEventDueDate.Enabled = false;
                            }
                            else
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }

                            rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                        }

                    }
                    ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                    if (ddlNonComplianceType.SelectedValue == "-1")
                    {
                        divMonetary.Visible = false;
                        divNonMonetary.Visible = false;
                    }

                    if (compliance.NonComplianceType != null)
                    {
                        ddlNonComplianceType_SelectedIndexChanged(null, null);

                        tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                        tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                        if (compliance.VariableAmountPerDay.HasValue)
                        {
                            ddlPerDayMonth.SelectedValue = "0";
                            tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                        }
                        else
                        {
                            ddlPerDayMonth.SelectedValue = "1";
                            tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                        }

                        tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                        tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                        tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;

                        chbImprisonment.Checked = compliance.Imprisonment ?? false;
                        if (chbImprisonment.Checked)
                        {
                            divImprisonmentDetails.Visible = true;
                        }
                        else
                        {
                            divImprisonmentDetails.Visible = false;
                        }
                        tbxDesignation.Text = compliance.Designation;
                        tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                        tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;
                    }

                    //InitializeDateFilter(compliance.DueDate.HasValue > 0 ? DateTime.ParseExact(compliance.DueDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now.Date);
                    tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;
                }
                else// checklist
                {
                    ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                    //------checklist------
                    if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                    {
                        divOneTime.Visible = false;
                        if (compliance.EventID != null)
                        {
                            chkEventBased.Checked = true;
                            divEvent.Visible = true;
                            divComplianceDueDays.Visible = true;
                            divNonEvents.Visible = false;
                            if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                            {
                                ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                ddlEvents_SelectedIndexChanged(null, null);

                                if (compliance.SubEventID != null)
                                {
                                    divSubEventmode.Visible = true;
                                    SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                    tbxSubEvent.Text = subevent.Name;
                                    SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                }
                            }

                            if (compliance.DueDate != null)
                            {
                                txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                            }
                            if (compliance.EventComplianceType != null)
                            {
                                rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                            }

                        }
                        else
                        {
                            if (compliance.ComplinceVisible == true)
                            {
                                divFunctionBased.Visible = true;
                                divFrequency.Visible = true;
                            }
                            else
                            {
                                divFunctionBased.Visible = false;
                                divFrequency.Visible = false;
                            }
                            divNonComplianceType.Visible = false;

                            chkEventBased.Checked = false;
                            divEvent.Visible = false;
                            divComplianceDueDays.Visible = false;
                            divNonEvents.Visible = true;
                            ddlEvents.SelectedIndex = 0;
                            txtEventDueDate.Text = string.Empty;
                            ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();

                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                            {
                                vivDueDate.Visible = false;
                            }
                            else
                            {
                                vivDueDate.Visible = true;
                            }

                            if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                            {
                                ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                            }
                            else
                            {
                                if (compliance.DueDate != null && compliance.DueDate != -1)
                                {
                                    if (compliance.DueDate <= 31)
                                    {
                                        ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                    }
                                    else
                                    {
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }
                                }
                            }


                            //if (compliance.ComplianceType == 2)
                            if (compliance.CheckListTypeID == 2)
                            {
                                divTimebasedTypes.Visible = true;
                                if (compliance.SubComplianceType == 0)//fixed gap -- time based
                                {
                                    divNonEvents.Visible = false;
                                    divFunctionBased.Visible = false;
                                    divNonComplianceType.Visible = false;
                                    divFrequency.Visible = false;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = false;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                                else if (compliance.SubComplianceType == 1)
                                {
                                    divNonEvents.Visible = true;
                                    divFunctionBased.Visible = true;
                                    divFrequency.Visible = true;
                                    divNonComplianceType.Visible = false;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = false;
                                    rgexEventDueDate.Enabled = false;
                                }
                                else
                                {
                                    divNonEvents.Visible = true;
                                    divFrequency.Visible = true;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }

                                rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                            }
                        }

                    }
                    else//one time checklist
                    {
                        if (compliance.EventID != null)
                        {
                            chkEventBased.Checked = true;
                            divEvent.Visible = true;
                            divComplianceDueDays.Visible = true;
                            divNonEvents.Visible = false;
                            divEventComplianceType.Visible = true;
                            if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                            {
                                ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                ddlEvents_SelectedIndexChanged(null, null);

                                if (compliance.SubEventID != null)
                                {
                                    divSubEventmode.Visible = true;
                                    SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                    tbxSubEvent.Text = subevent.Name;
                                    SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                }
                            }

                            if (compliance.DueDate != null)
                            {
                                txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                            }
                            if (compliance.EventComplianceType != null)
                            {
                                rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                            }

                        }
                        else
                        {
                            chkEventBased.Checked = false;
                            divEvent.Visible = false;
                            divComplianceDueDays.Visible = false;
                            divNonEvents.Visible = true;
                            ddlEvents.SelectedIndex = 0;
                            txtEventDueDate.Text = string.Empty;

                            divTimebasedTypes.Visible = false;
                            divFunctionBased.Visible = false;

                            if (rdoNotComplianceVisible.Checked == true)
                            {
                                divOneTime.Visible = false;
                                divChecklist.Visible = false;
                            }

                            //divOneTime.Visible = true;

                            tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                            divComplianceDueDays.Visible = false;
                            txtEventDueDate.Text = null;
                        }

                    }

                }

                if (Convert.ToString(compliance.ReminderType) == "")
                {
                    rbReminderType.SelectedValue = "0";
                    divForCustome.Visible = false;
                }
                else
                {
                    rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                    if (compliance.ReminderType == 0)
                    {
                        divForCustome.Visible = false;
                    }
                    else
                    {
                        divForCustome.Visible = true;
                    }
                }
                //rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                //if (compliance.ReminderType == 0)
                //{
                //    divForCustome.Visible = false;
                //}
                //else
                //{
                //    divForCustome.Visible = true;
                //}

                txtReminderBefore.Text = Convert.ToString(compliance.ReminderBefore);
                txtReminderGap.Text = Convert.ToString(compliance.ReminderGap);
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker1", "initializeDatePicker1(null);", true);
                }

                upComplianceDetails.Update();
               // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                //BindComplianceParameters();

            }
            catch (Exception ex)
            {

            }
        }

        private void BindNatureOfCompliance()
        {
            try
            {
                ddlNatureOfCompliance.DataSource = Business.ComplianceManagement.GetAllComplianceNature();
                ddlNatureOfCompliance.DataTextField = "Name";
                ddlNatureOfCompliance.DataValueField = "ID";
                ddlNatureOfCompliance.DataBind();

                ddlNatureOfCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        public string ComplianceActiveOrInActive(int ComplianceID)
        {
            try
            {
                string result = "Active";
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).FirstOrDefault();

                if (data.Status == "D" || data.Status == null)
                {
                    result = "Active";
                }
                else
                {
                    result = "DeActive";
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";

            }
            return "DeActive";
        }

        public Boolean ButtonDisplayComplianceActiveOrInActive(int ComplianceID)
        {
            try
            {
                Boolean result = true;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).FirstOrDefault();

                if (data.Status == "D" || data.Status == null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText= "Server Error Occured. Please try again.";

            }
            return true;
        }
        protected void chbImprisonment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                divImprisonmentDetails.Visible = ((CheckBox)sender).Checked;
                if (!divImprisonmentDetails.Visible)
                {
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                diverror.Visible = true;
                divsuccess.Visible = false;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                divSampleForm.Visible = false;
                saveopo.Value = "false";
                //GComplianceID = 0;
                //ViewState["ComplianceParameters"] = new List<ComplianceParameter>();
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;
                rdoComplianceVisible.Checked = true;

                //ddlComplianceSubType.DataSource = null;
                //ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Clear();

                tbxRequiredForms.Text = tbxDescription.Text = tbxNonComplianceEffects.Text = tbxSections.Text = string.Empty;
                ddlNatureOfCompliance.SelectedIndex = ddlComplianceType.SelectedIndex = ddlAct.SelectedIndex = ddlFrequency.SelectedIndex = ddlDueDate.SelectedIndex = ddlNonComplianceType.SelectedIndex = ddlRiskType.SelectedIndex = ddlEvents.SelectedIndex;
                vivWeekDueDays.Visible = false;
                ddlWeekDueDay.SelectedValue = "-1";

                divComplianceDueDays.Visible = false;
                divEvent.Visible = false;
                tbxSubEvent.Text = "< Select Sub Event >";
                tvSubEvent.Nodes.Clear();
                txtEventDueDate.Text = string.Empty;
                chkDocument.Checked = false;
                //lblSampleForm.Text = "< Not selected >";
                ddlNonComplianceType_SelectedIndexChanged(null, null);

                ddlComplianceType_SelectedIndexChanged(null, null);

                txtReferenceMaterial.Text = string.Empty;
                rbReminderType.SelectedValue = "0";
                txtReminderBefore.Text = string.Empty;
                txtReminderGap.Text = string.Empty;
                txtPenaltyDescription.Text = string.Empty;
                txtShortDescription.Text = string.Empty;
                txtSampleFormLink.Text = string.Empty;
                txtCompliancetag.Text = string.Empty;

                txtIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
                txtEntityType.Text = "< Select >";

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    chkEntityType.Checked = false;
                    CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                    EntityTypeSelectAll.Checked = false;
                }

                tbxOnetimeduedate.Text = string.Empty;
                divForCustome.Visible = false;
                divChecklist.Visible = false;
                divOneTime.Visible = false;
                chkEventBased.Checked = false;
                upComplianceDetails.Update();
                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = true;

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                diverror.Visible = true;
                divsuccess.Visible = false;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }



        //private void OpenScheduleInformation(Business.Data.Compliance compliance)
        //{
        //    try
        //    {
        //        saveopo.Value = "true";
        //        ViewState["ComplianceID"] = compliance.ID;
        //        ViewState["Frequency"] = compliance.Frequency.Value;
        //        if (compliance.DueDate != null)
        //        {
        //            ViewState["Day"] = compliance.DueDate.Value;
        //        }

        //        if ((compliance.Frequency.Value == 0 || compliance.Frequency.Value == 1))
        //            divStartMonth.Visible = false;
        //        else
        //            divStartMonth.Visible = true;

        //        var scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
        //        if (scheduleList.Count == 0)
        //        {
        //            Business.ComplianceManagement.GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType);
        //            scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
        //        }

        //        int step = 0;
        //        if (compliance.Frequency.Value == 0)
        //            step = 0;
        //        else if (compliance.Frequency.Value == 1)
        //            step = 2;
        //        else if (compliance.Frequency.Value == 2)
        //            step = 5;
        //        else if (compliance.Frequency.Value == 4)
        //            step = 3;
        //        else
        //            step = 11;

        //        var dataSource = scheduleList.Select(entry => new
        //        {
        //            ID = entry.ID,
        //            ForMonth = entry.ForMonth,
        //            ForMonthName = compliance.Frequency.Value == 0 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
        //            SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
        //            SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
        //        }).ToList();

        //        if (divStartMonth.Visible)
        //        {
        //            //switch ((Frequency)compliance.Frequency.Value)
        //            //{
        //            //    case Frequency.Quarterly:
        //            //        ddlStartMonth.DataSource = Enumerations.GetAll<Month>().Take(3);
        //            //        ddlStartMonth.DataBind();
        //            //        break;
        //            //    case Frequency.HalfYearly:
        //            //        ddlStartMonth.DataSource = Enumerations.GetAll<Month>().Take(6);
        //            //        ddlStartMonth.DataBind();
        //            //        break;
        //            //    case Frequency.Annual:
        //            //        ddlStartMonth.DataSource = Enumerations.GetAll<Month>();
        //            //        ddlStartMonth.DataBind();
        //            //        break;
        //            //}


        //            //ddlStartMonth.DataSource = Enumerations.GetAll<Month>().Where(entry => entry.ID == 1 && entry.ID == 4).ToList();
        //            //ddlStartMonth.DataBind();

        //            //ddlStartMonth.SelectedValue = dataSource.First().SpecialMonth.ToString();
        //            //if (compliance.SubComplianceType == 1)
        //            //{
        //            //    if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
        //            //        ddlStartMonth.SelectedValue = Convert.ToString(1);
        //            //    else
        //            //        ddlStartMonth.SelectedValue = Convert.ToString(4);
        //            //}
        //            //else
        //            //{
        //            //    ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
        //            //}
        //        }

        //        //repComplianceSchedule.DataSource = dataSource;
        //        //repComplianceSchedule.DataBind();

        //        //upSchedulerRepeter.Update();
        //        //upComplianceScheduleDialog.Update();
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        saveopo.Value = "true";
        //        var dataSource = new List<object>();
        //        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
        //        {
        //            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

        //            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
        //            HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
        //            DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
        //            DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


        //            dataSource.Add(new
        //            {
        //                ID = hdnID.Value,
        //                ForMonth = Convert.ToByte(hdnForMonth.Value),
        //                ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
        //                SpecialDay = ddlDays.SelectedValue,
        //                SpecialMonth = ddlMonths.SelectedValue
        //            });


        //            Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
        //            int totalDays = 0;
        //            switch (month)
        //            {
        //                case Month.February:
        //                    totalDays = 28;
        //                    break;
        //                case Month.January:
        //                case Month.March:
        //                case Month.May:
        //                case Month.July:
        //                case Month.August:
        //                case Month.October:
        //                case Month.December:
        //                    totalDays = 31;
        //                    break;
        //                case Month.April:
        //                case Month.June:
        //                case Month.September:
        //                case Month.November:
        //                    totalDays = 30;
        //                    break;
        //            }

        //            var daysdataSource = new List<object>();

        //            for (int j = 1; j <= totalDays; j++)
        //            {
        //                daysdataSource.Add(new { ID = i, Name = i.ToString() });
        //            }

        //            ddlDays.DataSource = daysdataSource;
        //            ddlDays.DataBind();
        //            //upSchedulerRepeter.Update();
        //        }

        //        //repComplianceSchedule.DataSource = dataSource;
        //        //repComplianceSchedule.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Frequency frequency = (Frequency)Convert.ToByte(ViewState["Frequency"]);
        //        byte day = Convert.ToByte(ViewState["Day"]);
        //        byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
        //        byte step = 1;
        //        switch (frequency)
        //        {
        //            case Frequency.Quarterly:
        //                step = 3;
        //                break;
        //            case Frequency.FourMonthly:
        //                step = 4;
        //                break;
        //            case Frequency.HalfYearly:
        //                step = 6;
        //                break;
        //            case Frequency.Annual:
        //                step = 12;
        //                break;
        //            case Frequency.TwoYearly:
        //                step = 12;
        //                break;
        //            case Frequency.SevenYearly:
        //                step = 12;
        //                break;
        //        }

        //        var dataSource = new List<object>();

        //        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
        //        {
        //            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

        //            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
        //            HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
        //            DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
        //            DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

        //            int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
        //            int specialMonth;
        //            if (step == 12)
        //            {
        //                specialMonth = startMonth + (step * i);
        //            }
        //            else
        //            {
        //                if (month < 10)
        //                {
        //                    if (month > 12)
        //                    {
        //                        specialMonth = 1;
        //                    }
        //                    else
        //                    {
        //                        if (frequency == Frequency.FourMonthly)
        //                            specialMonth = startMonth + (step * i) + 4;
        //                        else
        //                            specialMonth = startMonth + (step * i) + 6;

        //                    }
        //                }
        //                else
        //                {
        //                    if (frequency == Frequency.FourMonthly)
        //                        specialMonth = 4;
        //                    else
        //                        specialMonth = startMonth + (step * i) - 6;
        //                }
        //            }

        //            dataSource.Add(new
        //            {
        //                ID = hdnID.Value,
        //                ForMonth = month,
        //                ForMonthName = frequency == Frequency.Monthly ? ((Month)month).ToString() : ((Month)month).ToString() + " - " + ((Month)((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
        //                SpecialDay = day,
        //                SpecialMonth = specialMonth
        //            });
        //        }

        //        //repComplianceSchedule.DataSource = dataSource;
        //        //repComplianceSchedule.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");


                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                diverror.Visible = true;
                divsuccess.Visible = false;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    divSampleForm.Visible = false;
                    grdSampleForm.DataSource = null;
                    grdSampleForm.DataBind();

                    hdnFile.Value = "";

                    lblErrorMassage.Text = "";
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    ViewState["GComplianceID"] = null;
                    ViewState["GComplianceID"] = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    hdnFile.Value = Convert.ToString(complianceID);

                    var vGetIndustryMappedIDs = Business.ComplianceManagement.GetIndustryMappedID(complianceID);

                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }

                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                        {
                            IndustrySelectAll.Checked = true;

                        }
                        else
                        {
                            IndustrySelectAll.Checked = false;

                        }
                    }

                    var vGetLegalEntityTypeMappedID = Business.ComplianceManagement.GetLegalEntityTypeMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        chkEntityType.Checked = false;
                        CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                        for (int i = 0; i <= vGetLegalEntityTypeMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetLegalEntityTypeMappedID[i].ToString())
                            {
                                chkEntityType.Checked = true;
                            }

                        }
                        if ((rptEntityType.Items.Count) == (vGetLegalEntityTypeMappedID.Count))
                        {
                            EntityTypeSelectAll.Checked = true;

                        }
                        else
                        {
                            EntityTypeSelectAll.Checked = false;

                        }


                    }

                    txtIndustry.Text = "< Select >";
                    txtEntityType.Text = "< Select >";

                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;


                    ddlAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescription.Text = compliance.ShortDescription;
                    tbxDescription.Text = compliance.Description;
                    tbxSections.Text = compliance.Sections;
                    chkDocument.Checked = compliance.UploadDocument ?? false;
                    ddlComplianceType.SelectedValue = compliance.ComplianceType.ToString();

                    if (compliance.ComplianceType == 1)
                    {
                        divActionable.Visible = true;
                    }
                    else
                    {
                        divActionable.Visible = false;
                    }
                    rdoComplianceVisible.Checked = compliance.ComplinceVisible ?? false;
                    if (rdoComplianceVisible.Checked == false)
                    {
                        rdoComplianceVisible.Checked = false;
                        rdoNotComplianceVisible.Checked = true;

                        divChecklist.Visible = false;
                        divOneTime.Visible = false;
                        divFrequency.Visible = false;
                        vivDueDate.Visible = false;
                    }
                    else
                    {
                        rdoComplianceVisible.Checked = true;
                        rdoNotComplianceVisible.Checked = false;
                    }
                    ddlComplianceType_SelectedIndexChanged(null, null);

                    var compliancetag = Business.ComplianceManagement.GetComplianceDatatagMapping(complianceID);
                    List<string> CIntertagfixvalue = new List<string>();

                    foreach (var tag in compliancetag)
                    {
                        string tag1 = tag.FileTag;
                        CIntertagfixvalue.Add(tag1);
                    }

                    string nameOfString = (string.Join(",", CIntertagfixvalue.Select(x => x.ToString()).ToArray()));
                    txtCompliancetag.Text = nameOfString;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                    if (complianceForm.Count > 0)
                    {
                        divSampleForm.Visible = true;
                        grdSampleForm.DataSource = complianceForm;
                        grdSampleForm.DataBind();
                    }
                    else
                    {
                        divSampleForm.Visible = false;
                    }
                    tbxRequiredForms.Text = compliance.RequiredForms;
                    txtSampleFormLink.Text = compliance.SampleFormLink;
                    ddlRiskType.SelectedValue = compliance.RiskType.ToString();
                    if (Convert.ToString(compliance.PenaltyDescription) != null)
                    {
                        txtPenaltyDescription.Text = compliance.PenaltyDescription.ToString();
                    }
                    if (Convert.ToString(compliance.ReferenceMaterialText) != null)
                    {
                        txtReferenceMaterial.Text = compliance.ReferenceMaterialText.ToString();
                    }


                    ddlComplianceType_SelectedIndexChanged(null, null);

                    //if (complianceForm != null)
                    //{
                    //    lblSampleForm.Text = complianceForm.Name;
                    //}
                    //else
                    //{
                    //    lblSampleForm.Text = "< Not selected >";
                    //}

                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2)//function based or time based
                    {
                        ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();

                        ddlNatureOfCompliance_SelectedIndexChanged(null, null);

                        if (compliance.ComplianceSubTypeID == null)
                        {
                            ddlComplianceSubType.SelectedValue = "-1";
                        }
                        else
                        {
                            ddlComplianceSubType.SelectedValue = compliance.ComplianceSubTypeID.ToString();

                        }
                        if (compliance.EventID != null)
                        {
                            chkEventBased.Checked = true;
                            divEvent.Visible = true;
                            divComplianceDueDays.Visible = true;
                            divNonEvents.Visible = false;
                            if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                            {
                                ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                ddlEvents_SelectedIndexChanged(null, null);

                                if (compliance.SubEventID != null)
                                {
                                    divSubEventmode.Visible = true;
                                    SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                    tbxSubEvent.Text = subevent.Name;
                                    SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                }
                            }


                            if (compliance.DueDate != null)
                            {
                                txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                            }
                            if (compliance.EventComplianceType != null)
                            {
                                rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                            }
                        }
                        else
                        {
                            chkEventBased.Checked = false;
                            divEvent.Visible = false;
                            divComplianceDueDays.Visible = false;
                            divNonEvents.Visible = true;
                            ddlEvents.SelectedIndex = 0;
                            txtEventDueDate.Text = string.Empty;
                            ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();

                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                            {
                                vivDueDate.Visible = false;
                            }
                            else
                            {
                                vivDueDate.Visible = true;
                            }

                            vivWeekDueDays.Visible = false;
                            if (ddlFrequency.SelectedValue == "8")
                            {
                                ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                vivWeekDueDays.Visible = true;
                            }
                            else
                            {
                                vivWeekDueDays.Visible = false;
                            }

                            if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                            {
                                ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                            }
                            else
                            {
                                if (compliance.DueDate != null && compliance.DueDate != -1)
                                {
                                    if (compliance.DueDate <= 31)
                                    {
                                        ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                    }
                                    else
                                    {
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }
                                }

                            }
                            if (compliance.ComplianceType == 2)
                            {
                                if (compliance.SubComplianceType == 0)
                                {
                                    divNonEvents.Visible = false;
                                    divFrequency.Visible = false;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = false;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                                else if (compliance.SubComplianceType == 1)
                                {
                                    divNonEvents.Visible = true;
                                    divFrequency.Visible = true;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = false;
                                    rgexEventDueDate.Enabled = false;
                                }
                                else
                                {
                                    divNonEvents.Visible = true;
                                    divFrequency.Visible = true;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }

                                rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                            }

                        }
                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }

                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);

                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }

                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;
                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }
                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;
                        }

                        //InitializeDateFilter(compliance.DueDate.HasValue > 0 ? DateTime.ParseExact(compliance.DueDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now.Date);
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;
                    }
                    else// checklist
                    {
                        ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                        //------checklist------
                        if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                        {
                            divOneTime.Visible = false;
                            if (compliance.EventID != null)
                            {
                                chkEventBased.Checked = true;
                                divEvent.Visible = true;
                                divComplianceDueDays.Visible = true;
                                divNonEvents.Visible = false;
                                if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                                {
                                    ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                    ddlEvents_SelectedIndexChanged(null, null);

                                    if (compliance.SubEventID != null)
                                    {
                                        divSubEventmode.Visible = true;
                                        SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                        tbxSubEvent.Text = subevent.Name;
                                        SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                    }
                                }

                                if (compliance.DueDate != null)
                                {
                                    txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                                }
                                if (compliance.EventComplianceType != null)
                                {
                                    rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                                }

                            }
                            else
                            {
                                if (compliance.ComplinceVisible == true)
                                {
                                    divFunctionBased.Visible = true;
                                    divFrequency.Visible = true;
                                }
                                else
                                {
                                    divFunctionBased.Visible = false;
                                    divFrequency.Visible = false;
                                }
                                divNonComplianceType.Visible = false;

                                chkEventBased.Checked = false;
                                divEvent.Visible = false;
                                divComplianceDueDays.Visible = false;
                                divNonEvents.Visible = true;
                                ddlEvents.SelectedIndex = 0;
                                txtEventDueDate.Text = string.Empty;
                                ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();

                                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                                {
                                    vivDueDate.Visible = false;
                                }
                                else
                                {
                                    vivDueDate.Visible = true;
                                }

                                if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                                {
                                    ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                }
                                else
                                {
                                    if (compliance.DueDate != null && compliance.DueDate != -1)
                                    {
                                        if (compliance.DueDate <= 31)
                                        {
                                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                        }
                                        else
                                        {
                                            txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                        }
                                    }
                                }


                                //if (compliance.ComplianceType == 2)
                                if (compliance.CheckListTypeID == 2)
                                {
                                    divTimebasedTypes.Visible = true;
                                    if (compliance.SubComplianceType == 0)//fixed gap -- time based
                                    {
                                        divNonEvents.Visible = false;
                                        divFunctionBased.Visible = false;
                                        divNonComplianceType.Visible = false;
                                        divFrequency.Visible = false;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = false;
                                        divComplianceDueDays.Visible = true;
                                        rgexEventDueDate.Enabled = true;
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }
                                    else if (compliance.SubComplianceType == 1)
                                    {
                                        divNonEvents.Visible = true;
                                        divFunctionBased.Visible = true;
                                        divFrequency.Visible = true;
                                        divNonComplianceType.Visible = false;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = true;
                                        divComplianceDueDays.Visible = false;
                                        rgexEventDueDate.Enabled = false;
                                    }
                                    else
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = true;
                                        divComplianceDueDays.Visible = true;
                                        rgexEventDueDate.Enabled = true;
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }

                                    rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                                }
                            }

                        }
                        else//one time checklist
                        {
                            if (compliance.EventID != null)
                            {
                                chkEventBased.Checked = true;
                                divEvent.Visible = true;
                                divComplianceDueDays.Visible = true;
                                divNonEvents.Visible = false;
                                divEventComplianceType.Visible = true;
                                if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                                {
                                    ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                    ddlEvents_SelectedIndexChanged(null, null);

                                    if (compliance.SubEventID != null)
                                    {
                                        divSubEventmode.Visible = true;
                                        SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                        tbxSubEvent.Text = subevent.Name;
                                        SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                    }
                                }

                                if (compliance.DueDate != null)
                                {
                                    txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                                }
                                if (compliance.EventComplianceType != null)
                                {
                                    rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                                }

                            }
                            else
                            {
                                chkEventBased.Checked = false;
                                divEvent.Visible = false;
                                divComplianceDueDays.Visible = false;
                                divNonEvents.Visible = true;
                                ddlEvents.SelectedIndex = 0;
                                txtEventDueDate.Text = string.Empty;

                                divTimebasedTypes.Visible = false;
                                divFunctionBased.Visible = false;

                                if (rdoNotComplianceVisible.Checked == true)
                                {
                                    divOneTime.Visible = false;
                                    divChecklist.Visible = false;
                                }

                                //divOneTime.Visible = true;

                                tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                                divComplianceDueDays.Visible = false;
                                txtEventDueDate.Text = null;
                            }

                        }

                    }

                    if (Convert.ToString(compliance.ReminderType) == "")
                    {
                        rbReminderType.SelectedValue = "0";
                        divForCustome.Visible = false;
                    }
                    else
                    {
                        rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                        if (compliance.ReminderType == 0)
                        {
                            divForCustome.Visible = false;
                        }
                        else
                        {
                            divForCustome.Visible = true;
                        }
                    }
                    //rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                    //if (compliance.ReminderType == 0)
                    //{
                    //    divForCustome.Visible = false;
                    //}
                    //else
                    //{
                    //    divForCustome.Visible = true;
                    //}

                    txtReminderBefore.Text = Convert.ToString(compliance.ReminderBefore);
                    txtReminderGap.Text = Convert.ToString(compliance.ReminderGap);

                    upComplianceDetails.Update();
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                    //BindComplianceParameters();
                }
                //else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                //{
                //    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                //    if (chkexistsComplianceschedule == false)
                //    {
                //        int complianceID = Convert.ToInt32(e.CommandArgument);
                //        Business.ComplianceManagement.Delete(complianceID);
                //        BindCompliancesNew();
                //    }
                //    else
                //    {
                //        //cvDuplicateEntry.IsValid = false;
                //        cvDuplicateEntry.ErrorMessage = "Compliance assigned,can not deleted.";
                //        upComplianceDetails.Update();
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                //    }
                //}
                //else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                //{
                //    int complianceID = Convert.ToInt32(e.CommandArgument);
                //    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                //    //OpenScheduleInformation(compliance);
                //}
                //else if (e.CommandName.Equals("STATUS"))
                //{


                //    lblErrorMassage.Text = "";
                //    //lblDeactivate.Text = "";
                //    string deactivateOn = "";
                //    //txtShortDescriptionStatus.Text = "";
                //    //tbxDescriptionStatus.Text = "";
                //    //tbxDescriptionStatus.Text = "";
                //    int complianceID = Convert.ToInt32(e.CommandArgument);
                //    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                //    var compliancedeactive = Business.ComplianceManagement.GetComplianceDeactivate(complianceID);

                //    ViewState["Mode"] = 1;
                //    ViewState["ComplianceID"] = complianceID;
                //    //BindStatusActs();
                //    //ddlStatusAct.SelectedValue = compliance.ActID.ToString();
                //    //txtShortDescriptionStatus.Text = compliance.ShortDescription;
                //    //tbxDescriptionStatus.Text = compliance.Description;
                //    //tbxSectionsStatus.Text = compliance.Sections;


                //    if (compliance.DeactivateOn != null)
                //    {
                //        deactivateOn = Convert.ToDateTime(compliance.DeactivateOn).ToString("dd-MM-yyyy");
                //    }

                //    //txtDeactivateDate.Text = deactivateOn;

                //    //txtDeactivateDesc.Text = compliance.DeactivateDesc;

                //    //if (compliancedeactive != null)
                //    //{
                //    //    lblDeactivate.Text = compliancedeactive.Name;
                //    //}
                //    //upComplianceStatusDetails.Update();
                //    DateTime date = DateTime.Now;
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceStatusDialog\").dialog('open')", true);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                diverror.Visible = true;
                divsuccess.Visible = false;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliancesNew();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFunctionBased.Visible = vaDueDate.Enabled = divComplianceSubType.Visible = divNatureOfCompliance.Visible = (ddlComplianceType.SelectedValue == "0" || ddlComplianceType.SelectedValue == "2");

                tbxOnetimeduedate.Text = "";
                txtEventDueDate.Text = "";
                ddlDueDate.SelectedValue = "1";
                ddlWeekDueDay.SelectedValue = "-1";
                ddlNatureOfCompliance.SelectedValue = "-1";

                //ddlComplianceSubType.DataSource = null;
                //ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Clear();

                ddlFrequency.SelectedValue = "-1";
                ddlNonComplianceType.SelectedValue = "-1";
                txtEventDueDate.Text = "";

                if (ddlComplianceType.SelectedValue == "0")//function based
                {
                    divActionable.Visible = false;
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                    {
                        vivDueDate.Visible = false;
                    }
                    else
                    {
                        vivDueDate.Visible = true;
                    }
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;
                }
                else if (ddlComplianceType.SelectedValue.Equals("2"))//timebased
                {
                    divActionable.Visible = false;
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = true;
                    divComplianceDueDays.Visible = true;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divNonComplianceType.Visible = true;
                    rbTimebasedTypes.SelectedValue = "0";

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;

                }
                else//checklist
                {
                    divActionable.Visible = true;

                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;


                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    //divChecklist.Visible = true;
                    ddlChklstType.SelectedValue = "0";

                    if (rdoComplianceVisible.Checked == true)
                    {
                        divOneTime.Visible = true;
                        divChecklist.Visible = true;
                    }
                    else
                    {
                        divOneTime.Visible = false;
                        divChecklist.Visible = false;
                    }

                    //divOneTime.Visible = true;

                    //rbReminderType.Items[0].Enabled = false;
                    //rbReminderType.SelectedValue = "1";
                    //divForCustome.Visible = true;
                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = false;
                }

                ddlNonComplianceType_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlChklstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbReminderType.SelectedValue = "0";
            rbReminderType.Enabled = false;
            divOneTime.Visible = true;
            if (ddlChklstType.SelectedValue == "0")//one time
            {

                divOneTime.Visible = true;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = false;
                divNonEvents.Visible = false;
                divNonComplianceType.Visible = false;


                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = false;

            }
            else if (ddlChklstType.SelectedValue.Equals("1"))//function based
            {

                divOneTime.Visible = false;
                divTimebasedTypes.Visible = false;

                divFunctionBased.Visible = true;
                divNonEvents.Visible = true;
                divFrequency.Visible = true;
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }
                divTimebasedTypes.Visible = false;
                divComplianceDueDays.Visible = false;
                divNonComplianceType.Visible = false;



            }
            else if (ddlChklstType.SelectedValue.Equals("2"))//time based
            {

                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;
            }
            else
            {
                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;

            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);


        }

        protected void rbTimebasedTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = false;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = true;

                    }


                }
                else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = false;
                    rgexEventDueDate.Enabled = false;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = true;
                        divFrequency.Visible = false;
                        cvfrequency.Enabled = false;
                    }
                }
                else
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlNonComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divMonetary.Visible = ddlNonComplianceType.SelectedValue == "0" || ddlNonComplianceType.SelectedValue == "2";
                divNonMonetary.Visible = ddlNonComplianceType.SelectedValue == "1" || ddlNonComplianceType.SelectedValue == "2";

                if (!divMonetary.Visible)
                {
                    tbxFixedMinimum.Text = tbxFixedMaximum.Text = tbxVariableAmountPerDay.Text = tbxVariableAmountPerDayMax.Text = tbxVariableAmountPercent.Text = tbxVariableAmountPercentMaximum.Text = string.Empty;
                }

                if (!divNonMonetary.Visible)
                {
                    chbImprisonment.Checked = false;
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText= "Server Error Occured. Please try again.";
            }
        }

        //protected void btnSaveSchedule_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
        //        {

        //            bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
        //            if (chkexistsComplianceschedule == true)
        //            {
        //                //cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
        //            }
        //            else
        //            {
        //                List<ComplianceSchedule> scheduleList = new List<ComplianceSchedule>();

        //                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
        //                {
        //                    //RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

        //                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
        //                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
        //                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
        //                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

        //                    scheduleList.Add(new ComplianceSchedule()
        //                    {
        //                        ID = Convert.ToInt32(hdnID.Value),
        //                        ComplianceID = Convert.ToInt64(ViewState["ComplianceID"]),
        //                        ForMonth = Convert.ToInt32(hdnForMonth.Value),
        //                        SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
        //                    });
        //                }
        //                Business.ComplianceManagement.UpdateComplianceUpdatedOn(Convert.ToInt64(ViewState["ComplianceID"]));
        //                Business.ComplianceManagement.UpdateScheduleInformation(scheduleList);
        //                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
        //                //upSchedulerRepeter.Update();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                 if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker1", "initializeDatePicker1(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetailsStatus_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);


                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                //for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                //{
                //    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;
                //    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");
                //    AsyncPostBackTrigger apbt = new AsyncPostBackTrigger();
                //    apbt.ControlID = ddlMonths.UniqueID;
                //    upComplianceScheduleDialog.Triggers.Add(apbt);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            //rfvFile.Enabled = chkDocument.Checked;
            if (!chkDocument.Checked)
            {
                //lblSampleForm.Text = "< Not selected >";
            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        //protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        //if (rdFunctionBased.Checked)
        //        //    CmType = 0;
        //        //if (rdChecklist.Checked)
        //        //    CmType = 1;

        //        var compliancesData = Business.ComplianceManagement.GetAll1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.RiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.RiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.RiskType == 2)
        //                risk = "Low";

        //            dataSource.Add(new
        //            {
        //                complianceInfo.ID,
        //                complianceInfo.ActName,
        //                complianceInfo.Sections,
        //                complianceInfo.Description,
        //                complianceInfo.ComplianceType,
        //                complianceInfo.EventID,
        //                //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
        //                complianceInfo.UploadDocument,
        //                complianceInfo.RequiredForms,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
        //                complianceInfo.NonComplianceEffects,
        //                Risk = risk,
        //                complianceInfo.ShortDescription,
        //                complianceInfo.SubComplianceType,
        //                complianceInfo.CheckListTypeID
        //                //Parameters = GetParameters(complianceInfo.Value)
        //            });
        //        }

        //        if (direction == SortDirection.Ascending)
        //        {
        //            dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //            direction = SortDirection.Descending;
        //            ViewState["SortOrder"] = "Asc";
        //            ViewState["SortExpression"] = e.SortExpression.ToString();
        //        }
        //        else
        //        {
        //            dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
        //            direction = SortDirection.Ascending;
        //            ViewState["SortOrder"] = "Desc";
        //            ViewState["SortExpression"] = e.SortExpression.ToString();
        //        }
        //        //foreach (DataControlField field in grdCompliances.Columns)
        //        //{
        //        //    if (field.SortExpression == e.SortExpression)
        //        //    {
        //        //        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
        //        //    }
        //        //}

        //        //grdCompliances.DataSource = dataSource;
        //        //grdCompliances.DataBind();


        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                //ddlFilterComplianceType.DataTextField = "Name";
                //ddlFilterComplianceType.DataValueField = "ID";

                //ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                //ddlFilterComplianceType.DataBind();

                //ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                //ddlComplinceCatagory.DataTextField = "Name";
                //ddlComplinceCatagory.DataValueField = "ID";

                //ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                //ddlComplinceCatagory.DataBind();

                //ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceSubType(int NatureOfComplianceID)
        {
            try
            {
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";

                ddlComplianceSubType.DataSource = ComplianceCategoryManagement.GetComplianceSubTypes(NatureOfComplianceID);
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("< Nature Of Compliance Sub Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }
        private void BindEvents()
        {
            try
            {
                ddlEvents.DataTextField = "Name";
                ddlEvents.DataValueField = "ID";

                ddlEvents.DataSource = EventManagement.GetAllEvents(-1, string.Empty);
                ddlEvents.DataBind();

                ddlEvents.Items.Insert(0, new ListItem("< Select Event>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        private void BindFrequencies()
        {
            try
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                //ddlFilterFrequencies.DataTextField = "Name";
                //ddlFilterFrequencies.DataValueField = "ID";


                ddlFrequency.DataSource = Enumerations.GetAll<Frequency>();
                ddlFrequency.DataBind();
                //ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                //ddlFilterFrequencies.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));
                //ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {

                //ddlFilterFrequencies.DataTextField = "Name";
                //ddlFilterFrequencies.DataValueField = "ID";

                //ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                //ddlFilterFrequencies.DataBind();

                //ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                //ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompliancesNew()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CmType = -1;
                //if (rdFunctionBased.Checked)
                //    CmType = 0;
                //if (rdChecklist.Checked)
                //    CmType = 1;

                var compliancesQuery = (from row in entities.Compliances
                                        join row1 in entities.Acts on row.ActID equals row1.ID
                                        where row.IsDeleted == false && row.EventFlag == null
                                        select new
                                        {
                                            row.ID
                                                ,
                                            row.ActID
                                                ,
                                            ActName = row1.Name
                                                ,
                                            row.Description
                                                ,
                                            row.Sections
                                                ,
                                            row.ComplianceType
                                                ,
                                            row.UploadDocument
                                                ,
                                            row.NatureOfCompliance
                                                ,
                                            row.RequiredForms
                                                ,
                                            row.Frequency
                                                ,
                                            row.DueDate
                                                ,
                                            row.RiskType
                                                ,
                                            row.NonComplianceType
                                                ,
                                            row.NonComplianceEffects
                                                ,
                                            row.ShortDescription
                                                ,
                                            row.CreatedOn
                                                ,
                                            row1.ComplianceCategoryId
                                                ,
                                            row1.ComplianceTypeId
                                                ,
                                            row.EventID
                                                ,
                                            row.SubComplianceType
                                                ,
                                            row.FixedGap
                                                ,
                                            row.CheckListTypeID
                                                ,
                                            RISK = row.RiskType == 0 ? "High" :
                                                row.RiskType == 1 ? "Medium" :
                                                row.RiskType == 2 ? "Low" : "",
                                            FrequencyName = row.Frequency == 0 ? "Monthly" :
                                            row.Frequency == 1 ? "Quarterly" :
                                            row.Frequency == 2 ? "HalfYearly" :
                                            row.Frequency == 3 ? "Annual" :
                                            row.Frequency == 4 ? "FourMonthly" :
                                            row.Frequency == 5 ? "TwoYearly" :
                                            row.Frequency == 6 ? "SevenYearly" :
                                            row.Frequency == 7 ? "Daily" :
                                            row.Frequency == 8 ? "Weekly" : ""
                                        });

                int risk = -1;
                //if (tbxFilter.Text.ToUpper().Equals("HIGH"))
                //    risk = 0;
                //else if (tbxFilter.Text.ToUpper().Equals("MEDIUM"))
                //    risk = 1;
                //else if (tbxFilter.Text.ToUpper().Equals("LOW"))
                //    risk = 2;

                string frequency = "";
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                //if (!string.IsNullOrEmpty(tbxFilter.Text))
                //{
                //    if (CheckInt(tbxFilter.Text))
                //    {
                //        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                //        compliancesQuery = compliancesQuery.Where(entry => entry.ID == a);
                //    }
                //    else
                //    {
                //        compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Sections.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.RequiredForms.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                //    }
                //}

                //if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                //{
                //    int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                //    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == a);
                //}
                //if (Convert.ToInt32(ddlComplinceCatagory.SelectedValue) != -1)
                //{
                //    int b = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
                //    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == b);
                //}
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                //if (Convert.ToInt32(ddlFilterFrequencies.SelectedValue) != -1)
                //{
                //    int c = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                //    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == c);
                //}
                //if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
                //{
                //    int c = Convert.ToInt32(ddlAct1.SelectedValue);
                //    compliancesQuery = compliancesQuery.Where(entry => entry.ActID == c);
                //}
                var compliances = compliancesQuery.ToList();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderBy(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderBy(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderBy(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderBy(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                //grdCompliances.DataSource = compliances;
                //grdCompliances.DataBind();
                //upCompliancesList.Update();
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //private void BindCompliances()
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        //if (rdFunctionBased.Checked)
        //        //    CmType = 0;
        //        //if (rdChecklist.Checked)
        //        //    CmType = 1;

        //        var compliancesData = Business.ComplianceManagement.GetAll1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.RiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.RiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.RiskType == 2)
        //                risk = "Low";

        //            string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency));
        //            dataSource.Add(new
        //            {
        //                complianceInfo.ID,
        //                complianceInfo.ActName,
        //                complianceInfo.Sections,
        //                complianceInfo.Description,
        //                complianceInfo.ComplianceType,
        //                complianceInfo.EventID,
        //                //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
        //                complianceInfo.UploadDocument,
        //                complianceInfo.RequiredForms,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
        //                complianceInfo.NonComplianceEffects,
        //                Risk = risk,
        //                complianceInfo.ShortDescription,
        //                complianceInfo.SubComplianceType,
        //                complianceInfo.CheckListTypeID
        //                //Parameters = GetParameters(complianceInfo.Value)
        //            });
        //        }
        //        //grdCompliances.DataSource = dataSource;
        //        //grdCompliances.DataBind();
        //        //upCompliancesList.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        #region Comment by rahul on 16 march 2016 from view delete
        //private void BindCompliances()
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        if (rdFunctionBased.Checked)
        //            CmType = 0;
        //        if (rdChecklist.Checked)
        //            CmType = 1;

        //        var compliancesData = Business.ComplianceManagement.GetAll(true, Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.Key.RiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.Key.RiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.Key.RiskType == 2)
        //                risk = "Low";

        //            string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency));
        //            dataSource.Add(new
        //            {
        //                complianceInfo.Key.ID,
        //                complianceInfo.Key.ActName,
        //                complianceInfo.Key.Sections,
        //                complianceInfo.Key.Description,
        //                complianceInfo.Key.ComplianceType,
        //                complianceInfo.Key.EventID,
        //                //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
        //                complianceInfo.Key.UploadDocument,
        //                complianceInfo.Key.RequiredForms,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency != null ? (int)complianceInfo.Key.Frequency : -1)),
        //                complianceInfo.Key.NonComplianceEffects,
        //                Risk = risk,
        //                complianceInfo.Key.ShortDescription,
        //                complianceInfo.Key.SubComplianceType,
        //                complianceInfo.Key.CheckListTypeID
        //                //Parameters = GetParameters(complianceInfo.Value)
        //            });
        //        }
        //        grdCompliances.DataSource = dataSource;
        //        grdCompliances.DataBind();
        //        upCompliancesList.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        #endregion
        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = ActManagement.GetAllNVP();
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        //private void BindStatusActs()
        //{
        //    try
        //    {
        //        ddlStatusAct.DataTextField = "Name";
        //        ddlStatusAct.DataValueField = "ID";

        //        ddlStatusAct.DataSource = ActManagement.GetAllNVP();
        //        ddlStatusAct.DataBind();

        //        ddlStatusAct.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    LinkButton LinkButton3 = (LinkButton)e.Row.FindControl("LinkButton3");
                    LinkButton lnkStatus = (LinkButton)e.Row.FindControl("lnkStatus");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;

                    //lblDeactivate.Visible = false;
                    //lnkStatus.Visible = false;
                    if (AuthenticationHelper.Role.Equals("CADMN"))
                    {
                        LinkButton3.Visible = false;
                        lnkStatus.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                        //LinkButton3.Visible = true;
                        //lblDeactivate.Visible = true;
                        lnkStatus.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                        //LinkButton3.Visible = true;
                        //lblDeactivate.Visible = true;
                        //lnkStatus.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }

        }

        protected void chkEventBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                vivDueDate.Visible = !chkEventBased.Checked;
                divEvent.Visible = chkEventBased.Checked;

                divComplianceDueDays.Visible = chkEventBased.Checked;
                cvfrequency.Enabled = !chkEventBased.Checked;
                divFrequency.Visible = !chkEventBased.Checked;
                vaDueDate.Enabled = !chkEventBased.Checked;
                if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divComplianceDueDays.Visible = !chkEventBased.Checked;
                }
                if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divComplianceDueDays.Visible = chkEventBased.Checked;
                    rfvEventDue.Enabled = chkEventBased.Checked;
                    vaDueDate.Enabled = chkEventBased.Checked;
                    cvfrequency.Enabled = !chkEventBased.Checked;
                    divFrequency.Visible = !chkEventBased.Checked;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
            }
        }

        protected bool ViewSchedule(long? EventID, object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (Convert.ToString(frequency) == "7" || Convert.ToString(frequency) == "8")
                {
                    return false;
                }
                else if (EventID != null)
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Server Error Occured. Please try again.";
                return false;
            }

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        //cvDuplicateEntry.IsValid = false;
                        // cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                        divsuccess.Visible = false;
                        diverror.Visible = true;
                        lblerror.InnerText = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                       // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        //upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbReminderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    divForCustome.Visible = true;
                }
                else
                {
                    divForCustome.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation(int eventID)
        {
            try
            {
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                var subEvents = EventManagement.GetAllHierarchy(eventID);
                if (subEvents[0].Children.Count > 0)
                {
                    divSubEventmode.Visible = true;
                    foreach (var item in subEvents)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvSubEvent.Nodes.Add(node);
                    }
                }
                else
                {
                    divSubEventmode.Visible = false;
                }

                tvSubEvent.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxSubEvent.Text = "< Select Sub Event >";
            tvSubEvent.Nodes.Clear();
            if (Convert.ToInt32(ddlEvents.SelectedValue) != -1)
            {
                divSubEventmode.Visible = true;
                BindLocation(Convert.ToInt32(ddlEvents.SelectedValue));
            }
            else
            {
                divSubEventmode.Visible = false;
            }
        }

        protected void tvSubEvent_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                if (tvSubEvent.SelectedNode.ChildNodes.Count == 0)
                {
                    //divEventComplianceType.Visible = false;
                    tbxSubEvent.Text = tvSubEvent.SelectedNode != null ? Regex.Replace(tvSubEvent.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);
                    rbEventComplianceType.SelectedValue = "0";
                }
                else
                {
                    divEventComplianceType.Visible = true;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").show(\"true\", null, 500, function () { });", true);
                }



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void TreeViewSelectedNode(string value)
        {
            foreach (TreeNode node in tvSubEvent.Nodes)
            {

                if (node.ChildNodes.Count > 0)
                {
                    foreach (TreeNode child in node.ChildNodes)
                    {
                        if (child.Value == value)
                        {
                            child.Selected = true;
                        }
                    }
                }
                else if (node.Value == value)
                {
                    node.Selected = true;
                }
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
        //private void BindIndustry()
        //{
        //    try
        //    {
        //        chkIndustry.DataTextField = "Name";
        //        chkIndustry.DataValueField = "ID";
        //        chkIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
        //        chkIndustry.DataBind();

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void BindLegalEntityType()
        //{
        //    try
        //    {
        //        chkLegalEntityType.DataTextField = "EntityTypeName";
        //        chkLegalEntityType.DataValueField = "ID";
        //        chkLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
        //        chkLegalEntityType.DataBind();

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private void BindIndustry()
        {
            try
            {

                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEntityType()
        {
            try
            {
                rptEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                rptEntityType.DataBind();

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");

                    if (!chkEntityType.Checked)
                    {
                        chkEntityType.Checked = true;
                    }
                }
                CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                EntityTypeSelectAll.Checked = true;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
            //BindComplianceMatrix();
            //if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
            //{
            //    setDateToGridView();
            //}
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        //protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(ViewState["ComplianceID"]), Date, "D", txtDeactivateDesc.Text);
        //        Business.ComplianceManagement.UpdateComplianceDate(Convert.ToInt32(ViewState["ComplianceID"]));
        //        if (FileUploadDeactivateDoc.FileBytes != null && FileUploadDeactivateDoc.FileBytes.LongLength > 0)
        //        {
        //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
        //            {
        //                FileUploadDeactivateDoc.SaveAs(ConfigurationManager.AppSettings["DriveUrl"] + "/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName);
        //            }
        //            else
        //            {
        //                FileUploadDeactivateDoc.SaveAs(Server.MapPath("~/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName));
        //            }
        //            ComplianceDeactive Deact = new ComplianceDeactive()
        //            {
        //                ComplianceID = Convert.ToInt32(ViewState["ComplianceID"]),
        //                Name = FileUploadDeactivateDoc.FileName,
        //                FileData = FileUploadDeactivateDoc.FileBytes,
        //                FilePath = "~/DeactiveComplianceFiles/" + FileUploadDeactivateDoc.FileName
        //            };
        //            Business.ComplianceManagement.DeleteOldDeactiveDoc(Convert.ToInt32(ViewState["ComplianceID"]));
        //            Business.ComplianceManagement.CreateDeactivateFile(Deact, true);

        //        }

        //        var compliancedeactive = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(ViewState["ComplianceID"]));
        //        if (compliancedeactive.Status == "D")
        //        {
        //            string ReplyEmailAddressName = "Avantis";
        //            string DeactivateDate = Convert.ToDateTime(compliancedeactive.DeactivateOn).ToString("dd-MM-yyyy");
        //            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ComplainceDeactivate
        //                                .Replace("@ShortDescription", compliancedeactive.ShortDescription)
        //                                .Replace("@DetailDescription ", compliancedeactive.Description)
        //                                .Replace("@DeactivateDate", DeactivateDate)
        //                                .Replace("@Requestor ", AuthenticationHelper.User)
        //                                .Replace("@DeactivateDescription ", compliancedeactive.DeactivateDesc)
        //                                .Replace("@From", ReplyEmailAddressName);

        //            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
        //            string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

        //            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "Compliance Deactivation Request.", message);
        //        }

        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
        //        BindCompliancesNew();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void ddlNatureOfCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceSubType(Convert.ToInt32(ddlNatureOfCompliance.SelectedValue));
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlAct1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //grdCompliances.DataSource = null;
            //grdCompliances.DataBind();
            //if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
            //{
            //    BindCompliancesNew();
            //}
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdoComplianceVisible_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdoComplianceVisible.Checked == true)
                {
                    ddlChklstType.SelectedValue = "0";
                    divChecklist.Visible = true;
                    divOneTime.Visible = true;
                }
                else
                {
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;
                    divTimebasedTypes.Visible = false;

                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSampleForm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    var file = Business.ComplianceManagement.GetSelectedComplianceFileName(ID, complianceID);
                    //Response.Buffer = true;
                    //Response.Clear();
                    //Response.ClearContent();
                    //Response.ContentType = "application/octet-stream";
                    //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.FileData); // create the file

                    //HttpContext.Current.Response.Flush(); // send it to the client to download
                    //Response.End();
                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename= " + file.Name);
                    Response.BinaryWrite(file.FileData);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
                else if (e.CommandName.Equals("DeleleSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    Business.ComplianceManagement.DeleteComplianceForm(ID, complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    grdSampleForm.DataSource = complianceForm;
                    grdSampleForm.DataBind();
                    //upCompliancesList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool chkexists = false;
                if (ViewState["GComplianceID"] != null)
                {
                    chkexists = Business.ComplianceManagement.ExistsSavedCompliance(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue), Convert.ToInt32(ViewState["GComplianceID"]));
                }
                else
                {
                    chkexists = Business.ComplianceManagement.ExistsR(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue));
                }


                Boolean ComplianceVisible = false;
                if (rdoComplianceVisible.Checked == true)
                {
                    ComplianceVisible = true;
                }

                if (ddlComplianceType.SelectedValue != "1")
                {
                    ComplianceVisible = true;
                }

                //Boolean chkIndustryFlag = false;
                //foreach (RepeaterItem aItem in rptIndustry.Items)
                //{
                //    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                //    if (chkIndustry.Checked)
                //    {
                //        chkIndustryFlag = true;
                //    }
                //}

                //if (chkIndustryFlag == false)
                //{
                //    saveopo.Value = "true";
                //    cvDuplicateEntry.ErrorMessage = "Please select at least one industry.";
                //    //cvDuplicateEntry.IsValid = false;
                //    return;
                //}

                if (chkexists == false)
                {
                    Business.Data.Compliance compliance = new Business.Data.Compliance()
                    {
                        ActID = Convert.ToInt32(ddlAct.SelectedValue),
                        Description = tbxDescription.Text,
                        Sections = tbxSections.Text,
                        ShortDescription = txtShortDescription.Text,
                        UploadDocument = chkDocument.Checked,
                        ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                        //ComplianceSubTypeID = Convert.ToByte(ddlComplianceSubType.SelectedValue),
                        RequiredForms = tbxRequiredForms.Text,
                        SampleFormLink = txtSampleFormLink.Text,
                        RiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                        PenaltyDescription = txtPenaltyDescription.Text,
                        ReferenceMaterialText = txtReferenceMaterial.Text,
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                        ComplinceVisible = ComplianceVisible,
                        //StartDate=Convert.ToDateTime(txtStartDate.Text),
                        ShortForm = txtShortForm.Text
                    };
                    string startdate = Request[txtStartDate.UniqueID].ToString().Trim();
                    DateTime dtstartdate = new DateTime();
                    if (startdate != "")
                    {
                        dtstartdate = DateTime.ParseExact(startdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        compliance.StartDate = dtstartdate;
                    }
                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2)//function based or time based
                    {
                        if (ddlNatureOfCompliance.SelectedValue != "")
                        {
                            if (ddlNatureOfCompliance.SelectedValue != "-1")
                            {
                                compliance.NatureOfCompliance = Convert.ToByte(ddlNatureOfCompliance.SelectedValue);
                            }
                        }
                        if (ddlComplianceSubType.SelectedValue != "")
                        {
                            if (ddlComplianceSubType.SelectedValue != "-1")
                            {
                                compliance.ComplianceSubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                            }
                        }
                        if (compliance.ComplianceType == 2)//time based
                        {
                            if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                            {
                                compliance.EventID = null;
                                compliance.Frequency = null;
                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                compliance.SubComplianceType = 0;
                            }
                            else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                if (compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                }
                                else
                                {
                                    compliance.DueDate = null;
                                }
                                compliance.SubComplianceType = 1;
                            }
                            else
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                if (compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                }
                                else
                                {
                                    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                }
                                compliance.SubComplianceType = 2;
                            }
                        }
                        else
                        {

                            if (chkEventBased.Checked == true)
                            {
                                compliance.Frequency = null;
                                if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                                {
                                    compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                }
                                else
                                {
                                    compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                    compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                                }

                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                compliance.EventComplianceType = Convert.ToByte(rbEventComplianceType.SelectedValue);
                            }
                            else
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                if (compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                }
                                else
                                {
                                    compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                }
                            }
                        }

                        compliance.NonComplianceType = ddlNonComplianceType.SelectedValue == "-1" ? (byte?)null : Convert.ToByte(ddlNonComplianceType.SelectedValue);
                        compliance.NonComplianceEffects = tbxNonComplianceEffects.Text;

                        compliance.FixedMinimum = tbxFixedMinimum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMinimum.Text) : (double?)null;
                        compliance.FixedMaximum = tbxFixedMaximum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMaximum.Text) : (double?)null;
                        if (ddlPerDayMonth.SelectedValue == "0")
                        {
                            compliance.VariableAmountPerDay = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerMonth = null;
                        }
                        else
                        {
                            compliance.VariableAmountPerMonth = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerDay = null;
                        }
                        compliance.VariableAmountPerDayMax = tbxVariableAmountPerDayMax.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDayMax.Text) : (double?)null;
                        compliance.VariableAmountPercent = tbxVariableAmountPercent.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercent.Text) : (double?)null;
                        compliance.VariableAmountPercentMax = tbxVariableAmountPercentMaximum.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercentMaximum.Text) : (double?)null;

                        compliance.Imprisonment = chbImprisonment.Checked;
                        compliance.Designation = tbxDesignation.Text;
                        compliance.MinimumYears = tbxMinimumYears.Text.Length > 0 ? Convert.ToInt32(tbxMinimumYears.Text) : (int?)null;
                        compliance.MaximumYears = tbxMaximumYears.Text.Length > 0 ? Convert.ToInt32(tbxMaximumYears.Text) : (int?)null;
                    }
                    else
                    {

                        compliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);

                        if (chkEventBased.Checked == true)
                        {
                            if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                            {
                                compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                            }
                            else
                            {
                                compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                            }
                            compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                        }


                        compliance.NatureOfCompliance = null;
                        compliance.NonComplianceType = null;
                        compliance.NonComplianceEffects = null;
                        //------checklist------
                        if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based
                        {
                            if (compliance.CheckListTypeID == 2)//time based
                            {
                                if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = null;
                                    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                    compliance.SubComplianceType = 0;
                                }
                                else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = null;
                                    }
                                    compliance.SubComplianceType = 1;
                                }
                                else
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                    }
                                    compliance.SubComplianceType = 2;
                                }
                            }
                            else
                            {

                                if (chkEventBased.Checked == true)
                                {
                                    compliance.Frequency = null;
                                    if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                                    {
                                        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                        compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                                    }

                                    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                    compliance.EventComplianceType = Convert.ToByte(rbEventComplianceType.SelectedValue);
                                }
                                else
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                    }
                                }
                            }
                            //--------
                        }
                        else if (compliance.CheckListTypeID == 0 && compliance.ComplinceVisible == true)//One time 
                        {
                            string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                            DateTime dtOneTimeDate = new DateTime();
                            if (Onetimedate != "")
                            {
                                dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                compliance.OneTimeDate = dtOneTimeDate;
                            }
                        }
                    }

                    ////Boolean IsComplianceFileDuplicate = false;
                    //ComplianceForm form = null;
                    //if (chkDocument.Checked)
                    //{
                    //    if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                    //    {
                    //        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //        {
                    //            //string directoryPath = null;
                    //            //directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/ComplianceFiles/" + fuSampleFile.FileName;
                    //            fuSampleFile.SaveAs(ConfigurationManager.AppSettings["DriveUrl"] + "/ComplianceFiles/" + fuSampleFile.FileName);
                    //            //string filepathvalue = string.Empty;
                    //            //if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //            //{
                    //            //    filepathvalue = directoryPath.Replace("\\", "");
                    //            //}
                    //            form = new ComplianceForm()
                    //            {
                    //                Name = fuSampleFile.FileName,
                    //                FileData = fuSampleFile.FileBytes,
                    //                FilePath = "~/ComplianceFiles/" + fuSampleFile.FileName
                    //            };
                    //        }
                    //        else
                    //        {
                    //            fuSampleFile.SaveAs(Server.MapPath("~/ComplianceFiles/" + fuSampleFile.FileName));
                    //            form = new ComplianceForm()
                    //            {
                    //                Name = fuSampleFile.FileName,
                    //                FileData = fuSampleFile.FileBytes,
                    //                FilePath = "~/ComplianceFiles/" + fuSampleFile.FileName
                    //            };
                    //        }
                    //    }
                    //}

                    ComplianceForm form = null;
                    List<ComplianceForm> ComplianceForms = new List<ComplianceForm>();
                    if (chkDocument.Checked)
                    {
                        if (fuSampleFile.PostedFiles.Count() > 0)
                        //if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                        {
                            HttpFileCollection fileCollection = Request.Files;
                            if (fileCollection.Count > 0)
                            {

                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = null;
                                    uploadfile = fileCollection[i];
                                    string fileName = uploadfile.FileName;

                                    if (!string.IsNullOrEmpty(fileName))
                                    {
                                        //fuSampleFile.SaveAs(Server.MapPath("~/ComplianceFiles/" + fileName));
                                        fileCollection[i].SaveAs(Server.MapPath("~/ComplianceFiles/" + fileName));
                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                        form = new ComplianceForm()
                                        {
                                            Name = fileName,
                                            FileData = bytes,
                                            FilePath = "~/ComplianceFiles/" + fileName,
                                        };

                                        ComplianceForms.Add(form);
                                    }
                                }
                            }
                        }
                    }

                    compliance.ReminderType = Convert.ToByte(rbReminderType.SelectedValue);
                    if (rbReminderType.SelectedValue.Equals("1"))
                    {

                        compliance.ReminderBefore = Convert.ToInt32(txtReminderBefore.Text);
                        compliance.ReminderGap = Convert.ToInt32(txtReminderGap.Text);
                    }

                    List<ComplianceParameter> parameters = new List<ComplianceParameter>(); //GetComplianceParameters();

                    if ((int)ViewState["Mode"] == 1)
                    {
                        compliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        //Business.ComplianceManagement.Create(compliance, form);
                        Business.ComplianceManagement.CreateMultiple(compliance, ComplianceForms);
                        /// Add compliance new filed in  ComplianceDetail table SACHIN 04 Aug 2017
                        ComplianceDetail compliancedetail = new ComplianceDetail()
                        {
                            ComplianceID = compliance.ID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };
                        Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);

                        #region AddingIndustry
                        //List<int> IndustryIds = new List<int>();
                        //foreach (RepeaterItem aItem in rptIndustry.Items)
                        //{
                        //    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        //    if (chkIndustry.Checked)
                        //    {
                        //        IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));


                        //        IndustryMapping IndustryMapping = new IndustryMapping()
                        //        {
                        //            ComplianceId = compliance.ID,
                        //            IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                        //            IsActive = true,
                        //            EditedDate = DateTime.UtcNow,
                        //            EditedBy = Convert.ToInt32(Session["userID"]),


                        //        };
                        //        Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                        //    }
                        //}
                        #endregion

                        #region AddEntityType
                        //List<int> EntityTypeIds = new List<int>();
                        //foreach (RepeaterItem aItem in rptEntityType.Items)
                        //{
                        //    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        //    if (chkEntityType.Checked)
                        //    {
                        //        EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                        //        LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                        //        {
                        //            ComplianceId = compliance.ID,
                        //            LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                        //            IsActive = true,
                        //            EditedDate = DateTime.UtcNow,
                        //            EditedBy = Convert.ToInt32(Session["userID"]),

                        //        };
                        //        Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                        //    }
                        //}
                        #endregion
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                        if (chkexistsComplianceschedule == true)
                        {

                            //Business.ComplianceManagement.UpdateIFInstanceIsCreatedCompliance(compliance, form);
                            Business.ComplianceManagement.UpdateIFInstanceIsCreatedCompliance(compliance, ComplianceForms);


                        }
                        else
                        {
                            //Business.ComplianceManagement.Update(compliance, form);
                            Business.ComplianceManagement.Update(compliance, ComplianceForms);
                        }
                        /// Add compliance new filed in  ComplianceDetail table SACHIN 04 Aug 2017
                        bool chkexistsComplianceDetail = Business.ComplianceManagement.ExistsComplianceDetail(Convert.ToInt64(ViewState["ComplianceID"]));
                        if (chkexistsComplianceDetail == false)
                        {
                            ComplianceDetail compliancedetail = new ComplianceDetail()
                            {
                                ComplianceID = compliance.ID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };
                            Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);
                        }
                        else
                        {
                            Business.ComplianceManagement.UpdateComplianceDetail(compliance.ID, AuthenticationHelper.UserID);
                        }
                        //---------add Industry--------------------------------------------
                        List<int> IndustryIds = new List<int>();
                        Business.ComplianceManagement.UpdateIndustryMappedID(compliance.ID);
                        foreach (RepeaterItem aItem in rptIndustry.Items)
                        {
                            CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                            if (chkIndustry.Checked)
                            {
                                IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                                IndustryMapping IndustryMapping = new IndustryMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                };

                                Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                            }
                        }

                        //---------add Legal Entity type--------------------------------------------
                        List<int> EntityTypeIds = new List<int>();
                        Business.ComplianceManagement.UpdateLegalEntityMappedID(compliance.ID);
                        foreach (RepeaterItem aItem in rptEntityType.Items)
                        {
                            CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                            if (chkEntityType.Checked)
                            {
                                EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                                LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),

                                };
                                Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                            }
                        }
                    }

                    Business.ComplianceManagement.UpdateComplianceTagMappedID(compliance.ID);

                    if (txtCompliancetag.Text != "")
                    {
                        string[] arrFileTags = txtCompliancetag.Text.Split(',');

                        if (arrFileTags.Length > 0)
                        {
                            List<ComplianceDatatagMapping> lstFiletagdataMapping = new List<ComplianceDatatagMapping>();

                            for (int j = 0; j < arrFileTags.Length; j++)
                            {
                                ComplianceDatatagMapping objFileTagMapping = new ComplianceDatatagMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    FileTag = arrFileTags[j].Trim(),
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    UpdatedOn = DateTime.Now,
                                    DocType = "S"
                                };

                                lstFiletagdataMapping.Add(objFileTagMapping);
                            }

                            if (lstFiletagdataMapping.Count > 0)
                            {
                                Business.ComplianceManagement.CreateUpdate_ComplianceFileTagsMappingData(lstFiletagdataMapping);
                            }
                        }
                    }
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close')", true);
                    BindCompliancesNew();
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(compliance.ID);

                    if (complianceForm.Count > 0)
                    {
                        hdnFile.Value = Convert.ToString(compliance.ID);
                        divSampleForm.Visible = true;
                        grdSampleForm.DataSource = complianceForm;
                        grdSampleForm.DataBind();
                    }
                    else
                    {
                        divSampleForm.Visible = false;
                    }
                    //upCompliancesList.Update();
                    divsuccess.Visible = true;
                    //cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                    lblsuccess.InnerText= "Record Saved Sucessfully.";
                    diverror.Visible = false;
                }
                else
                {
                    //cvDuplicateEntry.IsValid = false;
                    divsuccess.Visible = false;
                    diverror.Visible = true;
                    lblerror.InnerText = "Compliance Short Description and Detailed Description compliance allready present in System";
                    //cvDuplicateEntry.ErrorMessage = "Compliance Short Description and Detailed Description compliance allready present in System";
                    //saveopo.Value = "true";
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                //cvDuplicateEntry.IsValid = false;
                divsuccess.Visible = false;
                diverror.Visible = true;
                lblerror.InnerText = "Compliance Short Description and Detailed Description compliance allready present in System";
            }
        }
    }

}