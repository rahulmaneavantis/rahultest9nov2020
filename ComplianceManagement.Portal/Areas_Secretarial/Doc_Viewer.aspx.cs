﻿using BM_ManegmentServices.Services.DocumentManagenemt;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using GleamTech.DocumentUltimate.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Areas
{
    public partial class Doc_Viewer : System.Web.UI.Page
    {
        FileData_Service objIFileData_Service = new FileData_Service();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docurl"]))
                {
                    string filepath = Request.QueryString["docurl"].ToString();
                    if (filepath != "undefined")
                    {
                        long fileID = 0;
                        if(!long.TryParse(filepath, out fileID))
                        {
                            fileID = 0;
                        }
                        if(fileID >0)
                        {
                            var UserID = AuthenticationHelper.UserID;
                            var obj = objIFileData_Service.GetFile(fileID, UserID);

                            if(obj.FileData!=null)
                            {
                                lblFileName.Text = obj.FileName +" V. "+obj.Version +".0";
                                DocumentInfo di = new DocumentInfo(obj.FileKey, obj.FileName, null);
                                doccontrol.DocumentSource = new DocumentSource(di, obj.FileData);

                                doccontrol.SidePaneVisible = false;
                            }
                        }
                    }
                    else
                    {
                        //lblMessage.Text = "Sorry File not find";
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["agendaDocUrl"]))
                {
                    string filepath = Request.QueryString["agendaDocUrl"].ToString();
                    if (filepath != "undefined")
                    {
                        long fileID = 0;
                        if (!long.TryParse(filepath, out fileID))
                        {
                            fileID = 0;
                        }
                        if (fileID > 0)
                        {
                            var UserID = AuthenticationHelper.UserID;
                            BM_ManegmentServices.Services.Masters.FileUpload objFileUpload = new BM_ManegmentServices.Services.Masters.FileUpload();
                            var obj = objFileUpload.GetAgendaItemsFile(fileID, UserID);

                            if (obj.FileData != null)
                            {
                                lblFileName.Text = obj.FileName;// + " V. " + obj.Version + ".0";
                                DocumentInfo di = new DocumentInfo(obj.FileKey, obj.FileName, null);
                                doccontrol.DocumentSource = new DocumentSource(di, obj.FileData);
                            }
                        }
                    }
                    else
                    {
                        //lblMessage.Text = "Sorry File not find";
                    }
                }
                else if(!string.IsNullOrEmpty(Request.QueryString["ConcludedMeetingMinutes"]))
                {

                }

                #region Agenda Doc Url
                //
                #endregion
            }
        }
    }
}