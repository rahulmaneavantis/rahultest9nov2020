﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Chart
{
    public partial class PieChart : System.Web.UI.UserControl
    {
        public event EventHandler Click = null;

        public int Height { get; set; }
        public int Width { get; set; }
        public List<DataItem> DataSource { get; set; }
        public DataItem Selected { get; private set; }

        protected bool DoPostBack
        {
            get
            {
                return Click != null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["DataSource"] != null)
            {
                DataSource = ViewState["DataSource"] as List<DataItem>;
            }
        }

        protected void btnPostback_Click(object sender, EventArgs e)
        {
            if (Click != null)
            {
                Selected = DataSource[Convert.ToInt32(hdnSelectedID.Value)];
                Click(this, null);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (DataSource == null)
            {
                DataSource = new List<DataItem>();
            }
            ViewState["DataSource"] = DataSource;
            string dataSouce = DataSource.ToJSON();
            ScriptManager.RegisterStartupScript(this, this.GetType(), ClientID + "_Initialize", string.Format("{0}_Initialize({1});", ClientID, dataSouce), true);
        }
    }
}