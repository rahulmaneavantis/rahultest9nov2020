﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BarChart.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Chart.BarChart" %>
<style type="text/css">
    .axis path, .axis line
    {
        fill: none;
        stroke: black;
        shape-rendering: crispEdges;
    }
    
    .axis text
    {
        font-family: sans-serif;
        font-size: 11px;
    }
    
    #div_<%= ClientID %>_Tooltip
    {
        position: absolute;
        width: 200px;
        height: auto;
        padding: 10px;
        background-color: white;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        -webkit-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        -moz-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.4);
        pointer-events: none;
    }
    
    #div_<%= ClientID %>_Tooltip.hidden
    {
        display: none;
    }
    
    #div_<%= ClientID %>_Tooltip p
    {
        margin: 0;
        font-family: sans-serif;
        font-size: 16px;
        line-height: 20px;
    }
</style>
<div id="div_<%= ClientID %>_Chart">
</div>
<div id="div_<%= ClientID %>_Tooltip" class="hidden">
    <p>
        <strong><span id="name">Label Heading</span></strong></p>
    <p>
        <span id="value">100</span></p>
</div>
<script type="text/javascript">
function <%= ClientID %>_Initialize(dataset)
{
    //Width and height
    var w = <%= Width %>;
    var h = <%= Height %>;
    var padding = 30;

    //var dataset = [ 5, 10, 13, 19, 21, 25, 22, 18, 15, 13,
    //		11, 12, 15, 20, 18, 17, 16, 18, 23, 25 ];

//    var dataset = [
//    				    { 'Name': 'Yogesh Satpute', 'Code': 'YS', 'Count': 5 },
//    				    { 'Name': 'Preeti Nahar', 'Code': 'PN', 'Count': 10 },
//    				    { 'Name': 'Ameya Nadkarni', 'Code': 'AN', 'Count': 13 },
//    				    { 'Name': 'Sudarshan Kapse', 'Code': 'SK', 'Count': 25 },
//    				    { 'Name': 'Sujata Bhagat', 'Code': 'SB', 'Count': 22 },
//    				    { 'Name': 'Ratika Sangeshwar', 'Code': 'RS', 'Count': 13 },
//    				    { 'Name': 'Madhuri Rasal', 'Code': 'MR', 'Count': 15 },
//    				    { 'Name': 'Rupali Jadhav', 'Code': 'RJ', 'Count': 20 }
//    			    ];

//        var dataset = [];

    var xScale = d3.scale.ordinal()
							.domain(dataset.map(function (d) { return d.Code; }))
							.rangeRoundBands([padding, w - padding], 0.05);

    var yScale = d3.scale.linear()
							.domain([0, d3.max(dataset, function (d) { return d.Count; })])
							.range([h - padding, padding]);

    //Create SVG element
    var svg = d3.select("#div_<%= ClientID %>_Chart")
						.append("svg")
						.attr("width", w)
						.attr("height", h)
                        .attr("style", "padding-left:25px");
                        
    if (dataset.length == 0)
    {
        svg.append("text")
		    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")")
			.attr("text-anchor", "middle")
			.attr("fill", "black")
			.text('No data available');

        return;
    }
        
    var xAxis = d3.svg.axis()
						  .scale(xScale)
						  .tickSize(5)
						  .tickSubdivide(false);

    var yAxis = d3.svg.axis()
				  .scale(yScale)
                  .ticks(h / 50)
				  .tickSize(3)
				  .orient("left")
				  .tickSubdivide(false);

    svg.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0,' + (h - padding) + ')')
			.call(xAxis);

    svg.append('g')
			.attr('class', 'y axis')
			.attr('transform', 'translate(' + (padding) + ',0)')
			.call(yAxis);

    //Create bars
    svg.selectAll("rect")
			   .data(dataset)
			   .enter()
			   .append("rect")
			   .transition()
			   .delay(function (d, i) {
			       return i * 50;
			   })
			   .duration(500)
			   .attr("x", function (d, i) {
			       return xScale(d.Code);
			   })
			   .attr("y", function (d) {
			       return yScale(d.Count);
			   })
			   .attr("width", xScale.rangeBand())
			   .attr("height", function (d) {
			       return (h - padding - yScale(d.Count));
			   })
			   .attr("fill", function (d) {
			       return "teal";
			   })
               .attr ('title', function (d, i) { return dataset[i].Name + ': ' + dataset[i].Count; })
               .attr("opacity", "0.7");

    svg.selectAll("rect")
			   .on('click', function (d, i) {
			       //alert('Name : ' + d.Name);
			   })
			   .on('mouseover', function (d) {
			       d3.select(this)
                    .attr("opacity", "1");
			       //Get this bar's x/y values, then augment for the tooltip
//			       var xPosition = d3.event.pageX;
//			       var yPosition = d3.event.pageY;

//			       //Update the tooltip position and value
//			       d3.select("#div_<%= ClientID %>_Tooltip")
//					  .style("left", xPosition + "px")
//					  .style("top", yPosition + "px")
//					  .select("#value")
//					  .text(d.Count);

//			       d3.select("#div_<%= ClientID %>_Tooltip")
//					  .select('#name')
//					  .text(d.Name);

//			       //Show the tooltip
//			       d3.select("#div_<%= ClientID %>_Tooltip").classed("hidden", false);
			   })
				.on('mouseout', function (d) {
				    d3.select(this)
                        .attr("opacity", "0.7");
				    //Hide the tooltip
//				    d3.select("#div_<%= ClientID %>_Tooltip").classed("hidden", true);
				});
}
</script>
