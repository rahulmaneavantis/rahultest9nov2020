﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SecurityQuestion.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Reset password :: AVANTIS - Products that simplify</title>

    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../NewCSS/style.css" rel="stylesheet" />
    <link href="../NewCSS/style-responsive.css" rel="stylesheet" />
     <script type="text/javascript">
         var _paq = window._paq || [];
         /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
         _paq.push(['trackPageView']);
         _paq.push(['enableLinkTracking']);
         (function () {
             var u = "//analytics.avantis.co.in/";
             _paq.push(['setTrackerUrl', u + 'matomo.php']);
             _paq.push(['setSiteId', '1']);
             var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
             g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
         })();

         function settracknew(e, t, n, r) {
             debugger;
             try {
                 _paq.push(['trackEvent', e, t, n])
             } catch (t) { } return !0
         }

         function settracknewForForgotPass() {
             settracknew('Login', 'Forgot password', 'Success', '')
         }
         function settracknewForFailed() {
             settracknew('Login', 'Forgot password', 'Failed', '')
         }
    </script>
</head>
<body>

    <div class="container">
        <form runat="server" class="login-form" name="login" id="Form1" autocomplete="off">
            <asp:Panel ID="Panel1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />

               

                <asp:UpdatePanel ID="upResetPassword" runat="server" style="margin-top: 50px;">
                    <ContentTemplate>

                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.5;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                        <div class="col-md-12 login-form-head">
                            <p class="login-img"><a href="https://www.avantis.co.in">
                                <img src="../Images/avantil-logo.png" /></a>
                            </p>
                        </div>

                        <div class="login-wrap">
                                                           
                                <div id="divResetPasswordSubmit" class="row"  runat="server">
                                     <h1>Reset Password</h1>
                                    <div class="input-group">
                                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                                    <asp:TextBox ID="txtResetPasswordUserID" CssClass="form-control" runat="server" placeholder="Enter User Name/ Email ID" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please enter email Id." ControlToValidate="txtResetPasswordUserID"
                                    runat="server" ValidationGroup="ResetPasswordValidationGroup" Display="None" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <asp:Button Text="Submit" runat="server" ID="btnProceed" TabIndex="3" CssClass="btn btn-primary btn-lg btn-block"
                                    OnClick="btnProceed_Click" ValidationGroup="ResetPasswordValidationGroup" />

                                    <div style="margin-top: 15px;">
                                        <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Text="Click Here to Go Back" OnClick="lnklogin_Click"></asp:LinkButton>
                                    </div>
                                </div>  
                                                     
                            <div class="clearfix" style="height: 10px"></div>
                         
                            <div id="tblResetPasswordQuestion" visible="false" runat="server">
                                <div class="clearfix" style="height: 10px"></div>
                                <h2>Please answer your security questions</h2>
                                <p>This questions helps verify your identity</p>
                                <div class="clearfix" style="height: 10px"></div>
                                <h3>
                                    <asp:Label ID="lblRPQustion1ID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblRPQustion1" runat="server"></asp:Label>
                                </h3>
                                <div class="input-group" style="width: 100%">
                                    <%--<input type="text" class="form-control" placeholder="Enter answer" autofocus>--%>
                                    <asp:TextBox ID="txtRPQustion1" runat="server" class="form-control" placeholder="Enter answer" TabIndex="4"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Answer can not be empty." ControlToValidate="txtRPQustion1"
                                        runat="server" ValidationGroup="ResetPasswordValidationGroup" Display="None" />
                                </div>
                                <h3>
                                    <asp:Label ID="lblRPQustion2ID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblRPQustion2" runat="server"></asp:Label>
                                </h3>
                                <div class="input-group" style="width: 100%">
                                    <asp:TextBox ID="txtRPQustion2" runat="server" class="form-control" placeholder="Enter answer" TabIndex="5"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Answer can not be empty." ControlToValidate="txtRPQustion2"
                                        runat="server" ValidationGroup="ResetPasswordValidationGroup" Display="None" />
                                    <%--<input type="text" class="form-control" placeholder="Enter answer" autofocus>--%>
                                </div>
                                <div class="clearfix"></div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:Button Text="Submit" runat="server" ID="btnResetPassword" CssClass="btn btn-primary btn-lg btn-block" TabIndex="8"
                                            ValidationGroup="ResetPasswordValidationGroup" OnClick="btnResetPassword_Click" />
                                    </div>

                                    <div class="col-md-6">
                                        <asp:Button Text="Clear" runat="server" ID="btnResetPasswordClear" CssClass="btn btn-primary btn-lg btn-block"
                                            TabIndex="9" OnClick="btnResetPasswordClear_Click" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <%--<div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="icon-remove"></i>
                                </button>--%>
                            <%-- <strong>Sorry!</strong> --%>
                            <asp:ValidationSummary ID="ValidationSummary3" class="alert alert-block alert-danger fade in" runat="server"
                                ValidationGroup="ResetPasswordValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ResetPasswordValidationGroup" Display="None" />
                            <%--  </div>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
</body>
</html>
