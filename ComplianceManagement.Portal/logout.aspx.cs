﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                //FormsAuthentication.SignOut();
                //Session.Abandon();
                //FormsAuthentication.RedirectToLoginPage();
                //Response.Redirect("LogoutSuccessfully.aspx", false);   
                try
                {
                    int userID = -1;                    
                    if (Convert.ToString(Session["userID"]) != null)
                    {
                        if (Convert.ToString(Session["userID"]) != "")
                        {
                            userID = Convert.ToInt32(Session["userID"]);
                        }
                        else
                        {
                            userID = AuthenticationHelper.UserID;
                        }
                    }
                    else
                    {
                        userID = AuthenticationHelper.UserID;
                    }
                    //LoginsRepository login = new LoginsRepository();
                    //login.UserId = userID;               
                    //login.SessionId = System.Web.HttpContext.Current.Session.SessionID; ;
                    //login.LoggedIn = false;
                    //login.IS_Start_Stop = "LT";
                    //UserManagement.SessionLog(login);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }           
               
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                Response.Redirect("LogoutSuccessfully.aspx", false);
            }
            catch (Exception ex)
            {
               // LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}