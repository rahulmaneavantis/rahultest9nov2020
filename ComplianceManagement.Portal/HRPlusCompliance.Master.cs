﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class HRPlusCompliance : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string userRole;
        protected List<Int32> roles;
        protected List<Int32> TaskRoles;
        protected static int customerid;
        protected static int userid;
        protected string Approveruser_Roles;
        protected int checkTaskapplicable = 0;

        protected bool showMyWorkspace = false;
        protected bool vendorAuditApplicable = false;
        public List<RLCSMenu> LstMenuRLCSItems;
        public List<RLCSChildMenu> LstChildMenuDetails;
        
        protected IList<Cust> lstUserAssignedCustomers;
        public class Cust
        {
            public int custID { get; internal set; }
            public string custName { get; internal set; }
            public string logoPath { get; internal set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    userRole = AuthenticationHelper.Role;

                    if (Session["AssignedRoles"] != null)
                    {
                        roles = (Session["AssignedRoles"]) as List<int>; ;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["AssignedRoles"] = roles;
                    }

                    BindUserAssignedCustomers();

                    if (!IsPostBack)
                    {
                        hdnProfileID.Value = AuthenticationHelper.ProfileID;
                        hdnAuthKey.Value = AuthenticationHelper.AuthKey;

                        customerid = Convert.ToInt32(AuthenticationHelper.CustomerID); //UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        userid = AuthenticationHelper.UserID;

                        //if (Session["vendorAuditApplicable"] != null)
                        //{
                        //    vendorAuditApplicable = Convert.ToBoolean(Session["vendorAuditApplicable"]);
                        //}
                        //else
                        //{
                        //    vendorAuditApplicable = RLCSManagement.CheckScopeApplicability(customerid, "SOW10");
                        //    Session["vendorAuditApplicable"] = vendorAuditApplicable;
                        //}

                        showMyWorkspace = true;
                        //if (Session["showMyWorkspace"] != null)
                        //{
                        //    showMyWorkspace = Convert.ToBoolean(Session["showMyWorkspace"]);
                        //}
                        //else
                        //{
                        //    showMyWorkspace = RLCS_Master_Management.CheckShowHideInputs(customerid, userid, 95);
                        //    Session["showMyWorkspace"] = showMyWorkspace;
                        //}

                        Page.Header.DataBind();

                        if (Session["LastLoginTime"] != null)
                        {
                            LastLoginDate = Session["LastLoginTime"].ToString();
                        }

                        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            if (AuthenticationHelper.UserID != -1)
                            {
                                //var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                                //if (cname != null)
                                //{
                                //    CustomerName = cname;
                                //}
                            }
                        }

                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (LoggedUser != null)
                        {
                            if (LoggedUser.ImagePath != null)
                            {
                                ProfilePicTop.Src = LoggedUser.ImagePath;
                            }
                            else
                            {
                                ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                            }
                        }
                        else
                        {
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                        }

                        string pageName = this.ContentPlaceHolder1.Page.GetType().FullName;

                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                        if (cname != null)
                        {
                            custName.InnerHtml = cname;
                        }
                    }

                    int? serviceproviderid = CustomerManagement.GetServiceProviderID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (serviceproviderid == 94)
                    {
                        LstMenuRLCSItems = new List<RLCSMenu>();
                        LstChildMenuDetails = new List<RLCSChildMenu>();

                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 1, MenuName = "Authority Address", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 2, MenuName = "PO Details", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 3, MenuName = "Commercial SetUp", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 4, MenuName = "Reg Knowledge", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 5, MenuName = "Scope & PD", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 6, MenuName = "Registration Repository", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 7, MenuName = "EPF/ESIC Summary Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 8, MenuName = "Minimum Wages Reckoner", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 9, MenuName = "Compliance Certificate", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 10, MenuName = "Registrations And Certifications", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 11, MenuName = "Digital Signature List", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 12, MenuName = "Corporation & Client Master Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 13, MenuName = "Inspection Notices", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 14, MenuName = "HealthCheck Creation", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 15, MenuName = "KYC&PMRPY", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 16, MenuName = "ESIC Cards Input", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 17, MenuName = "MIS Abstract And Display", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 18, MenuName = "Regulatory Abstracts", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 19, MenuName = "Leave Summary", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 20, MenuName = "Document Activity Mapping", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 21, MenuName = "Client Location Setup", LoggedInUserId = AuthenticationHelper.ProfileID });

                        LstMenuRLCSItems.Add(new RLCSMenu { ParentId = 1, ParentMainMenu = "RLCS", LstChild = LstChildMenuDetails });

                        LstChildMenuDetails = new List<RLCSChildMenu>();
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 22, MenuName = "Invoice Generation", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 23, MenuName = "Invoice Updation", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 24, MenuName = "Invoice Approval", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 25, MenuName = "Invoice FaceSheet", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 26, MenuName = "Invoice Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 27, MenuName = "Invoice Lock", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 28, MenuName = "Govt Invoice", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 29, MenuName = "Invoice GST Upload", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 30, MenuName = "Invoice GST Approval", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstMenuRLCSItems.Add(new RLCSMenu { ParentId = 2, ParentMainMenu = "RLCS Invoicing", LstChild = LstChildMenuDetails });

                        LstChildMenuDetails = new List<RLCSChildMenu>();
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 31, MenuName = "Women workinhg Nightshift", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 32, MenuName = "ERI Applicability Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 33, MenuName = "Employee Min Wage Setup", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 34, MenuName = "Monthly Employee Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 35, MenuName = "Manage Tracker", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 36, MenuName = "Negative Salary Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 37, MenuName = "Master Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 38, MenuName = "MIS External Report", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 39, MenuName = "Invoice ZoneWise", LoggedInUserId = AuthenticationHelper.ProfileID });
                        LstChildMenuDetails.Add(new RLCSChildMenu { Id = 40, MenuName = "Internal MIS Report", LoggedInUserId = AuthenticationHelper.ProfileID });

                        LstMenuRLCSItems.Add(new RLCSMenu { ParentId = 3, ParentMainMenu = "RLCS Reports", LstChild = LstChildMenuDetails });
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindUserAssignedCustomers()
        {
            try
            {
                int loggedInUserID = Convert.ToInt32((AuthenticationHelper.UserID));

                var custmerlist = UserCustomerMappingManagement.Get_UserCustomerMapping(loggedInUserID);

                if (custmerlist.Count > 1)
                {
                    lstUserAssignedCustomers = new List<Cust>();

                    foreach (var item in custmerlist)
                    {
                        string path = string.Empty;
                        if (item.LogoPath != null)
                            path = item.LogoPath.Replace("~", "");

                        lstUserAssignedCustomers.Add(new Cust()
                        {
                            custID = item.ID,
                            custName = item.Name,
                            logoPath = string.IsNullOrEmpty(path) ? null : path
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClick_Click(object sender, EventArgs e)
        {
            int customerID = Convert.ToInt32(hfCustId.Value);
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", "alert('" + customerID + "')", true);

            ProductMappingStructure _obj = new ProductMappingStructure();
            if (_obj.ReAuthenticate_User(customerID))
                Response.Redirect("~/RLCS/HRPlus_DFMDashboard.aspx", false);
        }
    }

    public class RLCSMenu
    {
        public int ParentId { get; set; }
        public string MenuName { get; set; }

        public int Id { get; set; }

        public string ParentMainMenu { get; set; }

        public List<RLCSChildMenu> LstChild { get; set; }

        public string LoggedInUserId { get; set; }

    }

    public class RLCSChildMenu
    {
        public int Id { get; set; }

        public string MenuName { get; set; }

        public string LoggedInUserId { get; set; }

    }
}