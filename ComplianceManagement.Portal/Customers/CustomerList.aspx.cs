﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        Session["CustomerID"] = null;
                        BindCustomers();
                        if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT") || AuthenticationHelper.Role.Equals("CADMN")  || AuthenticationHelper.Role.Equals("DADMN")  ))
                        {
                            btnAddCustomer.Visible = false;
                            divFilter.Visible = false;
                            btnSendNotification.Visible = false;
                           
                        }
                        ViewState["AssignedCompliancesID"] = null;
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                    
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
                
            }


            udcInputForm.OnSaved += (inputForm, args) => { BindCustomers(); };
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Customer List");
                        DataTable ExcelData = null;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            int CustomerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var compliancesData = (from row in entities.sp_CustomerBranchList(CustomerID)
                                                   select row).ToList();

                            DataTable table = compliancesData.AsDataTable();
                       
                            DataView view = new System.Data.DataView(table);
                            List<sp_CustomerBranchList_Result> cb = new List<sp_CustomerBranchList_Result>();
                            ExcelData = view.ToTable("Selected", false, "ID", "Name", "CustomerID", "ParentID");

                            exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A1"].Value = "ID";
                            exWorkSheet.Cells["A1"].AutoFitColumns(10);

                            exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B1"].Value = "Name";
                            exWorkSheet.Cells["B1"].AutoFitColumns(40);

                            exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C1"].Value = "CustomerID";
                            exWorkSheet.Cells["C1"].AutoFitColumns(15);

                            exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D1"].Value = "ParentID";
                            exWorkSheet.Cells["D1"].AutoFitColumns(15);
                        
                        }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 4])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=CustomerList.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    customerID = (int)AuthenticationHelper.CustomerID;
                }
                else
                {
                    var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                    if (aa == "AM" || aa == "AH")
                    {
                        customerID = (int)AuthenticationHelper.CustomerID;
                    }
                }
                if (AuthenticationHelper.Role.Equals("CADMN"))
                {
                    lbtnExportExcel.Visible = true;
                }
                else
                {
                    lbtnExportExcel.Visible = false;
                }
                List<object> dataSource = new List<object>();

                var customerData = CustomerManagement.GetAllCustomers(customerID, AuthenticationHelper.Role, distributorID, tbxFilter.Text);

                foreach (var customerInfo in customerData)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }

                grdCustomer.DataSource = dataSource;
                grdCustomer.DataBind();

                upCustomerList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    // TBD
                    udcInputForm.EditCustomerInformation(customerID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                  
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    if (CustomerManagement.CustomerDelete(customerID))
                    {
                        CustomerManagement.Delete(customerID);
                        CustomerManagementRisk.Delete(customerID);
                        BindCustomers();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Customer is associated with assigned compliance, can not be deleted')", true);
                    }
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["CustomerID"] = customerID;
                    Session["ParentID"] = null;
                    Response.Redirect("~/Customers/CustomerBranchList.aspx",false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomers();
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            // TBD
            udcInputForm.AddCustomer();
            BindCustomers();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
           
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role.Equals("CADMN"))
                {
                    foreach (DataControlField column in grdCustomer.Columns)
                    {
                        if (column.HeaderText == "")
                            column.Visible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var customerlist = CustomerManagement.GetAll(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }

                grdCustomer.DataSource = customerlist;
                grdCustomer.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        private void CheckBoxValueSaved()
        {
            List<int> chkList = new List<int>();
            int index = -1;
            foreach (GridViewRow gvrow in grdCustomer.Rows)
            {
                index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCustomer")).Checked;

                if (ViewState["CustomerID"] != null)
                    chkList = (List<int>)ViewState["CustomerID"];

                if (result)
                {
                    if (!chkList.Contains(index))
                        chkList.Add(index);
                }
                else
                    chkList.Remove(index);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["CustomerID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<int> chkList = (List<int>)ViewState["CustomerID"];
            if (chkList != null && chkList.Count > 0)
            {

                foreach (GridViewRow gvrow in grdCustomer.Rows)
                {
                    int index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                    if (chkList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCustomer");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {
                upServerDownTimeDetails.Update();
                ddlStartTime.Items.Clear();
                ddlEndTime.Items.Clear();
                SetTime(ddlStartTime);
                SetTime(ddlEndTime);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('open');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upServerDownTimeDetails_Load(object sender, EventArgs e)
        {
            try
            {
               
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", string.Format("initializeDowntimeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", "initializeDowntimeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SetTime(DropDownList DDl)
        {
            DDl.Items.Add(new ListItem("Select", "-1"));
            for (int i = 0; i <=12; i++)
            {
                if (i != 12)
                DDl.Items.Add(new ListItem(i.ToString("00") + " AM", i + " AM"));
                else
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }

            for (int i = 1; i <= 11; i++)
            {
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }
        }

        protected void btnSend_click(object sender, EventArgs e)
        {
            new Thread(() => { SendNotification(); }).Start();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('close');", true);
        }

        private void SendNotification()
        {
            try
            {
                string dated = txtDate.Text;
                string starttime = ddlStartTime.SelectedValue;
                string endtime = ddlEndTime.SelectedValue;

                CheckBoxValueSaved();
                List<int> chkList = (List<int>)ViewState["CustomerID"];
                if (chkList != null)
                {
                    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                    string ReplyEmailAddressName = "Avantis";

                    foreach (User user in userList)
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ServerDown
                                                .Replace("@User", username)
                                                .Replace("@Dated", dated)
                                                .Replace("@StartTime", starttime)
                                                .Replace("@EndTime", endtime)
                                                .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ReplyEmailAddressName);

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM server down.", message);
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}