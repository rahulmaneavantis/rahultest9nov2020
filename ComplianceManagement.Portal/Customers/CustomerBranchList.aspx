﻿<%@ Page Title="Customer Entities" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="CustomerBranchList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.CustomerBranchList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      .float-child {
            width: 83%;
            float: left;   
        }  
        .float-child1 {
            width: 10%;
            float: left;   
        }  
</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="center">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomerBranch" OnClick="btnAddCustomerBranch_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                            RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                    runat="server" Style="text-decoration: none; color: Black" />
                            </ItemTemplate>
                            <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                            <SeparatorStyle Font-Size="12" />
                            <SeparatorTemplate>
                                &gt;
                            </SeparatorTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdCustomerBranch" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCustomerBranch_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdCustomerBranch_Sorting"
                Font-Size="12px" OnRowCommand="grdCustomerBranch_RowCommand"
                OnPageIndexChanging="grdCustomerBranch_PageIndexChanging">
                <%--DataKeyNames="ID"--%>
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="TypeName" HeaderText="Type Name" SortExpression="TypeName" />
                    <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" HeaderStyle-Height="20px" ItemStyle-Height="25px" SortExpression="ContactPerson" />
                    <asp:BoundField DataField="EmailID" HeaderText="Email" SortExpression="EmailID" />
                    <asp:BoundField DataField="Landline" HeaderText="Landline" ItemStyle-HorizontalAlign="Center" SortExpression="Landline" />
                    <asp:BoundField DataField="Industry" HeaderText="Industry" Visible="false" SortExpression="Industry" />
                    <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Center" SortExpression="CreatedOn">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Entity">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>'>sub-entities</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Sub-entity" title="Edit Sub-entity" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER_BRANCH" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('This will also delete all the sub-entities associated with current entity. Are you certain you want to delete this entity?');"><img src="../Images/delete_icon.png" alt="Disable Sub-entity" title="Disable Sub-entity" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divCustomerBranchesDialog">
        <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="CustomerBranchValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; color: #333;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:Literal ID="litCustomer" runat="server" />
                    </div>
                    <div id="divParent" runat="server" style="margin-bottom: 7px; color: #333;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Parent</label>
                        <asp:Literal ID="litParent" runat="server" />
                    </div>
                   
                  <%--  <div class="clearfix" style="height: 21px;"></div>--%>
                    <div style="margin-bottom: 7px; margin-top: 7px; display:none;" class="float-container">
                        <div class="float-child">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                                Gst Details</label>
                            <asp:TextBox runat="server" ID="tbxGstNumber" CssClass="form-control" autocomplete="off" Style="width: 290px;" MaxLength="500" />
                        </div>
                        <div class="float-child1" id="divgst" runat="server">
                            <asp:Button Text="Get Details" runat="server" ID="btrgetGstdetails" OnClick="btrgetGstdetails_Click" CssClass="btn btn-primary" />
                        </div>
                    </div>
                   <%-- <div class="clearfix" style="height: 21px; "></div>--%>
                    <div style="margin-bottom: 7px; margin-top:7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 390px;" MaxLength="50" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()\/@#.,-]*$"></asp:RegularExpressionValidator>


                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Sub Entity Type</label>
                        <asp:DropDownList runat="server" ID="ddlType" Style="padding: 5px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select Sub Entity Type." ControlToValidate="ddlType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divCompanyType" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label id="lblType" style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlCompanyType" Style="padding: 5px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />

                    </div>
                    <div runat="server" id="divLegalEntityType" style="margin-bottom: 7px" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label id="lblLegalEntityType" style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Legal Entity Type</label>
                        <asp:DropDownList runat="server" ID="ddlLegalEntityType" Style="padding: 5px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />

                    </div>
                    <div runat="server" id="divLegalRelationship" style="margin-bottom: 7px" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Legal Relationship</label>
                        <asp:DropDownList runat="server" ID="ddlLegalRelationShip" Style="padding: 5px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select legal relationship."
                            ControlToValidate="ddlLegalRelationShip" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>

                    <div runat="server" id="divIndustry" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Industry
                        </label>
                        <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 5px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 260px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvIndustry">

                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                            <%--OnClick="btnRefresh_Click" --%>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>

                    </div>

                    <%--  <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label runat="server" id="lblLegalRelationShipOrStatus" style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Legal Status</label>
                        <asp:DropDownList runat="server" ID="ddlLegalRelationShipOrStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />

                    </div>--%>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Address Line 1</label>
                        <asp:TextBox runat="server" ID="tbxAddressLine1" Style="height: 16px; width: 390px;"
                            MaxLength="150" />
                        <asp:RequiredFieldValidator ErrorMessage="Address Line 1 can not be empty." ControlToValidate="tbxAddressLine1"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Address Line 2</label>
                        <asp:TextBox runat="server" ID="tbxAddressLine2" Style="height: 16px; width: 390px;"
                            MaxLength="150" />
                    </div>
                     <div runat="server" id="divState" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            State</label>
                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            City</label>
                        <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" />
                        <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divOther" style="margin-bottom: 7px">
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Others</label>
                        <asp:TextBox runat="server" ID="tbxOther" Style="height: 16px; width: 390px;" MaxLength="50" />
                        <asp:RequiredFieldValidator ErrorMessage="Other City Name can not be empty." ControlToValidate="tbxOther"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Pin Code</label>
                        <asp:TextBox runat="server" ID="tbxPinCode" Style="height: 16px; width: 390px;" MaxLength="6" />
                        <%--   <asp:RequiredFieldValidator ErrorMessage="Pin Code can not be empty." ControlToValidate="tbxPinCode"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid pin code." ControlToValidate="tbxPinCode"
                            ValidationExpression="^[0-9]{6}$"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Contact Person</label>
                        <asp:TextBox runat="server" ID="tbxContactPerson" Style="height: 16px; width: 390px;"
                            MaxLength="200" />
                        <asp:RequiredFieldValidator ErrorMessage="Contact Person name can not be empty."
                            ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid contact person name."
                            ControlToValidate="tbxContactPerson" ValidationExpression="^[a-zA-Z_ .-]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Landline No.</label>
                        <asp:TextBox runat="server" ID="tbxLandline" Style="height: 16px; width: 390px;"
                            MaxLength="15" />
                        <%--      <asp:RequiredFieldValidator ErrorMessage="Landline Number can not be empty." ControlToValidate="tbxLandline"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />--%>
                        <%--  <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid landline number." ControlToValidate="tbxLandline"
                            ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Mobile No.</label>
                        <asp:TextBox runat="server" ID="tbxMobile" Style="height: 16px; width: 390px;" MaxLength="15" />
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Mobile Number can not be empty."
                            ControlToValidate="tbxMobile" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            Display="None" />--%>
                        <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid mobile number."
                            ControlToValidate="tbxMobile" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Email</label>
                        <asp:TextBox runat="server" ID="tbxEmail" Style="height: 16px; width: 390px;" MaxLength="200" />
                        <asp:RequiredFieldValidator ErrorMessage="Email can not be empty." ControlToValidate="tbxEmail"
                            runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Status</label>
                        <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Person Responsible Applicable</label>
                        <asp:DropDownList runat="server" ID="ddlPersonResponsibleApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 257px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="CustomerBranchValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divCustomerBranchesDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divCustomerBranchesDialog').dialog({
                height: 650,
                width: 720,
                autoOpen: false,
                draggable: true,
                title: "Customer Branch Information",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
            $("#<%= ddlType.ClientID %>").combobox();
            $("#<%= ddlLegalRelationShip.ClientID %>").combobox();
           <%--  $("#<%= ddlLegalRelationShipOrStatus.ClientID %>").combobox();--%>
            $("#<%= ddlState.ClientID %>").combobox();
            $("#<%= ddlCity.ClientID %>").combobox();
            $("#<%= ddlCustomerStatus.ClientID %>").combobox();
            $("#<%= ddlLegalEntityType.ClientID %>").combobox();
            $("#<%= ddlCompanyType.ClientID %>").combobox();
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
</asp:Content>
