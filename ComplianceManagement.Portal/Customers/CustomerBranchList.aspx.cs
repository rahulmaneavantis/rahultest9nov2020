﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class CustomerBranchList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["CustomerID"] = Session["CustomerID"];
                    ViewState["ParentID"] = null;

                    BindCustomerBranches();
                    BindCustomerStatus();
                    BindIndustry();
                    BindStates();
                    if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT") || AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "DADMN"))
                    {
                        btnAddCustomerBranch.Visible = false;
                    }
                    else
                    {

                        int cid = Convert.ToInt32(ViewState["CustomerID"]);
                        int ServiceproviderID = 0;
                        bool chkserviceProviderId = checkISServiseProvider(Convert.ToInt32(cid));
                        if (chkserviceProviderId)
                        {
                            ServiceproviderID = (int)cid;
                        }
                        else
                        {
                            ServiceproviderID = GETServiseProviderID((int)cid);
                        }
                        if (AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            bool CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(ServiceproviderID, cid);
                            if (!CheckCustomerBranchLimit)
                            {
                                btnAddCustomerBranch.Enabled = false;
                                //btnAddCustomerBranch.Attributes["disabled"] = "disabled";
                                btnAddCustomerBranch.ToolTip = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                            }
                        }
                        else
                        {
                            bool CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(ServiceproviderID, cid);
                            if (!CheckCustomerBranchLimit)
                            {
                                btnAddCustomerBranch.Enabled = false;
                                //btnAddCustomerBranch.Attributes["disabled"] = "disabled";
                                btnAddCustomerBranch.ToolTip = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                            }
                        }                        
                    }
                    BindLegalEntityType();
                    BindCompanyTypeType();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex,MethodBase.GetCurrentMethod().DeclaringType.ToString(),MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    
                } 
            }
        }
        public bool checkISServiseProvider(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ServiceproviderId = false;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select (bool)cust.IsDistributor).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }

        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.ClearSelection();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var dataSource = CustomerBranchManagement.GetAllNodeTypes();
                if (ViewState["ParentID"] == null)
                {
                    dataSource = dataSource.Where(entry => entry.ID == 1).ToList();
                }
                ddlType.DataSource = dataSource;

                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindIndustry()
        {
            try
            {

                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindIndustry()
        //{
        //    try
        //    {
        //        ddlIndustry.DataTextField = "Name";
        //        ddlIndustry.DataValueField = "ID";

        //        ddlIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
        //        ddlIndustry.DataBind();
        //        ddlIndustry.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindCustomerBranches();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                long parentID = -1;
                //ddlLegalRelationShipOrStatus.Items.Clear();
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                    //BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                }
                else
                {
                    ViewState["IndustryId"] = null;
                }

                dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(Convert.ToInt32(ViewState["CustomerID"]), parentID);
                dlBreadcrumb.DataBind();

                grdCustomerBranch.DataSource = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                grdCustomerBranch.DataBind();
                upCustomerBranchList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                //if (ViewState["IndustryId"] != null)
                //{
                //    BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                //    //ddlIndustry.SelectedValue = ViewState["IndustryId"].ToString();
                //}
                //if (!divIndustry.Visible)
                //{
                //    //    ddlIndustry.SelectedIndex = -1;
                //    //    if (ViewState["IndustryId"] == null)
                //    //BindLegalRelationShipOrStatus(0);
                //}
                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }
                if(ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                    divCompanyType.Visible = true;
                }
                else
                {
                    divLegalEntityType.Visible = false;
                    divCompanyType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindLegalRelationShipOrStatus(Convert.ToInt32(ddlIndustry.SelectedValue));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void BindLegalRelationShipOrStatus(List<int> industryList)
        //{
        //    try
        //    {
        //        ddlLegalRelationShipOrStatus.DataSource = null;
        //        ddlLegalRelationShipOrStatus.DataBind();
        //        ddlLegalRelationShipOrStatus.ClearSelection();

        //        ddlLegalRelationShipOrStatus.DataTextField = "Name";
        //        ddlLegalRelationShipOrStatus.DataValueField = "ID";


        //        ddlLegalRelationShipOrStatus.DataSource = CustomerBranchManagement.GetAllLegalStatus(industryId, ddlType.SelectedValue.Length > 0 ? Convert.ToInt32(ddlType.SelectedValue) : -1);
        //        ddlLegalRelationShipOrStatus.DataBind();

        //        ddlLegalRelationShipOrStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private void BindCities()
        {
            try
            {
                ddlCity.DataSource = null;
                ddlCity.DataBind();
                ddlCity.ClearSelection();

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                ddlCity.DataBind();

                ddlCity.Items.Insert(0, new ListItem("< Other >", "0"));
                ddlCity.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = AddressManagement.GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stateid = 0;            
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue.ToString() != "-1")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int cid = Convert.ToInt32(ViewState["CustomerID"]);
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                        var users = (from row in entities.CustomerBranches
                                     where row.IsDeleted == false
                                     && row.CustomerID == cid
                                     && row.StateID == stateid
                                     select row).FirstOrDefault();
                        if (users != null)
                        {
                            divgst.Attributes.Add("style", "display:none");
                        }
                        else
                        {
                            divgst.Attributes.Add("style", "display:block");
                            tbxGstNumber.Text = string.Empty;
                        }
                    }
                }
            }


            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0")
                {
                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerBranchID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CustomerBranchID"] = customerBranchID;
                    txtIndustry.Text = "< Select >";
                    CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);

                    PopulateInputForm();

                    if (!string.IsNullOrEmpty(customerBranch.Name))
                    {
                        tbxName.Text = customerBranch.Name;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                    {
                        if (customerBranch.Type != -1)
                        {
                            ddlType.SelectedValue = customerBranch.Type.ToString();
                            ddlType_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                    {
                        if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                        {
                            ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                    {
                        if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                        {
                            ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                    {
                        if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                        {
                            ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                        }
                    }


                    var vGetIndustryMappedIDs = Business.ComplianceManagement.GetCustomerBranchIndustryMappedID(customerBranchID);

                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }
                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                        {
                            IndustrySelectAll.Checked = true;
                        }
                        else
                        {
                            IndustrySelectAll.Checked = false;
                        }
                    }

                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Industry)))
                    //{
                    //    if (customerBranch.Industry != -1)
                    //    {
                    //        //ddlIndustry.SelectedValue = (customerBranch.Industry).ToString();
                    //        //ddlIndustry_SelectedIndexChanged(null, null);

                    //        if (customerBranch.Industry == -1 || string.IsNullOrEmpty(customerBranch.Industry.ToString()))
                    //        {
                    //            int industryID = -1;
                    //            if (ViewState["IndustryId"] == null)
                    //            {
                    //                if (customerBranch.ParentID != null)//Added by Rahul on 9 FEB 2016
                    //                {
                    //                    industryID = Convert.ToInt32(CustomerBranchManagement.GetByID(Convert.ToInt32(customerBranch.ParentID)).Industry);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                industryID = Convert.ToInt32(ViewState["IndustryId"]);
                    //            }
                    //            BindLegalRelationShipOrStatus(industryID);
                    //        }
                    //    }
                    //}

                    //if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipOrStatus)))
                    //{
                    //    if (customerBranch.LegalRelationShipOrStatus != -1)
                    //    {
                    //        ddlLegalRelationShipOrStatus.SelectedValue = customerBranch.LegalRelationShipOrStatus != 0 ? customerBranch.LegalRelationShipOrStatus.ToString() : "-1";
                    //    }
                    //}                                        
                    tbxAddressLine1.Text = customerBranch.AddressLine1;
                    tbxAddressLine2.Text = customerBranch.AddressLine2;

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                    {
                        if (customerBranch.StateID != -1)
                        {
                            ddlState.SelectedValue = customerBranch.StateID.ToString();
                            ddlState_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                    {
                        if (customerBranch.CityID != -1)
                        {
                            ddlCity.SelectedValue = customerBranch.CityID.ToString();
                            ddlCity_SelectedIndexChanged(null, null);
                        }
                    }

                    tbxOther.Text = customerBranch.Others;
                    tbxPinCode.Text = customerBranch.PinCode;

                    //tbxIndustry.Text = customerBranch.Industry;
                    tbxContactPerson.Text = customerBranch.ContactPerson;
                    tbxLandline.Text = customerBranch.Landline;
                    tbxMobile.Text = customerBranch.Mobile;
                    tbxEmail.Text = customerBranch.EmailID;
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                    {
                        if (customerBranch.Status != -1)
                        {
                            ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                    {
                        if (customerBranch.AuditPR == false)
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                        }
                        else
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                        }
                    }
                    //ddlIndustry.Enabled = false;
                    ddlType.Enabled = false;
                    //ddlLegalRelationShipOrStatus.Enabled = false;
                    ddlLegalRelationShip.Enabled = false;

                    upCustomerBranches.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {

                    if (CustomerManagement.CustomerBranchDelete(customerBranchID))
                    {
                        CustomerBranchManagement.Delete(customerBranchID);
                        CustomerBranchManagementRisk.Delete(customerBranchID);
                        BindCustomerBranches();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('CustomerBranch is associated with assigned compliance, can not be deleted')", true);
                    }
                                   
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {
                        ViewState["ParentID"] = customerBranchID;
                        if (ViewState["IndustryId"] == null)
                        {
                            ViewState["IndustryId"] = CustomerBranchManagement.GetIndustryIDByCustomerId(customerBranchID);
                        }

                        BindCustomerBranches();                     
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                //ddlIndustry.Enabled = true;
                ddlType.Enabled = true;
                //ddlLegalRelationShipOrStatus.Enabled = true;
                ddlLegalRelationShip.Enabled = true;

                tbxName.Text = tbxAddressLine1.Text = tbxAddressLine2.Text = tbxOther.Text = tbxPinCode.Text = tbxContactPerson.Text = tbxLandline.Text = tbxMobile.Text = tbxEmail.Text = string.Empty;
                PopulateInputForm();

                //ddlIndustry.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlType_SelectedIndexChanged(null, null);

                ddlCompanyType.SelectedValue = "-1";

                ddlState.SelectedValue = "-1";
                ddlState_SelectedIndexChanged(null, null);
                ddlCity_SelectedIndexChanged(null, null);

                ddlCustomerStatus.SelectedIndex = -1;

                txtIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                BindBranchTypes();
                BindLegalRelationShips(ViewState["ParentID"] == null);
                divParent.Visible = ViewState["ParentID"] != null;

                litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(ViewState["CustomerID"])).Name;

                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int? GetServiseProviderId(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int? ServiceproviderId = 0;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select cust.ServiceProviderID).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    if (ddlType.SelectedValue == "1")
                    {
                        if (ddlCompanyType.SelectedValue == "-1")
                        {
                            cvDuplicateEntry.ErrorMessage = "Please select Type.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "Please select Type.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                Boolean chkIndustryFlag = false;
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    if (chkIndustry.Checked)
                    {
                        chkIndustryFlag = true;
                    }
                }

                if (chkIndustryFlag == false)
                {
                    cvDuplicateEntry.ErrorMessage = "Please select at least one industry.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }


                int comType = 0;
                if (!string.IsNullOrEmpty(ddlCompanyType.SelectedValue))
                {
                    if (ddlCompanyType.SelectedValue.ToString() == "-1")
                    {
                        comType = 0;
                    }
                    else
                    {
                        comType = Convert.ToInt32(ddlCompanyType.SelectedValue);
                    }
                }


                #region 
                CustomerBranch customerBranch = new CustomerBranch()
                {
                    Name = tbxName.Text.Trim(),
                    Type = Convert.ToByte(ddlType.SelectedValue),
                    ComType = Convert.ToByte(comType),
                    AddressLine1 = tbxAddressLine1.Text,
                    AddressLine2 = tbxAddressLine2.Text,
                    StateID = Convert.ToInt32(ddlState.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    Others = tbxOther.Text,
                    PinCode = tbxPinCode.Text,
                    //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                    ContactPerson = tbxContactPerson.Text,
                    Landline = tbxLandline.Text,
                    Mobile = tbxMobile.Text,
                    EmailID = tbxEmail.Text,
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };
                if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                {
                    customerBranch.AuditPR = true;
                }
                if (ddlType.SelectedValue == "1")
                {
                    if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                    {
                        customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                    {
                        customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                    }
                }
                else
                {
                    customerBranch.LegalRelationShipID = null;
                    customerBranch.LegalEntityTypeID = null;
                }

                //if (!string.IsNullOrEmpty(ddlLegalRelationShipOrStatus.SelectedValue))
                //{
                //    if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                //        customerBranch.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                //    else
                //        customerBranch.LegalRelationShipOrStatus = 0;
                //}
                //else
                //{
                //    customerBranch.LegalRelationShipOrStatus = 0;
                //}
                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                }

                if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    CustomerBranchManagement.Create(customerBranch);

                    // Added by SACHIN 28 April 2016
                    string ReplyEmailAddressName = "Avantis";
                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                        .Replace("@NewCustomer", litCustomer.Text)
                                        .Replace("@BranchName", tbxName.Text)
                                        .Replace("@LoginUser", AuthenticationHelper.User)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer branch added.", message);

                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerBranchManagement.Update(customerBranch);
                }

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                {
                    Name = tbxName.Text.Trim(),
                    Type = Convert.ToByte(ddlType.SelectedValue),
                    ComType = Convert.ToByte(comType),
                    AddressLine1 = tbxAddressLine1.Text,
                    AddressLine2 = tbxAddressLine2.Text,
                    StateID = Convert.ToInt32(ddlState.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    Others = tbxOther.Text,
                    PinCode = tbxPinCode.Text,
                    //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                    ContactPerson = tbxContactPerson.Text,
                    Landline = tbxLandline.Text,
                    Mobile = tbxMobile.Text,
                    EmailID = tbxEmail.Text,
                    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                };
                if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                {
                    customerBranch1.AuditPR = true;
                }
                if (ddlType.SelectedValue == "1")
                {
                    if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                    {
                        customerBranch1.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                    {
                        customerBranch1.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                    }
                }
                else
                {
                    customerBranch1.LegalRelationShipID = null;
                    customerBranch1.LegalEntityTypeID = null;
                }
                //if (!string.IsNullOrEmpty(ddlLegalRelationShipOrStatus.SelectedValue))
                //{
                //    if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                //        customerBranch1.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                //    else
                //        customerBranch1.LegalRelationShipOrStatus = 0;
                //}
                //else
                //{
                //    customerBranch1.LegalRelationShipOrStatus = 0;
                //}

                if ((int)ViewState["Mode"] == 1)
                {
                    customerBranch1.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                }

                if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(ViewState["CustomerID"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    CustomerBranchManagement.Create1(customerBranch1);

                    //---------add Industry--------------------------------------------
                    List<int> IndustryIds = new List<int>();
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        if (chkIndustry.Checked)
                        {
                            IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                            CustomerBranchIndustryMapping IndustryMapping = new CustomerBranchIndustryMapping()
                            {
                                CustomerBranchID = customerBranch1.ID,
                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),
                            };
                            Business.ComplianceManagement.CreateCustomerBranchIndustryMapping(IndustryMapping);
                        }
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    CustomerBranchManagement.Update1(customerBranch1);

                    //---------add Industry--------------------------------------------
                    List<int> IndustryIds = new List<int>();
                    Business.ComplianceManagement.UpdateCustomerBranchIndustryMappingMappedID(customerBranch1.ID);
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        if (chkIndustry.Checked)
                        {
                            IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                            CustomerBranchIndustryMapping IndustryMapping = new CustomerBranchIndustryMapping()
                            {
                                CustomerBranchID = customerBranch1.ID,
                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),
                            };

                            Business.ComplianceManagement.CreateCustomerBranchIndustryMapping(IndustryMapping);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divCustomerBranchesDialog\").dialog('close')", true);
                BindCustomerBranches();
                upCustomerBranches.Update();
                #endregion


                int cid = Convert.ToInt32(ViewState["CustomerID"]);
                int ServiceproviderID = 0;
                bool chkserviceProviderId = checkISServiseProvider(Convert.ToInt32(cid));
                if (chkserviceProviderId)
                {
                    ServiceproviderID = (int)cid;
                }
                else
                {
                    ServiceproviderID = GETServiseProviderID((int)cid);
                }
                if (AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT"))
                {
                    bool CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(ServiceproviderID, cid);
                    if (!CheckCustomerBranchLimit)
                    {
                        btnAddCustomerBranch.Enabled = false;
                        //btnAddCustomerBranch.Attributes["disabled"] = "disabled";
                        btnAddCustomerBranch.ToolTip = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
                else
                {
                    bool CheckCustomerBranchLimit = ICIAManagement.CheckCustoemrBranchLimit(ServiceproviderID, cid);
                    if (!CheckCustomerBranchLimit)
                    {
                        btnAddCustomerBranch.Enabled = false;
                        //btnAddCustomerBranch.Attributes["disabled"] = "disabled";
                        btnAddCustomerBranch.ToolTip = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                    }
                }
                        
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int GETServiseProviderID(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ServiceproviderId = -1;
                try
                {
                    ServiceproviderId = (from cust in entities.Customers
                                         where cust.ID == CustomerId
                                         select  (int)cust.ServiceProviderID).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
                return ServiceproviderId;
            }
        }
        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdCustomerBranch_OnPreRender(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (AuthenticationHelper.Role.Equals("CADMN"))
        //        {
        //            foreach (DataControlField column in grdCustomerBranch.Columns)
        //            {
        //                if (column.HeaderText == "")
        //                    column.Visible = false;
        //            }

        //            grdCustomerBranch.Columns[4].HeaderText = string.Empty;
        //        }
               
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomerBranch_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                var customerBranchList = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerBranchList = customerBranchList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerBranchList = customerBranchList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomerBranch.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomerBranch.Columns.IndexOf(field);
                    }
                }

                grdCustomerBranch.DataSource = customerBranchList;
                grdCustomerBranch.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {

                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //added by Manisha
        private void BindLegalEntityType()
        {
            try
            {
               
                ddlLegalEntityType.DataTextField = "EntityTypeName";
                ddlLegalEntityType.DataValueField = "ID";
                ddlLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                ddlLegalEntityType.DataBind();
                ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompanyTypeType()
        {
            try
            {
                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyTypeCustomerBranch();
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static string Invoke(string Method, string Uri, string Body)
        {
            HttpClient cl = new HttpClient();
            cl.BaseAddress = new Uri(Uri);
            int _TimeoutSec = 90;
            cl.Timeout = new TimeSpan(0, 0, _TimeoutSec);
            string _ContentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            //var authValue = "5E8E3D45-172D-4D58-8DF8-EB0B0E";
            //cl.DefaultRequestHeaders.Add("Authorization", String.Format("{0}", authValue));
            //var _UserAgent = "f2UKmAm0KlI98bEYGGxEHQ==";
            //// You can actually also set the User-Agent via a built-in property
            //cl.DefaultRequestHeaders.Add("X-User-Id-1", _UserAgent);
            //// You get the following exception when trying to set the "Content-Type" header like this:
            //// cl.DefaultRequestHeaders.Add("Content-Type", _ContentType);
            //// "Misused header name. Make sure request headers are used with HttpRequestMessage, response headers with HttpResponseMessage, and content headers with HttpContent objects."

            HttpResponseMessage response;
            var _Method = new HttpMethod(Method);

            switch (_Method.ToString().ToUpper())
            {
                case "GET":
                case "HEAD":
                    // synchronous request without the need for .ContinueWith() or await
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    response = cl.GetAsync(Uri).Result;
                    break;
                case "POST":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "POSTJson":
                    {


                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PostAsync(Uri, _Body).Result;
                    }
                    break;
                case "PUT":
                    {
                        // Construct an HttpContent from a StringContent
                        HttpContent _Body = new StringContent(Body);
                        // and add the header to this object instance
                        // optional: add a formatter option to it as well
                        _Body.Headers.ContentType = new MediaTypeHeaderValue(_ContentType);
                        // synchronous request without the need for .ContinueWith() or await
                        response = cl.PutAsync(Uri, _Body).Result;
                    }
                    break;
                case "DELETE":
                    response = cl.DeleteAsync(Uri).Result;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            // either this - or check the status to retrieve more information
            string responseContent = string.Empty;

            if (response.IsSuccessStatusCode)
                // get the rest/content of the response in a synchronous way
                responseContent = response.Content.ReadAsStringAsync().Result;

            return responseContent;
        }

        #region 
        public class Compliance
        {
            public object filingFrequency { get; set; }
        }

        public class Addr
        {
            public string st { get; set; }
            public string lg { get; set; }
            public string stcd { get; set; }
            public string pncd { get; set; }
            public string bno { get; set; }
            public string flno { get; set; }
            public string dst { get; set; }
            public string loc { get; set; }
            public string bnm { get; set; }
            public string lt { get; set; }
            public string city { get; set; }
        }

        public class Pradr
        {
            public string ntr { get; set; }
            public Addr addr { get; set; }
        }

        public class TaxpayerInfo
        {
            public object errorMsg { get; set; }
            public string ctj { get; set; }
            public Pradr pradr { get; set; }
            public string cxdt { get; set; }
            public string sts { get; set; }
            public string gstin { get; set; }
            public string ctjCd { get; set; }
            public string ctb { get; set; }
            public string stj { get; set; }
            public string dty { get; set; }
            public List<object> adadr { get; set; }
            public object frequencyType { get; set; }
            public string lgnm { get; set; }
            public List<string> nba { get; set; }
            public string rgdt { get; set; }
            public string stjCd { get; set; }
            public string tradeNam { get; set; }
            public string panNo { get; set; }
        }

        public class Root
        {
            public Compliance compliance { get; set; }
            public TaxpayerInfo taxpayerInfo { get; set; }
            public List<object> filing { get; set; }
        }

        #endregion
        protected void btrgetGstdetails_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(tbxGstNumber.Text))
                {
                    //string postURL = "https://appyflow.in/api/verifyGST?gstNo=27BHLPS8412D1Z5&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string postURL = "https://appyflow.in/api/verifyGST?gstNo=" + tbxGstNumber.Text.Trim() + "&key_secret=EKaYL1L0AFUtZ50Pkl7LMVdWDYG2";
                    string responseData = Invoke("GET", postURL, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var myDeserializedClass = JsonConvert.DeserializeObject<Root>(responseData);

                        var panno = myDeserializedClass.taxpayerInfo.panNo;
                        var tradeNam = myDeserializedClass.taxpayerInfo.tradeNam;
                        var address = myDeserializedClass.taxpayerInfo.pradr.addr.bnm + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.lt + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.st + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.bno + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.dst + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.loc + " " +
                                    myDeserializedClass.taxpayerInfo.pradr.addr.pncd;

                        tbxName.Text = tradeNam;
                        tbxAddressLine1.Text = address;                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}