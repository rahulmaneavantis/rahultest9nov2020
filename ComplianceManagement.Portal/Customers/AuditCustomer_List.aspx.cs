﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class AuditCustomer_List : System.Web.UI.Page
    {
        //protected override void OnPreInit(EventArgs e)
        //{
        //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
        //        this.MasterPageFile = "~/LitigationMaster.Master";
        //    else
        //        this.MasterPageFile = "~/AuditTool.Master";

        //    base.OnPreInit(e);
        //}

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                this.MasterPageFile = "~/AuditTool.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["CustomerID"] = null;
                BindCustomers();
                bindPageNumber();
                //GetPageDisplaySummary();
                //BindIndustries();
                if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT")))
                {
                    btnAddCustomer.Visible = false;
                    divFilter.Visible = false;
                    btnSendNotification.Visible = false;
                }

                ViewState["AssignedCompliancesID"] = null;
            }
         
            udcInputForm.OnSaved += (inputForm, args) => { BindCustomers(); };
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCustomer.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
            int customerID = -1;
            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            //BindVerticalData(customerID);
            BindCustomers();
        }


        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                  var aa=  UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                  if (aa=="AM" || aa=="AH")
                  {
                      customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                  }
                }

                var customerData = CustomerManagement.GetAllAuditCustomer(customerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var customerInfo in customerData)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }
                grdCustomer.DataSource = dataSource;
                Session["TotalRows"] = customerData.Count;
                grdCustomer.DataBind();

                upCustomerList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    // TBD
                    udcInputForm.EditCustomerInformation(customerID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    CustomerManagement.Delete(customerID);
                    CustomerManagementRisk.Delete(customerID);
                    BindCustomers();
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["CustomerID"] = customerID;
                    Session["ParentID"] = null;
                    Response.Redirect("~/Customers/AuditCustomerBranch_List.aspx", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdCustomer.PageIndex = e.NewPageIndex;
                BindCustomers();
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            // TBD
            udcInputForm.AddCustomer();
            BindCustomers();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;
                BindCustomers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomer_OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role.Equals("CADMN"))
                {
                    foreach (DataControlField column in grdCustomer.Columns)
                    {
                        if (column.HeaderText == "")
                            column.Visible = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var customerlist = CustomerManagement.GetAllAuditCustomer(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    customerlist = customerlist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    customerlist = customerlist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdCustomer.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomer.Columns.IndexOf(field);
                    }
                }

                grdCustomer.DataSource = customerlist;
                grdCustomer.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void grdCustomer_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}

        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;

        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}

        //added by sudarshan for sending mail for server down

        private void CheckBoxValueSaved()
        {
            List<int> chkList = new List<int>();
            int index = -1;
            foreach (GridViewRow gvrow in grdCustomer.Rows)
            {
                index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCustomer")).Checked;

                if (ViewState["CustomerID"] != null)
                    chkList = (List<int>)ViewState["CustomerID"];

                if (result)
                {
                    if (!chkList.Contains(index))
                        chkList.Add(index);
                }
                else
                    chkList.Remove(index);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["CustomerID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<int> chkList = (List<int>)ViewState["CustomerID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdCustomer.Rows)
                {
                    int index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                    if (chkList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCustomer");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {
                upServerDownTimeDetails.Update();
                ddlStartTime.Items.Clear();
                ddlEndTime.Items.Clear();
                SetTime(ddlStartTime);
                SetTime(ddlEndTime);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('open');", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);


                

                //CheckBoxValueSaved();
                //List<int> chkList = (List<int>)ViewState["CustomerID"];
                //if (chkList != null)
                //{
                //    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                //    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                //    string ReplyEmailAddressName = "Avantis";

                //    foreach (User user in userList)
                //    {
                //        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                //        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                                .Replace("@User", username)
                //                                .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                                .Replace("@Password", "")
                //                                .Replace("@From", ReplyEmailAddressName);

                //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM account created.", message);
                //    }
                
                //}
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upServerDownTimeDetails_Load(object sender, EventArgs e)
        {
            try
            {
               
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", string.Format("initializeDowntimeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDowntimeDatePicker", "initializeDowntimeDatePicker(null);", true);
                }

               

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SetTime(DropDownList DDl)
        {
            DDl.Items.Add(new ListItem("Select", "-1"));
            for (int i = 0; i <=12; i++)
            {
                if (i != 12)
                DDl.Items.Add(new ListItem(i.ToString("00") + " AM", i + " AM"));
                else
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }

            for (int i = 1; i <= 11; i++)
            {
                DDl.Items.Add(new ListItem(i.ToString("00") + " PM", i + " PM"));
            }
        }

        protected void btnSend_click(object sender, EventArgs e)
        {
            new Thread(() => { SendNotification(); }).Start();
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenNotificationTime", "$(\"#divNotificationTime\").dialog('close');", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:caller()", true);
        }

        private void SendNotification()
        {
            try
            {

                string dated = txtDate.Text;
                string starttime = ddlStartTime.SelectedValue;
                string endtime = ddlEndTime.SelectedValue;

                CheckBoxValueSaved();
                List<int> chkList = (List<int>)ViewState["CustomerID"];
                if (chkList != null)
                {
                    List<User> userList = UserManagement.GetAll(-1).Where(entry => entry.CustomerID != null).ToList();
                    userList = userList.Where(entry => chkList.Contains((int)entry.CustomerID)).ToList();
                    string ReplyEmailAddressName = "Avantis";

                    foreach (User user in userList)
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ServerDown
                                                .Replace("@User", username)
                                                .Replace("@Dated", dated)
                                                .Replace("@StartTime", starttime)
                                                .Replace("@EndTime", endtime)
                                                .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                .Replace("@From", ReplyEmailAddressName);

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "Avantis Audit Product - server down.", message);
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////if (!IsValid()) { return; };
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdCustomer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindCustomers();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCustomer.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdCustomer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid                
        //        BindCustomers();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdCustomer.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdCustomer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindCustomers();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        private bool IsValid()
        {
            try
            {
                return true;
                //if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                //{
                //    SelectedPageNo.Text = "1";
                //    return false;
                //}
                //else if (!IsNumeric(SelectedPageNo.Text))
                //{
                //    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}