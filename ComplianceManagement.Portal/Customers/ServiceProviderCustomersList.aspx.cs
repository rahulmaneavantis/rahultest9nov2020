﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Customers
{
    public partial class ServiceProviderCustomersList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        Session["CustomerID"] = null;
                        BindServiceProviders();
                        if (!(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT") || AuthenticationHelper.Role.Equals("CADMN")))
                        {
                            btnAddCustomer.Visible = false;
                            divFilter.Visible = false;
                            btnSendNotification.Visible = false;
                            lbtnExportExcel.Visible = true;
                        }
                        ViewState["AssignedCompliancesID"] = null;
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }

                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }

            }

            udcInputForm.OnSaved += (inputForm, args) => { BindServiceProviders(); txtHideReportButton.Text = "No"; };

            if (!String.IsNullOrEmpty(Convert.ToString(Session["CustomerID"])))
            {
                int serviceProviderCustomerId = Convert.ToInt32(Session["CustomerID"].ToString());
                udcInputForm.OnSaved += (inputForm, args) => { BindCustomers(serviceProviderCustomerId); txtHideReportButton.Text = "Yes"; };
            }
        }

        protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                int custId = -1;
                string serviceProviderName = string.Empty;
                string serviceProviderNameFromSession = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["CustomerID"])))
                {
                    custId = Convert.ToInt32(Session["CustomerID"].ToString());
                }
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                customerID = Convert.ToInt32(commandArgs[0]);
                serviceProviderName = Convert.ToString(commandArgs[1]);
                Session["serviceProviderName"] = serviceProviderName;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["serviceProviderName"])))
                {
                    serviceProviderNameFromSession = Session["serviceProviderName"].ToString();
                }
                if (e.CommandName.Equals("EDIT_CUSTOMER"))
                {
                    // TBD
                    udcInputForm.EditCustomerInformation(customerID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                else if (e.CommandName.Equals("VIEW_CUSTOMER"))
                {
                    ServiceProviderCustomerLimit serviceProviderCustomerLimit = CustomerManagement.GetServiceProviderCustomerLimit(customerID);
                    if (serviceProviderCustomerLimit != null)
                    {
                        lblServiceProviderName.Text = serviceProviderNameFromSession;
                        if (serviceProviderCustomerLimit.CustomerLimit == 0)
                            txtCustomerLimit.Text = string.Empty;
                        else
                            txtCustomerLimit.Text = Convert.ToString(serviceProviderCustomerLimit.CustomerLimit);

                        if (serviceProviderCustomerLimit.UserLimit == 0)
                            txtUserLimit.Text = string.Empty;
                        else
                            txtUserLimit.Text = Convert.ToString(serviceProviderCustomerLimit.UserLimit);

                        if (serviceProviderCustomerLimit.ParentBranchLimit == 0)
                            txtParentBranchLimit.Text = string.Empty;
                        else
                            txtParentBranchLimit.Text = Convert.ToString(serviceProviderCustomerLimit.ParentBranchLimit);

                        if (serviceProviderCustomerLimit.BranchLimit == 0)
                            txtBranchLimit.Text = string.Empty;
                        else
                            txtBranchLimit.Text = Convert.ToString(serviceProviderCustomerLimit.BranchLimit);

                        if (serviceProviderCustomerLimit.EmployeeLimit == 0)
                            txtEmployeeLimit.Text = string.Empty;
                        else
                            txtEmployeeLimit.Text = Convert.ToString(serviceProviderCustomerLimit.EmployeeLimit);


                        txthiddenServiceProviderId.Text = Convert.ToString(serviceProviderCustomerLimit.ServiceProviderID);
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyDepartment\").dialog('open');", true);
                    upModifyDepartment.Update();
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER"))
                {
                    if (CustomerManagement.CustomerDelete(customerID))
                    {
                        CustomerManagement.Delete(customerID);
                        CustomerManagementRisk.Delete(customerID);
                        BindServiceProviders();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Customer is associated with assigned compliance, can not be deleted')", true);
                    }
                }
                else if (e.CommandName.Equals("VIEW_COMPANIES"))
                {
                    Session["CustomerID"] = customerID;
                    Session["serviceProviderName"] = serviceProviderName;
                    Session["ParentID"] = null;
                    Session["HideViewButton"] = true;
                    txthiddenServiceProviderIdForReport.Text = Convert.ToString(customerID);
                    linkbtnServiceProviderName.Text = serviceProviderName;
                    if (customerID != -1 && customerID != 0)
                    {
                        tbxFilter.Text = string.Empty;
                        BindCustomers(customerID);
                        lbtnExportExcel.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(txtHideReportButton.Text))
                    {
                        if (txtHideReportButton.Text == "No")
                        {
                            lbtnExportExcel.Visible = true;
                            testDiv.Style.Remove("display");
                            lbtnExportExcel.Style.Remove("display");
                        }
                        else
                        {
                            lbtnExportExcel.Visible = false;
                            testDiv.Style.Add("display", "none");
                            lbtnExportExcel.Style.Add("display", "none");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdCustomer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            bool HideViewButton = Convert.ToBoolean(Session["HideViewButton"]);
            if (HideViewButton)
            {
                LinkButton LinkButton3 = (LinkButton)e.Row.FindControl("LinkButton3");
                if (LinkButton3 != null)
                {
                    LinkButton3.Visible = false;
                    LinkButton3.Attributes.Add("class", "HideClass");
                }
            }
            else
            {
                LinkButton LinkButton3 = (LinkButton)e.Row.FindControl("LinkButton3");
                if (LinkButton3 != null)
                {
                    LinkButton3.Visible = true;
                    //LinkButton3.Attributes.Add("class", "");
                    LinkButton3.Attributes.Remove("class");
                }
            }

            LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("LinkButton2");
            if (LinkButton2 != null)
            {
                LinkButton2.Visible = false;
            }
        }

        public void BindServiceProviders()
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    var aa = UserManagement.GetByID(AuthenticationHelper.UserID).IsAuditHeadOrMgr ?? null;
                    if (aa == "AM" || aa == "AH")
                    {
                        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    }
                }
                //if (AuthenticationHelper.Role.Equals("CADMN"))
                //{
                lbtnExportExcel.Visible = true;
                //}
                //else
                //{
                //    lbtnExportExcel.Visible = false;
                //}


                var customerData = CustomerManagement.GetAllServiceProviderCustomers(customerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var customerInfo in customerData)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }
                Session["HideViewButton"] = false;
                grdCustomer.DataSource = dataSource;
                grdCustomer.DataBind();                
                txtHideReportButton.Text = "No";
                lbtnExportExcel.Visible = true;
                upCustomerList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindCustomers(int customerID)
        {
            try
            {
                var customerData = CustomerManagement.GetAllCustomersAgainstServiceProvider(customerID, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var customerInfo in customerData)
                {
                    dataSource.Add(new
                    {
                        customerInfo.ID,
                        customerInfo.Name,
                        customerInfo.Address,
                        customerInfo.Industry,
                        customerInfo.BuyerName,
                        customerInfo.BuyerContactNumber,
                        customerInfo.BuyerEmail,
                        customerInfo.CreatedOn,
                        customerInfo.IsDeleted,
                        customerInfo.StartDate,
                        customerInfo.EndDate,
                        customerInfo.DiskSpace,
                        Status = Enumerations.GetEnumByID<CustomerStatus>(Convert.ToInt32(customerInfo.Status != null ? (int)customerInfo.Status : -1)),
                    });
                }
                grdCustomer.DataSource = dataSource;
                grdCustomer.DataBind();
                Session["HideViewButton"] = true;
                grdCustomer.Columns[9].Visible = false;//  customer link button from gridview
                txtHideReportButton.Text = "Yes";
                lbtnExportExcel.Visible = false;
                upCustomerList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomer.PageIndex = 0;

                if (!string.IsNullOrEmpty(Convert.ToString(Session["CustomerID"])))
                {
                    int customerID = Convert.ToInt32(Session["CustomerID"]);
                    BindCustomers(customerID);
                    lbtnExportExcel.Visible = false;
                    Session["HideViewButton"] = true;
                }
                else
                {
                    BindServiceProviders();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceProviderCustomerLimit serviceProviderCustomerLimit = new ServiceProviderCustomerLimit();
                List<string> errorMessage = new List<string>();


                if (!string.IsNullOrEmpty(txtCustomerLimit.Text))
                {
                    bool check;
                    int a;
                    check = int.TryParse(txtCustomerLimit.Text.Trim(), out a);
                    if (check)
                    {
                        serviceProviderCustomerLimit.CustomerLimit = Convert.ToInt32(txtCustomerLimit.Text.Trim());
                    }
                    else
                    {
                        errorMessage.Add("Customer limit should be number");
                    }

                    if (serviceProviderCustomerLimit.CustomerLimit < 0)
                    {
                        errorMessage.Add("Customer limit should be greater than or equal to 0");
                    }
                }
                else
                {
                    errorMessage.Add("Please enter customer limit.");
                }

                if (!string.IsNullOrEmpty(txtUserLimit.Text))
                {
                    bool check;
                    int a;
                    check = int.TryParse(txtUserLimit.Text.Trim(), out a);
                    if (check)
                    {
                        serviceProviderCustomerLimit.UserLimit = Convert.ToInt32(txtUserLimit.Text.Trim());
                    }
                    else
                    {
                        errorMessage.Add("User limit should be number");
                    }
                    if (serviceProviderCustomerLimit.UserLimit < 0)
                    {
                        errorMessage.Add("User limit should be greater than or equal to 0");
                    }
                }
                else
                {
                    errorMessage.Add("Please enter user limit.");
                }

                if (!string.IsNullOrEmpty(txtParentBranchLimit.Text))
                {
                    bool check;
                    int a;
                    check = int.TryParse(txtParentBranchLimit.Text.Trim(), out a);
                    if (check)
                    {
                        serviceProviderCustomerLimit.ParentBranchLimit = Convert.ToInt32(txtParentBranchLimit.Text.Trim());
                    }
                    else
                    {
                        errorMessage.Add("Parent branch limit should be number");
                    }
                    if (serviceProviderCustomerLimit.ParentBranchLimit < 0)
                    {
                        errorMessage.Add("Parent branch limit greater than or equal to 0");
                    }
                }
                else
                {
                    errorMessage.Add("Please enter parent branch limit.");
                }

                if (!string.IsNullOrEmpty(txtBranchLimit.Text))
                {
                    bool check;
                    int a;
                    check = int.TryParse(txtBranchLimit.Text.Trim(), out a);
                    if (check)
                    {
                        serviceProviderCustomerLimit.BranchLimit = Convert.ToInt32(txtBranchLimit.Text.Trim());
                    }
                    else
                    {
                        errorMessage.Add("Branch limit should be number");
                    }
                    if (serviceProviderCustomerLimit.BranchLimit < 0)
                    {
                        errorMessage.Add("Branch limit should be greater than or equal to 0");
                    }
                }
                else
                {
                    errorMessage.Add("Please enter branch limit.");
                }

                if (!string.IsNullOrEmpty(txtEmployeeLimit.Text))
                {
                    bool check;
                    int a;
                    check = int.TryParse(txtEmployeeLimit.Text.Trim(), out a);
                    if (check)
                    {
                        serviceProviderCustomerLimit.EmployeeLimit = Convert.ToInt32(txtEmployeeLimit.Text.Trim());
                    }
                    else
                    {
                        errorMessage.Add("Employee limit should be number");
                    }
                    if (serviceProviderCustomerLimit.EmployeeLimit < 0)
                    {
                        errorMessage.Add("Employee limit should be greater than or equal to 0");
                    }
                }
                else
                {
                    errorMessage.Add("Please enter employee limit.");
                }

                if (errorMessage.Count > 0)
                {
                    ErrorMessages(errorMessage);
                    return;
                }

                serviceProviderCustomerLimit.ServiceProviderID = Convert.ToInt32(txthiddenServiceProviderId.Text);

                bool result = CustomerManagement.UpdateCustomerBranchLimits(serviceProviderCustomerLimit);
                if (result)
                {
                    CustomServiceProviderCustomerLimit.IsValid = false;
                    CustomServiceProviderCustomerLimit.ErrorMessage = "Service Provider Customer Limit updated successfully";
                    BindServiceProviders();
                }

                upModifyDepartment.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            CustomServiceProviderCustomerLimit.IsValid = false;
            CustomServiceProviderCustomerLimit.ErrorMessage = finalErrMsg;
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            udcInputForm.AddCustomer();
            BindServiceProviders();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

        }

        protected void grdCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdCustomer.PageIndex = e.NewPageIndex;
                BindServiceProviders();
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void CheckBoxValueSaved()
        {
            List<int> chkList = new List<int>();
            int index = -1;
            foreach (GridViewRow gvrow in grdCustomer.Rows)
            {
                index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCustomer")).Checked;

                if (ViewState["CustomerID"] != null)
                    chkList = (List<int>)ViewState["CustomerID"];

                if (result)
                {
                    if (!chkList.Contains(index))
                        chkList.Add(index);
                }
                else
                    chkList.Remove(index);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["CustomerID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<int> chkList = (List<int>)ViewState["CustomerID"];
            if (chkList != null && chkList.Count > 0)
            {

                foreach (GridViewRow gvrow in grdCustomer.Rows)
                {
                    int index = Convert.ToInt32(grdCustomer.DataKeys[gvrow.RowIndex].Value);
                    if (chkList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCustomer");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                int serviceProviderId = -1;
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Customer List");
                    DataTable ExcelData = null;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var compliancesData = (from row in entities.sp_GetServiceProviderAgainstCustomerLimit(serviceProviderId)
                                               select row).ToList();

                        DataTable table = compliancesData.AsDataTable();

                        DataView view = new System.Data.DataView(table);
                        List<sp_GetServiceProviderAgainstCustomerLimit_Result> cb = new List<sp_GetServiceProviderAgainstCustomerLimit_Result>();
                        ExcelData = view.ToTable("Selected", false, "ServiceProvider", "ContactNumber", "BuyerEmail", "BuyerName", "TotalCustomers", "TotalBranches", "TotalUsers", "ParentBranchLimit");

                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "Service Provider";
                        exWorkSheet.Cells["A1"].AutoFitColumns(40);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "Contact Number";
                        exWorkSheet.Cells["B1"].AutoFitColumns(15);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Buyer Email";
                        exWorkSheet.Cells["C1"].AutoFitColumns(35);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Buyer Name";
                        exWorkSheet.Cells["D1"].AutoFitColumns(35);

                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "Total Customers";
                        exWorkSheet.Cells["E1"].AutoFitColumns(15);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "Total Branches";
                        exWorkSheet.Cells["F1"].AutoFitColumns(15);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "Total Users";
                        exWorkSheet.Cells["G1"].AutoFitColumns(15);

                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H1"].Value = "Parent Branch Limit";
                        exWorkSheet.Cells["H1"].AutoFitColumns(15);

                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 8])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ServiceProviderAgainstCustomer.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void linkbtnServiceProviderName_Click(object sender, EventArgs e)
        {
            grdCustomer.PageIndex = 0;
            Session["HideViewButton"] = false;
            BindServiceProviders();
            if (!string.IsNullOrEmpty(Convert.ToString(Session["serviceProviderName"])))
            {
                tbxFilter.Text = Session["serviceProviderName"].ToString();
            }
            linkbtnServiceProviderName.Text = string.Empty;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "refreshpage", "RefreshPage();", true);
        }
    }
}