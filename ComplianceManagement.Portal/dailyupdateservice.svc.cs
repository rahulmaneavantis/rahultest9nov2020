﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "dailyupdateservice" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select dailyupdateservice.svc or dailyupdateservice.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
   
    public class dailyupdateservice : Idailyupdateservice
    {

        public bool sendmailresult=false;
        public List<String> BindNotifications(int userid)
        {
            List<String> ReturnList = new List<String>();

            ReturnList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetNewUserNotification(userid);

            return ReturnList;
        }
        public int NotificationMail(int Userid)
        {
            return com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetUnreadNotificationsCount(Convert.ToInt32(Userid));
        }
        public List<NewsLetter> DailyNewsLetter()
        {
            List<NewsLetter> ReturnList = new List<NewsLetter>();
            ReturnList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetNewsLetter();
            return ReturnList;
        }
        public List<DailyUpdate> DailyUpdate()
        {
            List<DailyUpdate> ReturnList = new List<DailyUpdate>();
            ReturnList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetDailyUpdates();
            return ReturnList;           
        }

        public List<DailyUpdate> DailyUpdate_RLCS()
        {
            List<DailyUpdate> ReturnList = new List<DailyUpdate>();
            ReturnList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetDailyUpdates_RLCS();
            return ReturnList;
        }

        public int createticket(string name, string email, string subject, string message)
        {
            int i = 1;
            string SenderEmailAddress = System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
            message = "Hi, <br/><br/> Following ticket has been raise <br/> Name: " + name + " <br/> Email:" + email + " <br/>Message:" + message;
            EmailManager.SendMail(SenderEmailAddress, new List<string>(new string[] { "support@avantis.co.in" }), null, null, "AVACOM Ticket - " + subject, message);

            return i;
        }
        public bool ticket(string subject, string message, int requestor, string tickettype)
        {

            try
            {
                User usr = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(requestor);
               
                //long NotificationID = 0;
                //Notification newNotification = new Notification()
                //{
                //    ActID = 0,
                //    ComplianceID = 0,
                //    Type = "ticket",
                //    Remark = "Dear " + usr.FirstName + ", Your ticket number is  <a href='../users/ticketdetails.aspx?id=" + ticketNo + "' title='Click to check Ticket status'>" + ticketNo + "</a>.",
                //    CreatedBy = requestor,
                //    CreatedOn = DateTime.Now.Date,
                //    UpdatedBy = requestor,
                //    UpdatedOn = DateTime.Now.Date,
                //};

                //// if (!Business.ComplianceManagement.ExistsNotification(newNotification))
                //if (1 == 1)
                //{
                //    Business.ComplianceManagement.CreateNotification(newNotification);
                //    NotificationID = newNotification.ID;
                //}
                ////else
                ////{
                ////    NotificationID = Business.ComplianceManagement.UpdateNotification(newNotification);
                ////}

                //if (NotificationID != 0)
                //{

                //    UserNotification UNF = new UserNotification()
                //    {
                //        NotificationID = NotificationID,
                //        UserID = Convert.ToInt32(requestor),
                //        IsRead = false,
                //    };

                //    if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                //        Business.ComplianceManagement.CreateUserNotification(UNF);
                //    else
                //        Business.ComplianceManagement.UpdateNotification(newNotification);

                //}

                System.Net.WebClient wcClient = new System.Net.WebClient();
                Dictionary<string, object> dicPayload = new Dictionary<string, object>();

                dicPayload.Add("name", usr.FirstName + " " + usr.LastName);
                dicPayload.Add("email", usr.Email);
                dicPayload.Add("subject", subject);
                dicPayload.Add("message", message);
                dicPayload.Add("topicId", tickettype);
                string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(dicPayload);
                wcClient.Headers.Add("Content-Type", "application/json");
                string ticketNo = wcClient.UploadString(System.Configuration.ConfigurationManager.AppSettings["ticketUrl"] + "", "POST", strJson);

            }
            catch (Exception ex)
            {
                return false;
            }


            return true;
        }
        //comment by rahul on 6 JAN 2017
        //public bool ticket(string subject, string message, int requestor, string tickettype)
        //{

        //    try
        //    {
        //        User usr = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(requestor);
        //        Dictionary<string, object> dicPayload = new Dictionary<string, object>();
        //        System.Net.WebClient wcClient = new System.Net.WebClient();

        //        dicPayload.Add("name", usr.FirstName + " " + usr.LastName);
        //        dicPayload.Add("email", usr.Email);
        //        dicPayload.Add("subject", subject);
        //        dicPayload.Add("message", message);
        //        dicPayload.Add("topicId", tickettype);

        //        wcClient.Headers.Add("X-API-Key", System.Configuration.ConfigurationManager.AppSettings["ticketApi"]); //29A1509E65EFB1E778855A2084C4DF36
        //        wcClient.Headers.Add("Expect", string.Empty);
        //        wcClient.Headers.Add("User-Agent", "My osTicket Client");

        //        string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(dicPayload);

        //      string ticketnumber=wcClient.UploadString(System.Configuration.ConfigurationManager.AppSettings["ticketUrl"] + "api/http.php/tickets.json", "POST", strJson);

        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }


        //    return true;
        //}
        public RootObject GetWhetherInfo(string city)
        {
            string str = "";

            RootObject tmp=new RootObject();
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://api.openweathermap.org/data/2.5/weather?q="+ city + "&appid=d03c110840d27c4892eda6026da4f3dd");
            try
            {
                System.Net.WebResponse response = request.GetResponse();
                using (System.IO.Stream responseStream = response.GetResponseStream())
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(responseStream, Encoding.UTF8);
                    str=reader.ReadToEnd();
                     tmp = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(str);
                    // Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(reader.Read().ToString());
                }
            }
            catch (Exception ex)
            {
                 
            }  
            return tmp;
        }

        public widget getwidgets(string userid)
        {
            return com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetWidgets(Convert.ToInt32(userid));
             
        }

        public List<emailview> Mails(int Userid)
        {
            return com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetEmails(Convert.ToInt32(Userid));
        }

        public int Mailscnt(int Userid) {
            return com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetEmailscnt(Convert.ToInt32(Userid));
        }
        #region Previous Code This Code Change by Sushant 29 DEC 2016 Email Validation
        //public bool UserMessaging(string recepient,int sender,string subject,string message)
        //{
        //    string Message;

        //    if (!string.IsNullOrEmpty(recepient))
        //    {
        //        string[] arryval = recepient.Split(',');//split values with ‘,’  
        //        int j = arryval.Length;
        //        int i = 0;
        //        foreach (string v in arryval)
        //        {
        //            try
        //            {
        //                Email n = new Email();
        //                n.Recepient = v;
        //                n.Sender = sender;
        //                n.Subjecttext = subject;
        //                n.MessageBody = message;
        //                n.CreatedDate = DateTime.Now;
        //                com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement CM = new com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement();
        //                CM.Addemaildata(n);

        //            }
        //            catch { }
        //        }
        //        string email = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(sender).Email;
        //        EmailManager.SendMail(System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { recepient }), null, null, subject+ " :: " + email, message);
        //    }

        //    return true;
        //}
        #endregion
        #region Previous Code This Code Change by Sushant 30 DEC 2016 Email Validation
        //public bool UserMessaging(string recepient, int sender, string subject, string message)
        //{            
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(recepient))
        //        {
        //            string[] arryval = recepient.Split(',');//split values with ‘,’  
        //            int j = arryval.Length;
        //            int i = 0;
        //            foreach (string v in arryval)
        //            {
        //                try
        //                {
        //                    Email n = new Email();
        //                    n.Recepient = v;
        //                    n.Sender = sender;
        //                    n.Subjecttext = subject;
        //                    n.MessageBody = message;
        //                    n.CreatedDate = DateTime.Now;                       
        //                    int CustiD = Convert.ToInt32(sender);

        //                    var custdetails = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(sender);
        //                    if (custdetails !=null)
        //                    {
        //                        int? customerID = custdetails.CustomerID;
        //                        List<string> CustList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMailList(Convert.ToInt32(customerID));
        //                        if (CustList.Any(v.Contains))
        //                        {
        //                            com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.Addemaildata(n);
        //                            string email = custdetails.Email;
        //                            EmailManager.SendMail(System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { recepient }), null, null, subject + " :: " + email, message);
        //                        }
        //                    }                            
        //                }
        //                catch { }
        //            }                   
        //        }
        //    }
        //    catch { return false; }
        //    return true;
        //}
        #endregion
        #region Previous Code This Code Change by Sushant 2 JAN 2017 Email Validation
        //public bool UserMessaging(string recepient, int sender, string subject, string message)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(recepient))
        //        {
        //            string[] arryval = recepient.Split(',');//split values with ‘,’  
        //            int j = arryval.Length;
        //            int i = 0;
        //            foreach (string v in arryval)
        //            {
        //                try
        //                {
        //                    Email n = new Email();
        //                    n.Recepient = v;
        //                    n.Sender = sender;
        //                    n.Subjecttext = subject;
        //                    n.MessageBody = message;
        //                    n.CreatedDate = DateTime.Now;
        //                    int CustiD = Convert.ToInt32(sender);

        //                    var custdetails = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(sender);
        //                    if (custdetails != null)
        //                    {
        //                        int? customerID = custdetails.CustomerID;
        //                        List<string> CustList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMailList(Convert.ToInt32(customerID));
        //                        if (CustList.Any(v.Contains))
        //                        {
        //                            com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.Addemaildata(n);
        //                            string email = custdetails.Email;
        //                            EmailManager.SendMail(System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { recepient }), null, null, subject + " :: " + email, message);
        //                        }                              
        //                    }
        //                }
        //                catch { }
        //            }
        //        }
        //    }
        //    catch { return false; }

        //    return true;
        //}
        #endregion
        public bool UserMessaging(string recepient, int sender, string subject, string message)
        {
            try
            {
                string ListMail = System.Configuration.ConfigurationManager.AppSettings["ImpliMailList"];
                //string[] AllmailList2 = new string[] { };
                //if (!string.IsNullOrEmpty(recepient))
                //{
                //    AllmailList2 = ListMail.Split(',');
                //}
                if (!string.IsNullOrEmpty(recepient))
                {
                    string[] arryval = recepient.Split(',');//split values with ‘,’  
                    int j = arryval.Length;
                    foreach (string v in arryval)
                    {
                        try
                        {
                          
                            Email n = new Email();
                            n.Recepient = v;
                            n.Sender = sender;
                            n.Subjecttext = subject;
                            n.MessageBody = message;
                            n.CreatedDate = DateTime.Now;
                            int CustiD = Convert.ToInt32(sender);
                         
                            var custdetails = com.VirtuosoITech.ComplianceManagement.Business.UserManagement.GetByID(sender);
                            if (custdetails != null)
                            {
                                //foreach (string x in AllmailList2)
                                //{
                                int? customerID = custdetails.CustomerID;
                                List<string> CustList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetMailList(Convert.ToInt32(customerID));
                                if (CustList.Any(v.Contains) || ListMail.Contains("," + v + ","))
                                {
                                    sendmailresult = true;
                                    com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.Addemaildata(n);
                                    string email = custdetails.Email;
                                    EmailManager.SendMail(System.Configuration.ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { recepient }), null, null, subject + " :: " + email, message);
                                }
                                //}
                            }
                        }
                        catch { }
                    }
                }
            }
            catch { return false; }

            return true;
        }

        public bool widget(int id, string control, bool yes)
        {
            com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement CM = new com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement();
            return CM.UpdateWidget(id, control, yes);
        }

        public List<string> BindReports(string type, string subtype, int userid)
        {
            throw new NotImplementedException();
        }

        public bool upcolor(string high, int sender, string medium, string low , string critical)
        {
            ManagementColor Cmd = new ManagementColor();
            Cmd.High = high;
            Cmd.Low = low;
            Cmd.Medium = medium;
            Cmd.Critical = critical;
            Cmd.UserId = sender;
            com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement CM = new com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement();
            return CM.AddupdateColordata(Cmd);
        }


        public String BindAutoComplete(string SearchText, int CustID, int UserID, String Role)
        {
            String ReturnList = String.Empty;

            ReturnList = com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.GetSearchList(SearchText, CustID, UserID, Role);

            return ReturnList;
        }
    }


    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double pressure { get; set; }
        public double humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class Sys
    {
        public int type { get; set; }
        public int id { get; set; }
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class RootObject
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public int visibility { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public Sys sys { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }




}
