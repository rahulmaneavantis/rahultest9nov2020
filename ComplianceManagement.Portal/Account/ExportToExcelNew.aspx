﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ExportToExcelNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ExportToExcelNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">



     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" >
                <tr align="center">
                 
                    <td>
                        
                    </td>
                    <td align="left">
                        <asp:Button ID="btnExport" runat="server" Text="Export to Excel"
                            OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            
            <div id="divComp" runat="server">
                <asp:GridView ID="grdCompliance" AutoGenerateColumns="false" CellPadding="5" runat="server">
                    <Columns>

                         <asp:BoundField HeaderText="Title" DataField="Title" />
                         <asp:BoundField HeaderText="Act" DataField="Act" />
                         <asp:BoundField HeaderText="Description" DataField="Description" />

                      <%--  <asp:BoundField HeaderText="ComplianceID" DataField="ID" />
                        <asp:BoundField HeaderText="ActID" DataField="ActID" />
                        <asp:BoundField HeaderText="ActName" DataField="ActName" />
                        <asp:BoundField HeaderText="Sections" DataField="Sections" />
                        <asp:BoundField HeaderText="ShortDescription" DataField="ShortDescription" />
                        <asp:BoundField HeaderText="Description" DataField="Description" />
                        <asp:BoundField HeaderText="UploadDocument" DataField="UploadDocument" />
                        <asp:BoundField HeaderText="RequiredForms" DataField="RequiredForms" />
                        <asp:BoundField HeaderText="Frequency" DataField="Frequency" />
                        <asp:BoundField HeaderText="ComplianceType" DataField="ComplianceType" />
                        <asp:BoundField HeaderText="SubComplianceType" DataField="SubComplianceType" />
                        <asp:BoundField HeaderText="DueDate" DataField="DueDate" />
                        <asp:BoundField HeaderText="FixedGap" DataField="FixedGap" />
                        <asp:BoundField HeaderText="ComplianceCategory" DataField="ComplianceCategory" />
                        <asp:BoundField HeaderText="State" DataField="State" />        --%>            
                    </Columns>
                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>

   
</asp:Content>
