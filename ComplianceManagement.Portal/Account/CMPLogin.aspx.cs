﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.Security;
using System.Configuration;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class CMPLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    HttpCookie loginCookie = Request.Cookies.Get("LoginCookie");
                    if (null != loginCookie)
                    {
                        cbRememberMe.Checked = true;
                        tbxUsername.Text = loginCookie.Values["Username"].ToString();

                        tbxPassword.Attributes.Add("value", loginCookie.Values["Password"].ToString());
                    }
                    tbxUsername.Focus();

                    if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                    {
                        int userID = Convert.ToInt32(FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name);
                        User user = UserManagement.GetByID(userID);
                        ProcessAuthenticationInformation(user);
                        cbRememberMe.Checked = true;
                    }

                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        Response.Redirect("~/Default");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                User user = null;
                if (UserManagement.IsValidUser(tbxUsername.Text, Util.CalculateMD5Hash(tbxPassword.Text), out user))
                {
                    if (user.IsActive)
                    {
                        if (cbRememberMe.Checked)
                        {
                            if (FormsAuthentication.CookiesSupported)
                            {
                                HttpCookie loginCookie = new HttpCookie("LoginCookie");
                                //let the cookie expire after 30 days
                                loginCookie.Expires = DateTime.Now.AddDays(30);
                                loginCookie.Values["Username"] = tbxUsername.Text;
                                loginCookie.Values["Password"] = tbxPassword.Text;

                                Response.Cookies.Add(loginCookie);
                            }
                        }
                        else
                        {
                            Response.Cookies["LoginCookie"].Expires = DateTime.Now.AddDays(-1);
                        }

                        ProcessAuthenticationInformation(user);

                        //added by sudarshan to login same user multiple time
                        if (Application[user.ID.ToString()] != null)
                        {
                            Session["ShowAlertForLogin"] = "true";
                        }
                        else
                        {
                            Session["ShowAlertForLogin"] = "false";
                            Application[user.ID.ToString()] = 1;
                        }

                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your account is disabled.";
                    }
                }
                else
                {
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Please enter a valid username or password.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ProcessAuthenticationInformation(User user)
        {
            try
            {

                int checkInternalapplicable = 0;
                // int rahul =(int ?)CustomerManagement.GetByIcompilanceapplicableID(Convert.ToInt32(user.ID)).IComplianceApplicable;
                Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                if (customer.IComplianceApplicable != null)
                {
                    checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                }     

                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), cbRememberMe.Checked);
                //FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2}", user.ID, role, name), false);
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3}", user.ID, role, name, checkInternalapplicable), false);

                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);

                User userToUpdate = new User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.UtcNow;
                UserManagement.Update(userToUpdate);

                //FormsAuthentication.SetAuthCookie(user.ID.ToString(), cbRememberMe.Checked);
                if (role.Equals("SADMN"))
                {
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("MGMT"))
                {
                    Response.Redirect("~/Management/MangementDashboard.aspx", false);
                }
                else
                {
                    Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                }

                DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                DateTime currentDate = DateTime.Now;
                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);

                if (dateDifference == noDays || dateDifference > noDays)
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
                else
                {
                    Session["ChangePassword"] = false;
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}