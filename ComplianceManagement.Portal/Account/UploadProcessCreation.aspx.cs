﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadProcessCreation : System.Web.UI.Page
    {
        bool suucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                Tab1_Click(sender, e);                
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }              
        #region Add Process      
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoProcess.Checked)
                            {
                                bool flag = ProcessSheetsExitsts(xlWorkbook, "ProcessSubProcess");
                                if (flag == true)
                                {
                                    ProcessData(xlWorkbook);
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No data found in file.";
                                }
                            }                                                        
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                            if (suucess == true)//commented by Manisha
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                            }
                            if(suucess==false)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool ProcessSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ProcessSubProcess"))
                    {                       
                        if (sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }                    
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }        
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ProcessData(ExcelPackage xlWorkbook)
        {
            try
            {
                int customerID = -1;
                customerID =Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
               
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ProcessSubProcess"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int processid = -1;                   
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<mst_Subprocess> mstSubprocesslist = new List<mst_Subprocess>();
                    List<mst_Subprocess> mstSubprocesslist1 = new List<mst_Subprocess>();
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        //processid = RiskCategoryManagement.GetProcessIDByName(xlWorksheet.Cells[i, 1].Value.ToString());
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 1].Text.ToString(), customerID);
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + count + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        string isprocessnonprocess = "";
                        if (xlWorksheet.Cells[i, 3].Value != null)
                        {
                            if (xlWorksheet.Cells[i, 3].Value.ToString().Trim() == "P")
                            {
                                isprocessnonprocess = "P";
                            }
                            else
                            {
                                isprocessnonprocess = "N";
                            }
                        }
                        string subprocessname = "";                       
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            subprocessname = Convert.ToString(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (!(ProcessManagement.ExistsProcess(processid, Convert.ToString(subprocessname), isprocessnonprocess)))
                        {
                            mst_Subprocess mstSubprocess = new mst_Subprocess();
                            mstSubprocess.Name = subprocessname;
                            mstSubprocess.ProcessId = processid;
                            mstSubprocess.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            mstSubprocess.IsDeleted = false;
                            mstSubprocess.CreatedOn = DateTime.Now;
                            mstSubprocess.IsProcessNonProcess = isprocessnonprocess;
                            mstSubprocesslist.Add(mstSubprocess);
                        }//exists end
                    }
                    mstSubprocesslist1 = mstSubprocesslist.Where(entry => entry.ProcessId == 0).ToList();
                    if (mstSubprocesslist1.Count == 0)
                    {
                        suucess = ProcessManagement.CreateExcelProcess(mstSubprocesslist);                        
                        suucess = true;
                    }
                    else
                    {
                        suucess = false;                       
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }        
        #endregion               
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
        }
    }
}