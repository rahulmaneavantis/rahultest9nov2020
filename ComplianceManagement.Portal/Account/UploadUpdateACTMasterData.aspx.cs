﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadUpdateACTMasterData : System.Web.UI.Page
    {               
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;                              
            }
        }        
        #region Add Compliance
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    bool suucess = false;
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = ActSheetsExitsts(xlWorkbook, "ActIndustry");
                            if (flag == true)
                            {
                                ProcessActIndustryNameData(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No data found in file.";
                            }
                            if (suucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Excelsheet data saved successfully.";                                
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }
        private bool ActSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ActIndustry"))
                    {
                        flag = true;
                        break;
                    }               
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }     
        private bool ProcessActIndustryNameData(ExcelPackage xlWorkbook)
        {
            bool flag = false;
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ActIndustry"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                List<Act_IndustryMapping> MASTER_AIMLIST = new List<Act_IndustryMapping>();
                List<Act_SubIndustryMapping> MASTER_SUBAIMLIST = new List<Act_SubIndustryMapping>();

                for (int i = 3; i <= filledrow; i++)
                {
                    //ActID 
                    string ActID = Convert.ToString(xlWorksheet.Cells[i, 1].Value);

                    #region First 
                    //3 Aviation    
                    string Aviation = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                    if (Aviation == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 3;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //1 Agriculture and Allied Industries 
                    string Agriculture_and_Allied_Industries = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                    if (Agriculture_and_Allied_Industries == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 1;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //2 Automotive and Auto Ancillary   
                    string Automotive_and_Auto_Ancillary = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                    if (Automotive_and_Auto_Ancillary == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 2;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //4 Cement 
                    string Cement = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                    if (Cement == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 4;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //5 Chemical and Fertilizer 
                    string Chemical_and_Fertilizer = Convert.ToString(xlWorksheet.Cells[i, 6].Value);
                    if (Chemical_and_Fertilizer == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 5;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //6 Defense 
                    string Defense = Convert.ToString(xlWorksheet.Cells[i, 7].Value);
                    if (Defense == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 6;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //7 Education   
                    string Education = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                    if (Education == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 7;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //8 Electronics 
                    string Electronics = Convert.ToString(xlWorksheet.Cells[i, 9].Value);
                    if (Electronics == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 8;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //9 Energy  
                    string Energy = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                    if (Energy == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 9;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    // 10 Engineering and Capital Goods           
                    string Engineering_and_Capital_Goods = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                    if (Engineering_and_Capital_Goods == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 10;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    #endregion

                    #region 11 Financial Services                    
                    //Banking                        
                    string Banking = Convert.ToString(xlWorksheet.Cells[i, 12].Value);
                    //Housing  Finance Company
                    string Housing_Finance_Company = Convert.ToString(xlWorksheet.Cells[i, 13].Value);
                    //Insurance
                    string Insurance = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                    //Non Banking Financial Company
                    string Non_Banking_Financial_Company = Convert.ToString(xlWorksheet.Cells[i, 15].Value);
                    //Other Financial Services
                    string Other_Financial_Services = Convert.ToString(xlWorksheet.Cells[i, 16].Value);

                    if (Banking == "N" && Housing_Finance_Company == "N" && Insurance == "N" && Non_Banking_Financial_Company == "N" && Other_Financial_Services == "N")
                    {

                    }
                    else
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 11;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);

                        if (Banking == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 11;
                            ASUBIM.SubIndustryID = 1;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Housing_Finance_Company == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 11;
                            ASUBIM.SubIndustryID = 2;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Insurance == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 11;
                            ASUBIM.SubIndustryID = 3;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Non_Banking_Financial_Company == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 11;
                            ASUBIM.SubIndustryID = 4;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Other_Financial_Services == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 11;
                            ASUBIM.SubIndustryID = 5;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                    }
                    #endregion

                    #region 12 FMCG
                    //Cosmetic 
                    string Cosmetic = Convert.ToString(xlWorksheet.Cells[i, 17].Value);
                    //Dairy 
                    string Dairy = Convert.ToString(xlWorksheet.Cells[i, 18].Value);
                    //Processed Food
                    string ProcessedFood = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                    if (Cosmetic == "N" && Dairy == "N" && ProcessedFood == "N")
                    {

                    }
                    else
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 12;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);

                        if (Cosmetic == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 12;
                            ASUBIM.SubIndustryID = 6;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Dairy == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 12;
                            ASUBIM.SubIndustryID = 7;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (ProcessedFood == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 12;
                            ASUBIM.SubIndustryID = 8;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                    }
                    #endregion

                    #region Second
                    //13 Health Care 
                    string HealthCare = Convert.ToString(xlWorksheet.Cells[i, 20].Value);
                    if (HealthCare == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 13;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //15 IT / ITES   
                    string IT_ITES = Convert.ToString(xlWorksheet.Cells[i, 21].Value);
                    if (IT_ITES == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 15;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //16 Media and Entertainment
                    string Media_and_Entertainment = Convert.ToString(xlWorksheet.Cells[i, 22].Value);
                    if (Media_and_Entertainment == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 16;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //17 Metal and Mining   
                    string Metal_and_Mining = Convert.ToString(xlWorksheet.Cells[i, 23].Value);
                    if (Metal_and_Mining == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 17;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //18 Oil and Gas 
                    string Oil_and_Gas = Convert.ToString(xlWorksheet.Cells[i, 24].Value);
                    if (Oil_and_Gas == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 18;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //19 Pharmaceutical  
                    string Pharmaceutical = Convert.ToString(xlWorksheet.Cells[i, 25].Value);
                    if (Pharmaceutical == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 19;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //20 Plastic 
                    string Plastic = Convert.ToString(xlWorksheet.Cells[i, 26].Value);
                    if (Plastic == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 20;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //21 Ports   
                    string Ports = Convert.ToString(xlWorksheet.Cells[i, 27].Value);
                    if (Ports == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 21;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //22 Pulp and Paper
                    string Pulp_and_Paper = Convert.ToString(xlWorksheet.Cells[i, 28].Value);
                    if (Pulp_and_Paper == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 22;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //23 Real Estate and Construction
                    string Real_Estate_and_Construction = Convert.ToString(xlWorksheet.Cells[i, 29].Value);
                    if (Real_Estate_and_Construction == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 23;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    #endregion

                    #region 24 Services
                    //Hospitality                                       
                    string Hospitality = Convert.ToString(xlWorksheet.Cells[i, 30].Value);
                    //Logistics and Transport  
                    string Logistics_and_Transport = Convert.ToString(xlWorksheet.Cells[i, 31].Value);
                    if (Hospitality == "N" && Logistics_and_Transport == "N")
                    {

                    }
                    else
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 24;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);

                        if (Hospitality == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 24;
                            ASUBIM.SubIndustryID = 9;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                        if (Logistics_and_Transport == "Y")
                        {
                            Act_SubIndustryMapping ASUBIM = new Act_SubIndustryMapping();
                            ASUBIM.ActID = Convert.ToInt64(ActID);
                            ASUBIM.IndustryID = 24;
                            ASUBIM.SubIndustryID = 10;
                            ASUBIM.IsActive = true;
                            ASUBIM.EditedBy = 1;
                            ASUBIM.EditedDate = DateTime.Now;
                            MASTER_SUBAIMLIST.Add(ASUBIM);
                        }
                    }
                    #endregion

                    #region Third

                    //25 Shipping 
                    string Shipping = Convert.ToString(xlWorksheet.Cells[i, 32].Value);
                    if (Shipping == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 25;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //26 Steel   
                    string Steel = Convert.ToString(xlWorksheet.Cells[i, 33].Value);
                    if (Steel == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 26;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //27 Telecommunication 
                    string Telecommunication = Convert.ToString(xlWorksheet.Cells[i, 34].Value);
                    if (Telecommunication == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 27;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //28 Textile 
                    string Textile = Convert.ToString(xlWorksheet.Cells[i, 35].Value);
                    if (Textile == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 28;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //29 Tobacco 
                    string Tobacco = Convert.ToString(xlWorksheet.Cells[i, 36].Value);
                    if (Tobacco == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 29;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    //14 Infrastructure
                    string Infrastructure = Convert.ToString(xlWorksheet.Cells[i, 37].Value);
                    if (Infrastructure == "Y")
                    {
                        Act_IndustryMapping AIM = new Act_IndustryMapping();
                        AIM.ActID = Convert.ToInt64(ActID);
                        AIM.IndustryID = 14;
                        AIM.IsActive = true;
                        AIM.EditedBy = 1;
                        AIM.EditedDate = DateTime.Now;
                        MASTER_AIMLIST.Add(AIM);
                    }
                    #endregion
                }
                bool actindustrySave = BulkActIndustryMappingInsert(MASTER_AIMLIST);
                bool actSubindustrySave=  BulkActSubIndustryMappingInsert(MASTER_SUBAIMLIST);
                if (actindustrySave && actSubindustrySave)
                {
                    flag = true;
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return flag;               
            }
        }
        public static bool IndustryExists(long ActID ,int IndustryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_IndustryMapping
                             where row.ActID == ActID 
                             && row.IndustryID== IndustryID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool SubIndustryExists(long ActID, int IndustryID,int SubIndustryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_SubIndustryMapping
                             where row.ActID == ActID
                             && row.IndustryID == IndustryID
                             && row.SubIndustryID== SubIndustryID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool BulkActIndustryMappingInsert(List<Act_IndustryMapping> ListIRH)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    ListIRH.ForEach(EachStep =>
                    {                       
                        if (!IndustryExists(EachStep.ActID, EachStep.IndustryID))
                        {
                            Count++;
                            entities.Act_IndustryMapping.Add(EachStep);
                            if (Count >= 500)
                            {
                                entities.SaveChanges();
                                Count = 0;
                            }
                        }                                                
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool BulkActSubIndustryMappingInsert(List<Act_SubIndustryMapping> ListIRH)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int Count = 0;
                    ListIRH.ForEach(EachStep =>
                    {
                        if (!SubIndustryExists(EachStep.ActID, EachStep.IndustryID, EachStep.SubIndustryID))
                        {
                            Count++;
                            entities.Act_SubIndustryMapping.Add(EachStep);
                            if (Count >= 500)
                            {
                                entities.SaveChanges();
                                Count = 0;
                            }
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }      
        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            //Act_Industry_SampleFormat.xlsx
            string FileName = "Act_Industry_SampleFormat.xlsx";
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "image/jpeg";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(Server.MapPath("~/ExcelFormat/Act_Industry_SampleFormat.xlsx"));
            response.Flush();
            response.End();
        }
        #endregion
    }
}