﻿<%@ Page Title="Upload Master Data" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UpdateCompliances_OLD.aspx.cs" 
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UpdateCompliances_OLD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <table style="width: 100%;">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            
            <td colspan="4">
                <asp:Panel ID="r" runat="server" Height="520" ScrollBars="Both">

                    <asp:GridView ID="grdCompliance" AutoGenerateColumns="false" CellPadding="5" runat="server">
                        <Columns>
                            <asp:BoundField HeaderText="Act Name" DataField="ActName" />
                            <asp:BoundField HeaderText="Section (S) / Rule (S)" DataField="Sections" />
                            <asp:BoundField HeaderText="Compliance Type" DataField="ComplianceType" />
                            <asp:BoundField HeaderText="Timely based type" DataField="SubComplianceType" />
                            <asp:BoundField HeaderText="Short Description" DataField="ShortDescription" />
                            <asp:BoundField HeaderText="Detailed Description" DataField="Description" />
                            <asp:BoundField HeaderText="Nature Of Compliance" DataField="NatureOfCompliance" />
                            <asp:BoundField HeaderText="Upload Document" DataField="UploadDocument" />
                            <asp:BoundField HeaderText="Required Forms" DataField="RequiredForms" />
                            <asp:BoundField HeaderText="Sample Form" DataField="sampleform" />
                            <asp:BoundField HeaderText="Event based" DataField="iseventbased" />
                            <asp:BoundField HeaderText="Event name" DataField="EventName" />
                            <asp:BoundField HeaderText="Event compliance type" DataField="EventComplianceType" />
                            <asp:BoundField HeaderText="Sub Event name" DataField="SubEventName" />
                            <asp:BoundField HeaderText="Compliance due (in days)" DataField="DueDate" />
                            <asp:BoundField HeaderText="Frequency" DataField="Frequency" />
                            <asp:BoundField HeaderText="DueDate" DataField="DueDate" />
                            <asp:BoundField HeaderText="Special Due Date - For Month" DataField="spdate" />
                            <asp:BoundField HeaderText="Special Due Date - Date" DataField="spdate" />
                            <asp:BoundField HeaderText="Reminder type" DataField="ReminderType" />
                            <asp:BoundField HeaderText="Before (in days)" DataField="ReminderBefore" />
                            <asp:BoundField HeaderText="Gap (in days)" DataField="ReminderGap" />
                            <asp:BoundField HeaderText="Risk Type" DataField="RiskType" />
                            <asp:BoundField HeaderText="Non Compliance Type" DataField="NonComplianceType" />
                            <asp:BoundField HeaderText="Fixed Minimum" DataField="FixedMinimum" />
                            <asp:BoundField HeaderText="Fixed Maximum" DataField="FixedMaximum" />
                            <asp:BoundField HeaderText="Day/Month" DataField="VariableAmountPerMonth" />
                            <asp:BoundField HeaderText="Variable Amount Rs." DataField="VariableAmountPerDay" />
                            <asp:BoundField HeaderText="Variable Amount (Max)" DataField="VariableAmountPerDayMax" />
                            <asp:BoundField HeaderText="Variable Amount (%)" DataField="VariableAmountPercent" />
                            <asp:BoundField HeaderText="Variable Amount (% Max)" DataField="VariableAmountPercentMax" />
                            <asp:BoundField HeaderText="Imprisonment" DataField="Imprisonment" />
                            <asp:BoundField HeaderText="Designation" DataField="Designation" />
                            <asp:BoundField HeaderText="Minimum Years" DataField="MinimumYears" />
                            <asp:BoundField HeaderText="Maximum Years" DataField="MaximumYears" />
                            <asp:BoundField HeaderText="Others" DataField="Others" />
                            <asp:BoundField HeaderText="CalFlag" DataField="calflag" />
                        </Columns>
                        <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                    </asp:GridView>

                </asp:Panel>
            </td>
        </tr>
    </table>


</asp:Content>
