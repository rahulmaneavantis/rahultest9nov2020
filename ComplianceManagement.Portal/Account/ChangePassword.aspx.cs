﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Threading;
using Logger;
using System.Reflection;
using System.Web.Security;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Text.RegularExpressions;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ChangePassword : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              //  lblmsg.Text = string.Empty;
            }
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            try
            {
                int userID = -1;
                if (AuthenticationHelper.UserID != -1)
                {
                    userID = AuthenticationHelper.UserID;
                }
                else
                {
                    if (Convert.ToString(Session["userID"]) != null)
                    {
                        if (Convert.ToString(Session["userID"]) != "")
                        {
                            userID = Convert.ToInt32(Session["userID"]);
                        }
                    }
                }
                if (userID != -1 && !string.IsNullOrEmpty(Convert.ToString(Session["Email"])))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                       var pt = (from row in entities.PasswordRestrictions
                                where row.IsActive == false
                                select row.Pvalue.ToUpper().Trim()).ToList();
                        if (pt.Contains(txtNewPassword.Text.ToUpper().Trim()))
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "The password created is easy to predict. Please create another password.";
                        }
                        else
                        {
                            if (Convert.ToString(Session["Email"]).Trim().Contains('@'))
                            {
                                if (!UserManagement.EmailIDExists(Convert.ToString(Session["Email"]).Trim()))
                                {
                                    #region If EmailID 
                                    User user = UserManagement.GetByID(userID);
                                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(userID));
                                    bool check = false;
                                    if (user.EnType == "A")
                                    {
                                        check = user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim()));
                                    }
                                    else
                                    {
                                        check = user.Password.Equals(Util.CalculateMD5Hash(txtOldPassword.Text.Trim()));
                                    }
                                    //if (user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim())))
                                    if (check)
                                    {

                                        if (!(txtOldPassword.Text.Trim().Equals(txtNewPassword.Text.Trim())))
                                        {
                                            string passwordText = txtNewPassword.Text.Trim();
                                            string Newpassword = Util.CalculateAESHash(passwordText);


                                            var ps = (from row in entities.OldUserPasswordRestrictions
                                                      where row.UserID == user.ID &&
                                                            row.Password == Newpassword
                                                      select row).ToList();

                                            if (ps != null)
                                            {
                                                if (ps.Count != 0)
                                                {
                                                    CustomValidator1.IsValid = false;
                                                    CustomValidator1.ErrorMessage = "New Password can not be same as previous Password. Please enter different password.";
                                                }
                                                else
                                                {
                                                    user.Password = Newpassword;
                                                    mstuser.Password = Newpassword;

                                                    string ReplyEmailAddressName = "";
                                                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.UserID == -1)
                                                    {
                                                        ReplyEmailAddressName = "Avantis";
                                                    }
                                                    else
                                                    {
                                                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(user.CustomerID));
                                                    }

                                                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                                                    bool result = UserManagement.ChangePassword(user);
                                                    bool result1 = UserManagementRisk.ChangePassword(mstuser);
                                                    if (result && result1)
                                                    {

                                                        OldUserPasswordRestriction pass1 = new OldUserPasswordRestriction();
                                                        pass1.UserID = user.ID;
                                                        pass1.Password = Newpassword;
                                                        entities.OldUserPasswordRestrictions.Add(pass1);
                                                        entities.SaveChanges();

                                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Password reset successfully, Please login again.";

                                                        Session.Clear();
                                                        Session.Abandon();
                                                        Session.RemoveAll();
                                                        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                                                        FormsAuthentication.SignOut();
                                                        //FormsAuthentication.RedirectToLoginPage();
                                                      
                                                    }
                                                    else
                                                    {
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "New Password can not be same as Old Password. Please enter different password.";
                                        }
                                    }
                                    else
                                    {
                                        CustomValidator1.IsValid = false;
                                        CustomValidator1.ErrorMessage = "Please enter correct old password.";
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region If EmailID 
                                    User user = UserManagement.GetByID(userID);
                                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(Convert.ToInt32(userID));
                                    RLCS_User_Mapping rlcsuser = UserManagement.rlcsGetByID(Convert.ToInt32(userID));
                                    bool check = false;
                                    if (user.EnType == "A")
                                    {
                                        check = user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim()));
                                    }
                                    else
                                    {
                                        check = user.Password.Equals(Util.CalculateMD5Hash(txtOldPassword.Text.Trim()));
                                    }
                                    //if (user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim())))
                                    if (check)
                                    {

                                        if (!(txtOldPassword.Text.Trim().Equals(txtNewPassword.Text.Trim())))
                                        {
                                            string passwordText = txtNewPassword.Text.Trim();
                                            string Newpassword = Util.CalculateAESHash(passwordText);


                                            var ps = (from row in entities.OldUserPasswordRestrictions
                                                      where row.UserID == user.ID &&
                                                            row.Password == Newpassword
                                                      select row).ToList();

                                            if (ps != null)
                                            {
                                                if (ps.Count != 0)
                                                {
                                                    CustomValidator1.IsValid = false;
                                                    CustomValidator1.ErrorMessage = "New Password can not be same as previous Password. Please enter different password.";
                                                }
                                                else
                                                {
                                                    user.Password = Newpassword;
                                                    mstuser.Password = Newpassword;
                                                    rlcsuser.Password = Newpassword;
                                                    string ReplyEmailAddressName = "";
                                                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.UserID == -1)
                                                    {
                                                        ReplyEmailAddressName = "Avantis";
                                                    }
                                                    else
                                                    {
                                                        ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(user.CustomerID));
                                                    }

                                                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                                                    bool result = UserManagement.ChangePassword(user);
                                                    bool result1 = UserManagementRisk.ChangePassword(mstuser);
                                                    bool result2 = UserManagement.RLCSChangePassword(rlcsuser);
                                                    if (result && result1)
                                                    {

                                                        OldUserPasswordRestriction pass1 = new OldUserPasswordRestriction();
                                                        pass1.UserID = user.ID;
                                                        pass1.Password = Newpassword;
                                                        entities.OldUserPasswordRestrictions.Add(pass1);
                                                        entities.SaveChanges();

                                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Password reset successfully.";

                                                        Session.Clear();
                                                        Session.Abandon();
                                                        Session.RemoveAll();
                                                        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                                                        FormsAuthentication.SignOut();
                                                        FormsAuthentication.RedirectToLoginPage();
                                                       
                                                    }
                                                    else
                                                    {
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "New Password can not be same as Old Password. Please enter different password.";
                                        }
                                    }
                                    else
                                    {
                                        CustomValidator1.IsValid = false;
                                        CustomValidator1.ErrorMessage = "Please enter correct old password.";
                                    }
                                    #endregion

                                }

                            }
                            else
                            {
                                #region If NOT EmailID 
                                RLCS_User_Mapping user = UserManagement.GetRLCSByID(Convert.ToString(Session["Email"]).Trim());
                                if (user != null)
                                {
                                    bool check = false;
                                    if (user.EnType == "A")
                                    {
                                        check = user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim()));
                                    }
                                    else
                                    {
                                        check = user.Password.Equals(Util.CalculateMD5Hash(txtOldPassword.Text.Trim()));
                                    }

                                    //if (user.Password.Equals(Util.CalculateAESHash(txtOldPassword.Text.Trim())))                                    
                                    if (check)
                                    {
                                        if (!(txtOldPassword.Text.Trim().Equals(txtNewPassword.Text.Trim())))
                                        {

                                            string passwordText = txtNewPassword.Text.Trim();
                                            string Newpassword = Util.CalculateAESHash(passwordText);
                                            var ps = (from row in entities.OldUserPasswordRestrictions
                                                      where row.UserID == user.ID &&
                                                            row.Password == Newpassword
                                                      select row).ToList();

                                            if (ps != null)
                                            {
                                                if (ps.Count != 0)
                                                {
                                                    CustomValidator1.IsValid = false;
                                                    CustomValidator1.ErrorMessage = "New Password can not be same as exist Password. Please enter different password.";
                                                }
                                                else
                                                {
                                                    user.Password = Newpassword;
                                                    string ReplyEmailAddressName = "";
                                                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.UserID == -1)
                                                    {
                                                        ReplyEmailAddressName = "Avantis";
                                                    }
                                                    else
                                                    {
                                                        if (AuthenticationHelper.CustomerID !=null && AuthenticationHelper.CustomerID !=-1)
                                                        {
                                                            ReplyEmailAddressName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                                        }                                                        
                                                    }
                                                    string message = EmailNotations.RLCSSendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);
                                                    bool result = UserManagement.RLCSChangePassword(user);
                                                    if (result)
                                                    {

                                                        OldUserPasswordRestriction pass1 = new OldUserPasswordRestriction();
                                                        pass1.UserID = user.ID;
                                                        pass1.Password = Newpassword;
                                                        entities.OldUserPasswordRestrictions.Add(pass1);
                                                        entities.SaveChanges();

                                                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Password reset successfully.";
                                                        Session.Clear();
                                                        Session.Abandon();
                                                        Session.RemoveAll();
                                                        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                                                        FormsAuthentication.SignOut();
                                                        FormsAuthentication.RedirectToLoginPage();
                                                       
                                                    }
                                                    else
                                                    {
                                                        CustomValidator1.IsValid = false;
                                                        CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "New Password can not be same as Old Password. Please enter different password.";
                                        }
                                    }
                                    else
                                    {
                                        CustomValidator1.IsValid = false;
                                        CustomValidator1.ErrorMessage = "Please enter correct old password.";
                                    }
                                }
                                else
                                {
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Please enter correct old password.";
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtConfirmPassword.Text = string.Empty;
               // lblmsg.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            try
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
                //  Response.Redirect("../Login.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}