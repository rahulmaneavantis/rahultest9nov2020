﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class DumpMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (User.Identity.IsAuthenticated)
                {
                    BindUserList();
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                }
                else
                    FormsAuthentication.RedirectToLoginPage();               
            }
        }

        public void BindUserList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                   
                    var user = (from row in entities.Users
                                where row.IsActive == true
                                && row.IsDeleted == false
                                && (row.RoleID == 12 || row.RoleID == 24 || row.RoleID == 25 || row.RoleID == 26)
                                select new UserName()
                                {
                                    ID = row.ID,
                                    Name = row.FirstName + " "+ row.LastName
                                }).ToList();

                    drpUserlst.DataTextField = "Name";
                    drpUserlst.DataValueField = "ID";
                    drpUserlst.Items.Clear();
                    drpUserlst.DataSource = user;
                    drpUserlst.DataBind();
                    drpUserlst.Items.Insert(0, new ListItem("Select User", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public class UserName
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        protected void btnexcel_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpUserlst.SelectedValue != "-1")
                {

                    bool flagOutput = false;
                    List<int> actslst = new List<int>();
                    List<long> Compliancelst = new List<long>();
                    string actids = txtactid.Text;

                    if (!string.IsNullOrEmpty(actids))
                    {
                        string[] values = actids.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (Convert.ToInt32(values[i].Trim()) > 0)
                                actslst.Add(Convert.ToInt32(values[i].Trim()));
                        }
                    }

                    actslst = actslst.Distinct().ToList();

                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<ComplianceViewExport> table = new List<ComplianceViewExport>();

                            table = (from row in entities.ComplianceViewExports
                                     select row).ToList();
                            if (table.Count > 0)
                            {
                                if (actslst.Count > 0)
                                    table = table.Where(x => actslst.Contains(x.ActID)).ToList();

                            }
                            table = table.Distinct().ToList();
                            if (table.Count > 0)
                            {
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Data Dump List");
                                System.Data.DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table as List<ComplianceViewExport>).ToDataTable());
                                ExcelData1 = view1.ToTable("Selected", false, "ID", "ActID", "ActName", "Sections", "ShortDescription", "Description", "Frequency", "ComplianceType", "State", "ComplianceCategory", "ActionableOrInformative", "Ctype");

                                exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A1"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["B1"].Value = "ActID";
                                exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["C1"].Value = "ActName";
                                exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["D1"].Value = "Sections";
                                exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["E1"].Value = "ShortDescription";
                                exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F1"].Value = "Description";
                                exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G1"].Value = "Frequency";
                                exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["H1"].Value = "ComplianceType";
                                exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["H1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["I1"].Value = "State";
                                exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["I1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["J1"].Value = "ComplianceCategory";
                                exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["J1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["K1"].Value = "Actionable/Informative";
                                exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["K1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["L1"].Value = "IsEventBased";
                                exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["L1"].AutoFitColumns(20);
                                

                                using (ExcelRange col = exWorkSheet1.Cells[2, 1, 2 + ExcelData1.Rows.Count, 13])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }
                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=DumpReport.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                flagOutput = true;
                            }

                            if (flagOutput)
                            {
                                DataDumpLog obj = new DataDumpLog();

                                obj.UserID = Convert.ToInt32(drpUserlst.SelectedValue);
                                obj.ActID = txtactid.Text.Trim();
                                obj.CreatedBy = AuthenticationHelper.UserID;
                                obj.CreatedDate = DateTime.Now;

                                entities.DataDumpLogs.Add(obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Please Select User.')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}