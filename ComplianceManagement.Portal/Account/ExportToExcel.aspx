﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ExportToExcel.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ExportToExcel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">



     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" >
                <tr align="center">
                   <%-- <td>
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>--%>
                    <td>
                        <asp:RadioButtonList ID="rbtnlstActOrCompliance" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbtnlstActOrCompliance_SelectedIndexChanged">
                            <asp:ListItem Text="Act" Value="0" Selected="True" />
                            <asp:ListItem Text="Compliance" Value="1" />
                        </asp:RadioButtonList>
                    </td>
                    <td align="left">
                        <asp:Button ID="btnExport" runat="server" Text="Export to Excel"
                            OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div id="divAct" runat="server">
                <asp:GridView ID="grdAct" AutoGenerateColumns="false" CellPadding="5" runat="server">
                    <Columns>
                        <asp:BoundField HeaderText="ActID" DataField="ID" />
                        <asp:BoundField HeaderText="ActName" DataField="Name" />
                        <asp:BoundField HeaderText="State" DataField="State" />
                        <asp:BoundField HeaderText="City" DataField="City" />
                        <asp:BoundField HeaderText="ComplianceType" DataField="ComplianceTypeName" />
                        <asp:BoundField HeaderText="ComplianceCategory" DataField="ComplianceCategoryName" />
                    </Columns>
                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                </asp:GridView>
            </div>
            <div id="divComp" runat="server">
                <asp:GridView ID="grdCompliance" AutoGenerateColumns="false" CellPadding="5" runat="server">
                    <Columns>
                        <asp:BoundField HeaderText="ComplianceID" DataField="ID" />
                        <asp:BoundField HeaderText="ActID" DataField="ActID" />
                        <asp:BoundField HeaderText="ActName" DataField="ActName" />
                        <asp:BoundField HeaderText="Sections" DataField="Sections" />
                        <asp:BoundField HeaderText="ShortDescription" DataField="ShortDescription" />
                        <asp:BoundField HeaderText="Description" DataField="Description" />
                        <asp:BoundField HeaderText="UploadDocument" DataField="UploadDocument" />
                        <asp:BoundField HeaderText="RequiredForms" DataField="RequiredForms" />
                        <asp:BoundField HeaderText="Frequency" DataField="Frequency" />
                        <asp:BoundField HeaderText="ComplianceType" DataField="ComplianceType" />
                        <asp:BoundField HeaderText="SubComplianceType" DataField="SubComplianceType" />
                        <asp:BoundField HeaderText="DueDate" DataField="DueDate" />
                        <asp:BoundField HeaderText="FixedGap" DataField="FixedGap" />
                        <asp:BoundField HeaderText="ComplianceCategory" DataField="ComplianceCategory" />
                        <asp:BoundField HeaderText="State" DataField="State" />
                    <%--    <asp:BoundField HeaderText="ComplianceTypeID" DataField="ComplianceTypeId" />--%>
                    </Columns>
                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>

    <%--  <script type="text/javascript">
       
        function initializeCombobox() {
             $("#<%= ddlUsers.ClientID %>").combobox();
         }

         function initializeDatePicker(date) {

             var startDate = new Date();
             $(".StartDate").datepicker({
                 dateFormat: 'dd-mm-yy',
                 setDate: startDate,
                 numberOfMonths: 1
             });
         }

         function setDate() {
             $(".StartDate").datepicker();
         }
    </script>--%>
</asp:Content>
