﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ExportToExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    BindGridviewAct();
                }
                else
                    FormsAuthentication.RedirectToLoginPage();             
            }
        }        
        protected void rbtnlstActOrCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbtnlstActOrCompliance.SelectedValue.Equals("0"))
                {
                    BindGridviewAct();
                }
                else
                {
                    BindGridviewCompliance();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
         
            }
        }

        protected void BindGridviewAct()
        {
            divAct.Visible = true;
            divComp.Visible = false;
            grdAct.DataSource = ActManagement.GetAll(-1, -1, null);
            grdAct.DataBind();
            //---------------------------------------------------------
        }

        protected void BindGridviewCompliance()
        {                
            divAct.Visible = false;
            divComp.Visible = true;

            var compliancesData = Business.ComplianceManagement.GetAllExcelExport(null);
            
            List<object> dataSource = new List<object>();
            foreach (var complianceInfo in compliancesData)
            {
                dataSource.Add(new
                {
                    complianceInfo.ActID,
                    complianceInfo.ID,
                    complianceInfo.ActName,
                    complianceInfo.Sections,
                    complianceInfo.ShortDescription,
                    complianceInfo.Description,
                    complianceInfo.UploadDocument,                   
                    complianceInfo.RequiredForms,
                    complianceInfo.Frequency,                 
                    complianceInfo.ComplianceType,
                    complianceInfo.SubComplianceType,
                    complianceInfo.DueDate,
                    complianceInfo.FixedGap,
                    complianceInfo.ComplianceCategory,
                    complianceInfo.State,
                    complianceInfo.ComplianceTypeId,
                });
            }
            grdCompliance.DataSource = dataSource;
            grdCompliance.DataBind();

            
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (rbtnlstActOrCompliance.SelectedValue.Equals("0"))
                {
                    //..Act
                    Response.ClearContent();
                    Response.Buffer = true;
                    //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ActsExported.xls"));
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    grdAct.AllowPaging = false;
                    BindGridviewAct();
                    //Change the Header Row back to white color
                    grdAct.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < grdAct.HeaderRow.Cells.Count; i++)
                    {
                        grdAct.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                    }
                    grdAct.RenderControl(htw);
                    Response.Write(sw.ToString());
                    Response.End();
                    //-----------------------------------------------------------------------------------------------------
                }
                else
                {

                    Response.ClearContent();
                    Response.Buffer = true;
                    //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ComplianceExported.xls"));
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    grdCompliance.AllowPaging = false;
                    BindGridviewCompliance();
                    //Change the Header Row back to white color
                    grdCompliance.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < grdCompliance.HeaderRow.Cells.Count; i++)
                    {
                        grdCompliance.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                    }
                    grdCompliance.RenderControl(htw);
                    Response.Write(sw.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
     
            }
        }

        //protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}