﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using OfficeOpenXml;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadInternalMasterData : System.Web.UI.Page
    {
        DataTable dtCompliance = new DataTable();
        bool suucess = false;
        bool suucessSave = false;
        bool CheckListsuucess = false;
        bool CheckListsuucessSave = false;
        static int? customerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                //BindCustomers();
                //BindCheckListCustomers();
                Tab1_Click(sender, e);
            }
        }

        //private void BindCustomers()
        //{
        //    try
        //    {
        //        ddlCustomer.DataTextField = "Name";
        //        ddlCustomer.DataValueField = "ID";

        //        int customerID = -1;

        //        ddlCustomer.DataSource = CustomerManagement.GetAllIComplianceApplicableCustomer(customerID);
        //        ddlCustomer.DataBind();

        //        if (AuthenticationHelper.Role != "CADMN")
        //            ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        //private void BindCheckListCustomers()
        //{
        //    try
        //    {
        //        ddlchecklistCustomer.DataTextField = "Name";
        //        ddlchecklistCustomer.DataValueField = "ID";

        //        int customerID = -1;

        //        ddlchecklistCustomer.DataSource = CustomerManagement.GetAllIComplianceApplicableCustomer(customerID);
        //        ddlchecklistCustomer.DataBind();

        //        if (AuthenticationHelper.Role != "CADMN")
        //            ddlchecklistCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        #region Add Compliance
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = ActSheetsExitsts(xlWorkbook, "InternalCompliances");
                            if (flag == true)
                            {
                                ValidateComplianceData(xlWorkbook);
                            }
                            else
                            {

                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No data found in file.";
                            }
                            if (suucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Excelsheet data verified successfully.";
                                btnSave.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }

        private bool ActSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("InternalCompliances"))
                    {
                        if (!sheet.Name.Equals("InternalChecklist"))
                        {
                            if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Compliances") || sheet.Name.Trim().Equals("InternalCompliances"))
                            {
                                flag = true;
                                break;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (data.Equals("InternalChecklist"))
                    {
                        if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types") || sheet.Name.Trim().Equals("Checklist") || sheet.Name.Trim().Equals("InternalChecklist") )
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
     
        private void ProcessActData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Compliances"];
                //xlWorksheet = xlWorkbook.Workbook.Worksheets["Act"];
                if (xlWorksheet != null)
                {
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<InternalCompliancesCategory> complianceCategoryList = new List<InternalCompliancesCategory>();
                    List<Act> actList1 = new List<Act>();

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        if (!(ComplianceCategoryManagement.ExistsInternalComplianceCategory(Convert.ToString(xlWorksheet.Cells[i, 1].Value),customerID)))
                        {
                            InternalCompliancesCategory Category = new InternalCompliancesCategory();
                            Category.Name = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            Category.CustomerID = customerID;// Convert.ToInt32(ddlCustomer.SelectedValue);
                            complianceCategoryList.Add(Category);
                        }
                    }

                    ComplianceCategoryManagement.CreateInternalCompliancesCategory(complianceCategoryList);

                    List<InternalComplianceType> complianceTypeList = new List<InternalComplianceType>();

                    for (int i = 2; i <= xlrow2 - 2; i++)
                    {
                        if (!(ComplianceTypeManagement.ExistsInternalComplianceType(Convert.ToString(xlWorksheet.Cells[i, 1].Value),customerID)))
                        {
                            InternalComplianceType complianceType = new InternalComplianceType();
                            complianceType.Name = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                            complianceType.CustomerID = customerID;//Convert.ToInt32(ddlCustomer.SelectedValue);
                            complianceTypeList.Add(complianceType);
                        }
                    }
                    ComplianceTypeManagement.Create(complianceTypeList);
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;

                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ValidateComplianceData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalCompliances"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                createDataTable();
                for (int i = 2; i <= filledrow; i++)
                {
                    if (((xlWorksheet.Cells[i, 5].Value) == null) && (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == string.Empty))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + "  <br />  Compliance ShortDescription can not blank";
                        return;
                    }

                    DataRow drCompliance = dtCompliance.NewRow();
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance type can not blank";
                        return;
                    }

                    if (((xlWorksheet.Cells[i, 2].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 2].Value) != string.Empty))
                    {
                        drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Category can not blank";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Functionbased") || (Convert.ToString(xlWorksheet.Cells[i, 3].Value) == string.Empty))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "Recurring") || (Convert.ToString(xlWorksheet.Cells[i, 8].Value) == "One Time"))
                    {
                    }
                    else
                    { 
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Occourrence";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 9].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Annual") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "HalfYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Quarterly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "FourMonthly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "TwoYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "SevenYearly") || (Convert.ToString(xlWorksheet.Cells[i, 9].Value) == "Monthly"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Frequency";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 13].Value) == "Standard") || (Convert.ToString(xlWorksheet.Cells[i, 13].Value) == "Custom"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Reminder type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "High") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "Medium") || (Convert.ToString(xlWorksheet.Cells[i, 16].Value) == "Low"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Risk Type";
                        return;
                    }

                    complianceData.CustomerID = customerID;// UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID;//  Convert.ToInt32(ddlCustomer.SelectedValue);
                    drCompliance["CustomerID"] = customerID;//  Convert.ToInt32(ddlCustomer.SelectedValue);

                    if ((xlWorksheet.Cells[i, 8].Value.ToString()) == "Recurring")
                    {
                        if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                        {
                            drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Type can not blank(Function based,Checklist,Time Based)";
                            return;
                        }
                    }

                      drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                      complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Value);

                    //if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                    //{
                    //    drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                    //}
                    //else
                    //{
                    //    cvDuplicateEntry.IsValid = false;
                    //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Compliance Type can not blank(Function based,Checklist,Time Based)";
                    //    return;
                    //}

                    if (((xlWorksheet.Cells[i, 6].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 6].Value) != string.Empty))
                    {
                        if (Convert.ToString(xlWorksheet.Cells[i, 6].Value).Trim().Equals("Yes"))
                        {
                            drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[i, 7].Value);

                        }
                        else
                        {
                            complianceData.IUploadDocument = false;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Upload Document can not blank(Yes,No)";
                        return;
                    }

                        drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[i, 7].Value);

                    if (((xlWorksheet.Cells[i, 8].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != string.Empty))
                    {
                        drCompliance["IComplianceOccurrence"] = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ComplianceOccurrence can not blank";
                        return;
                    }

                    if (((xlWorksheet.Cells[i,16].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty))
                    {
                      
                            drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[i, 16].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Risk Type can not blank(High,Low,Medium)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Functionbased"))
                    {

                        if (((xlWorksheet.Cells[i, 9].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 9].Value) != string.Empty))
                        {

                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 9].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Frequency can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 10].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 10].Value) != string.Empty))
                        {

                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                            return;
                        }

                        //if ((xlWorksheet.Cells[i, 11].Value != null && xlWorksheet.Cells[i, 11].Value != null) && (xlWorksheet.Cells[i, 12].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 12].Value.ToString().Trim() != ""))
                        //{

                            drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                            drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[i, 12].Value);
                        //}
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SpecilMonth and SpecialDay can not blank";
                        //    return;
                        //}


                        if (((xlWorksheet.Cells[i, 18].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 18].Value) != string.Empty))
                        {
                            drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 18].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  calflag can not blank";
                            return;
                        }
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 3].Value).Trim().Equals("Timebased"))
                    {

                        if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                        {
                            drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                            return;
                        }
                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                                return;
                            }
                        }
                    }

                    if (((xlWorksheet.Cells[i, 13].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 13].Value) != string.Empty))
                    {
                        drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[i, 13].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderType can not blank(Standard,Custom)";
                        return;
                    }
                    if (Convert.ToString(xlWorksheet.Cells[i, 13].Value).Trim().Equals("Standard"))
                    {
                    }
                    else
                    {
                        if (((xlWorksheet.Cells[i, 17].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 17].Value) != string.Empty))
                        {
                            drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[i,17].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  OneTime Date can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 14].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 14].Value) != string.Empty))
                        {
                            drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderBefore can not blank";
                            return;
                        }
                        if (((xlWorksheet.Cells[i, 13].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 13].Value) != string.Empty))
                        {
                            drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[i, 15].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  ReminderGap can not blank";
                            return;
                        }
                    }

                    if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                    {
                        drCompliance["IsDocumentRequired"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  IsDocumentRequired not blank('Yes'/'No')";
                        return;
                    }

                    if (((xlWorksheet.Cells[i, 20].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 20].Value) != string.Empty))
                    {
                        drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[i, 20].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Detailed Description can not blank";
                        return;
                    }

                    dtCompliance.Rows.Add(drCompliance);

                    grdCompliance.DataSource = dtCompliance;
                    grdCompliance.DataBind();
                }
                suucess = true;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ValidateCheckListData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["InternalChecklist"];
                int xlrow = xlWorksheet.Dimension.End.Row;
                int filledrow = 0;
                for (int i = 1; i <= xlrow; i++)
                {
                    if ((xlWorksheet.Cells[i, 1].Value) != null)
                    {
                        filledrow = filledrow + 1;
                    }
                }
                createDataTable();
                for (int i = 2; i <= filledrow; i++)
                {
                    if (((xlWorksheet.Cells[i, 6].Value) == null) && (Convert.ToString(xlWorksheet.Cells[i, 6].Value) == string.Empty))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + "  <br />  Compliance ShortDescription can not blank";
                        return;
                    }

                    DataRow drCompliance = dtCompliance.NewRow();
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((xlWorksheet.Cells[i, 1].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 1].Value) != string.Empty))
                    {
                        drCompliance["IComplianceTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i,6].Value) + " <br />  Compliance type can not blank";
                        return;
                    }
                    if (((xlWorksheet.Cells[i, 2].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 2].Value) != string.Empty))
                    {
                        drCompliance["IComplianceCategoryID"] = Convert.ToString(xlWorksheet.Cells[i, 2].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Compliance Category can not blank";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 3].Value) == "Checklist"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Compliance Type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 4].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "FixedGap") || (Convert.ToString(xlWorksheet.Cells[i, 4].Value) == "PeriodicallyBased"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Timely based compliance";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Function based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "One Time based Checklist") || (Convert.ToString(xlWorksheet.Cells[i, 5].Value) == "Time Based Checklist"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct CheckListType";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 11].Value) == string.Empty) || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Annual") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "HalfYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Quarterly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "FourMonthly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "TwoYearly")
                      || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "SevenYearly") || (Convert.ToString(xlWorksheet.Cells[i, 11].Value) == "Monthly"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Frequency";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 15].Value) == "Standard") || (Convert.ToString(xlWorksheet.Cells[i, 15].Value) == "Custom"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Reminder type";
                        return;
                    }

                    if ((Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "High") || (Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "Medium") || (Convert.ToString(xlWorksheet.Cells[i, 18].Value) == "Low"))
                    {
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  Please enter correct Risk Type";
                        return;
                    }




                    complianceData.CustomerID = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);
                    drCompliance["CustomerID"] = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);

                    if (((xlWorksheet.Cells[i, 3].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 3].Value) != string.Empty))
                    {
                        drCompliance["IComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 3].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Compliance Type can not blank(Checklist)";
                        return;
                    }

                    drCompliance["IShortDescription"] = Convert.ToString(xlWorksheet.Cells[i, 6].Value);
                    complianceData.IShortDescription = Convert.ToString(xlWorksheet.Cells[i, 6].Value);

                    if (((xlWorksheet.Cells[i, 8].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 8].Value) != string.Empty))
                    {
                          drCompliance["IUploadDocument"] = Convert.ToString(xlWorksheet.Cells[i, 8].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Upload Document can not blank(Yes,No)";
                        return;
                    }

                    drCompliance["IRequiredFrom"] = Convert.ToString(xlWorksheet.Cells[i, 9].Value);

                    if (((xlWorksheet.Cells[i, 18].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 18].Value) != string.Empty))
                    {

                        drCompliance["IRiskType"] = Convert.ToString(xlWorksheet.Cells[i, 18].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Risk Type can not blank(High,Low,Medium)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Function based Checklist"))
                    {
                        if (((xlWorksheet.Cells[i, 11].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 11].Value) != string.Empty))
                        {

                            drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Frequency can not blank";
                            return;
                        }

                        if (((xlWorksheet.Cells[i, 12].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 12].Value) != string.Empty))
                        {

                            drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 12].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  DueDate can not blank";
                            return;
                        }

                        //if ((xlWorksheet.Cells[i, 11].Value != null && xlWorksheet.Cells[i, 11].Value != null) && (xlWorksheet.Cells[i, 12].Value.ToString().Trim() != "" && xlWorksheet.Cells[i, 12].Value.ToString().Trim() != ""))
                        //{

                        drCompliance["specilMonth"] = Convert.ToString(xlWorksheet.Cells[i, 13].Value);
                        drCompliance["specialDay"] = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                        //}
                        //else
                        //{
                        //    cvDuplicateEntry.IsValid = false;
                        //    cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SpecilMonth and SpecialDay can not blank";
                        //    return;
                        //}

                        if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                        {
                            drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  calflag can not blank";
                            return;
                        }
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i, 5].Value).Trim().Equals("Time Based Checklist"))
                    {
                        if (Convert.ToString(xlWorksheet.Cells[i, 4].Value).Trim().Equals("FixedGap"))
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["IDueDate"] = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  DueDate can not blank";
                                return;
                            }
                        }
                        else
                        {
                            if (((xlWorksheet.Cells[i, 4].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 4].Value) != string.Empty))
                            {
                                drCompliance["ISubComplianceType"] = Convert.ToString(xlWorksheet.Cells[i, 4].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 5].Value) + " <br />  SubComplianceType can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 19].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 19].Value) != string.Empty))
                            {
                                drCompliance["calflag"] = Convert.ToString(xlWorksheet.Cells[i, 19].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  calflag can not blank";
                                return;
                            }

                            if (((xlWorksheet.Cells[i, 11].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 11].Value) != string.Empty))
                            {

                                drCompliance["IFrequency"] = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Frequency can not blank";
                                return;
                            }
                        }
                    }

                    if (((xlWorksheet.Cells[i, 15].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 15].Value) != string.Empty))
                    {
                        drCompliance["IReminderType"] = Convert.ToString(xlWorksheet.Cells[i, 15].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderType can not blank(Standard,Custom)";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i,15].Value).Trim().Equals("Standard"))
                    {
                    }
                    else
                    {

                        if (((xlWorksheet.Cells[i, 16].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 16].Value) != string.Empty))
                        {
                            drCompliance["IReminderBefore"] = Convert.ToString(xlWorksheet.Cells[i, 16].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderBefore can not blank";
                            return;
                        }
                        if (((xlWorksheet.Cells[i, 17].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 17].Value) != string.Empty))
                        {
                            drCompliance["IReminderGap"] = Convert.ToString(xlWorksheet.Cells[i, 17].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  ReminderGap can not blank";
                            return;
                        }
                    }


                    if (((xlWorksheet.Cells[i, 5].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 5].Value) != string.Empty))
                    {
                        drCompliance["CheckListTypeID"] = Convert.ToString(xlWorksheet.Cells[i, 5].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  CheckListType can not blank";
                        return;
                    }

                    if (Convert.ToString(xlWorksheet.Cells[i,5].Value).Trim().Equals("One Time based Checklist"))
                    {
                        if (((xlWorksheet.Cells[i, 7].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 7].Value) != string.Empty))
                        {
                            drCompliance["IOneTimeDate"] = Convert.ToString(xlWorksheet.Cells[i, 7].Value);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i,6].Value) + " <br />  OneTime Date can not blank";
                            return;
                        }
                    }

                    if (((xlWorksheet.Cells[i, 20].Value) != null) && (Convert.ToString(xlWorksheet.Cells[i, 20].Value) != string.Empty))
                    {
                        drCompliance["IDetailedDescription"] = Convert.ToString(xlWorksheet.Cells[i, 20].Value);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record No -" + (i - 1) + " <br /> Short Description -" + Convert.ToString(xlWorksheet.Cells[i, 6].Value) + " <br />  Detailed Description can not blank";
                        return;
                    }

                    complianceData.CreatedOn = DateTime.Now;
                    complianceData.IsDeleted = false;

                    dtCompliance.Rows.Add(drCompliance);

                    grdCheckList.DataSource = dtCompliance;
                    grdCheckList.DataBind();
                }
                CheckListsuucess = true;
            }
            catch (Exception ex)
            {
                CheckListsuucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ProcessComplianceData()
        {
            try
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance> complianceList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance>();

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                for (int i = 0; i <= grdCompliance.Rows.Count-1; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((grdCompliance.Rows[i].Cells[0].Text) != null) && (Convert.ToString(grdCompliance.Rows[i].Cells[0].Text) != string.Empty))
                    {
                        List<InternalComplianceType> complianceTypeList = new List<InternalComplianceType>();
                        if (!(ComplianceTypeManagement.ExistsInternalComplianceType(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID)))
                        {
                            InternalComplianceType complianceType = new InternalComplianceType();
                            complianceType.Name = Convert.ToString(grdCompliance.Rows[i].Cells[0].Text);
                            complianceType.CustomerID = customerID; //Convert.ToInt32(ddlCustomer.SelectedValue);
                            complianceTypeList.Add(complianceType);
                            ComplianceTypeManagement.Create(complianceTypeList);
                            complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID);

                        }
                        else
                        {
                            complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[0].Text), customerID );
                        }

                        List<InternalCompliancesCategory> complianceCategoryList = new List<InternalCompliancesCategory>();
                        if (!(ComplianceCategoryManagement.ExistsInternalComplianceCategory(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text), customerID)))
                        {
                            InternalCompliancesCategory Category = new InternalCompliancesCategory();
                            Category.Name = Convert.ToString(grdCompliance.Rows[i].Cells[1].Text);
                            Category.CustomerID = customerID;// Convert.ToInt32(ddlCustomer.SelectedValue);
                            complianceCategoryList.Add(Category);
                            ComplianceCategoryManagement.CreateInternalCompliancesCategory(complianceCategoryList);
                            complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text), customerID);
                        }
                        else
                        {
                            complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCompliance.Rows[i].Cells[1].Text),customerID);
                        }

                        complianceData.CustomerID = customerID;// Convert.ToInt32(ddlCustomer.SelectedValue);

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Functionbased"))
                        {

                            complianceData.IComplianceType = 0;
                        }
                        else if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Timebased"))
                        {
                            complianceData.IComplianceType = 2;
                        }
                        else
                        {
                            complianceData.IComplianceType = 0;
                        }

                        complianceData.IShortDescription = Convert.ToString(grdCompliance.Rows[i].Cells[4].Text);

                        complianceData.IDetailedDescription = Convert.ToString(grdCompliance.Rows[i].Cells[17].Text);

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[5].Text).Trim().Equals("Yes"))
                        {
                            complianceData.IUploadDocument = true;
                        }
                        else
                        {
                            complianceData.IUploadDocument = false;
                        }

                        complianceData.IRequiredFrom = Convert.ToString(grdCompliance.Rows[i].Cells[6].Text);

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[13].Text).Trim().Equals("High"))
                        {
                            complianceData.IRiskType = 0;
                        }
                        else if (Convert.ToString(grdCompliance.Rows[i].Cells[13].Text).Trim().Equals("Medium"))
                        {
                            complianceData.IRiskType = 1;
                        }
                        else
                        {
                            complianceData.IRiskType = 2;
                        }

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Functionbased"))
                        {
                            complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text)));
                            if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                            {
                                complianceData.IDueDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text);
                            }
                            int step = 1;
                            switch ((Frequency) Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text))))
                            {
                                case Frequency.Monthly:
                                    step = 1;
                                    break;
                                case Frequency.Quarterly:
                                    step = 3;
                                    break;
                                case Frequency.FourMonthly:
                                    step = 4;
                                    break;
                                case Frequency.HalfYearly:
                                    step = 6;
                                    break;
                                case Frequency.Annual:
                                    step = 12;
                                    break;
                                default:
                                    step = 12;
                                    break;
                            }
                            int specilMonth = 0;
                            int specialDay = 0;
                            if ((grdCompliance.Rows[i].Cells[11].Text != null && grdCompliance.Rows[i].Cells[11].Text != "&nbsp;" && grdCompliance.Rows[i].Cells[12].Text != null) && (grdCompliance.Rows[i].Cells[11].Text.ToString().Trim() != "" && grdCompliance.Rows[i].Cells[12].Text != "&nbsp;" && grdCompliance.Rows[i].Cells[12].Text.ToString().Trim() != ""))
                            {
                                specilMonth = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text);
                                specialDay = Convert.ToInt32(grdCompliance.Rows[i].Cells[12].Text);
                            }
                            List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                            int SpecialMonth;
                            string calflag = Convert.ToString(grdCompliance.Rows[i].Cells[15].Text);
                            var frequency= complianceData.IFrequency;
                            if (calflag == "Y" && complianceData.IFrequency !=1 && complianceData.IFrequency != 0)
                            {
                                for (int month = 4; month <= 12; month += step)
                                {
                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 10;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 8;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 8)
                                    {
                                        SpecialMonth = 12;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 3 && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 3 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if ((complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }

                                    //int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                    //if (Convert.ToInt32(compliance.IDueDate.Value.ToString("D2")) > lastdate)
                                    //{
                                    //    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    //}
                                    //else
                                    //{
                                    //    complianceShedule.SpecialDate = compliance.IDueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                    //}

                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                    if (complianceData.IDueDate > lastdate)
                                    {
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {
                                        if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCompliance.Rows[i].Cells[10].Text).ToString("D2");
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                    }
                                    else
                                    {
                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                    }
                                }
                            }//Financial Year close
                            else
                            {
                                for (int month = 1; month <= 12; month += step)
                                {

                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 7)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 9)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if ((complianceData.IFrequency == 3 || complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                    if (complianceData.IDueDate > lastdate)
                                    {
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {

                                        if (grdCompliance.Rows[i].Cells[9].Text != null && grdCompliance.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCompliance.Rows[i].Cells[10].Text).ToString("D2");
                                            }
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                            complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                }
                            }//Calender Year Close
                        }

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[2].Text).Trim().Equals("Timebased"))
                        {


                            if (Convert.ToString(grdCompliance.Rows[i].Cells[3].Text).Trim().Equals("FixedGap"))
                            {
                                complianceData.ISubComplianceType = 0;//fixed gap
                                complianceData.IDueDate = Convert.ToInt32(grdCompliance.Rows[i].Cells[9].Text);
                            }
                            else
                            {
                                complianceData.ISubComplianceType = 1;//periodically
                                int step = 1;
                                complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text)));
                                switch ((Frequency) Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCompliance.Rows[i].Cells[8].Text))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                //int SpecialMonth;
                                List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                                for (int month = 1; month <= 12; month += step)
                                {
                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;
                                    //}

                                    //if (subcomplincetype == 1 || subcomplincetype == 2)
                                    //{
                                    int monthCopy = month;
                                    if (monthCopy == 1)
                                    {
                                        monthCopy = 12;
                                        complianceShedule.ForMonth = monthCopy;
                                    }
                                    else
                                    {
                                        monthCopy = month - 1;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    //if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)//Annual,TwoYearly,SevenYearly
                                    if (Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("Annual") || Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("TwoYearly") || Convert.ToString(grdCompliance.Rows[i].Cells[8].Text).Trim().Equals("SevenYearly"))
                                    {
                                        monthCopy = 3;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                    complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                    //complianceData.ComplianceSchedules.Add(complianceShedule);
                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);

                                    //}
                                }

                            }
                        }
                        if (Convert.ToString(grdCompliance.Rows[i].Cells[10].Text).Trim().Equals("Standard"))
                        {
                            complianceData.IReminderType = 0;//standard
                        }
                        else
                        {
                            complianceData.IReminderType = 1;//custom
                            complianceData.IOneTimeDate = Convert.ToDateTime(grdCompliance.Rows[i].Cells[14].Text);
                            complianceData.IReminderBefore = Convert.ToInt32(grdCompliance.Rows[i].Cells[11].Text);
                            complianceData.IReminderGap = Convert.ToInt32(grdCompliance.Rows[i].Cells[12].Text);
                        }

                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;

                        if (Convert.ToString(grdCompliance.Rows[i].Cells[7].Text).Trim().Equals("Recurring"))
                        {
                            complianceData.IComplianceOccurrence = 1;
                        }
                        else
                        {
                            complianceData.IComplianceOccurrence = 0;
                        }
                        string IsDocumentRequired = Convert.ToString(grdCompliance.Rows[i].Cells[16].Text);
                        if (IsDocumentRequired == "Yes")
                        {
                            complianceData.IsDocumentRequired = true;
                        }
                        else
                        {
                            complianceData.IsDocumentRequired = false;
                        }

                        complianceData.CreatedBy = AuthenticationHelper.UserID;
                        complianceData.UpdatedBy = AuthenticationHelper.UserID;
                    }
                    complianceList.Add(complianceData);
                }
                Business.ComplianceManagement.CreateInternal(complianceList, customerID);
                suucessSave = true;
            }
            catch (Exception ex)
            {
                suucessSave = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ProcessCheckListData()
        {
            try
            {
                List<Business.Data.InternalCompliance> complianceList = new List<Business.Data.InternalCompliance>();
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                for (int i = 0; i <= grdCheckList.Rows.Count - 1; i++)
                {
                    com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance complianceData = new com.VirtuosoITech.ComplianceManagement.Business.Data.InternalCompliance();
                    if (((grdCheckList.Rows[i].Cells[0].Text) != null) && (Convert.ToString(grdCheckList.Rows[i].Cells[0].Text) != string.Empty))
                    {
                        List<InternalComplianceType> complianceTypeList = new List<InternalComplianceType>();
                        if (!(ComplianceTypeManagement.ExistsInternalComplianceType(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID)))
                        {
                            InternalComplianceType complianceType = new InternalComplianceType();
                            complianceType.Name = Convert.ToString(grdCheckList.Rows[i].Cells[0].Text);
                            complianceType.CustomerID = customerID;
                            complianceTypeList.Add(complianceType);
                            ComplianceTypeManagement.Create(complianceTypeList);
                            complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID);
                        }
                        else
                        {
                            complianceData.IComplianceTypeID = ActManagement.GetInternalComplianceTypeIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[0].Text), customerID);
                        }

                        List<InternalCompliancesCategory> complianceCategoryList = new List<InternalCompliancesCategory>();
                        if (!(ComplianceCategoryManagement.ExistsInternalComplianceCategory(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID)))
                        {
                            InternalCompliancesCategory Category = new InternalCompliancesCategory();
                            Category.Name = Convert.ToString(grdCheckList.Rows[i].Cells[1].Text);
                            Category.CustomerID = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);
                            complianceCategoryList.Add(Category);
                            ComplianceCategoryManagement.CreateInternalCompliancesCategory(complianceCategoryList);
                            complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID);
                        }
                        else
                        {
                            complianceData.IComplianceCategoryID = ActManagement.GetInternalComplianceCategoryIDByName(Convert.ToString(grdCheckList.Rows[i].Cells[1].Text), customerID);
                        }

                        complianceData.CustomerID = customerID;// Convert.ToInt32(ddlchecklistCustomer.SelectedValue);

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[2].Text).Trim().Equals("Function based Checklist"))
                        {

                            complianceData.IComplianceType = 0;
                        }
                        else if (Convert.ToString(grdCheckList.Rows[i].Cells[2].Text).Trim().Equals("Time Based Checklist"))
                        {
                            complianceData.IComplianceType = 2;
                        }
                        else
                        {
                            complianceData.IComplianceType = 1;
                        }

                        complianceData.IShortDescription = Convert.ToString(grdCheckList.Rows[i].Cells[4].Text);

                        complianceData.IDetailedDescription = Convert.ToString(grdCheckList.Rows[i].Cells[19].Text);

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[5].Text).Trim().Equals("Yes"))
                        {
                            complianceData.IUploadDocument = true;
                        }
                        else
                        {
                            complianceData.IUploadDocument = false;
                        }

                        complianceData.IRequiredFrom = Convert.ToString(grdCheckList.Rows[i].Cells[6].Text);

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[15].Text).Trim().Equals("High"))
                        {
                            complianceData.IRiskType = 0;
                        }
                        else if (Convert.ToString(grdCheckList.Rows[i].Cells[15].Text).Trim().Equals("Medium"))
                        {
                            complianceData.IRiskType = 1;
                        }
                        else
                        {
                            complianceData.IRiskType = 2;
                        }

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Function based Checklist"))
                        {
                            complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text)));
                            if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                            {
                                complianceData.IDueDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text);
                            }
                            int step = 1;
                            switch ((Frequency) Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text))))
                            {
                                case Frequency.Monthly:
                                    step = 1;
                                    break;
                                case Frequency.Quarterly:
                                    step = 3;
                                    break;
                                case Frequency.FourMonthly:
                                    step = 4;
                                    break;
                                case Frequency.HalfYearly:
                                    step = 6;
                                    break;
                                case Frequency.Annual:
                                    step = 12;
                                    break;
                                default:
                                    step = 12;
                                    break;
                            }
                            int specilMonth = 0;
                            int specialDay = 0;
                            if ((grdCheckList.Rows[i].Cells[10].Text != null && grdCheckList.Rows[i].Cells[10].Text != "&nbsp;" && grdCheckList.Rows[i].Cells[10].Text != null) && (grdCheckList.Rows[i].Cells[11].Text.ToString().Trim() != "" && grdCheckList.Rows[i].Cells[11].Text != "&nbsp;" && grdCheckList.Rows[i].Cells[11].Text.ToString().Trim() != ""))
                            {
                                specilMonth = Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text);
                                specialDay = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text);
                            }
                            List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();

                            int SpecialMonth;
                            #region Financial Year And Calender
                            string calflag = Convert.ToString(grdCheckList.Rows[i].Cells[18].Text);
                            if (calflag == "Y")
                            {
                                for (int month = 4; month <= 12; month += step)
                                {
                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 10;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 8;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 8)
                                    {
                                        SpecialMonth = 12;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 3 && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if (complianceData.IFrequency == 3 && SpecialMonth == 4)
                                    {
                                        SpecialMonth = 4;
                                    }
                                    else if ((complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                    if (complianceData.IDueDate > lastdate)
                                    {
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {
                                        if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text).ToString("D2");
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                    }
                                    else
                                    {
                                        complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                    }
                                }
                            }//Financial Year close
                            else
                            {
                                for (int month = 1; month <= 12; month += step)
                                {

                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    SpecialMonth = month;
                                    if (complianceData.IFrequency == 0 && SpecialMonth == 12)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 1 && SpecialMonth == 10)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 2 && SpecialMonth == 7)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if (complianceData.IFrequency == 4 && SpecialMonth == 9)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else if ((complianceData.IFrequency == 3 || complianceData.IFrequency == 5 || complianceData.IFrequency == 6) && SpecialMonth == 1)
                                    {
                                        SpecialMonth = 1;
                                    }
                                    else
                                    {
                                        SpecialMonth = SpecialMonth + step;
                                    }
                                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                    if (complianceData.IDueDate > lastdate)
                                    {
                                        complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                    }
                                    else
                                    {

                                        if (grdCheckList.Rows[i].Cells[9].Text != null && grdCheckList.Rows[i].Cells[9].Text.ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                            {
                                                //string negativevalue = Convert.ToString(xlWorksheet.Cells[i, 17].Value).Trim('-');                                               
                                                //complianceShedule.SpecialDate = Convert.ToInt32(negativevalue).ToString("D2") + SpecialMonth.ToString("D2");
                                            }
                                            else
                                            {
                                                complianceShedule.SpecialDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[11].Text).ToString("D2") + Convert.ToInt32(grdCheckList.Rows[i].Cells[10].Text).ToString("D2");
                                            }
                                        }
                                    }

                                    if (specilMonth == month)
                                    {
                                        if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceShedule.SpecialDate = specialDay.ToString("D2") + month.ToString("D2");
                                            complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text) < 0)
                                        {
                                        }
                                        else
                                        {
                                            complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                        }
                                    }
                                }
                            }//Calender Year Close

                            #endregion
                        }
                        #region Time Based
                        if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Time Based Checklist"))
                        {
                            if (Convert.ToString(grdCheckList.Rows[i].Cells[3].Text).Trim().Equals("FixedGap"))
                            {
                                complianceData.ISubComplianceType = 0;//fixed gap
                                complianceData.IDueDate = Convert.ToInt32(grdCheckList.Rows[i].Cells[9].Text);
                            }
                            else
                            {
                                complianceData.ISubComplianceType = 1;//periodically
                                int step = 1;
                                complianceData.IFrequency = Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text)));
                                switch ((Frequency) Convert.ToByte(Enumerations.GetEnumByName<Frequency>(Convert.ToString(grdCheckList.Rows[i].Cells[8].Text))))
                                {
                                    case Frequency.Monthly:
                                        step = 1;
                                        break;
                                    case Frequency.Quarterly:
                                        step = 3;
                                        break;
                                    case Frequency.FourMonthly:
                                        step = 4;
                                        break;
                                    case Frequency.HalfYearly:
                                        step = 6;
                                        break;
                                    case Frequency.Annual:
                                        step = 12;
                                        break;
                                    default:
                                        step = 12;
                                        break;
                                }
                                List<InternalComplianceSchedule> complianceSchedule = new List<InternalComplianceSchedule>();
                                for (int month = 1; month <= 12; month += step)
                                {
                                    InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                                    complianceShedule.ForMonth = month;

                                    int monthCopy = month;
                                    if (monthCopy == 1)
                                    {
                                        monthCopy = 12;
                                        complianceShedule.ForMonth = monthCopy;
                                    }
                                    else
                                    {
                                        monthCopy = month - 1;
                                        complianceShedule.ForMonth = monthCopy;
                                    }
                                    if (Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("Annual") || Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("TwoYearly") || Convert.ToString(grdCheckList.Rows[i].Cells[8].Text).Trim().Equals("SevenYearly"))
                                    {
                                        monthCopy = 3;
                                        complianceShedule.ForMonth = monthCopy;
                                    }

                                    int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                    complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                                    complianceData.InternalComplianceSchedules.Add(complianceShedule);
                                }

                            }
                        }
                        if (Convert.ToString(grdCheckList.Rows[i].Cells[12].Text).Trim().Equals("Standard"))
                        {
                            complianceData.IReminderType = 0;//standard
                        }
                        else
                        {
                            complianceData.IReminderType = 1;//custom
                            complianceData.IReminderBefore = Convert.ToInt32(grdCheckList.Rows[i].Cells[13].Text);
                            complianceData.IReminderGap = Convert.ToInt32(grdCheckList.Rows[i].Cells[14].Text);
                        }

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Function based Checklist"))
                        {
                            complianceData.CheckListTypeID = 1;
                        }
                        else if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("Time Based Checklist"))
                        {
                            complianceData.CheckListTypeID = 2;
                        }
                        else
                        {
                            complianceData.CheckListTypeID = 0;
                        }

                        complianceData.CreatedOn = DateTime.Now;
                        complianceData.UpdatedOn = DateTime.Now;
                        complianceData.IsDeleted = false;
                        complianceData.IComplianceOccurrence = 1;
                        complianceData.CreatedBy = AuthenticationHelper.UserID;
                        complianceData.UpdatedBy = AuthenticationHelper.UserID;

                        if (Convert.ToString(grdCheckList.Rows[i].Cells[17].Text).Trim().Equals("One Time based Checklist"))
                        {
                            complianceData.IOneTimeDate = Convert.ToDateTime(grdCheckList.Rows[i].Cells[16].Text);
                            complianceData.IComplianceOccurrence = 0;
                        }
                        #endregion reminder                 
                       
                    }
                    complianceList.Add(complianceData);
                }
                Business.ComplianceManagement.CreateInternal(complianceList,customerID);
                CheckListsuucessSave = true;
            }
            catch (Exception ex)
            {
                CheckListsuucessSave = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void createDataTable()
        {
            dtCompliance = new DataTable();
            dtCompliance.Columns.Add("CustomerID");
            dtCompliance.Columns.Add("IComplianceCategoryID");
            dtCompliance.Columns.Add("IComplianceTypeID");
            dtCompliance.Columns.Add("IShortDescription");
            dtCompliance.Columns.Add("IComplianceType");
            dtCompliance.Columns.Add("IUploadDocument");
            dtCompliance.Columns.Add("IRequiredFrom");
            dtCompliance.Columns.Add("IFrequency");
            dtCompliance.Columns.Add("IDueDate");
            dtCompliance.Columns.Add("IReminderType");
            dtCompliance.Columns.Add("IRiskType");
            dtCompliance.Columns.Add("IReminderBefore");
            dtCompliance.Columns.Add("IReminderGap");
            dtCompliance.Columns.Add("ISubComplianceType");
            dtCompliance.Columns.Add("calflag");
            dtCompliance.Columns.Add("specilMonth");
            dtCompliance.Columns.Add("specialDay");
            //dtCompliance.Columns.Add("IsDeleted");
            //dtCompliance.Columns.Add("CreatedOn");
            //dtCompliance.Columns.Add("CreatedBy");
            //dtCompliance.Columns.Add("UpdatedOn");
            //dtCompliance.Columns.Add("UpdatedBy");
            dtCompliance.Columns.Add("IComplianceOccurrence");
            dtCompliance.Columns.Add("IOneTimeDate");
            dtCompliance.Columns.Add("CheckListTypeID");
            dtCompliance.Columns.Add("EffectiveDate");
            dtCompliance.Columns.Add("IsDocumentRequired");
            dtCompliance.Columns.Add("IDetailedDescription");
        }

        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            MainView.ActiveViewIndex = 0;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessComplianceData();
                btnSave.Enabled = false;

                if (suucessSave == true)
                {
                    grdCompliance.DataSource = null;
                    grdCompliance.DataBind();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Data save successfully.";
                    btnSave.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChecklistUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterCheckListFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterCheckListFileUpload.FileName);
                    MasterCheckListFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());

                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = ActSheetsExitsts(xlWorkbook, "InternalChecklist");
                            if (flag == true)
                            {
                                ValidateCheckListData(xlWorkbook);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No data found in file.";
                            }
                            if (CheckListsuucess == true)
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Excelsheet data verified successfully.";
                                btnCheckListSave.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";

                    }


                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }

        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex =1;
        }
        protected void btnCheckListSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessCheckListData();
                btnCheckListSave.Enabled = false;

                if (CheckListsuucessSave == true)
                {
                    grdCheckList.DataSource = null;
                    grdCheckList.DataBind();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Data save successfully.";
                    btnCheckListSave.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnExcelFormat_Click(object sender, EventArgs e)
        {
            string FileName = "Internal Compliance Upload Format.xlsx";

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "image/jpeg";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(Server.MapPath("~/ExcelFormat/Internal Compliance Upload Format.xlsx"));
            response.Flush();
            response.End();
        }
    }
    #endregion
}