﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using OfficeOpenXml;
using System.Web;
using System.Globalization;
using System.Configuration;
using System.Threading;
using OfficeOpenXml.Style;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ExportCountExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (AuthenticationHelper.UserID != 203)
                //{
                //    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                //}
                //else
                //{      
                BindUsers(ddlUsers);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                ddlUsers_SelectedIndexChanged(sender,e);
                //}
            }
        }

        private void BindUsers(DropDownList ddlUserList)
        {
            try
            {
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllImplementaionUsers();
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< All >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rbtnlstActOrCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (rbtnlstActOrCompliance.SelectedValue.Equals("0"))
                //{
                    BindGridviewAct();
                //}
                //else
                //{
                //    BindGridviewCompliance();
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
         
            }

        }

        protected void BindGridviewAct()
        {
            divAct.Visible = true;
            //divComp.Visible = false;
            grdAct.DataSource = ActManagement.GetAll(-1, -1, null);
            grdAct.DataBind();
            //---------------------------------------------------------
        }

        //protected void BindGridviewCompliance()
        //{                
        //    divAct.Visible = false;
        //    //divComp.Visible = true;

        //    var compliancesData = Business.ComplianceManagement.GetAllExcelExport(null);
            
        //    List<object> dataSource = new List<object>();
        //    foreach (var complianceInfo in compliancesData)
        //    {
        //        dataSource.Add(new
        //        {
        //            complianceInfo.ActID,
        //            complianceInfo.ID,
        //            complianceInfo.ActName,
        //            complianceInfo.Sections,
        //            complianceInfo.ShortDescription,
        //            complianceInfo.Description,
        //            complianceInfo.UploadDocument,                   
        //            complianceInfo.RequiredForms,
        //            complianceInfo.Frequency,                 
        //            complianceInfo.ComplianceType,
        //            complianceInfo.SubComplianceType,
        //            complianceInfo.DueDate,
        //            complianceInfo.FixedGap,
                   
        //        });
        //    }
        //    grdCompliance.DataSource = dataSource;
        //    grdCompliance.DataBind();

            
        //}

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {

             try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("UserWiseDataCountReport");
                        DataTable ExcelData = null;
                       
                        DateTime dtfrom = DateTime.Now;
                        DateTime dtTo = DateTime.Now;
                        DateTime TempdtTo = DateTime.Now;
                        if (txtFromDate.Text == "")
                        {
                            dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            dtfrom = DateTime.ParseExact(txtFromDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture); 
                        }
                        if (txtToDate.Text == "")
                        {
                            dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            dtTo = DateTime.ParseExact(txtToDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            TempdtTo = dtTo;
                            dtTo = dtTo.AddDays(1);

                        }
                        ComplianceDBEntities entities = new ComplianceDBEntities();

                        var LoanFill = entities.SP_GetComplianceCount(Convert.ToInt32(ddlUsers.SelectedValue), dtfrom, dtTo).ToList(); 
                        if (LoanFill != null)
                        {
                            grdAct.DataSource = LoanFill;
                            grdAct.DataBind();
                        }

                        DataView view = new System.Data.DataView((grdAct.DataSource as List<SP_GetComplianceCount_Result>).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "Name", "ActCount", "ComplianceCount", "ChecklistCount");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A2"].Value = "Date Wise Data Entry";
                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Style.Font.Size = 15;

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Style.Font.Size = 12;

                        if (txtFromDate.Text == "")
                        {
                             exWorkSheet.Cells["A3"].Value = "";
                        }
                        else
                        {
                            exWorkSheet.Cells["A3"].Value = "Date";
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        }

                        if (txtFromDate.Text == "")
                        {
                            exWorkSheet.Cells["B3"].Value = "";
                        }
                        else
                        {
                            exWorkSheet.Cells["B3"].Value = dtfrom.ToString("dd-MMM-yyyy") + " To " + TempdtTo.ToString("dd-MMM-yyyy");
                            exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B3"].Style.Font.Bold = true;
                        }
                   
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                    
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].Value = "Act Count"; 

                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Value = "Compliance Count"; 

                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Checklist Count";

                        int colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                        exWorkSheet.Column(colIdx).Width = 40;

                        colIdx = ExcelData.Columns.IndexOf("ActCount") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("complianceCount") + 1;
                        exWorkSheet.Column(colIdx).Width = 25;

                        colIdx = ExcelData.Columns.IndexOf("ChecklistCount") + 1;
                        exWorkSheet.Column(colIdx).Width = 25;


                        using (ExcelRange col = exWorkSheet.Cells[4, 1, 4 + ExcelData.Rows.Count, 4])
                        {
                            col.Style.WrapText = true;
                            //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        //using (ExcelRange col = exWorkSheet.Cells[5, 1, 6 + ExcelData.Rows.Count, 1])
                        //{
                        //    //col.Style.Numberformat.Format = "dd/MM/yyyy";
                        //    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //    col.AutoFitColumns();
                        //}

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=DataEntryReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                    }
                    catch (Exception ex)
                    {
                    }
               }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

            //try
            //{
            //    //if (rbtnlstActOrCompliance.SelectedValue.Equals("0"))
            //    //{
            //        //..Act
            //        Response.ClearContent();
            //        Response.Buffer = true;
            //        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

            //        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ActsExported.xls"));
            //        Response.ContentType = "application/ms-excel";
            //        StringWriter sw = new StringWriter();
            //        HtmlTextWriter htw = new HtmlTextWriter(sw);
            //        grdAct.AllowPaging = false;
            //        BindGridviewAct();
            //        //Change the Header Row back to white color
            //        grdAct.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //        //Applying stlye to gridview header cells
            //        for (int i = 0; i < grdAct.HeaderRow.Cells.Count; i++)
            //        {
            //            grdAct.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            //        }
            //        grdAct.RenderControl(htw);
            //        Response.Write(sw.ToString());
            //        Response.End();
            //        //-----------------------------------------------------------------------------------------------------
            //    //}
            //    //else
            //    //{

            //    //    Response.ClearContent();
            //    //    Response.Buffer = true;
            //    //    //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

            //    //    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ComplianceExported.xls"));
            //    //    Response.ContentType = "application/ms-excel";
            //    //    StringWriter sw = new StringWriter();
            //    //    HtmlTextWriter htw = new HtmlTextWriter(sw);
            //    //    //grdCompliance.AllowPaging = false;
            //    //    //BindGridviewCompliance();
            //    //    //Change the Header Row back to white color
            //    //    //grdCompliance.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //    //    //Applying stlye to gridview header cells
            //    //    //for (int i = 0; i < grdCompliance.HeaderRow.Cells.Count; i++)
            //    //    //{
            //    //    //    grdCompliance.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            //    //    //}
            //    //    //grdCompliance.RenderControl(htw);
            //    //    Response.Write(sw.ToString());
            //    //    Response.End();
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
     
            //}
        //}

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                if(txtFromDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture); 
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtFromDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture); //Convert.ToDateTime(txtFromDate.Text);
                }
                if (txtToDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture); 
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtToDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);  //Convert.ToDateTime(txtToDate.Text);
                    dtTo = dtTo.AddDays(1); 
                }
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var compliance = entities.SP_GetComplianceCount(Convert.ToInt32(ddlUsers.SelectedValue),dtfrom, dtTo).ToList();
                grdAct.DataSource = compliance;
                grdAct.DataBind();
                 // BindGridviewAct();
            }
            catch (Exception ex)
            {
                  LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}