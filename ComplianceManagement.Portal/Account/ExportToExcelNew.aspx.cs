﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class ExportToExcelNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGridviewCompliance();
            }
        }        
       
        
        public static List<DemoDailyUpdate> GetAllExcelExport(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.DemoDailyUpdates
                                      orderby row.ID
                                      select row).ToList();


                return complianceList;
            }
        }
        protected void BindGridviewCompliance()
        {                
           
            divComp.Visible = true;

            var compliancesData = GetAllExcelExport(null);
            
            List<object> dataSource = new List<object>();
            foreach (var complianceInfo in compliancesData)
            {
                dataSource.Add(new
                {
                    complianceInfo.Title,
                    complianceInfo.Act,
                    complianceInfo.Description                   
                });
            }
            grdCompliance.DataSource = dataSource;
            grdCompliance.DataBind();

            
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                
                    Response.ClearContent();
                    Response.Buffer = true;
                    //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ComplianceExported.xls"));
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    grdCompliance.AllowPaging = false;
                    BindGridviewCompliance();
                    //Change the Header Row back to white color
                    grdCompliance.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < grdCompliance.HeaderRow.Cells.Count; i++)
                    {
                        grdCompliance.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                    }
                    grdCompliance.RenderControl(htw);
                    Response.Write(sw.ToString());
                    Response.End();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
     
            }
        }
        
    }
}