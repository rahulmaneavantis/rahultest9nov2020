﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void ProcessUserData(ExcelPackage xlWorkbook)
        {
            try
            {
                bool saveSuccess = false;
                int uploadedContractCount = 0;

                #region Excel
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ServiceProvider"];
                if (xlWorksheet != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int count = 0;
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        List<long> lstDepartmentIDs = new List<long>();
                        List<string> errorMessage = new List<string>();
                        string IsServiceProvider = string.Empty;
                        string CustmerName = string.Empty;
                        string Address = string.Empty;
                        string ContactName = string.Empty;
                        string ContactNumber = string.Empty;
                        string Email = string.Empty;
                        List<int> ProductList = new List<int>();

                        #region Validations
                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                        {
                            count = count + 1;
                            IsServiceProvider = string.Empty;
                            CustmerName = string.Empty;
                            Address = string.Empty;
                            ContactName = string.Empty;
                            ContactNumber = string.Empty;
                            Email = string.Empty;

                            #region 1 IsServiceProvider
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                            {
                                IsServiceProvider = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(IsServiceProvider))
                            {
                                errorMessage.Add("Required Service Provider at row number-" + rowNum);
                            }
                            #endregion
                            #region 2 CustmerName
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                            {
                                CustmerName = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                Customer customer = new Customer();
                                mst_Customer mstCustomer = new mst_Customer();
                                customer.Name = CustmerName;
                                mstCustomer.Name = CustmerName;
                                if (!CustomerManagement.ExistsbyName(customer))
                                {
                                    errorMessage.Add("Customer Name already exist at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!CustomerManagementRisk.ExistsbyName(mstCustomer))
                                    {
                                        errorMessage.Add("Customer Name already exist at row number-" + rowNum);
                                    }
                                }
                            }
                            if (String.IsNullOrEmpty(CustmerName))
                            {
                                errorMessage.Add("Required Customer Name at row number-" + rowNum);
                            }
                            #endregion
                            #region 3 Address
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                            {
                                Address = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                            }
                            #endregion
                            #region 4 Contact Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                            {
                                ContactName = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                if (string.IsNullOrEmpty(ContactName))
                                {
                                    errorMessage.Add("Required Contact Name at row number- " + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required Contact Name, Correct at row number-" + rowNum);
                            }
                            #endregion
                            #region 5 Contact number
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                            {
                                bool ValidatePhone = UsersUpload.IsValidPhoneNumber(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim());
                                if (ValidatePhone == false)
                                {
                                    errorMessage.Add("Contact Number Not Proper Format at row number - " + (count + 1) + "");
                                }
                            }
                            else
                            {
                                errorMessage.Add("Contact Number required at row number - " + (count + 1) + "");

                            }

                            #endregion
                            #region 6 Email    
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                            {
                                Email = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                if (!Email.Contains('@'))
                                {
                                    errorMessage.Add("Please Correct the User(Email) at row number-" + rowNum);
                                }
                            }
                            else
                            {
                                errorMessage.Add("Required User (Email) at row number-" + rowNum);
                            }
                            #endregion

                            #region 7 Audit product
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                            {
                                string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim().ToLower();
                                if (checkvalue == "yes")
                                {
                                    ProductList.Add(4);
                                }
                            }
                            #endregion
                            #region 8 Labour Compliance
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                            {
                                string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim().ToLower();
                                if (checkvalue == "yes")
                                {
                                    ProductList.Add(9);
                                }
                            }
                            #endregion
                            #region 9 Secretarial Compliance
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                            {
                                string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim().ToLower();
                                if (checkvalue == "yes")
                                {
                                    ProductList.Add(8);
                                }
                            }
                            #endregion
                            #region 10 Legal Compliance
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                            {
                                string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim().ToLower();
                                if (checkvalue == "yes")
                                {
                                    ProductList.Add(1);
                                }
                            }
                            #endregion

                            if (ProductList.Count == 0)
                            {
                                errorMessage.Add("Atleast one Product Required at row number-" + rowNum);
                            }
                        }
                        #endregion

                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                        else
                        {
                            #region Save 
                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                IsServiceProvider = string.Empty;
                                CustmerName = string.Empty;
                                Address = string.Empty;
                                ContactName = string.Empty;
                                ContactNumber = string.Empty;
                                Email = string.Empty;
                                int serviceProviderID = 0;
                                int CustomerIDCom = 0;
                                List<int> ProductListSave = new List<int>();

                                #region 1 IsServiceProvider
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    IsServiceProvider = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                                }
                                #endregion
                                #region 2 CustmerName
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    CustmerName = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                }
                                #endregion
                                #region 3 Address
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    Address = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                }
                                #endregion
                                #region 4 Contact Name
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    ContactName = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                }
                                #endregion
                                #region 5 Contact number
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    ContactNumber = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim());
                                }
                                #endregion
                                #region 6 Email    
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    Email = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                }
                                #endregion
                                #region 7 Audit product
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim().ToLower();
                                    if (checkvalue == "yes")
                                    {
                                        ProductListSave.Add(4);
                                    }
                                }
                                #endregion
                                #region 8 Labour Compliance
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim().ToLower();
                                    if (checkvalue == "yes")
                                    {
                                        ProductListSave.Add(9);
                                    }
                                }
                                #endregion
                                #region 9 Secretarial Compliance
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim().ToLower();
                                    if (checkvalue == "yes")
                                    {
                                        ProductListSave.Add(8);
                                    }
                                }
                                #endregion
                                #region 10 Legal Compliance
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                                {
                                    string checkvalue = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim().ToLower();
                                    if (checkvalue == "yes")
                                    {
                                        ProductListSave.Add(1);
                                    }
                                }
                                #endregion

                                Customer customer = new Customer();
                                customer.Name = CustmerName.Trim();
                                customer.Address = Address.Trim();
                                customer.BuyerName = ContactName.Trim();
                                customer.BuyerContactNumber = ContactNumber.Trim();
                                customer.BuyerEmail = Email.Trim();
                                customer.VerticalApplicable = 0;
                                customer.Status = 1;

                                mst_Customer mstCustomer = new mst_Customer();
                                mstCustomer.Name = CustmerName.Trim();
                                mstCustomer.Address = Address.Trim();
                                mstCustomer.BuyerName = ContactName.Trim();
                                mstCustomer.BuyerContactNumber = ContactNumber.Trim();

                                mstCustomer.BuyerEmail = Email.Trim();
                                mstCustomer.VerticalApplicable = 0;
                                mstCustomer.Status = 1;

                                if (IsServiceProvider.ToLower().Trim() == "yes")
                                {
                                    serviceProviderID = 0;
                                    mstCustomer.ServiceProviderID = serviceProviderID;
                                    customer.ServiceProviderID = serviceProviderID;
                                    customer.ParentID = serviceProviderID;
                                    mstCustomer.ParentID = serviceProviderID;
                                    customer.IsServiceProvider = true;
                                    mstCustomer.IsServiceProvider = true;
                                    customer.IsDistributor = true;
                                    mstCustomer.IsDistributor = true;
                                }
                                CustomerIDCom = CustomerManagement.Create(customer);
                                if (CustomerIDCom > 0)
                                {
                                    saveSuccess = CustomerManagementRisk.Create(mstCustomer);
                                    if (saveSuccess)
                                    {
                                        uploadedContractCount++;
                                        ServiceProviderCustomerLimit obj = new ServiceProviderCustomerLimit();
                                        obj.ServiceProviderID = CustomerIDCom;
                                        obj.CustomerLimit = 25;
                                        obj.UserLimit = 5;
                                        obj.DistributorID = CustomerIDCom;
                                        obj.ParentBranchLimit = 25;
                                        obj.EmployeeLimit = 25;
                                        obj.BranchLimit = 1;
                                        ICIAManagement.CreateServiceProviderCustomerLimit(obj);

                                        ProcessManagement.CreateProcessSubprocessPredefined(CustomerIDCom, AuthenticationHelper.UserID);
                                        #region code to mapped Audit,Secretarial Compliance,Labour Compliance product
                                        List<ProductMapping_Risk> productMappingRiskList = new List<ProductMapping_Risk>();
                                        List<Business.Data.ProductMapping> productMappingList = new List<Business.Data.ProductMapping>();
                                        foreach (var item in ProductListSave)
                                        {
                                            ProductMapping_Risk productMappingRisk = new ProductMapping_Risk();
                                            productMappingRisk.CustomerID = CustomerIDCom;
                                            productMappingRisk.ProductID = item;
                                            productMappingRisk.IsActive = false;
                                            productMappingRisk.CreatedBy = AuthenticationHelper.UserID;
                                            productMappingRisk.CreatedOn = DateTime.Now;
                                            productMappingRiskList.Add(productMappingRisk);

                                            Business.Data.ProductMapping productMapping = new Business.Data.ProductMapping();
                                            productMapping.CustomerID = CustomerIDCom;
                                            productMapping.ProductID = item;
                                            productMapping.IsActive = false;
                                            productMapping.CreatedBy = AuthenticationHelper.UserID;
                                            productMapping.CreatedOn = DateTime.Now;
                                            productMappingList.Add(productMapping);
                                        }

                                        if (productMappingRiskList.Count > 0)
                                        {
                                            foreach (var item in productMappingRiskList)
                                            {
                                                bool isExistsInAudit = UserManagementRisk.IsCustomerProductMappingExists(item.CustomerID);
                                                if (!isExistsInAudit)
                                                {
                                                    UserManagementRisk.CreateCustomerProductMapping(item);
                                                }
                                            }
                                        }

                                        if (productMappingList.Count > 0)
                                        {
                                            foreach (var item in productMappingList)
                                            {
                                                bool isExistsInCompliance = UserManagement.IsCustomerProductMappingExistsCompliance(item.CustomerID);
                                                if (!isExistsInCompliance)
                                                {
                                                    UserManagement.CreateCustomerProductMappingCompliance(item);
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        CustomerManagement.deleteCustReset(CustomerIDCom);
                                    }
                                }
                            }
                            #endregion
                        }

                        if (saveSuccess)
                        {
                            if (uploadedContractCount > 0)
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = uploadedContractCount + " Customer(s) Details Uploaded Successfully";
                                cvUploadUtilityPage.CssClass = "alert alert-success";
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                            }
                        }
                    }// END DB Context
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });
                finalErrMsg += "</ol>";
            }
            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }

        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool matchSuccess = checkSheetExist(xlWorkbook, "ServiceProvider");
                            if (matchSuccess)
                            {
                                ProcessUserData(xlWorkbook);
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ServiceProvider'.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }

        }
    }
}

