﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;

namespace License
{
    public partial class LicenseManagement : System.Web.UI.MasterPage
    {
        protected string user_Roles;
        protected List<Int32> roles;

        protected string LastLoginDate;
        protected string CustomerName;
        protected int customerid;
        protected int userid;
        protected string CompanyAdmin = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                user_Roles = AuthenticationHelper.Role;
                //customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //userid = AuthenticationHelper.UserID;

                if (!IsPostBack)
                {
                    User LoggedUser = null;

                    if (hdnImagePath.Value != null && !String.IsNullOrEmpty(hdnImagePath.Value))
                    {
                        ProfilePic.Src = hdnImagePath.Value;
                        ProfilePicTop.Src = hdnImagePath.Value;                       
                    }
                    else
                    {
                        LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                        Page.Header.DataBind();

                        if (LoggedUser != null)
                        {
                            if (LoggedUser.ImagePath != null)
                            {
                                ProfilePic.Src = LoggedUser.ImagePath;
                                ProfilePicTop.Src = LoggedUser.ImagePath;                               
                                hdnImagePath.Value = LoggedUser.ImagePath;
                            }
                            else
                            {
                                ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                                ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";                                
                                hdnImagePath.Value = "~/UserPhotos/DefaultImage.png";
                            }
                        }

                       // roles = ContractManagement.GetAssignedRoles_Contract(AuthenticationHelper.UserID);
                    }

                    if (Session["LastLoginTime"] != null)
                    {
                        LastLoginDate = Session["LastLoginTime"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
    }
}