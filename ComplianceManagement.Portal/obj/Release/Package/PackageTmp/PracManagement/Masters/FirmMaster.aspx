﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="FirmMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.FirmMaster" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fClosepopup() {
            $('#divCustomerBranchesDialog').modal('show');
        };

        function fClosepopup() {
            $('#divCustomerBranchesDialog').modal('hide');
        };

      
    function checkAll(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkProduct") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }
    
    function UncheckHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkProduct']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkProduct']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='ProductSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {

            rowCheckBoxHeader[0].checked = false;
        }
    }
    function initializeJQueryUI(textBoxID, divID) {
        ;
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });       
    }
   

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
    
      <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/FirmMaster.aspx" runat="server" Text="Firm"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Office-Location"></asp:LinkButton>                   
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMFirmUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>                             
            </div>
        </div>
    </div>
    
    <asp:UpdatePanel ID="upCustomerBranchList" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                              <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                                  <div style="text-align:left;" >                                   
                                  </div>
                                </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>                           
                            <div class="col-md-3 colpadding0 entrycount" id="divFilter" runat="server" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" >
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="Width:150px" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                            <div style="text-align:right">                                 
                            </div> 
                     
                            <div style="margin-bottom: 4px;"> 
                                &nbsp
                                <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                                OnSorting="grdCustomer_Sorting" DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging" >
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Name" />                                   
                                     <asp:BoundField DataField="GSTNumber" HeaderText="GST Number" />
                                     <asp:BoundField DataField="Pan" HeaderText="Pan" />
                                    <asp:TemplateField HeaderText="Office Location">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>'>office-location</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Justify" HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1"   runat="server"   ToolTip="Edit office-location"  CommandName="EDIT_CUSTOMER"  CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" data-toggle="tooltip" data-placement="top" alt="Edit office-location"  title="Edit office-location"/></asp:LinkButton>                                           
                                        </ItemTemplate>                       
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" /> 
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />             
                                <PagerTemplate>
                                
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                </asp:GridView>
                                  <div style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                        </asp:DropDownListChosen>                                       
                                  </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0"  style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <div class="table-paging-text" style="float:right;">
                                        <p>
                                            Page
                                          
                                        </p>
                                    </div>                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>



    <div class="modal fade" id="divCustomerBranchesDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional" OnLoad="upCustomerBranches_Load">
                        <ContentTemplate>
                            <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="CustomerBranchValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                 <div runat="server" id="divState" style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        State</label>
                                    <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; width: 300px;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                                    <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                        Display="None" />
                                </div>

                                <div style="margin-bottom: 7px" class="float-container">
                                    <div class="float-child">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Gst Details</label>
                                        <asp:TextBox runat="server" ID="tbxGstNumber" CssClass="form-control" autocomplete="off" Style="width: 200px;" MaxLength="500" />
                                    </div>
                                    <div class="float-child1" id="divgst" runat="server" style="display:none;">
                                        <asp:Button Text="Get Details" runat="server" ID="btrgetGstdetails" OnClick="btrgetGstdetails_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top:7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                       Firm Name</label>
                                    <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />
                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Name." ControlToValidate="tbxName"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />                                   
                                </div>

                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                        Address  </label>
                                    <asp:TextBox runat="server" ID="tbxAddress" class="form-control" Style="width: 300px;"
                                        MaxLength="150" />
                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Address." ControlToValidate="tbxAddress"
                                        runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                       Pan</label>
                                    <asp:TextBox runat="server" ID="tbxpan" class="form-control" Style="width: 300px;"
                                        MaxLength="150" />
                                </div>                                                              
                                <div style="margin-bottom: 7px; margin-left: 200px; margin-top: 25px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="CustomerBranchValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal"  OnClientClick="fClosepopup()" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }


        $(document).ready(function () {
            setactivemenu('Firm Master');
            fhead('Firm Master');
        });


    </script>
</asp:Content>
