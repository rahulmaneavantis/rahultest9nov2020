﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PracticeManagement.Master" AutoEventWireup="true" CodeBehind="PracticeMGMTSetup.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.PracManagement.Masters.PracticeMGMTSetup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <style type="text/css">
        .aspNetDisabled {
            cursor: not-allowed;
        }

        .clearfix:after {
            clear: both;
            content: "";
            display: block;
            height: 0;
        }

        .pull-right {
            float: right;
        }

        .step > a {
            color: #333;
            text-decoration: none;
        }

        .step.current > a {
            color: white;
            text-decoration: none;
        }

        .step > a:active {
            color: #333;
            text-decoration: none;
        }

        .step > a:hover {
            color: white;
        }

        .step {
            clear: revert !important;
        }
        /* Breadcrups CSS */

        .arrow-steps .step {
            font-size: 14px;
            text-align: center;
            color: #666;
            cursor: default;
            margin: 0 3px;
            padding: 10px 10px 6px 30px;
            min-width: 10%;
            float: left;
            position: relative;
            background-color: #d9e3f7;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            transition: background-color 0.2s ease;
        }

            .arrow-steps .step:after,
            .arrow-steps .step:before {
                content: " ";
                position: absolute;
                top: 0;
                right: -17px;
                width: 0;
                height: 0;
                border-top: 19px solid transparent;
                border-bottom: 17px solid transparent;
                border-left: 17px solid #d9e3f7;
                z-index: 2;
                transition: border-color 0.2s ease;
            }

            .arrow-steps .step:before {
                right: auto;
                left: 0;
                border-left: 17px solid #fff;
                z-index: 0;
            }

            .arrow-steps .step:first-child:before {
                border: none;
            }

            .arrow-steps .step:last-child:after {
                border: none;
            }

            .arrow-steps .step:first-child {
                border-top-left-radius: 4px;
                border-bottom-left-radius: 4px;
            }

            .arrow-steps .step:last-child {
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .arrow-steps .step span {
                position: relative;
            }

                .arrow-steps .step span:before {
                    opacity: 0;
                    content: "✔";
                    position: absolute;
                    top: -2px;
                    left: -20px;
                }

            .arrow-steps .step.done span:before {
                opacity: 1;
                -webkit-transition: opacity 0.3s ease 0.5s;
                -moz-transition: opacity 0.3s ease 0.5s;
                -ms-transition: opacity 0.3s ease 0.5s;
                transition: opacity 0.3s ease 0.5s;
            }

            .arrow-steps .step.current {
                color: #fff;
                background-color: #23468c;
            }

                .arrow-steps .step.current:after {
                    border-left: 17px solid #23468c;
                }

        .float-child {
            width: 84%;
            float: left;
        }

        .float-child1 {
            width: 10%;
            float: left;
        }
    </style>

    <script type="text/javascript">
        function fopenpopupPMC() {
            $('#divCustomersDialog').modal('show');
        };

        function fClosepopupPMC() {
            $('#divCustomersDialog').modal('hide');
        };

      
    function checkAll(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkProduct") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }
    
    function UncheckHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkProduct']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkProduct']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='ProductSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {

            rowCheckBoxHeader[0].checked = false;
        }
    }
    function initializeJQueryUI(textBoxID, divID) {
        ;
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });       
    }
   
    function initializeJQueryUI() {
       
        $("#ContentPlaceHolder1_txtProductType").unbind('click');

        $("#ContentPlaceHolder1_txtProductType").click(function () {            
            $("#dvProduct").toggle("blind", null, 500, function () { });          
        });
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/PracManagement/Masters/PracticeMGMTSetup.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>                   
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/PracManagement/Masters/PMUser_List.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                 <div class="step">
                    <asp:LinkButton ID="lnkBtnTax" PostBackUrl="~/PracManagement/Masters/PMTaxDetails.aspx" runat="server" Text="Tax Details"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/PracManagement/Masters/PMUserCustomerMapping.aspx" runat="server" Text="User Customer Assingment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/PracManagement/Masters/PMCustomerServices.aspx" runat="server" Text="Customer Services"></asp:LinkButton>
                </div>              
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                        <div style="margin-bottom: 4px" />
                        <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0" style="width:47%;"> 
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>                             
                                  <div class="col-md-6 colpadding0">
                                <asp:DropDownListChosen runat="server" ID="ddlPageSize" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2"
                                    CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownListChosen>
                                      </div>
                            </div>
                           
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: -10%;">Customer</p>
                                </div>
                                <div class="col-md-9 colpadding0">
                                     <asp:DropDownListChosen ID="ddlCustomerlist" AllowSingleDeselect="false" runat="server"
                                          OnSelectedIndexChanged="ddlCustomerlist_SelectedIndexChanged"
                                         DisableSearchThreshold="2" CssClass="form-control" Width="100%">
                                    </asp:DropDownListChosen>                       
                                </div>
                            </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 35%">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Filter</p>
                                </div>
                                <div class="col-md-9 colpadding0" style="margin-left: -5%;">
                                    <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="height: 32px;" MaxLength="50" AutoPostBack="true"
                                        OnTextChanged="tbxFilter_TextChanged" />
                                </div>
                            </div>
                            <div class="col-md-1 colpadding0 entrycount" style="margin-top: 5px; padding-left: 18px; float: right;">
                                <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomer" OnClick="btnAddCustomer_Click" CssClass="btn btn-primary" />
                            </div>

                        </div>
                        <div style="margin-bottom: 2px">
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 4px;">
                          
                                <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                                    DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging">
                                    <Columns>

                                        <asp:BoundField DataField="CustomerGroupName" HeaderText="Group Name" HeaderStyle-Height="20px" ItemStyle-Height="25px" />
                                        <asp:BoundField DataField="Name" HeaderText="Legal Entity Name" HeaderStyle-Height="20px" ItemStyle-Height="25px" />
                                        <asp:BoundField DataField="BuyerName" HeaderText="Buyer Name" />
                                       <%-- <asp:BoundField DataField="BuyerContactNumber" HeaderText="Buyer Contact" />
                                        <asp:BoundField DataField="BuyerEmail" HeaderText="Buyer Email" />--%>
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:TemplateField  HeaderText="Branch">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>'>Branch</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Customer" title="Edit Customer" /></asp:LinkButton>
                                                <%--<asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID") %>'
                                                    OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../../Images/delete_icon_new.png" alt="Disable Customer" title="Disable Customer" /></asp:LinkButton>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <HeaderStyle BackColor="#ECF0F1" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page                                          
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>




    <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
                        <ContentTemplate>
                           <div>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CustomerValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                        ValidationGroup="CustomerValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                                </div>

                                <div id="issprd" runat="server" visible="false" style="margin-bottom: 7px;">
                                    <asp:Label Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                                    <asp:Label Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                                     Is Service Provider</asp:Label>
                                    <asp:CheckBox ID="chkSp" runat="server" AutoPostBack="true" OnCheckedChanged="chkSp_CheckedChanged" />
                                </div>
                                <div style="margin-bottom: 7px; display:none;">
                                    <asp:Label ID="lblast" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                                    <asp:Label ID="lblsp" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                                        Service Provider Name</asp:Label>
                             
                                     <asp:DropDownListChosen ID="ddlSPName" AllowSingleDeselect="false" runat="server" OnSelectedIndexChanged="ddlSPName_SelectedIndexChanged"
                                         DisableSearchThreshold="2" CssClass="form-control" Width="49%">
                                    </asp:DropDownListChosen>     
                                </div>

                                <div style="margin-bottom: 7px">
                                    <asp:Label ID="lblRegAst" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                                    <asp:Label ID="lblRegBy" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                                             Registration By</asp:Label>
                                    <asp:DropDownList runat="server" ID="ddlRegistrationBy" Style="padding: 0px; margin: 0px; width: 300px;"  CssClass="form-control m-bot15">
                                    </asp:DropDownList>
                                </div>                                   
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                              
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer Group Name </label>
                                      <asp:DropDownListChosen ID="ddlGroupName" AllowSingleDeselect="false" runat="server" AutoPostBack="true"
                                          OnSelectedIndexChanged="ddlGroupName_SelectedIndexChanged"
                                         DisableSearchThreshold="2" CssClass="form-control" Width="49%">
                                    </asp:DropDownListChosen>                                  
                                </div>

                               <div style="margin-bottom: 7px; margin-top: 7px;" id="isGroupname" runat="server" visible="false">
                              
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer Group Name </label>
                                    <asp:TextBox runat="server" ID="tbxGroupName" CssClass="form-control" Style="width: 300px;" MaxLength="100" />                                   
                                </div>

                                <div style="margin-bottom: 7px; margin-top: 7px;" class="float-container">
                                    <div class="float-child">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Gst Details</label>
                                        <asp:TextBox runat="server" ID="tbxGstNumber" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />
                                    </div>
                                    <div class="float-child1" id="divgst" runat="server">
                                        <asp:Button Text="Get Details" runat="server" ID="btrgetGstdetails" OnClick="btrgetGstdetails_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                              
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Legal Entity Name</label>
                                    <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" Style="width: 300px;" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                                        ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Address</label>
                                    <asp:TextBox runat="server" ID="tbxAddress" class="form-control" Style="height: 50px; width: 300px;" MaxLength="500"
                                        TextMode="MultiLine" />
                                </div>

                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Pan</label>
                                    <asp:TextBox runat="server" ID="tbxpan" class="form-control" Style="width: 300px;" 
                                        MaxLength="150" />
                                </div>
                               <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Tan</label>
                                    <asp:TextBox runat="server" ID="tbxTan" class="form-control" Style="width: 300px;" 
                                        MaxLength="150" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Buyer Name</label>
                                    <asp:TextBox runat="server" ID="tbxBuyerName" Style="width: 300px;" class="form-control"
                                        MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                                        ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                                        Display="None" />
                                </div>
                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Buyer Contact No</label>
                                    <asp:TextBox runat="server" ID="tbxBuyerContactNo"  class="form-control" Style="width: 300px;"
                                        MaxLength="15" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty."
                                        ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                                        Display="None" />
                                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                        ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" filtertype="Numbers" targetcontrolid="tbxBuyerContactNo" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                        ValidationGroup="CustomerValidationGroup" ErrorMessage="Please enter only 10 digit."
                                        ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>

                                <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                         Buyer Email</label>
                                    <asp:TextBox runat="server" ID="tbxBuyerEmail" Style="width: 300px;" class="form-control"
                                        MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Buyer Email can not be empty."
                                        ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                                        Display="None" />
                                </div>
                               <div style="margin-bottom: 7px">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Email for Invoice Sharing                                      
                                    </label>
                                    <asp:TextBox runat="server" ID="tbxEmailInvoiceSahring" Style="width: 300px;" class="form-control"
                                        MaxLength="100" />                                   
                                </div>

                                <div style="margin-bottom: 7px; display:none;" class="float-container">
                                    <div class="float-child">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Buyer Email</label>
                                        <asp:TextBox runat="server" ID="tbxBuyerEmail1" class="form-control" Style=" width: 300px;"
                                            MaxLength="200" />
                                     
                                    </div>
                                    <div class="float-child1" id="divemail" runat="server">
                                        <asp:Button Text="Add Email " runat="server" ID="btnAddEmail" OnClick="btnAddEmail_Click" CssClass="btn btn-primary" />
                                    </div>
                                </div>                        
                                <div class="clearfix"></div>
                                <div style="margin-bottom: 7px; margin-top: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer Status</label>
                                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" 
                                        Style="padding: 0px; margin: 0px; width: 300px;"  CssClass="form-control m-bot15" />
                                </div>                                                                                     
                                <div style="margin-bottom: 7px;">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Assign Product(s)</label>
                                    <asp:TextBox runat="server" ID="txtProductType"    class="form-control" Style="width: 300px;" />
                                    <div style="display: none; margin-left: 210px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvProduct">
                                        <asp:Repeater ID="rptProductType" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width:75px;">
                                                            <asp:CheckBox ID="ProductSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                        <td style="width: 220px;">
                                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkProduct" runat="server" onclick="UncheckHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblProductID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>                           
                                <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="CustomerValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fClosepopupPMC();" />
                                </div>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                           
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
