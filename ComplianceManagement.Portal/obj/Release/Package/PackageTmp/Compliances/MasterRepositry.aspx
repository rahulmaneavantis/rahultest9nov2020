﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MasterRepositry.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.MasterRepositry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <script>
        $(document).ready(function () {
            fhead('My Reports / Act Repository');
            setactivemenu('Myreport');
            fmaters1();
                       
            //setactivemenu('ComplianceDocumentList');
            //fmaters()
        });

    </script>
    <style type="text/css">
        span.k-icon.k-i-arrow-60-down {
    margin-top: 5px;
}


            .k-grid-content
        {
            min-height:394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

          .myKendoCustomClass {
            z-index:999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.6em;
        }
       
        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }
        .k-grouping-header {
             border-right: solid 1px #ceced2;
               border-left: solid 1px #ceced2;
}
        table.k-selectable {
        border-right: solid 1px #ceced2;
        }
        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>

    <script type="text/javascript">
        function FilterGrid() {

            //risk Details
            var Riskdetails = [];
            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Riskdetails.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });


            //Act Details
            var Actdetails = [];
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                Actdetails.push({
                    field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                });
            }

            //ComplianceType Details
            var Statusdetails = [];
            var liststatus = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            $.each(liststatus, function (i, v) {
                Statusdetails.push({
                    field: "ComplianceType", operator: "eq", value: v
                });
            });

            var dataSource = $("#grid").data("kendoGrid").dataSource;

            //three
            if (Statusdetails.length > 0
                && Riskdetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }
                //two
            else if (Riskdetails.length > 0
                && Statusdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
            }
            else if (Statusdetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }
            else if (Riskdetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }
                //one
            else if (Riskdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [

                        {
                            logic: "or",
                            filters: Riskdetails
                        }
                    ]
                });
            }
            else if (Statusdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [

                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
            }
            else if (Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdownACT").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdownACT', 'filterAct', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Compliance Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }


            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

    </script>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            //url: '<% =Path%>/Data/GetAssignedMasterCompliances?Uid=<% =UId%>&Flag=<%=RoleFlag%>',
                            url: '<% =Path%>/Data/GetAssignedMasterCompliances?Uid=<% =UId%>&Flag=<%=RoleFlag%>&CID=<%=CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetAssignedMasterCompliances?Uid=<% =UId%>&Flag=<%=RoleFlag%>'                        
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 20
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                //height: 523,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    { hidden: true, field: "ActID", title: "ActID" },
                    { hidden: true, field: "ComplianceId", title: "ComplianceId" },                  
                    {
                        field: "ActName", title: 'ActName',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                    , {
                        field: "ShortDescription", title: 'Compliance',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Description", title: 'Description',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                        //, dataBound: function (e) {
                        //    e.sender.list.width("1000");
                        //}
                    },
                    {
                        field: "Sections", title: 'Sections',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            multi: true,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "RiskCategory", title: 'Risk',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'                            
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                      {
                          hidden:true,
                          field: "ComplianceType", title: 'ComplianceType',
                          width: "10%",
                          attributes: {
                               style: 'white-space: nowrap;'                              
                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                        { hidden: true, field: "ComplinceAction", title: "Sub Compliance type" }
                ]
            });


            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {                 
                    var content = e.target.context.textContent;                   
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "top",
                width: 700,
                content: function (e) {                                      
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "top",
                width: 150,
                animation: {
                    open: {
                        effects: "fade:in"
                    }},
                content: function (e) {                                     
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "top",
                width: 150,
                content: function (e) {                                                     
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
        }


        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }

        function BindGridApply(e) {
            BindGrid();
            FilterGrid();
            e.preventDefault();
        }
        $(document).ready(function () {
            BindGrid();



            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Compliance Type",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
                    settracknew('Act Repository ', 'Filtering ', 'Compliance type', '')		
                },
                dataSource: [
                    { text: "Functionbased", value: "Functionbased" },
                    { text: "EventBased", value: "EventBased" },
                    { text: "Timebased", value: "Timebased" },
                    { text: "Checklist", value: "Checklist" },
                     { text: "One Time", value: "OneTime" }
                ]
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                    settracknew('Act Repository ', 'Filtering ', 'Risk', '')		


                },
                dataSource: [
                     { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });


            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGrid();
                    settracknew('Act Repository ', 'Filtering ', 'Act', '')		
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<%=RoleFlag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<%=RoleFlag%>"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("1000");
                }
            });

        });
        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
    function CloseClearPopup() {
        $('#APIOverView').attr('src', "../Common/blank.html");
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        
            <div class="row">
                <div class="row">
                    <input id="dropdownlistRisk" data-placeholder="Risk">
                    <input id="dropdownlistStatus" data-placeholder="Compliance Type">
                    <input id="dropdownACT" style="width: 341px;">
                    <button id="Applyfilter" style="float: right; margin-left: 1%; display: none;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Apply Filter</button>
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;"></div>
            <div class="row" style="padding-bottom: 0px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterAct">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterstatus">&nbsp;</div>
            <div id="grid" style="border: none;"></div>
        </div>
</asp:Content>
