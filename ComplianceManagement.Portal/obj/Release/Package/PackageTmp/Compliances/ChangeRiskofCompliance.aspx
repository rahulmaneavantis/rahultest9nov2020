﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ChangeRiskofCompliance.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ChangeRiskofCompliance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript">
      $(function () {
          $(document).tooltip();
      });
        var gridViewId = '#<%= grdCompliances.ClientID %>';
        function checkAll(selectAllCheckbox) {
            //get all checkbox and select it
            $('td :checkbox', gridViewId).prop("checked", selectAllCheckbox.checked);
        }
        function unCheckSelectAll(selectCheckbox) {
            //if any item is unchecked, uncheck header checkbox as also
            if (!selectCheckbox.checked)
                $('th :checkbox', gridViewId).prop("checked", false);
        }

        function validateCheckBoxes() {
            debugger;
            var isValid = false;
            var gridView = document.getElementById('<%= grdCompliances.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            //alert("Please select atleast one checkbox");
            document.getElementById("lblcompliance").innerHTML = "Please select atleast one compliance";
            return false;
        }
    </script>
        <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
        
        .ui-state-active{
                border: 1px solid #fad42e !important;
                background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
                color: #363636 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>


             <div style="margin-bottom: 4px; width: auto">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                <div align="center" style="margin-top: 0px; font-family: Arial; font-size: 10pt">
                                    <asp:Label ID="lblMsg1" runat="server" Text=""></asp:Label>
                                  
                                    <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                </div>
                            </div>

           
            <table width="100%">
                <tr>
                     <td>
                        <asp:DropDownList runat="server" ID="ddlAct" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" 
                            Style="padding: 0px; margin: 0px; height: 28px; width: 320px;" CssClass="txtbox" AutoPostBack="true"  />
                                          
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceType" Style="padding: 0px; margin: 0px; height: 28px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true"  OnSelectedIndexChanged="ddlComplinceType_SelectedIndexChanged">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Value="0">Statutory </asp:ListItem>
                            <asp:ListItem Value="1">Checklist </asp:ListItem>
                            <asp:ListItem Value="2">Event based </asp:ListItem>
                        </asp:DropDownList>
                    </td>
              
                    <td style="width: 25%; padding-right: 30px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" OnTextChanged="tbxFilter_TextChanged" Width="250px" MaxLength="50" AutoPostBack="true" />
                    </td>
                     <td style="display:none;">
                           <asp:LinkButton ID="lnkSampleFormat" runat="server" class="newlink" data-placement="bottom" data-toggle="tooltip" Font-Underline="True"  Text="Sample Format(Excel)" ToolTip="Download Sample Excel Document Format for User/ Customer Branch Upload"></asp:LinkButton>
                            
                       
                    </td>
                    <td style="width: 15%; padding-right: 20px;" align="right">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                </asp:LinkButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExport" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                   
                 
                   
                </tr>
                <tr>
                 
                    <td>
                        <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                           
                          <asp:FileUpload ID="MasterFileUpload" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                        </div>
                    </td>
                    <td>
                     
                        <asp:Button ID="btnUploadFile" runat="server" OnClick="btnUploadFile_Click"  ValidationGroup="oplValidationGroup" Text="Upload"  />
                   
                    </td>
                  
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="18" Width="100%"
                    Font-Size="12px" DataKeyNames="ID" OnRowDataBound="grdCompliances_RowDataBound" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                         <asp:BoundField DataField="ID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ID" />                        
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblComplianceInstenaceID" runat="server"  Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                <asp:Label ID="lblComplianceID" runat="server"  Text='<%# Eval("ID") %>'></asp:Label>
                                 <asp:Label ID="lblcrisk" runat="server"  Text='<%# Eval("ClientRisk") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      
                        <asp:TemplateField  HeaderText="Location" ItemStyle-Width="200px">
                             <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                            
                        <asp:BoundField DataField="FrequencyName" HeaderText="Frequency" ItemStyle-Width="180px" />                      
                        <asp:BoundField DataField="AvacomRisk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="AvacomRisk" />
                        <asp:TemplateField HeaderText="Client Risk" ItemStyle-Width="100px">                        
                            <ItemTemplate>                              
                                    <asp:DropDownList ID="ddlrisk" runat="server">
                                    </asp:DropDownList>                            
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="50px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="Chkselection" runat="server" onclick="unCheckSelectAll(this)" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
            <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btn_SaveClick" CssClass="button" ValidationGroup="cvUploadUtilityPage" />
            </div>
        </ContentTemplate>
           <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
