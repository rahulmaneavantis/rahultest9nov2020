﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="User_Group.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.User_Group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function Validate(sender, args) {
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkDepartment']:checked").length
            if (rowCheckBoxSelected == 0 || rowCheckBoxSelected == "undefined") {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkDepartment") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        $(document).ready(function () {

            $("#btnRepeater").click(function () {
                $("#dvDept").toggle("blind", null, 500, function () { });
            });
        });
        function initializeJQueryUIDeptDDL() {
            $("#<%= txtDepartment.ClientID %>").unbind('click');

            $("#<%= txtDepartment.ClientID %>").click(function () {
                $("#dvDept").toggle("blind", null, 100, function () { });
            });
        }
        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkDepartment']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkDepartment']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='DepartmentSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {
                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .td1 {
            width: 15%;
        }

        .td2 {
            width: 35%;

        }

        .td3 {
            width: 35%;
        }

        .td4 {
            width: 15%;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#divModifyUser').dialog({
                height: 480,
                width: 650,
                top: 100,
                autoOpen: false,
                draggable: true,
                title: "Security Group",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upDepeList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom">
                        <div id="divCustomerfilter" runat="server" style="margin-left: 300px">
                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: White; margin-bottom: -5px;">
                            </label>
                            <div style="width: 150px; float: left; margin-top: -15px; margin-left: 109px;">
                            </div>
                        </div>
                    </td>
                    <td align="right" style="width: 65%; padding-right: 60px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="gridview" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" Font-Size="12px"
                    OnPageIndexChanging="gridview_PageIndexChanging"
                    OnRowCommand="gridview_RowCommand">
                    <Columns>
                        <asp:TemplateField Visible="false" HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("IPID") %>' ToolTip='<%# Eval("IPID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Security Group" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%" />
                        <asp:BoundField DataField="IPName" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%" />
                        <%--                        <asp:BoundField DataField="IPAddress" HeaderText="Location" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%" />--%>
                        <asp:BoundField DataField="RememberMe" HeaderText="Remember Device Days" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%" />
                        <asp:BoundField DataField="PasswordEXpiry" HeaderText="Password Expiry Days" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%" />
                        <asp:BoundField DataField="Remark" HeaderText="Remark" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_IPBlock"
                                    CommandArgument='<%# Eval("IPID")+","+ Eval("GroupID") %>'><img src="../../Images/edit_icon.png" alt="Edit Security Group Details" title="Edit Security Group Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_IPBlock" OnClientClick="return confirm('Are you certain you want to delete Security Group Details?');"
                                    CommandArgument='<%# Eval("IPID")+","+ Eval("GroupID") %>'><img src="../../Images/delete_icon.png" alt="Delete Security Group Details" title="Delete Security Group Details" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divModifyUser">
        <asp:UpdatePanel ID="upModifyUser" runat="server" UpdateMode="Conditional" OnLoad="upModifyAssignment_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width: 100%;" cellpadding="6px">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <%--<asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="ModifyIPValidationGroup" />
                                    <asp:CustomValidator ID="CustomModify" runat="server" EnableClientScript="False"
                                        ValidationGroup="ModifyIPValidationGroup" Display="None" />--%>

                                    <asp:ValidationSummary  ID="vsLicenseListPage" runat="server"  Style="padding-left: 5%"  Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="ModifyIPValidationGroup" />
                                    <asp:CustomValidator ID="CustomModify" runat="server" Style="padding-left: 5%" EnableClientScript="False"
                                        ValidationGroup="ModifyIPValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2">
                                <label style="width: 50px; display: block; float: left; font-size: 13px; color: red; text-align: right;     margin-top: 2px;">*</label>
                                <label style="width: 158px; display: block; font-size: 13px; color: #333; text-align: right;">
                                    Security Group</label>
                            </td>
                            <td class="td3">
                                <asp:TextBox runat="server" ID="txtName" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                    AutoPostBack="true"
                                    OnTextChanged="txtName_TextChanged"
                                    CssClass="txtbox" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    ErrorMessage="Security Group can not be empty." ControlToValidate="txtName"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup" Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None"
                                    runat="server" ValidationGroup="ModifyIPValidationGroup"
                                    ErrorMessage="Please enter valid Security Group" ControlToValidate="txtName"
                                    ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ &-:.]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="td4"></td>
                        </tr>

                        <tr>
                            <td colspan="4" style="height:20px;">

                            </td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2">
                                <label style="width: 158px; display: block; font-size: 13px; color: #333;  text-align: right;">
                                    Remember Device Days</label>
                            </td>
                            <td class="td3">
                                <asp:TextBox runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                                     ID="txtRememberMe" onkeypress="return isNumberKey(event)" CssClass="form-control" />
                            </td>
                            <td class="td4"></td>
                        </tr>
                        <tr>
                            <td class="td1"></td>

                            <td class="td2">
                                <label style="width: 158px; display: block; font-size: 13px; color: #333; text-align: right;">
                                    Password Expiry Days</label>
                            </td>
                            <td class="td3">
                                <asp:TextBox runat="server" ID="txtPasswordExpiry" onkeypress="return isNumberKey(event)"
                                     CssClass="form-control" Style="padding: 0px; height: 22px; width: 250px;" />

                            </td>
                            <td class="td4"></td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2">
                                <label style="width: 158px; display: block; font-size: 13px; color: #333;  text-align: right;">
                                    Remark</label>
                            </td>
                            <td class="td3">
                                <asp:TextBox runat="server" ID="txtremark" CssClass="form-control" Style="width: 250px; 
                                 height: 40px" TextMode="MultiLine" />
                            </td>
                            <td class="td4"></td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2">
                                <label style="width: 90px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-top: 2px;">*</label>
                                <label style="width: 158px; display: block; font-size: 13px; color: #333; text-align: right;">
                                    Location</label>
                            </td>
                            <td class="td3">
                                <div>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please select at least one IP Group."
                                        ClientValidationFunction="Validate" ForeColor="Red" ValidationGroup="ModifyIPValidationGroup" Display="None"></asp:CustomValidator>
                                </div>
                                <div>
                                    <asp:TextBox runat="server" ID="txtDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                        CssClass="txtbox" />

                                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; width: 250px;" id="dvDept" class="dvDeptHideshow">
                                        <asp:Repeater ID="rptDepartment" runat="server">
                                            <HeaderTemplate>
                                                <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                    <tr>
                                                        <td style="width: 100px;">
                                                            <asp:CheckBox ID="DepartmentSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" ItemStyle-HorizontalAlign="Left" /></td>
                                                        <td style="width: 282px;">
                                                            <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" ItemStyle-HorizontalAlign="Left" OnClientClick="HidedropdownList()" /></td>
                                                        <td>
                                                            <asp:CustomValidator ID="cValidation" runat="server"
                                                                ErrorMessage="Invalid"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 20px;">
                                                        <asp:CheckBox ID="chkDepartment" runat="server" onclick="UncheckHeader();" /></td>
                                                    <td style="width: 200px;">
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px; padding-bottom: 5px;">
                                                            <asp:Label ID="lblIPBlockID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                            <asp:Label ID="lblIPBlockName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                            </td>
                            <td class="td4"></td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2"></td>
                            <td class="td3"><asp:CheckBox runat="server" ID="chkMobileAccess" AutoPostBack="true" OnCheckedChanged="chkMobileAccess_Checkedchanged" Style="padding: 0px; margin: 0px; height: 22px; width: 10px;" MaxLength="15" Text="Allow Mobile Access?" /></td>
                            <td class="td4"></td>
                        </tr>
                        <tr style="display: none;">
                            <td class="td1"></td>
                            <td class="td2"></td>
                            <td class="td3">
                                <asp:CheckBox runat="server" ID="chkIMEINumber" OnCheckedChanged="chkIMEINumber_Checkedchanged"
                                    Style="padding: 0px; margin: 0px; height: 22px; width: 10px;" MaxLength="15" Text="Enforced By IMEI Number?" />
                            </td>
                            <td class="td4"></td>
                        </tr>
                        <tr>
                            <td class="td1"></td>
                            <td class="td2"></td>
                            <td class="td3"><asp:CheckBox runat="server" ID="chkotp" OnCheckedChanged="chkotp_Checkedchanged" Style="padding: 0px; margin: 0px; height: 22px; width: 10px; text-align: right" MaxLength="15" Text="OTP Mandatory?" /></td>
                            <td class="td4">  <label style="width: 50px; display: block; float: left; font-size: 13px; color: red; text-align: right; margin-bottom: 7px">&nbsp;</label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 0px; margin-top: 0px; margin-left: 25%;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                                        CssClass="button" ValidationGroup="ModifyIPValidationGroup" />
                                    <asp:Button Text="Close" runat="server" Style="margin-left: 10px" ID="Button2" CssClass="button" OnClientClick="$('#divModifyUser').dialog('close');" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                              <%--  <div style="margin-bottom: 12px; float: left; margin-left: 90px; margin-top: 2px;">--%>
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                             <%--   </div>--%>
                            </td>
                        </tr>
                    </table>
                </div>
               
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
