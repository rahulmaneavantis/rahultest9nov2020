﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerInternalCompliancestatusTransaction.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.PerInternalCompliancestatusTransaction" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <!-- Bootstrap CSS -->
    <link href='../NewCSS/bootstrap.min.css' rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href='../NewCSS/bootstrap-theme.css' rel="stylesheet" type="text/css" />

    <link href='../NewCSS/responsive-calendar.css' rel="stylesheet" type="text/css" />

    <link href='../NewCSS/font-awesome.min.css' rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href='../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css' rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href='../NewCSS/owl.carousel.css' type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href='../NewCSS/fullcalendar.css' type="text/css" />
    <link href='../NewCSS/stylenew.css' rel="stylesheet" type="text/css" />
    <link href='../NewCSS/jquery-ui-1.10.4.min.css' rel="stylesheet" type="text/css" />
    <link href='../Newjs/bxslider/jquery.bxslider.css' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="../Newjs/moment.min.js"></script>
    <script type="text/javascript">
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
        function fInternalFilesubmit() {
            if (ValidateFilestatus()) {
                fileUpload = document.getElementById('fuSampleFile2');
                if (fileUpload.value != '') {
                    document.getElementById("<%=UploadInternalDocument.ClientID %>").click();
                }
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";
            }
        }
        function fInternalWorkingFilesubmit() {
            if (ValidateFilestatus()) {
                fileUpload = document.getElementById('FileUpload12');
                if (fileUpload.value != '') {
                    document.getElementById("<%=UploadInternalDocument.ClientID %>").click();
                }
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";

            }
        }

        $(document).ready(function () {

            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUp4').modal('hide');
                $('#modalDocumentPerformerViewerInternal').modal('hide');
                $('#InternalDocumentPriview').modal('hide');
                $('#InternalDocumentShowPopUp').modal('hide');
            });
        });

        function fopenInternalDocumentPriview(file) {
            $('#InternalDocumentPriview').modal('show');
            $('#docInternalPriview').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendocfile4() {
            $('#DocumentPopUp4').modal();
            $('#docViewerAll4').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample4.ClientID %>").text());
        }

        function fopendoctaskfileReviewInternal(file) {
            $('#modalDocumentPerformerViewerInternal').modal('show');
            $('#docViewerPerformerAllInternal').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendoctaskfileReviewInternalPopUp() {
            $('#modalDocumentPerformerViewerInternal').modal('show');
        }

        function fopendocfileInternalLicenseAllShowPopUp() {
            $('#InternalDocumentShowPopUp').modal('show');
        }
        function fopendocfileInternallicense(file) {
            $('#InternalDocumentShowPopUp').modal('show');
            $('#docViewerInternalLicenseAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }


        //checkbox
        function SelectheaderDOCCheckboxes(headerchk) {
            $('#taskIntDocslower').hide();
            var spanparent = $(headerchk).parent('div');
            var subtaskDocumentlistobj = $(spanparent).find('.subtaskDocumentlist');
            var count = 0;
            $('#taskIntDocslower').html($(subtaskDocumentlistobj).html());
            $('#ConfirmationIntDocumentModel').modal('show');
            $('#taskIntDocslower').show();
        }
        var nomessage = '';
        var yesmessage = '';
        var IsBothYesNo = '';
        function SelectheaderCheckboxesInternal(headerchk) {

            $('#taskslower').hide();
            $('#lblYes').show();
            $('#lblNo').show();
            var spanparent = $(headerchk).parent('span.True');
            var tr = $(spanparent).parent('div');
            var subtasklistobj = $(tr).find('.subtasklist');

            var count = 0;
            var gvcheck = document.getElementById("<%=gridSubTaskInternal.ClientID %>");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("<%= gridSubTaskInternal.ClientID %>");
        var inputList = gv.getElementsByTagName("input");

        for (var i = 0; i < inputList.length; i++) {
            if (inputList[i].type == "checkbox" && inputList[i].checked) {
                count = count + 1;

                $('#taskslower').html($(subtasklistobj).html());
                if ($(spanparent).attr('data-msg') == '') {
                    $('#lblYes').removeAttr("href");
                } else {
                    $('#idProceed').html($(spanparent).attr('data-msg'));
                    if ($(spanparent).attr('data-IsBothYesNo') == "True" || $(spanparent).attr('data-IsBothYesNo') == "1") {
                        $('#lblNo').removeAttr("href");
                        $('#lblYes').removeAttr("href");
                        IsBothYesNo = $(spanparent).attr('data-IsBothYesNo');
                    }

                    yesmessage = $(spanparent).attr('data-yesmessage');
                    nomessage = $(spanparent).attr('data-nomessage');

                    if ($(spanparent).attr('data-yes') == "True" || $(spanparent).attr('data-yes') == "1") {
                        $('#lblNo').removeAttr("href");

                    }
                    if ($(spanparent).attr('data-no') == "True" || $(spanparent).attr('data-no') == "1") {
                        $('#lblYes').removeAttr("href");
                    }
                }
                $('#ConfirmationModelInternal').modal('show');
            }
        }
    }

    function callOnButtonYesInternal() {

        if (IsBothYesNo == "True" || IsBothYesNo == 1) {
            var r = confirm(yesmessage);
        }
        else {
            var count = 0;
            var gvcheck = document.getElementById("<%=gridSubTaskInternal.ClientID %>");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("<%= gridSubTaskInternal.ClientID %>");
           var inputList = gv.getElementsByTagName("input");
           for (var i = 0; i < inputList.length; i++) {
               if (inputList[i].type == "checkbox" && inputList[i].checked) {
               }
           }
           if ($('#lblYes').attr("href") == null || $('#lblYes').attr("href") == undefined || $('#lblYes').attr("href") == '') {
               $('#taskslower').show();
               $('#ConfirmationModelInternal').modal('show');
               return false;
           }
           else {
               if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                   var r = confirm(yesmessage);
               }
               else {
                   var r = confirm(yesmessage);
                   if (r == true) {
                       $('#ConfirmationModelInternal').modal('hide');
                       return true;
                   } else {
                       $('#ConfirmationModelInternal').modal('show');
                       return false;
                   }
               }
           }
       }
       $('#ConfirmationModelInternal').modal('hide');
       initializeDatePicker();
   }
   $('.btn-minimize').click(function () {
       var s1 = $(this).find('i');
       if ($(this).hasClass('collapsed')) {
           $(s1).removeClass('fa-chevron-down');
           $(s1).addClass('fa-chevron-up');
       } else {
           $(s1).removeClass('fa-chevron-up');
           $(s1).addClass('fa-chevron-down');
       }
   });

   function btnminimize(obj) {
       var s1 = $(obj).find('i');
       if ($(obj).hasClass('collapsed')) {

           $(s1).removeClass('fa-chevron-up');
           $(s1).addClass('fa-chevron-down');
       } else {
           $(s1).removeClass('fa-chevron-down');
           $(s1).addClass('fa-chevron-up');
       }
   }
   function InitializeRequest(sender, args) { }
   function EndRequest(sender, args) { BindControls(); }

   function BindControls() {

       var enddates = new Date($("#lblEndDate").text());
       if (enddates != null) {
           var startDate = new Date();
           $(function () {
               $('input[id*=txtStartDate]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   numberOfMonths: 1,
                   minDate: enddates,
                   changeMonth: true,
                   changeYear: true,
               });
               $('input[id*=txtEndDate]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   numberOfMonths: 1,
                   minDate: enddates,
                   changeMonth: true,
                   changeYear: true,
               });
               $('input[id*=tbxDate2]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   maxDate: startDate,
                   numberOfMonths: 1,
                   //changeMonth: true,
                   //changeYear: true,
               });
           });

       }
       else {
           var startDate = new Date();
           $(function () {
               $('input[id*=txtStartDate]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   numberOfMonths: 1,
                   changeMonth: true,
                   changeYear: true,
               });
               $('input[id*=txtEndDate]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   numberOfMonths: 1,
                   changeMonth: true,
                   changeYear: true,
               });

               $('input[id*=tbxDate2]').datepicker({
                   dateFormat: 'dd-mm-yy',
                   maxDate: startDate,
                   numberOfMonths: 1,
                   //changeMonth: true,
                   //changeYear: true,
               });

           });
       }

   }

   function callOnButtonNoInternal() {

       if (IsBothYesNo == "True" || IsBothYesNo == 1) {
           var r = confirm(nomessage);
       }
       else {
           var count = 0;
           var gvcheck = document.getElementById("<%=gridSubTaskInternal.ClientID %>");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("<%= gridSubTaskInternal.ClientID %>");
            var inputList = gv.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                if (inputList[i].type == "checkbox" && inputList[i].checked) {
                    count = count + 1;
                }
            }
            initializeDatePicker();
            if ($('#lblNo').attr("href") == null || $('#lblNo').attr("href") == undefined || $('#lblNo').attr("href") == '') {
                $('#taskslower').show();
                $('#ConfirmationModelInternal').modal('show');
                return false;
            }
            else {
                if (IsBothYesNo == "True" || IsBothYesNo == 1) {
                    var r = confirm(nomessage);
                }
                else {
                    var r = confirm(nomessage);
                    if (r == true) {
                        $('#ConfirmationModelInternal').modal('hide');
                        return true;
                    } else {
                        $('#ConfirmationModelInternal').modal('show');
                        return false;
                    }
                }
            }
        }
        $('#ConfirmationModelInternal').modal('hide');
    }
        $(document).ready(function () {
            //$('#txtEndDate').attr('readonly', true);
            $('#txtEndDate').addClass('input-disabled')
            $('#txtStartDate').addClass('input-disabled')
        });
    function fopendocfileReview(file) {
        $('#modalDocumentPerformerViewer').modal('show');
        $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocfileReviewPopUp() {
        $('#modalDocumentPerformerViewer').modal('show');
    }

    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#modalDocumentPerformerViewer').modal('hide');
        });

    });
    function openModalInternalReviewer() {
        $('#ComplainceInternalReviewer').modal('show');
        return true;
    }
    function openModalInternalPer() {
        $('#ComplainceInternalPerformaer').modal('show');
        return true;
    }
    function openInternalTaskSummary(obj) {

        var taskInstanceID = $(obj).attr('data-inid');
        var taskScheduleOnID = $(obj).attr('data-scid');
        ShowDialog(taskInstanceID, taskScheduleOnID)
    }

    function ShowDialog(taskInstanceID, taskScheduleOnID) {

        $('#IntTaskPopUp').modal('show');
        $('.modal-dialog').css('width', '95%');
        $('#IntTaskViewerAll').attr('width', '100%');
        $('#IntTaskViewerAll').attr('height', '550px');
        $('#IntTaskViewerAll').attr('src', "/Task/TaskStatusTransactionPerformer.aspx?TID=" + taskInstanceID + "&TSOID=" + taskScheduleOnID);
    };
    function downloadTaskSummary(obj) {
        var formonth = $(obj).attr('data-formonth');
        var instanceId = $(obj).attr('data-inid');
        var scheduleOnID = $(obj).attr('data-scid');
        $('#filedownload').attr("src", "/task/downloadtaskdoc.aspx?taskScheduleOnID=" + scheduleOnID + "&r=" + Math.random());

    }
    function ClosePop() {        
        window.parent.closeModal();
    }

    var validFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];
    function ValidateFilestatus() {

        var label = document.getElementById("Label1");
        var fuSampleFile = $("#fuSampleFile2").get(0).files;
        var FileUpload1 = $("#FileUpload12").get(0).files;
        var isValidFile = true;

        for (var i = 0; i < fuSampleFile.length; i++) {
            var fileExtension = fuSampleFile[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }


        for (var i = 0; i < FileUpload1.length; i++) {
            var fileExtension = FileUpload1[i].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                isValidFile = false;
                break;
            }
        }

        if (!isValidFile) {
            $('#ValidationSummary1').IsValid = false;
            document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";


        }
        return isValidFile;
    }
    function DocValidate() {        
        var nBytes = 0,
        oFiles = $("#fuSampleFile2")[0].files;
        var oFilesCount = 0;
        var validFilesTypes = ["exe", "bat", "dll", "css", "js"];

        for (var nFileId = 0; nFileId < oFiles.length; nFileId++) {
            nBytes += parseInt(oFiles[nFileId].size);

            var fileExtension = oFiles[nFileId].name.split('.').pop();
            if (validFilesTypes.indexOf(fileExtension) != -1) {
                oFilesCount++;
            }
        }

        var oWFiles = $("#FileUpload12")[0].files;
        var oWFilesCount = 0;

        for (var nWFileId = 0; nWFileId < oWFiles.length; nWFileId++) {
            nBytes += parseInt(oWFiles[nWFileId].size);

            var wfileExtension = oWFiles[nWFileId].name.split('.').pop();
            if (validFilesTypes.indexOf(wfileExtension) != -1) {
                oWFilesCount++;
            }
        }
        if (oFilesCount > 0 || oWFilesCount > 0 || parseInt(nBytes) > 104857600) {
            if (oFilesCount > 0) {               
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid Compliance Document(s) uploded. .exe,.bat,.dll formats not supported.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("InternalVD").value = "Invalid Compliance Document(s) uploded. .exe,.bat,.dll formats not supported.";

            }
            if (oWFilesCount > 0) {                
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid Working file(s) uploded. .exe,.bat,.dll formats not supported.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("InternalVD").value = "Invalid Working file(s) uploded. .exe,.bat,.dll formats not supported.";

            }
            if (parseInt(nBytes) > 104857600) {               
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Total File Size of uploaded Compliance Document(s) and Working File(s) should not exceed 100MB.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("InternalVD").value = "Total File Size of uploaded Compliance Document(s) and Working File(s) should not exceed 100MB.";

            }
            return false;
        }
    }



    $(document).ready(function () {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);

        BindControls();
        //alert(1);
        $('#tbxDate').change(function () {
            //PenaltyVisibleValidate()
        });

    });


    function compare(dateTimeA, dateTimeB) {
        var momentA = moment(dateTimeA, "DD/MM/YYYY");
        var momentB = moment(dateTimeB, "DD/MM/YYYY");
        if (momentA > momentB) return 1;
        else if (momentA < momentB) return -1;
        else return 0;
    }
    function PenaltyValidate() {
        var tbxDate = $("#tbxDate2").val();
        if (tbxDate != "") {
            var appdate = new Date($("#lblApplicationDate").text());
            var adate = new Date($.datepicker.parseDate("dd-mm-yy", $("#tbxDate2").val()));           
            var lstatus = -1;
            var a = compare(appdate, adate);
            if (a == 1) {
                lstatus = 2;
            } else if (a == -1) {
                lstatus = 3;
            } else if (a == 0) {
                lstatus = 2;
            }
            var monnonmon = $("#HDNIsDocumentCompulsary").val();
            var ddlstatus = $('select#ddlStatus2 option:selected').val();
            if (ddlstatus == -1) {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Please select status");
                $('#InternalVD').IsValid = false;
                document.getElementById("InternalVD").value = "Please select status";
                return false;
            }

            var filename = $("#fuSampleFile2").val();
            var rowscount = $("#grdInternalDocument tr").length;
            var monnonmon = $("#HDNIsDocumentCompulsary").val();
            if (rowscount == 0 && monnonmon == "T") {

                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Please select documents for upload.");
                $('#InternalVD').IsValid = false;
                document.getElementById("InternalVD").value = "Please select documents for upload.";
                return false;
            }
            if (ddlstatus == 7) {
                var lno = $("#txtLicenseNo").val();
                var lT = $("#txtLicenseTitle").val();
                var lSD = $("#txtStartDate").val();
                var lED = $("#txtEndDate").val();

                if (lno == "") {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please enter license number");
                    $('#InternalVD').IsValid = false;
                    document.getElementById("InternalVD").value = "Please enter license number";
                    return false;
                }
                if (lT == "") {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please enter license title");
                    $('#InternalVD').IsValid = false;
                    document.getElementById("InternalVD").value = "Please enter license title";
                    return false;
                }
                if (lSD == "") {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please enter license start date");
                    $('#InternalVD').IsValid = false;
                    document.getElementById("InternalVD").value = "Please enter license start date";
                    return false;
                }
                if (lED == "") {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please enter license end date");
                    $('#InternalVD').IsValid = false;
                    document.getElementById("InternalVD").value = "Please enter license end date";
                    return false;
                }
            }                     
        }
        else {
            $("#Labelmsg").css('display', 'block');
            $('#Labelmsg').text("Please select date");
            $('#InternalVD').IsValid = false;
            document.getElementById("InternalVD").value = "Please select date";
            return false;
        }
    }
    </script>

    <style>
         .input-disabled {
            background-color: #f7f7f7;
            border: 1px solid #c7c7cc;
            padding: 6px 12px;
        }
        tr.spaceUnder > td {
            padding-bottom: 1em;
        }

        span.True {
            display: block !important;
        }

        .topdivlive {
            width: 100%;
        }

        .topdivlivetext {
            width: 69%;
            float: left;
        }

        .topdivliveperformer {
            width: 16%;
        }

        .topdivliveimage {
            width: 6%;
            float: right !important;
        }

        .subhead {
            font-size: 14px;
            font-weight: 500;
        }

        .doctaks {
            background-image: url(../img/icon-download.png) !important;
            width: 17px !important;
            display: block !important;
        }

        .topdivlive.topline {
            margin-top: 5px;
            border-top: 1px solid #dddddd;
            padding-top: 5px;
        }

        .circle {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>


</head>
<body>
    <form runat="server">
        <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>
        <div id="divComplianceDetailsDialog11">
            <iframe id="filedownload" style="display: none;" src="about:blank"></iframe>
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails2_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 2px">
                            <asp:ValidationSummary ID="InternalVD" runat="server" Style="padding-left: 5%" Display="none"
                                class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                                EnableClientScript="true" ValidationGroup="ComplianceValidationGroup" Style="display: none;" />
                            <asp:Label ID="Label1" Style="display: none;" class="alert alert-block alert-danger fade in" runat="server"></asp:Label>

                            <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>

                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                        </div>

                        <div class="clearfix" style="margin-bottom: 5px"></div>
                        <div>
                            <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                            <div id="divRiskType2" runat="server" class="circle"></div>
                            <asp:Label ID="lblRiskType2" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                        </div>

                        <div id="ActDetails" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                    <h2>Compliance Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseActDetails" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceID2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Short Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceDiscription2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblFrequency2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRisk2" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:UpdatePanel ID="upsample2" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber4" Style="width: 300px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample4" Style="width: 300px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample2_Click" />
                                                                        <asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm4" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile4();" />
                                                                        <asp:Label ID="lblpathsample4" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333; display: none;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:Label ID="lblPeriod1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />

                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:HiddenField ID="hiddenDueDateInternal" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ComplianceDetails" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                    <h2>License Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseComplianceDetails" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Type</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseType" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Number</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseNumber" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Title</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseTitle" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Application Due Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblApplicationDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Start Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblStartdate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">End Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblEndDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 25%; font-weight: bold;">Versions</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <table width="100%" style="text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                                                    OnItemCommand="rptLicenseVersion_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseDocumnets">
                                                                                            <thead>
                                                                                                <%-- <th>Versions</th>--%>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                        <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                        <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:LinkButton ID="lblpathDownload" CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                                    OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>

                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseDocumnets">
                                                                                            <thead>
                                                                                                <th>Compliance Related Documents</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                </asp:LinkButton>
                                                                                                   <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />  

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                                <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseWorkingFiles">
                                                                                            <thead>
                                                                                                <th>Compliance Working Files</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                                </asp:LinkButton>
                                                                                                   <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div runat="server" id="divTask" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTaskInternal" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTaskInternal_RowCommand" OnRowDataBound="gridSubTaskInternal_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("TaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblCbranchId" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; width: 250px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>

                                                                        <asp:CheckBox ID="chkTask" CssClass='<%# Eval("IsYesNo") %>' Width="30px" data-toggle="tooltip" Style="display: none; float: left;" data-yesmessage='<%# Eval("Yesmessage") %>' data-nomessage='<%# Eval("Nomessage") %>' data-attr='<%# Eval("IsYesNo") %>' data-msg='<%# Eval("Message") %>' data-yes='<%# Eval("IsYes") %>' data-no='<%# Eval("IsNo")  %>' data-IsBothYesNo='<%# Eval("IsBothYesNo")  %>' ToolTip="Click to close if not applicable" AutoPostBack="true" runat="server" onclick="SelectheaderCheckboxesInternal(this)" />
                                                                        <asp:Image ID="chkDocument" ImageUrl="../Images/View-icon-new.png" data-toggle="tooltip" CssClass="Documentsbtask" ToolTip="Click to download subtasks documents" runat="server" OnClick="javascript:SelectheaderDOCCheckboxes(this)" />
                                                                        <asp:Label ID="lblsubtasks" CssClass="subtasklist" runat="server" Style="display: none;"></asp:Label>
                                                                        <asp:Label ID="lblsubtaskDocuments" CssClass="subtaskDocumentlist" runat="server" Style="display: none;"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="UpdateComplianceStatus" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                                    <h2>Update License Status</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 0px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:DropDownList runat="server" ID="ddlStatus2" OnSelectedIndexChanged="ddlStatus2_SelectedIndexChanged"
                                                                    AutoPostBack="true" class="form-control m-bot15" Style="width: 280px;" />
                                                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus2"
                                                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div id="fieldsetRenewal" runat="server" visible="false" style="margin-bottom: 7px">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Number</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseNo" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="rfvLicenseNo" ErrorMessage="Please Enter License Number."
                                                                        ControlToValidate="txtLicenseNo" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Title</label>
                                                                </td>

                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseTitle" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter License Title."
                                                                        ControlToValidate="txtLicenseTitle" runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Start Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDate"
                                                                        runat="server" ID="RequiredFieldValidator4" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">End Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtEndDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDate"
                                                                        runat="server" ID="RequiredFieldValidator3" ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                                      <table style="width: 100%">
                                                       <% if (UploadDocumentLink == "True")
                                                                    {%>
                                                             <tr>
                                                                    <td style="width: 25%;">
                                                                         <% if (IsDocumentCompulsary)
                                                                            {%>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <%}
                                                                            else
                                                                            {%>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <%}%>
                                                                        <label style="font-weight: bold; vertical-align: text-top;">Compliance Document(s)</label>
                                                                    </td>

                                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                    <td style="width: 73%;">
                                                                        <asp:TextBox runat="server" ID="TxtCompliancedocumentlnk" class="form-control" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="UploadlinkCompliancefile" runat="server" Text="Add Link" Style="" OnClick="UploadlinkCompliancefile_Click"
                                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="UploadlinkCompliancefile" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 25%;">
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                        <label style="font-weight: bold; vertical-align: text-top;">Working Files(s)</label>
                                                                    </td>

                                                                    <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                    <td style="width: 73%;">
                                                                        <asp:TextBox runat="server" ID="Txtworkingdocumentlnk" class="form-control" />
                                                                      
                                                                    </td>
                                                                    <td>
                                                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="UploadlinkWorkingfile" runat="server" Text="Add Link" Style="" OnClick="UploadlinkWorkingfile_Click"
                                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="UploadlinkWorkingfile" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>

                                                      <%}%>
                                                        <% if (UploadDocumentLink == "False")
                                                                    {%>
                                                  
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <% if (IsDocumentCompulsary)
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                    else
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Compliance Document(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 60%;">
                                                                <asp:FileUpload ID="fuSampleFile2" Multiple="Multiple" onchange="fInternalFilesubmit()" runat="server" Style="color: black" />
                                                                <asp:HiddenField ID="HDNIsDocumentCompulsary" runat="server" />
                                                                <%--  <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile2"
                                                                    runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup" Display="None" />--%>
                                                            </td>
                                                            <td style="width: 13%;">
                                                                <%--OnClientClick="javascript:return DocValidate();"--%>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadInternalDocument" runat="server" Text="Upload Document" Style="display: none;"
                                                                            OnClick="UploadInternalDocument_Click"
                                                                            
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                            CausesValidation="true" />
                                                                    </ContentTemplate>
                                                                  <%--  <Triggers>
                                                                        <asp:PostBackTrigger ControlID="UploadInternalDocument" />
                                                                    </Triggers>--%>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>

                                                            <%}%>
                                                    </table>
                                                </div>
                                                 <% if (UploadDocumentLink == "False")
                                                                    {%>
                                                <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Working Files(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:FileUpload ID="FileUpload12" Multiple="Multiple" onchange="fInternalWorkingFilesubmit()" runat="server" Style="color: black" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                  <%}%>
                                                <div style="margin-bottom: 7px" runat="server" id="divInternalgrdFiles">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:GridView runat="server" ID="grdInternalDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    PageSize="100" AllowPaging="true" OnRowCommand="grdInternalDocument_RowCommand" OnRowDataBound="grdInternalDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                                    <asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                                <asp:LinkButton ID="lnkRedirectDocument" runat="server" data-toggle="tooltip" Style="color: blue; text-decoration: underline;"
                                                                                                OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %> Text='<%# Eval("DocName") %>' />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                    <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                                            <asp:Label ID="lblInternalDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                                            <asp:Label ID="lblInternalScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                                            <asp:Label ID="lblInternalComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lnkDownloadInternalDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                  <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkViewInternalDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                 <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkDeleteInternalDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                 <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkDownloadInternalDocument" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerSettings Visible="false" />
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Record Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxDate2" placeholder="DD-MM-YYYY"
                                                                    class="form-control" Style="width: 115px; cursor: text;" />

                                                                <%--<asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate2"
                                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup"
                                                                     Display="None" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">

                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks2" TextMode="MultiLine" class="form-control" Rows="2" />
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Please enter remark"
                                                                    ControlToValidate="tbxRemarks2" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">

                                                    <asp:Button Text="Submit" runat="server" ID="btnSave2" OnClientClick="javascript:return PenaltyValidate();" OnClick="btnSave_Click"
                                                        ValidationGroup="ComplianceValidationGroup" CssClass="btn btn-search" />
                                                    <asp:Button Text="Close" runat="server"
                                                        Style="margin-left: 15px; display:none;" ID="btnCancel2"  CssClass="btn btn-search" data-dismiss="modal" />
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div runat="server" id="divDeleteDocument" visible="false" style="text-align: left;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Compliance Related Documents</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblWorkingFiles">
                                                                <thead>
                                                                    <th>Compliance Working Files</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>

                            </fieldset>
                        </div>


                        <div id="AuditLog2" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog2">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog2"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseAuditLog2" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory2" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory2_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory2_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory2_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory2_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>

                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                            </div>

                                            <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                            <asp:Button ID="btnDownload2" runat="server" Style="display: none" OnClick="btnDownload2_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload2" />
                    <asp:PostBackTrigger ControlID="btnSave2" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample4" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div>
            <div class="modal fade" id="DocumentPopUp4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                            OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th>Sample Forms</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                                    runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceSampleView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <iframe src="about:blank" id="docViewerAll4" runat="server" width="100%" height="550px"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="modalDocumentPerformerViewerInternal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptComplianceVersionViewInternal" runat="server" OnItemCommand="rptComplianceVersionViewInternal_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionViewInternal"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionViewInternal" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionViewInternal" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessageTaskInternal" runat="server" Style="color: red;"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerPerformerAllInternal" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="InternalDocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#InternalDocumentPriview').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docInternalPriview" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="ConfirmationModelInternal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 85%;">
                <div class="modal-content" style="width: 100%;">

                    <div class="modal-header">
                        <div style="width: 10%; float: right">
                            <button type="button" class="close" onclick="$('#ConfirmationModelInternal').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                            <p style="font-size: 20px">Confirmation</p>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="DivYesNoConf">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div>
                                        <div style="width: 100%; margin-left: 3%;">
                                            <p id="idProceed" style="margin-left: 11%; width: 65%;">Proceeds to subsequent checks?</p>
                                            <div id="taskslower" style="padding: 4px; width: 98%; overflow: auto; color: #333; padding-left: 0px; display: none;">
                                            </div>

                                            <div id="yesnobtn" style="margin-left: 45%;">
                                                <asp:LinkButton ID="lblYes" runat="server" Text="Yes" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="javascript:return callOnButtonYesInternal()"></asp:LinkButton>
                                                <asp:LinkButton ID="lblNo" runat="server" Text="No" Style="margin-left: 15px;" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="javascript:return callOnButtonNoInternal()"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="lblNo" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="IntTaskPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#IntTaskPopUp').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="IntTaskViewerAll" frameborder="0" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ConfirmationIntDocumentModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 85%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <div style="width: 10%; float: right">
                            <button type="button" class="close" onclick="$('#ConfirmationIntDocumentModel').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                            <p style="font-size: 20px">Document(s)</p>
                        </div>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="width: 100%; margin-left: 3%;">
                                        <div id="taskIntDocslower" style="padding: 4px; width: 98%; overflow: auto; color: #333; padding-left: 0px; display: none;">
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="InternalDocumentShowPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdatleMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptLicenseVersionView" runat="server" OnItemCommand="rptLicenseVersionView_ItemCommand"
                                                                OnItemDataBound="rptLicenseVersionView_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblLicenseDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("LicenseID") + ","+ Eval("Version") + ","+ Eval("FileID") %>'
                                                                                        ID="lblLicenseVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblLicenseVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptLicenseVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerInternalLicenseAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script type="text/javascript" src="../Newjs/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="../Newjs/jquery.nicescroll.js"></script>
        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="../Newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="../Newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="../Newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="../Newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="../Newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
        <script type="text/javascript" src="../Newjs/scripts.js"></script>
        <asp:HiddenField runat="server" ID="mont" />
    </form>
</body>
</html>


