﻿<%@ Page Title="User-Customer Mapping" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UserCustomerMapping_Details.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.UserCustomerMapping_Details" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .custom-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 210px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#divModifyCategory').dialog({
                height: 350,
                width: 580,
                top: 100,
                autoOpen: false,
                draggable: true,
                title: "User Customer Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
        function initializeComboboxForAssignmentsDialog() {

            $("#<%= ddldistributor.ClientID %>").combobox();
            $("#<%= ddlDistributorpopup.ClientID %>").combobox();
            
            $("#<%= ddlCustomer.ClientID %>").combobox();
            $("#<%= ddlCustomerpopup.ClientID %>").combobox();

            $("#<%= ddlproduct.ClientID %>").combobox();
            $("#<%= ddlproductpopup.ClientID %>").combobox();

            $("#<%= ddlUser.ClientID %>").combobox();
            $("#<%= ddlUserpopup.ClientID %>").combobox();

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCategoryList" runat="server" UpdateMode="Conditional" OnLoad="upCategoryList_Load">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom">
                        <%--<div id="divCustomerfilter" runat="server" style="margin-left: 300px">--%>                        
                        <div style="width: 150px; float: left; margin-top: 0px;">
                            <asp:DropDownList runat="server" ID="ddldistributor" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddldistributor_SelectedIndexChanged" />
                        </div>                        
                    </td>
                    <td style="vertical-align: bottom">                        
                        <div style="width: 150px; float: left; margin-top: 0px; margin-left: 90px;">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />
                        </div>
                    </td>
                    <td style="vertical-align: bottom">                        
                        <div style="width: 150px; float: left; margin-top: 0px; margin-left: 81px;">
                            <asp:DropDownList runat="server" ID="ddlproduct" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlproduct_SelectedIndexChanged" />
                        </div>

                    </td>

                    <td style="vertical-align: bottom">                        
                        <div style="width: 150px; float: left; margin-top: 0px; margin-left: 81px;">
                            <asp:DropDownList runat="server" ID="ddlUser" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged" />
                        </div>
                    </td>
                    <td align="right">
                        <div style="width: 150px; float: left; margin-top: 0px; margin-left: 81px;">
                        <asp:TextBox runat="server" ID="tbxFilter" Width="200px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">


                <asp:GridView runat="server" ID="grdCategorization" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" Font-Size="12px"
                    OnPageIndexChanging="grdCategorization_PageIndexChanging"
                    OnRowCommand="grdCategorization_RowCommand">
                    <Columns>

                        <asp:BoundField DataField="ID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="35%" />
                        <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="35%" />
                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%--OnClientClick="fopenpopup()"--%>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Category"
                                    CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon.png" alt="Edit Category" title="Edit Category Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Category"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Category Details?');"><img src="../../Images/delete_icon.png" alt="Delete Category Details" title="Delete Category Details" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divModifyCategory">
        <asp:UpdatePanel ID="upModifycategory" runat="server" UpdateMode="Conditional" OnLoad="upModifycategory_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <div style="margin-bottom: 4px">
                                    <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                        ValidationGroup="ModifyAsignmentValidationGroup" />
                                    <asp:CustomValidator ID="CustomModifyDepartment" runat="server" EnableClientScript="False"
                                        ValidationGroup="ModifyAsignmentValidationGroup" Display="None" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; font-size: 13px; color: #333;">
                                    Select Distributor</label>
                            </td>
                            <td style="width: 35%;">
                                <asp:DropDownList runat="server" ID="ddlDistributorpopup" Style="padding: 0px; margin: 0px; height: 22px; width: 260px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlDistributorpopup_SelectedIndexChanged" />
                            </td>
                            <td style="width: 15%;"></td>
                            <td style="width: 35%;"></td>

                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; font-size: 13px; color: #333;">
                                    Select Customer</label>
                            </td>
                            <td style="width: 35%;">
                                <asp:DropDownList runat="server" ID="ddlCustomerpopup" Style="padding: 0px; margin: 0px; height: 22px; width: 260px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerpopup_SelectedIndexChanged" />
                            </td>
                            <td style="width: 15%;"></td>
                            <td style="width: 35%;"></td>

                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; font-size: 13px; color: #333;">
                                    Select Product</label>
                            </td>
                            <td style="width: 35%;">
                                <asp:DropDownList runat="server" ID="ddlproductpopup" Style="padding: 0px; margin: 0px; height: 22px; width: 260px;"
                                    CssClass="txtbox" />
                            </td>
                            <td style="width: 15%;"></td>
                            <td style="width: 35%;"></td>

                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; font-size: 13px; color: #333;">
                                    Select User</label>
                            </td>
                            <td style="width: 35%;">
                                <asp:DropDownList runat="server" ID="ddlUserpopup" Style="padding: 0px; margin: 0px; height: 22px; width: 260px;"
                                    CssClass="txtbox" />
                            </td>
                            <td style="width: 15%;"></td>
                            <td style="width: 35%;"></td>

                        </tr>
                        <tr>
                            <td colspan="4" style="margin-bottom: 7px;">
                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <div style="margin-bottom: 7px; margin-top: 10px; margin-left: 25%;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                        CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divModifyCategory').dialog('close');" />
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

