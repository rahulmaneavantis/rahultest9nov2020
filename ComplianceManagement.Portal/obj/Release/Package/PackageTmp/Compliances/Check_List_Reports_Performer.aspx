﻿<%@ Page Title="Canned Reports" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true"
    CodeBehind="Check_List_Reports_Performer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Check_List_Reports_Performer" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/CannedReportPerformer.ascx" TagName="CannedReportPerformer"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('CheckList Report Performer');
        });
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }
        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function myFunction() {
            $('#ContentPlaceHolder1_divFilterLocationPerformer').show();
        }

    </script>
    <style>
        .table thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="margin: 10px 20px 10px 10px; display: none">
        <div>
            Role :
        <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow"
            OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Performer" Selected="True" />
        </asp:RadioButtonList>
        </div>
        <div style="width: 100px; float: left; margin-left: 335px; margin-top: -30px;">
        </div>
    </div>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
              <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="active">                                                     
                                                        <asp:LinkButton ID="liPerformer"  runat="server">Performer</asp:LinkButton>
                                                    </li>
                                        </ul>
                                </header>
   <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-2 colpadding0 entrycount">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 70px; float: left">
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>

                <div class="col-md-9 colpadding0" style="text-align: right; float: right">
                    <div class="col-md-8 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlComplianceType" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" class="form-control m-bot15 search-select">
                            <asp:ListItem Text="Statutory" Value="-1" />
                             <asp:ListItem Text="Event Based" Value="0" />
                        </asp:DropDownList>

                        <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select">
                            <asp:ListItem Text="Risk" Value="-1" />
                             <asp:ListItem Text="Critical" Value="3" />
                             <asp:ListItem Text="Medium" Value="1" />
                            <asp:ListItem Text="High" Value="0" />
                            <asp:ListItem Text="Low" Value="2" />
                           
                        </asp:DropDownList>

                        <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select">
                        </asp:DropDownList>

                          <div style="float: left; margin-right: 2%;" id="divloc">
                        <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationPerformer" onfocus="myFunction()" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 140px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10;" id="divFilterLocationPerformer" runat="server">
                            <asp:TreeView runat="server" ID="tvFilterLocationPerformer" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocationPerformer_SelectedNodeChanged" >
                            </asp:TreeView>
                        </div>
                    </div>

                      <%--  <asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 search-select">
                        </asp:DropDownLis--%>

                    </div>
                    <div class="col-md-4 colpadding0">
                        <div class="col-md-6 colpadding0">
                            <%--<a class="btn btn-search" href="" title="Search">Search</a>--%>
                            <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" runat="server" Text="Apply" />

                        </div>
                        <div class="col-md-6">
                           <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" style="margin-top: -40px; position: absolute; width: 126px;" OnClick="lbtnExportExcel_Click" runat="server"/>
                            <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advance Search</a>
                        </div>
                    </div>

                </div>

                <!--advance search starts-->
                <div class="col-md-12 AdvanceSearchScrum">
                    <div id="divAdvSearch" runat="server" visible="false">
                        <p>
                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                        </p>
                        <p>
                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                        </p>
                    </div>

                     <div runat="server" id="DivRecordsScrum" style="float: right;">
             
             <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
            </div>

                    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    
                                </div>
                                <div class="modal-body">
                                    <h2 style="text-align: center">Advanced Search</h2>
                                    
                                    <div class="col-md-12 colpadding0">
                                        <div class="table-advanceSearch-selectOpt">
                                            <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="table-advanceSearch-selectOpt">
                                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Panel ID="PanelAct" runat="server">
                                            <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                                <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                                                </asp:DropDownList>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel1" runat="server">
                                             <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                              <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtStartDate" CssClass="StartDate"/>
                                           </div>  </asp:Panel>
                                        <div class="clearfix"></div>
                                         <asp:Panel ID="Panel2" runat="server">
                                             <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                             <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtEndDate" CssClass="StartDate"/>
                                            </div> </asp:Panel>

                                        <div class="table-advanceSearch-buttons">
                                            <table id="tblSearch">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnAdvSearch" Text="Search" class="btn btn-search" OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                                                    </td>
                                                    <td>
                                                        <button id="btnAdvClose" type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="clearfix"></div>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--advance search ends-->
                </div>

             <%--   OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"--%>
                <div style="float: right; margin-right: 10px; margin-top: -35px;">
                    <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" 
                        DataKeyField="ID">
                        <SeparatorTemplate>
                            <span style="margin: 0 5px 0 5px">|</span>
                        </SeparatorTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                        <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                            Font-Underline="true" />
                    </asp:DataList>
                </div>
                <asp:GridView runat="server" ID="grdComplianceTransactions" CssClass="table" BorderWidth="0px" AutoGenerateColumns="false"
                    GridLines="None" OnRowCreated="grdComplianceTransactions_RowCreated" PageSize ="5" AllowPaging="True" OnSorting="grdComplianceTransactions_Sorting"
                    DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging" OnRowDataBound="grdComplianceTransactions_RowDataBound">
                    <HeaderStyle CssClass="clsheadergrid" />
                    <RowStyle CssClass="clsROWgrid" />
                    <Columns>
                       <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                          <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                          </ItemTemplate>
                       </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Performer">
                       <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblPerformer" runat="server" Text='<%# GetPerformer((int)Eval("ComplianceStatusID") ,(long)Eval("ScheduledOnID"), (long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                        </div>
                      </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Month">
                                        <ItemTemplate>
                                            <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due Date">
                            <ItemTemplate>
                               <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Name">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                <asp:Label ID="lblEventName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Event Nature">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                <asp:Label ID="lblEventNature" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                       <%-- <asp:BoundField DataField="ForMonth" HeaderText="Period" />--%>
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                </asp:GridView>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="table-paging" style="margin-bottom: 20px">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                        </div>
                    </div>
                </div>
                    </section>
                    </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>
        </ContentTemplate>
     <Triggers>
            <asp:PostBackTrigger ControlID ="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
