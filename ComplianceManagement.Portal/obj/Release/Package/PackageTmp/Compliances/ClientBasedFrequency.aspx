﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ClientBasedFrequency.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ClientBasedFrequency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;            
            padding: 0;
        }
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 450px;
        }
    </style>
      <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
           
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterCustomerList_SelectedIndexChanged">
                        </asp:DropDownList>

                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlFilterCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterFrequencies_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterAct" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterAct_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 20%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="190px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td>
                        <u><a href="../../ExcelFormat/ClientBasedFrequency_Sample.xlsx">Sample Format - Check Client Frequency</a></u>
                    </td>
                    <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddFrequency" OnClick="btnAddFrequency_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; padding-right: 20px;" align="right">
                         <asp:FileUpload ID="FU_Upload" runat="server" />
                    </td>
                    <td class="newlink" align="right" style="width: 70px">
                        
                        <asp:Button Text="Upload" runat="server" ID="btnUploadSave" OnClick="btnUploadSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
              <div style="margin: 5px 5px;">
                    <div style="margin-bottom: 4px">
                        <%--<asp:ValidationSummary ID="vsLicenseListPage1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup1" />

                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>

                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />

                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                 </div>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdClient" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                         <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ComplianceID" />
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Avacom Frequency" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="FrequencyName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("AvacomFrequencyName") %>' ToolTip='<%# Eval("AvacomFrequencyName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Client Frequency" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="ClientFrequency">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("ClientFrequency") %>' ToolTip='<%# Eval("ClientFrequency") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE"                                     
                                    CommandArgument='<%# Eval("ID")+";"+ Eval("CustomerID")+";"+Eval("ComplianceID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadSave" />
        </Triggers>
    </asp:UpdatePanel>

    <div id="divClientFrequencyDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>

                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="vsLicenseListPage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ComplianceValidationGroup" />

                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>

                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer Name</label>
                        <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlCustomerList" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act"
                            ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:DropDownList runat="server" ID="ddlshortdescription" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlshortdescriptionList_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select short Description"
                            ControlToValidate="ddlshortdescription" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 40px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Avacom Frequency</label>
                        <asp:Label ID="lblAvacomFrequency" runat="server" Text="" CssClass="label"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">                       
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Client Frequency</label>
                        <asp:DropDownList runat="server" ID="ddlClientFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlClientFrequency_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Client Frequency"
                            ControlToValidate="ddlClientFrequencies" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 210px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divClientFrequencyDetailsDialog').dialog('close');" />
                    </div>
                   
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    
    <script type="text/javascript">
        $(function () {
            $('#divClientFrequencyDetailsDialog').dialog({
                height: 450,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Client Frequency",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });     
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlCustomerList.ClientID %>").combobox();
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlshortdescription.ClientID %>").combobox();
            $("#<%= ddlClientFrequencies.ClientID %>").combobox();
            
            
        }
    </script>
</asp:Content>
