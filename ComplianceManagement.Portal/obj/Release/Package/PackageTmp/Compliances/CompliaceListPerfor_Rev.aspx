﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="CompliaceListPerfor_Rev.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CompliaceListPerfor_Rev" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                    </div>
                <div id="FilterLocationdiv" runat="server" style="float: left; margin-left: 100px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                    <%--<asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>--%>
                </div>
                <div runat="server" id="divComType" style="margin-left: 20px; margin-top: 5px; float: left;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Compliace Type
                    </label>
                    <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                        <asp:ListItem Text="Statutory" Value="-1" />
                        <asp:ListItem Text="Internal" Value="0" />
                        <asp:ListItem Text="Event Based" Value="1"/>
                         <asp:ListItem Text="CheckList" Value="2" />
                    </asp:DropDownList>
                </div>
                <div runat="server" id="div1" style="margin-right: 20px; margin-top: 5px; float: right;">
                <asp:Button ID="btnExporttoExcel" Text="Export to Excel" class="btn btn-search" style="width: 100%;float: right;" OnClick="btnExporttoExcel_Click" runat="server"/>
                </div>
                    <br />
                <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server" style="margin-top: 23px;">
                <asp:GridView runat="server" ID="grdComplianceType" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdComplianceType_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdComplianceType_Sorting"
                    Font-Size="12px" DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceType_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ComplianceID" />
                      <%--  <asp:TemplateField HeaderText="Compliance Instance ID">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label runat="server" Text='<%# Eval("ComplianceInstanceID") %>' ToolTip='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Description" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%--  <asp:BoundField DataField="StartDate" HeaderText="StartDate" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>--%>
                         <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <%--  <asp:BoundField DataField="StartDate" HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>--%>
                        <asp:BoundField DataField="Performer" HeaderText="Performer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>
                        <asp:BoundField DataField="Reviewer" HeaderText="Reviewer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>
                       
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                 </asp:Panel> 
                <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server" Visible="false">
                <asp:GridView runat="server" ID="grdComplianceTypeInternal" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                    Font-Size="12px" DataKeyNames="InternalComplianceInstanceID" OnPageIndexChanging="grdComplianceTypeInternal_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="InternalComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" Visible="false" HeaderStyle-Height="20px" />
                        <asp:TemplateField HeaderText="Internal Comp Instance ID">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label runat="server" Text='<%# Eval("InternalComplianceInstanceID") %>' ToolTip='<%# Eval("InternalComplianceInstanceID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:BoundField DataField="Branch" HeaderText="Location" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>
                        <asp:BoundField DataField="Performer" HeaderText="Performer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>
                        <asp:BoundField DataField="Reviewer" HeaderText="Reviewer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px"/>
                       
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                 </asp:Panel> 
                </div>

            </ContentTemplate>
         <Triggers>
           <asp:PostBackTrigger ControlID="btnExporttoExcel" />
       </Triggers>
         </asp:UpdatePanel>
</asp:Content>
