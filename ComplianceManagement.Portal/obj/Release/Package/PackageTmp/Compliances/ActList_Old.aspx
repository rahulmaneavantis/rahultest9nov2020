﻿<%@ Page Title="Act List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ActList_Old.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ActList_Old" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/ComplienceStyleSeet.css" rel="stylesheet" />
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
     <script type="text/javascript">
      function initializeCombobox() {
    }
  </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .ui-state-active {
            border: 1px solid #fad42e !important;
            background: #fbec88 url(images/ui-bg_flat_55_fbec88_40x100.png) 50% 50% repeat-x !important;
            color: #363636 !important;
        }
         .ui-datepicker-div {
            top: 482.453px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:DropDownList runat="server" ID="ddlfilterCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlFilterType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddAct_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdAct" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdAct_RowDataBound"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAct_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" OnSorting="grdAct_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdAct_RowCommand" OnPageIndexChanging="grdAct_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="Act ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10px" />
                    <asp:TemplateField HeaderText="Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ComplianceCategoryName" HeaderText="Compliance Category" ItemStyle-Width="200px" SortExpression="ComplianceCategoryName"
                        ItemStyle-VerticalAlign="Top" />
                    <asp:BoundField DataField="ComplianceTypeName" HeaderText="Compliance Type" ItemStyle-VerticalAlign="Top" SortExpression="ComplianceTypeName" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="State" HeaderText="State" ItemStyle-VerticalAlign="Top" ItemStyle-Width="200px" SortExpression="State" />
                    <asp:BoundField DataField="City" HeaderText="City" ItemStyle-VerticalAlign="Top" ItemStyle-Width="200px" SortExpression="City" />
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_ACT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Act" title="Edit Act" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this act?');"><img src="../Images/delete_icon.png" alt="Delete Act" title="Delete Act" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divActDialog" style="height: auto;">
        <asp:UpdatePanel ID="upAct" runat="server" UpdateMode="Conditional" OnLoad="upAct_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ActValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ActValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 30px; width: 390px;" MaxLength="100" ToolTip="" TextMode="MultiLine" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ActValidationGroup" Display="None" />
                        <%-- <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="ActValidationGroup"
                            ErrorMessage="Please enter a valid act name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z][a-zA-Z0-9_ ]*"></asp:RegularExpressionValidator>--%>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Type." ControlToValidate="ddlType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Category</label>
                        <asp:DropDownList runat="server" ID="ddlCategory" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                        <asp:CompareValidator ErrorMessage="Please select Category." ControlToValidate="ddlCategory"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>
                       <div style="margin-bottom:19px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Due Date Type (Holiday)</label>
                        <asp:DropDownList runat="server" ID="ddlDueDateType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true">
                                <asp:ListItem Selected="True" Value="0">After Due Date </asp:ListItem>
                                <asp:ListItem Value="1">Before Due Date </asp:ListItem>
                            </asp:DropDownList>
                        <asp:CompareValidator ErrorMessage="Please select DueDate Type." ControlToValidate="ddlDueDateType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divState" visible="false" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            State</label>
                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                              <asp:CompareValidator ID="cmpvState" ErrorMessage="Please select State." ControlToValidate="ddlState"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divCity" style="margin-bottom: 7px">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            City</label>
                        <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                    </div>
                    <div runat="server" id="div1" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Start Date</label>
                        <asp:TextBox runat="server" ID="tbxStartDate" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                            ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ActValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Industry
                        </label>
                        <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 0px; margin: 0px; height: 30px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 150px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px;" id="dvIndustry">
                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Department</label>
                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Ministry</label>
                        <asp:DropDownList runat="server" ID="ddlMinistry" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Regulator</label>
                        <asp:DropDownList runat="server" ID="ddlRegulator" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>
                    <fieldset style="border-style: solid; border-width: 1px; height:325px; border-color: #dddddd; width: 100%;">
                        <div style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Act Document Type</label>
                            <asp:DropDownList runat="server" ID="ddlActDocType" OnSelectedIndexChanged="ddlActDocType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                CssClass="txtbox" AutoPostBack="true" />
                            <asp:CompareValidator ErrorMessage="Please select Act Document Type." ControlToValidate="ddlActDocType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ActValidationGroup"
                                Display="None" />
                        </div>

                        <div style="margin-bottom: 7px" id="divDate" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Month-Year
                            </label>
                            <asp:TextBox runat="server" ID="txtActDocVerstionDate" CssClass='monthYearPicker' Style="height: 30px; width: 150px;" />
                            <asp:RequiredFieldValidator ID="rfvDate" ErrorMessage="Please Select Month and Year"
                                ControlToValidate="txtActDocVerstionDate" runat="server" ValidationGroup="ActValidationGroup"
                                Display="None" />
                        </div>

                        <div runat="server" id="divfile" style="margin-bottom: 7px; margin-left: 2%;">
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                File</label>
                            <asp:FileUpload runat="server" ID="ActFile_upload" AllowMultiple="true" />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button Text="Save Document" runat="server" ID="btnActDocSave" OnClick="btnActDocSave_Click" Style="width: 150px; margin-left: 150px; margin-top: 5px;" CssClass="button" ValidationGroup="ActValidationGroup" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnActDocSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div runat="server" id="GrdShowForFile" style="margin-top: 4%; height: 100px;" visible="false">
                                <asp:Panel ID="Panel2" Width="100%" Height="180px" ScrollBars="Vertical" runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="GrdforFileName" runat="server" AutoGenerateColumns="false" GridLines="Vertical"
                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" Width="100%"
                                                AllowSorting="true"
                                                Font-Size="12px" DataKeyNames="ID" OnRowCommand="GrdforFileName_RowCommand">
                                                <Columns>
                                                    <%-- <asp:BoundField DataField="FileName" HeaderText="File Name" HeaderStyle-HorizontalAlign="Left" />--%>
                                                    <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:TemplateField HeaderText="File Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                <asp:Label runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="DocumentType" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                <asp:Label runat="server" Text='<%# Eval("DocumentType") %>' ToolTip='<%# Eval("DocumentType") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VersionDate" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                                                        <ItemTemplate>
                                                            <%# Eval("Act_TypeVersionDate")!= null?((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy"):""%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDownload" Text="Download" CommandArgument='<%# Eval("Id") %>' runat="server" OnClick="lnkDownload_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDelete" Text="Delete" CommandArgument='<%# Eval("Id") %>' runat="server" OnClick="lnkDelete_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="140px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="upFileUploadPanel" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="lbkDownload" runat="server" CommandName="DownloadActDocument" CommandArgument='<%# Eval("ID") + "," + Eval("Act_ID") %>' ToolTip="Download Document"><img src="../Images/downloaddoc.png" alt="Download"/></asp:LinkButton>
                                                                    <asp:LinkButton ID="lbkDelete" runat="server" CommandName="DeleleActDocument" CommandArgument='<%# Eval("ID") + "," + Eval("Act_ID") %>'
                                                                        OnClientClick="return confirm('Are you sure you want to delete this Act Document?');" ToolTip="Delete Document"><img src="../Images/delete_icon.png" alt="Delete Form"/></asp:LinkButton>
                                                                    <asp:LinkButton ID="lbkView" runat="server" CommandName="ViewActDocument" CommandArgument='<%# Eval("ID") + "," + Eval("Act_ID") %>' ToolTip="View Document"><img src="../Images/package_icon.png" alt="View Form"/></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="lbkDownload" />
                                                                    <asp:AsyncPostBackTrigger ControlID="lbkDelete" />
                                                                    <asp:PostBackTrigger ControlID="lbkView" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="#CCCC99" />
                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                <PagerSettings Position="Top" />
                                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="GrdforFileName" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                                </asp:Panel>
                                <div style="margin-bottom: 7px; margin-left: 0px" id="Div5" runat="server">
                                    <asp:Label ID="Label2" runat="server" Style="color: red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div id="DivSave" style="margin-bottom: 7px; margin-top:4%; margin-left: 152px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClientClick="if (!ValidateFile()) return false;" OnClick="btnSave_Click" CssClass="button" ValidationGroup="ActValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>

                        <%--   <label style="width: 208px; display: block; float: left; font-size: 13px; color: red;">Note :: (*) Fields Are Compulsary</label>--%>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divActDialog').dialog({
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Act",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divActDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divActDialog").dialog('open');

        }
        function initializeCombobox() {
            $("#<%= ddlCategory.ClientID %>").combobox();
            $("#<%= ddlType.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
            $("#<%= ddlCity.ClientID %>").combobox();
            $("#<%= ddlDepartment.ClientID %>").combobox();
            $("#<%= ddlMinistry.ClientID %>").combobox();
            $("#<%= ddlRegulator.ClientID %>").combobox();
            $("#<%= ddlActDocType.ClientID %>").combobox();
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        var validFilesTypes = ["exe", "bat", "dll"];

        function ValidateFile() {

            var label = document.getElementById("<%=Label2.ClientID%>");
            var fuSampleFile = $("#<%=ActFile_upload.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

        function fopendocfileReview(path) {

            if (path != '') {
                window.open('../docviewer.aspx?docurl=' + path);
            }
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        //$(document).ready(function () {
        //    function WireUpValues() {               
        //        $('input[data-role="tagsinput"]').tagsinput({

        //        });
        //    }
        //});

        //function ResolveUrl() {
           
        //    $('input[data-role="tagsinput"]').tagsinput({
        //    });
        //}

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        function initializeDatePicker(todayDate) {

            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;

            $(".StartDate").datepicker({
                //showOn: 'button',
                // buttonImageOnly: true,
                //buttonImage: '../Images/Cal.png',
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
                ,
                beforeShow: function (input, inst) {

                    var date = $("#<%= tbxStartDate.ClientID %>").val();

                    if (date.toString().trim() != '') {

                        //startDate = date;
                        setTimeout(function () {
                            inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
                        }, 100);
                    }


                    // $(".StartDate").datepicker('setDate', startDate);
                    $(".StartDate").datepicker('setDate', date);

                },
                onClose: function (dateText, inst) {

                    if (dateText != null) {
                        $("#<%= tbxStartDate.ClientID %>").val(dateText);
                }
                }
            });

        $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
            $(this).attr('title', 'Select Start Date');
        });


        $('.monthYearPicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M yy'
        }).focus(function () {
            var thisCalendar = $(this);
            $('.ui-datepicker-calendar').detach();
            $('.ui-datepicker-close').click(function () {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                thisCalendar.datepicker('setDate', new Date(year, month, 1));
            });
        });
        }
    </script>
    
</asp:Content>
