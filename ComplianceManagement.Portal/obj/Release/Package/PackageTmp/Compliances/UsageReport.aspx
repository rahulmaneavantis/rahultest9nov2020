﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="UsageReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.UsageReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <title></title>
     <style type="text/css">
        div#ContentPlaceHolder1_Div3 {
            margin-left: 21px;
        }
       </style>
    <script type="text/javascript">
       

        $(document).ready(function () {
            setactivemenu('Myreport2');
            fhead('Usage Report');
            fmaters1();
        });


        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-M-yy',
                minDate: $('#ContentPlaceHolder1_udcCannedReportPerformer_txtStartDate').val(),
                maxDate: $('#ContentPlaceHolder1_udcCannedReportPerformer_txtEndDate').val(),
                setDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2025',
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel1_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary" runat="server"
                    ValidationGroup="ValidationUsageReport" class="alert alert-block alert-danger fade in" ForeColor="Red" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ValidationUsageReport" Display="None" class="alert alert-block alert-danger fade in" ForeColor="Red" />
                <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
            </div>
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-12 colpadding0" style="float: right">                                            
                                                  <div class="col-md-5 colpadding0 entrycount" style="float:left;margin-right: -1%; width:33.667% !important">
                                                      <label style=" display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Customer Branch</label>
                                                     <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 320px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                        CssClass="txtbox" />   
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="320px"   NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                        </asp:TreeView>
                                                    </div>  
                                               </div>
                                                <div id="Div3" runat="server" class="table-advanceSearch-selectOpt" style="width:130px;">
                                                    <label style=" display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    From Date</label>
                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="From Date"
                    ID="txtStartDate" CssClass="StartDate form-group form-control" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Previous Date can not be empty."
                            ControlToValidate="txtStartDate" runat="server" ValidationGroup="ValidationUsageReport"
                            Display="None" />
            </div>
            <div id="Div4" runat="server" class="table-advanceSearch-selectOpt" style="width:130px;">
                <label style=" display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    To Date</label>
                <asp:TextBox runat="server"
                    Style="padding-left: 7px;" placeholder="To Date"
                    CssClass="StartDate form-group form-control" ID="txtEndDate" />

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="To Date can not be empty."
                            ControlToValidate="txtEndDate" runat="server" ValidationGroup="ValidationUsageReport"
                            Display="None" />
            </div>

                                            <div id="Div1" runat="server" class="table-advanceSearch-selectOpt" style="width:130px;">
                <label style=" display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Role</label>
              <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select"  style="width:100%; max-width: 135px !important;" >
                  <%--  <asp:ListItem Value="0"> Select </asp:ListItem>--%>
                     <asp:ListItem Value="1"> Managment </asp:ListItem>  
                  <%-- 
                    <% if (roles.Contains(3)){%>
                                                 
                     <asp:ListItem Value="2"> Performer </asp:ListItem>
                    <%}%>
                   <% if (roles.Contains(4)) {%>
                                                
                     <asp:ListItem Value="3"> Reviewer </asp:ListItem>  
                  <%}%>--%>
                   </asp:DropDownList>
                                              <%--       <asp:CompareValidator ErrorMessage="Please Select Role." ControlToValidate="ddlStatus"
                            runat="server" ValueToCompare="0" Operator="NotEqual" ValidationGroup="ValidationUsageReport"
                            Display="None" />--%>
            </div>
                <div class="col-md-3 colpadding0 entrycount" style="float:left;margin-right: 1%; width:20%">
                                                      
                                                   </div>

                                            <div class="col-md-2 colpadding0" style="width: 22.333333%; float:right">
                                                <div class="col-md-6 colpadding0">  <label style=" display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    &nbsp;</label>                                                  
                                                    <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" CausesValidation="false" class="btn btn-search" Style="margin-left:33px !important;" runat="server" Text="Export To Excel" />
                                                </div>
                                            </div> 
                                            </div>         
                                        </div>
                                </div>                        
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

