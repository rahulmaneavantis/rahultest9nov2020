﻿<%@ Page Title="Holiday List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="HolidayList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.HolidayList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .HolidayDatePicker .ui-datepicker-year {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upHolidayList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 20%">
                        <asp:DropDownList runat="server" ID="ddlFilterState" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterState_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 25%">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddHoliday" OnClick="btnAddHoliday_Click" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdHoliday" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdHoliday_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" Width="100%" OnSorting="grdHoliday_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdHoliday_RowCommand" OnPageIndexChanging="grdHoliday_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="Holiday ID" ItemStyle-VerticalAlign="Top" ItemStyle-Width="80px" SortExpression="ID" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" ItemStyle-Height="25px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" SortExpression="Name">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# string.Format("{0}", Convert.ToDateTime(Eval("Month") + "/" + Eval("Day") + "/" + Eval("HYear")).Date.ToString("dddd, dd MMMM yyyy"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ComplianceTypeName" HeaderText="Compliance Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ComplianceTypeID" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="State" HeaderText="State" ItemStyle-VerticalAlign="Top" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="State" />

                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_HOLIDAY" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Holiday" title="Edit Holiday" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_HOLIDAY" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('Are you certain you want to delete this Holiday?');"><img src="../Images/delete_icon.png" alt="Delete Holiday" title="Delete Holiday" /></asp:LinkButton>

                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divHolidayDialog">
        <asp:UpdatePanel ID="upHoliday" runat="server" UpdateMode="Conditional" OnLoad="upHoliday_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 7px; width: 600px;">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="HolidayValidationGroup" Style="width: 600px;" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="HolidayValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; width: 600px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 30px; width: 390px;" MaxLength="100" ToolTip="" TextMode="SingleLine" CssClass="txtbox" onkeypress="return checkSpecialChar(event)" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="HolidayValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 30px; width: 200px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select type." ControlToValidate="ddlComplianceType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="HolidayValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divState" visible="false" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            State</label>
                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 30px; width: 200px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="cvState" ErrorMessage="Please select state." ControlToValidate="ddlState"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="HolidayValidationGroup"
                            Display="None" />
                    </div>
                  

                      <div style="margin-bottom: 7px" id="divDate" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                           Date
                        </label>
                     
                        <asp:TextBox runat="server" CssClass="StartDate" ID="tbxDate" Style="height: 30px; width: 100px;" />
                        <asp:RequiredFieldValidator ID="rfvDate" ErrorMessage="Please select date."
                            ControlToValidate="tbxDate" runat="server" ValidationGroup="HolidayValidationGroup"
                            Display="None" />

                    </div>
                    <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 50px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="HolidayValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divHolidayDialog').dialog('close');" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divHolidayDialog').dialog({
                height: 400,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Holiday",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
        }

        function checkSpecialChar(event) {
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 47) && (event.keyCode <= 57) || (event.keyCode == 40) || (event.keyCode == 41) || (event.keyCode == 32))) {
                event.returnValue = false;
                return;
            }
            event.returnValue = true;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };


        function initializeDatePicker(todayDate) {            
            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;           

            $(".StartDate").datepicker({
                //showOn: 'button',
               // buttonImageOnly: true,
                //buttonImage: '../Images/Cal.png',
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
                ,
               beforeShow: function (input, inst) {

                    var date = $("#<%= tbxDate.ClientID %>").val(); 
                   
                   if (date.toString().trim() != '' ) {
                      
                       //startDate = date;
                       setTimeout(function () {
                           inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
                       }, 100);
                   }                                    
                   $(".StartDate").datepicker('setDate', date);
                },
                onClose: function (dateText, inst) {
        
                    if (dateText != null) {                       
                        $("#<%= tbxDate.ClientID %>").val(dateText);
                    }
                }
            });

            $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
                $(this).attr('title', 'Select Start Date');
            });           
        }
      <%--  function initializeDatePicker(date) {
            var startDate = new Date();


            $(".StartDate").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../Images/Cal.png',
                dateFormat: 'dd-MM',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: new Date().getFullYear().toString() + ':' + new Date().getFullYear().toString(),
                beforeShow: function (input, inst) {

                    var dd = $("#<%= tbxDay.ClientID %>").val();
                    var mm = $("#<%= tbxMonth.ClientID %>").val();
                    var yy = startDate.getFullYear();

                    var mmNo = monthNameToNum(mm.toString());

                    if (parseInt(dd) > 0 && parseInt(mmNo) > 0) {
                        startDate = new Date(parseInt(yy), parseInt(mmNo) - 1, parseInt(dd));
                    }

                    inst.dpDiv.addClass('HolidayDatePicker');
                    $(".StartDate").datepicker('setDate', startDate);
                    $("#<%= tbxDay.ClientID %>").attr("style", "display:none;");
                },
                onClose: function (dateText, inst) {

                    var month = dateText.split('-')[1];
                    var day = dateText.split('-')[0];

                    if (month != null && day != null) {
                        $("#<%= tbxDay.ClientID %>").attr("style", "height: 22px; width: 50px; float: left;");
                        $("#<%= tbxDay.ClientID %>").val(day);
                        $("#<%= tbxMonth.ClientID %>").val(month);
                    }
                }

            });

            $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
                $(this).attr('title', 'Select Date');
            });
        }--%>

        function monthNameToNum(monthname) {
            var months = ['January', 'February', 'March', 'April', 'May',
                         'June', 'July', 'August', 'September',
                         'October', 'November', 'December'];

            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
    </script>
</asp:Content>
