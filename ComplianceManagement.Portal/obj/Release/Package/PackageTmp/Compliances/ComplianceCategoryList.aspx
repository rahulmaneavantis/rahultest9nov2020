﻿<%@ Page Title="Compliance Category List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ComplianceCategoryList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceCategoryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceCategoryList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddComplianceCategory" OnClick="btnAddComplianceCategory_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdComplianceCategory" AutoGenerateColumns="false" OnRowDataBound="grdComplianceCategory_RowDataBound"
                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnRowCreated="grdComplianceCategory_RowCreated"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdComplianceCategory_Sorting"
                Width="100%" Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdComplianceCategory_RowCommand"
                OnPageIndexChanging="grdComplianceCategory_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" ItemStyle-Height="20px" SortExpression="Name" />
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_COMPLIANCE_CATEGORY"
                                CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance Category" title="Edit Compliance Category" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_COMPLIANCE_CATEGORY"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this compliance category?');"><img src="../Images/delete_icon.png" alt="Delete Compliance Category" title="Delete Compliance Category" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceCategoryDialog">
        <asp:UpdatePanel ID="upComplianceCategory" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 250px;" MaxLength="100" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="ComplianceCategoryValidationGroup"
                            ErrorMessage="Please enter a valid category name." ControlToValidate="tbxName"
                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="tbxDescription" Style="height: 40px; width: 250px;" TextMode="MultiLine" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 57px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceCategoryValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceCategoryDialog').dialog('close');" />
                    </div>

                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divComplianceCategoryDialog').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Compliance Category",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
