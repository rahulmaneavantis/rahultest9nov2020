﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="assignmentReportKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.assignmentReportKendo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <script>
        $(document).ready(function () {
            fhead('My Reports / Assignment Report');
            setactivemenu('Myreport');
            fmaters1();
        });

    </script>
    <style type="text/css">

        .k-grouping-header {
            border-top: 1px solid #ceced2;
        }
        span.k-icon.k-i-arrow-60-down {
    margin-top: 5px;
} 
        .k-grid-header-wrap.k-auto-scrollable {
            width: 99.9%;
        }
        table.k-selectable {
            border-right: 1px solid #ceced2;
        }

        .k-grid-content {
            min-height: 394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
       
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

          .myKendoCustomClass {
            z-index:999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.6em;
        }
       
        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 22%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

      
            .k-reset, .k-multicheck-wrap
            {
              width:300px;
            }
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
          
        }
        .k-reset
        {
            width:260px;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
            width:355px;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
            
        }
        .k-pager-numbers
        {
              width:400px;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }
           .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }
        .k-grouping-header {
             border-right: solid 1px #ceced2;
               border-left: solid 1px #ceced2;
       }
        table.k-selectable {
        /*border: solid 1px red;*/
        border-right: solid 1px #ceced2;
        }
        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {

            BindType();
            BindTypeNew();
            BindFrequency();
            BindAct();
            BindCategory();
            BindBranch();
            BindGrid();
        });

        var Descr = "Description";
        function BindGrid() {
             
          if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "S" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "C" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "E"
              || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,E" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C,E"
              || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "C,E" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C,E,I,IC" || $("#dropdownTypeNew").data("kendoDropDownTree")._values =="") {
                Descr = "Description";
            }
          else if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "I" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "IC" || $("#dropdownTypeNew").data("kendoDropDownTree")._values=="I,IC") {
                Descr = "ShortDescription";
            }

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var Locationlist = $("#dropdownTypeNew").data("kendoDropDownTree")._values;
            var LocationID = [];
            $.each(Locationlist, function (i, v) {
                LocationID.push(v);
            });


            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetAssignmentReportData?Customerid=<%=CustId%>&type=' + $("#dropdownTypeNew").data("kendoDropDownTree")._values + '&roles=<%=roles%>&Uid=<% =UId%> ',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetAssignmentReportData?Customerid=<%=CustId%>&type=' + JSON.stringify(LocationID) + '&roles=<%=roles%>&Uid=<% =UId%> ',
                        //read: '<% =Path%>/Data/GetAssignmentReportData?Customerid=<%=CustId%>&type=' +  $("#dropdownTypeNew").data("kendoDropDownTree")._values + '&roles=<%=roles%>&Uid=<% =UId%> ',
                       
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 12
                },

               

                excel: {
                    allPages: true,
                },
            
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [


                    {
                        field: "ComplianceID", title: "ComplianceId", width: "10%",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },

                      {
                          field: Descr, title: 'Compliance',
                          width: "20%;",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              },autoWidth:true,
                          }
                        
                      },

                    {
                        field: "Branch", title: 'Branch',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },


                  

                    {
                        field: "Performer", title: 'Performer',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        field: "Reviewer", title: 'Reviewer',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        hidden: true,
                        field: "Approver", title: 'Approver',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap; width:100px;'
                        }, filterable: {
                            extra: false, multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    },
                    {
                        hidden: true,
                        field: "StartDate", title: 'StartDate',
                        width: "10%",
                        type: "date",
                        //  format: "{0:dd-MM-yyy}",
                        template: "#= kendo.toString(kendo.parseDate(StartDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    },

                    {
                        hidden: true,
                        field: "ComplianceType", title: 'ComplianceType',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "State", title: 'State',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },


                ]
            });


            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the Third column's cells
                position: "top",
                width: 700,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

        }

        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                autoClose: false,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {

                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filterLocation', 'Location');
                    //  e.preventDefault();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    }
            });

        }

      
        function BindAct()  {

               $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: true,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Name",
                optionLabel: "Select Act",
                change: function (e) {
                    BindGrid();
                    FilterAll();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =roles%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =roles%>"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("1000");
                }
            });

        }
         function BindCategory()  {
               $("#dropdownCategory").kendoDropDownList({
                filter: "startswith",
                autoClose: true,
                autoWidth: true,
                dataTextField: "CategoryName",
                dataValueField: "CategoryId",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterAll();
                   
                },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Data/BindCategoriesAllNew?CId=<% =CustId%>&Type=' + $("#dropdownTypeNew").data("kendoDropDownTree")._values,
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         // read: "<% =Path%>Data/BindCategoriesAllNew?CId=<% =CustId%>&Type=" + $("#dropdownType").val()
                           //read: "<% =Path%>Data/BindCategoriesAllNew?CId=<% =CustId%>&Type=" +  $("#dropdownTypeNew").data("kendoDropDownTree")._values
                    }
                }  
            });

         }

        function ApplyBtnAdvancedFilter()
        {
            BindGrid();
        }

        function BindType() {
            debugger;
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                 
                    BindGrid();
                    BindCategory();
                },
                dataSource: [
                    //{ text: "Status", value: "-1" },
                    { text: "Statutory", value: "S" },
                    { text: "Statutory CheckList", value: "C" },
                    { text: "EventBased", value: "E" },
                    { text: "Internal", value: "I" },
                    { text: "Internal CheckList", value: "IC" },
                ]
            });
        }

        function BindTypeNew()
        {
            $("#dropdownTypeNew").kendoDropDownTree({
                placeholder: "Type",
                checkboxes: true,
                tagMode: true,
               // checkAll: true,
                autoClose: false,
                autoWidth: true,
               // checkAllTemplate: "Select All",
             
                dataTextField: "text",
                dataValueField: "value",
               
                change: function () {
                    //fCreateStoryBoard('dropdownTypeNew', 'filterTypeNew', 'Type');
                    ApplyBtnAdvancedFilter();
                    BindCategory();

                },
                dataSource: [
                    { text: "Statutory", value: "S" ,checked:true},
                    { text: "Statutory CheckList", value: "C" },
                    { text: "EventBased", value: "E" },
                    { text: "Internal", value: "I" },
                    { text: "Internal CheckList", value: "IC" },
                   
                ]
            });

        }
        function BindFrequency() {
            debugger;
            $("#dropdownFrequency").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select Frequency",
                change: function (e) {
                    FilterAll();
                    
                },
                dataSource: [
                   
                    { text: "Monthly", value: "Monthly" },
                    { text: "Quarterly", value: "Quarterly" },
                    { text: "HalfYearly", value: "HalfYearly" },
                    { text: "Annual", value: "Annual" },
                    { text: "FourMonthly", value: "FourMonthly" },
                    { text: "TwoYearly", value: "TwoYearly" },
                    { text: "SevenYearly", value: "SevenYearly" },
                    { text: "Daily", value: "Daily" },
                     { text: "Weekly", value: "Weekly" },
                ]
            });
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filterLocation', 'Location');
            fCreateStoryBoard('dropdownTypeNew', 'filterTypeNew', 'Type');

            CheckFilterClearorNotMain();

        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownTypeNew').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');

            }

        }

        function fCreateStoryBoard(Id, div, filtername) {
            debugger;

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filterLocation') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterTypeNew') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();

        }

        function FilterAll() {
            debugger;
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
       
            if (locationlist.length > 0  || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || ($("#dropdownCategory").val() != "" && $("#dropdownCategory").val() != null && $("#dropdownCategory").val() != undefined)
                || ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined)
                ) {

                var finalSelectedfilter = { logic: "and", filters: [] };

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "BranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
           
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActName", operator: "eq", value: $("#dropdownACT").val()
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != undefined) {
                    var FrequencyFilter = { logic: "or", filters: [] };
                    FrequencyFilter.filters.push({
                        field: "Frequency", operator: "Contains", value: $("#dropdownFrequency").val()
                    });
                    finalSelectedfilter.filters.push(FrequencyFilter);
                }
                if ($("#dropdownCategory").val() != "" && $("#dropdownCategory").val() != null && $("#dropdownCategory").val() != undefined) {
                    var CategoryFilter = { logic: "or", filters: [] };
                    CategoryFilter.filters.push({
                        field: "ComplianceCategoryId", operator: "eq", value: $("#dropdownCategory").val()
                    });
                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }
            

        function ClearAllFilterMain(e) {

            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownTypeNew").data("kendoDropDownTree").value([]);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdownCategory").data("kendoDropDownList").select(0);
            $("#dropdownFrequency").data("kendoDropDownList").select(0);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function exportReportAdvanced(e) {
            var ReportName = "";
            var FileName = "";
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "S") {
                ReportName = "Statutory Compliance Assignment Report";
                FileName = "StatutoryComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "C") {
                ReportName = "CheckList Report";
                FileName = "CheckListComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "E") {
                ReportName = "Event Based Report";
                FileName = "EventBasedComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "I") {
                ReportName = "Internal Compliance Assignment Report";
                FileName = "InternalComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="IC") {
                ReportName = "Internal CheckList Compliance Report";
                FileName = "InternalChecklistComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="S,C") {
                ReportName = "Statutory and CheckList Compliance Report";
                FileName = "StatutoryChecklistComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="S,E") {
                ReportName = "Statutory and Eventbased Compliance Report";
                FileName = "StatutoryEventbasedComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="I,IC") {
                ReportName = "Internal Checklist Compliance Report";
                FileName = "InternalChecklistComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="C,E") {
                ReportName = "Checklist and Eventbased Compliance Report";
                FileName = "ChecklistEventbasedComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="S,C,E") {
                ReportName = "Statutory,Checklist and Eventbased Compliance Report";
                FileName = "StatutoryChecklistEventbasedComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="S,C,E,I,IC") {
                ReportName = "All Compliance Report";
                FileName = "AllComplianceReport";
            }
            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values =="") {
                ReportName = "All Compliance Report";
                FileName = "AllComplianceReport";
            }

            //var ReportNameAdd = "Report of " + ReportName + " ";

            var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');

            var grid = $("#grid").getKendoGrid();

            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "S" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "C" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "E"
                || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,E" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C,E"
               || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "C,E" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "S,C,E,I,IC" || $("#dropdownTypeNew").data("kendoDropDownTree")._values =="") {

                var rows = [
                    {
                        cells: [
                            { value: "Customer Name:", bold: true },
                            { value: '<% =CustomerName%>' }
                          ]
                      },
                      {
                          cells: [
                              { value: "Report Name:", bold: true },
                              { value: ReportName }
                          ]
                      },
                      {
                          cells: [
                              { value: "Report Generated On:", bold: true },
                              { value: todayDate }
                          ]
                      },
                      {
                          cells: [
                              { value: "" }
                          ]
                      },
                      {
                          cells: [
                              { value: "S.No.", bold: true },
                              { value: "ComplianceID", bold: true },
                              { value: "Branch", bold: true },
                              { value: "ActName", bold: true },
                              { value: "ShortDescription", bold: true },
                              { value: "Description", bold: true },
                              { value: "ShortForm", bold: true },
                              { value: "Section", bold: true },
                              { value: "Risk", bold: true },
                              { value: "Frequency", bold: true },
                              { value: "ComplianceType", bold: true },
                              { value: "Performer", bold: true },
                              { value: "Reviewer", bold: true },
                              { value: "Approver", bold: true },
                              { value: "State", bold: true },
                              { value: "ComplianceTypeName", bold: true },
                              { value: "Start Date", bold: true },
                              { value: "Label", bold: true },
                              { value: "Department", bold: true },

                          ]
                      }
                  ];

                  var trs = grid.dataSource;
                  var filteredDataSource = new kendo.data.DataSource({
                      data: trs.data(),
                      filter: trs.filter()
                  });

                  filteredDataSource.read();
                  var data = filteredDataSource.view();
                  for (var i = 0; i < data.length; i++) {
                      var dataItem = data[i];
                      rows.push({
                          cells: [ // dataItem."Whatever Your Attributes Are"
                              { value: '' },
                              { value: dataItem.ComplianceID },
                              { value: dataItem.Branch },
                              { value: dataItem.ActName },
                              { value: dataItem.ShortDescription },
                              { value: dataItem.Description },
                              { value: dataItem.ShortForm },
                              { value: dataItem.Section },
                              { value: dataItem.Risk },
                              { value: dataItem.Frequency },
                              { value: dataItem.ComplianceType },
                              { value: dataItem.Performer },
                              { value: dataItem.Reviewer },
                              { value: dataItem.Approver },
                              { value: dataItem.State },
                              { value: dataItem.ComplianceTypeName },
                              { value: dataItem.StartDate },
                              { value: dataItem.SequenceID },
                              { value: dataItem.Department }
                          ]
                      });

                  }
                  for (var i = 4; i < rows.length; i++) {
                      for (var j = 0; j < 19; j++) {
                          rows[i].cells[j].borderBottom = "#000000";
                          rows[i].cells[j].borderLeft = "#000000";
                          rows[i].cells[j].borderRight = "#000000";
                          rows[i].cells[j].borderTop = "#000000";
                          rows[i].cells[j].hAlign = "left";
                          rows[i].cells[j].vAlign = "top";
                          rows[i].cells[j].wrap = true;

                          if (i != 4) {
                              rows[i].cells[0].value = i - 4;
                          }
                          if (i == 4) {
                              rows[4].cells[j].background = "#A9A9A9";
                          }
                      }
                  }

                  excelExport(rows, ReportName);
              }

            if ($("#dropdownTypeNew").data("kendoDropDownTree")._values == "I" || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "IC"
                || $("#dropdownTypeNew").data("kendoDropDownTree")._values == "I,IC") {

                  var rows = [
                      {
                          cells: [
                              { value: "Customer Name:", bold: true },
                              { value: '<% =CustomerName%>' }
                          ]
                      },
                      {
                          cells: [
                              { value: "Report Name:", bold: true },
                              { value: ReportName }
                          ]
                      },
                      {
                          cells: [
                              { value: "Report Generated On:", bold: true },
                              { value: todayDate }
                          ]
                      },
                      {
                          cells: [
                              { value: "" }
                          ]
                      },
                      {
                          cells: [
                              { value: "S.No.", bold: true },
                              { value: "ComplianceID", bold: true },
                              { value: "Branch", bold: true },
                              { value: "ShortDescription", bold: true },
                              { value: "DetailedDescription", bold: true },
                              { value: "Risk", bold: true },
                              { value: "Frequency", bold: true },
                              { value: "ComplianceType", bold: true },
                              { value: "Performer", bold: true },
                              { value: "Reviewer", bold: true },
                              { value: "Approver", bold: true },
                              { value: "ComplianceTypeName", bold: true },
                              { value: "Start Date", bold: true },
                              { value: "Label", bold: true },
                              { value: "Department", bold: true },

                          ]
                      }
                  ];

                  var trs = grid.dataSource;
                  var filteredDataSource = new kendo.data.DataSource({
                      data: trs.data(),
                      filter: trs.filter()
                  });

                  filteredDataSource.read();
                  var data = filteredDataSource.view();
                  for (var i = 0; i < data.length; i++) {
                      var dataItem = data[i];
                      rows.push({
                          cells: [ // dataItem."Whatever Your Attributes Are"
                              { value: '' },
                              { value: dataItem.ComplianceID },
                              { value: dataItem.Branch },
                              { value: dataItem.ShortDescription },
                              { value: dataItem.DetailedDescription },
                              { value: dataItem.Risk },
                              { value: dataItem.Frequency },
                              { value: dataItem.ComplianceType },
                              { value: dataItem.Performer },
                              { value: dataItem.Reviewer },
                              { value: dataItem.Approver },
                              { value: dataItem.ComplianceTypeName },
                              { value: dataItem.StartDate },
                              { value: dataItem.SequenceID },
                              { value: dataItem.Department }
                          ]
                      });

                  }
                  for (var i = 4; i < rows.length; i++) {
                      for (var j = 0; j < 15; j++) {
                          rows[i].cells[j].borderBottom = "#000000";
                          rows[i].cells[j].borderLeft = "#000000";
                          rows[i].cells[j].borderRight = "#000000";
                          rows[i].cells[j].borderTop = "#000000";
                          rows[i].cells[j].hAlign = "left";
                          rows[i].cells[j].vAlign = "top";
                          rows[i].cells[j].wrap = true;

                          if (i != 4) {
                              rows[i].cells[0].value = i - 4;
                          }
                          if (i == 4) {
                              rows[4].cells[j].background = "#A9A9A9";
                          }
                      }
                  }

                  excelExport(rows, ReportName);
              }
              function excelExport(rows, ReportName) {

                  var workbook = new kendo.ooxml.Workbook({
                      sheets: [
                          {
                              columns: [
                                  { autoWidth: true },
                                  { autoWidth: true },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 250 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 300 },
                                  { width: 300 },
                                  { width: 250 },
                                  { width: 150 },
                                  { width: 150 },
                                  { width: 250 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 200 },
                                  { width: 200 }

                              ],
                              title: FileName,
                              rows: rows
                          },
                      ]
                  });

                  var nameOfPage = FileName;
                  //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
                  kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

              }

              e.preventDefault();
          }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="example">
        
            <div class="row">
                <div class="row">
                   <%-- <input id="dropdownType" data-placeholder="Status" style="width:147px;"/>--%>
                    <input id="dropdownTypeNew" data-placeholder="Status" style="width:210px;"/>
                    <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 244px;"/> 
                    <input id="dropdownACT" data-placeholder="Act" style="width: 214px;" />
                    <input id="dropdownCategory" data-placeholder="Category" style="width: 147px;" />
                    <input id="dropdownFrequency" data-placeholder="Frequency" style="width: 147px;" />
                   <button id="exportAdvanced" onclick="exportReportAdvanced(event)" data-toggle="tooltip" title="Export to Excel" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>        
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>
            <div class="clearfix" style="height: 10px;"></div>
            <div class="row" style="padding-bottom: 0px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterLocation">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterTypeNew">&nbsp;</div>
            <div id="grid" style="border: none;"></div>
        </div>
</asp:Content>
