﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VendorAudit.Master" AutoEventWireup="true" CodeBehind="AuditMyReportNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.AuditMyReportNew" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">                               
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        // For Enter Only Number Only.
      
      

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" CssClass="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblMsg1" CssClass="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                        </div>
                        <section class="panel">
                           <header class="panel-heading tab-bg-primary ">                              
                            </header>
                            <div class="clearfix"></div>
                            <div class="panel-body">
                            <div class="col-md-12 colpadding0">

                                <div class="col-md-10 colpadding0" style="width:68%;">
                                    
                                    <div class="col-md-2 colpadding0" style="width:16%;">
                                        <div class="col-md-4 colpadding0" >
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 65px;margin-right:10px; float: left" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" Selected="True"/>
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>                                        
                                    </div>
                                    <div class="col-md-2 colpadding0"> 
                                         <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15" Style="width: 100px;margin-right:10px; float: right" 
                                            AutoPostBack="true">
                                            <asp:ListItem Text="All" Value="-1" Selected="True"/>
                                            <asp:ListItem Text="Open" value="0"/>
                                            <asp:ListItem Text="Closed" value="1"/>                                        
                                        </asp:DropDownList>  
                                    </div>
                                    <div class="col-md-2 colpadding0"> 
                                     <asp:DropDownList runat="server" ID="ddlVendor" Style="width: 100px;margin-right:10px; float: right" 
                                            class="form-control m-bot15" AutoPostBack="true" />
                                    
                                        </div>
                                    <div class="col-md-2 colpadding0">    
                                       <asp:DropDownList runat="server" ID="ddlAudit" Style="width: 100px;margin-right:10px; float: right" 
                                            class="form-control m-bot15" AutoPostBack="true" />
                                    </div>
                                    <div class="col-md-4 colpadding0"> 
                                            <div style="float:left;margin-right: 1%;">
                                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 373px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                CssClass="txtbox" />                                                       
                                                <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                    <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                    Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>  
                                            </div>           
                                        </div>
                               
                                </div>
                                   <div class="col-md-2 colpadding0" style="text-align: right; float: right;width: 19%;">                                                                    
                                        <div class="col-md-6 colpadding0" style="margin-left: 14px;width: 41%;">               
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">   
                                             <asp:LinkButton runat="server" ID="btnExcelReport" OnClick="btnExcelReport_Click" CssClass="btn btn-primary"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Export Report">
                                                Export Report</asp:LinkButton>
                                        </div>
                                 
                                </div>  
                             
                               
                                <!-- Advance Search scrum-->
                                 <div class="clearfix"></div>
                            <div class="col-md-12 AdvanceSearchScrum">
                                 
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                            </div>
                            </div>
                            </div>
                        <div class="clearfix"></div>                                                                      
                        
                        <div id="ReviewerGrids" runat="server">                               
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    CellPadding="4" Width="100%" 
                                     AllowPaging="True" PageSize="10" AutoPostBack="true">
                                    <Columns>                                        
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>     
                                      
                                    
                                        
                                             <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 160px;">
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Eval("LocationName") %>' ToolTip='<%# Eval("LocationName") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Vendor Name">
                                        <ItemTemplate>
                                            <div style="overflow: hidden;text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblActName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Audit Name">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblScheduledOn" runat="server" ToolTip='<%# Eval("AuditName") %>' Text='<%# Eval("AuditName") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Audit Start Date">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                                <asp:Label ID="AuditStartDatelbl" runat="server" ToolTip=' <%# Convert.ToDateTime(Eval("AuditStartDate")).ToString("dd-MMM-yyyy") %>' Text=' <%# Convert.ToDateTime(Eval("AuditStartDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Audit End Date">
                                            <ItemTemplate>                                             
                                                  <div style="overflow: hidden; text-align:left; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                                <asp:Label ID="AuditEndDatelbl" runat="server" ToolTip=' <%# Convert.ToDateTime(Eval("AuditEndDate")).ToString("dd-MMM-yyyy") %>' Text=' <%# Convert.ToDateTime(Eval("AuditEndDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>   
                                                      </div>                                        
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                       <%-- <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                              <asp:Button ID="btnAdd" Width="72px" ValidationGroup="ComplianceInstanceValidationGroup" Text="Save" runat="server" OnClientClick="return ValidateDaysAndIntermDays(this);" OnClick="btnAdd_Click" CssClass="btn btn-primary" />                                            
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>                               
                            </div>                                                     
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                     
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcelReport" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Report');
            setactivemenu('leftescalationmenu');
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
