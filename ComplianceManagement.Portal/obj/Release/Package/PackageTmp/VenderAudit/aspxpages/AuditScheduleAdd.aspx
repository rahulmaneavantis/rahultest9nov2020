﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="AuditScheduleAdd.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages.AuditScheduleAdd" %>


<%-- <%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %> --%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>


    <script type="text/javascript" src="../../Newjs/fullcalendar.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>

<%--    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>--%>


    <!-- custom select -->
    <%--  <script type="text/javascript" src="../../Newjs/jquery.customSelect.min.js"></script>--%>
    <script type="text/javascript" language="javascript">

        $(function () {
            initializeCombobox();
        });
        //$(document).ready(function () {
        //    initializeCombobox();
        //});
        //custom select box
        //$(function () {
        //    $('select.styled').customSelect();
        //});


        function initializeCombobox() {
            $("#<%= ddlVendor.ClientID %>").combobox();
            $("#<%= ddlAuditor.ClientID %>").combobox();

        }


    </script>

    <script type="text/javascript">
        $(function () {
            $('#divAuditScheduleDialog').dialog({
                height: 550,
                width: 788,
                autoOpen: false,
                draggable: true,
                title: "Edit Audit Schedule",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <style type="text/css">
        .td1 {
            width: 15%;
        }

        .td2 {
            width: 20%;
        }

        .td3 {
            width: 15%;
        }

        .td4 {
            width: 20%;
        }
    </style>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <%-- <style type="text/css">
        .dd_chk_select {
            height: 81px !important;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            height: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>--%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional" OnLoad="upEventList_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                </div>
                <div style="float: right; margin-top: 10px; margin-right: 15px;">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAdd" OnClick="btnAdd_Click" />
                </div>
                <table runat="server" width="90%">
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Vendor Name</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlVendor" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Vendor." ControlToValidate="ddlVendor"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Location</label>
                        </td>
                        <td class="td4">
                            <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divBranches">
                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" Height="200px" Width="394px"
                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                            <asp:CompareValidator ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Auditor Name</label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList ID="ddlAuditor" runat="server" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                CssClass="txtbox">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Auditor." ControlToValidate="ddlAuditor"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Start Date</label>
                        </td>
                        <td class="td4">
                            <asp:TextBox runat="server" ID="tbxStartDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Start Date can not be empty."
                                ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td1">
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Audit Name</label>
                        </td>
                        <td class="td2">
                            <asp:TextBox runat="server" ID="txtAuditName" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Audit Name can not be empty."
                                ControlToValidate="txtAuditName" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                        <td class="td3">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                End Date</label>
                        </td>
                        <td class="td4">
                            <asp:TextBox runat="server" ID="tbxEndDate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="End Date can not be empty."
                                ControlToValidate="tbxEndDate" runat="server" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                    </tr>
                </table>

                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 7px">
                            <asp:Panel ID="Panel1" Width="100%" Height="300px" ScrollBars="Vertical" runat="server">
                                <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" GridLines="Vertical"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                    CellPadding="4" ForeColor="Black" AllowPaging="false" PageSize="10" Width="100%"
                                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdChecklist_RowCommand">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkChecklistheader" Text="Select" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkChecklist" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ID" />
                                        <asp:TemplateField HeaderText="Checklist Name" SortExpression="Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                    <asp:Label ID="lblCheckListName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    <asp:Label ID="lblCheckListID" runat="server" Visible="false" Text='<%# Eval("ID")%>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtEdit" runat="server" OnClick="btnEditAuditSchedule_Click" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon.png" alt="Edit Audit" title="Edit Audit" /></asp:LinkButton>
                                                <%--                               <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Event?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete Event" /></asp:LinkButton>--%>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                    <PagerSettings Position="Top" />
                                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div style="margin-bottom: 7px; float: right; margin-right: 467px; margin-top: 10px; clear: both">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                        ValidationGroup="ComplianceInstanceValidationGroup" />
                    <asp:Button Text="Back" runat="server" ID="btnClose" CssClass="button" OnClick="btnClose_Click" />
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>




    <div id="divAuditScheduleDialog">
        <asp:UpdatePanel ID="upchecklist" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="Label3" Style="color: Red"></asp:Label>
                    </div>

                    <center>
                        <table runat="server" width="80%">
                            <tr>
                                <td width="30%">Audit Name</td>
                                <td>
                                    <asp:Label ID="lblAuditName" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td width="30%">Type</td>
                                <td>
                                    <asp:Label ID="lblType" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td width="30%">Checklist Name</td>
                                <td>
                                    <asp:Label ID="lblChecklistName" runat="server"></asp:Label></td>
                            </tr>


                        </table>
                        <br />
                        <br />

                        <%--  <div style="display:none;">
                            <asp:DropDownCheckBoxes ID="ddlLocationListNew1" runat="server" 
                                                UseButtons="True" SelectionMode="Multiple" Width="300px">
                                                <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>

                                            </asp:DropDownCheckBoxes>
                        </div>--%>
                        <%--<div style ="height:200px;">

                        </div>--%>
                        <div style="margin-bottom: 7px;">

                            <asp:GridView runat="server" ID="GrdAuditschedule" AutoGenerateColumns="false" GridLines="Vertical"
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                                CellPadding="4" ForeColor="Black" AllowPaging="false" PageSize="10" Width="100%" DataKeyName="ID"
                                OnRowCancelingEdit="GrdAuditschedule_RowCancelingEdit"
                                OnRowEditing="GrdAuditschedule_RowEditing"
                                OnRowDataBound="GrdAuditschedule_RowDataBound" OnRowCommand="GrdAuditschedule_RowCommand" Font-Size="12px">

                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcomplianceID" runat="server" Text='<%# Eval("ComplianceID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Short Description" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                <asp:Label ID="lbllocation" Text='<%# SelectedBranchName(Convert.ToInt64(Eval("AuditID")),Convert.ToInt64(Eval("ChecklistID")),Convert.ToInt64(Eval("ComplianceID"))) %>' ToolTip='<%# SelectedBranchName(Convert.ToInt64(Eval("AuditID")),Convert.ToInt64(Eval("ChecklistID")),Convert.ToInt64(Eval("ComplianceID"))) %>' runat="server"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <%--  <asp:DropDownCheckBoxes ID="ddlLocationListNew" runat="server" 
                                                UseButtons="True" SelectionMode="Multiple" Width="300px">
                                                <Style SelectBoxWidth="390" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>

                                            </asp:DropDownCheckBoxes>--%>
                                            <%--    <asp:DropDownList ID="ddlLocationListNew" Width="300px" runat="server">
                                                    </asp:DropDownList>--%>
                                            <asp:ListBox ID="ddlLocationListNew" runat="server" SelectionMode="Multiple" Width="250px"></asp:ListBox>

                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ShowHeader="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lbtSave" runat="server" Text="Save" CommandName="Save_COMPLIANCE" CommandArgument='<%# Eval("ComplianceID") %>'></asp:LinkButton>
                                            <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                <PagerSettings Position="Top" />
                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>


                    </center>

                </div>


            </ContentTemplate>
        </asp:UpdatePanel>
    </div>








    <script type="text/javascript" language="javascript">

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });
        }


        function initializeConfirmDatePicker1(date) {
            var startDate = new Date();
            $('#<%= tbxEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });

        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function SelectheaderCheckboxes(headerchk) {
            var gvcheck = document.getElementById("<%=grdChecklist.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].disabled == false)
                        gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }
    </script>


    <%--<script type="text/javascript">
        $(function () {
            $('[id*=ddlLocationListNew]').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>--%>


</asp:Content>


