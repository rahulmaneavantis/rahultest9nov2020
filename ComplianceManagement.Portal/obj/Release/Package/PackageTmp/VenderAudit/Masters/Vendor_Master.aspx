﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="Vendor_Master.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.Masters.Vendor_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#divVendorDialog').dialog({
                height: 300,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Add Vendor",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        $(document).ready(function () {
            if (document.getElementById('BodyContent_saveopo').value == "true") {

                $('#divVendorDialog').dialog({
                    height: 300,
                    width: 600,
                    autoOpen: false,
                    draggable: true,
                    title: "Add Vendor",
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    }
                });
                newfun();
            }
        });
        function newfun() {

            $("#divVendorDialog").dialog('open');

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upVendorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <table width="100%">
                <tr>
                    <td align="right"></td>
                    <td align="right" style="width: 20%"></td>
                    <td align="right" style="width: 25%">Filter :
                       <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                           OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddVendor" OnClick="btnAddVendor_Click" Visible="false" />
                    </td>
                </tr>
            </table>



            <div class="col-md-12 colpadding0">

                <asp:GridView runat="server" ID="grdComplienceVender" AutoGenerateColumns="false" GridLines="Vertical"
                    OnRowDataBound="grdComplienceVender_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                    OnRowCreated="grdComplienceVender_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%"
                    OnSorting="grdComplienceVender_Sorting"
                    OnPageIndexChanging="grdComplienceVender_PageIndexChanging"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdComplienceVender_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Vendor Name" ItemStyle-Width="80%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />

                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Vendor" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon.png" alt="Edit Vendor" title="Edit Vendor" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Vendor" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this Vendor?');"><img src="../../Images/delete_icon.png" alt="Delete Vendor" title="Delete Vendor" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div id="divVendorDialog">
        <asp:UpdatePanel ID="upVendor" runat="server" UpdateMode="Conditional" OnLoad="upVendor_Load">
            <ContentTemplate>
                <div>
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="PromotorValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="PromotorValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; align-content: center">
                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                        <label style="width: 120px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                            Vendor</label>
                        <asp:TextBox runat="server" ID="tbxVendor" CssClass="form-control" Style="width: 400px; height: 30px;" TextMode="MultiLine" MaxLength="500" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter vendor name" ControlToValidate="tbxVendor"
                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 7px; text-align: center; margin-top: 10px">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="PromotorValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" data-dismiss="modal" OnClientClick="$('#divVendorDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                    </div>
                    <div class="clearfix" style="height: 50px">
                    </div>
                     <asp:HiddenField ID="saveopo" runat="server" Value="false" />
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>


</asp:Content>
