﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceOverdueSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ComplianceOverdueSummary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

      <style type="text/css">
    .k-grid-content {
            max-height: 100Px !important;
        }

            .k-grid-content k-auto-scrollable {
                height: auto;
            }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1em;
            border-bottom-width: 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
            display: none;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            display: none;
            margin-top: -3px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-edge .k-pager-info, .k-ff .k-pager-info, .k-ie11 .k-pager-info, .k-safari .k-pager-info, .k-webkit .k-pager-info {
            display: none;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /*border-width: 0 0 0px 0px;*/
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        td.k-command-cell {
            border-width: 0px 1px 1px 1px;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .change-condition {
            color: blue;
        }

    </style>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
         
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },
                dataSource: {
                    transport: {
                     read: {
                            url: '<% =Path%>Data/GetMGMTOverdueComplianceSummary?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=All&FY=0&Isdept=0&IsApprover=<% =isapprover%>&RiskId=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetMGMTOverdueComplianceSummary?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=All&FY=0&Isdept=0&IsApprover=<% =isapprover%>&RiskId=0'
                    },
                    schema: {
                        data: function (response) {
                             

                            return response[0].SList;
                        },
                        total: function (response) {
                            return response[0].SList.length;
                        }
                    },
                    pageSize: 2
                },

                toolbar: kendo.template($("#template").html()),
                height: 180,
                sortable: false,
                // groupable: true,
                filterable: false,
                columnMenu: false,
                //pageable: true,
                reorderable: false,
                resizable: false,
                multi: true,
                selectable: false,
                columns: [
                    {
                        field: "ShortDescription", title: 'Compliance Items',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {
                         field: "Performer", title: 'Responsibility',
                         width: "15%;",
                         attributes: {
                             style: 'white-space: nowrap;'

                         }, filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                    {
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        width:"10%",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        width:"10%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "edit", text: "  ", title: "Action", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",

                            }
                        ], title: "Action", width: "7%;", headerAttributes: {
                            style: "border-right: solid 1px #ceced2;"
                        }
                    }
                ]
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                parent.OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, 'Statutory');
                return true;
            });

            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
        });

     
           


    </script>

    <title></title>

</head>
<body style="overflow-x: hidden;">
    <form>

        <div id="example">
            <div id="grid" style="border: none;"></div>
        </div>
    </form>
</body>
</html>

