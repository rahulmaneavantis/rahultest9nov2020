﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceDetailsAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ComplianceDetailsAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
  
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
       
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="../Scripts/KendoPage/ComplianceDetail.js"></script>
    
    <style type="text/css">

        span.k-icon.k-i-window-minimize {
    display: none;
}

         .k-grid-content
        {
            min-height:420px !important;
              overflow: hidden  !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }
        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }    
        
        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }
            
        .k-grouping-header 
        {
            border-right: 0px solid;
            border-left: 0px solid;
        }

        .k-grid td {
            line-height: 2.0em;
        }
        .k-i-more-vertical:before {
            content: "\e006";
        }
        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }
        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }
        #grid .k-grid-toolbar {
            background: white;
        }


     /*.k-pager-wrap > .k-link > .k-icon {
             margin-top: 5px; 
            color: inherit;
        }*/
        .k-grid-toolbar:first-child, .k-grouping-header + .k-grid-toolbar {
            border-width: 0 0 0px;
            padding: 0px;
        }

        li.k-button
        {
            margin-top: 2px;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: auto;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
            width: 10%;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
            margin-top: -1px;
            margin-left: -1px;
            margin-right: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-grid-header-wrap 
        {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0px 0px 0 0;
            zoom: 1;
        }

        .k-grid table 
        {
            width: 100%;
            max-width: none;
            border-collapse: separate;
            empty-cells: show;
            margin: -1px;
            border-spacing: 0px;
            border-width: 0px;
            outline: 0px;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
          .change-condition {
            color: blue;
        }
        span.k-icon.k-i-arrow-60-down {
            margin-top: -12px;
        }
    </style>
   
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
      
    <script type="text/x-kendo-template" id="template">      
               
    </script>
    
    <script type="text/javascript">

        $(window).resize(function(){
           window.parent.forchild($("body").height()+50);  
        });

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
        var perorrev = "";
        function BindGrid()
        {
            var grid = $("#grid").data("kendoGrid");
            if (grid != undefined || grid != null) {
                $("#grid").empty();
            }
            if ($("#dropdownlistUserRole").val() == "3") {

                perorrev = "Performer";

            }
            else if ($("#dropdownlistUserRole").val() == "4") {
                perorrev = "Reviewer";
            }
            else {
                perorrev = "Performer/Reviewer";
            }

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {                        
                        read: {
                             url: '<% =Path%>Data/DashboradComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&bid=<% =BID%>&catid=<% =catid%>&StatusFlag=<% =StatusFlagID%>&IsApprover=<% =isapprover%>&Isdept=<% =IsDeptHead%>',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                        //read: '<% =Path%>Data/DashboradComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&bid=<% =BID%>&catid=<% =catid%>&StatusFlag=<% =StatusFlagID%>&IsApprover=<% =isapprover%>&Isdept=<% =IsDeptHead%>'
                    },
                    schema: {
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Statustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].DeptStatustory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 3) {
                                return response[0].DeptInternal.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                toolbar: kendo.template($("#template").html()),
                height: 488,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                //columnMenu: true,
                //columnMenuInit:function(e){
                //    debugger;
                //    var menu = e.container.find(".k-menu").data("kendoMenu");
                //    var field = e.field;
                //    alert(field);
                //    if(field == "Name"){                        
                //        e.container.find(".k-filter-menu .k-popup").css({"max-width": "60%", });
                //        e.container.find(".k-multicheck-wrap").css("overflow-x","auto");
                //    }
                //},
                columnMenu: true,
                filterMenuInit: function(e){      
                    debugger;
                    if(e.field == "Name"){                        
                        e.container.css({"max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x","auto");
                    }else if( e.field == "ShortDescription"){
                        e.container.css({"max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x","auto");
                    }else if( e.field == "Section"){
                        e.container.css({"max-width": "60%", });
                        e.container.find(".k-multicheck-wrap").css("overflow-x","auto");
                    }
                },
                columns: [
                    {
                        field: "Branch", title: 'Location',
                        width: "17%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <% if (StatusFlagID== -1) {%>   {
                        field: "Name", title: 'Act',
                        width: "34%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                   
                    {
                        field: "Section", title: 'Section',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }, 
                    <%}%>
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "User", title: perorrev,
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ]
            });
            window.parent.forchild($("body").height());  
        }
        
        $(document).ready(function () {

            BindGrid();

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterGrid();
                },
                index: 0,
                dataSource: [
                    { text: "Role", value: "-1" },
                    { text: "Performer", value: "3" },
                    { text: "Reviewer", value: "4" }
                ]
            });
             <% if (StatusFlagID== -1) {%>  
             $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select category",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,
                   
                    transport: {   
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"                       
                    }
                }
             });
            <%} else {%> 
             $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select category",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,
                   
                    transport: {  
                    read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"                       
                    }
                }
             });
            <%}%>
            //
             $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                 change: function (e) { 
                     FilterGrid();
                     fCreateStoryBoard('dropdownACT', 'filterAct', 'act')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
                    }
                },dataBound: function(e) {
                    e.sender.list.width("1000");
                }
            });

            $("#dropdownFrequency").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Name",
                optionLabel: "Select Frequency",
                 change: function (e) { 
                     FilterGrid();
                     fCreateStoryBoard('dropdownFrequency', 'filterCompType', 'frequency')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                     read: {
                            url: '<% =Path%>Data/BindFrequency',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindFrequency"
                    }
                },dataBound: function(e) {
                    e.sender.list.width("200");
                }
            });
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {    
                    FilterGrid();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');window.parent.forchild($("body").height());  
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            
          
            //function ClearAllFilterMain(e) {
            //    alert(1);
            //    debugger;
            //    $("#dropdowntree").data("kendoDropDownTree").value([]);
            //    $("#dropdownlistUserRole").data("kendoDropDownTree").value([]);
            //    $('#ClearfilterMain').css('display', 'none');
            //    $("#grid").data("kendoGrid").dataSource.filter({});
            //    e.preventDefault();
            //}
        });
        
        function exportReport(e) {            
            e.preventDefault();
            debugger;
            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var CustomerName = document.getElementById('CustName').value;

            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Role Details
            var Roledetails = [];
            if ($("#dropdownlistUserRole").val() != "" && $("#dropdownlistUserRole").val() != null && $("#dropdownlistUserRole").val() != "-1" && $("#dropdownlistUserRole").val() != undefined) {
                Roledetails.push(parseInt($("#dropdownlistUserRole").val()));
            }

            //function details
            var functiondetails = [];
            if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != "-1" && $("#dropdownfunction").val() != undefined) {
                functiondetails.push(parseInt($("#dropdownfunction").val()));
            }

            ////Act Details
            //var Actdetails = [];
            //var list3 = $("#dropdownACT").data("kendoDropDownTree")._values;
            //$.each(list3, function (i, v) {
            //    Actdetails.push(v);
            //});

            //Act Details
            var Actdetails = [];
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != "-1" && $("#dropdownACT").val() != undefined) {
                Actdetails.push(parseInt($("#dropdownACT").val()));
            }


            //Frequency Details
            var Frequencydetails = [];
            if ($("#dropdownFrequency").val() != "" && $("#dropdownFrequency").val() != null && $("#dropdownFrequency").val() != "-1" && $("#dropdownFrequency").val() != undefined) {
                Frequencydetails.push($("#dropdownFrequency").val());
            }
            var sflag=<% =StatusFlagID%>;
            var did=<% =IsDeptHead%>;
            var isapp =<% =isapprover%>;
           
            $.ajax({
                type: "GET",
                url: '' + PathName + '//ExportReport/ReportMgmt',
                data: {
                    StatusFlag: sflag,Isdept : did,isapprover : isapp,CName : CustomerName, UserId: UId, CustomerID: customerId,
                    location: JSON.stringify(locationsdetails),

                    Role: JSON.stringify(Roledetails), Function: JSON.stringify(functiondetails),
                    actDetail: JSON.stringify(Actdetails),
                    FrequencyDetail: JSON.stringify(Frequencydetails),
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }
    </script>
  </head>
    <body style="overflow-x:hidden;">
  <form>
      <div class="col-lg-5 col-md-5 colpadding0">
                                    <h1 id="pagetype" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;color: #666;margin-bottom: 12px;font-size: 19px;padding-left:5px;padding-top:5px;font-weight:bold;" >Compliances</h1>
                                </div>

    <div id="example">
            <div class=row style="padding-bottom: 37px;">
                        <div class="toolbar"> 
                            <div class="row">
                            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:242px;" />                                           
                            <input id="dropdownlistUserRole" data-placeholder="Role" /> 
                            <input id="dropdownfunction" style="width:223px;" />                                 
                                <% if (StatusFlagID== -1) {%>                                  
                                    <input id="dropdownACT" style="width:242px;" />   
                                     <%}%>   
                                <input id="IsDeptId" type="hidden" value="<% =IsDeptHead%>" />      
                                 <input id="IsSIId" type="hidden" value="<% =StatusFlagID%>" />    
                                 <input id="dropdownFrequency" style="width:181px;" />
                                 <button id="export" onclick="exportReport(event)" data-toggle="tooltip" title="Export to Excel" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>        
                        </div>               
                    </div>
                </div> 
            
            
            
            <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 13.6%;">
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 10px;">                           
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">                           
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="padding-left: 435px;">                             
<%--                             <button id="ClearfilterMain" style="float: right; height: 28px; margin-left: 1%;display:none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>                                                                      --%>
                            <%--<button id="ClearfilterMain" style="" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>--%>                                                                      
                            <button id="ClearfilterMain" style="float: right;margin-top: -37px;margin-right: 2px; margin-left: 1%; height: 30px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                        </div>
                    </div>
                </div>                           
       
            <div class=row style="padding-bottom: 7px;font-size: 12px;display:none;font-weight:bold;" Id="filtersstoryboard">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtertype">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterrole">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterstatus">&nbsp;</div>
                
        <div id="grid" ></div>
        
            <input id="CustName" type="hidden" value="<% =CustomerName%>">
            <input id="UId" type="hidden" value="<% =UId%>">
            <input id="Path" type="hidden" value="<% =Path%>">
           <input id="CustomerId" type="hidden" value="<% =CustId%>">
      
               
    </div>  
                <script type="text/javascript">
          function fhead(Compliances)
          { 
              $('#pagetype').html(val);
              //  $('#sppagetype').html(val)   
          }
      </script>       
  </form>
        </body>
 </html>

