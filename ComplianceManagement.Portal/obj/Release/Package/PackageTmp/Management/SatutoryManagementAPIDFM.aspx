﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SatutoryManagementAPIDFM.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.SatutoryManagementAPIDFM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min1.1.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <script src="../Scripts/KendoPage/StatutoryMGMT_v1.js"></script>
    <title></title>

    <style type="text/css">
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 2px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
    </style>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        $(window).resize(function () {
            window.parent.forchild($("body").height()+70);
        });
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function BindGrid() {

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(12)').remove();
                },
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetDFMStatutoryManagementDashboardPages?Uid=<% =UId%>&ptname=<%=pointname%>&attr=<%=attribute%>&distributorid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=<% =functionid%>&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&Status=All&rolecode=<% =RCode%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            <%--beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },--%>
                        }
                    //     GetDFMStatutoryManagementDashboardPages(int Uid, string ptname,
                    //string attr, int distributorid, int bid, string FDte, string EDte, string Fltr, int fid,
                    //string ChartName, string INTorSAT, string lstcid, string Status, string MonthId, string FY,string rolecode)
                    },
                    schema: {
                        data: function (response) {

                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart;
                            }
                                                  
                        },
                        total: function (response) {

                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart.length;
                            }                                                        
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                columns: [
                    { hidden: true, field: "ShortForm", title: "Short&nbsp;Form", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Performer", title: "Performer", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Reviewer", title: "Reviewer", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "17%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act&nbsp;Name',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%",
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%",
                    },
                    {
                        field: "Status", title: 'Status', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%",
                    },
                    {
                        command: [
                            {
                                name: "edit5", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit4", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            { name: "edit2", text: "  ", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", width: "150px", headerAttributes: {
                            style: "border-right: solid 1px #ceced2;text-align: center;"
                        }
                    }
                ]
            });

    $("#grid").kendoTooltip({
        filter: ".k-grid-edit5",
        content: function (e) {
            return "View";
        }
    });
    $("#grid").kendoTooltip({
        filter: ".k-grid-edit4",
        content: function (e) {
            return "Download";
        }
    });
    $("#grid").kendoTooltip({
        filter: ".k-grid-edit2",
        content: function (e) {
            return "Overview";
        }
    });

    $("#grid").kendoTooltip({
        filter: "th",
        content: function (e) {
            var target = e.target; // element for which the tooltip is shown 
            return $(target).text();
        }
    });
    $("#grid").kendoTooltip({
        filter: "td", //this filter selects the second column's cells
        position: "down",
        content: function (e) {
            var content = e.target.context.textContent;
            if (content != "" && content != "  ") {
                return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
            }
            else
                e.preventDefault();
        }
    }).data("kendoTooltip");


    $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
        return true;
    });

    $(document).on("click", "#grid tbody tr .ob-download", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        $('#downloadfile').attr('src', "../ComplianceDocument/DownloadMGMTDocAPI.aspx?ComplianceScheduleID=" + item.ScheduledOnID + "&IsFlag=-1");
        return true;
    });

    $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        OpenDocumentOverviewpup(item.ScheduledOnID, item.ID)
        return true;
    });
    window.parent.forchild($("body").height());
}

function OpenDocumentOverviewpup(scheduledonid, transactionId) {
    $('#divOverView').modal('show');
    $('#OverViews').attr('width', '1150px');
    $('#OverViews').attr('height', '600px');
    $('.modal-dialog').css('width', '1200px');
    $('#OverViews').attr('src', "../Common/DocumentOverviewAPI.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionId + "&ISStatutoryflag=-1");
}

function onChangeSD() {

}
function onChangeLD() {

}

function BindGridApply(e) {
    var setStartDate = $("#Startdatepicker").val();
    var setEndDate = $("#Lastdatepicker").val();


    BindGrid();
    //Added by Mrunali on 24 jan 2020
    FilterGridDemo();

    if (setStartDate != null) {
        $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
    }
    if (setEndDate != null) {
        $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
    }

    //comment by rahul on 24 jan 2020
    //FilterGrid();

    e.preventDefault();
}
function FilterGridDemo() {


    var Riskdetails = [];
    if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
        Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    }

    //status Details
    var Statusdetails = [];
    if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
        var Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;

        if (Statusdetails.length >= 0) {
            debugger;
            for (var i = 0; i < Statusdetails.length; i++) {
                if (Statusdetails[i] === "No" || Statusdetails[i] === "" || Statusdetails[i] === "-1") {
                    Statusdetails.splice(i, 1);
                }
            }
        }
    }


    //user Details      
    var Userdetails = [];
    if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
        Userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
    }


    //location details
    var locationsdetails = [];
    if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
        locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
    }

    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
        });
    }

    var Departmentdetails = -1;
    if (DisplayDepartment.value > 0) {
        Departmentdetails = DisplayDepartment.value;
    }


    var finalSelectedfilter = { logic: "and", filters: [] };

    if (Riskdetails.length > 0 || Statusdetails.length > 0 || locationsdetails.length > 0 ||
        Userdetails.length > 0 || datedetails.length > 0 ||
        ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) ||
        ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) ||
        ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") ||
        Departmentdetails > 0) {
        if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != "" && $("#dropdownSequence").val() != null) {

            var SeqFilter = { logic: "or", filters: [] };

            SeqFilter.filters.push({
                field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
            });

            finalSelectedfilter.filters.push(SeqFilter);
        }

        if (Departmentdetails > 0) {

            var FunctionDepartmentFilter = { logic: "or", filters: [] };

            FunctionDepartmentFilter.filters.push({
                field: "DepartmentID", operator: "eq", value: parseInt(Departmentdetails)
            });

            finalSelectedfilter.filters.push(FunctionDepartmentFilter);
        }
        if (datedetails.length > 0) {
            var DateFilter = { logic: "or", filters: [] };

            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            finalSelectedfilter.filters.push(DateFilter);
        }
        if (Userdetails.length > 0) {
            var UserFilter = { logic: "or", filters: [] };

            $.each(Userdetails, function (i, v) {
                UserFilter.filters.push({
                    field: "UserID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(UserFilter);
        }

        if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
            var ActFilter = { logic: "or", filters: [] };
            debugger;
            ActFilter.filters.push({
                field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
            });
            finalSelectedfilter.filters.push(ActFilter);
        }
        if (locationsdetails.length > 0) {
            var LocationFilter = { logic: "or", filters: [] };
            debugger;
            $.each(locationsdetails, function (i, v) {
                LocationFilter.filters.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });
            finalSelectedfilter.filters.push(LocationFilter);
        }
        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
            if ($("#dropdownfunction").val() != 0) {
                var FunctionFilter = { logic: "or", filters: [] };
                FunctionFilter.filters.push({
                    field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                });
                finalSelectedfilter.filters.push(FunctionFilter);
            }
        }
        if (Statusdetails.length > 0) {
            var StatusFilter = { logic: "or", filters: [] };
            debugger;         
            $.each(Statusdetails, function (i, v) {
                //if (v !="" && v !="-1" ) {                    
                    StatusFilter.filters.push({                  
                        field: "FilterStatus", operator: "eq", value: v
                    });
                //}
            });
            finalSelectedfilter.filters.push(StatusFilter);
        }
        if (Riskdetails.length > 0) {
            var CategoryFilter = { logic: "or", filters: [] };

            $.each(Riskdetails, function (i, v) {

                CategoryFilter.filters.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });
            finalSelectedfilter.filters.push(CategoryFilter);
        }
        if (finalSelectedfilter.filters.length > 0) {
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.filter(finalSelectedfilter);
        }
        else {
            $("#grid").data("kendoGrid").dataSource.filter({});
        }
    }
    else {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }


}
$(document).ready(function () {

    $("#Startdatepicker").kendoDatePicker({
        format:"dd-MM-yyyy",
        change: onChangeSD
    });
    $("#Lastdatepicker").kendoDatePicker({
        change: onChangeLD,
        format: "dd-MM-yyyy",
    });

    $("#dropdownfunction").kendoDropDownList({
        filter: "startswith",
        autoClose: false,
        autoWidth: true,
        dataTextField: "Name",
        dataValueField: "Id",
        optionLabel: "Select Category",
        change: function (e) {
            BindGrid();
            FilterGridDemo();
            fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
        },
        dataSource: {
            severFiltering: true,
            transport: {
                read: {
                    url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                        }
                        //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT"
                    }
                }
            });

            $("#dropdownfunction").data("kendoDropDownList").value('<%=functionid%>');

            $("#dropdownfunction").val('<%=functionid%>');



            BindGrid();


            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                        fCreateStoryBoard('dropdownFY', 'filterFY', 'FY');
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                ]
            });


            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                    window.parent.forchild($("body").height() + 15);
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });
            $("#dropdownlistRisk").data("kendoDropDownTree").value('<%= riskid%>');
            //$("#dropdownlistRisk").val('<%= riskid%>');


            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
                },
                dataSource: [
                    <%--    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "6" },
                    <%}%>
                    { text: "Closed Timely", value: "4" },
                    { text: "Closed Delayed", value: "5" }--%>

                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "NotComplied" },
                    <%}%>

                   <%--  <%if (IsCompliedButDocumentPending == true)%>
                    <%{%>
                    { text: "Complied But Document Pending", value: "Complied But Document Pending" },
                    <%}%>--%>

                    { text: "Closed Timely", value: "ClosedTimely" },
                    { text: "Closed Delayed", value: "ClosedDelayed" },
                    { text: "In Progress", value: "InProgress" }
        ]
    });

            $("#dropdownlistStatus").data("kendoDropDownTree").value(["<%=StatusID%>", "<%=StatusID1%>", "<%=StatusID2%>", "<%=StatusID3%>"]);


            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });



            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                }
                //read: "<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>"
            }
        }
    });


            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: true,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                }
                //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
            }
        }
    });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownUser', 'filterUser1', 'user');
                    window.parent.forchild($("body").height() + 15);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                }
                //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>"
            },
        }
    });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    window.parent.forchild($("body").height() + 15);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =RCode%>&IsStatutoryInternal=S',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                }
                //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S"
            },
            schema: {
                data: function (response) {
                    return response[0].locationList;
                },
                model: {
                    children: "Children"
                }
            }
        }
    });

        });


function CloseClearPopup() {
    $('#APIOverView').attr('src', "../Common/blank.html");
}

function OpenOverViewpupMain(scheduledonid, instanceid) {
    $('#divApiOverView').modal('show');
    $('#APIOverView').attr('width', '1150px');
    $('#APIOverView').attr('height', '600px');
    $('.modal-dialog').css('width', '1200px');
    $('#APIOverView').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
}
function ClearAllFilterMain(e) {
    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
    $("#dropdownUser").data("kendoDropDownTree").value([]);
    $("#dropdownACT").data("kendoDropDownList").value([]);
    if ($("#dropdownSequence").data("kendoDropDownList") != undefined) {
        $("#dropdownSequence").data("kendoDropDownList").value([]);
    }
    $("#dropdownPastData").data("kendoDropDownList").select(4);
    $("#dropdownFY").data("kendoDropDownList").select(0);
    $("#dropdownfunction").data("kendoDropDownList").select(0);
    $("#Startdatepicker").val('');
    $("#Lastdatepicker").val('');
    $('#ClearfilterMain').css('display', 'none');
    $("#grid").data("kendoGrid").dataSource.filter({});
    BindGrid();
    e.preventDefault();
}

$("#export").kendoTooltip({
    filter: ".k-grid-edit3",
    content: function (e) {
        return "Export to Excel";
    }
});

    </script>
</head>
<body style="overflow-x: hidden;">
    <form>

        <div class="row">
            <div class="col-lg-12 col-md-12 colpadding0">
                <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px; padding-left: 5px; padding-top: 5px;">Compliance List</h1>
                <h1 id="display" runat="server" style="display: none; margin-top: 2px; font-size: 20px; font-weight: 100; font-size: 17px; color: #666; font-weight: 500; margin-bottom: 17px;"></h1>
            </div>
        </div>

        <div id="example">
            <div style="margin: 0.5% 0 0.5%;">
                <input id="dropdowntree" style="width: 23%; margin-right: 0.8%;" />
                <input id="dropdownFY" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownlistRisk" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownlistStatus" style="width: 15%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 13.4%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" cssclass="clsROWgrid" style="width: 12.9%;" />
            </div>

            <div style="margin: 0.5% 0 0.5%;">
                <input id="CustName" type="hidden" value="<% =CustomerName%>" />
                <input id="DisplayDepartment" type="hidden" value="<% =Departmentid%>" />
                <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
                <input id="dropdownACT" style="width: 23%; margin-right: 0.8%;" />
                <input id="dropdownPastData" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownfunction" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownUser" style="width: 15%; margin-right: 0.8%;" />
                <button id="ClearfilterMain" style="display: none; float: right; height: 30px; margin-left: 0.9%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                <button id="Applyfilter" style="margin-left: 0.8%; height: 30px; float: right;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="export" onclick="exportReport(event)" style="height: 30px; float: right;"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>

            </div>

            <div class="clearfix"></div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: 1px;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: -1px;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; color: #535b6a; margin-top: -1px;" id="filterUser1">&nbsp;</div>
            <div id="grid" style="margin-bottom: 10px;"></div>



            <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="overflow: hidden;">
                <div class="modal-dialog" style="width: 1200px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <iframe id="downloadfile" src="about:blank" width="0" height="0" style="display: none;"></iframe>
    </form>
</body>
</html>
