﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Help" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="NewCSS/stylenew.css" rel="stylesheet" />
    <link href="NewCSS/Help.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="Newjs/jquery.js"></script>
    <script type="text/javascript" src="Newjs/jquery-ui-1.9.2.custom.min.js"></script>
</head>
<body>
    <div id="Help_MainContainer">
       <div id="Help_Container">
            <div class="clsHelpClick" onclick="ViewPannal()" style="cursor: pointer">Help</div>
        </div>
        <div id="Help_Video">
            <div class="clsVideoClick" onclick="ViewVideoPannal()" style="cursor: pointer">Video</div>
        </div>
        <form id="form1" runat="server">
            <%-- <header class="panel-heading tab-bg-primary ">
          <ul id="rblRole1" class="nav nav-tabs">
                                          
               <li class="dummyval" id="Help_Container">                                                     
                     <asp:LinkButton ID="liPerformer" onclientclick="ViewPannal()" runat="server">Help</asp:LinkButton>
               </li>
                                             
              <li class="dummyval" id="Help_Video">                                                      
                 <asp:LinkButton ID="liReviewer" onclientclick="ViewVideoPannal()" runat ="server">Video</asp:LinkButton>
              </li>
                                                                                 
           </ul>                                
     </header>--%>
            <div id="Help_Container" class="container" style="color: #666; background-color: white; padding-left: 0px;">
                <div class="panel-body" id="Dashbord" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading" id="pagetype">Dashboard</h1>
                    </div>
                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">How Can I Manage Widgets on My Dashboard?</h4>
                    </div>
                    <div style="color: #666; background-color: white;">You can manage the widgets on My Dashboard. Click the User Preference/User Profile on the right hand side of My Dashboard and choose from the widgets you wish to keep:</div>
                    <div style="color: #666; background-color: white;">
                        <ul>
                            <li style="list-style-position: outside; list-style: square;"><a href="#persummary" style="color: ;" class="HelpHover">Performer Summary</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#revsummary" style="color: " class="HelpHover">Reviewer Summary</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#evensummary" style="color" class="HelpHover">Event Owner Summary</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#perloc" style="color: " class="HelpHover">Performer Location</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#revloc" style="color: " class="HelpHover">Reviewer Location</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#daily" style="color: " class="HelpHover">Daily Updates</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#news" style="color: " class="HelpHover">Newsletter</a></li>
                        </ul>
                    </div>
                    <h4>What Can I Do on My Dashboard?</h4>

                    <ul style="list-style-type: circle" id='persummary'>
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view your summary as Performer</li>
                        <p style="color: #666666;">
                            Performer Summary gives you an overview of the task/compliance items which require action from you. You can click on the following tabs to take action:
                        </p>
                        <ul style="list-style-type: circle">
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Upcoming</li>
                            <p style="color: #666666;">Upcoming Compliances reflect those tasks/compliance items, the due date of which is in the next 30 days. </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Overdue</li>
                            <p style="color: #666666;">Overdue Compliances those tasks/compliance items, the due date of which is in the past.</p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Checklist</li>
                            <p style="color: #666666;">Checklists are those tasks which are ongoing in nature. </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Rejected</li>
                            <p style="color: #666666;">Task/compliance items which have been sent for review by you and rejected by your Reviewer.</p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Pending For Review</li>
                            <p style="color: #666666;">Task/compliance items which have been completed by you and awaiting review from your Reviewer.</p>
                        </ul>
                    </ul>


                    <ul id='revsummary'>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc; font-weight: 400;">You can view your summary as Reviewer</li>

                        <p style="color: #666666;">Reviewer Summary gives you an overview of the task/compliance items which have been completed or not completed by your Performer and which require further action from you. You can click on the following tabs to take action:</p>
                        <ul style="list-style-type: circle">
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Due But Not Submitted
                            </li>
                            <p style="color: #666666;">
                                Due But Not Submitted are those tasks/compliance items, the due date of which has been missed by your Performer.
                            </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Pending For Review
                            </li>
                            <p style="color: #666666;">
                                Task/compliance items which have been completed by your Performer and awaiting review from you.
                            </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Checklist
                            </li>
                            <p style="color: #666666;">
                                Checklists are those tasks which are ongoing in nature.
                            </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Rejected
                            </li>
                            <p style="color: #666666;">
                                Task/compliance items which have been sent for review by your Performer and rejected by you.
                            </p>
                        </ul>
                    </ul>
                    <ul style="list-style-type: circle" id="evensummary">
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view your summary as Event Owner</li>
                        <p style="color: #666666;">
                            You are responsible for activating the Events which have been assigned to you as an Event Owner. There are certain tasks/compliance items that are associated with an Event. These tasks/compliance items get triggered when the date of the Event is updated.
                        </p>
                    </ul>
                    <ul id="perloc">
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view know your Performer’s Location
                        </li>
                        <p class="Help_font" style="color: #666666;">
                            You can view the location of your Performer and also the percentage of completed Statutory and Internal tasks/compliances.
                        </p>
                    </ul>
                    <ul id="revloc">
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view know your Reviewer’s Location
                        </li>
                        <p class="Help_font" style="color: #666666;">
                            You can view the location of your Reviewer and also the percentage of completed Statutory and Internal tasks/compliances.
                        </p>
                    </ul>
                    <ul id="daily">
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Stay updated with the latest news by reading our Daily Updates
                        </li>
                    </ul>
                    <ul id="news">
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Missed our Daily Updates? You can read our Newsletters at the end of the fortnight.
                        </li>
                    </ul>
                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Generate Support Ticket
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="images/manager-support.png" kasperskylab_antibanner="on">
                            Communicate your issues or problems related to AVACOM through Support Ticket.
                        </p>
                    </ul>
                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Send Internal Emails
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="images/manager-email.png" kasperskylab_antibanner="on">
                            Send internal emails through AVACOM.                                
                        </p>
                    </ul>
                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Chat Online
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="Images/chat2.PNG" kasperskylab_antibanner="on" />
                            Have a question or query? Chat with our experts. 
                        </p>
                    </ul>
                </div>
                <div class="panel-body" id="Workspace" style="padding-left: 0px;">
                    <div>
                        <h1>My Workspace</h1>
                    </div>
                    <ul>
                        <li style="font-size: 16px;">What Can I Do in My Workspace
                
                        </li>
                        <p style="color: #666666;">
                            You can view all the tasks/compliance items assigned to you. These tasks/compliance items are unfiltered and consolidated in My Workspace.
                        </p>
                        <li style="font-size: 16px;">How Do I Use Filters
                
                        </li>
                        <p style="color: #666666;">
                            Four filter options are available on My Workspace. You can use a single filter or a combination of the filters and click Search.
                        </p>
                        <li style="font-size: 16px;">How Do I Use Advance Search
                
                        </li>
                        <p style="color: #666666;">
                            The Advance Search option enables you to filter tasks/compliance items by Type, Category, Act and Date.
                        </p>
                    </ul>
                </div>
                <div class="panel-body" id="Reports" style="padding-left: 0px;">
                    <div class="report_Heading">
                        <h1 class="Help_Heading">My Reports</h1>
                    </div>
                    <ul>
                        <li class="Help_font" style="font-size: 16px; line-height: 20px!important;">What Can I Do or See in My Reports
                    
                        </li>
                        <p style="color: #666666;">
                            All the tasks/compliance items assigned to you as a Performer or Reviewer can be viewed in My Reports. You can export the report to Excel.
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Use Filters
                    
                        </li>
                        <p style="color: #666666;">
                            Four filter options are available on My Workspace. You can use a single filter or a combination of the filters and click Search.
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Use Advance Search
                    
                        </li>
                        <p style="color: #666666;">
                            The Advance Search option enables you to filter tasks/compliance items by Type, Category, Act and Date.
                        </p>
                    </ul>
                </div>

                <div class="panel-body" id="Documents" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading">My Documents</h1>
                    </div>
                    <p style="color: #666666;">Documents and Working Files uploaded by you while saving a task/compliance item can be viewed in My Documents.</p>
                    <p style="color: #666666;">
                        You can download documents in multiple by checking the tick box on the left side of the task/compliance item and then clicking Download.
                    </p>
                </div>
                <div class="panel-body" id="Reminders" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading">My Reminders</h1>
                    </div>
                    <ul>
                        <li class="Help_font" style="font-size: 16px;">What Can I Do or See in My Reminders
                    
                        </li>
                        <p style="color: #666666;">
                            You can view all the reminders for the compliance assigned to you. The reminders you see are standard reminders set by AVACOM. The reminders are sent according to the table below:
                        </p>
                        <asp:Table runat="server" ID="table1" BorderColor="#666666" BorderWidth="2" CellPadding="5" CellSpacing="5">
                            <asp:TableHeaderRow runat="server">
                                <asp:TableHeaderCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Frequency of Compliance</asp:TableHeaderCell>
                                <asp:TableHeaderCell BorderWidth="1" Font-Bold="true">First reminder (before)</asp:TableHeaderCell>
                                <asp:TableHeaderCell BorderWidth="1" Font-Bold="true">Second reminder (before)</asp:TableHeaderCell>
                                <asp:TableHeaderCell BorderWidth="1" Font-Bold="true">Third reminder (before)</asp:TableHeaderCell>
                                <asp:TableHeaderCell BorderWidth="1" Font-Bold="true">Fourth reminder (before)</asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Monthly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">7 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">NA</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Quarterly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">NA</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Four Monthly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">NA</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Half Yearly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">60 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Annual</asp:TableCell>
                                <asp:TableCell BorderWidth="1">60 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Two Yearly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">60 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell BackColor="#1fd9e1" BorderWidth="1" Font-Bold="true">Seven Yearly</asp:TableCell>
                                <asp:TableCell BorderWidth="1">60 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">30 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">15 days</asp:TableCell>
                                <asp:TableCell BorderWidth="1">2 days</asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <li class="Help_font" style="font-size: 16px; padding-top: 2%;">How Do I Use Filters
                    
                        </li>
                        <p style="color: #666666;">
                            Three filter options are available on My Reminders. You can use a single filter or a combination of the filters and click Search. You can also type keywords to search.
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Add New Reminders
                    
                        </li>
                        <p style="color: #666666;">
                            You can add new reminders by clicking Add New and choosing the Compliance Type, Location and Compliance Name. Select a Remind On date and Save.
                        </p>
                    </ul>
                </div>

                   <div class="panel-body" id="Escalation" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading">My Escalation</h1>
                    </div>
                    <ul>
                        <li class="Help_font" style="font-size: 16px;">What Can I Do or See in My Escalation
                     
                        </li>
                        <p style="color: #666666;">
                            You can view all the compliaces which will reviewed by you. For these compliance you can configure the escalations as well reviewer can set the dates by/on which performer should start creating or submitting compliance working file.</p>
                         <p style="color: #666666;">
                           <div style="float: left;color: #666666;">
                                        <b>Work File Timeline</b> - This is the day/date on which performer should start sharing/submits the complianace working file to reviewer.
                                    </div>
                                     <div style="float: left;color: #666666;">
                                        <b>Escalation</b> - From this day/date the reviewer will start receiving the mail alerts for compliance
                                    </div>
                         </p>
                         <div class="clearfix"></div>
                        <li class="Help_font" style="font-size: 16px; padding-top: 2%;">How Do I Use Filters
                    
                        </li>
                        <p style="color: #666666;">
                            Three filter options are available on My Escalation. You can use a single filter or a combination of the filters and click Search. You can also type keywords to search.
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Add/update escalations
                    
                        </li>
                        <p style="color: #666666;">
                            You can add new escalation by clicking on Add in table or you update from table.
                        </p>
                    </ul>
                </div>

                <div class="panel-body" id="Dashbord1">
                    <div>
                        <h1 id="pagetype1"><u>Help Text for AVACOM: Management</u></h1>
                    </div>
                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">How Can I Manage Widgets on My Dashboard?</h4>
                    </div>
                    <div style="color: #666; background-color: white;">You can manage the widgets on My Dashboard. Click the User Preference/User Profile on the right hand side of My Dashboard and chose from the widgets you wish to keep:</div>
                    <div style="color: #666; background-color: white;">
                        <ul>
                            <li style="list-style-position: outside; list-style: square;"><a href="#funsummary" style="color: #666;" class="HelpHover">Functions Summary</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#Risksummary" style="color: #666" class="HelpHover">Risk  Summary</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#GradingRep" style="color: #666" class="HelpHover">Grading Report</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#DailyUp" style="color: #666" class="HelpHover">Daily Updates</a></li>
                            <li style="list-style-position: outside; list-style: square;"><a href="#NewsLet" style="color: #666" class="HelpHover">Newsletter</a></li>
                        </ul>
                    </div>
                    <h4>What Can I Do on My Dashboard?</h4>
                    <br />
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Entities assigned to you</li>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Location of your Entities</li>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Functions</li>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view all assigned Compliances</li>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view all Users </li>
                    </ul>
                    <ul style="list-style-type: circle" id="funsummary">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Functions Summary </li>
                        <p>Functions Summary shows you compliance status under each function. This chart also shows compliance/tasks completed in time, completed after due date and not completed.</p>
                    </ul>
                    <ul style="list-style-type: circle" id="Risksummary">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Risk Summary </li>
                        <p>Risk Summary shows you the chart of compliance/tasks of each risk type whether completed in time, completed after due date or not completed.</p>
                    </ul>
                    <ul style="list-style-type: circle" id="GradingRep">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can view Grading Report</li>
                        <p>Grading Report shows the grades given to every month in accordance with completed tasks/compliances. The grades are given on basis of risk. The color of grades are indicated as follows:</p>
                        <ul>
                            <li>
                                <img src="Images/Blue.PNG" style="width: 60px; height: 20px" />Indicates 100% High Risk and Medium Risk compliances are completed in time and at least 75% Low Risk compliances are completed in time.
                            </li>
                            <li>
                                <img src="Images/Yellow1.PNG" style="width: 60px; height: 20px" />Indicates 75% High Risk compliances are completed in time and at least 50% Medium Risk and Low Risk compliances are completed in time.
                            </li>
                            <li>
                                <img src="Images/Red1.png" style="width: 60px; height: 20px" />Otherwise red.
                            </li>
                        </ul>
                    </ul>
                    <ul style="list-style-type: circle" id="DailyUp">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">Stay updated with the latest news by reading our Daily Updates</li>
                    </ul>
                    <ul style="list-style-type: circle" id="NewsLet">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">Missed our Daily Updates? You can read our Newsletters at the end of the fortnight.</li>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can generate Support Ticket</li>
                        <ul>
                            <li>
                                <img style="width: 80px; height: 80px" src="images/manager-support.png" kasperskylab_antibanner="on">Communicate your issues or problems related to AVACOM through Support Ticket.
                            </li>
                        </ul>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can send Internal Emails</li>
                        <ul>
                            <li>
                                <img style="width: 80px; height: 80px" src="images/manager-email.png" kasperskylab_antibanner="on">
                                Send internal emails through AVACOM.
                            </li>
                        </ul>
                    </ul>
                    <ul style="list-style-type: circle">
                        <li style="font-size: 17px; list-style-position: outside; list-style: disc;">You can Chat Online/Offline</li>
                        <ul>
                            <li>
                                <img style="width: 70px; height: 70px" src="Images/chat2.PNG" kasperskylab_antibanner="on" />
                                Have a question or query? Chat with our experts.
                            </li>
                        </ul>
                    </ul>

                </div>



            </div>
    </div>
    <div class="" id="HelpVideo_Container" style="">
        <%-- <div>
                        <h1 class="Help_Heading">Video</h1>
                    </div>--%>
          <h1 class="Video_Heading" id="Video_Head1">Login</h1>
          <div id="videoPlayer_01" >
            <video controls loop >
                <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/login1.mov" type="video/mp4">
            </video>
        </div>
        <h1 class="Video_Heading" id="Video_Head2">Reset Password</h1>
        <div id="videoPlayer_02" style="width:650px;">
            <video controls loop >
                <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/resetyourpassword.mov" type="video/mp4">
            </video>
        </div>
        <h1 class="Video_Heading" id="Video_Head3">Performer/Reviewer</h1>
        <div id="videoPlayer_03">
            <video controls style="width: 640px; height: 340px;" >
                <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/demo.mp4" type="video/mp4">
            </video>
        </div>
          <h1 class="Video_Heading" id="Video_Head4" style="top:250%">Management</h1>
        <div id="videoPlayer_04" style="top:280%">
            <video controls style="width: 640px; height: 340px;" >
                <source src=" https://avantiscdnprodstorage.blob.core.windows.net/cdn/management.mp4" type="video/mp4">
            </video>
        </div>
        <%--<div id="videoPlayer_04">
			                <video controls loop autoplay>
			                  <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/demo.mp4" type="video/mp4">
			                </video>
		                </div>
                         <div id="videoPlayer_05">
			                <video controls loop autoplay>
			                  <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/demo.mp4" type="video/mp4">
			                </video>
		                </div>
                         <div id="videoPlayer_06">
			                <video controls loop autoplay>
			                  <source src="https://avantiscdnprodstorage.blob.core.windows.net/cdn/demo.mp4" type="video/mp4">
			                </video>
		                </div>--%>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">


        //function getUrlVars() {
        //    var vars = [], hash;
        //    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        //    for (var i = 0; i < hashes.length; i++) {
        //        hash = hashes[i].split('=');
        //        vars.push(hash[0]);
        //        vars[hash[0]] = hash[1];
        //    }
        //    return vars;
        //}

        function ReadQuerySt(e) {
            try {
                for (hu = window.location.search.substring(1), gy = hu.split('&'), i = 0; i < gy.length; i++)
                    if (ft = gy[i].split('='), ft[0].toLowerCase() == e.toLowerCase()) return ft[1];
                return ''
            } catch (t) {
                return ''
            }
        }

        function fhead(val) {
            $('#pagetype').html(val);
            //  $('#sppagetype').html(val)

        }

        //$(document).ready(function () {
        //    $('.clsHelpClick').click(function () {
        //        $('.clsHelpClick').css('background-color', "#EEE");
        //        $(this).css('background-color', "#1fd9e1");
        //    });
        //});
        function ViewVideoPannal() {
            //alert("hiiiiiiiiiiiiii")
            $('.panel-body').css("display:block");
            $('#HelpVideo_Container').show();
            $('#HelpVideo_Container').css("display:block");
            $('.clsVideoClick').css('text-decoration', "underline");
            $('.clsHelpClick').css('text-decoration', "none");
            $('.clsHelpClick').css('color', "#000000");
            $('.clsVideoClick').css('color', "#1fd9e1");
            $('#videoPlayer_01,#videoPlayer_02,#videoPlayer_0').show();
            $('#Dashbord').hide();
            $('#Workspace').hide();
            $('#Reports').hide();
            $('#Documents').hide();
            $('#Reminders').hide();
            $('#Dashbord1').hide();
            // $('.container').hide();
        }

        function ViewPannal() {
            var PanelName = ReadQuerySt('id');
            //var PanelName = getUrlVars('id');
            if (PanelName == "Dashboard") {
                $('#Dashbord').show();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "My%20Workspace") {
                $('#Dashbord').hide();
                $('#Workspace').show();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "My%20Reports") {
                $('#Dashbord').hide();
                $('#Workspace').hide();
                $('#Reports').show();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "My%20Documents") {
                $('#Dashbord').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').show();
                $('#Reminders').hide();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "My%20Reminders") {
                $('#Dashbord').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').show();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "managementDashboard") {
                $('#Dashbord').hide();
                $('#Workspace').show();
                $('#Reports').show();
                $('#Documents').show();
                $('#Reminders').show();
                $('#Dashbord1').show();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else if (PanelName == "My%20Escalation") {
                $('#Dashbord').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#Dashbord1').hide();
                $('#Escalation').show();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }
            else {
                $('#Dashbord').show();
                $('#Workspace').show();
                $('#Reports').show();
                $('#Documents').show();
                $('#Reminders').show();
                $('#Dashbord1').hide();
                $('#HelpVideo_Container').hide();
                $('.clsHelpClick').css('text-decoration', "underline");
                $('.clsHelpClick').css('color', "#1fd9e1");
                $('.clsVideoClick').css('text-decoration', "none");
                $('.clsVideoClick').css('color', "#000000");
            }



        }
        ViewPannal();
    </script>

    </form>
</div>
</body>
</html>
