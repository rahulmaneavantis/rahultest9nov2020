﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="myTask.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Task.myTask" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });
        }
        function initializeDatePicker() {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            $('#<%= txtEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                //maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }

        $(document).on("click", "#ContentPlaceHolder1_UpdatePanel4", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });

            }
        });

        $(function () {
            $("[id*=tvFilterLocation1] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });
        })

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        //$(document).ready(function () {

        //    $(".nav a").on("click", function () {
        //        $(".nav").find(".active").removeClass("active");
        //        $(this).parent().addClass("active");
        //    });
        //    $('#loaderdiv').hide();
        //    window.parent.hideloader();
        //});
        function fredv(type) {
            if (type == "P") {
                $('#ContentPlaceHolder1_liPerformerReviewer').addClass('active');
                $('#ContentPlaceHolder1_liTaskCreation').removeClass('active');
                $('#ContentPlaceHolder1_TaskCreation').removeClass('active');
                $('#ContentPlaceHolder1_PerformerReviewer').addClass('active');
            } else if (type == "R") {
                $('#ContentPlaceHolder1_liTaskCreation').addClass('active');
                $('#ContentPlaceHolder1_liPerformerReviewer').removeClass('active');
                $('#ContentPlaceHolder1_TaskCreation').addClass('active');
                $('#ContentPlaceHolder1_PerformerReviewer').removeClass('active');
            }
        }
        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function openModal() {
            if (Displays() == true) {
                $('#ComplaincePerformer').modal('show');
            }
            return true;
        }
        function setIframeHeight(height) {
            $('#showdetails').attr('height', height);
        }
        function ShowDialog(taskInstanceID, taskScheduleOnID) {
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '550px');
            $('#showdetails').attr('src', "/Task/TaskStatusTransactionPerformer.aspx?TID=" + taskInstanceID + "&TSOID=" + taskScheduleOnID);
        };

        function ShowReviewerDialog(taskInstanceID, taskScheduleOnID) {
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '550px');
            $('#showdetails').attr('src', "/Task/TaskStatusTransactionReviewer.aspx?TID=" + taskInstanceID + "&TSOID=" + taskScheduleOnID);
        };

        function openModalInternalPer() {
            $('#ComplainceInternalPerformaer').modal('show');
            return true;
        }

        function openModalReviewer() {
            $('#ComplainceReviewer').modal('show');
            return true;
        }

        function openModalInternalReviewer() {
            $('#ComplainceInternalReviewer').modal('show');
            return true;
        }

        function openModalEventBased() {
            $('#ComplainceEventBased').modal('show');
            return true;
        }

        function openModalEventBasedReviewer() {
            $('#ComplainceEventBasedReviewer').modal('show');
            return true;
        }



        function fopenpopup() {
            $('#divTaskDetailsDialog').modal('show');
        }
        function OpenTaskType() {
            $('#divTaskTypeDetailDialog').modal('show');
        }
        function OpenSubTaskType() {
            $('#divSubTaskTypeDetailDialog').modal('show');
        }
        function fclosepopup() {
            $('#divTaskDetailsDialog').modal('hide');
        }

        function fopenpopup1() {
            $('#divAssignmentDetailsDialog').modal('show');
        }
        function fclosepopup1() {
            $('#divAssignmentDetailsDialog').modal('hide');
        }

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            setBreadCrumText();
            initializeDatePicker();
        });

        function setBreadCrumText() {
            setactivemenu('leftworkspacemenu');

            var filterbyType = $("#<%= ddlTaskType.ClientID %> option:selected").text();
            var filterbyStatus = $("#<%= ddlStatus.ClientID %> option:selected").text();

            if (filterbyType == '') {
                fhead('My Task');
            } else {
                if (filterbyStatus != '') {
                    $('#pagetype').css("font-size", "22px");
                    if (filterbyStatus == 'PendingForReview') {
                        filterbyStatus = 'Pending For Review';
                    } else if (filterbyStatus == 'DueButNotSubmitted') {
                        filterbyStatus = 'Due But Not Submitted';
                    } else if (filterbyStatus == 'Status') {
                        filterbyStatus = 'All';
                    }

                    fhead('My Task/ ' + filterbyType + '/ ' + filterbyStatus);
                }
                else {
                    fhead('My Task/ ' + filterbyType);
                }
            }
        }

    </script>

    <style type="text/css">
        .tab-pane .entrycount {
            min-width: 168px !important;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }


        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        .chosen-results {
            height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <ul class="nav nav-tabs calender-li" role="tablist">
                <li id="liPerformerReviewer" style="cursor: pointer;" runat="server" onclick="fredv('P');"><a id="A1" runat="server" aria-controls="PerformerReviewer" role="tab" data-toggle="tab">Perform/Review</a></li>
                <li id="liTaskCreation" runat="server" style="cursor: pointer;" onclick="fredv('R');"><a id="A2" runat="server" aria-controls="TaskCreation" role="tab" data-toggle="tab">Task Creation</a></li>
            </ul>
            <div class="tab-content" style="padding-top: 2px">
                <div role="tabpanel" class="tab-pane" runat="server" id="PerformerReviewer">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <div>
                                <div class="row Dashboard-white-widget">
                                    <div class="dashboard">
                                        <div class="col-lg-12 col-md-12 ">
                                            <section class="panel">

                        <%--Header Section--%>
                        <header class="panel-heading tab-bg-primary ">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <%if (TaskRoles != null)
                                    {
                                %>                                           
                                <% if (TaskRoles.Contains(3))%>
                                <%{%>
                                    <li class="dummyval">                                                     
                                        <asp:LinkButton ID="liPerformer" OnClick="liPerformer_Click" runat="server">Performer</asp:LinkButton>
                                    </li>
                                <%}%>
                                <% if (TaskRoles.Contains(4))%>
                                <%{%>
                                    <li class="dummyval">                                                      
                                        <asp:LinkButton ID="liReviewer" OnClick="liReviewer_Click" runat="server">Reviewer</asp:LinkButton>
                                    </li>
                                <%}%>    
                                <%}%>                                    
                            </ul>
                        </header>
                        <%--Header Section END--%>

                        <div class="clearfix"></div>
           <div style="float:right;margin-right:5px; margin-top:-42px;">
               <asp:LinkButton ID="linkbutton" runat="server" CssClass="btn btn-search" onclick="linkbutton_onclick" >Back</asp:LinkButton>
  
           </div>
                        <div class="panel-body">
                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="vsMyTask" Style="padding-left: 5%" runat="server" Display="none"
                                    class="alert alert-block alert-danger fade in" ValidationGroup="TaskValidationGroup" />
                                <asp:CustomValidator ID="cvMyTask" runat="server" class="alert alert-block alert-danger fade in"
                                    EnableClientScript="true" ValidationGroup="TaskValidationGroup" />
                            </div>

                            <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                <%--<div class="col-md-10 colpadding0" style="width: 69.666667%;">--%>
                                <div class="col-md-2 colpadding0 entrycount">
                                    <div class="col-md-3 colpadding0">
                                        <p style="color: #999; margin-top: 5px">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                                        class="form-control" Style="width: 40%; float: left; margin-left: 10px;">
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-2 colpadding0 entrycount">
                                    <asp:DropDownList runat="server" ID="ddlTaskType" class="form-control" Style="width: 90%;"
                                        OnSelectedIndexChanged="ddlTaskType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Statutory" Value="1" />
                                        <asp:ListItem Text="Internal" Value="2" />
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-2 colpadding0 entrycount">
                                    <asp:DropDownList runat="server" ID="ddlStatus" class="form-control" Style="width: 90%;">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount">
                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" CssClass="txtbox" autocomplete="off" PlaceHolder="Entity/Sub-Entity/Location"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" />
                                    <div id="divFilterLocation" style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="120%" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;"
                                            ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>
                                <%-- </div>--%>

                                <div class="col-md-3 colpadding0">
                                    <div class="col-md-6 colpadding0" style="text-align: center">
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" OnClientClick="setBreadCrumText();"
                                            OnClick="btnSearch_Click" runat="server" Text="Apply" />
                                    </div>
                                    <%-- <div class="col-md-6" style="margin-left: -10%;">
                                         <button id="btnAddTaskDetails" runat="server" visible="false" class="form-control m-bot15" type="button" style="margin-top: -40px; position: absolute; width: 150px;"  data-toggle="dropdown">More Link
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu" style="top:0px !important;width:151px !important;margin: -9px 15px 0 !important; min-width:150px !important; ">                                                   
                                                                  <li>   <a id="btnAddTask" runat="server"  href="../Task/PerformerTaskDetail.aspx">Task Details</a></li>                                
                                                                </ul> 
                                           </div>--%>
                                    <div class="col-md-6" style="margin-left: -10%;">
                                                    <button id="btnmore" runat="server" visible="false" class="form-control m-bot15" type="button" style="margin-top: -40px; position: absolute; width: 150px;"  data-toggle="dropdown">More Link
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu" style="top:0px !important;width:151px !important;margin: -9px 15px 0 !important; min-width:150px !important; ">                                                   
                                                                  <li>   <a id="btnReassignPerformer" runat="server"  href="../Task/TaskReassignment.aspx">Reassign&nbsp;Performer</a></li>                                
                                                                </ul> 
                                                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search" style="width: 150px;">Advanced Search</a>
                                                </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                                <!--advance search starts-->
                                <div class="col-md-12 AdvanceSearchScrum">
                                    <div id="divAdvSearch" runat="server" visible="false">
                                        <p>
                                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                                        </p>
                                        <p>
                                            <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceList_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                                        </p>
                                    </div>

                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                </div>

                                <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="width: 75%">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body" style="margin-left: 40px;">
                                                <h2 style="text-align: center">Advanced Search</h2>

                                                <div class="col-md-12 colpadding0">
                                                    <div class="table-advanceSearch-selectOpt">
                                                        <asp:DropDownListChosen runat="server" ID="ddlType" class="form-control" Width="95%" AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                        </asp:DropDownListChosen>
                                                    </div>

                                                     <asp:Panel ID="Panelsubtype" runat="server">
                                                        <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:DropDownListChosen runat="server" ID="ddlComplianceSubType" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </asp:Panel>
                                                   
                                                    <asp:Panel ID="PanelAct" runat="server">
                                                        <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </asp:Panel>

                                                     <div class="table-advanceSearch-selectOpt">
                                                        <asp:DropDownListChosen runat="server" ID="ddlCategory" class="form-control" Width="95%"  AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                        </asp:DropDownListChosen>
                                                    </div>

                                                   
                                                    </div>
                                                    <div class="clearfix"></div>

                                                     <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                                                         <div style="display:none;">
                                                    <asp:Panel ID="PanelPerformerList" runat="server">
                                                        <div id="Div1" runat="server" class="table-advanceSearch-selectOpt" >
                                                            <asp:DropDownListChosen runat="server" ID="ddlPerfRevUser" class="form-control" Width="95%" 
                                                                DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="2">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </asp:Panel>
                                                         </div>
                                                          <asp:Panel ID="Panel1" runat="server">
                                                        <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Height="35px" Width="95%" Style="padding-left: 7px; border-radius: 5px;" 
                                                                placeholder="Due Date(From)" CssClass="form-control" ID="txtStartDate" />
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Height="35px" Width="95%" Style="padding-left: 7px; border-radius: 5px;" 
                                                                placeholder="Due Date(To)" CssClass="form-control" ID="txtEndDate" />
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="PanelSearchType" runat="server">
                                                        <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                                            <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="Type to Filter" Width="95%"  
                                                                ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                                                        </div>
                                                    </asp:Panel>

                                                   

                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="table-advanceSearch-buttons" style="height: 30px; margin: 10px auto;">
                                                        <table id="tblSearch">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnAdvSearch" Text="Search" OnClick="btnAdvSearch_Click" class="btn btn-search" runat="server" OnClientClick="return hidediv();" />
                                                                </td>
                                                                <td>
                                                                    <button id="btnAdvClose" type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--advance search ends-->
                            

                            <div class="clearfix"></div>

                            <div class="tab-content">
                                <div runat="server" id="PerformerTask" class="tab-pane active col-md-12 colpadding0">                                    
                                        <asp:GridView runat="server" ID="grdTaskPerformer" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                            OnRowDataBound="grdTaskPerformer_RowDataBound" PageSize="10" AllowPaging="true" AutoPostBack="true"
                                            CssClass="table" GridLines="None" Width="100%" AllowSorting="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; width: 300px;">
                                                            <asp:Label ID="lblVertical" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Compliance">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Period">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reviewer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblReviewer" runat="server" Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID")) %>' data-toggle="tooltip" data-placement="bottom" ToolTip=""></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:Button runat="server" ID="btnChangeStatus" OnClick="btnChangeStatus_Click" CssClass="btnss"  CommandName="CHANGE_STATUS" 
                                                            CommandArgument='<%# Eval("TaskScheduledOnID") + "," + Eval("TaskInstanceID") %>' Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("TaskStatusID")) %>'/> 
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>                                    
                                </div>

                                <div runat="server" id="ReviewerTask" class="tab-pane active col-md-12 colpadding0">
                                        <asp:GridView runat="server" ID="grdTaskReviewer" AutoGenerateColumns="false"
                                            OnRowDataBound="grdTaskReviewer_RowDataBound" PageSize="10" AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                            CssClass="table" GridLines="None" Width="100%" AllowSorting="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; width: 220px;">
                                                            <asp:Label ID="lblVertical" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Compliance">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblShortDescription" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                            <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblPerformer" runat="server" Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID")) %>' data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Period">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblPeriod" runat="server" Text='<%# Eval("ForMonth") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:Button runat="server" ID="btnChangeStatusReviewer" OnClick="btnChangeStatusReviewer_Click" CssClass="btnss" CommandName="CHANGE_STATUS" 
                                                             CommandArgument='<%# Eval("TaskScheduledOnID") + "," + Eval("TaskInstanceID") %>' Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("TaskStatusID")) %>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Records Found.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                </div>
                                 <div class="col-md-12 colpadding0">
                                            <div class="col-md-6 colpadding0">
                                            </div>
                                            <div class="col-md-6 colpadding0">
                                                <div class="table-paging" style="margin-bottom:20px">
                                                    <asp:ImageButton ID="lBPrevious" CssClass ="table-paging-left"  runat="server" ImageUrl ="~/img/paging-left-active.png" OnClick ="Previous_Click"/>

                                                    <div class="table-paging-text">
                                                        <p><asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label></p> 
                                                    </div>
                                                    <asp:ImageButton ID="lBNext" CssClass ="table-paging-right"  runat="server" ImageUrl ="~/img/paging-right-active.png" OnClick ="Next_Click"/>
                                                </div>
                                            </div>
                                      <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                   </div>
                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnTitle" runat="server" />
                            <asp:HiddenField ID="IsActiveControl" runat="server" Value="false" />
                        </div>
                    </div>

                </div>
                <div role="tabpanel" runat="server" id="TaskCreation">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <div>
                                <asp:UpdatePanel ID="upTaskList" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>


                                        <div>
                                            <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="ComplianceValidationGroupMainTask" />
                                            <asp:CustomValidator ID="CvMainTask" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceValidationGroupMainTask" Display="None" class="alert alert-block alert-danger fade in" />
                                            <asp:Label runat="server" ID="Label3" ForeColor="Red"></asp:Label>
                                        </div>

                                        <div class="row Dashboard-white-widget">
                                            <div class="dashboard">
                                                <div class="col-lg-12 col-md-12 ">
                                                    <section class="panel">                 
                            <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSizeTask" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSizeTask_SelectedIndexChanged" >                        
                                <asp:ListItem Text="5"/>
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                            </div>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                   <asp:DropDownList runat="server" ID="ddlTask" class="form-control"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlTask_SelectedIndexChanged">
                                    </asp:DropDownList>
                              </div>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; margin-left: 10px;">
                                    <asp:DropDownList runat="server" ID="ddlTaskTypeCreation" class="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlTaskTypeCreation_SelectedIndexChanged">
                                    <asp:ListItem Text="All" Value="-1" />
                                    <asp:ListItem Text="Statutory" Value="1" />
                                    <asp:ListItem Text="Internal" Value="2" />
                                    </asp:DropDownList>
                                 </div>
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; margin-left: 10px;"> 
                                <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50" PlaceHolder="Type to Search" AutoPostBack="true" Width="200px" OnTextChanged="tbxFilter_TextChanged" />                              
                            </div>
                             <div style="float: right;margin-right: 25px;margin-top: -34px;">
                                    <asp:LinkButton runat="server" id="linkbuttonTask" style="margin-top: 36px; margin-left: 10px;" CssClass="btn btn-search" onclick="linkbuttonTask_onclick" >Back</asp:LinkButton>  
                             </div>
                            <div style="float:right; margin-top: 2px;margin-left: 50px;">  
                            <asp:LinkButton Text="Add New" CssClass="btn btn-primary" style="margin-top: 1px; margin-left: 10px;" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" /> 
                            </div>

                            <div class="col-md-12 AdvanceSearchScrum">              
                            <div runat="server" id="DivRecordsScrumTask" style="float: right;">
                                 <p style="padding-top: 10px;">
                                    <asp:Label ID="Label1" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecordTask" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecordTask" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecordTask" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>   
                            <div style="margin-bottom: 4px">                                                 
                                <asp:GridView runat="server" ID="grdTask" AutoGenerateColumns="false" AllowSorting="true" 
                                    PageSize="10" AllowPaging="True"  AutoPostBack="true" CssClass="table" 
                                        GridLines="none" Width="100%"   DataKeyNames="ID" OnRowCommand="grdTask_RowCommand"
                                        OnPageIndexChanging="grdTask_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-HorizontalAlign="Center"  />

                                        <asp:TemplateField HeaderText="Title"  ItemStyle-Width="200px">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="TaskTypeName" HeaderText="Task Type"   />

                                        <asp:TemplateField  HeaderText="Sub Task" >
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" Style="color:blue" CommandName="VIEW_TASKS" CommandArgument='<%# Eval("ID") %>'>sub-tasks</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_TASK" OnClientClick="fopenpopup()" CommandArgument='<%# Eval("ID") %>' ToolTip="Edit Task"><img src="../Images/edit_icon_new.png" alt="Edit Task"/></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="SHOW_ASSIGNMENT" OnClientClick="fopenpopup1()" CommandArgument='<%# Eval("ID") %>' ToolTip="Show Assingment">
                                                    <img src="../Images/View-icon-new.png" alt="Display Schedule Information"/></asp:LinkButton>
                                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_TASK" CommandArgument='<%# Eval("ID") %>'
                                                    OnClientClick="return confirm('Are you certain you want to delete this Task?');" ToolTip="Delete Task"><img src="../Images/delete_icon_new.png" alt="Delete Task"/></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                   <RowStyle CssClass="clsROWgrid"   />
                                <HeaderStyle CssClass="clsheadergrid"    />
                                <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                                
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0"></div>                                                        
                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging" style="margin-bottom: 20px;">
                                        <asp:ImageButton ID="ImageButton1" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="PreviousTask_Click" />                           
                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNoTask" runat="server" Text=""></asp:Label>/
                                                <asp:Label ID="lTotalCountTask" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNextTask" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="NextTask_Click"/>            
                                        <asp:HiddenField ID="TotalRowsTask" runat="server" Value="0" />
                                    </div>
                                </div>
                            </div>
                       </section>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divShowDialog" style="height: 100%" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divTaskDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 650px;">
            <div class="modal-content" style="width: 650px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divTaskDetails">
                        <asp:UpdatePanel ID="upTaskDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskDetails_Load">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                    <asp:Label runat="server" ID="Label9" ForeColor="Red"></asp:Label>
                                </div>
                                <div>

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px" id="Div6" runat="server">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Type
                                                </label>
                                                <asp:RadioButton ID="rdoStatutory" Text="Statutory" Font-Bold="true" ForeColor="Black" GroupName="radiotype" runat="server" AutoPostBack="true" OnCheckedChanged="rdoStatutory_CheckedChanged" Checked="true" />
                                                <asp:RadioButton ID="rdoInternal" Text="Internal" Font-Bold="true" ForeColor="Black" GroupName="radiotype" runat="server" AutoPostBack="true" OnCheckedChanged="rdoStatutory_CheckedChanged" />
                                            </div>
                                            <div runat="server" id="divStatutory">
                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Act Filter
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlActTask" CssClass="form-control"
                                                        Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true" OnSelectedIndexChanged="ddlActTask_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Compliance
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlCompliance" OnSelectedIndexChanged="ddlCompliance_SelectedIndexChanged"
                                                        CssClass="form-control" Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div runat="server" id="divInternal">
                                                <div style="margin-bottom: 7px">
                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Internal Compliance
                                                    </label>
                                                    <asp:DropDownList runat="server" ID="ddlInternalCompliance" OnSelectedIndexChanged="ddlInternalCompliance_SelectedIndexChanged"
                                                        CssClass="form-control" Style="padding: 0px; margin: 0px; width: 390px;" AutoPostBack="true" />
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Compliance Type</label>
                                                <asp:TextBox runat="server" ID="txtComplianceType" Text="" ReadOnly="true" Style="width: 390px;" CssClass="form-control" />
                                            </div>

                                            <div style="margin-bottom: 7px;">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Compliance Due Day</label>
                                                <asp:TextBox runat="server" ID="txtComplianceDueDays" Text="" ReadOnly="true" Style="width: 80px;" CssClass="form-control" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px; margin-top: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Title</label>
                                        <asp:TextBox runat="server" ID="txtTaskTitle" Style="width: 390px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Task title can not be empty."
                                            ControlToValidate="txtTaskTitle" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" MaxLength="100" Style="width: 390px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Description can not be empty."
                                            ControlToValidate="txtDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Due Day</label>
                                                <asp:TextBox runat="server" ID="txtDueDays" MaxLength="4" Width="80px" type="number" CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter Due(Day)"
                                                    ControlToValidate="txtDueDays" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                    Display="None" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                                    ValidationGroup="ComplianceValidationGroup" ErrorMessage="Please enter a valid Due Days."
                                                    ControlToValidate="txtDueDays" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtDueDays" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task mapping
                                        </label>
                                        <asp:TextBox runat="server" ID="txtTask" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 209px; position: absolute; z-index: 50; overflow-y: auto; background: white; width: 392px; border: 1px solid gray; height: 200px;" id="dvTask">
                                            <asp:Repeater ID="rptTask" runat="server">
                                                <HeaderTemplate>
                                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                        <tr>
                                                            <td style="width: 100px;">
                                                                <asp:CheckBox ID="TaskSelectAll" Text="Select All" runat="server" onclick="checkAllTask(this)" /></td>
                                                            <td style="width: 282px;">
                                                                <asp:Button runat="server" ID="btnTaskRepeater" Text="Ok" Style="float: left"/></td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 20px;">
                                                            <asp:CheckBox ID="chkTask" runat="server" onclick="UncheckTaskHeader();" /></td>
                                                        <td style="width: 470px;">
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 460px; padding-bottom: 5px;">
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskName" runat="server" Text='<%# Eval("Title")%>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Type
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlAllTaskType" Style="padding: 0px; margin: 0px; width: 380px;" OnSelectedIndexChanged="ddlAllTaskType_SelectedIndexChanged"
                                            CssClass="form-control" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lblAddTaskType" runat="server" Visible="false" OnClientClick="OpenTaskType()" OnClick="lblAddTaskType_Click" ToolTip="Add Task Type"><img src="../Images/add_icon_new.png" alt="Add Task Type" style="margin-top:-29px;float: right;"/></asp:LinkButton>
                                    </div>

                                    <div style="margin-bottom: 7px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sub Task Type
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlSubTaskType" Style="padding: 0px; margin: 0px; width: 380px;"
                                            CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSubTaskType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lblAddSubTask" runat="server" Visible="false" OnClientClick="OpenSubTaskType()" OnClick="lblAddSubTask_Click" ToolTip="Add SubTask Type"><img src="../Images/add_icon_new.png" alt="Add SubTask Type" style="margin-top:-29px;float: right;"/></asp:LinkButton>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            IsAfter/Before
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlIsAfter" Style="padding: 0px; margin: 0px; width: 380px;"
                                            CssClass="form-control" AutoPostBack="true">
                                            <asp:ListItem Text="Before" Value="False" />
                                            <asp:ListItem Text="After" Value="True" />
                                        </asp:DropDownList>
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Is Task Conditional
                                        </label>
                                        <asp:CheckBox ID="ChkIsTaskConditional" runat="server" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Conditional Message</label>
                                        <asp:TextBox runat="server" ID="TxtConditionalMessage" Style="width: 390px;" CssClass="form-control" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            True Condtion</label>
                                        <asp:RadioButtonList ID="rbTrueCondtion" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="0" Selected="True" />
                                            <asp:ListItem Text="No" Value="1" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Display Both Message</label>
                                        <asp:RadioButtonList ID="rbBothMessage" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="0" />
                                            <asp:ListItem Text="No" Value="1" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Yes Message</label>
                                        <asp:TextBox runat="server" ID="txtYesMessage" Style="width: 390px;" CssClass="form-control" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            No Message</label>
                                        <asp:TextBox runat="server" ID="txtNoMessage" Style="width: 390px;" CssClass="form-control" />
                                    </div>

                                    <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Sample Form</label>
                                                <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" Style="color: black;" />
                                                <br />
                                                <asp:FileUpload runat="server" ID="fuSampleFile" Style="color: black;" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="ComplianceValidationGroup" OnClientClick="fopenpopup()" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="fclosepopup()" />
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divAssignmentDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divAssignmentDetails">
                        <asp:UpdatePanel ID="upTaskAssignmentDetails" runat="server" UpdateMode="Conditional" OnLoad="upTaskAssignmentDetails_Load">
                            <ContentTemplate>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 4px">
                                        <asp:ValidationSummary runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="TaskAssignmentValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            class="alert alert-block alert-danger fade in" ValidationGroup="TaskAssignmentValidationGroup1" Display="None" />
                                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="Divbranchlist">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Compliance Location
                                                </label>
                                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation1" PlaceHolder="Select Compliance Applicable Location" autocomplete="off"
                                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                        CssClass="txtbox" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation1">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation1" ShowCheckBoxes="All" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                                            Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                        </asp:TreeView>
                                                    </div>
                                                </div>

                                                <div style="width: 50%; float: right;">
                                                </div>
                                            </div>

                                            <div style="width: 100%; float: left; margin-right: 2%;" runat="server" id="DivbranchlistTask">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Reporting Location
                                                </label>
                                                <div style="width: 50%; float: left; margin-bottom: 15px">
                                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationTask" PlaceHolder="Select Task Applicable Location" autocomplete="off"
                                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 390px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                        CssClass="txtbox" />
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocationTask">
                                                        <asp:TreeView runat="server" ID="tvFilterLocationTask" Width="390px" NodeStyle-ForeColor="#8e8e93"
                                                            OnSelectedNodeChanged="tvFilterLocationTask_SelectedNodeChanged" Style="overflow: auto; height: 250px; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                        </asp:TreeView>
                                                        <asp:CompareValidator ControlToValidate="tbxFilterLocationTask" ErrorMessage="Please select Task Applicable Location."
                                                            runat="server" ValueToCompare="Select Task Applicable Location" Operator="NotEqual" ValidationGroup="TaskAssignmentValidationGroup"
                                                            Display="None" />
                                                    </div>
                                                </div>

                                                <div style="width: 50%; float: right;">
                                                </div>
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Start Date</label>
                                                <asp:TextBox runat="server" ID="tbxStartDate" Style="width: 390px;" CssClass="form-control" AutoPostBack="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select start date"
                                                    ControlToValidate="tbxStartDate" runat="server" ValidationGroup="TaskAssignmentValidationGroup"
                                                    Display="None" />
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Performer</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlPerformer" Width="390px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    CssClass="form-control">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Reviewer</label>
                                                <asp:DropDownListChosen runat="server" ID="ddlReviewer" Width="390px" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    CssClass="form-control">
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                                <asp:Button Text="Save" runat="server" ID="btnSaveTaskAssignment" OnClick="btnSaveTaskAssignment_Click" CssClass="btn btn-primary"
                                                    ValidationGroup="TaskAssignmentValidationGroup" />
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <asp:Panel ID="Panel3" Width="100%" runat="server" Style="min-height: 250px; max-height: 400px; width: 100%; overflow-y: auto;">
                                                    <asp:GridView runat="server" ID="GrdAssigment" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                                        AllowSorting="true"
                                                        DataKeyNames="TaskAssignmentID,UserID" OnRowEditing="GrdAssigment_RowEditing" OnRowCancelingEdit="GrdAssigment_RowCancelingEdit"
                                                        OnRowUpdating="GrdAssigment_RowUpdating" OnRowDataBound="GrdAssigment_RowDataBound" AllowPaging="True" PageSize="100" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Compliance Branch">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Reporting Location">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblTaskBranch" runat="server" Text='<%# Eval("TaskCustomerBranchName")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Start Date">
                                                                <ItemTemplate>
                                                                    <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="User">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("User") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("User") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:DropDownList ID="ddlUserList" Width="100px" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Role">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; width: 100px; text-overflow: ellipsis; white-space: nowrap;">
                                                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divTaskTypeDetailDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divTaskTypeDetail">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComValidationAddTaskType" Display="none" class="alert alert-block alert-danger fade in" />
                                    <asp:CustomValidator ID="CuValAddTaskType" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComValidationAddTaskType" Display="None" />
                                    <asp:Label runat="server" ID="Label10" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Task Type</label>
                                        <asp:TextBox runat="server" ID="tbxTaskType" Style="width: 300px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Task type can not be empty."
                                            ControlToValidate="tbxTaskType" runat="server" ValidationGroup="ComValidationAddTaskType"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnAddTaskType" CssClass="btn btn-primary" OnClick="btnAddTaskType_Click"
                                            ValidationGroup="ComValidationAddTaskType" />
                                        <asp:Button Text="Close" runat="server" ID="Close" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divSubTaskTypeDetailDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 500px;">
            <div class="modal-content" style="width: 500px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <div id="divSubTaskTypeDetail">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <asp:ValidationSummary runat="server"
                                        ValidationGroup="ComValidationAddSubTaskType" Display="none" class="alert alert-block alert-danger fade in" />

                                    <asp:CustomValidator ID="CuValSubTaskType" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComValidationAddSubTaskType" Display="None" />
                                    <asp:Label runat="server" ID="Label11" ForeColor="Red"></asp:Label>
                                </div>
                                <div style="margin: 5px 5px 80px;">
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sub Task Type</label>
                                        <asp:TextBox runat="server" ID="tbxSubTaskType" Style="width: 300px;" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Sub task type can not be empty."
                                            ControlToValidate="tbxSubTaskType" runat="server" ValidationGroup="ComValidationAddSubTaskType"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center; margin-top: 5%;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:Button Text="Save" runat="server" ID="btnTaskSubType" CssClass="btn btn-primary" OnClick="btnTaskSubType_Click"
                                            ValidationGroup="ComValidationAddSubTaskType" />
                                        <asp:Button Text="Close" runat="server" ID="btnCloseSubTaskType" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
