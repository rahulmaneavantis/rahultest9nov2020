﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ActivatedEventNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ActivatedEventNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


<%--       <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

   
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />--%>
    
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />

    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
        .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }

        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
            border-bottom-width: 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            margin-left: 10px;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
            margin-left: 2px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

       .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        .k-grid tbody tr {
            height: 38px;
        }

        div.k-grid-header {
            margin-right: 1px;
        }
    </style>
    <script type="text/javascript">

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;


            if (locationlist.length > 0
                || ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventClassificationID", operator: "eq", value: parseInt($("#dropdownlistComplianceType1").val())
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }
        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            BindGrid();
        }
        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')

            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            //else if (div == 'filterstatus') {
            //    $('#' + div).append('Compliance Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            //    $('#ClearfilterMain').css('display', 'block');
            //}
            //else if (div == 'filterAct') {
            //    $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            //    $('#Clearfilter').css('display', 'block');
            //}


            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

    </script>
 
    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Activated Events');
            BindGrid();
            $("#txtSearchfilter").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "Name" || x.field == "UserName" || x.field == "EventDescription" || x.field == "CustomerBranchName") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });

                grid.dataSource.filter(filter);

            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                placeholder: "Select",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select",
                autoClose: true,
                dataSource: [
                    { text: "Secretarial", value: "1" },
                    { text: "Non Secretarial", value: "2" },
                ],
                index: 0,
                change: function (e) {
                    FilterAll();

                }
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        <%--                        read: "<% =Path%>Data/GetLocationList?UserId=<% =UId%>"--%>
                        read: {
                            url: "<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    //},
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

                });


        var record = 0;
        var Details = "";
        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>Data/ActivatedEventData?eventRoleID=10&UserID=<%=UId%>&CustomerID=<% =customerid%>",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/ActivatedEventData?eventRoleID=10&UserID=<%=UId%>&CustomerID=<% =customerid%>"
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        title: "Sr No",
                        template: "#= ++record #",
                        width: "58px",
                    },
                    {
                        field: "Name",
                        title: "Name",
                        width: "400px;",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        width: "200px",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "UserName",
                        title: "User",
                        width: "150px",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "EventDescription",
                        title: "Nature Of Event",
                        width: "180px",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" },
                        ], title: "Action", width: "3", lock: true,

                    }
                ]
            });


            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                width: 80,
                content: function (e) {
                    return "Edit Event";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "View Event";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                debugger;
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                Details = 'EventDetailKendo.aspx?eventId=' + item.ID + '&CustomerBranchID=' + item.CustomerBranchID + '&eventType=AciveEvent';
                OpenActivatedCompliancePopup();
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                debugger;
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Event/ViewPageActivated.aspx?ID=" + item.ID + '&Type=2&EventScheduledOnID=' + item.EventScheduledOnId + '&CustomerBranchID=' + item.CustomerBranchID + '&EventClassificationID=' + item.EventClassificationID + '&EventDescription=' + item.EventDescription);
                $('#divShowReminderDialog').modal('show');

            });

        }

        function BindGridApply(e) {
            BindGrid();
            e.preventDefault();
        }

        function OpenActivatedCompliancePopup() {
            $("#divwindow").kendoWindow({
                modal:true,
                width: "75pc",
                height: "37pc",
                position: {
                    top:20,
                },
                title: "Complaince Details",
                content: Details,
                visible: false,
                scrollable:false,
                actions: [
                  //  "Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
     <div id="divwindow" style="display:none;"></div>

    <div class="row Dashboard-white-widget">
            <div class="col-lg-12 col-md-12 ">
                <section class="panel">
                      <header class="panel-heading tab-bg-primary ">
                           
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liAssignedEvents" OnClick="liAssignedEvents_Click"  runat="server">Assigned Events</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liActiveEvents" CssClass="active"  OnClick="liActiveEvents_Click"  runat="server">Activated Events</asp:LinkButton>
                                                    </li>
                                          </ul>
  
                        </header>
                </section>
            </div>
        </div>
    <div id="example">

        <div class="row" style="margin-bottom:0.5%">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 200px;" />
            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 200px;" />
            <input id='txtSearchfilter' class='k-textbox' placeholder="Type to Filter" style="width: 250px;" />

            <button id="Applyfilter" style="float: right; margin-left: 1%; display: none;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Apply Filter</button>
            <button id="ClearfilterMain" style="float: right; margin-right: 30%; height: 24px; display: block; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
    </div>
    <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>

    <div id="grid" style="border: none;"></div>
    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 936px; margin-left: -172px; height: 690px;">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" scrolling="no" frameborder="0" runat="server" width="100%" height="300px" style="height: 631px;"></iframe>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
