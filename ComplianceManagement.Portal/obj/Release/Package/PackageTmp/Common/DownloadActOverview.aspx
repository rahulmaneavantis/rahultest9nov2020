﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadActOverview.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.DownloadActOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>


    <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
    <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
    <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />


    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>
    <style type="text/css">
        table#basic > tr, td {
            border-radius: 5px;
        }

        table#basic {
            border-collapse: unset;
            border-spacing: 3px;
        }

        .locationheadbg {
            background-color: #999;
            color: #fff;
            border: #666;
        }

        .locationheadLocationbg {
            background-color: #fff;
        }

        td.locationheadLocationbg > span.tree-icon {
            background-color: #1976d2 !important;
            padding-right: 12px;
            color: white;
        }

        .GradingRating1 {
            background-color: #8fc156;
        }

        .GradingRating2 {
            background-color: #ffc107;
        }

        .GradingRating3 {
            background-color: #ef9a9a;
        }

        .Viewcss {
            width: 50px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }

        .downloadcss {
            width: 100px;
            color: blue;
            text-align: center;
            cursor: pointer;
        }
    </style>
    <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .circle {
            width: 24px;
            height: 24px;
            border-radius: 59%;
            display: inline-block;
            margin-left: 8px;
        }
    </style>


    <script type="text/javascript">
        function ShowDownloadDocument() {
            $('#divDownloadDocument').modal('show');
            return true;
        };

        function ShowIntenalDownloadDocument() {
            $('#divInternalDownloadDocument').modal('show');
            return true;
        };

        function OpenDocumentDowmloadOverviewpup(ActID, ID) {
            $('#DownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        }

    </script>

    <script type="text/javascript">


        $(document).ready(function () {
            $('#basic').simpleTreeTable({
                collapsed: false
            });
        });



    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div class="modal fade" id="divDownloadDocument" style="bottom: auto; overflow: hidden;" tabindex="-1" role="dialog;" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 665px; margin-top: -27px; margin-left: -15px;">
                    <div class="modal-content">
                        <div class="modal-header">
                        </div>
                        <div class="modal-body">
                            <asp:Panel runat="server" ID="pnl" ScrollBars="Auto">
                                <div style="width: 90%; height: 300px; margin-top: 15px;">
                                    <%=ActDocString%>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 650px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DownloadViews" src="about:blank" width="535px" height="350px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <%--        <div class="modal fade" id="divActFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#divActFilePopUp').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table style="width: 100%; text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td style="vertical-align: top">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptActDocVersionView" runat="server" OnItemCommand="rptActDocVersionView_ItemCommand" OnItemDataBound="rptActDocVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblActViewDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' ID="lblActDocumentVersionView"
                                                                                    runat="server" ToolTip='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString() +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString() %>' Text='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString().Substring(0,4) +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblActDocumentVersionView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptActDocVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblMessageAct" runat="server" Style="color: red;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="IframeActFile" runat="server" width="100%" height="550px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </form>
</body>
</html>
