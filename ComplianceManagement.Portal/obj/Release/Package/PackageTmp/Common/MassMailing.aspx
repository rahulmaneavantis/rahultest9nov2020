﻿<%@ Page Title="Mass Email" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="MassMailing.aspx.cs" 
Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.MassMailing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
            right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upMassMailList" runat="server" UpdateMode="Conditional" OnLoad="upMassMailList_Load">
        <ContentTemplate>
          <div style="margin: 10px">
            <asp:RadioButtonList runat="server" ID="rdMailOption" RepeatDirection="Horizontal" AutoPostBack="true"
            RepeatLayout="Flow" OnSelectedIndexChanged="rdMailOption_SelectedIndexChanged">
            <asp:ListItem Text="Mass Email" Value="mass" Selected="True" />
            <asp:ListItem Text="Role Wise" Value="rolewise" />
        </asp:RadioButtonList>
          </div>
           <div style="margin-top: 40px;margin-left: 10px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="MassEmailingValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="MassEmailingValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divRole" runat="server" visible="false">
                        <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">
                            Role</label>
                        <asp:DropDownList runat="server" ID="ddlRole"  Style="padding: 0px; margin: 0px; height: 22px;
                            width: 200px;" CssClass="txtbox" AutoPostBack="false"  />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Role." ControlToValidate="ddlRole"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="MassEmailingValidationGroup"
                            Display="None" Visible="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">
                            Subject</label>
                        <asp:TextBox runat="server" ID="tbxSubject" Style="width: 80%;" MaxLength="100" ToolTip="Name"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxSubject"
                            runat="server" ValidationGroup="MassEmailingValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">
                            Attachment(s)</label>
                        <asp:FileUpload ID="fuAttachments" Multiple="Multiple" Style="width: 80%;" runat="server" />
                    </div>
                     <div style="margin-bottom: 7px">
                        <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">
                            Message Body</label>
                        <asp:TextBox ID="txtMessagebody" Style="width: 80%;" TextMode="multiline" Columns="50" Rows="20"
                                runat="server" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 202px;">
                        <asp:Button Text="Save" runat="server" ID="btnSend" CssClass="button" OnClick="btnSend_Click" 
                            ValidationGroup="MassEmailingValidationGroup" />
                        <asp:Button Text="Clear" runat="server" ID="btnCancel"  OnClick="btnClear_Click" CssClass="button" />
                    </div>
               </div>
        </ContentTemplate>
         <Triggers>
          <asp:PostBackTrigger ControlID="btnSend" />
         </Triggers>
</asp:UpdatePanel>

    <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rdMailOption.ClientID %>"));
    });

    function initializeRadioButtonsList(controlID) {

        $("#<%= rdMailOption.ClientID %>").buttonset();
    }
    function initializeCombobox() {
        $("#<%= ddlRole.ClientID %>").combobox();
    }

    </script>
</asp:Content>
