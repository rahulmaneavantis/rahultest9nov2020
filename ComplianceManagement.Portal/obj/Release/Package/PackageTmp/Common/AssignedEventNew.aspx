﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="AssignedEventNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.AssignedEventNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

    

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .k-widget.k-tooltip-validation {
            display: none !important;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }

        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
            border-bottom-width: 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            margin-left: 10px;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
            margin-left: 2px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        #grid .k-auto-scrollable {
            overflow: hidden;
        }

        #AssignedComplianceListgrid .k-auto-scrollable {
            overflow: hidden;
        }

        #gridNotAssigned .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        .k-grid tbody tr {
            height: 38px;
        }

        div.k-grid-header {
            margin-right: 0px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Assigned Events');
            BindGrid();
            $("#txtSearchfilter").on('input', function (e) {
                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;
                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field == "Name" || x.field == "CustomerBranchName") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
            });
            $("#dropdownlist").kendoDropDownList({
                placeholder: "Select",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select",
                autoClose: true,
                dataSource: [
                    { text: "Secretarial", value: "1" },
                    { text: "Non Secretarial", value: "2" }
                ],
                index: 0,
                change: function (e) {
                    FilterAll();
                }
            });
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    placeholder: "Entity/Sub-Entity/Location",
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                       //read: "<% =Path%>Data/GetLocationList?customerId=<% =customerid%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }); 
        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
            if (locationlist.length > 0
                || ($("#dropdownlist").val() != 0 && $("#dropdownlist").val() != -1 && $("#dropdownlist").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlist").val() != 0 && $("#dropdownlist").val() != -1 && $("#dropdownlist").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventClassificationID", operator: "eq", value: parseInt($("#dropdownlist").val())
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        var Details = "";

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
          
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')

            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '30px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        function OpenActivatedCompliancePopup() {
            $("#divwindow").kendoWindow({
                modal: true,
                width: "75pc",
                height: "37pc",
                position: {
                    top: 20,
                },
                title: "Compliance Details",
                content: Details,
                visible: false,
                scrollable: false,
                actions: [
                   // "Pin",
                   // "Minimize",
                    "Maximize",
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            return false;
        }

        var products = "";
        var EventID = [];
        var record = 0;

        function BindAssignedComplianceListFilters() {

            $("#tbxStartDate").kendoDatePicker({
                format: "dd-MM-yyyy"
            });

            $("#ddlFilterPerformer").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Performer",
                autoClose: true,
                change: function (e) {
                   // Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =customerid%>&Flags=" + false
                    },
                }
            });

            $("#ddlFilterReviewer").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Reveiwer",
                autoClose: true,
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =customerid%>&Flags=" + false
                    },
                }
            });

            $("#ddlFilterApprover").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Approver",
                autoClose: true,
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: "<% =Path%>Litigation/UserList?CutomerID=<% =customerid%>&Flags=" + true
                    },
                }
            });

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/Event/GetCheckdEvent?EventID=' + EventID.toString(),
                success: function (json) {
                    if (json.message == "No NotComplianceAssigned") {
                        $("#ddlEvent").kendoDropDownList({
                            dataTextField: "Name",
                            dataValueField: "ID",
                            optionLabel: "Select Event",
                            autoClose: true,
                            change: function (e) {
                            },
                            dataSource: json.status
                        });
                    }
                },
                failure: function (json) {
                }
            });

        }

        var dataSource = new kendo.data.DataSource({
            
            transport: {

                read: {
                    url: "<% =Path%>Data/AssignedEventData?eventRoleID=10&UserId=<%=UId%>&CustID=<% =customerid%>",
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },

                update: {
                    
                    //url: "/Event/CheckAllNotAssignedComplinceListForEvent",
                    url: "/Event/SaveAllActivatedEvent",                       
                },

                create: {
                },
                destroy: {
                }
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "EventInstanceID",
                    fields: {
                        CustomerBranchName: { editable: false },
                        Name: { editable: false },
                        ActivateDate: { type: "date" },
                        NatureOfEventID: { type: "text" },
                    }
                }
            },
            requestEnd: onRequestEnd
        });

        function onRequestEnd(e) {
            debugger
            if (e.type == "create") {
                e.sender.read();
            }
            else if (e.type == "update") {
                debugger
                e.sender.read();
                alert("Event Activated Successfully");
            }
        }

        function editAll() {
           
            var theGrid = $("#grid").data("kendoGrid");
            $("#grid tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#grid").focus();
        }
       

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                },
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: true,
               // editable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                dataBound: function () {
                    editAll();
                },
                columns: [
                    {
                        selectable: true,
                        width: "30px"
                    },
                    {
                        title: "Sr",
                        template: "#= ++record #",
                        width: "40px",
                    },
                    {
                        field: "Name",
                    
                        title: "Name",
                        width: "22%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                       
                    },
                    {
                        field: "CustomerBranchName",
                        type: { editable: "false" },
                        title: "Location",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    //{ template: "<input type='search' id='NatureOfEventID' name='NatureOfEventID' required validationMessage='Enter Nature Of Event' class='k-textbox' />", title: "Nature Of Event", width: "15%", },
                                                                                                                                                                                                                     

                    { template: "<input data-bind='value:NatureOfEventID'  type='text' class='k-textbox' />", title: "Nature Of Event", width: "15%", },
                    {
                        template: "<input data-bind='value:ActivateDate'  type='date' class='k-textbox'/>", title: "Activate Date", width: "15%"
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" },
                        ], title: "Action", width: "10%", lock: true,

                    },
                ],
               // editable: "inline" ,
            //    editable: true,
            //    dataSource: dataSource,
            //schema: {
            //        model: {
            //            fields: {
            //                CustomerBranchName: { editable: false },
            //                Name: { editable: false }
            //            }
            //        }
            //}

            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                debugger;
                kendo.ui.progress($(".chart-loading"), true);

                setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
            
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                if (item.ActivateDate != undefined &&
                    item.ActivateDate != null
                    && item.NatureOfEventID != undefined &&
                    item.NatureOfEventID != null)
                {
                    var items = [];
                    items.push(item);

                    items = JSON.stringify({ 'ActivatedEvents': items });

                    $.ajax({
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: '/Event/CheckAllNotAssignedComplinceListForEvent',
                        data: items,
                        success: function (json) {
                            if (json.message == "NotComplianceAssigned") {
                                debugger;
                                OpenNoAssignedCompliancePopup();
                                products = json.status;
                                EventID = json.EventID;
                                BindNotAssignedComplianceList();
                            }
                            else {
                                $.ajax({
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    type: 'POST',
                                    url: '/Event/SaveAllActivatedEventNew',
                                    data: items,
                                    success: function (json) {
                                        
                                        if (json.message == "Successfully Activated") {
                                            alert("Event Activated Successfully");
                                        }
                                        $('#grid').data('kendoGrid').dataSource.read();

                                        $('#grid').data('kendoGrid').refresh();
                                        $('#grid').data('kendoGrid')._selectedIds = {};
                                        $('#grid').data('kendoGrid').clearSelection();
                                    },
                                    failure: function (json) {
                                    }
                                });
                            }

                        },
                        failure: function (json) {
                        }
                    });
                }
                else {
                    alert('please select activate date and Nature Of Event');
                }
            });
        
            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                debugger;
                kendo.ui.progress($(".chart-loading"), true);

                setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                Details = 'EventDetailKendo.aspx?eventId=' + item.ID + '&CustomerBranchID=' + item.CustomerBranchID + '&eventType=AssignedEvent';
                OpenActivatedCompliancePopup();
             
            });
            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Activate";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "View Event";
                }
            });

        }

        function close_windows() {
            $('div.k-window').each(function (index) {
                $(this).hide();
            });
        };

        function btn_ApplyAll() {
            debugger;

           

            setTimeout(function () { kendo.ui.progress($(".chart-loading"), false); }, 3000);
            var grid = $('#grid').data("kendoGrid");
            var rows = $('#grid :checkbox:checked');

            var items = [];
            $.each(rows, function () {
                var item = grid.dataItem($(this).closest("tr"));
                items.push(item);
            });

            if (items.length > 0)
            {
                kendo.ui.progress($(".chart-loading"), true);

                items = JSON.stringify({ 'ActivatedEvents': items });

                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: '/Event/CheckAllNotAssignedComplinceListForEvent',
                    data: items,
                    success: function (json) {
                        if (json.message == "NotComplianceAssigned") {
                            debugger;
                            OpenNoAssignedCompliancePopup();
                            products = json.status;
                            EventID = json.EventID;
                            BindNotAssignedComplianceList();
                        }
                        else {
                            $.ajax({
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                type: 'POST',
                                url: '/Event/SaveAllActivatedEventNew',
                                data: items,
                                success: function (json) {
                                    //$("#dvdforum").show();
                                    //$('#lblforum').text(json.message);
                                    alert(json.message);
                                    $('#grid').data('kendoGrid').dataSource.read();

                                    $('#grid').data('kendoGrid').refresh();
                                    $('#grid').data('kendoGrid')._selectedIds = {};
                                    $('#grid').data('kendoGrid').clearSelection();
                                },
                                failure: function (json) {
                                }
                            });
                        }

                    },
                    failure: function (json) {
                    }
                });
            }
            else {
                alert("Please select at Least one event");
            }
        }

        function OpenNoAssignedCompliancePopup() {
            debugger;
            $("#divCompliancewindow").kendoWindow({
                //modal: true,
                width: "73pc",
                height: "40pc",
                title: "Compliance",
                position: {
                    top: 20,
                },
                visible: false,
                actions: [
                  //  "Pin",
                  //  "Minimize",
                    "Maximize",
                    "Close"
                ]

            }).data("kendoWindow").open().center();

        }

        function BindNotAssignedComplianceList() {
            debugger;
            var grid = $('#gridNotAssigned').data("kendoGrid");

            if (grid != undefined || grid != null)
                $('#gridNotAssigned').empty();

            var gridNotAssigned = $("#gridNotAssigned").kendoGrid({
                dataSource: {
                    data: products,
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        title: "ID",
                        field: "ID",
                        width: "8%",
                    },
                    {
                        field: "EvnetName",
                        title: "Event Name",
                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        editable : false
                    },
                    {
                        field: "ComplianceName",
                        title: "Compliance Name",
                    }
                ]

            });

            $("#gridNotAssigned").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#gridNotAssigned").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "top",
                width: 250,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

        }

        function btnComplianceList_Click(e) {

            $("#divAssignedComplianceList").kendoWindow({
                //modal: true,
                width: "75pc",
                height: "37pc",
                position: {
                    top: 20,
                },
                title: "Assign Complaince",
                visible: false,
                scrollable: false,
                actions: [
                  //  "Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            BindAssignedComplianceListFilters();
            BindAssignedComplianceList();
        }

        function BindAssignedComplianceList() {
            debugger;
            var record = 0;
            var gridNew = $('#AssignedComplianceListgrid').data("kendoGrid");
            $('#AssignedComplianceListgrid').empty();
            if (gridNew != undefined || gridNew != null)
                $('#AssignedComplianceListgrid').empty();

            var gridNew = $("#AssignedComplianceListgrid").kendoGrid({
                dataSource: products,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                persistSelection: true,
                multi: false,
                editable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "Assign",
                        selectable: true,
                        width: "5%"
                    },
                    {
                        title: "Sr No",
                        template: "#= ++record #",
                        width: "5%",
                    },
                    {
                        title: "ID",
                        field: "ID",
                        width: "8%",
                    },
                    {
                        field: "EvnetName",
                        title: "Event Name",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CustomerBranchName",
                        title: "Location",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceName", title: "Compliance Name"
                    }
                ]

            });
        }

        function SaveComplianceList_Click() {
            debugger;
            var grid = $('#AssignedComplianceListgrid').data("kendoGrid");
            var rows = $('#AssignedComplianceListgrid :checkbox:checked');
            var items = [];
            $.each(rows, function () {
                debugger;
                var item = grid.dataItem($(this).closest("tr"));
                item.tbxStartDate = $("#tbxStartDate").val();
                item.ddlFilterPerformer = $("#ddlFilterPerformer").val();
                item.ddlFilterReviewer = $("#ddlFilterReviewer").val();
                item.ddlFilterApprover = $("#ddlFilterApprover").val();
                items.push(item);
            });

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: '/Event/SaveComplianceList',
                data: JSON.stringify(items),
                //data: items,
                success: function (json) {
                    if (json.message == "ComplianceList Saved") {
                        debugger;
                        alert("Successfully Updated");
                        $("#btncloseall").click();
                        $("#ApplyAll").click();
                        $("#tbxStartDate").val('');
                        $("#ddlFilterPerformer").data("kendoDropDownList").select(0);
                        $("#ddlFilterReviewer").data("kendoDropDownList").select(0);
                        $("#ddlFilterApprover").data("kendoDropDownList").select(0);
                    }
                    else {
                        alert("Error in Assigning Compliance");
                    }
                },
                failure: function (json) {
                }
            });
        }
        
        $(document).ready(function () {
            var divAssignedComplianceList = $("#divAssignedComplianceList").kendoValidator().data("kendoValidator");

            $("#SaveComplianceList").click(function () {

                var ddlFilterPerformer = $('#ddlFilterPerformer').data("kendoDropDownList");

                var ddlFilterReviewer = $('#ddlFilterReviewer').data("kendoDropDownTree");

                var ddlFilterApprover = $('#ddlFilterApprover').data("kendoDropDownList");

                var ddlEvent = $('#dropdownCaseType').data("kendoDropDownList");

                if (ddlFilterPerformer.value() == "") {
                    ddlFilterPerformer.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlFilterReviewer.value() == "") {
                    ddlFilterReviewer.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlFilterApprover.value() == "0") {
                    ddlFilterApprover.wrapper.find(".k-input").css("border", "1px solid red");
                }
                if (ddlEvent.value() == "0") {
                    ddlEvent.wrapper.find(".k-input").css("border", "1px solid red");
                }

                if (divAssignedComplianceList.validate() && ddlFilterPerformer.value() != ""
                    && ddlFilterReviewer.value() != ""
                    && ddlFilterApprover.value() != "0" && ddlEvent.value() != "0") {
                    SaveComplianceList_Click();
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
   
   
    <div id="divwindow"></div>
<%--              $('<div class="k-loading-mask" style="width: 100%; height: 100%; top: 0px; left: 0px;"><span class="k-loading-text">Loading...</span><div class="k-loading-image"></div><div class="k-loading-color"></div></div>').appendTo('#divwindow .k-grid-content');--%>

   
    <div id="btncloseall" onclick="close_windows()" style="display: none;"></div>
    <div class="row Dashboard-white-widget">
        <div class="col-lg-12 col-md-12 ">
            <section class="panel">
                      <header class="panel-heading tab-bg-primary ">
                           
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liAssignedEvents" CssClass="active" OnClick="liAssignedEvents_Click"  runat="server">Assigned Events</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liActiveEvents" OnClick="liActiveEvents_Click"  runat="server">Activated Events</asp:LinkButton>
                                                    </li>
                                          </ul>
  
                        </header>
                </section>
        </div>
    </div>

    <input type="text" id="products" runat="server" style="display: none;" />
    <div id="divCompliancewindow" style="display: none;">
        <div style="margin: 5px; color: red; font-size: 17px; font-weight: bold;">
            Listed compliance are not assigned to performer and reviewer, kindly assign to process further for activation. Once assignmemt completed please click save at main page.
        </div>
        <div id="gridNotAssigned" style="border: none;"></div>
        <button style="width: 166px; margin-top: 1%;" id="btnComplianceList" class="btn btn-search" onclick="btnComplianceList_Click()">Assign Compliance</button>
    </div>
    <div id="example">
        <div class="row">
            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 200px;" />
            <input id="dropdownlist" data-placeholder="Type" style="width: 150px;" />
            
            <input id='txtSearchfilter' type="text" class='k-textbox' placeholder="Type to Filter" style="width: 220px" onkeydown="return (event.keyCode!=13);"/>
                                 <button id="ApplyAll" style="float: right; margin-left: 0.3%; height: 25px;" onclick="btn_ApplyAll(event)"><span onclick="javascript:return false;"></span>Save</button>

        </div>
       <%--  <div class="row" style="margin-bottom: 4px;margin-top: 5px;">
           <button id="ApplyAll" style="float: right; margin-left: 0.3%; height: 25px;" onclick="btn_ApplyAll(event)"><span onclick="javascript:return false;"></span>Save</button>
 
           <div style="float:right"> 
          <input runat="server" id='txtnatureeventfilter' class='k-textbox' placeholder="Nature of Event" style="width: 195px;float: left; margin-right:46px" onkeydown="return (event.keyCode!=13);" />

            <input runat="server" id="Startdatepicker"  placeholder="Activate Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 200px; margin-right: 199px;" />
            
             </div>
        </div>--%>
        <div class="row" style="margin-bottom: 5px; margin-top: 5px;">
            <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
        <div id="grid" style="border: none;"></div>
     
    </div>


    <div id="divAssignedComplianceList" style="display: none;">
        <div class="row" style="margin-bottom: 0.5%">
            <input id="ddlFilterPerformer" required data-placeholder="Performer" style="width: 200px;" />
            <input id="ddlFilterReviewer" required data-placeholder="Reviewer" style="width: 150px;" />
            <input id="ddlFilterApprover" required placeholder="Approver" style="width: 220px" />
            <input id="tbxStartDate" required placeholder="Start Date" style="width: 220px" />
            <input id="ddlEvent" required placeholder="Event Name" style="width: 220px" />
        </div>

        <div id="AssignedComplianceListgrid"></div>
        <button style="width: 170px; margin-top: 1%;" id="SaveComplianceList" class="btn btn-search" onclick="SaveComplianceList_Click()">Save</button>
    </div>
                <div class="chart-loading"></div>
</asp:Content>
