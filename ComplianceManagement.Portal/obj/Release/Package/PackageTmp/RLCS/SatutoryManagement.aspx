﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SatutoryManagement.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.SatutoryManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
         #grdSummaryDetails {
                table-layout: fixed;
                text-overflow: ellipsis;
                width:100%;
                overflow:hidden;
                white-space:nowrap;
            }
            td {
                max-width: 100px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
            .branch{
                width:98px;
            }
            .duedate{
                width:84px;
            }
            .act{
                width: 254px;
            }   
            .compliance{
                width:285px;
            }
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        /*.table tbody > tr > td {
            padding: 2px;
            line-height: 1.428571429;
            vertical-align: middle;
        }*/
            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
        padding: 2px;
        line-height: 1.428571429;
        vertical-align: middle;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
        
        .panel-heading{
            padding-top:0px;
        }
        .table{
            margin-bottom:0px;
        }
         #grdSummaryDetails {
         table-layout: fixed;
          text-overflow: ellipsis;
          width:100%;
           overflow:hidden;
           white-space:nowrap;
        }
          td {
        max-width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
     .action{
             width: 50px;
     }   
    </style>

    <script type="text/javascript">

        function fopendocfileReview(file) {
            $('#divViewDocument').modal('show');
            $('#docViewerStatutory').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };


        function fComplianceOverview(obj) {
            OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
        }

        function SelectheaderCheckboxes(headerchk, gridname) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");
            var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");
        var i;
        if (headerchk.checked) {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }
        else {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }
    }

    function Selectchildcheckboxes(header, gridname) {
        var i;
        var count = 0;
        var rolecolumn;

        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");

        var headerchk = document.getElementById(header);
        var chkheaderid = header.split("_");

        var rowcount = gvcheck.rows.length;

        for (i = 1; i < gvcheck.rows.length - 1; i++) {
            if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                count++;
            }
        }

        if (count == gvcheck.rows.length - 2) {
            headerchk.checked = true;
        }
        else {
            headerchk.checked = false;
        }
    }

    function DownloadDoc() {
        var count = 0;
        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");
         for (var i = 1; i < gvcheck.rows.length; i++) {
             if (gvcheck.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked)
                 count++;
         }
         if (count > 0)
             return true;
         else {
             alert("please select at least one record to download.");
             return false;
         }
     }
     $(document).ready(function () {
         $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
         // window.parent.forchild($('#grdSummaryDetails').find('tr').length);
         //window.parent.loaderhide();
     });

     function initializeJQueryUI(textBoxID, divID) {
         $("#" + textBoxID).unbind('click');

         $("#" + textBoxID).click(function () {
             $("#" + divID).toggle("blind", null, 500, function () { });
         });
     }

    </script>

     <script>
         $(document).ready(function () {
             $("#grdSummaryDetails").css('margin-bottom', '0px');
         });
            function OpenOverViewpup(scheduledonid, instanceid) {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1090px');
                $('#OverViews').attr('height', '330px');
                $('.modal-dialog').css('width', '1150px');
                $('#OverViews').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }

           // window.parent.loaderhide();
        </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div id="divSummaryDetails" style="margin-top: 15px;">

            <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                <ContentTemplate>
                    <asp:UpdateProgress ID="updateProgress" runat="server">
                        <ProgressTemplate>
                            <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <div class="col-md-12">
                        <div class="col-md-1">
                            <%--<div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show</p>
                                </div>--%>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <%--<asp:ListItem Text="5" Selected="True" />--%>
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>

                        <div class="col-md-3">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" CssClass="txtbox" autoComplete="off"
                                Style="padding: 0px; padding-left: 10px; margin: 0px; height: 34px; width: 98%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">                            
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="92%" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; margin-top: -22px; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                        <div class="col-md-1 colpadding0">
                            <asp:DropDownList runat="server" ID="ddlrisk" class="form-control m-bot15" Style="width: 80px; float: left">
                                <asp:ListItem Text="Risk" Value="-1" />
                                <asp:ListItem Text="High" Value="High" />
                                <asp:ListItem Text="Medium" Value="Medium" />
                                <asp:ListItem Text="Low" Value="Low" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:DropDownList runat="server" ID="ddlMonth" class="form-control m-bot15" Style="width: 182px; float: left">
                                <asp:ListItem Text="Month" Value="-1"/>
                                <asp:ListItem Text="January" Value="01"/>
                                <asp:ListItem Text="February" Value="02"/>
                                <asp:ListItem Text="March" Value="03"/>
                                <asp:ListItem Text="April" Value="04"/>
                                <asp:ListItem Text="May" Value="05"/>
                                <asp:ListItem Text="June" Value="06"/>
                                <asp:ListItem Text="July" Value="07"/>
                                <asp:ListItem Text="August" Value="08"/>
                                <asp:ListItem Text="September" Value="09"/>
                                <asp:ListItem Text="October" Value="10"/>
                                <asp:ListItem Text="November" Value="11"/>
                                <asp:ListItem Text="December" Value="12"/>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-1 colpadding0">
                            <asp:DropDownList runat="server" ID="ddlYear" class="form-control m-bot15" Style="width: 98px; float: left">
                                <asp:ListItem Text="Year" Value="-1"/>
                               <asp:ListItem Text="2009" Value="2009"/>
                                <asp:ListItem Text="2010" Value="2010"/>
                                <asp:ListItem Text="2011" Value="2011"/>
                                <asp:ListItem Text="2012" Value="2012"/>
                                <asp:ListItem Text="2013" Value="2013"/>
                                <asp:ListItem Text="2014" Value="2014"/>
                                <asp:ListItem Text="2015" Value="2015"/>
                                <asp:ListItem Text="2016" Value="2016"/>
                                <asp:ListItem Text="2017" Value="2017"/>
                                <asp:ListItem Text="2018" Value="2018"/>
                                <asp:ListItem Text="2019" Value="2019"/>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList runat="server" ID="ddlStatus" class="form-control" Width="100%">
                            </asp:DropDownList>
                        </div>

                        <div class="col-md-2"style="padding-left: 0px; padding-right: 0px;">
                            <div class="col-md-6 text-left">
                                <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                            </div>
                            <div class="col-md-6 text-right">
                                <asp:LinkButton runat="server" CssClass="btn" 
                                    Style="background: url('../images/Excel _icon.png'); width: 47px; height: 33px;" ID="lbtnExportExcel"
                                    toggle="tooltip" data-placement="bottom" Title="Export All to Excel" ToolTip="Export All to Excel" OnClick="lbtnExportExcel_Click">          
                                </asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-md-2"></div>

                        
                        
                        <div class="col-md-1">
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 AdvanceSearchScrum" style="display:none;">
                        <div id="divAdvSearch" runat="server">
                            <p>
                                <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                                <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
                            </p>
                        </div>

                        <div runat="server" id="DivRecordsScrum" style="float: right; display: none;">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                        <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
                    </div>

                    <div class="col-md-12">
                        <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                            OnRowCommand="grdSummaryDetails_RowCommand" OnPageIndexChanging="grdSummaryDetails_PageIndexChanging"
                            CellPadding="4" CssClass="table" AllowPaging="true" PageSize="10" Width="100%" OnRowDataBound="grdSummaryDetails_RowDataBound"
                            DataKeyNames="ComplianceInstanceID" ShowHeaderWhenEmpty="true" >
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCompliances" runat="server" OnCheckedChanged="chkCompliances_CheckedChanged" AutoPostBack="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                               <%-- <asp:TemplateField HeaderText="Sr">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <%-- <asp:BoundField DataField="Branch" HeaderText="Location"  />--%>

                                <asp:TemplateField HeaderText="Branch"  ItemStyle-CssClass="branch" HeaderStyle-CssClass="branch">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:130px;">
                                            <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" 
                                                data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Act" ItemStyle-CssClass="act" HeaderStyle-CssClass="act">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                            <asp:Label ID="lblact" runat="server" data-toggle="tooltip" 
                                                data-placement="bottom" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Compliance" ItemStyle-CssClass="compliance" HeaderStyle-CssClass="compliance">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;  width: 90px;">
                                            <asp:Label ID="Label2" runat="server" data-toggle="tooltip" 
                                                data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Form">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                            <asp:Label ID="lblRequiredForms" runat="server" data-toggle="tooltip" 
                                                data-placement="bottom" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Frequency">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                            <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip" 
                                                data-placement="bottom" Text='<%# Eval("TextFrequency") %>' ToolTip='<%# Eval("TextFrequency") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Performer" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID")) %>' ToolTip='<%#Performername%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Reviewer" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>' ToolTip='<%#Reviewername%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Due Date" ItemStyle-CssClass="duedate" HeaderStyle-CssClass="duedate">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Period">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                            <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="RiskCategory" HeaderText="Risk" />

                                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" ImageAlign="Middle"
                                                    CommandArgument='<%# Eval("ScheduledOnID") +","+ Eval("ComplianceID") %>' ToolTip="Download" Visible='<%# ViewDownload((int)Eval("ComplianceStatusID")) %>'></asp:ImageButton>
                                                <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/Images/view-doc.png" CommandName="View" Visible='<%# ViewDownload((int)Eval("ComplianceStatusID")) %>'
                                                    CommandArgument='<%# Eval("ScheduledOnID")+","+ Eval("ComplianceID") %>' ToolTip="View" ImageAlign="Middle"></asp:ImageButton>
                                                <asp:ImageButton ID="lblOverView" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                                    OnClientClick='fComplianceOverview(this)' ToolTip="Overview" ImageAlign="Middle"></asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                <asp:PostBackTrigger ControlID="lblViewfile" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0">
                            <div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 15px;"></asp:Label>
                                    </p>
                                </div>
                                <asp:Button Text="Download" CssClass="btn btn-search" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click Here to Download Document(s) of Selected Compliances."
                                    OnClientClick="javascript:return DownloadDoc();" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
                            </div>
                        </div>

                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 0px; margin-top: 15px;margin-right:10px; "> <%--margin-right: 40px;--%>
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>

                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>

            <div>
                <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 100%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 25%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                    OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>Versions</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersionView"
                                                                                    runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 510px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none; padding: 0px 7px; min-height: 0.43px; height: 20px;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">

                                <iframe id="OverViews" src="blank.html" width="1200px" height="100%" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

         <script>
             //$('#updateProgress1').show();

             //$(document).ready(function () {
             //    $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
             //    $('#updateProgress1').hide();
             //});

             function HideLoaderDiv()
             {
                 // $('#updateProgress1').hide();
                 window.parent.loaderhide();
             }
         </script>
    </form>
</body>
</html>
