﻿<%@ Page Title="Entity/Branch Assignment :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityAssignment_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EntityAssignment_New" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
    </style>
        
    <script type="text/javascript">       
         $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
         });

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvBranches.ClientID %>') > -1) {
                    $("#divBranches").show();
                } else {
                    $("#divBranches").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvBranches.ClientID %>') > -1) {
                $("#divBranches").show();
            } else if (event.target.id != '<%= tbxBranch.ClientID %>') {
                $("#divBranches").hide();
            } else if (event.target.id == '<%= tbxBranch.ClientID %>') {
                $('<%= tbxBranch.ClientID %>').unbind('click');

                $('<%= tbxBranch.ClientID %>').click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
        });

        $(document).ready(function () {
            fhead('Masters/Entity-Branch Assignment');           
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.25;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" Visible="false" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

     <div class="row colpadding0" style="margin-bottom: 10px">
        <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
            <ul class="nav nav-tabs">
                <li>
                    <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                </li>
                <li class="active">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                </li>
            </ul>
        </header>
    </div>

    <div class="clearfix"></div>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row col-md-12 colpadding0">
                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlCustomer" Width="95%"
                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AllowSingleDeselect="false">
                    </asp:DropDownListChosen>
                </div>
                
                <div class="col-md-3 colpadding0" id="FilterLocationdiv" runat="server">
                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" CssClass="form-control" PlaceHolder="Entity/Branch" Width="95%" />
                    <div id="divFilterLocation" style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%; margin-top:-20px">
                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>

                <div class="col-md-3 colpadding0" runat="server" id="divFilterUsers">
                    <asp:DropDownListChosen runat="server" ID="ddlFilterUsers" Width="95%"
                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged" AllowSingleDeselect="false">
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0" style="text-align: right;">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddNew" class="btn btn-primary" OnClientClick="$('#divAssignEntitiesDialogpopup').modal('show');" OnClick="btnAddNew_Click" />
                </div>
            </div>

            <div class="row">
                <div runat="server" id="performerdocuments" class="col-md-12 colpadding0">
                    <asp:GridView runat="server" ID="grdAssignEntities" AutoGenerateColumns="false" GridLines="None" CssClass="table" AllowSorting="true" OnRowCreated="grdAssignEntities_RowCreated"
                        PageSize="10" Width="100%" OnSorting="grdAssignEntities_Sorting" AllowPaging="true"
                        DataKeyNames="ID" OnPageIndexChanging="grdAssignEntities_PageIndexChanging" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No." SortExpression=" ">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entity/Location/Branch" SortExpression="Branch">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="UserName" HeaderText="User" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No entity/branch assignment record(s) found or all records are filtered out
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="record1" runat="server" class="col-md-12 colpadding0">
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    <div class="col-md-6 colpadding0">
                        <div runat="server" id="DivRecordsScrum">
                            <p style="padding-right: 0px !Important; color: #666;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="table-paging" runat="server" id="DivnextScrum" style="margin-bottom: 20px">
                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-1" style="padding-left: 0px;">
                <label for="ddlPageSize" class="hidden-label">Show</label>
                <asp:DropDownList runat="server" Visible="false" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                    class="form-control">
                    <asp:ListItem Text="5" />
                    <asp:ListItem Text="10" Selected="True" />
                    <asp:ListItem Text="20" />
                    <asp:ListItem Text="50" />
                </asp:DropDownList>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
  
    <div class="modal fade" id="divAssignEntitiesDialogpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog w35per">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" style="font-size: large;">
                        Assign Entity/Branch to User</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upCompliance" runat="server" style="padding-top: 25px;" UpdateMode="Conditional" OnLoad="upCompliance_Load">
                        <ContentTemplate>
                            <div class="row col-md-12-colpadding0">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-danger fade in"
                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                            </div>

                            <div class="row form-group col-md-12">
                                <div class="col-md-2 colpadding0">
                                    <label class="control-label">User</label>
                                </div>
                                <div class="col-md-10 colpadding0">
                                    <asp:DropDownListChosen ID="ddlUsers" runat="server" Width="100%" CssClass="form-control" AllowSingleDeselect="false" DataPlaceHolder="Select">
                                    </asp:DropDownListChosen>
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Select User" ControlToValidate="ddlUsers"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                        Display="None" />
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="row form-group col-md-12">
                                        <div class="col-md-2 colpadding0">
                                            <label class="control-label">Entity/Branch</label>
                                        </div>
                                        <div class="col-md-10 colpadding0">
                                            <asp:TextBox runat="server" ID="tbxBranch" CssClass="form-control" />
                                            <div style="position: absolute; z-index: 10; margin-top: -20px; width: 100%;" id="divBranches">
                                                <asp:TreeView runat="server" ID="tvBranches" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" 
                                                    ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                                </asp:TreeView>
                                            </div>
                                            <asp:CompareValidator ID="CompareValidator2" ControlToValidate="tbxBranch" ErrorMessage="Select Entity/Branch"
                                                runat="server" ValueToCompare="tbxBranch" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                                Display="None" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="row col-md-12 colpadding0" style="text-align: center;">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" class="btn btn-primary" Style="margin-top: 24px;"
                                    ValidationGroup="ComplianceInstanceValidationGroup" />
                            </div>

                            <div class="row col-md-12 colpadding0">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function ClosePopupWindowAfterSave() {
            setTimeout(function () {document.querySelector('.close').click();}, 3000);
        }
    </script>
</asp:Content>
