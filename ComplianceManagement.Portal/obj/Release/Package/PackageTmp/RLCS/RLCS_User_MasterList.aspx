﻿<%@ Page Title="User Master" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_User_MasterList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_User_MasterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title></title>

    <style>
        .k-grid-content {
            min-height: 50px;
        }

        .k-grid td {
            line-height: 2.0em;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        function checkUserActive(value) {            
            if (value=true) {
                return "Active";
            } else if (value=false) {
                return "In-Active";
            }
        }

        function BindUserGrid() {
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 50,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAll_UserMaster?customerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            if (response != undefined)
                                return response.Result;
                        },
                        total: function (response) {
                            if (response != undefined)
                                return response.Result.length;
                        }
                    }
                },
                //toolbar: kendo.template($("#template").html()),
                //height: 471,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    // pageSizes: true,
                    //pageSizes: [ 10 , 25, 50 ],
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID", title: "UserID" },

                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "FirstName", title: 'First Name',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LastName", title: 'Last Name',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Email", title: 'Email',
                        width: "20%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ContactNumber", title: 'Contact No',
                        width: "13%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center; '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Role", title: 'Role',
                        width: "13%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "IsActive", title: 'Active',
                        width: "10%",
                        template: '#:checkUserActive(IsActive)#',
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }

                    }
                    //{
                    //    command: [
                    //     { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "ob-download",click: Edit },
                    //        //{ name: "Upload", text: "", iconClass: "k-icon k-i-upload", className: "ob-download" },
                    //        //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                    //    ], title: "Action", lock: true,width: "12.7%;"
                    //}
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        $(document).ready(function () {
            BindUserGrid();

            $("#btnSearch").click(function () {
                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "LastName", operator: "contains", value: $x },
                              { field: "FirstName", operator: "contains", value: $x },
                              { field: "Role", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    BindUserGrid();
                }
                // return false;
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server" PostBackUrl="~/RLCS/RLCS_EntityLocation_Master.aspx">Entity\Location\Branch</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabStateCity" runat="server" PostBackUrl="~/RLCS/RLCS_Location_Master.aspx">State-City</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabDesignation" runat="server" PostBackUrl="~/RLCS/RLCS_Designation_Master.aspx">Designation</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabLeaveType" runat="server" PostBackUrl="~/RLCS/RLCS_LeaveType_Master.aspx">Leave Type</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
        </div>
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-6 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 70%" placeholder="Type Text to Search..." />
                <button id="btnSearch" class="btn btn-primary">Search</button>
            </div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
