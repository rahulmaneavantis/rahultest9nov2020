﻿<%@ Page Title="My Payments" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_MyPayments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <script src="../Newjs/moment.min.js"></script>

    <style>
        .k-grid-content {
            min-height: 100px;
            /*max-height: 350px;*/
            overflow-y: hidden;
        }
    </style>

    <style>
        .tab-bg-primary {
            background: none;
            border-bottom: none;
        }

        .panel-heading .nav > li > a {
            font-size: 17px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Payments');            

           <%-- if("<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role%>"!=="SPADM"){                
                var grid = $("#gridPaymentHistory").data("kendoGrid");
                if(grid!=null&&grid!=undefined){
                    if(grid.columns!=null && grid.columns!=undefined)        
                        grid.hideColumn(grid.columns.length-1);
                }
            }--%>

            $('#PaymentDetails').hide();

            $('#divProceedPayment').hide();

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            $("#btnExcel_Deductions").click(function (e) {
                ExportToExcel_Deductions(e);
            });

            $("#btnExcel_Business").click(function (e) {
                ExportToExcel_ConsolidatedBusiness(e);
            });
        });

        function ClearFilterPayment(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlMode").data("kendoDropDownTree").value([]);

            var txnDateFrom=$("#txnDateFrom").data("kendoDatePicker");

            if(txnDateFrom!=null && txnDateFrom!=undefined){
                txnDateFrom.value(null);
                txnDateFrom.trigger("change");
            }

            var txnDateTo=$("#txnDateTo").data("kendoDatePicker");

            if(txnDateTo!=null && txnDateTo!=undefined){
                txnDateTo.value(null);
                txnDateTo.trigger("change");
            }
           
            $('#btnClearFilterPayment').css('display', 'none');   
            
            return false;
        }

        function ClearFilterDeduction(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlCustomers").data("kendoDropDownTree").value([]);

            var FromMonth_Deduction=$("#FromMonth_Deduction").data("kendoDatePicker");

            if(FromMonth_Deduction!=null && FromMonth_Deduction!=undefined){
                FromMonth_Deduction.value(null);
                FromMonth_Deduction.trigger("change");
            }

            var ToMonth_Deduction=$("#ToMonth_Deduction").data("kendoDatePicker");

            if(ToMonth_Deduction!=null && ToMonth_Deduction!=undefined){
                ToMonth_Deduction.value(null);
                ToMonth_Deduction.trigger("change");
            }
           
            $('#btnClearFilterDeduction').css('display', 'none');   
            
            return false;
        }

        function ClearFilterBusiness(e) {
            e.preventDefault();
            //e.stopPropogation();
            $("#ddlDistributors").data("kendoDropDownTree").value([]);

            var FromMonth_Business=$("#FromMonth_Business").data("kendoDatePicker");

            if(FromMonth_Business!=null && FromMonth_Business!=undefined){
                FromMonth_Business.value(null);
                FromMonth_Business.trigger("change");
            }

            var ToMonth_Business=$("#ToMonth_Business").data("kendoDatePicker");

            if(ToMonth_Business!=null && ToMonth_Business!=undefined){
                ToMonth_Business.value(null);
                ToMonth_Business.trigger("change");
            }
           
            $('#btnClearFilterBusiness').css('display', 'none');   
            
            return false;
        }

        function ExportToExcel(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridPaymentHistory").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, false);
            return false;
        }

        function ExportToExcel_Deductions(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentDeductionReport").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridPaymentDeductionReport").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridPaymentDeductionReport").data("kendoGrid").element, false);
            return false;
        }

        function ExportToExcel_ConsolidatedBusiness(e) {            
            e.preventDefault();
            kendo.ui.progress($("#gridConsolidatedBusiness").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridConsolidatedBusiness").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridConsolidatedBusiness").data("kendoGrid").element, false);
            return false;
        }

        function BindGrid() {     
            var fileName = '';
            var exportExcelName = fileName.concat('PaymentReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
            var custName= '<%=custName%>';
            var gridview = $("#gridPaymentHistory").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentHistory?custID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    }
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "PaymentID" },
                    { hidden: true, field: "Mode" },                                        
                    //{
                    //    field: "TxnDate", 
                    //    type: "date",
                    //    //hidden: true,
                    //    //format: "{0:DD/MM/YYYY}"
                    //    //template: "#= kendo.toString(kendo.parseDate(TxnDate, 'yyyy-MM-dd')) #",
                    //},
                    { hidden: true, field: "FilterDate",type: "date", },  
                    {
                        field: "TxnDate", title: 'Transaction Date',
                        type: "date",
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'
                        },
                        headerAttributes: { style: "text-align:center" },
                        template: "#= dateConverter(data)#", 
                        //format: "{0:dd-MMM-yyyy HH:mm tt}",                     
                    },
                    {
                        field: "Mode", title: 'Mode',
                        template: "#= showModeText(data)#",
                        width: "10%",
                        attributes: {
                            style: 'text-align:center;'
                        },
                        headerAttributes: { style: "text-align:center" },                        
                    },
                    {
                        field: "OrderID", title: 'OrderID',
                        width: "20%;",                        
                        attributes: {
                            style: 'white-space: nowrap; text-align:center;'
                        },
                        headerAttributes: { style: "text-align:center" },                       
                    },
                    {
                        field: "StatusName", title: 'Status',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap; text-align:center;'
                        },
                        headerAttributes: { style: "text-align:center" },                        
                    },
                    {
                        field: "Amount", title: 'Amount',
                        template: "#= currencyConverter(data)#",
                        width: "15%",
                        attributes: {
                            style: 'text-align:right;'
                        },
                        headerAttributes: { style: "text-align:right" }, 
                        footerAttributes: { style: "text-align:right" }, 
                        footerTemplate: "Total Credit: #=GetTotalCredit()#",                        
                    },
                    {
                        title: "Action", lock: true, width: "10%;",
                        attributes: {
                            style: 'text-align: center;'
                        },
                        headerAttributes: { style: "text-align:center" },
                        command:
                            [
                               { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", visible: showHideButton, click:openPaymentDetails },                               
                               { name: "TxnStatusCheck", text: "", iconClass: ".k-icon .k-i-refresh", className: "TxnStatusCheck", visible: showHideTxnStatusButton, click:CallCheckTxnStatus },
                            ],
                    }
                ],
                dataBound: function () {
                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                    $(".TxnStatusCheck").find("span").addClass("k-icon k-i-refresh");
                },
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];   
                    
                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];
                    sheet.name = "Payments";

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 0;
                        row.cells[cellIndex].format = "dd-MMM-yyyy hh:mm AM/PM";  
                        row.cells[cellIndex].autoWidth = true;
                    }

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });                    
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });
        }   

        function GetTotalCredit() {
            debugger;
            if($('#gridPaymentHistory').data('kendoGrid')!=null && $('#gridPaymentHistory').data('kendoGrid')!=undefined){
                var dataSource = $('#gridPaymentHistory').data('kendoGrid').dataSource;
                if(dataSource!=null && dataSource!=undefined){
                    var data = dataSource.data();
                    
                    var item, sum = 0;
                    for (var idx = 0; idx < data.length; idx++) {
                        item = data[idx];
                        if (((item.PGID===2 && item.StatusID===1)||(item.PGID===0 && item.StatusID===2)) && item.Amount) {
                            sum += item.Amount;
                        }
                    }
                }
            }
            return ConvertToCurrency(sum);
        }
        
        function BindGrid_PaymentDeduction() {  
            var fileName = '';
            var exportExcelName = fileName.concat('PaymentDeductionReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
            var custName= '<%=custName%>';
            var gridview = $("#gridPaymentDeductionReport").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentDeductionReport?distID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    aggregate: [{ field: "Amount", aggregate: "sum" }],
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    },                   
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "CustomerID" },
                    { hidden: true, field: "BranchID" },
                    { hidden: true, field: "FilterDate", type: "date" },  
                    {
                        field: "TxnDate", title: 'Transaction Date',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'
                        },
                        headerAttributes: { style: "text-align:center" },
                        template: "#= dateConverter(data)#", 
                        //format: "{0:dd-MMM-yyyy HH:mm tt}",
                    },                    
                    {
                        field: "CustomerName", title: 'Customer',
                        width: "20%;",                        
                        attributes: {
                            style: 'white-space: nowrap;'
                        },                        
                    },  
                    {
                        field: "BranchCount", title: 'No. of Location/Branch',
                        width: "10%;",                        
                        attributes: { style: 'white-space: nowrap;text-align:right;'},      
                        headerAttributes: { style: "text-align:right" },                        
                    },
                    {
                        field: "EmployeeCount", title: 'No. of Employee(s)',
                        width: "10%;",                        
                        attributes: { style: 'white-space: nowrap;text-align:right;'},      
                        headerAttributes: { style: "text-align:right" },                         
                    },
                    {
                        field: "PeriodName", title: 'Period',
                        width: "10%;",                        
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'
                        },      
                        headerAttributes: { style: "text-align:center" },
                    }, 
                     {
                         field: "Year", title: 'Year',
                         width: "10%;",                        
                         attributes: {
                             style: 'white-space: nowrap;text-align:center;'
                         },                        
                         headerAttributes: { style: "text-align:center" },
                     },
                     {
                         field: "CreditDebit", title: 'Credit/Debit',                        
                         width: "10%",
                         attributes: {
                             style: 'text-align:center;'
                         },
                         headerAttributes: { style: "text-align:center" },                        
                     },
                    {
                        field: "Amount", title: 'Amount',
                        template: "#= currencyConverter(data)#",
                        width: "20%",
                        attributes: {
                            style: 'text-align:right;'
                        },
                        //aggregates: ["sum"], footerTemplate: "Total : #=sum#",
                        headerAttributes: { style: "text-align:right" },   
                        aggregates: ["sum"],
                        footerTemplate: "Total Debit: #=ConvertToCurrency(sum)#",
                        footerAttributes: { style: "text-align:right" }, 
                    },                                     
                ],
                dataBound: function () {                    
                    
                },                
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0]; 

                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];
                    sheet.name = "Deductions";

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 0;
                        var value = row.cells[cellIndex].value;
                        var newValue = DateFormatConverter(value,"dd-MMM-yyyy hh:mm tt");

                        row.cells[cellIndex].value = newValue;                        
                        row.cells[cellIndex].autoWidth = true;

                        //Mode
                        cellIndex = 1;
                        var modeValue = row.cells[cellIndex].value;
                        var modeNewValue = GetMode(modeValue);

                        row.cells[cellIndex].value = modeNewValue;                        
                        row.cells[cellIndex].autoWidth = true;                        
                    }
                                        
                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });
        }


        function GetMode(providedData) {

            var strDate = '';
            kendo.culture("en-IN");

            if (providedData != null && providedData != undefined) {
                if(providedData===1)
                    modeText="Online";
                else if(providedData===2)
                    modeText="Offline";
            }

            return strDate;
        }

        function DateFormatConverter(providedDate, format) {

            var strDate = '';
            kendo.culture("en-IN");
            if (providedDate != null && providedDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(providedDate), format)
            }
            return strDate;
        }

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownTree({
                placeholder: "Select Customer",               
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ApplyFilter_Deductions,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =avacomRLCSAPIURL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function BindGrid_ConsolidatedBusiness() {   
            var fileName = '';
            var exportExcelName = fileName.concat('ConsolidatedBusinessReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");
             var custName= '<%=custName%>';
            var gridview = $("#gridConsolidatedBusiness").kendoGrid({
                dataSource: {                   
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetPaymentConsolidatedReport?distID=' + <%=custID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },   
                    pageSize: 10,
                    schema: {
                        data: function (response) {                              
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    },
                    //
                    group: {
                        field: "ParentName",  
                        aggregates: [{ field: "TotalDebit", aggregate: "sum" }]
                    },                    
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ParentID" },
                    { hidden: true, field: "CustID" },   
                    { hidden: true, field: "FilterDate", type:"date" }, 
                    {
                        field: "ParentName", title: 'Customer',
                        width: "20%;",                        
                        attributes: { style: 'white-space: nowrap;'},                        
                    },
                    {
                        field: "PeriodName", title: 'Period',
                        width: "10%;",                        
                        attributes: {style: 'white-space: nowrap; text-align:center;'},      
                        headerAttributes: { style: "text-align:center" },
                    }, 
                    {
                        field: "Year", title: 'Year',
                        width: "10%;",                        
                        attributes: {style: 'white-space: nowrap; text-align:center;'},                        
                        headerAttributes: { style: "text-align:center" },
                    },                     
                    {
                        field: "TotalDebit", title: 'Total',
                        template: "#= currencyCommaSeperatedConverter(data)#",
                        width: "10%",
                        attributes: {style: 'text-align:right;'},
                        aggregates: ["sum"], 
                        groupHeaderTemplate: "Total: #=sum#",
                        //groupFooterTemplate: " : #=sum#",
                        headerAttributes: { style: "text-align:right" },                        
                    },                                     
                ],
                dataBound: function () {                    
                    
                },
                excelExport: function (e) {

                    var sheet = e.workbook.sheets[0];   
                    
                    sheet.frozenRows = 1;
                    sheet.mergedCells = ["A1:B1"];                    

                    var myHeaders = [{
                        value: custName,                        
                    }];

                    sheet.rows.splice(0, 0, { cells: myHeaders, type: "header"});

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });
        }

        function Bind_MyDistributors() {
            $("#ddlDistributors").kendoDropDownTree({
                placeholder: "Select",               
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: ApplyFilter_ConsolidatedBusiness,
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetMyDistributors?parentDistID=<%=distID%>&uptoSubDist=true',                            
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function dateConverter(data) {
            
            var strDate = '';
            kendo.culture("en-IN");
            if (data.TxnDate != null && data.TxnDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.TxnDate), "dd-MMM-yyyy hh:mm tt")
            }
            return strDate;
        }

        function onRowBound(e) {
            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
            $(".k-grid-delete").find("span").addClass("k-icon k-delete");
            $(".k-grid-add").find("span").addClass("k-i-plus-outline");
        }

        function showHideButton(data) {            
            var showHide = false; 
            
            if("<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role%>"!=="SPADM"){
                showHide=false;
            }else{
                showHide=true;
            }
            
            return showHide;
        }

        function showHideTxnStatusButton(data) {  
            
            var showHide = false; 
            
            if (data.Mode != null && data.Mode != undefined) {
                if(data.Mode===1){
                    if(data.StatusID===0){
                        showHide=true;
                    }else{
                        showHide=false;
                    }
                }                   
            }            
            
            //if (data.Mode != null && data.Mode != undefined) {
            //    if(data.Mode===2)
            //        showHide=true;
            //}
            return showHide;
        }        
        
        function showModeText(data) {            
            var modeText = '';
            kendo.culture("en-IN");
            if (data.Mode != null && data.Mode != undefined) {
                if(data.Mode===1)
                    modeText="Online";
                else if(data.Mode===2)
                    modeText="Offline";
            }
            return modeText;
        }

        function currencyConverter(data) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.Amount != null && data.Amount != undefined) {
                strPrice = kendo.toString(data.Amount, "n")
            }
            return strPrice;
        }

        function ConvertToCurrency(amount) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (amount != null && amount != undefined) {
                strPrice = kendo.toString(amount, "n")
            }
            return strPrice;
        }

        function currencyCommaSeperatedConverter(data) {            
            var strPrice = '';
            kendo.culture("en-IN");
            if (data.TotalDebit != null && data.TotalDebit != undefined) {
                strPrice = kendo.toString(data.TotalDebit, "n")
            }
            return strPrice;
        }

        function openPaymentDetails(e){
            e.preventDefault();
            
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            <%--$("#<%= hdnPaymentID.ClientID %>").val(dataItem.PaymentID);--%>

            if(dataItem!=null && dataItem!=undefined){
                if(dataItem.PaymentID!=null && dataItem.PaymentID!=undefined){

                    $('#PaymentDetails').show();
                    var myWindowAdv = $("#PaymentDetails");

                    myWindowAdv.kendoWindow({
                        width: "60%",
                        height: '90%',                
                        iframe: true,
                        title: "Payment Details",
                        visible: false,
                        actions: ["Close"],
                        close: onClose,
                        open: onOpen
                    });

                    $('#iframePayment').attr('src', 'about:blank');
                    myWindowAdv.data("kendoWindow").center().open();
                    $('#iframePayment').attr('src', "/RLCS/HRPlus_MyPaymentDetails.aspx?PID=" + dataItem.PaymentID);
                }
            }

            return false;
        }    
        
        function OpenPaymentSummaryPopup() {  
            
            var custID=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;

            $('#divProceedPayment').show();

            var myWindowAdv = $("#divProceedPayment");
            
            myWindowAdv.kendoWindow({
                width: "90%",
                height: '90%',
                title: "Order Summary",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                open: onOpen
            });
            
            $('#iframeProceedPayment').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeProceedPayment').attr('src', "/RLCS/HRPlus_PaymentCheckout.aspx?CustID="+custID);
            return false;
        }

        function CallCheckTxnStatus(e){
            e.preventDefault();
            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, true);
            
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            
            if(dataItem!=null && dataItem!=undefined){
                if(dataItem.OrderID!=null && dataItem.OrderID!=undefined && dataItem.CustomerID!=null && dataItem.CustomerID!=undefined){
                    $("#<%= hdnOrderID.ClientID %>").val(dataItem.OrderID);
                    $("#<%= hdnCustomerID.ClientID %>").val(dataItem.CustomerID);   
                    
                    document.getElementById("<%=btnCheckTxnStatus.ClientID %>").click();
                }
            }

            kendo.ui.progress($("#gridPaymentHistory").data("kendoGrid").element, false);
        }

        function showWindow(){
            
            $("#PaymentDetails").show();

            var myWindow = $("#PaymentDetails");

            myWindow.kendoWindow({
                width: "45%",
                height: "60%", 
                top:"0px",
                visible:false,
                modal: true,
                title: "Payment Details",
                actions: [ "Close" ],                
                close: onClose,
                open: onOpen
            });

            myWindow.data("kendoWindow").center().open();          
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $("#PaymentDetails").hide();
            $('#gridPaymentHistory').data('kendoGrid').dataSource.read();

            //$(this.element).empty();
        }

        function Bind_PaymentMode() {
            $("#ddlMode").kendoDropDownTree({
                placeholder: "Select Mode",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter_Payments,
                dataSource: [
                    { text: "Online", value: "1" },
                    { text: "Offline", value: "2" },                    
                ]
            });
        }

        function Bind_DeductionMode() {
            $("#ddlDeductionMode").kendoDropDownTree({
                placeholder: "Select Type",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter_Deductions,
                dataSource: [
                    { text: "Credit", value: "Credit" },
                    { text: "Debit", value: "Debit" },                    
                ]
            });
        }

        function ApplyFilter_Payments() {
                        
            //Branch
            var selectedPaymentModes = $("#ddlMode").data("kendoDropDownTree")._values;
                        
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedPaymentModes.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedPaymentModes, function (i, v) {
                    modeFilter.filters.push({
                        field: "Mode", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }                       
            
            //datefilter                       
            if ($("#txnDateFrom").data("kendoDatePicker").value() != null && $("#txnDateFrom").data("kendoDatePicker").value() != "") {
                var startFilterDate = moment($("#txnDateFrom").data("kendoDatePicker").value());
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#txnDateTo").data("kendoDatePicker").value() != null && $("#txnDateTo").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());
                var endFilterDate = new Date($("#txnDateTo").data("kendoDatePicker").value().getFullYear(), 
                                             $("#txnDateTo").data("kendoDatePicker").value().getMonth(), 
                                             $("#txnDateTo").data("kendoDatePicker").value().getDate(),
                                             23, 59, 59);

                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridPaymentHistory").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);

                $('#btnClearFilterPayment').css('display', 'block');
            } else {
                var dataSource = $("#gridPaymentHistory").data("kendoGrid").dataSource;
                dataSource.filter({});

                $('#btnClearFilterPayment').css('display', 'none');
            }
        }
                
        function ShowPaymentTab(){

            $('#btnClearFilterPayment').css('display', 'none');   

            BindGrid();

            $("#gridPaymentHistory").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View Payment Details";
                }
            });

            $("#gridPaymentHistory").kendoTooltip({
                filter: ".k-grid-TxnStatusCheck",
                content: function (e) {
                    return "Check Current Status";
                }
            });
            
            Bind_PaymentMode();
                        
            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Payments();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Payments();
            }

            var start = $("#txnDateFrom").kendoDatePicker({
                change: startChange,                   
                format: "dd-MMM-yyyy",                
                dateInput: false               
            }).data("kendoDatePicker");

            var end = $("#txnDateTo").kendoDatePicker({
                change: endChange,                                     
                format: "dd-MMM-yyyy",                
                dateInput: false
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());            
        }

        function ApplyFilter_Deductions() {
                        
            //Customer
            var selectedCustomers = $("#ddlCustomers").data("kendoDropDownTree")._values;
            //var selectedDeductionModes = $("#ddlDeductionMode").data("kendoDropDownTree")._values;
                        
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedCustomers.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedCustomers, function (i, v) {
                    modeFilter.filters.push({
                        field: "CustomerID", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }

            //datefilter                       
            if ($("#FromMonth_Deduction").data("kendoDatePicker").value() != null && $("#FromMonth_Deduction").data("kendoDatePicker").value() != "") {
                //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
                var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value().getFullYear(), 
                                               $("#FromMonth_Deduction").data("kendoDatePicker").value().getMonth(), 
                                               1);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#ToMonth_Deduction").data("kendoDatePicker").value() != null && $("#ToMonth_Deduction").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

                var year=$("#ToMonth_Deduction").data("kendoDatePicker").value().getFullYear();
                var month=$("#ToMonth_Deduction").data("kendoDatePicker").value().getMonth();

                if(month==12){
                    year=year+1;
                    month=1;
                }else{
                    month=month+1;
                }

                var endFilterDate = new Date(year, month,0, 23, 59, 59);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridPaymentDeductionReport").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);

                $('#btnClearFilterDeduction').css('display', 'block');   
            } else {
                var dataSource = $("#gridPaymentDeductionReport").data("kendoGrid").dataSource;
                dataSource.filter({});

                $('#btnClearFilterDeduction').css('display', 'none');   
            }
        }

        function ShowDeductionsTab(){
            $('#btnClearFilterDeduction').css('display', 'none');   
            BindGrid_PaymentDeduction();
            Bind_Customer();
            //Bind_DeductionMode();

            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Deductions();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_Deductions();
            }
                        
            var start = $("#FromMonth_Deduction").kendoDatePicker({
                change: startChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
                parseFormats: ["MM-yyyy"]
            }).data("kendoDatePicker");

            var end = $("#ToMonth_Deduction").kendoDatePicker({                
                change: endChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());
        }

        function ApplyFilter_ConsolidatedBusiness() {
            var finalSelectedfilter = { logic: "and", filters: [] };
            
            //Customer
            var selectedDistributors = $("#ddlDistributors").data("kendoDropDownTree")._values;

            if (selectedDistributors.length > 0) {
                var modeFilter = { logic: "or", filters: [] };

                $.each(selectedDistributors, function (i, v) {
                    modeFilter.filters.push({
                        field: "ParentID", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(modeFilter);
            }
                
            //datefilter                       
            if ($("#FromMonth_Business").data("kendoDatePicker").value() != null && $("#FromMonth_Business").data("kendoDatePicker").value() != "") {
                //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
                var startFilterDate = new Date($("#FromMonth_Business").data("kendoDatePicker").value().getFullYear(), 
                                               $("#FromMonth_Business").data("kendoDatePicker").value().getMonth(), 
                                               1);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "gte", value: startFilterDate });
            }
            
            if ($("#ToMonth_Business").data("kendoDatePicker").value() != null && $("#ToMonth_Business").data("kendoDatePicker").value() != "") {
                //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

                var year=$("#ToMonth_Business").data("kendoDatePicker").value().getFullYear();
                var month=$("#ToMonth_Business").data("kendoDatePicker").value().getMonth();

                if(month==12){
                    year=year+1;
                    month=1;
                }else{
                    month=month+1;
                }

                var endFilterDate = new Date(year, month,0, 23, 59, 59);                
                finalSelectedfilter.filters.push({ logic: "or", field: "FilterDate", operator: "lte", value: endFilterDate });
            }

            if (finalSelectedfilter.filters.length > 0) {
                var dataSource = $("#gridConsolidatedBusiness").data("kendoGrid").dataSource;
                dataSource.filter(finalSelectedfilter);
                $('#btnClearFilterBusiness').css('display', 'block');
            } else {
                var dataSource = $("#gridConsolidatedBusiness").data("kendoGrid").dataSource;
                dataSource.filter({});
                $('#btnClearFilterBusiness').css('display', 'none');
            }
        }

        function ShowBusinessTab(){

            Bind_MyDistributors();

            BindGrid_ConsolidatedBusiness();

            function startChange() {
                
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_ConsolidatedBusiness();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter_ConsolidatedBusiness();
            }
                        
            var start = $("#FromMonth_Business").kendoDatePicker({
                change: startChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            var end = $("#ToMonth_Business").kendoDatePicker({                
                change: endChange,
                // defines the start view
                start: "year",
                // defines when the calendar should return date
                depth: "year",
                // display month and year in the input
                format: "MMM-yyyy",
                // specifies that DateInput is used for masking the input element
                //dateInput: true
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());

            $('#btnClearFilterBusiness').css('display', 'none');
        }
    </script>
    <style>
        .k-grid td {
            line-height: 2.5em;
        }

        .k-widget *, .k-widget :before {
            box-sizing: inherit;
        }

        div.k-window-content {
            position: relative;
            height: 100%;
            padding: 0em;
            overflow: unset;
            outline: 0;
        }

        .mt10 {
            margin-top: 10px;
        }

        .mb10 {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnPaymentID" runat="server" />
    <asp:HiddenField ID="hdnCustomerID" runat="server" />
    <asp:HiddenField ID="hdnOrderID" runat="server" />

    <div style="display: none">
        <asp:Button ID="btnShowPayment" runat="server" />
    </div>

    <div class="col-md-12 colpadding0">
        <header class="panel-heading tab-bg-primary colpadding0">
            <ul class="nav nav-tabs">
                <li class="active" id="liPayment" runat="server">
                    <asp:LinkButton ID="lnkPayments" CausesValidation="false" runat="server" OnClick="lnkPayments_Click">Payments</asp:LinkButton>
                </li>
                <li class="" id="liDeduction" runat="server">
                    <asp:LinkButton ID="lnkDeductions" CausesValidation="false" runat="server" OnClick="lnkDeductions_Click">Deductions</asp:LinkButton>
                </li>
                <li class="" id="liBusiness" runat="server">
                    <asp:LinkButton ID="lnkConsolidatedBusiness" CausesValidation="false" runat="server" OnClick="lnkConsolidatedBusiness_Click">Consolidated Business</asp:LinkButton>
                </li>
                <li class="" id="liAvailableBalance" runat="server" style="float: right">
                    <asp:LinkButton ID="LinkButton1" CausesValidation="false" runat="server" OnClientClick="return false;">
                        Available Balance:
                        <asp:Label ID="lblBalance" runat="server">0.00</asp:Label>
                    </asp:LinkButton>
                </li>
            </ul>
        </header>
    </div>

    <div class="clearfix"></div>

    <asp:MultiView ID="MainView" runat="server">
        <asp:View ID="PaymentsView" runat="server">
            <div class="row">
                <asp:ValidationSummary ID="vsPayments" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="PaymentsValidationGroup" />
                <asp:CustomValidator ID="cvPayments" runat="server" EnableClientScript="False"
                    ValidationGroup="PaymentsValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div id="divContainerPayments" class="mt10 col-md-12 colpadding0">
                <div class="row mb10 colpadding0">
                    <div class="col-md-10 colpadding0 text-right">
                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="txnDateFrom" placeholder="From Date" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="txnDateTo" placeholder="To Date" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ddlMode" data-placeholder="type" style="width: 100%;" />
                        </div>
                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <button id="btnClearFilterPayment" class="btn btn-primary" onclick="ClearFilterPayment(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>
                    </div>

                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != null)
                        { %>
                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "SPADM")
                        { %>
                    <div class="col-md-1 colpadding0 text-right">
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="lnkBtnBack" data-toggle="tooltip" data-placement="bottom" ToolTip="Back to My Distributors"
                            PostBackUrl="~/RLCS/HRPlus_MyDistributors.aspx">Back</asp:LinkButton>
                    </div>
                    <% } %>
                    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "DADMN")
                        { %>
                    <div class="col-md-1 text-right">
                        <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddAmount" data-toggle="tooltip" data-placement="bottom" ToolTip="Add Payment"
                            OnClientClick="return OpenPaymentSummaryPopup();"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Add
                        </asp:LinkButton>
                        <%--OnClick="btnAddAmount_Click"--%>
                        <% } %>
                    </div>
                    <% } %>

                    <div class="col-md-1 colpadding0 text-right">
                        <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCheckTxnStatus" OnClick="btnCheckTxnStatus_Click" Style="display: none"></asp:Button>
                        <button id="btnExcel" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Export to Excel"><span class="k-icon k-i-excel"></span>Export</button>                        
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridPaymentHistory" style="border: none;">
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="DeductionsView" runat="server">
            <div id="divContainerDeductions" class="mt10 col-md-12 colpadding0">
                <div class="row mb10 colpadding0">
                    <div class="col-md-10 colpadding0 text-right">
                        <div class="col-md-1" style="width: 30%; padding-left: 0px">
                            <input id="ddlCustomers" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="FromMonth_Deduction" placeholder="From (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ToMonth_Deduction" placeholder="To (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ddlDeductionMode" data-placeholder="Mode" style="width: 100%; display: none;" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <button id="btnClearFilterDeduction" class="btn btn-primary" onclick="ClearFilterDeduction(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>
                    </div>

                    <div class="col-md-2 colpadding0 text-right">
                        <button id="btnExcel_Deductions" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Export to Excel"><span class="k-icon k-i-excel"></span>Export</button>                        
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridPaymentDeductionReport" style="border: none;">
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="BusinessView" runat="server">
            <div id="divContainerConsolidatedBusiness" class="mt10 col-md-12 colpadding0">
                <div class="row mb10 colpadding0">
                    <div class="col-md-10 colpadding0 text-right">
                         <div class="col-md-1" style="width: 30%; padding-left: 0px">
                            <input id="ddlDistributors" placeholder="To (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="FromMonth_Business" placeholder="From (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <input id="ToMonth_Business" placeholder="To (Month-Year)" style="width: 100%" />
                        </div>

                        <div class="col-md-1" style="width: 20%; padding-left: 0px">
                            <button id="btnClearFilterBusiness" class="btn btn-primary" onclick="ClearFilterBusiness(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>
                    </div>

                    <div class="col-md-2 colpadding0 text-right">
                        <button id="btnExcel_Business" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Export to Excel"><span class="k-icon k-i-excel"></span>Export</button>                        
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row" style="display: flex; /* align-items: center; *//* border: 0px; *//* justify-content: center; */">
                    <div id="gridConsolidatedBusiness" style="border: none;">
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <div id="PaymentDetails">
        <iframe id="iframePayment" style="width: 100%; height: 100%; border: none"></iframe>
    </div>

    <div id="divProceedPayment">
        <iframe id="iframeProceedPayment" style="width: 100%; height: 98%; border: none;"></iframe>
    </div>
</asp:Content>
