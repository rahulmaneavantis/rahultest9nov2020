<%@ Page Title="Entity/Branch :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EntityLocation_Master_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EntityLocation_Master_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>

    <style type="text/css">
        div.k-window-content {
            position: relative;
            height: 100%;
            padding: .58em;
             overflow:hidden;
            outline: 0;
        }
        .k-grid-content {
            min-height: auto;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-icon-plus {
            background-position: -48px -64px;
        }

        .k-i-foo {
            background-color: #f00;
            background-image: url(".place.your.own.icon.here.");
        }

        .k-grid tbody button.k-button {
            min-width: 32px;
            min-height: 32px;
        }

        .k-icon, .k-tool-icon {
            position: relative;
            display: inline-block;
            overflow: hidden;
            width: 1em;
            height: 1em;
            text-align: center;
            vertical-align: middle;
            background-image: none;
            font: 16px/1 WebComponentsIcons;
            speak: none;
            font-variant: normal;
            text-transform: none;
            text-indent: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: inherit;
        }
        div.k-confirm
                {
                    width:411px;
                    left:500.5px;
                }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
    </style>

    <script type="text/javascript">
       
        $(document).ready(function () {

            $("#btnAddEntity").kendoTooltip({ content: "Add New-Entity" });
            $("#btnAddBranch").kendoTooltip({ content: "Add New-Branch" });
            $("#btnUpload").kendoTooltip({ content: "Upload Entity/Branch" });

            $('#divRLCSCustomerBranchesDialog').hide();
            $('#divRLCSCustomerLocationDialog').hide();
            $('#divUploadLocationDetails').hide();            
            $('#divRLCSEmployeeUploadDialogBulk').hide();
            fhead('Masters/ Entity-Branch');
            //debugger;
           
            BindEntityLocation();
           
        });
        function BindEntityLocation()
        {
             var dataSource = new kendo.data.TreeListDataSource({
                transport: {
                    read: {                        
                        url: '<%=avacomRLCSAPI_URL%>GetCustomerBranchDetail?customerID=<%=CustId%>',
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                        dataType: 'json',
                    }
                },
                schema: {
                    data: function (response) {
                        if (response.Result != null)
                            return response.Result;
                    },
                    model: {
                        id: "ID",
                        parentId: "ParentID",
                        fields: {
                            ID: { type: "number", nullable: false },
                            ParentID: { field: "ParentID", nullable: true }
                        },
                        expanded: true
                    }
                }
            });

            $("#treelist").kendoTreeList({
                dataSource: dataSource,
                editable: "true",
                sortable: true,
                filterable: true,
                // toolbar: [{ name: "create", text: " " }],
                columnMenu: true,
                columns: [
                    { hidden: true, field: "Type" },
                    { field: "Name", expandable: true, title: "Entity/Branch", width: "27%" },
                    { field: "CM_ClientID", title: "ClientID", width: "10%" },
                    { field: "CM_State", title: "State Code", width: "10%" },
                    { field: "SM_Name", title: "State", width: "12%" },
                    { field: "CM_City", title: "Location Code", width: "8%" },
                    { field: "LM_Name", title: "City", width: "12%" },
                    { field: "CM_EstablishmentType", title: "Establishment Type", hidden: true },
                    {
                        title: "Action",
                        command: [
                                 //{ name: "View", text: " ", imageClass: "k-i-info", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" },
                                { name: "x", text: " ", imageClass: "k-i-edit", iconClass: ".k-icon k-i-eye", className: "my-edit" },
                                { name: "del", text: " ", iconClass: ".k-icon k-i-eye", imageClass: "k-i-delete", className: "my-del" },
                                { name: "Branch", text: " ", iconClass: ".k-icon k-i-plus", imageClass: "k-i-plus", className: "my-branch" }
                                                        
                        ], lock: true,
                        width: "12%",
                       
                    }
                ]
                , dataBound: onDataBound
            });
        }
        function onDataBound(e) {

            var data = this.dataSource.view();
            for (var i = 0; i < data.length; i++) {
                var uid = data[i].uid;
                var row = this.table.find("tr[data-uid='" + uid + "']");
                if (data[i].BranchType == "B") {
                    row.find(".my-branch").hide();
                }
            }
        }

        $(document).on("click", "#treelist tbody tr .ob-overviewMain", function (e) {
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            
            if (item.BranchType == "E") {
                OpenRLCSEntityClientPopup(item.ID, "1");
            }
            if (item.BranchType == "B") {
                OpenRLCSLocationBranchPopup(item.ID, "1");
            }
            return false;
        });

        // $(document).on("click", "#treelist tbody tr .my-edit", function (e) {
        $(document).on("click", "#treelist tbody tr .my-edit", function (e) {
            //debugger;            
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            
            if (item.BranchType == "E") {
                OpenRLCSEntityClientPopup(item.ID, "1");
            }
            if (item.BranchType == "B") {
                OpenRLCSLocationBranchPopup(item.ID, "1");
            }
            return false;
        });

        $(document).on("click", "#treelist tbody tr .my-del", function (e) {
            var tr = $(e.target).closest("tr");
           
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            var custBranchID = item.ID;
            var ConfirmationUrl = "";
            if (item.BranchType == "E") {
                ConfirmationUrl = "DeleteCustomerEntityConfirmation"
            }
            else if (item.BranchType == "B"){
                ConfirmationUrl="DeleteCustomerBranchConfirmation";
            }

            var $tr = $(this).closest("tr");
            window.kendoCustomConfirm("Are you  want to delete this?", "Confirm").then(function () {
                $.ajax({
                        type: 'POST',
                        url: '<%=avacomRLCSAPI_URL%>' + ConfirmationUrl + '?CustomerBranchID=' + custBranchID,
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            xhr.setRequestHeader('Content-Type', 'application/json');
                        },
                        success: function (result) {
                            debugger;
                            if (result.Result) {
                                DeleteBranch(custBranchID);
                            }
                            else {
                                window.kendoCustomConfirm("Compliance Assign to this branch, Are you  want to delete this?", "Confirm").then(function () {
                                    DeleteBranch(custBranchID);
                                }, function () {

                                });

                            }
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }

                    });
            }, function () {
                // For Cancel
            });

            return false;
        });
        function DeleteBranch(custBranchID) {
             $.ajax({
                        type: 'POST',
                        url: '<%=avacomRLCSAPI_URL%>DeleteCustomerBranch?CustomerBranchID=' + custBranchID,
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            xhr.setRequestHeader('Content-Type', 'application/json');
                        },
                        success: function (result) {
                            $("#treelist").data("kendoTreeList").dataSource.read();
                            alert("Deleted Successfully");
                            console.log(result);
                            
                           
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }

                    });
        }

        $(document).on("click", "#treelist tbody tr .my-branch", function (e) {
            debugger;
            var item = $("#treelist").data("kendoTreeList").dataItem($(this).closest("tr"));
            if (item.ID != undefined && item.ID != null)
                if(item.BranchType=="E")
                {
                    LocationValidation(item.ID)
                    //OpenRLCSLocationBranchPopup(item.ID, "");
                }
                else
                {
                    OpenRLCSLocationBranchPopup(item.ParentID, "");
                }
                   
        });

         ///GG ADD 30July2020
        function EntityValidateCheck()
         {
            debugger;
            var CustomerID = '<%=CustId%>';
            var  NewRecord=1;  ///ADD
            var Ajax='Ajax';
           
            if (CustomerID != '') {
                event.preventDefault();
                $.ajax({
                    url: "/Setup/CheckForEntityValidation",
                    data: { NewRecord,CustomerID ,Ajax},
                    type: "POST",
                    content: 'application/json;charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        debugger;
                        if(data === true)
                        {
                            OpenRLCSEntityClientPopup('0','0');   ///ADD
                        }
                        else
                        {
                            alert("Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.");
                        }
                    }

                });
            }
        }
        function LocationValidation(ID)
        {
             ///GG ADD 30July2020
                     debugger;
                        var CustomerID = '<%=CustId%>';
                        var  NewRecord=1;  ///ADD
                        var Ajax='Ajax';
                       
                        if (CustomerID != '') {
                            event.preventDefault();
                            $.ajax({
                                url: "/Setup/CheckForLocationValidation",
                                data: { NewRecord,CustomerID,ID,Ajax},
                                type: "POST",
                                content: 'application/json;charset=utf-8',
                                dataType: 'json',
                                success: function (data) {
                                    debugger;
                                    if(data === true)
                                    {
                                        OpenRLCSLocationBranchPopup(ID, ""); ///Branch Add
                                    }
                                    else
                                    {
                                        alert("Facility to create more branches is not available in the Free version. Kindly contact Avantis to activate the same.");
                                    }
                                }

                            });
                        }
                    

                    ///END
        }
         ///END
        function OpenRLCSEntityClientPopup(branchID, mode) {
            debugger;
            $('#divRLCSCustomerBranchesDialog').show();

            var myWindowAdv = $("#divRLCSCustomerBranchesDialog");

            myWindowAdv.kendoWindow({
                width: "75%",
                height: '90%',
                content: "/RLCS/RLCS_CustomerBranchDetails_New.aspx?CustomerID=<%=CustId%>" + "&Mode=" + mode + "&BranchID=" + branchID,
                iframe: true,
                title: "Entity/Client Details-HR Compliance",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();           

            return false;
        }

        function OpenRLCSLocationBranchPopup(ID, Master) {
            $('#divRLCSCustomerLocationDialog').show();

            var myWindowAdv = $("#divRLCSCustomerLocationDialog");

            myWindowAdv.kendoWindow({
                width: "87%",
                height: "90%",
                title: "Branch Details-HR+ Compliance",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master);
            
            if (Master == "")
            {
                $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master + "&Edit=NewBranch");
            }
            else
            {
                $('#iframeRLCSLocationBranch').attr('src', "/Setup/CreateClientLocationSetup?Id=" + ID + "&getMaster=" + Master + "&Edit=YES");
            }
            return false;
        }

        function onClose() {
            $('#treelist').data('kendoTreeList').dataSource.read();
        }
        
        function OpenUploadPopup() {
            $('#divUploadLocationDetails').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeLocationUpload').attr('src', "/Setup/UploadLocationFiles");
            var myWindowAdv = $("#divUploadLocationDetails");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "35%",
                title: "Upload Entity/Branch",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }
        //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadEntity() {
             debugger;
             var   cust=<%=CustId%>;
            $('#divRLCSEmployeeUploadDialogBulk').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulk");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Client/Entity",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
           
            $('#iframeUploadBulk').attr('src', "/Setup/UploadEntityUpdateSelected?CustID="+ <%=CustId%>+"&UserID="+ <%=UserId%>); 
            return false;
        }
          //ADD NEW FUNCTION
         function OpenWindowSelectedBulkUploadLocation() {
             debugger;
             var   cust=<%=CustId%>;
            $('#divRLCSEmployeeUploadDialogBulkLoc').show();
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulkLoc");
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                title: "Update Selected Column Location/Branch",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            $("#ContentPlaceHolder1_updateProgress").show();
            $('#iframeUploadBulkLoc').attr('src', "/Setup/UploadLocationUpdateSelected?CustID="+ <%=CustId%>+"&UserID="+<%=UserId%>);
            return false;
        }  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton> <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="row col-md-12 colpadding0" style="margin-top: 10px;">
        <div class="col-md-3 colpadding0" style=" text-align: right;">
            </div>
           <div class="col-md-1 colpadding0" style=" text-align: right;">
            <button id="btnAddEntity" class="k-button k-btnUpload" onclick="return EntityValidateCheck()">
                <span class="k-icon k-i-plus-outline" title="Hello World"></span>Entity
            </button>

           <%-- <button id="btnAddBranch" class="k-button k-btnUpload" onclick="return OpenRLCSLocationBranchPopup('','')">
                <span class="k-icon k-i-plus-outline"></span>Branch
            </button>--%>
        </div>
        <div class="col-md-1 colpadding0" style=" text-align: right;">
            <button id="btnUpload" class="k-button k-btnUpload" onclick="return OpenUploadPopup()">
                <span class="k-icon k-i-upload"></span>Upload
            </button>
        </div>
        <div class="col-md-1 colpadding0" style=" text-align: right;">
            <asp:Button Text="Paycode" ToolTip="Add Paycode" ID="btnPaycode" runat="server" CssClass="k-button" OnClick="btnPaycode_Click" ></asp:Button>
        </div>
         <div class="col-md-6 colpadding0" style=" text-align: right;">
            <button id="btnBulkUploadSelected" class="k-button k-btnUpload" onclick="return OpenWindowSelectedBulkUploadEntity()">
                <span class="k-icon k-i-upload"></span>Update Selected Column Entity
            </button>
              <button id="btnUploadSelectedLocation" class="k-button k-btnUpload" onclick="return OpenWindowSelectedBulkUploadLocation()">
                <span class="k-icon k-i-upload"></span>Update Selected Column Branch
            </button>
         </div>

    </div>

    <div class="row col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
        <div id="treelist"></div>
    </div>

    <div id="divRLCSCustomerBranchesDialog">
        <iframe id="iframeRLCSEntityClient" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divRLCSCustomerLocationDialog">
        <iframe id="iframeRLCSLocationBranch" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>

    <div id="divUploadLocationDetails">
        <div class="chart-loading" id="loader"></div>
        <iframe id="iframeLocationUpload" style="width: 580px; height: 200px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
      <div id="divRLCSEmployeeUploadDialogBulkLoc">
        <iframe id="iframeUploadBulkLoc" style="width: 100%; height: 100%; border: none">
            <div class="chart-loading"></div>
        </iframe>
    </div>
    
</asp:Content>
