﻿<%@ Page Title="Compliance Assignment Report" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_ComplianceAssignmentReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ComplianceAssignmentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>--%>

    <script src="../Scripts/KendoPage/HRPlus_AssignmentReport.js"></script>

    <style type="text/css">
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .change-condition {
            color: blue;
        }
        div.k-window-content {
            position: relative;
            height: 100%;
            padding: .58em;
            overflow:hidden;
            outline: 0;
            }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        //empty datasource
        var emptyDataSource = new kendo.data.DataSource({
            data: []
        });

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownList({
                optionLabel: "Select Customer",
                filter: "contains",
                //autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ChangeCustomer,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                            
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_EntityBranch() {            
            var customerID = -1;

            if ($('#ddlTreeEntityBranch').data('kendoDropDownTree')) {
                $('#ddlTreeEntityBranch').data('kendoDropDownTree').destroy();
                $('#divBranches').empty();
                $('#divBranches').append('<input id="ddlTreeEntityBranch" data-placeholder="Entity/Branch" style="width: 98%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            if (customerID !== -1) {
                $("#ddlTreeEntityBranch").kendoDropDownTree({
                    placeholder: "Entity/Branch",
                    checkboxes: {
                        checkChildren: true
                    },
                    //checkboxes: true,
                    checkAll: true,
                    autoWidth: true,
                    checkAllTemplate: "Select All",
                    tagMode: "single",
                    dataTextField: "Name",
                    dataValueField: "ID",
                    change: ApplyFilter,
                    dataSource: {
                        transport: {
                            read: {
                                url: '<%=AVACOM_RLCS_API_URL%>GetAllEntitiesLocationsList?customerID=' + customerID,
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }
                        },
                        schema: {
                            data: function (response) {
                                return response.Result;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    }
                });
            } else {
                $("#ddlTreeEntityBranch").kendoDropDownTree({
                    dataSource: emptyDataSource,
                    placeholder: "Entity/Branch",
                });
            }
        }

        function Bind_Roles() {
            $("#ddlUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    
                },
                index: 0,
                dataSource: [
                    { text: "Role", value: "-1" },
                    { text: "Performer", value: "3" },
                    { text: "Reviewer", value: "4" }
                ]
            });
        }

        function Bind_AssignedUsers() {
            var customerID = -1;
            debugger;
            if ($('#ddlUsers').data('kendoDropDownTree')) {
                $('#ddlUsers').data('kendoDropDownTree').destroy();
                $('#divUsers').empty();
                $('#divUsers').append('<input id="ddlUsers" data-placeholder="User" style="width: 98%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            var apiURL = '';

            if (customerID !== -1) {
                apiURL = '<%=AVACOM_RLCS_API_URL%>GetUsers?custID=' + customerID + '&distID=<% =distID%>&spID=<% =spID%>';
            } else {
                apiURL = '<% =AVACOM_RLCS_API_URL%>GetAssignedSPOCs?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>';
            }

            $("#ddlUsers").kendoDropDownTree({
                placeholder: "Select",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "UserName",
                dataTextField: "UserName",
                //dataValueField: "UserID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url: apiURL,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            if (response != null && response != undefined) {
                                return response.Result;
                            }
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                debugger;
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        }
                    },
                    pageSize: 50,
                }
            });
        }

        function Bind_ComplianceType() {
            $("#ddlComplianceType").kendoDropDownList({
                placeholder: "Registers/Returns/Challans",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    
                },
                index: 0,
                dataSource: [
                    { text: "All", value: "-1" },
                    { text: "Registers", value: "Registers" },
                    { text: "Returns", value: "Returns" },
                    { text: "Challans", value: "Challans" },
                ]
            });
        }

        var exportExcelName = 'ComplianceAssignmentReport-' + '<%= DateTime.Now.ToString("ddMMyyyy") %>' + '.xlsx';

        function BindGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=AVACOM_RLCS_API_URL%>GetComplianceAssignment?userID=<% =loggedInUserID%>&profileID=<% =loggedUserProfileID%>&custID=<% =customerID%>&distID=<% =distID%>&spID=<% =spID%>&roleCode=<% =loggedUserRole%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            if (response != null && response != undefined) {
                                return response.Result;
                            }
                        },
                        total: function (response) {
                            if (response.Result != null && response.Result != undefined) {
                                if (response.Result.length > 0) {

                                }
                                return response.Result.length;
                            }
                        }
                    },

                    pageSize: 50
                },
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                    filterable: true
                },
                excelExport: function (e) {
                    var columns = e.workbook.sheets[0].columns;
                  
                    columns[0].autoWidth = true;
                    columns[1].autoWidth = true;
                    columns[2].autoWidth = false;
                    columns[2].width = 550;
                    columns[3].autoWidth = true;
                    //columns[4].autoWidth = true;

                    columns[4].autoWidth = false;
                    columns[4].width = 100;//width adjusted
                    columns[5].autoWidth = false;//width adjusted
                    columns[5].width = 170;//width adjusted
                    columns[6].autoWidth = false;//width adjusted
                    columns[6].width = 170;//width adjusted
                   
                    
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                     {
                         field: "CustomerName", title: 'Customer',
                         width: "17%;", attributes: { style: 'white-space: nowrap;' },
                     },
                    {
                        field: "Branch", title: 'Location',
                        width: "17%;", attributes: { style: 'white-space: nowrap;' },
                    },


                    {
                        hidden: true,
                        field: "ActName", title: 'Act',
                        width: "15%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "ShortForm", title: 'Compliance',
                        width: "25%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "RequiredForms", title: 'Form',
                        width: "10%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        width: "10%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Performer", title: 'Performer',
                        width: "10%", attributes: { style: 'white-space: nowrap;' },
                    },
                    {
                        field: "Reviewer", title: 'Reviewer',
                        width: "10%", attributes: { style: 'white-space: nowrap;' },
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.CustomerName;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ShortForm;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Performer;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(8)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.Reviewer;
                    return content;
                }
            }).data("kendoTooltip");
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $('#iframeReAssign').attr('src', 'about:blank');
        }        

        function ShowReAssignPopup(CustomerID) {            
            $('#divReAssignCompliance').show();

            var myWindowAdv = $("#divReAssignCompliance");

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                //maxHeight: '90%',
                //minHeight:'50%',
                title: "Re-Assign Compliance",
                visible: false,
                actions: ["Close"],
                open: onOpen,
                close: onClose
            });

            $('#iframeReAssign').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeReAssign').attr('src', "/RLCS/ClientSetup/RLCS_ComplianceReAssign.aspx?CustID=" + CustomerID);

            return false;
        }

        function OpenReAssignPopup(e) {            
            e.preventDefault();
            var customerID = -1;

            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();

                if (customerID != -1)
                    ShowReAssignPopup(customerID);
            }
        }

        function ExportToExcel(e) {            
            e.preventDefault();
            //kendo.ui.progress($("#grid"), true);

            // trigger export of the products grid
            $("#grid").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#grid"), false);
            return false;
        }
        
        $(document).ready(function () {
            fhead('My Reports/Compliance Assignment Report');

            $('#divReAssignCompliance').hide();

            Bind_Customer();
            Bind_EntityBranch();
            Bind_AssignedUsers();
            //Bind_Roles();
            Bind_ComplianceType();

            BindGrid();

            $("#btnReAssign").click(function (e) {
                OpenReAssignPopup(e);
            });

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            kendo.ui.progress($("#grid"), false);

            $('#btnClearFilter').css('display', 'none');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div class="toolbar col-md-12 colpadding0">
            <div class="row">
                <div class="col-md-1 colpadding0" style="width: 25%;">
                    <input id="ddlCustomers" data-placeholder="Customer" style="width: 98%;" />
                </div>

                <div id="divBranches" class="col-md-1 colpadding0" style="width: 25%;">
                    <input id="ddlTreeEntityBranch" data-placeholder="Entity/Branch" style="width: 98%;" />
                </div>

                <div id="divUsers" class="col-md-1 colpadding0" style="width: 15%;">
                    <input id="ddlUsers" data-placeholder="User" style="width: 98%;" />
                </div>

                <div class="col-md-1 colpadding0" style="width: 10%;">
                    <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                    <input id="ddlUserRole" data-placeholder="Role" style="width: 98%; display: none" />
                    <%--<input id="ddlComplianceType" data-placeholder="RRC" style="width: 98%;" />--%>
                </div>

                <div class="col-md-1 colpadding0" style="width: 25%; text-align: right;">
                    <button id="btnReAssign" class="btn btn-primary">Re-Assign</button>                    
                    <button id="btnExcel" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Export to Excel"><span class="k-icon k-i-excel"></span>Export</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-2" style="width: 13.6%;">
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 10px;">
                </div>
                <div class="col-md-2" style="width: 15%; padding-left: 0px;">
                </div>
                <div class="col-md-2" style="width: 3%;">
                </div>
                <div class="col-md-1" style="padding-left: 435px;">
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterscustomerstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterrole">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filteruser">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filterrrc">&nbsp;</div>
        <div id="grid" style="border: none;"></div>
    </div>

    <div id="divReAssignCompliance" style="overflow: hidden;">
        <iframe id="iframeReAssign" style="width: 100%; height: 100%; border: none;"></iframe>
    </div>
</asp:Content>
