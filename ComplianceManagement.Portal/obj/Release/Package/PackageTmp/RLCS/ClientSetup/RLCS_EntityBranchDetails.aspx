﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_EntityBranchDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_EntityBranchDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/bootstrap-datepicker.min.css" rel="stylesheet" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>

    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
</head>
<body style="background: white;">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div id="divCustomerBranchesDialog">
            <asp:UpdatePanel ID="upCustomerBranches" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                             <asp:ValidationSummary runat="server" CssClass="alert alert-danger"
                                ValidationGroup="CustomerBranchValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            </div>
                        </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Customer</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 control-label">
                            <asp:Literal ID="litCustomer" runat="server" />
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row form-group" id="divParent" runat="server">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Parent</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:Literal ID="litParent" runat="server" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Name</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" MaxLength="50" autoComplete="nope"
                                OnTextChanged="tbxName_TextChanged" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="[a-zA-Z0-9\s()\/@#.,-]*$"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="row form-group" runat="server" id="divClientID" visible="false">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                ClientID</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <div class="col-xs-10 col-sm-10 col-md-10 colpadding0">
                                <asp:TextBox runat="server" ID="txtClientID" MaxLength="10" CssClass="form-control" />
                                <asp:RequiredFieldValidator ErrorMessage="ClientID can not be empty." ControlToValidate="txtClientID"
                                    runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2">
                                <asp:Button runat="server" ID="btnCheck" Text="Check" OnClick="btnCheck_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                            </div>
                            <div class="col-xs-8 col-sm-8 col-md-8">
                                <asp:Label runat="server" ID="lblCheckClient" ForeColor="red" CssClass="m-10" Text="Please click on check button to verify Client ID. "></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group" style="display: none;">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Sub Entity Type</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:DropDownListChosen runat="server" ID="ddlType" CssClass="form-control"
                                AutoPostBack="true" Width="100%" AllowSingleDeselect="false" /> <%--OnSelectedIndexChanged="ddlType_SelectedIndexChanged"--%>
                            <asp:CompareValidator ErrorMessage="Please select Sub Entity Type." ControlToValidate="ddlType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                    </div>

                    <div class="row form-group" id="divCompanyType" runat="server" style="display: none;">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label id="lblType" class="control-label">
                                Type</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:DropDownListChosen runat="server" ID="ddlCompanyType" CssClass="form-control" Width="100%" AllowSingleDeselect="false"/>
                        </div>
                    </div>

                    <div class="row form-group" runat="server" id="divLegalEntityType" visible="false">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label id="lblLegalEntityType" class="control-label">
                                Legal Entity Type</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:DropDownListChosen runat="server" ID="ddlLegalEntityType" CssClass="form-control" Width="100%" AllowSingleDeselect="false"/>
                        </div>
                    </div>
                       
                    <div class="row form-group" runat="server" id="divLegalRelationship" visible="false">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Legal Relationship</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">                             
                            <asp:DropDownListChosen runat="server" ID="ddlLegalRelationShip" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select legal relationship."
                                ControlToValidate="ddlLegalRelationShip" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                    </div>
                        
                    

                     <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Address Line 1</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:TextBox runat="server" ID="tbxAddressLine1" CssClass="form-control" autocomplete="off"
                                MaxLength="150" />
                            <asp:RequiredFieldValidator ErrorMessage="Address Line 1 can not be empty." ControlToValidate="tbxAddressLine1"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Address Line 2</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:TextBox runat="server" ID="tbxAddressLine2"  CssClass="form-control" autoComplete="off"
                                MaxLength="150" />
                        </div>
                    </div>

                      <div class="row form-group" runat="server" id="divState">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                State</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:DropDownListChosen runat="server" ID="ddlState"  CssClass="form-control"
                                  AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" Width="100%" AllowSingleDeselect="false" />
                            <asp:CompareValidator ErrorMessage="Please select State." ControlToValidate="ddlState"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                City</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:DropDownListChosen runat="server" ID="ddlCity" CssClass="form-control" 
                                AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" Width="100%" AllowSingleDeselect="false" />
                            <asp:CompareValidator ErrorMessage="Please select City." ControlToValidate="ddlCity"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                        </div>
                    </div>
                        
                     <div class="row form-group" runat="server" id="divOther">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label class="control-label">
                                Others</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:TextBox runat="server" ID="tbxOther" CssClass="form-control" MaxLength="50" autoComplete="off" />
                            <asp:RequiredFieldValidator ErrorMessage="Other City Name can not be empty." ControlToValidate="tbxOther"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Pin Code</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:TextBox runat="server" ID="tbxPinCode" CssClass="form-control" MaxLength="6" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Contact Person</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:TextBox runat="server" ID="tbxContactPerson" CssClass="form-control" autoComplete="off"
                                MaxLength="200" />
                            <asp:RequiredFieldValidator ErrorMessage="Contact Person name can not be empty."
                                ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                ValidationGroup="CustomerBranchValidationGroup" ErrorMessage="Please enter a valid contact person name."
                                ControlToValidate="tbxContactPerson" ValidationExpression="^[a-zA-Z_ .-]*$"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Landline No.</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:TextBox runat="server" ID="tbxLandline" CssClass="form-control" autoComplete="off"
                                MaxLength="15" />
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Mobile No.</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:TextBox runat="server" ID="tbxMobile" CssClass="form-control" MaxLength="15" autoComplete="off" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label class="control-label">
                                Email</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" MaxLength="200" autoComplete="off" />
                            <asp:RequiredFieldValidator ErrorMessage="Email can not be empty." ControlToValidate="tbxEmail"
                                runat="server" ValidationGroup="CustomerBranchValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerBranchValidationGroup"
                                ErrorMessage="Please enter a valid email." ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Status</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                             <asp:DropDownListChosen runat="server" ID="ddlCustomerStatus" CssClass="form-control" Width="100%" AllowSingleDeselect="false" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                        </div>
                    </div>

                    <div class="row form-group" style="display:none;">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                             <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="control-label">
                                Person Responsible Applicable</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <asp:DropDownListChosen runat="server" ID="ddlPersonResponsibleApplicable" CssClass="form-control" Width="100%" AllowSingleDeselect="false">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                    </div>

                    <div class="row form-group col-xs-12 col-sm-12 col-md-12" style="text-align:center">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" ValidationGroup="CustomerBranchValidationGroup" />
                    </div>
                        
                    <div class="row form-group col-xs-12 col-sm-12 col-md-12">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
