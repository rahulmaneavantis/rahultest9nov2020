﻿<%@ Page Title="Employee :: Setup/Onboarding" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusSPOCMGR.Master" CodeBehind="RLCS_EmployeeMaster_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EmployeeMaster_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />--%>

    <title></title>

    <style>
        .k-icon-calendar {
            background-position: -32px -176px;
        }

        .k-grid-content {
            min-height: 250px;
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            float: right;
            margin-right: 10px;
        }

        .k-grid tbody button.k-button {
            min-width: 32px;
            min-height: 32px;
        }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
        function Bind_Customers() {
            $("#ddlCustomer").kendoDropDownList({
               // placeholder: "Select",
               // optionLabel: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    debugger;
                    Bind_CustomerBranches();
                    BindGrid_Employee();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {     
                            url: '<%=avacomRLCSAPIURL%>GetHRProductMappedCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=userRole%>&prodType=2&showSubDist=false',
                            //url: '<%=avacomRLCSAPIURL%>GetCustomers?custID=' + <%=custID%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        }
                    }
                }
            });
        }

        function Bind_CustomerBranches(){
            debugger;
            var customerID=-1;

            if ($('#dropdowntree').data('kendoDropDownTree')) {
                $('#dropdowntree').data('kendoDropDownTree').destroy();
                $('#divBranches').empty();
                $('#divBranches').append('<input id="dropdowntree" data-placeholder="Entity/Branch" style="width: 98%;" />')
            }

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerID=$("#ddlCustomer").val();
            }else{
                customerID=<% =custID%>;
            }

            $("#dropdowntree").kendoDropDownTree({
                
                placeholder: "Entity/Branch",
                checkboxes: {
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                //dataTextField: "AVACOM_BranchName",
                //dataValueField: "AVACOM_BranchID",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
               
                change: function (e) { 
                    debugger;
                    if ($("#ddlStatus").data("kendoDropDownList").text()!="Select" )                   
                    {
                        setCommonAllfilter();
                    }
                    else{
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();

                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)    
                            });
                        });

                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                   
                },
                dataSource: {                    
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetAllEntitiesLocationsList?customerID='+customerID,
                            //url: '<%=avacomRLCSAPIURL%>GetAssignedEntities?customerId='+customerID+'&userId=<% =userID%>&profileID=<% =ProfileID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                            
                            return response.Result;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        $(document).ready(function () {
            fhead('Masters/Employee');
            Bind_Customers();
            BindGrid_Employee();
           // Bind_CustomerBranches();            
        });

        function checkEmployeeActive(value) {
            if (value == "A") {
                return "Active";
            } else if (value == "I") {
                return "InActive";
            }
        }

        function BindGrid_Employee() {
            debugger;

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined ||<% =custID%>!=-1||<% =distID%>!=-1){
                customerID=$("#ddlCustomer").val();

                if(customerID==""){
                    customerID=<% =custID%>;
                        var a=$("#ddlCustomer").data("kendoDropDownList");a.value(customerID);
                }
                Bind_CustomerBranches(); 
                $("#grid").empty(); 
                var gridview = $("#grid").kendoGrid({
                    dataSource: {
                        serverPaging: false,
                        pageSize: 10,
                        transport: {
                            read: {
                                url: '<% =avacomRLCSAPIURL%>GetAll_EmployeeMaster?CustomerID='+customerID,
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                            }
                        },
                        batch: true,
                        pageSize: 10,
                        schema: {
                            data: function (response) {                            
                                if (response != null && response != undefined)                                
                                    return response.Result;
                            },
                            total: function (response) {                            
                                if (response != null && response != undefined)
                                    if (response.Result != null && response.Result != undefined)
                                        return response.Result.length;
                            }
                        }
                    },

                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: {
                        refresh: true,
                        buttonCount: 3,
                    },
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    selectable: true,
                    columns: [

                        { hidden: true, field: "AVACOM_CustomerID", title: "CustID" },
                        { hidden: true, field: "AVACOM_BranchID", title: "BranchID" },
                        { hidden: true, field: "EM_Branch", title: "Branch" },
                        {
                            title: "Sr.",
                            field: "rowNumber",
                            template: "<span class='row-number'></span>",
                            width: "8%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_EmpID", title: 'EmpID',
                            width: "13%;",
                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_EmpName", title: 'Name',
                            width: "15%",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        }, 
                        {
                            field: "EM_Branch", title: 'Branch',
                            width: "21%",
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_Location", title: 'Location',
                            width: "13%",
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_DOJ",
                            title: 'Joining Date',
                            template: "#= kendo.toString(kendo.parseDate(EM_DOJ, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                            width: "15%",
                            attributes: {
                                style: 'white-space: nowrap '
                            },
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EM_DOL",
                            title: 'Leaving Date',
                            template: "#= dateConverter(data)#",
                            //template: "#= kendo.toString(kendo.parseDate(EM_DOL, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                            width: "13%",
                            attributes: {
                                style: 'white-space: nowrap '
                            },
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                         {
                             field: "EM_PT_State", title: 'PT State',
                             width: "14%",
                             attributes: {
                                 style: 'white-space: nowrap;'
                             },
                             filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }
                         },
                        {
                            field: "EM_Status",
                            title: 'Status',
                            template: '#:checkEmployeeActive(EM_Status)#',
                            width: "11%",
                            attributes: {
                                style: 'white-space: nowrap '
                            },
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            command: 
                                [
                                    { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-download" },
                                    { name: "delete", text: "", iconClass: "k-icon k-i-delete", className: "my-delete" },
                                ], title: "Action", lock: true, width: "12.7%;"
                        }
                    ],
                    dataBound: function () {
                        var rows = this.items();
                        $(rows).each(function () {
                            var index = $(this).index() + 1
                                + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                            var rowLabel = $(this).find(".row-number");
                            $(rowLabel).html(index);
                        });
                    }
                });
            }
            else
            { $("#grid").empty();}
            //gridview.Custom("edit").Click("Edit");    
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function (e) {
                    return "InActive";
                }
            });
        }

        function dateConverter(data) {
            debugger;
            var strDate = '';
            kendo.culture("en-IN");
            if (data.EM_DOL != null && data.EM_DOL != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.EM_DOL), "dd-MMM-yyyy")
            }
            return strDate;
        }
       
        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            debugger;
            var d = e.data;
            
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

            var EMPID = item.EM_EmpID;
            var Client = item.EM_ClientID;
            var CustID = item.AVACOM_CustomerID;
       
            $('#divRLCSEmployeeAddDialog').show();
            //$("#idloader1").show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                content: "../Setup/CreateEmplodyeeMaster?EmpID=" + EMPID + "&CustID=" + CustID + "&Client=" + Client,
                iframe: true,
                title: "Employee Details-HR Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            //$('#iframeAdd').attr('src', "/Setup/CreateEmplodyeeMaster?EmpID=" + EMPID + "&CustID=" + CustID + "&Client=" + Client);

            // $('#grid').data('kendoGrid').refresh();
            // return false;
        }
        );
        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }

        $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var cust = item.AVACOM_CustomerID;
            var EmpID = item.EM_EmpID;
            var $tr = $(this).closest("tr");

            $.ajax({
                type: 'POST',
                url: '<% =avacomRLCSAPIURL%>DeleteEmployee?EmpID=' + EmpID + '&CustID=' + cust,
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                    xhr.setRequestHeader('Content-Type', 'application/json');
                },
                success: function (result) {
                    // notify the data source that the request succeeded
                    onClose();
                    $("#CheckCustomer").text("Employee InActivated Successfully.");
                    $('#btnModal').click();  
                    return false;
                    //grid = $("#grid").data("kendoGrid");
                    //grid.removeRow($tr);
                },
                error: function (result) {
                    // notify the data source that the request failed
                    console.log(result);
                }
            });
        });

        $(document).ready(function () {
            $('#divRLCSEmployeeUploadDialog').hide();
            $('#divRLCSEmployeeUploadDialogBulk').hide();
            $('#divRLCSEmployeeAddDialog').hide();
           // BindGrid_Employee();            
        });
    </script>

    <script type="text/javascript">
        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            
            CheckFilterClearorNotMain();
        };

        //Filter 16JAN2020
        function setCommonAllfilter() {
            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0      
                && $("#ddlStatus").data("kendoDropDownList").text()  != "Select")
            {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Status details
                var StatusVal = $("#ddlStatus").data("kendoDropDownList").value();
                var Statusdetails = [];
               //$.each(list1, function (i, v) {
                    Statusdetails.push({
                        field: "EM_Status", operator: "eq", value: StatusVal
                    });
              // });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });

            }
            else  if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                   && $("#ddlStatus").data("kendoDropDownList").text()=="Select" ) 
                   {

                //location details
                var list = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list, function (i, v) {
                    locationsdetails.push({
                        field: "AVACOM_BranchID", operator: "eq", value: parseInt(v)
                    });
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;
                   
                dataSource.filter({
                    logic: "or",
                    filters: [  
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
    
            }
            else  if ( $("#ddlStatus").data("kendoDropDownList").value()=="A" ) 
            {
                //Status details
                var StatusVal = $("#ddlStatus").data("kendoDropDownList").value();
                var Statusdetails = [];
                Statusdetails.push({
                    field: "EM_Status", operator: "eq", value: StatusVal
                });
              


                var dataSource = $("#grid").data("kendoGrid").dataSource;
                   
                dataSource.filter({
                    logic: "or",
                    filters: [  
                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
    
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {
          
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>&nbsp;');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }
        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) ) {
                $('#Clearfilter').css('display', 'none');
            }
        }
        //END
     
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }        
    </script>

    <script>
        function OpenUploadWindow() {

            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
            myWindowAdv.kendoWindow({
                width: "60%",
                height: "50%",
                content: "../Setup/UploadEmployeeFiles?CustID=" + customerID,
                iframe: true,
                title: "Upload",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            
            return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }

function OpenUploadWindowSelectedBulk() {
            var UserID=<% =userID%>;
            $('#divRLCSEmployeeUploadDialogBulk').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialogBulk");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
            myWindowAdv.kendoWindow({
                width: "85%",
                height: "80%",
                content: "../Setup/UploadEmployeeUpdateSelected?CustID=" + customerID+"&UserID="+UserID ,
                iframe: true,
                title: "Upload Employee Details",
                visible: false,
                actions: [
                    "Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            
            return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }

        function OpenUploadWindowNew() {
            var UserID=<% =userID%>;
            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $("#idloader3").show();
            //$('#iframeUpload').attr('src', "../Setup/UploadEmployeeFiles");
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");
            var customerID = $("#ddlCustomer").val();
            if (customerID!='') {
                myWindowAdv.kendoWindow({
                    width: "60%",
                    height: "50%",
                    content: "../Setup/UploadEmployeeFilesNew?CustID=" + customerID +"&UserID="+UserID ,
                    iframe: true,
                    title: "Upload",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
            
                return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }
        ///GG ADD 30July2020
        function EmployeeValidateCheck()
        {
            debugger;
            var CustomerID = $('#ddlCustomer').val();
            var  NewRecord=1;  ///ADD
            event.preventDefault();
            if (CustomerID!="") {
                $.ajax({
                    url: "/Setup/GetBranchCountCustomerwise",
                    data: { NewRecord,CustomerID},
                    type: "POST",
                    content: 'application/json;charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        if(data === true)
                        {
                            $.ajax({
                                url: "/Setup/CheckForEmployeeValidation",
                                data: { NewRecord,CustomerID},
                                type: "POST",
                                content: 'application/json;charset=utf-8',
                                dataType: 'json',
                                success: function (data) {
                                    debugger;
                                    if(data === true)
                                    {
                                        OpenAddEmployeePopup(0);   ///ADD
                                    }
                                    else
                                    {
                                        alert("Facility to create more employees is not available in the Free version. Kindly contact Avantis to activate the same.");
                                    }
                                }

                            });
                        }
                        else {
                            alert("Kindly Create the Entity/Branch to upload the employee Details");
                        }
                    }
                });
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }
        ///END
        function onClose() {
            BindGrid_Employee();
        }
        function OpenAddEmployeePopup(ID) {
            debugger;
            $('#divRLCSEmployeeAddDialog').show();
            //$("#idloader1").show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");
            var ddlCust= $('#ddlCustomer').val();
            if (ddlCust!='') {

                myWindowAdv.kendoWindow({
                    width: "86%",
                    height: '90%',
                    content: "../Setup/CreateEmplodyeeMaster?EmpID=" + ID+"&CustID="+ddlCust,
                    iframe: true,
                    title: "Employee Details-HR Compliance",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                //$('#iframeAdd').attr('src', "../Setup/CreateEmplodyeeMaster?Id=" + ID);

                // $('#grid').data('kendoGrid').refresh();
                BindGrid_Employee();
                return false;
            }
            else {
                $("#CheckCustomer").text("Please Select Customer..!");
                $('#btnModal').click();  
                return false;
            }
        }


        $(document).ready(function () {
            // function BindLocationFilter() {



            function ClearAllFilterMain(e) {

                if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                    customerID=$("#ddlCustomer").val();
                }else{
                    customerID=<% =custID%>;
            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '90%',
                content: "../Setup/CreateEmplodyeeMaster?EmpID=" + ID + "&CustID=" + customerID + "&Client=" + Client,
                iframe: true,
                title: "Employee Details-HR Compliance",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
           
            return false;
        }
        });
    $(document).ready(function () {
            
        function ClearAllFilterMain(e) {

            e.preventDefault();
            
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            //$("#ddlMonth").data("kendoDropDownList").value([]);
            //$("#ddlYear").data("kendoDropDownList").value([]);
            $('#ClearfilterMain').css('display', 'none');

            $("#grid").data("kendoGrid").dataSource.filter({});

            $('#chkAll').removeAttr('checked');
        }

        $("#ddlStatus").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "EM_Status",
            dataSource: 
                [
                    { text: "Select", EM_Status: "S" },
                    { text: "Active", EM_Status: "A" },
                    { text: "InActive", EM_Status: "I" }
                ],
            change: function () {
                debugger;
                var value = this.value();

                var gridview = $("#grid").data("kendoGrid");

                if (value) {
                    if ($("#dropdowntree").data("kendoDropDownTree")._values.length == 0)
                    {
                        gridview.dataSource.filter({ field: "EM_Status", operator: "eq", value: value });
                    }
                    else {
                        setCommonAllfilter();
                      //  $("#grid").data("kendoGrid").dataSource.filter({ field: "EM_Status", operator: "eq", value: value });
                        fCreateStoryBoard('ddlStatus', 'filterstatus1', 'loc');
                    }
                } else {
                    grid.dataSource.filter({});
                }
            }
        });
       
        $("#btnSearch").click(function () {
            debugger;
            var filter = [];
            $x = $("#txtSearch").val();
            if ($x) {
                var gridview = $("#grid").data("kendoGrid");
                gridview.dataSource.query({
                    page: 1,
                    pageSize: 50,
                    filter: {
                        logic: "or",
                        filters: [
                          { field: "EM_Branch", operator: "contains", value: $x },
                          { field: "EM_EmpName", operator: "contains", value: $x },
                          { field: "EM_EmpID", operator: "contains", value: $x },
                          { field: "EM_DOJ", operator: "contains", value: $x },
                          { field: "EM_Location", operator: "contains", value: $x },
                          { field: "EM_PT_State", operator: "contains", value: $x }                          
                        ]
                    }
                });

                return false;
            }
            else {
                //BindGrid_Employee();
                $('#grid').data('kendoGrid').dataSource.read();
                return false;
            }
           
        });
    });


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- Trigger the modal with a button -->
<button id ="btnModal" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display:none;">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Message</h3>
      </div>
      <div class="modal-body">
        <h4> <span id="CheckCustomer" ></span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server" PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx">Entity-Branch</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
            <div class="col-md-2 colpadding0">
                <input id="ddlCustomer" style="width: 98%" />
            </div>

            <div id="divBranches" class="col-md-3 colpadding0">
                <input id="dropdowntree" data-placeholder="Entity/Branch" style="width: 98%;" />
            </div>

            <div class="col-md-1 colpadding0">
                <input id="ddlStatus" style="width: 98%" />
            </div>

            <div class="col-md-2 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 65%" placeholder="Type to Search..." />
                <button id="btnSearch" class="k-button">Search</button>
            </div>

            <%--<div class="col-md-1 colpadding0">
                <button id="btnAdd" class="k-button k-btnUpload" onclick="return OpenAddEmployeePopup()"><span class="k-icon k-i-plus-outline"></span>Add</button>
                <button id="btnAdd1" class="k-button k-btn" onclick="return OpenAddEmployeePopup()"><span class="k-icon k-i-plus-outline">Add New</span></button>
            </div>--%>

            <div class="col-md-4 colpadding0 text-right">
                 <button id="btnAdd" class="k-button k-btnUpload pull-left" onclick="return EmployeeValidateCheck();OpenAddEmployeePopup();"><span class="k-icon k-i-plus-outline"></span>Add</button>
                <%--<button id="Clear" class="k-button k-btn" onclick="return OpenUploadWindow()"><span class="k-hidden"></span>Upload</button>--%>
                <button id="btnUploadEmployeeNew" class="k-button k-btn" onclick="return OpenUploadWindowNew()"><span class="k-icon k-i-upload"></span>Upload</button>
                <button id="btnUploadEmployeeSelectedCloumn" class="k-button k-btn" onclick="return OpenUploadWindowSelectedBulk()"><span class="k-icon k-i-upload"></span>Upload Selected Column</button>
               
            </div>

            <button id="ClearfilterMain" class="k-button" style="display: none;" onclick="ClearAllFilterMain(event)">
                <span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter
            </button>
        </div>
    </div>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;" id="filterstatus1">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">

            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
        <div id="divRLCSEmployeeUploadDialogBulk">
        <iframe id="iframeUploadBulk" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeUploadDialog">
        <iframe id="iframeUpload" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeAddDialog">
        <iframe id="iframeAdd" style="width: 1165px; height: 677px; border: none"></iframe>
    </div>
  
</asp:Content>

