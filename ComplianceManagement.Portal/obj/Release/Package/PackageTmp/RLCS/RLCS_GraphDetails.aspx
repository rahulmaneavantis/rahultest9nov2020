﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RLCS_GraphDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_GraphDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="/Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
</head>

<body class="bgColor-white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smDashboardGraphDetail" runat="server"></asp:ScriptManager>

        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="col-md-12">
                <asp:ValidationSummary ID="vsGraphDetailPage" runat="server" class="alert alert-block alert-danger fade in"
                    ValidationGroup="GraphDetailPageValidationGroup" />
                <asp:CustomValidator ID="cvGraphDetail" runat="server" EnableClientScript="False"
                    ValidationGroup="GraphDetailPageValidationGroup" Display="None" />
            </div>

            <div class="col-md-12 colpadding0">
                <div id="divddlActivityStatus" runat="server" class="col-md-3 colpadding0">
                    <label for="ddlStatus" class="filter-label">Status</label>
                    <asp:DropDownListChosen runat="server" ID="ddlActivityStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="50%">
                        <asp:ListItem Text="Due Today" Value="DT"></asp:ListItem>
                        <asp:ListItem Text="Upcoming" Value="UC"></asp:ListItem>
                        <asp:ListItem Text="Overdue" Value="OD"></asp:ListItem>
                        <asp:ListItem Text="High Risk" Value="HR"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div id="divddlYear" runat="server" class="col-md-3 colpadding0">
                    <label for="ddlYear" class="filter-label">Year</label>
                    <asp:DropDownListChosen runat="server" ID="ddlYear" class="form-control" DataPlaceHolder="Select Year" Width="50%">
                        <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                        <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                        <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                        <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
                               
                <div id="divddlMonth" runat="server" class="col-md-3 colpadding0">
                    <label for="ddlMonth" class="filter-label">Month</label>
                    <asp:DropDownListChosen runat="server" ID="ddlMonth" class="form-control" DataPlaceHolder="Select Month" Width="50%">
                        <asp:ListItem Text="JAN" Value="01"></asp:ListItem>
                        <asp:ListItem Text="FEB" Value="02"></asp:ListItem>
                        <asp:ListItem Text="MAR" Value="03"></asp:ListItem>
                        <asp:ListItem Text="APR" Value="04"></asp:ListItem>
                        <asp:ListItem Text="MAY" Value="05"></asp:ListItem>
                        <asp:ListItem Text="JUN" Value="06"></asp:ListItem>
                        <asp:ListItem Text="JUL" Value="07"></asp:ListItem>
                        <asp:ListItem Text="AUG" Value="08"></asp:ListItem>
                        <asp:ListItem Text="SEP" Value="09"></asp:ListItem>
                        <asp:ListItem Text="OCT" Value="10"></asp:ListItem>
                        <asp:ListItem Text="NOV" Value="11"></asp:ListItem>
                        <asp:ListItem Text="DEC" Value="12"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0"></div>
               
            </div>

            <div class="clearfix"></div> 

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdActivityList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Activity" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityName") %>' ToolTip='<%# Eval("ActivityName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="22%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DueDate") != DBNull.Value ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'
                                        ToolTip='<%# Eval("DueDate") != DBNull.Value ? Convert.ToDateTime(Eval("DueDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

             <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdLocationWiseComplianceStatusList" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Client" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientName") %>' ToolTip='<%# Eval("ClientName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="State" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; ">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StateName") %>' ToolTip='<%# Eval("StateName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LocationName") %>' ToolTip='<%# Eval("LocationName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Complied" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Compliance") %>' ToolTip='<%# Eval("Compliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Not Complied" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NotCompliance") %>' ToolTip='<%# Eval("NotCompliance") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdRegisters" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Client" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientName") %>' ToolTip='<%# Eval("ClientName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="State" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; ">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("State") %>' ToolTip='<%# Eval("State") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="upAction" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:LinkButton CommandArgument='<%# Eval("Download")%>'
                                            AutoPostBack="true" CommandName="Download" ID="lnkDownload" runat="server"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download"
                                             Visible='<%# ShowHideDownloadButton((string)Eval("button")) %>'>
                                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download"/>
                                        </asp:LinkButton>
                                    </ContentTemplate>                                    
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>                      
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdReturns" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Activity" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityName") %>' ToolTip='<%# Eval("ActivityName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="upAction" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:LinkButton CommandArgument='<%# Eval("Download")%>'
                                            AutoPostBack="true" CommandName="Download" ID="lnkDownloadReturn" runat="server"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download"
                                             Visible='<%# ShowHideDownloadButton((string)Eval("button")) %>'>
                                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download"/>
                                        </asp:LinkButton>
                                    </ContentTemplate>                                    
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>   
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdChallans" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" Visible="false">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Client" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ClientName") %>' ToolTip='<%# Eval("ClientName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="upAction" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:LinkButton CommandArgument='<%# Eval("Download")%>'
                                            AutoPostBack="true" CommandName="Download" ID="lnkDownloadPF" runat="server"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="PF Challan(s)"
                                             Visible='<%# ShowHideDownloadButton((string)Eval("button")) %>'>
                                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download"/>
                                        </asp:LinkButton>

                                        <asp:LinkButton CommandArgument='<%# Eval("DownloadESI")%>'
                                            AutoPostBack="true" CommandName="Download" ID="lnkDownloadESI" runat="server"
                                            data-toggle="tooltip" data-placement="bottom" ToolTip="ESI Challan(s)"
                                             Visible='<%# ShowHideDownloadButton((string)Eval("button1")) %>'>
                                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download"/>
                                        </asp:LinkButton>
                                    </ContentTemplate>                                    
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>   
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-9 col-sm-9 col-xs-9 colpadding0 w75per">
                    <div runat="server" id="DivRecordsScrum" style="color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="lblStartRecord" CssClass="control-label" Font-Bold="true" runat="server" Text=""></asp:Label>
                            -
                        <asp:Label ID="lblEndRecord" CssClass="control-label" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" CssClass="control-label" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-1 colpadding0 w10per">
                    <div class="col-md-8 col-sm-8 float-right colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="height: 32px !important"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5"  />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-2 text-right colpadding0 w15per">
                    <div class="col-md-6 col-sm-6 col-xs-12 plr0 text-right">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 pr0">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>

                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>

        </div>
    </form>
</body>
</html>
