﻿
<%@ Page Title="Customer :: Setup/Onboarding" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusSPOCMGR.Master" Culture="en-GB"
    CodeBehind="RLCS_EmpCtc.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_EmpCtc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <style>
        .k-grid-content {
            min-height: 200px;
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            /*float: right;*/
            margin-right: 10px;
        }

        .my-custom-edit {
            content: "\e10b";
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .panel-heading .nav > li > a {
            font-size: 17px;
        }
    </style>

    <style>
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }


                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
                div.k-confirm
                {
                    width:411px;
                    left:500.5px;
                }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        

        $(document).ready(function () {
              
            $('#iframeAdd2').attr({'src':"/Setup/uploadEmpCTCfiles", "scrolling":"no",})
           
        });
          
    </script>

    <script>
       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span id="popupNotification"></span>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="#">Employee CTC Upload</asp:LinkButton>
                    </li>
                   
                   
                </ul>
            </header>
        </div>

       
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
          
            <div id="grid2" style="border: none;">
               <iframe id="iframeAdd2" style="width: 100%; height: 1130px; border: none";></iframe>
            </div>
        </div>
    </div>

</asp:Content>
