﻿<%@ Page Title="Commercial SetUp " Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCSCommercialSetUp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCSCommercialSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".tempcls").attr('style', 'height: 0px');

            var ifrurl = "<%= ConfigurationManager.AppSettings["RLCS_WEB_URL"].ToString() %>" + "/Client_Setup/Commercial_Setup/ClientCommercialSetup";

            $('#iFrameDialog').attr('src', ifrurl);

        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div id="divDialog" title="" style="display: block; height: 475px; max-height: 475px">
        <iframe id="iFrameDialog" src="about:blank" width="100%" height="100%" style="border: none;"></iframe>
    </div>
</asp:Content>
