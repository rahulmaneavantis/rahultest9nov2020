﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternalComplianceStatusTransactionCA.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.InternalComplianceStatusTransactionCA" %>



<script type="text/javascript">
    $(function () {
        $('#divComplianceDetailsDialog').dialog({
            height: 600,
            width: 800,
            autoOpen: false,
            draggable: true,
            title: "Change Status",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    });

    function initializeComboboxUpcoming() {
        $("#<%= ddlStatus.ClientID %>").combobox();
    }

    function initializeDatePickerforPerformer(date) {
        var startDate = new Date();
        $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1
        });

        if (date != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
          }
      }


   // var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
    var validFilesTypes = ["exe", "bat", "dll"];
      function ValidateFile() {

          var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }


            for (var i = 0; i < FileUpload1.length; i++) {
                var fileExtension = FileUpload1[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";             
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

</script>

<div id="divComplianceDetailsDialog" style="display: none">

    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="true"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <fieldset style="border-style: solid; border-width: 1px; border-color: gray;">
                    <legend>Compliance Details</legend>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                            maximunsize="300px" autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;" maximunsize="300px"
                            autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;" maximunsize="300px"
                            autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:LinkButton ID="lbDownloadSample" Style="width: 300px; font-size: 13px; color: #333;"
                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                        <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                            runat="server" ID="rfvFile" ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                </fieldset>
                <div runat="server" id="divDeleteDocument" style="text-align: left;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                        <table width="100%">
                            <tr>
                                <td style="width: 50%">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Compliance Related Documents</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblWorkingFiles">
                                                        <thead>
                                                            <th>Compliance Working Files</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                            </asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>

                    </fieldset>
                </div>
                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Status</label>
                        <asp:DropDownList runat="server" ID="ddlStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Upload Compliance Document(s)</label>
                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" />
                        <%--<asp:label style="font-size: 11px;  margin-left: 240px;" runat="server" text="(only Pdf,Execl,Jpeg and Jpg documents allowed)"></asp:label>--%>
                    </div>
                    <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Upload Working Files(s)</label>
                        <asp:FileUpload ID="FileUpload1" Multiple="Multiple" runat="server" />
                        <%-- <asp:label runat="server" style="font-size: 11px;  margin-left: 240px;" text="(only Pdf,Execl,Jpeg and Jpg documents allowed)"></asp:label>--%>
                    </div>
                  
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Remarks</label>
                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" Style="height: 50px; width: 388px;" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 240px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="tbxDate" Style="height: 20px; width: 390px;" />
                        <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 240px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button" OnClientClick="if (!ValidateFile()) return false;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" />
                    </div>
                </fieldset>
                <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                    <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                        AllowPaging="true" PageSize="12" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                        BorderStyle="Solid" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging" OnRowCreated="grdTransactionHistory_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnSorting="grdTransactionHistory_Sorting"
                        DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" SortExpression="CreatedByText" />
                            <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn">
                                <ItemTemplate>
                                    <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#e1e1e1" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                </div>
                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lbDownloadSample" />
        </Triggers>
    </asp:UpdatePanel>
</div>
