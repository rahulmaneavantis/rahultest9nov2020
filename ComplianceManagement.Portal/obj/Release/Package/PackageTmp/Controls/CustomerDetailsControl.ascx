﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerDetailsControl.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CustomerDetailsControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script type="text/javascript">
    $(function () {
        $('#divCustomersDialog').dialog({
            height: 580,
            width: 750,
            autoOpen: false,
            draggable: true,
            title: "Customer Information",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });
  
    function checkAll(cb) {
        var ctrls = document.getElementsByTagName('input');
        for (var i = 0; i < ctrls.length; i++) {
            var cbox = ctrls[i];
            if (cbox.type == "checkbox" && cbox.id.indexOf("chkProduct") > -1) {
                cbox.checked = cb.checked;
            }
        }
    }
    
    function UncheckHeader() {
        var rowCheckBox = $("#RepeaterTable input[id*='chkProduct']");
        var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkProduct']:checked");
        var rowCheckBoxHeader = $("#RepeaterTable input[id*='ProductSelectAll']");
        if (rowCheckBox.length == rowCheckBoxSelected.length) {
            rowCheckBoxHeader[0].checked = true;
        } else {

            rowCheckBoxHeader[0].checked = false;
        }
    }

    
    function initializeJQueryUI() {        
        $("#<%= txtProductType.ClientID %>").unbind('click');

        $("#<%= txtProductType.ClientID %>").click(function () {            
            $("#dvProduct").toggle("blind", null, 500, function () { });
        });
    }
   
    function initializeDatePicker(date) {

        var startDate = new Date();
        $("#<%= txtStartDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
            }
        });

        $("#<%= txtEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
            }
        });

        if (date != null) {
            $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
            $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }
        
    function initializeCombobox() {
        $("#<%= ddlCustomerStatus.ClientID %>").combobox();
        $("#<%= ddlLocationType.ClientID %>").combobox();
        $("#<%= ddlTaskApplicable.ClientID %>").combobox();
        $("#<%= ddlSPName.ClientID %>").combobox();
        $("#<%= ddlComplianceApplicable.ClientID %>").combobox();
        $("#<%= ddlLabelApplicable.ClientID %>").combobox();
        $("#<%= ddlComplianceProductType.ClientID %>").combobox();
    }   
</script>

<style>
      .float-child {
            width: 80%;
            float: left;   
        }  
        .float-child1 {
            width: 10%;
            float: left;   
        }  
</style>
<div id="divCustomersDialog">
    <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="CustomerValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="CustomerValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                </div>
                <div id="issprd" runat="server"  visible="false"  style="margin-bottom: 7px;">
                    <asp:Label Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label Style="width: 230px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Is Service Provider</asp:Label>
                    <asp:CheckBox ID="chkSp" runat="server" AutoPostBack="true" OnCheckedChanged="chkSp_CheckedChanged" />
                </div>
                <div style="margin-bottom: 7px">
                    <asp:Label ID="lblast" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="lblsp" Style="width: 230px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Service Provider Name</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlSPName" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" AutoPostBack="true"
                        CssClass="custom-combobox" OnSelectedIndexChanged="ddlSPName_SelectedIndexChanged">                        
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px">
                    <asp:Label ID="lblRegAst" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="lblRegBy" Style="width: 230px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Registration By</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlRegistrationBy" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="custom-combobox">                        
                    </asp:DropDownList>
                </div>                 
                <div style="margin-bottom: 7px; margin-top:7px; display:none;" class="float-container">
                    <div class="float-child">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                            Gst Details</label>
                        <asp:TextBox runat="server" ID="tbxGstNumber" CssClass="form-control" autocomplete="off" Style="width: 300px;" MaxLength="500" />
                    </div>
                    <div class="float-child1" id="divgst" runat="server" >
                        <asp:Button Text="Get Details" runat="server" ID="btrgetGstdetails"  OnClick="btrgetGstdetails_Click" CssClass="btn btn-primary" />
                    </div>
                </div>
          <%--        <div class="clearfix" style="height:21px;"></div>--%>
                <div style="margin-bottom: 7px;    margin-top: 7px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Name</label>
                    <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 390px;" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                        ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Address</label>
                    <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                        TextMode="MultiLine" />
                </div>
                 
                <div style="margin-bottom: 7px; margin-top:7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Pan</label>
                    <asp:TextBox runat="server" ID="tbxpan" class="form-control" Style="width: 300px;"
                        MaxLength="150" />
                </div>
                <div style="margin-bottom: 7px; display:none; ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Location Type</label>
                    <asp:DropDownList runat="server" ID="ddlLocationType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Name</label>
                    <asp:TextBox runat="server" ID="tbxBuyerName" Style="height: 16px; width: 390px;"
                        MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                        ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Contact No</label>
                    <asp:TextBox runat="server" ID="tbxBuyerContactNo" Style="height: 16px; width: 390px;"
                        MaxLength="15" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty."
                        ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxBuyerContactNo" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                        ValidationGroup="CustomerValidationGroup" ErrorMessage="Please enter only 10 digit."
                        ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>
                <%--<div style="margin-bottom: 7px" class="float-container">
                    <div class="float-child">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                            Buyer Email</label>
                        <asp:TextBox runat="server" ID="tbxBuyerEmail" Style="height: 16px; width: 300px;"
                            MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                            ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                            ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="float-child1" id="divemail" runat="server">
                         <asp:Button Text="Add Email " runat="server" ID="btnAddEmail"  OnClick="btnAddEmail_Click" CssClass="btn btn-primary" />
                    </div>
                </div>--%>
                <div style="margin-bottom: 7px" class="float-container">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Email</label>
                    <asp:TextBox runat="server" ID="tbxBuyerEmail" Style="height: 16px; width: 390px;"
                        MaxLength="200" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                        ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                        ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </div>
              <%-- <div class="clearfix" style="height:21px;"></div>--%>
                <div style="margin-bottom: 7px;  margin-top:7px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Customer Status</label>
                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>
                <div style="margin-bottom: 7px;  display:none; ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Start Date</label>
                    <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px;  display:none; ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        End Date</label>
                    <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 390px;" ReadOnly="true"
                        MaxLength="200" />
                </div>
                <div style="margin-bottom: 7px; display:none; ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Disk Space</label>
                    <asp:TextBox runat="server" ID="txtDiskSpace" Style="height: 16px; width: 390px;"
                        MaxLength="200" />
                </div>

                <div style="margin-bottom: 7px; display:none;  ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Internal Compliance Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px;  display:none; ">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Task Management Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlTaskApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Label Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlLabelApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                        Compliance Product Type</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceProductType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="custom-combobox">
                        <asp:ListItem Text="Avacom" Value="0" Selected="True" />
                        <asp:ListItem Text="HRProduct Only" Value="2" />
                        <asp:ListItem Text="Avacom + TL" Value="3" />
                        <asp:ListItem Text="Avacom + HRProduct" Value="4" />
                    </asp:DropDownList>
                </div>
                <div style="margin-bottom: 7px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                      Assign Product(s)</label>
                    <%--<asp:DropDownList runat="server" ID="ddlProductType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;" CssClass="custom-combobox">                        
                    </asp:DropDownList>--%>

                    <asp:TextBox runat="server" ID="txtProductType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="display: none; margin-left: 240px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvProduct">

                            <asp:Repeater ID="rptProductType" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ProductSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 265px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" /></td>                                            
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkProduct" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblProductID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>

                </div>


                <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                        ValidationGroup="CustomerValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divCustomersDialog').dialog('close');" />
                </div>
            </div>

            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
