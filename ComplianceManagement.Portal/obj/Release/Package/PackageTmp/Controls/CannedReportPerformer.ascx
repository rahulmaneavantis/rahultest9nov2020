﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CannedReportPerformer.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CannedReportPerformer" %>

<script type="text/javascript">

    function fComplianceOverviewPerformer(obj) {
        OpenOverViewpupPerformer($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
    }

    function fComplianceOverviewInternalPerformer(obj) {
        OpenOverViewpupInternalPerformer($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
    }

    function hidediv() {
        var div = document.getElementById('AdvanceSearch');
        div.style.display == "none" ? "block" : "none";
        $('.modal-backdrop').hide();
        return true;
    } 
    $(document).on("click", "#ContentPlaceHolder1_udcCannedReportPerformer_upCannedReportPerformer", function (event) {

        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                $("#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer").show();
            } else {
                $("#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer").hide();
            }
        }
        else if (event.target.id != "ContentPlaceHolder1_udcCannedReportPerformer_tbxFilterLocationPerformer") {
            $("#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationPerformer') > -1) {
            $("#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer").show();
        } else if (event.target.id == "ContentPlaceHolder1_udcCannedReportPerformer_tbxFilterLocationPerformer") {
            $("#ContentPlaceHolder1_udcCannedReportPerformer_tbxFilterLocationPerformer").unbind('click');

            $("#ContentPlaceHolder1_udcCannedReportPerformer_tbxFilterLocationPerformer").click(function () {
                $("#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer").toggle("blind", null, 500, function () { });
            });

        }
    });
    <%--function initializeDatePicker11(date2) {
        var startDate = new Date();
        $('#<%= txtStartDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1
        });

        if (date2 != null) {
            $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
        }
    }

    function initializeDatePicker12(date1) {
        var startDate = new Date();
        $('#<%= txtEndDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1
        });

        if (date1 != null) {
            $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date1);
        }
    }--%>  
   
    function myFunction() {
        $('#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer').show();
    }
  
</script>

<asp:UpdatePanel ID="upCannedReportPerformer" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
    <ContentTemplate>

        <div style="float: right; margin-right: 10px; margin-top: -35px; display: none">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
            <%--<asp:Button ID="btnCheck" OnClick="btnCheck_Click" Visible ="false" runat="server" Text="" />--%>
        </div>

        <div class="col-md-12 colpadding0" >
            <div class="col-md-2 colpadding0" style="width: 108px;">
                <div class="col-md-3 colpadding0" style="float:left;width: 36%;">
                    <p style="color: #999; margin-top:5px;">Show </p>
                </div>

                <div class="col-md-3 colpadding0" style="float:left;">
                       <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 70px; float: left">
                    <asp:ListItem Text="5" Selected="True" />
                    <asp:ListItem Text="10" />
                    <asp:ListItem Text="20" />
                    <asp:ListItem Text="50" />
                </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-10 colpadding0" style="text-align: right; float: left;width: 87.7%;margin-left: 10px;">
                <%--<div class="col-md-8 colpadding0">--%>
                <div style="margin-left: 1px;">
                    <asp:DropDownList runat="server" ID="ddlComplianceType" style="margin-right: 1%;" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" class="form-control m-bot15 search-select">
                        <asp:ListItem Text="Statutory" Value="-1" />
                        <asp:ListItem Text="Internal" Value="0" />
                        <asp:ListItem Text="Event Based" Value="1" />
                        <asp:ListItem Text="Statutory CheckList" Value="2" />
                        <asp:ListItem Text="Event Based CheckList" Value="4" />
                        <asp:ListItem Text="Internal CheckList" Value="3" />
                    </asp:DropDownList>
                </div>
                <div>
                    <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select" style="margin-right: 1%;max-width: 80px;">
                        <asp:ListItem Text="Risk" Value="-1" />
                        <asp:ListItem Text="High" Value="0" />
                         <asp:ListItem Text="Medium" Value="1" />
                        <asp:ListItem Text="Low" Value="2" />
                       
                    </asp:DropDownList>
                     </div>
                <div>
                    <asp:DropDownList runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" class="form-control m-bot15 search-select"  style="margin-right: 1%;">
                    </asp:DropDownList>
                </div>

                    <div style="float: left; margin-right: 1%;" id="divloc">
                        <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationPerformer" onfocus="myFunction()" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 310px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                            CssClass="txtbox" />
                        <div style="margin-left: 1px; position: absolute; z-index: 10;" id="divFilterLocationPerformer" runat="server">
                            <asp:TreeView runat="server" ID="tvFilterLocationPerformer" SelectedNodeStyle-Font-Bold="true" Width="309px" NodeStyle-ForeColor="#8e8e93"
                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                OnSelectedNodeChanged="tvFilterLocationPerformer_SelectedNodeChanged" >
                            </asp:TreeView>
                        </div>
                    </div>

                    <%--<asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 search-select">
                    </asp:DropDownList>--%>
                    <div>
                         <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" runat="server" Text="Apply" />
                    </div>

                    <div>
                        <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
                    </div>            
            </div>

            <!--advance search starts-->
            <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 1000px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            
                        </div>
                        <div class="modal-body" style="margin-left:50px">
                            <h2 style="text-align: center;margin-top:10px;">Advanced Search</h2>
                            <div class="col-md-12 colpadding0">
                                <div class="table-advanceSearch-selectOpt">
                                    <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                    </asp:DropDownList>
                                </div>
                                <div class="table-advanceSearch-selectOpt">
                                    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                    </asp:DropDownList>
                                </div>
                                <asp:Panel ID="PanelAct" runat="server">
                                    <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:DropDownList runat="server" ID="ddlAct"  class="form-group form-control">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="Panelsubtype" runat="server">
                                    <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:DropDownList runat="server" ID="ddlComplianceSubType" class="form-group form-control">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchType" runat="server">
                                <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:TextBox runat="server"  
                                            style="padding-left:7px;" placeholder="Type To Filter"
                                             class="form-group form-control" ID="txtSearchType"
                                             CssClass="form-control"  onkeydown = "return (event.keyCode!=13);"/>
                                </div></asp:Panel>
                                
                                <asp:Panel ID="Panel1" runat="server">
                                      <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                       <asp:TextBox runat="server" style="padding-left:7px;" placeholder="From Date" 
                                       ID="txtStartDate" CssClass="StartDate form-group form-control"/>
                                    </div>  </asp:Panel>

                                  <asp:Panel ID="Panel2" runat="server">
                                      <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                      <asp:TextBox runat="server" 
                                           style="padding-left:7px;" placeholder="To Date" 
                                          CssClass="StartDate form-group form-control" ID="txtEndDate"/>
                                     </div> 
                                 </asp:Panel>

                                 <div class="clearfix" style="clear:both;"></div>
                                <div class="table-advanceSearch-buttons" > 
                                    <asp:Button ID="btnAdvSearch" Text="Search"  class="btn btn-search" 
                                         OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                                    <button type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                    

                                </div>
                                <br />
                               
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--advance search ends-->
        </div>  
        
        <div class="clearfix">      
 <div class="col-md-12 colpadding0" style="text-align: right; float: right">
 <div id="divEvent" class="col-md-10 colpadding0" style="text-align: right; float: right" runat="server" visible="false">
            <div style="float:left;margin-right: 2%;">
             <asp:DropDownList runat="server" ID="ddlEvent"  OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15" style="width:255px;">                
            </asp:DropDownList>         
            </div>
            <div style="float:left;margin-right: 2%;">
            <asp:DropDownList runat="server" ID="ddlEventNature"  class="form-control m-bot15" style="width:255px;" >               
            </asp:DropDownList>
            </div>
        </div>
    </div> 

        <div class="col-md-12 AdvanceSearchScrum">
            <div id="divAdvSearch" runat="server" visible="false">
                <p>
                    <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                </p>
                <p>
                    <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                </p>
            </div>

            <div runat="server" id="DivRecordsScrum" style="float: right;">
                <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
            </div>
        </div>

         
        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" CssClass="table"
            OnRowCreated="grdComplianceTransactions_RowCreated" GridLines="none" OnRowCommand="grdComplianceTransactions_RowCommand"
            AllowPaging="True" PageSize="5" OnSorting="grdComplianceTransactions_Sorting" BorderWidth="0px" OnRowDataBound="grdComplianceTransactions_RowDataBound"
            DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
            <HeaderStyle CssClass="clsheadergrid" />
            <RowStyle CssClass="clsROWgrid" />
            <Columns>
                
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                            <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
             

                <asp:TemplateField HeaderText="Reviewer">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>       
                       
                <asp:BoundField DataField="ForMonth" HeaderText="Period" />

                <asp:TemplateField HeaderText="Due Date">
                    <ItemTemplate>
                        <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                        <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Status" HeaderText="Status" />

                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnHistory" runat="server" CommandName="View_History" Text="View_History" CommandArgument='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
              <%--  <asp:BoundField DataField="Name" Visible="false" />
                <asp:BoundField DataField="Risk" Visible="false" />
                <asp:BoundField DataField="ComplianceStatusID" Visible="false" />
                <asp:BoundField DataField="ScheduledOnID" Visible="false" />
                <asp:BoundField DataField="ComplianceInstanceID" Visible="false" />
                <asp:BoundField DataField="EventName" Visible="false"/>
                <asp:BoundField DataField="EventNature" Visible="false"/>--%>
                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="upDownloadFileperformer" runat="server">
                            <ContentTemplate>
                                <asp:ImageButton ID="lblOverView3" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                    OnClientClick='fComplianceOverviewPerformer(this)' ToolTip="Click to OverView"></asp:ImageButton>
                            </ContentTemplate>
                             <Triggers>
                                <asp:AsyncPostBackTrigger ControlID ="lblOverView3" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" />
            <PagerTemplate>
                <table style="display: none">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
            <EmptyDataTemplate>
                No Records Found
            </EmptyDataTemplate>
        </asp:GridView>

        <asp:GridView Visible="false" CssClass="table" BorderWidth="0px" runat="server" ID="grdComplianceTransactionsInt" AutoGenerateColumns="false" AllowSorting="true"
            GridLines="none" AllowPaging="True" PageSize ="5" DataKeyNames="InternalComplianceInstanceID" OnRowDataBound="grdComplianceTransactionsInt_RowDataBound">
            <HeaderStyle CssClass="clsheadergrid" />
            <RowStyle CssClass="clsROWgrid" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                 <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                            <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

               <asp:TemplateField HeaderText="Reviewer">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewerInternal((long)Eval("InternalComplianceInstanceID")) %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField> 

                <asp:BoundField DataField="ForMonth" HeaderText="Period" />
               
                <asp:TemplateField HeaderText="Due Date">
                    <ItemTemplate>
                        <asp:Label ID="lblInternalScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                    
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Status" HeaderText="Status" />                 
               
                <asp:BoundField DataField="Risk" Visible="false" />
                <asp:BoundField DataField="InternalComplianceStatusID" Visible="false" />
                <asp:BoundField DataField="InternalScheduledOnID" Visible="false" />
                <asp:BoundField DataField="InternalComplianceInstanceID" Visible="false" />               
                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="upDownLoadperformerInternal" runat="server">
                            <ContentTemplate>
                                <asp:ImageButton ID="lblOverView4" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID")  %>'
                                    OnClientClick='fComplianceOverviewInternalPerformer(this)' ToolTip="Click to OverView"></asp:ImageButton>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID ="lblOverView4"/>
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" />
            <PagerTemplate>
                <table style="display: none">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </PagerTemplate>
             <EmptyDataTemplate>
                No Records Found
            </EmptyDataTemplate>
        </asp:GridView>

        <div class="col-md-12 colpadding0">
            <div class="col-md-6 colpadding0">
            </div>
            <div class="col-md-6 colpadding0">
                <div class="table-paging">
                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                    <div class="table-paging-text">
                        <p>
                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                </div>
            </div>
        </div>
        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
    </ContentTemplate>
</asp:UpdatePanel>

<div id="divActDialog111">
    <%--<asp:UpdatePanel ID="UpDetailView" runat="server">
        <ContentTemplate>--%>

            <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false"
                GridLines="Both" CssClass="table"
                AllowPaging="True" PageSize="50"
                DataKeyNames="ComplianceTransactionID">
                <Columns>
                    <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" SortExpression="CreatedByText" />
                    <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn">
                        <ItemTemplate>
                            <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
     <%--   </ContentTemplate>
    </asp:UpdatePanel>--%>
</div>

   


<script type="text/javascript">

      function OpenOverViewpupPerformer(scheduledonid, instanceid) {
          $('#divOverViewPerformer').modal('show');
          $('#OverViewsPerformer').attr('width', '1150px');
          $('#OverViewsPerformer').attr('height', '600px');
      $('.modal-dialog').css('width', '1200px');
      $('#OverViewsPerformer').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

      }

      function OpenOverViewpupInternalPerformer(scheduledonid, instanceid) {
         
          $('#divOverViewInternalPerformer').modal('show');
          $('#OverViewsInternalPerformer').attr('width', '1050px');
          $('#OverViewsInternalPerformer').attr('height', '600px');
          $('.modal-dialog').css('width', '1100px');
          $('#OverViewsInternalPerformer').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

      }
</script>
