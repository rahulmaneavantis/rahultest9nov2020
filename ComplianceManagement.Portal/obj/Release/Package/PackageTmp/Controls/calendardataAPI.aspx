﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calendardataAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.calendardataAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" /> 
        
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <style type="text/css">
       .k-grid-content
        {
            
            max-height:210Px !important;  
        }
   .k-grid-content k-auto-scrollable {
                    height: auto;
                }  
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }
        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }        
        .k-grid td {
            line-height: 2.0em;
        }
        .k-i-more-vertical:before {
            content: "\e006";
        }
        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }
        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }
        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -3px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }
          .k-edge .k-pager-info, .k-ff .k-pager-info, .k-ie11 .k-pager-info, .k-safari .k-pager-info, .k-webkit .k-pager-info {
            display: block;
        }
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
          .change-condition {
            color: blue;
        }

.k-pager-wrap .k-label {
    
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    margin: 0 0.7em;
}

    </style>
   
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
      
    <script type="text/x-kendo-template" id="template">      
               
    </script>
    
    <script id="StatusTemplate" type="text/x-kendo-template">                 
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetStatusType(ComplianceStatusID,ScheduledOn)# 
    </script>

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script type="text/javascript">

        function GetStatusType(value,ScheduledOn) {
            
            if (value == 5) {
                return '<img src="/Images/delayed.png"  />';
            }
            else if (value == 4 || value == 7 || value == 15) {
                return '<img src="/Images/completed.png"  />';
            }
            else if (value != 5 || value != 4) {

                var todaydate = new Date();
                var Scheduleddate = new Date(ScheduledOn);
                
                if (Scheduleddate = todaydate) {
                    return '<img src="/Images/upcoming.png"  />';
                }                
                else
                {
                    if (Scheduleddate < todaydate) {
                        return '<img src="/Images/overdue.png"  />';
                    }
                    else if (Scheduleddate >= todaydate) {
                        return '<img src="/Images/upcoming.png"  />';
                    }
                }
            }
            else {
                return "";
            }
        }

        function BindGrid()
        {

            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>Data/GetCalenderComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsApprover=<% =IsApprover%>&datevalue=<% =date%>&isallorsi=<% =isallorsi%>&isdh=<%=dhead%>&ismflag=' + $("#dropdownlistUserRole").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetCalenderComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsApprover=<% =IsApprover%>&datevalue=<% =date%>&isallorsi=<% =isallorsi%>&isdh=<%=dhead%>&ismflag=' + $("#dropdownlistUserRole").val()
                    },
                    schema: {
                        data: function (response) {   
				            window.parent.hideloader();                         
                            if ($("#dropdownlistUserRole").val() != undefined) {
                                if ($("#dropdownlistUserRole").val() == 3) {
                                    return response[0].PList;
                                }
                                else if ($("#dropdownlistUserRole").val() == 4) {
                                    return response[0].RList;
                                }
                            }
                            else {
                                 return response[0].PList;
                            }
                        },
                        total: function (response) {    
                            if ($("#dropdownlistUserRole").val() != undefined) {
                                if ($("#dropdownlistUserRole").val() == 3) {
                                    return response[0].PList.length;
                                }
                                else if ($("#dropdownlistUserRole").val() == 4) {
                                    return response[0].RList.length;
                                }
                            }
                            else {
                                return response[0].PList.length;
                            }
                        }
                    },

                },
                toolbar: kendo.template($("#template").html()),
                
                sortable: true,
                filterable: true,
                columnMenu: true,
                //pageable: true,
                pageable: {
			pageSize: 5,
    			input: true

  			},
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [                                    
                    {
                        field: "ShortDescription", title: 'Name',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },  
                    {
                        field: "ComplianceStatusID", title: "Status",
                        template: kendo.template($('#StatusTemplate').html()), width: "5%",
                        attributes: {
                            "class": "table-cell",
                            style: "text-align: Center;"
                        }
                    },
                    {
                        field: "CType", title: 'Type',
                        width: "5%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    field: "ScheduledOn", title: 'Due Date',
                    //    type: "date",
                    //    hidden: true,
                    //    template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    //    filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview",
                                visible: function (dataItem)
                                {
                                    if (dataItem.flag === true) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                    <%--if (<% =UploadDocumentLink%> == 0)
                                    {
                                        if (dataItem.flag === true) {
                                            return true;
                                        }
                                        else {
                                            return false;
                                        }
                                    }
                                    else {
                                        return false;
                                    }--%>
                                }
                            },
                                {
                                name: "edit3", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMGMT",
                                    visible: function ()
                                    {
                                      if (<% =UploadDocumentLink%> == 1) {
                                          return true;
                                    }
                                      else {
                                          return false;
                                    }                               
                                }
                            }
                        ], title: "Action", lock: true, width: "5%;",// width: 150,
                    }
                ]
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {               
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));               
                 parent.OpenPerrevpopup(item.ScheduledOnID, item.ComplianceInstanceID, item.Interimdays,
                    item.CType, item.RoleID, item.ScheduledOn);
                 return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overviewMGMT", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                parent.OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.CType);
                return true;
            });
$("#grid").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit3",
                content: function (e) {
                    return "Overview";
                }
            });
        }

        $(document).ready(function () {
                        
            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    BindGrid();
                },
                dataSource: [
                 <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

             BindGrid();   
            var grid = $("#grid").data("kendoGrid");
grid.dataSource.sort({field: "ComplianceStatusID", dir: "asc"});
        });

    </script>
    
  </head>
    <body>
  <form>     
      <div id="example">
          <div id="roledisplay" runat="server" >
            <input id="dropdownlistUserRole" data-placeholder="Role"  style="width:242px;">    
          </div>
          <div id="grid" style="border: none;"></div>
      </div>       
  </form>
        </body>
 </html>



