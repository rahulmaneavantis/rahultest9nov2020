﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="frmUpcomingCompliancessNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.frmUpcomingCompliancessNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">      
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="../Newjs/Kendo/MyWorkspace.js"></script>
    <style type="text/css">

        span.k-icon.k-i-arrow-60-down {
            margin-top: 5px;
        }

            .k-grid-content
        {
            min-height:394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }
        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }      
        
        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }
          
        .k-grouping-header 
        {
            border-right: 1px solid;
            border-left: 1px solid;
        }

        .k-grid td {
            line-height: 2.0em;
        }
        .k-i-more-vertical:before {
            content: "\e006";
        }
        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }
        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }
        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
          .change-condition {
            color: blue;
        }
    </style>
   
    <title></title>
      <%if (Session["filter"] ==null)  {  
          Session["filter"] = "Upcoming";
            }%> 
     
               
   
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>


    <script type="text/x-kendo-template" id="template"> 
       <div class=row style="padding-bottom: 4px;">
            <div class="toolbar">   
                    <input id="dropdownlistUserRole" data-placeholder="Role"  style="width:242px;">   
                    <input id="dropdownlistMoreLink" data-placeholder="Role"  style="width:242px;">   
            </div>
        </div>
    <div class=row style="padding-bottom: 4px;">
            <div class="toolbar">               
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:242px;">            
                <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">                  
                <input id="dropdownlistRisk" data-placeholder="Risk">                  
                <input id="dropdownlistStatus" data-placeholder="Status">
                <input id="dropdownlistTypePastdata" data-placeholder="Status">                                
                <button id="export" onclick="exportReport(event)"  class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;display:none;"></button>        
                <button id="AdavanceSearch" style="height: 23px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>                
            </div>
    </div> 
           
         <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 13.6%;;">
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 10px;">
                            <div id="dvdropdownEventName" style="display:none;"><input id="dropdownEventName" data-placeholder="Event Name" style="width:175px;"></div>          
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">
                           <div id="dvdropdownEventNature" style="display:none;"><input id="dropdownEventNature" data-placeholder="Event Nature"></div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%;padding-left: 22px;">                             
                             <button id="ClearfilterMain" style="float: right; margin-left: 1%;display:none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>                                         
                             <button id="dvbtndownloadDocumentMain" style="float: right;display:none;" onclick="selectedDocumentMain(event)">Download</button>                                                                         
                        </div>
                    </div>
                </div>                           
       
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtersstoryboard">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtertype">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterrisk">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterstatus">&nbsp;</div>

    </script>


    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function gotoMoreLink() {

            //update Penalty
            if ($("#dropdownlistMoreLink").val() == 1)
            {
                if ($("#dropdownlistUserRole").val() == 3)
                {
                    window.location.href = "../Penalty/PenaltyUpdation.aspx";
                }
                else {
                    window.location.href = "../Penalty/PenaltyUpdationReviewer.aspx";
                }
            }

            //Revise Compliance
            else if ($("#dropdownlistMoreLink").val() == 2)
            {
                if ($("#dropdownlistComplianceType").val() == 0)
                {
                    window.location.href = "../InternalCompliance/ReviseCompliancesInternal.aspx";
                }
                else {
                    window.location.href = "../Compliances/ReviseCompliances.aspx";
                }
            }

            //License Workspace
            else if ($("#dropdownlistMoreLink").val() == 5)
            {
                window.location.href = "../Compliances/ComplianceLicenseList.aspx";
            }

            //Reassign Performer(only for Reviewer)
            else if ($("#dropdownlistMoreLink").val() == 3)
            {
                if ($("#dropdownlistUserRole").val() == 4)
                {
                    window.location.href = "../Compliances/ReassignReviewerToPerformer.aspx";
                }
                 else { }
            }

            //Task(only for Perfomer)
            else if ($("#dropdownlistMoreLink").val() == 4)
            {
                if ($("#dropdownlistUserRole").val() == 3)
                {
                    window.location.href = "../Task/myTask.aspx?type=Statutory";
                }
                else { }
            }

             //My Leave
            //else if ($("#dropdownlistMoreLink").val() == 6)
            //{
            //    window.location.href = "../Users/PerformerOnLeave.aspx";
            //}           
        } 

        $(document).ready(function () {
            var FlagLocation = "S";
            if (<% =ComplianceTypeID%> == 0) {
                FlagLocation = "I";
            }

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }
            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    "Minimize",
                    //"Maximize",
                    "Close"
                ],
                close: onClose
            });

            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });

            function onChange() {

                $('#filterStartDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#filterStartDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterStartDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterStartDate').append('Start Date:&nbsp;');
                    $('#filterStartDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                }
                DateFilterCustom();
            }

            function DateFilterCustom() {

                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtndownloadDocument').css('display', 'none');
                $("#dropdownPastData").data("kendoDropDownList").select(4);
                var setStartDate = $("#Startdatepicker").val();
                var setEndDate = $("#Lastdatepicker").val();
                if (setStartDate != null || setEndDate != null) {
                    DataBindDaynamicKendoGrid();
                }
                if (setStartDate != null) {
                    $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
                }
                if (setEndDate != null) {
                    $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
                }
                FilterAllAdvancedSearch();
            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });

            function onChange1() {
                $('#filterLastDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterLastDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterLastDate').append('End Date&nbsp;&nbsp;:&nbsp;');

                    $('#filterLastDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                }
                DateFilterCustom();
            }

            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({

                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY='
                    },
                    schema: {
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased.length;
                            }
                        }
                    },
                    pageSize: 10,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                change: onChange,
                dataBound: OnGridDataBoundAdvanced,
                columns: [
                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Performer", title: "Performer" },
                    { hidden: true, field: "Reviewer", title: "Reviewer" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        attributes: {
                            style: 'white-space: nowrap;'

                        },filterable: {
                            extra: false,                             
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",

                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,

                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "state", title: 'state',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "region", title: 'region',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "zone", title: 'zone',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overviewMain"

                            },
                             { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMainnew" }

                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });

            function onSorting(arg) {
                settracknew('My Workspace', 'Sorting', arg.sort.field, '');
             
            }

            function onPaging(arg) {
                settracknew('My Workspace', 'Paging', arg.page, '');	
            }

            function OnGridDataBoundAdvanced(e) {
                var grid = $("#grid1").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($("#dropdownlistUserRole1").val() == 3) {
                        if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Upcoming" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d || gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Upcoming" || gridData[i].Status == "Overdue" || gridData[i].Status == "Rejected" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMain");
                            createUserButton.hide();
                        }
                        if(gridData[i].Status == "Overdue")
                        {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }
                        else{
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }

                    }
                    if ($("#dropdownlistUserRole1").val() == 4) {
                        if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Submitted For Interim Review" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d) {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Submitted For Interim Review" ||  gridData[i].Status == "Pending For Review" || gridData[i].Status == "Revise Compliance") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMain");
                            createUserButton.hide();
                        }
                        if(gridData[i].Status == "Overdue")
                        {

                        }
                        else{
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewMainnew");
                            createUserButton.hide();
                        }
                      
                       
                    }
                }
            }

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "overview";
                }
            });

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


            $("#grid1").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


            $("#grid1").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(8)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "td:nth-child(9)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            var grid = $("#grid").kendoGrid({

                dataSource: {
                    transport: {
                      read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>&StatusFlagID=<% =StatusFlagID%>&FlagPR=<% =UserRoleID%>&MonthId=All&FY='
                    },
                    schema: {
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased;
                            }
                        },
                        total: function (response) {
                            if (<% =ComplianceTypeID%> == -1) {
                                return response[0].Statutory.length;
                            }
                            else if (<% =ComplianceTypeID%> == 0) {
                                return response[0].Internal.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].EventBased.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBound,
                columns: [
                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Performer", title: "Performer" },
                    { hidden: true, field: "Reviewer", title: "Reviewer" },
                    {
                        field: "Branch", title: 'Location',
                        width: "18.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                            attributes: {
                            style: 'white-space: nowrap;'

                        },filterable: {
                            extra: false,                         
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",

                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CloseDate", title: "Close Date",
                        type: "date",
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(CloseDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(CloseDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(CloseDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "state", title: 'state',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "region", title: 'region',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "zone", title: 'zone',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview",
                            },
                             { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewnew" }
                        ], title: "Action", lock: true, width: "7%;",// width: 150,
                    }
                ]
            });

            function OnGridDataBound(e) {
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($("#dropdownlistUserRole").val() == 3) {
                        if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Upcoming" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d || gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Upcoming" || gridData[i].Status == "Overdue" || gridData[i].Status == "Rejected" || gridData[i].Status == "Interim Review Approved" || gridData[i].Status == "Interim Rejected") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overview");
                            createUserButton.hide();
                        }
                        if(gridData[i].Status == "Overdue")
                        {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }
                        else{
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }

                    }
                    if ($("#dropdownlistUserRole").val() == 4)
                    {
                        if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1) {
                            var d = new Date();
                            if (gridData[i].Status == "Submitted For Interim Review" && kendo.parseDate(gridData[i].ScheduledOn, 'yyyy-MM-dd') > d) {
                                if (gridData[i].Interimdays != null && gridData[i].Interimdays != undefined) {
                                    if (gridData[i].Interimdays != 0) {
                                        var row = grid.table.find("tr[data-uid='" + currentUid + "']");
                                        $.each(row.find('td'), function (index, element) {
                                            $(element).addClass("change-condition");
                                        });
                                    }
                                }
                            }
                        }
                        if (gridData[i].Status == "Submitted For Interim Review" || gridData[i].Status == "Pending For Review"  || gridData[i].Status == "Revise Compliance") {

                        }
                        else {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overview");
                            createUserButton.hide();
                        }
                        if(gridData[i].Status == "Overdue")
                        {

                        }
                        else{
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var createUserButton = $(currentRow).find(".ob-overviewnew");
                            createUserButton.hide();
                        }
                    }
                }
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                //checkboxes: true,
                //checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" }
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('My Workspace', 'Filtering', 'Compliance Type', '')	
                }
            });

              $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                  change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Sequence', '')	
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                     read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S',
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("900");
                }
            });        

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Risk', '')	
                    FilterAllMain();

                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                     
                    settracknew('My Workspace','Filtering','Status, branch etc','');

                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Upcoming", value: "0" },
                    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    { text: "DueButNotSubmitted", value: "4" },
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')	
                    DataBindDaynamicKendoGrid();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            $("#dropdownlistMoreLink").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace','More Actions ',$("#dropdownlistMoreLink").data("kendoDropDownList").text(),'');
                    gotoMoreLink();
                },
                //index: 1,
                dataSource: [
                    { text: "More Actions", value: "-1" },
                    { text: "Update Penalty", value: "1" },
                    { text: "Revise Compliance", value: "2" },
                     <%if (ReviewerFlagID == 1)%>
                     <%{%>
                         { text: "Reassign Performer", value: "3" }, 
                     <%}%>                                          
                      <%if (PerformerFlagID == 1)%>
                     <%{%>
                         { text: "Update Tasks", value: "4" },
                     <%}%>
                   // { text: "License Workspace", value: "5" },
                   // { text: "Update Leave", value: "6" }                     
                ]
            });

               $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                   change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')	
                    DataBindDaynamicKendoGriddMain();
                },
                //index: 1,
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                        { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                        { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });
            

            $("#dropdownlistUserRole1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'User', '')	

                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                 <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Period', '')	
                    DataBindDaynamicKendoGriddMain();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
            var evalEventName = 0;
            if ($("#dropdownEventName").val() != '') {
                evalEventName = $("#dropdownEventName").val()
            }
            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Name', '')	
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName").val() != '') {
                            evalEventName = $("#dropdownEventName").val()
                        }

                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });


            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Nature', '')	

                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            var evalEventName1 = 0;

            if ($("#dropdownEventName1").val() != '') {
                evalEventName1 = $("#dropdownEventName1").val()
            }
            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Name', '')	
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName1").val() != '') {
                            evalEventName1 = $("#dropdownEventName1").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });


            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Event Nature', '')	
                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                filter: "contains",
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Location', '')	
                    FilterAllMain();

                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                    settracknew('My Workspace','Filtering','Status, branch etc','')

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                     read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + ''
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

     
            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                filter: "contains",
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Location', '')	
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + '',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + FlagLocation + ''
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Risk', '')	
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                     { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'Financial Year', '');	
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },                    
                    { text: "2020-2021", value: "2020-2021" },
		    { text: "2019-2020", value: "2019-2020" },
		    { text: "2018-2019", value: "2018-2019" },
		    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }  
                ]

            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    settracknew('My Workspace', 'Filtering', 'User', '')
                    FilterAllAdvancedSearch();

                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                     read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownList({
                placeholder: "Status",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Status', '');
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Upcoming", value: "0" },
                    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    { text: "DueButNotSubmitted", value: "4" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Compliance Type', '');
                    DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" }
                ]
            });

            $("#dropdownDept").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Department",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Department', '');
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                      read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=<% =ComplianceTypeID%>"
                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('My Workspace', 'Filtering', 'Act', '')
                    FilterAllAdvancedSearch();

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                }
            });

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });


            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });


            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overviewnew", function (e) {
                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupnew(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-overviewMainnew", function (e) {
               
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupnewone(item.ScheduledOnID, item.ComplianceInstanceID, item.Status);
                return true;
            });

     
        });

        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }
        
         function CloseModalInternalPerformer() {
            $('#ComplainceInternalPerformaer').modal('hide');
            return true;
        }

        function DataBindDaynamicKendoGriddMain() {

            $('#dvdropdownEventNature').css('display', 'none');
            $('#dvdropdownEventName').css('display', 'none');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $('#dvbtndownloadDocumentMain').css('display', 'none');
            $("#grid").data('kendoGrid').dataSource.data([]);

            if ($("#dropdownlistComplianceType").val() == -1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                     read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY='
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statutory;
                        },
                        total: function (response) {
                            return response[0].Statutory.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                            }
                        }
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else if ($("#dropdownlistComplianceType").val() == 1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                    read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY='
                    },
                    schema: {
                        data: function (response) {
                            return response[0].EventBased;
                        },
                        total: function (response) {
                            return response[0].EventBased.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                            }
                        }
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            else if ($("#dropdownlistComplianceType").val() == 0) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                    read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus").val() + '&FlagPR=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&FY='
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Internal;
                        },
                        total: function (response) {
                            return response[0].Internal.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" },
                            }
                        }
                    },
                    pageSize: 10
                });
                var grid = $('#grid').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }

            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            if ($("#dropdownlistComplianceType").val() == 1) {
                $('#dvdropdownEventNature').css('display', 'block');
                $('#dvdropdownEventName').css('display', 'block');

                //$("#grid").data("kendoGrid").showColumn(5);
                //$("#grid").data("kendoGrid").showColumn(6);

                //$("#grid").data("kendoGrid").hideColumn(4);
            }
            else {                 
                //$("#grid").data("kendoGrid").hideColumn(5);
                //$("#grid").data("kendoGrid").hideColumn(6);
                //$("#grid").data("kendoGrid").showColumn(4);
            }


            if ($("#dropdownlistComplianceType").val() == 0)//Internal
            {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
            }

        }
       
        function DataBindDaynamicKendoGrid() {

            $("#grid1").data('kendoGrid').dataSource.data([]);

            if ($("#dropdownFY").val() != "0") {
                $("#dropdownPastData").data("kendoDropDownList").select(4);
            }

            <%if (RoleFlag == 1)%>
            <%{%>
                $("#dropdownUser").data("kendoDropDownTree").value([]);
            <%}%>
            $('input[id=chkAll]').prop('checked', false);

            $("#grid1").data("kendoGrid").dataSource.filter({});
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#Clearfilter').css('display', 'none');
            $('#dvbtndownloadDocument').css('display', 'none');

            $("#dvdropdownACT").css('display', 'block');
            if ($("#dropdownlistComplianceType1").val() == 0)//Internal and Internal Checklist
            {
                $("#dvdropdownACT").css('display', 'none');
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);

                var dataSourceDept = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=0"
                    },
                });
                dataSourceDept.read();
                $("#dropdownDept").data("kendoDropDownList").setDataSource(dataSourceDept);
            }
            else {
                var dataSource12 = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                });
                dataSource12.read();
                $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);

                var dataSourceDept = new kendo.data.HierarchicalDataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=-1"
                    },
                });
                dataSourceDept.read();
                $("#dropdownDept").data("kendoDropDownList").setDataSource(dataSourceDept);
            }

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $('#dvdropdownEventNature1').css('display', 'block');
                $('#dvdropdownEventName1').css('display', 'block');

                //$("#grid1").data("kendoGrid").showColumn(5);//Event Name
                //$("#grid1").data("kendoGrid").showColumn(6);//Event Nature
                //$("#grid1").data("kendoGrid").hideColumn(4);//Branch
            }
            else {
                $('#dvdropdownEventNature1').css('display', 'none');
                $('#dvdropdownEventName1').css('display', 'none');

                //$("#grid1").data("kendoGrid").hideColumn(5);//Event Name
                //$("#grid1").data("kendoGrid").hideColumn(6);//Event Nature
                //$("#grid1").data("kendoGrid").showColumn(4);//Branch
            }

            if ($("#dropdownlistComplianceType1").val() == -1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=-1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statutory;
                        },
                        total: function (response) {
                            return response[0].Statutory.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" }
                            }
                        }
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            if ($("#dropdownlistComplianceType1").val() == 0) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=0&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].Internal;
                        },
                        total: function (response) {
                            return response[0].Internal.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" }
                            }
                        }
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }
            if ($("#dropdownlistComplianceType1").val() == 1) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/KendoMyWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&ComplianceType=1&StatusFlagID=' + $("#dropdownlistStatus1").val() + '&FlagPR=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {
                            return response[0].EventBased;
                        },
                        total: function (response) {
                            return response[0].EventBased.length;
                        },
                        model: {
                            fields: {
                                ScheduledOn: { type: "date" }
                            }
                        }
                    },
                    pageSize: 10,
                });
                var grid = $('#grid1').data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
            }

             if ($("#dropdownlistComplianceType1").val() == 0)
            {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I'
                    }
                });
                dataSourceSequence.read();
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
            else
            {
                 var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S'
                    }
                });
                dataSourceSequence.read();
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
        }

        function CloseModalPerFormer() {
            
            $('#ComplaincePerformer').modal('hide');                  

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

           // DataBindDaynamicKendoGriddMain();
			//DataBindDaynamicKendoGrid();
            return true;
        }
        function CloseModalReviewer() {
          
            $('#ComplainceReviewer').modal('hide');
            
            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

             //DataBindDaynamicKendoGriddMain();
			//DataBindDaynamicKendoGrid();
            return true;
        }

        function CloseModalInternalReviewer() {
            $('#ComplainceInternalReviewer').modal('hide');
            
           // $('#ComplaincePerformer').modal('hide');                  

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            //DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }
        function CloseModalInternalPerformer() {
 
            $('#ComplainceInternalPerformaer').modal('hide');
            
            $('#ComplaincePerformer').modal('hide');                  

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

            //DataBindDaynamicKendoGriddMain();
            //DataBindDaynamicKendoGrid();
            return true;
        }

        function OpenOverViewpupMain(scheduledonid, instanceid, Status)
        {    
             
            settracknew('My Workspace','Action','Review','UserID');
            if ($("#dropdownlistUserRole1").val() == 3)
            {
                if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1)
                {
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected" || Status == "Interim Review Approved" || Status == "Interim Rejected") {
                        $('#ComplaincePerformer').modal('show');
                        $('#iPerformerFrame').attr('src', '/controls/compliancestatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType1").val() == 0)
                {
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected") {
                        $('#ComplainceInternalPerformaer').modal('show');
                        $('#iInternalPerformerFrame').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
            }
            if ($("#dropdownlistUserRole1").val() == 4)
            {
                if ($("#dropdownlistComplianceType1").val() == -1 || $("#dropdownlistComplianceType1").val() == 1)
                {
                    if (Status == "Submitted For Interim Review" || Status == "Pending For Review" || Status == "Revise Compliance") {
                        $('#ComplainceReviewer').modal('show');
                        $('#iReviewerFrame').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType1").val() == 0)
                {
                    if (Status == "Pending For Review"  || Status == "Revise Compliance" ) {
                         $('#ComplainceInternalReviewer').modal('show');
                         $('#iInternalReviewerFrame').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);            
                    }
                }
            }
        }

        function OpenOverViewpup(scheduledonid, instanceid, Status)
        {  
            settracknew('My Workspace', 'Action', $("#dropdownlistUserRole").data("kendoDropDownList").text(),<%=UId%>);

            if ($("#dropdownlistUserRole").val() == 3)
            {
                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1)
                {
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected" || Status == "Interim Review Approved" || Status == "Interim Rejected") {
                        $('#ComplaincePerformer').modal('show');
                        $('#iPerformerFrame').attr('src', '/controls/compliancestatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0)
                {
                    if (Status == "Upcoming" || Status == "Overdue" || Status == "Rejected") {
                        $('#ComplainceInternalPerformaer').modal('show');
                        $('#iInternalPerformerFrame').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
            }
            if ($("#dropdownlistUserRole").val() == 4)
            {
                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1)
                {
                    if (Status == "Submitted For Interim Review" || Status == "Pending For Review"  || Status == "Revise Compliance")
                    {
                        $('#ComplainceReviewer').modal('show');
                        $('#iReviewerFrame').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0)
                {
                    if (Status == "Pending For Review"  || Status == "Revise Compliance") {
                         $('#ComplainceInternalReviewer').modal('show');
                         $('#iInternalReviewerFrame').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scheduledonid + '&InId=' + instanceid);            
                    }
                }
            }

        }

        function OpenOverViewpupnew(scheduledonid, instanceid,Status) {
            debugger;
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistUserRole").val() == 4)
            {


                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1)
                {
                    if (Status == "Overdue")
                    {
                      $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0)
                {
                    if (Status == "Overdue") {
                    $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }

         
           }
        }

        function OpenOverViewpupnewone(scheduledonid, instanceid,Status) {
            debugger;
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistUserRole").val() == 4)
            {


                if ($("#dropdownlistComplianceType").val() == -1 || $("#dropdownlistComplianceType").val() == 1)
                {
                    if (Status == "Overdue")
                    {
                        $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }
                if ($("#dropdownlistComplianceType").val() == 0)
                {
                    if (Status == "Overdue") {
                     $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                    }
                }

              
            }
        }
        
        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");           
        }

      
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div id="grid" style="border: none;"></div>
        <div>

            <div>
                <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 85%">

                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                            </div>
                            <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 750px; overflow-y: hidden;">
                                <%--  <vit:ComplianceStatusTransaction runat="server" ID="udcStatusTranscatopn" Visible="false" />--%>
                                <iframe id="iPerformerFrame" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="modal fade" id="ComplainceInternalPerformaer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 85%">

                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                            </div>
                            <div class="modal-body" style="background-color: #f7f7f7; width: 100%; max-height: 750px; overflow-y: auto;">
                                <%--   <vit:InternalComplianceStatusTransaction runat="server" ID="udcInternalPerformerStatusTranscation" Visible="false" />--%>
                                <iframe id="iInternalPerformerFrame" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
        <div>
            <div class="modal fade" id="ComplainceReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:auto;">
                       <%--     <vit:ComplianceReviewerStatusTransaction runat="server" ID="udcReviewerStatusTranscatopn" Visible="false" />--%>
                            <iframe id="iReviewerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
             <div>
            <div class="modal fade" id="ComplainceInternalReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:auto;">
                          <%--  <vit:InternalComplianceReviewerStatusTransaction runat="server" ID="udcStatusTranscationInternal" Visible="false" />--%>
                             <iframe id="iInternalReviewerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
            <div>
                 <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999">                               
                 <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;">
                             <input id="dropdownlistUserRole1" data-placeholder="Role"  style="width:242px;">             
                    </div>
                </div>
                <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                            <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                            <input id="Startdatepicker" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;"/>
                        </div>
                        <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px;">
                            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13%; padding-left: 0px;display:none;">                            
                            <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                        </div>
                        <div class="col-md-4" id="dvdropdownACT" style="width: 29.3%; padding-left: 0px;">
                            <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                        </div>
                          <div class="col-md-4" style="width: 29.3%; padding-left: 0px;">
                            <input id="dropdownDept" data-placeholder="Dept" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13.4%; padding-left: 0px;display:none;" id="dvdropdownlistStatus1">                            
                            <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                        </div>
                         <div class="col-md-2" style="width: 13%; padding-left: 0px;float: right;display:none;">
                            <button id="exportAdvanced" onclick="exportReportAdvanced(event)"  class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>        
                         </div>
                    </div>
                </div>


                <div class="row" style="padding-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                          <div class="col-md-2" style="width: 20%;padding-left: 1px;">
                              <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>  
                             <input id="dropdownSequence" style="width: 100%;" />
                             <%}%>
                        </div>
                      <div class="col-md-2" style="width: 14.3%;padding-left: 1px;">
                            <div id="dvdropdownEventName1" style="display: none;">
                                <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 196px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 10%;">
                            <div id="dvdropdownEventNature1" style="display: none;">
                                <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;">
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%; padding-left: 105px;">
                            <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCompType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCategory">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterAct">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCompSubType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterLastDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filtersstoryboard1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filtertype1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterrisk1">&nbsp;</div>
                                                                                                           
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterpstData1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterUser">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterFY">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterstatus1">&nbsp;</div>


                <div id="grid1"></div>                
            </div>
            <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            fhead('My Workspace');
        });
        $(document).ready(function () {

            $("#dropdownlistUserRole").data("kendoDropDownList").value(<% =UserRoleID%>);
            $("#dropdownlistUserRole1").data("kendoDropDownList").value(<% =UserRoleID%>);

            $("#dropdownlistComplianceType").data("kendoDropDownList").value(<% =ComplianceTypeID%>);
            $("#dropdownlistComplianceType1").data("kendoDropDownList").value(<% =ComplianceTypeID%>);

            $("#dropdownlistStatus").data("kendoDropDownList").value(<% =StatusFlagID%>);
            $("#dropdownlistStatus1").data("kendoDropDownList").value(<% =StatusFlagID%>);

            if (<% =ComplianceTypeID%> == 0)
            {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I'
                    }
                });
                dataSourceSequence.read();
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
        });
    </script>
     

</asp:Content>

