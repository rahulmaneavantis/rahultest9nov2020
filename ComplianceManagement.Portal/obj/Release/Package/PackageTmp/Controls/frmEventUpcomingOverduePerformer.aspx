﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="frmEventUpcomingOverduePerformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.frmEventUpcomingOverduePerformer" %>

<%@ Register Src="~/Controls/ComplianceStatusTransactionEventPerformer.ascx" TagName="ComplianceStatusTransactionEventPerformer"
    TagPrefix="vit" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <style type="text/css">
        table th {
            border: 1px solid White;
            height: 20px;
            color: White;
            font-size: 12px;
            font-family: Tahoma;
        }

        .GridView1 {
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .graphDiv {
            width: 340px;
            height: 200px;
            float: left;
            border: 1px solid #4297d7;
        }

        .igraphImg {
            text-align: center;
            margin-top: 75px;
        }

        .mainGraph {
            width: 80%;
            float: left;
        }
    </style>
    <style type="text/css">
        .accordionContent {
            background-color: #D3DEEF;
            border-color: -moz-use-text-color #2F4F4F #2F4F4F;
            border-right: 1px dashed #2F4F4F;
            border-style: none dashed dashed;
            border-width: medium 1px 1px;
            padding: 10px 5px 5px;
            width: 96%;
        }

        .accordionHeaderSelected {
            background-color: #5078B3;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 96%;
        }

        .accordionHeader {
            background-color: #2E4D7B;
            border: 1px solid #2F4F4F;
            color: white;
            cursor: pointer;
            font-family: Arial,Sans-Serif;
            font-size: 12px;
            font-weight: bold;
            margin-top: 5px;
            padding: 5px;
            width: 96%;
        }

        .href {
            color: White;
            font-weight: bold;
            text-decoration: none;
        }
    </style>

    <script type="text/javascript">
        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <table width="100%" style="height: 35px">
                    <tr>
                        <td>
                            <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div style="margin-left:40px;">
                    <asp:UpdatePanel ID="s" runat="server">
                        <ContentTemplate>
                            <ajaxToolkit:Accordion ID="UserAccordion" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                                HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent" FadeTransitions="true" SuppressHeaderPostbacks="true" TransitionDuration="250" FramesPerSecond="40" RequireOpenedPane="false" AutoSize="None">
                                <Panes>
                                    <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                                        <Header><a href="#" class="href">Upcoming</a></Header>
                                        <Content>

                                            <asp:Panel ID="LoanEntry" runat="server">
                                                <div style="text-align: Left; margin-left: 0%; font-weight: bold; margin-bottom: 10px; color: black;">
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                </div>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="vertical-align: top; width: 50%;">
                                                            <div style="margin-top: 10px">
                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdComplianceTransactionsUpcoming" AutoGenerateColumns="false" AllowSorting="true"
                                                                            GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                                                                            BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="10" OnSorting="grdComplianceTransactionsUpcoming_Sorting"
                                                                            Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactionsUpcoming_PageIndexChanging"
                                                                            OnRowDataBound="grdComplianceTransactionsUpcoming_RowDataBound" OnRowCommand="grdComplianceTransactionsUpcoming_RowCommand" OnRowCreated="grdComplianceTransactionsUpcoming_RowCreated">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="ID" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="25%" SortExpression="Description">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="10%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                                                                                    <ItemTemplate>
                                                                                        <asp:Image runat="server" ID="imtemplat" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Impact" ItemStyle-Width="10%" SortExpression="NonComplianceType">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblImpact" runat="server" Text='<%# Eval("NonComplianceType") %>'></asp:Label>
                                                                                        <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                              <%--  <asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                                                                                    <ItemTemplate>
                                                                                        <%# Eval("ForMonth") %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" SortExpression="Status" ItemStyle-Width="5%" />
                                                                               <%-- <asp:TemplateField HeaderText="Event" ItemStyle-Width="30%" SortExpression="EventID">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblEventID" runat="server" Text='<%# ShowType((long?)Eval("EventID")) %>' ToolTip='<%# ShowType((long?)Eval("EventID")) %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="20%" SortExpression="EventName">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Event Nature" ItemStyle-Width="10%" SortExpression="EventNature">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Reference Material" ItemStyle-Width="30%" SortExpression="ReferenceMaterialText">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblReferenceMaterialText" runat="server" Text="Text" ToolTip='<%# Eval("ReferenceMaterialText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                                                                            CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                            <PagerSettings Position="Top" />
                                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </Content>
                                    </ajaxToolkit:AccordionPane>
                                    <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                                        <Header><a href="#" class="href">Overdue</a> </Header>
                                        <Content>
                                            <asp:Panel ID="Panel2" runat="server">
                                                <div style="text-align: Left; margin-left: 0%; font-weight: bold; margin-bottom: 10px; color: black;">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                </div>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="vertical-align: top; width: 50%;">
                                                            <div style="margin-top: 10px">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                                                                            GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                                                                            BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSorting="grdComplianceTransactions_Sorting"
                                                                            Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                                                                            OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowCreated="grdComplianceTransactions_RowCreated">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="ID" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="25%" SortExpression="Description">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                  
                                                                                <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="10%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </asp:TemplateField>
                                                                               
                                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                                                                                    <ItemTemplate>
                                                                                        <asp:Image runat="server" ID="imtemplat" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Impact" ItemStyle-Width="10%" SortExpression="NonComplianceType">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblImpact" runat="server" Text='<%# Eval("NonComplianceType") %>'></asp:Label>
                                                                                        <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--<asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                                                                                    <ItemTemplate>
                                                                                        <%# Eval("ForMonth") %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" SortExpression="Status" />
                                                                               <%-- <asp:TemplateField HeaderText="Event" ItemStyle-Width="30%" SortExpression="EventID">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblEventID" runat="server" Text='<%# ShowType((long?)Eval("EventID")) %>' ToolTip='<%# ShowType((long?)Eval("EventID")) %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="20%" SortExpression="EventName">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Event Nature" ItemStyle-Width="20%" SortExpression="EventNature">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Reference Material" ItemStyle-Width="30%" SortExpression="ReferenceMaterialText">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblReferenceMaterialText" runat="server" Text="Text" ToolTip='<%# Eval("ReferenceMaterialText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnChangeStatus" runat="server"
                                                                                            Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                                                                            CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status"  /></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                                                                            <PagerSettings Position="Top" />
                                                                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                            <AlternatingRowStyle BackColor="#E6EFF7" />
                                                                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </asp:Panel>
                                        </Content>
                                    </ajaxToolkit:AccordionPane>
                                </Panes>
                            </ajaxToolkit:Accordion>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:HiddenField ID="hdnTitle" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <vit:ComplianceStatusTransactionEventPerformer runat="server" ID="udcStatusTranscatopn" />
</asp:Content>
