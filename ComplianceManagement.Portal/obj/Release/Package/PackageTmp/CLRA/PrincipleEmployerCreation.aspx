﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrincipleEmployerCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.CLRA.PrincipleEmployerCreation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="../NewCSS/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-datepicker.min.js"></script>
    <link href="style.css" rel="stylesheet" />
    <link href="../assets/select2.min.css" rel="stylesheet" />
    <script src="../assets/select2.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            BindClientList();
            BindPrincipleEmployerTable("");
            $("#btnNewPrincipleEmployer").on("click", function (e) {
                $("#ModalPrincipleEmployer").modal("show");
            });
            $("#ddlClient").on("change", function (e) {
                BindPrincipleEmployerTable($(this).val());
            });
            $("#btnSave").click(function (e) {
                if (true) {
                    var DetailsObj = {
                        ID: $("#lblPEID").text(),
                        ClientID: $("#ddlClient").val(),
                        PEName: $("#txtPEName").val(),
                        NatureOfBusiness: $("#txtNatureOfBusiness").val(),
                        ContractFrom: $("#txtContractForm").val(),
                        ContractTo: $("#txtContractTo").val(),
                        NoOfEmployees: $("#txtNumberOfEmployees").val(),
                        Address: $("#txtAddress").val(),
                        Contractvalue: $("#txtControctalue").val(),
                        SecurityDeposit: $("#txtSecurityDeposit").val(),
                        IsCentralAct: ($("#chkCentral").is(":checked") ? 1 : 0),
                        Status: ($("#chkStatus").is(":checked") ? "A" : "I")
                    }

                    $.ajax({
                        type: "POST",
                        url: "./PrincipleEmployerCreation.aspx/SavePrincipleEmployerDetails",
                        data: JSON.stringify({ DetailsObj }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var Success = JSON.parse(data.d);
                            if (Success) {
                                alert("Contractor Save Successfully.");
                                $("#lblPEID").text("0");
                                $("#txtPEName").val("");
                                $("#txtNatureOfBusiness").val("");
                                $("#txtContractForm").val("");
                                $("#txtContractTo").val("");
                                $("#txtNumberOfEmployees").val("");
                                $("#txtAddress").val("");
                                $("#txtControctalue").val("");
                                $("#txtSecurityDeposit").val("");
                                $("#ModalPrincipleEmployer").modal("hide");
                            }
                            else {
                                alert("Mail ID Already Exist");
                            }
                            BindPrincipleEmployerTable($("#ddlClient").val());
                        },
                        failure: function (data) {
                            alert(data);
                        }
                    });
                }
            });
            $(document).on("click", "#tblPrincipleEmployer tbody tr .Update", function (e) {
                var item = $("#tblPrincipleEmployer").data("kendoGrid").dataItem($(this).closest("tr"));
                $("#lblID").text(item.ID);
                var DetailsObj = {
                    ID: item.ID
                }
                $.ajax({
                    type: "POST",
                    url: "./PrincipleEmployerCreation.aspx/GetPrincipleEmployerDetails",
                    data: JSON.stringify({ DetailsObj }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var EmployerDetails = JSON.parse(data.d);
                        if (EmployerDetails.length > 0) {
                            $("#lblPEID").text(EmployerDetails[0].ID);
                            $("#txtPEName").val(EmployerDetails[0].PEName);
                            $("#txtNatureOfBusiness").val(EmployerDetails[0].NatureOfBusiness);
                            const StartdateTime = EmployerDetails[0].ContractFrom;
                            const parts = StartdateTime.split(/[- :]/);
                            const FromDate = `${parts[0]}/${parts[1]}/${parts[2]}`;
                            $("#txtContractForm").val(FromDate);
                            const EnddateTime = EmployerDetails[0].ContractTo;
                            const parts1 = EnddateTime.split(/[- :]/);
                            const ToDate = `${parts1[0]}/${parts1[1]}/${parts1[2]}`;
                            $("#txtContractTo").val(ToDate);
                            //$("#txtContractForm").val(EmployerDetails[0].ContractFrom);
                        //    $("#txtContractTo").val(EmployerDetails[0].ContractTo);
                            $("#txtNumberOfEmployees").val(EmployerDetails[0].NoOfEmployees);
                            $("#txtAddress").val(EmployerDetails[0].Address);
                            $("#txtControctalue").val(EmployerDetails[0].Contractvalue);
                            $("#txtSecurityDeposit").val(EmployerDetails[0].SecurityDeposit);
                            debugger;
                            if (EmployerDetails[0].IsCentralAct!="False")
                                $("#chkCentral").prop("checked", true);
                            else
                                $("#chkCentral").prop("checked", false);

                            if (EmployerDetails[0].Status == "A")
                                $("#chkStatus").prop("checked", true);
                            else
                                $("#chkStatus").prop("checked", false);

                            $("#ModalPrincipleEmployer").modal("show");
                        }
                    },
                    failure: function (data) {
                        alert(data);
                    }
                });
            });
        });
        function BindClientList() {
            $.ajax({
                type: "POST",
                url: "./PrincipleEmployerCreation.aspx/BindClientList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var Customer = JSON.parse(data.d);
                    if (Customer.length > 0) {
                        $("#ddlClient").empty();
                        $("#ddlClient").append($("<option></option>").val("-1").html("Select Client"));
                        $.each(Customer, function (data, value) {
                            $("#ddlClient").append($("<option></option>").val(value.ClientID).html(value.ClientName));
                        })
                        $("#ddlClient").select2();
                    }
                },
                failure: function (data) {
                    alert(data);
                }
            });
        }

        function BindPrincipleEmployerTable(clntid) {
            debugger;
            var DetailsObj = {
                ClientID: clntid
            }
            $.ajax({
                type: "POST",
                url: "./PrincipleEmployerCreation.aspx/BindPrincipleEmployerTable",
                data: JSON.stringify({ DetailsObj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var AssignCheckList = JSON.parse(data.d);

                    $("#tblPrincipleEmployer").kendoGrid({
                        dataSource: {
                            data: AssignCheckList
                        },
                        height: 300,
                        sortable: true,
                        filterable: false,
                        columnMenu: true,
                        reorderable: true,
                        resizable: true,
                        noRecords: {
                            template: function (e) {
                                return "No data available";
                            }
                        },
                        columns: [
                            { hidden: true, field: "ClientID" },
                            { hidden: true, field: "NoOfEmployees" },
                            { hidden: true, field: "Contractvalue" },
                            { hidden: true, field: "SecurityDeposit" },
                            { hidden: true, field: "ID" },
                            { field: "PEName", title: "PE Name", width: "25%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "NatureOfBusiness", title: "Nature Of Business", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "ContractFrom", title: "Contract From", width: "20%" },
                            { field: "ContractTo", title: "Contract To", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "Address", title: "Address", width: "20%", attributes: { style: 'white-space: nowrap;' } },
                            { field: "IsCentralAct", title: "Central", width: "15%", attributes: { style: 'white-space: nowrap;' } },
                             {
                                 title: "Action", lock: true, width: "30%;",
                                 command: [{ name: "UpdatePrincipleEmployer", text: "Update", className: "Update" }]
                             }
                        ]
                    });
                    $("#tblContractor").kendoTooltip({
                        filter: ".k-ScheduleAudit",
                        content: function (e) {
                            return "";
                        }
                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    </script>

    <style>
        .select2-dropdown {
            background-color: #ffffff;
            color: #151010;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 1051;
        }


        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #c7c7cc;
        }

        .select2-container .select2-selection--single {
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 34px;
            user-select: none;
            -webkit-user-select: none;
        }
    </style>
</head>
<body>

    <!-- partial:index.partial.html -->

    <!--PEN CONTENT     -->
    <div class="content">
        <!--content inner-->
        <div class="content__inner">
            <div class="container">
                <!--content title-->

                <!--content title-->
                <h2 class="content__title">Principle Employer Creation</h2>
            </div>
            <div class="container overflow-hidden">
                <!--multisteps-form-->
                <div class="multisteps-form">
                    <!--progress bar-->
                    <div class="row">
                        <div class="col-12 col-lg-12 ml-auto mr-auto mb-4">
                            <div class="multisteps-form__progress">
                                <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Principle Employer Creation</button>
                                <button class="multisteps-form__progress-btn" type="button" title="Address">Address</button>
                                <button class="multisteps-form__progress-btn" type="button" title="Order Info">Order Info</button>
                                <button class="multisteps-form__progress-btn" type="button" title="Comments">Comments        </button>
                            </div>
                        </div>
                    </div>
                    <!--form panels-->
                    <div class="row">
                        <div class="col-12 col-lg-12 m-auto">
                            <form class="multisteps-form__form">
                                <!--single form panel-->
                                <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                                    <h3 class="multisteps-form__title">Principle Employer Creation</h3>
                                    <div class="multisteps-form__content">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                                <label style="color: black;">Client ID</label>
                                                <select class="form-control" id="ddlClient">
                                                </select>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                                <button class="btn btn-primary" type="button" id="btnNewPrincipleEmployer" title="Add New Principle Employer" style="margin-top: 26px;">Add New Principle Employer</button>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                                <div id="tblPrincipleEmployer"></div>
                                            </div>
                                        </div>

                                        <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <!--single form panel-->
                                <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                    <h3 class="multisteps-form__title">Your Address</h3>
                                    <div class="multisteps-form__content">
                                        <div class="form-row mt-4">
                                            <div class="col">
                                                <input class="multisteps-form__input form-control" type="text" placeholder="Address 1" />
                                            </div>
                                        </div>
                                        <div class="form-row mt-4">
                                            <div class="col">
                                                <input class="multisteps-form__input form-control" type="text" placeholder="Address 2" />
                                            </div>
                                        </div>
                                        <div class="form-row mt-4">
                                            <div class="col-12 col-sm-6">
                                                <input class="multisteps-form__input form-control" type="text" placeholder="City" />
                                            </div>
                                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                                <select class="multisteps-form__select form-control">
                                                    <option selected="selected">State...</option>
                                                    <option>...</option>
                                                </select>
                                            </div>
                                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                                <input class="multisteps-form__input form-control" type="text" placeholder="Zip" />
                                            </div>
                                        </div>
                                        <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <!--single form panel-->
                                <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                    <h3 class="multisteps-form__title">Your Order Info</h3>
                                    <div class="multisteps-form__content">
                                        <div class="row">
                                            <div class="col-12 col-md-6 mt-4">
                                                <div class="card shadow-sm">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Item Title</h5>
                                                        <p class="card-text">Small and nice item description</p>
                                                        <a class="btn btn-primary" href="#" title="Item Link">Item Link</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 mt-4">
                                                <div class="card shadow-sm">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Item Title</h5>
                                                        <p class="card-text">Small and nice item description</p>
                                                        <a class="btn btn-primary" href="#" title="Item Link">Item Link</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="button-row d-flex mt-4 col-12">
                                                <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                                <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--single form panel-->
                                <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                    <h3 class="multisteps-form__title">Additional Comments</h3>
                                    <div class="multisteps-form__content">
                                        <div class="form-row mt-4">
                                            <textarea class="multisteps-form__textarea form-control" placeholder="Additional Comments and Requirements"></textarea>
                                        </div>
                                        <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                            <button class="btn btn-success ml-auto" type="button" title="Send">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- partial -->
    <script src="script.js"></script>

    <div class="modal  modal-fullscreen" id="ModalPrincipleEmployer" role="dialog">
        <div class="modal-dialog modal-full">
            <!-- Modal content-->
            <div class="modal-content" id="divValidation" style="height: 450px; width: 710px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Principle Employer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">PE Name*</label>
                                <input type="text" id="txtPEName" class="form-control" />
                                <label style="color: black;" id="lblPEID" class="hidden">0</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Nature Of Business*</label>
                                <input type="text" id="txtNatureOfBusiness" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract From*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractForm" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract To*</label>
                                <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" id="txtContractTo" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                                <label style="color: black;">Number Of Employees*</label>
                                <input type="text" id="txtNumberOfEmployees" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label style="color: black;">Address*</label>
                                <input type="text" id="txtAddress" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Contract Value</label>
                                <input type="text" id="txtControctalue" class="form-control" maxlength="10" />

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <label style="color: black;">Security Deposit</label>
                                <input type="text" id="txtSecurityDeposit" class="form-control" maxlength="10" />

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chkCentral" value="1" />IsCentralAct</label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                                <div class="checkbox">
                                    <label style="color: black;">
                                        <input type="checkbox" id="chkStatus" value="A" />Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button id="btnSave" type="button" value="Submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
