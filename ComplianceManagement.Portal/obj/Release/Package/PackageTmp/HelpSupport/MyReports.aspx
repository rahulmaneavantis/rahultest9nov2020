﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 

    <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>


    <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
    .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1170px;


  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>
  
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>

</head>
<body>
    <form id="form1" runat="server">

         <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                   <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul>        </div>

          <div class="col-sm-9 MainContainer" style="line-height:25px">
      
              <h3>My report</h3>
       You will be able to view and download different reports in this section, following are they,<br />
           <br />         
     <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
         
        <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Detailed Report</b>
				</a>
				</h4>
			</div>
	  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
              Based on compliance type you can view detailed report and also download them. To view and download compliance report you need to follow below steps,
                               <ul >
                                   <li style="list-style-type:lower-roman">Select Detailed Report option from My Report, you will be redirected to page that features list of compliance.</li>
                                   <li style="list-style-type:lower-roman">On this page you can view the list of compliances based on compliance type (Statutory and Internal), you can select the type to view list. Also, you can set filter and generate desired list.</li><br />   <br />         
                                   <%--<img class="img1" style="width: 700px;margin-left:20px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Report_Detailed%20Report_1_Screenshot.png" />--%>
                                   <img src="../perfreviewerUpdatedscreens/Performer_My%20Report_Detailed%20Report_1_Screenshot.png" style="width: 700px;margin-left:20px; height: 180px" /><br /><br />
                                   <li style="list-style-type:lower-roman"> Advance Search- To generate more specific list this option can be used.</li><br /><br />
                                   <%--<img class="img1" style="width: 700px;margin-left:20px; height: 180px" src="../ImagesHelpCenter/Performer_My%20Report_Detailed%20Report_2_Screenshot.png" />--%>  
                                   <img src="../perfreviewerUpdatedscreens/Performer_My%20Report_Detailed%20Report_2_Screenshot.png" style="width: 700px;margin-left:20px; height: 180px" />  <br /><br />
                                   <li style="list-style-type:lower-roman">To view particular compliance detailed report, click on Action icon and a window will appear, holding the details of compliance, you will see different tabs Summary, Details, Historical Documents, Updates, Audit logs. And two sections - Current Documents and Comments, following are the use,
                                       <ul >
                                           <li style="list-style-type:lower-alpha"> Summary - This will feature basic details and historical completion status.</li>
                                           <li style="list-style-type:lower-alpha">Details - Compliance complete details will be featured.</li>
                                           <li style="list-style-type:lower-alpha">Updates - The updates related to compliance will be featured here, you can view the list in ascending and descending order, adjust no. of columns and set filter.</li>
                                           <li style="list-style-type:lower-alpha">Current documents- You can view the related documents in this section.</li>
                                           <li style="list-style-type:lower-alpha">Comment - To comment this section will be used, you can address particular user and comment.To do this, type "<@username> <your comment>" post.</li><br /><br />
                                           <%-- <img class="img1" style="width: 700px;margin-left:5px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Report_Detailed%20Report_3_Screenshot.png" />
                                      --%><img src="../perfreviewerUpdatedscreens/Performer_My%20Report_Detailed%20Report_4_Screenshot.png" style="width: 700px;margin-left:5px; height: 350px"/> </ul>
                                       </li></ul>
              
              </div>
          </div>
      </div>	   <br />
          

         <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Assignment Report</b>
				</a>
				</h4>
			</div>
	  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
               You can download the report of assigned compliance  location wise, following is the process to download,
                               <ul >
                                   <li style="list-style-type:lower-roman">Select Assignment Report option in My Report.</li>
                                   <li style="list-style-type:lower-roman">You will be redirected to page, on this page select compliance type and location.</li>
                                   <li style="list-style-type:lower-roman">And to download the report click on Export to Excel button.</li><br />
                                   <img class="img1" style="width: 700px;margin-left:20px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Report_Assign%20Compliance%20Report_1_Screenshot.png" /><br /><br />
                                </ul>
					 </div>
                </div>
            </div>    <br />


        <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Task Report</b>
				</a>
				</h4>
			</div>
	  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
                The Task report gives details of task assigned to you, and to view and download this report, follow the below process,
                               <ul>
                                   <li style="list-style-type:lower-roman">Select Task Report option in My Report, you will be redirected to page that features list of tasks.</li>
                                   <li style="list-style-type:lower-roman">You can generate list of tasks by setting filter and for more specific list of tasks use Advanced Search.</li>
                                   <li style="list-style-type:lower-roman">To download the report, click on Excel icon.</li><br />
                                   <img class="img1" style="width: 700px;margin-left:20px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Report_Task%20Report_Screenshot.png" /><br /><br />

					 </div>
                </div>
            </div>


          <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingThreenew">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThreenew" aria-expanded="true" aria-controls="collapseThreenew">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Act Repository</b>
				</a>
				</h4>
			</div>
	  <div id="collapseThreenew" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThreenew">
				<div class="panel-body">
               User can view lists all compliances under the assigned Acts. Follow the below process to use this feature,<ul>
                                   <li style="list-style-type:lower-roman">Go to My Reports and select Act Repository from menu.</li>
                                   <li style="list-style-type:lower-roman">This page will list compliances with filters</li>
                   <img style="width: 700px;margin-left:20px; height: 350px" src="../perfreviewerUpdatedscreens/Performer_My%20Report_Act%20Repository_Screenshot.png" />            
					 </div>
                </div>
            </div>

     
      </div>
  </div>

    


    </div>
    </div>  
    </form>

   

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
