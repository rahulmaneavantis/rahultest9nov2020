﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
 <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>

 <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
    .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
   width:1170px;


  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/
  
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>    
</head>
<body >
    <form id="form1" runat="server">
    <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                    <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul>          </div>

          <div class="col-sm-9 MainContainer">
     
              <h3>My Documents</h3>
              <br />
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
           <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Compliance Document-</b>
					</a>
				</h4>
			</div>
			 <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
                  You can view and download compliance documents, also you view overview the compliance details.  Follow the below process,
                              <ul >
                                  <li style="list-style-type:decimal">Select Compliance Document in My Document.</li>
                                  <li style="list-style-type:decimal">You will be redirected to page featuring list of compliance that are assigned to you.</li>
                                  <li style="list-style-type:decimal">To find particular compliance you can set filter and Advanced Search.</li>
                                  <li style="list-style-type:decimal">To view the document associated with the compliance click on View icon and to download the documents click on download icon. You can also view the compliance details by click on overview icon.</li><br /><br />
                                  <img class="img1" style="width: 700px;margin-left:30px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Document_Compliance%20Document_Screenshot.png" /><br /><br />
                              </ul>
                     </div>
                </div>
         </div> <br />

          <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b> Critical document</b>
					</a>
				</h4>
			</div>
			 <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
                   Critical document is personal folder for user, you can store your documents and these documents will not be accessible by any other user. You can create and search folder and upload file. Following are the steps,
                              <ul>
                                  <li style="list-style-type:decimal">To Search - In given search box type the folder name.</li>
                                   <br /><br />
                                  <img class="img1" style="width: 700px;margin-left:30px; height: 350px"src="../ImagesHelpCenter/Performer_My%20Document_Critical%20Document_1_Screenshot.png" /><br /><br />
                                   <li style="list-style-type:decimal">To Upload file - Select folder, click on New button and select New file option. A pop-up window will come to upload file from your system.<br /><br /></li>
                                  <img class="img1" style="width: 600px;margin-left:30px; height: 200px" src="../ImagesHelpCenter/Performer_My%20Document_Critical%20Document_2_Screenshot.png" /><br /><br />
                              </ul>
                     </div>
                </div>
         </div>  <br />

          <div class="panel panel-default">
		    	<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>  Task Documents </b>
					</a>
				</h4>
			</div>
			 <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
                This section will hold task related document. You can view and download these documents. Following is the process,
                              <ul >
                                  <li style="list-style-type:decimal">Select Task Document in My Document, you will be redirected to page featuring list of assigned tasks.</li>
                                  <li style="list-style-type:decimal">You can set filter to view specific list of tasks and to generate more specific list use Advance Search.</li>
                                  <li style="list-style-type:decimal">To view task details, click on View button and to download click on Download button.</li> <br /><br />
                            <%--<img class="img1" style="width: 700px;margin-left:30px; height: 350px" src="../ImagesHelpCenter/Performer_My%20Report_Task%20Report_Screenshot.png" />--%>
                                  <img class="img1" style="width: 700px;margin-left:30px; height: 350px" src="../ImagesHelpCenter/Reviewer_My%20Document_4_Task%20Documents_1_Screenshot.png" />  <br /><br />

                              </ul>
                </div>
                </div>
         </div> 

        
     </div>
    </div>
</div>
      </div>
    </form>
    

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
