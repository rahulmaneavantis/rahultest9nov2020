﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
     <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
      <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
     .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
     .img1{
        border:ridge;
        border-color:lightgrey;
    }
    
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
   width:1170px;
 
  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
   /*background-color:#1fd9e1;*/

 
}

* {
  box-sizing: border-box;
}

    </style>
 
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>   
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">

            <div class="row content">

                <div class="col-sm-3 sidenav">

                    <ul class="nav nav-pills nav-stacked">
                        <li style="font-size: 18px"><a href="../NewHelp.aspx" style="color: #333"><b>Standard Dashboard</b></a>
                            <li style="font-size: 14px"><a href="../NewHelp.aspx" style="color: #333"><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                                <ul style="line-height: 25px; font-size: 13px">
                                    <li><a href="../HelpSupport/PerformerSummary.aspx" style="color: #333">Performer Summary</a></li>
                                    <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color: #333">Performer Task Summary</a></li>
                                    <li><a href="../HelpSupport/PerformerLocation.aspx" style="color: #333">Performer Location</a></li>
                                    <li><a href="../HelpSupport/ReviewerSummary.aspx" style="color: #333">Reviewer Summary</a></li>
                                    <li><a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color: #333">Reviewer Task Summary</a></li>
                                    <li><a href="../HelpSupport/ReviewerLocation.aspx" style="color: #333">Reviewer Location</a></li>
                                    <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color: #333">Event Owner Summary</a></li>
                                    <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color: #333">My Compliance Calender</a></li>
                                    <li><a href="../HelpSupport/DailyUpdates.aspx" style="color: #333">Daily Updates</a></li>
                                    <li><a href="../HelpSupport/NewsLetter.aspx" style="color: #333">Newsletter</a></li>
                                </ul>
                            </li>
                            <li style="font-size: 14px"><a href="../HelpSupport/Compliance.aspx" style="color: #333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>Performer Workspace</b></a>


                                <%-- <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>--%>
                            </li>
                            <li style="font-size: 14px"><a href="../HelpSupport/Reviewerworkspace.aspx" style="color: #333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>Reviewer Workspace</b></a>

                                <li style="font-size: 14px"><a href="../HelpSupport/MyReports.aspx" style="color: #333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                                <li style="font-size: 14px"><a href="../HelpSupport/MyDocuments.aspx" style="color: #333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                                <li style="font-size: 14px"><a href="../HelpSupport/Reminders.aspx" style="color: #333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                                <li style="font-size: 14px"><a href="../HelpSupport/Escalation.aspx" style="color: #333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>
                    </ul>
                </div>



                <div class="col-sm-9 MainContainer" style="line-height: 25px;">
                    <p>When you click on My Workspace, you will see bifurcation as Compliance and License.</p>
                    <h3>
                        <li style="list-style-type: disc">Compliance</li>
                    </h3>
                    <p style="font-size: 16px;">
                        You can review compliance, and to review use following process,  
                        <br />
                        <br />
                        <b>Question-</b> How can I review compliance?<br />
                        <b>Answer-</b> In following manner you can review compliance, 
                    </p>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingZero">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        <b>Compliance</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseZero" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingZero">
                                <div class="panel-body">
                                    <%--<img class="img1" style="width: 700px;margin-left:50px;height: 350px" src="../ImagesHelpCenter/Performer_My%20Workspace_2_Compliance_Screenshot.png" />--%>
                                    <%-- <img style="width: 700px;margin-left:30px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer_My%20Workspace_1_Screenshot.png" />  <br /><br />
                                    --%>
                                    <ul>
                                        <li style="list-style-type: decimal">Click on My Workspace tab of menu and you will be redirected to page that features all the compliances to be reviewed.</li>
                                        <li style="list-style-type: decimal">You can search for compliance by setting Filter and to search specific compliance you can use Advanced Filter</li>
                                        <br />
                                        <br />
                                        <img class="img1" style="width: 700px; margin-left: 30px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer_My%20Workspace_1_Screenshot.png" /><br />
                                        <li style="list-style-type: decimal">To review a compliance, you will click on Action icon. A window will appear to review compliance. In case you do not see action button, it means that the compliance is pending to be completed from the Performer</li>
                                        <br />
                                        <br />
                                        <img class="img1" style="width: 700px; margin-left: 30px; height: 400px" src="../perfreviewerUpdatedscreens/Reviewer_My%20Workspace_2_Screenshot.png" /><br />
                                        <li style="list-style-type: decimal">Reviewer need to use following process to review the compliance, 
                                            <br />
                                            <img class="img1" style="width: 700px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_3_Screenshot.png" />
                                            <br />
                                            <ul>
                                                <li style="list-style-type: lower-alpha">Review compliance document- Reviewer need to download /view compliance document version and based on it you will approve or reject the Performer work. Once done with review you need to follow below steps,</li>
                                                <li style="list-style-type: lower-alpha">Select Status from the following options:
                                           <ul>
                                               <li style="list-style-type: lower-roman">Closed-Delayed- if compliance performed by performer after due date. Follow below steps
                                                  <ul>
                                                      <li>1.Enter the Interest and penalty amount, if not known check the Value is not known at this moment option</li>
                                                      <li>2.Enter date</li>
                                                      <li>3.Enter Remark </li>
                                                  </ul>
                                               </li>
                                               <li style="list-style-type: lower-roman">Closed-Timely- if compliance performed by performer within due date. Follow below steps
                                                   <ul>
                                                       <li>Enter date</li>
                                                       <li>Enter Remark</li>
                                                   </ul>
                                               </li>
                                               <li style="list-style-type: lower-roman">To approve the compliance click Approve button </li>
                                               <li style="list-style-type: lower-roman">To reject the compliance click Reject button</li>
                                               <li style="list-style-type: lower-roman">ou can close in case you are not planning to review the compliance. </li>
                                           </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li style="list-style-type: decimal">All asterisk marked fields are mandatory to fill and also reviewing the document is mandatory.</li>
                                        <li style="list-style-type: decimal">On My Workspace you will also fine More link dropdown, in this dropdown you can use features – Update penalty, Revise compliance, My Leave and Reassign Performer. And in following manner you can use these features,</li>
                                        <%--<img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Performer_My%20Workspace_3_More%20links_Screenshot.png" /><br /><br />
                                        --%>
                                        <img class="img1" style="width: 700px; margin-left: 30px; height: 300px" src="../perfreviewerUpdatedscreens/Reviewer_My%20Workspace_4_Screenshot.png" />
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <br />

                        <%--<img class="img1" style="width: 750px;margin-left:-5px; height: 580px" src="../perfreviewerUpdatedscreens/Reviewer_My%20Workspace_4_Screenshot.png" />   <br />      --%>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        <b>Update Penalty</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    If Performer updates the Interest and Penalty amount for a compliance, Reviewer will have to confirm it, in case Reviewer feels the Interest and Penalty has to be different he/she can change it. Following is the process to confirm and update penalty,                                                                             
                     <ul>
                         <li style="list-style-type: lower-roman">Click on More link dropdown and select Update Penalty.You will be redirected to page that features list of compliance, you can also search for specific compliance by setting filter of Risk type and Location.   </li>
                         <img class="img1" style="width: 650px; margin-left: -5px; height: 400px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_5_Update%20Penalty_1_Screenshot.png" />
                         <li style="list-style-type: lower-roman">To confirm the penalty amount, click on action icon,  click on asterisk marked Interest and Penalty amount  text box  and hit on Save button. This step will be only followed when you are okay with amount entered by Performer. </li>
                         <li style="list-style-type: lower-roman">And if you are not okay with amount filled by Performer then enter the amount you want as penalty in text box marked with asterisk and save.</li>
                         <br />
                         <img class="img1" style="width: 450px; margin-left: -5px; height: 320px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_6_Update%20Penalty_2_Screenshot.png" /><br />
                         <li style="list-style-type: lower-roman">For more specific search of compliance use Advanced Search.</li>
                     </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        <b>Update Leave</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    You can assign compliance to another Reviewer for the leave duration. All the compliances assigned to you till date will be reflected in new Reviewer dashboard, only for the leave duration. And once your leave duration is complete, then on that particular date all the assigned compliance will be visible on your dashboard.  In following manner, you can use this feature,<br />
                                    <br />
                                    <img class="img1" style="width: 750px; margin-left: 30px; height: 450px" src="../ImagesHelpCenter/Performer_My%20Workscpace_6_Revise%20Compliance_Screenshot.png" /><br />
                                    <br />
                                    <ul>
                                        <li style="list-style-type: lower-roman">Go to Workspace and click on Compliance option </li>
                                        <li style="list-style-type: lower-roman">After getting redirected to My Workspace page, click on ‘More Links’ dropdown and select My Leave. </li>
                                        <li style="list-style-type: lower-roman">Once clicked on My Leave, you will be redirected to My leave page. Here you can add new leave and view previous leave record. </li>
                                        <li style="list-style-type: lower-roman">Add New – This feature allows you to add leave in AVACOM. 
                                      <ul>
                                          <li>To add leave, click on Add New button. </li>
                                          <br />
                                          <img class="img1" style="width: 650px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_7_My%20Leave_1_Screenshot.png" /><br />
                                          <br />
                                          <li>A pop-up will appear, in pop-up you have to select Start date and End date of leave, and from dropdown select New User Reviewer (one you want to assign). </li>
                                          <br />
                                          <br />
                                          <img class="img1" style="width: 650px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_8_My%20Leave_2_Screenshot.png" /><br />
                                          <br />
                                          <li>Once done click on save button. </li>
                                          <li>In case you are unable to find the Reviewer in given list, then internally ask your Admin to add Reviewer. </li>

                                      </ul>
                                        </li>
                                        <li style="list-style-type: lower-roman">Once you add leave in system, a notification email will be sent to new Reviewer.</li>
                                        <br />
                                        <br />
                                        <img class="img1" style="width: 720px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workscpace_7_Revise%20Compliance_Screenshot.png" /><br />
                                        <br />

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        <b>Reassign Performer </b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <p>You can reassign the Event and Checklist compliance to new performer from the list of registered users. Following is the process, </p>
                                    <ul>
                                        <li style="list-style-type: lower-roman">Click on More links dropdown and select Reassign Performer option, you will be redirected to page that holds compliance list.</li>
                                        <li style="list-style-type: lower-roman">You can set filter of Compliance type, Event/Checklist and Location. While setting filter you can select the user from Select user to Assign dropdown</li>
                                        <li style="list-style-type: lower-roman">Once done setting filter a compliance will be featured or a list will be generated below, you need to check the compliance you want to reassign and save.</li>
                                        <img class="img1" style="width: 720px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Reviewer_My%20Workspace_9_Reassign%20Performer_Screenshot.png" />
                                    </ul>

                                </div>
                            </div>
                        </div>




                        <h2>
                            <li style="list-style-type: disc">License</li>
                        </h2>
                        <br />




                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFivenew">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFivenew" aria-expanded="true" aria-controls="collapseFivenew">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        <b>License</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFivenew" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFivenew">
                                <div class="panel-body">
                                    <p>You can review the License as per following process,</p>
                                    <ul>
                                        <li style="list-style-type: decimal">Select License in My Workspace, you will be redirected to page that holds License to be reviewed.</li>
                                        <li style="list-style-type: decimal">You can set filter to search for specific License or list of License.</li>
                                        <br />

                                        <img class="img1" style="width: 700px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workspace_17_License_Screenshot.png" /><br />
                                        <br />
                                        <li>To review License, click on Action icon. You will be redirected to window that will hold Compliance and License details, License documents to be reviewed, Audit log and section to update status. In Update License Status section you have to add remark and Approve/Reject the status.</li>
                                        <br />
                                        <img class="img1" style="width: 700px; margin-left: 30px; height: 400px" src="../ImagesHelpCenter/Performer_My%20Workspace_18_License_Screenshot.png" />
                                    </ul>
                                </div>
                            </div>
                        </div>




                    </div>




                </div>
            </div>
        </div>

    </form>

     

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

</body>
</html>
