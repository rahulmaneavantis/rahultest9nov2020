﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EventDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 40px">
        <div style="margin-bottom: 4px">
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        </div>
        <div runat="server" id="divEventDetail">
            <asp:UpdatePanel ID="upEventDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td style="width: 98%">
                                <asp:Label ID="lblEventName" Text="Event Name" runat="server" Style="color: #666666; font-family: 'Roboto' sans-serif; font-weight: normal; font: bold; font-size: 20px;"></asp:Label>
                            </td>
                            <td style="width: 2px">
                                <a id="btn_BackAssigned" visible="false" runat="server" class="btn btn-search" href="../Common/EventDashboard.aspx?type=assigned" style="margin-top: -41px;">Back</a>
                                <a id="btn_BackActivated" visible="false" runat="server" class="btn btn-search" href="../Common/EventDashboard.aspx?type=active" style="margin-top: -41px;">Back</a>

                            </td>
                        </tr>
                    </table>
                    <div>
                        <asp:GridView runat="server" ID="grdEventDetail" AutoGenerateColumns="false" CssClass="table"
                            BorderWidth="0px" AllowSorting="true" ShowHeader="true" GridLines="None"
                            AllowPaging="True" PageSize="200" Width="100%"
                            DataKeyNames="ID">
                            <Columns>
                                <asp:TemplateField HeaderText="Seq No" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="25px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                        <%--  <asp:Label runat="server" Text='<%# Eval("rowID") %>' ToolTip='<%# Eval("rowID") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sub Event" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("IntermediatesubEvent") %>' ToolTip='<%# Eval("IntermediatesubEvent") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Section" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("Sections") %>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Short Description" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Penalty Description" ItemStyle-HorizontalAlign="Justify" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="260px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("PenaltyDescription") %>' ToolTip='<%# Eval("PenaltyDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
