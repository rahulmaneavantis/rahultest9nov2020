﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PenaltyDetailsAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.PenaltyDetailsAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
  
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
       
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

   <%-- <script src="../Scripts/KendoPage/ComplianceDetail.js"></script>--%>
    
    <style type="text/css">
            .k-grid-content
        {
            min-height:394px !important;
              overflow: hidden  !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }
        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }        
        .k-grid td {
            line-height: 2.0em;
        }
        .k-i-more-vertical:before {
            content: "\e006";
        }
        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }
        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }
        #grid .k-grid-toolbar {
            background: white;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }

        .k-grouping-header 
        {
            border-right: 0px solid;
            border-left: 0px solid;
        }
        table.k-selectable {
        /*border: solid 1px red;*/
        border-right: solid 1px #ceced2;
        }
        /*.k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }*/

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: auto;
        }
        span.k-icon.k-i-arrow-60-down {
    margin-top: -10px;
}
        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 0px 0px 0px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-grid-header-wrap 
        {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0px 0px 0 0;
            zoom: 1;
        }

        .k-grid table 
        {
            width: 100%;
            max-width: none;
            border-collapse: separate;
            empty-cells: show;
            margin: -1px;
            border-spacing: 0px;
            border-width: 0px;
            outline: 0px;
        }


        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
          .change-condition {
            color: blue;
        }

           .k-grid-header th.k-with-icon .k-link {
            background-color: #F8F8F8;
        }

            .k-grid-toolbar:first-child, .k-grouping-header + .k-grid-toolbar {
            border-width: 0 0 1px;
            padding: 0px;
        }
        li.k-button{
                      margin-top: 2px;
                  }

        .k-multiselect-wrap > .k-i-close 
        {
            top: 8px;
            margin-right: 9px;
        }

         table.k-selectable {
        /*border-right: solid 1px #ceced2;*/
        }

        .k-grid-toolbar:first-child, .k-grouping-header + .k-grid-toolbar {
            border-width: 0 0 0px;
            padding: 0px;
        }
    </style>
   
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
      
    <script type="text/x-kendo-template" id="template">      
               
    </script>
    
    <script type="text/javascript">
        $(window).resize(function(){
            window.parent.forchild($("body").height()+50);  
        });
 


        function exportReport(e) {
            
            var ReportName = "Penalty Report";
            var grid = $("#grid").getKendoGrid();

            var rows = [
                {
                    cells: [
                        { value: "S.No.", bold: true },
                        { value: "Act Name", bold: true },
                        { value: "Short Description", bold: true },
                        { value: "Due Date", bold: true },
                        { value: "User Name", bold: true },
                        { value: "Interest", bold: true },
                        { value: "Penalty", bold: true },
                        { value: "Remarks", bold: true },
                    ]
                }
            ];

            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                rows.push({
                    cells: [ // dataItem."Whatever Your Attributes Are"
                        { value: i+1 },
                        { value: dataItem.ActName },
                        { value: dataItem.ShortDescription },
                        { value: dataItem.ScheduledOn },
                        { value: dataItem.UserName },
                        { value: dataItem.Interest },
                        { value: dataItem.Penalty },
                        { value: dataItem.Remarks },
                    ]
                });

            }
            for (var i = 0; i < rows.length; i++) {
                for (var j = 0; j < 8; j++) {
                    rows[i].cells[j].borderBottom = "#000000";
                    rows[i].cells[j].borderLeft = "#000000";
                    rows[i].cells[j].borderRight = "#000000";
                    rows[i].cells[j].borderTop = "#000000";
                    rows[i].cells[j].hAlign = "left";
                    rows[i].cells[j].vAlign = "top";
                    rows[i].cells[j].wrap = true;

                    //if (i != 4) {
                    //    rows[i].cells[0].value = i - 4;
                    //}
                    //if (i == 4) {
                    //    rows[4].cells[j].background = "#A9A9A9";
                    //}
                }
            }
            excelExport(rows, ReportName); 
            e.preventDefault();
            return false;
        }

        function excelExport(rows, ReportName) {
            
            var FileName="Penalty Report";
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { autoWidth: true },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 300 },
                            { width: 300 },
                            { width: 250 },
                            { width: 150 },
                        ],
                        title: FileName,
                        rows: rows
                    },
                ]
            });

            var nameOfPage = FileName;
            //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            return false;
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        function BindGrid()
        {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {  
                        read: {
                            url: '<% =Path%>Data/MGMTDashboradPenaltyDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&CBID=<% =branchid%>&attrbu=<% =attribute%>&Sdatev=<% =FromDate%>&Edatev=<% =Enddate%>&penaltystatus=' + $("#dropdownlistUserRole").val() + '&isapprover=<% =isapprover%>&IS=<% =IS%>&Isdept=<% =IsDeptHead%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/MGMTDashboradPenaltyDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&CBID=<% =branchid%>&attrbu=<% =attribute%>&Sdatev=<% =FromDate%>&Edatev=<% =Enddate%>&penaltystatus=' + $("#dropdownlistUserRole").val() + '&isapprover=<% =isapprover%>&IS=<% =IS%>&Isdept=<% =IsDeptHead%>'
                    },
                    schema: {
                        data: function (response) {
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].penalty;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].Deptpenalty;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].penaltyapprover;
                            }                            
                        },
                        total: function (response) {                            
                            if (<% =ComplianceTypeID%> == 0) {
                                return response[0].penalty.length;
                            }
                            else if (<% =ComplianceTypeID%> == 1) {
                                return response[0].Deptpenalty.length;
                            }
                            else if (<% =ComplianceTypeID%> == 2) {
                                return response[0].penaltyapprover.length;
                            }                            
                        }
                    },
                    pageSize: 10
                },
                toolbar: kendo.template($("#template").html()),
               // height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    //{
                    //    field: "Branch", title: 'Location',
                    //    width: "17%;",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'

                    //    }, filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "ActName", title: 'Act',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                    
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                    //    field: "ScheduledOn", title: 'Due Date',
                    //    type: "date",

                    //    template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    //    filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                        //},
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",    
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }    
                    },        
                    {
                        field: "UserName", title: 'User',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Interest", title: 'Interest',                        
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Penalty", title: 'Penalty',                        
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                        {
                            field: "Remarks", title: 'Remark',                        
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        }
                ]
            });


            $("#grid").kendoTooltip({                
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#grid").kendoTooltip({
                filter: "td",                
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    if ($(target).text() == "  ") {

                        return "Action";
                    }
                    else
                    {
                        return $(target).text();
                    }                   
                }
            }).data("kendoTooltip");
            window.parent.forchild($("body").height()+50);  
        }
        
        $(document).ready(function () {
          
            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    $("#dropdowntree").data("kendoDropDownTree").value([]);
                    $('#ClearfilterMain').css('display', 'none');
                    $("#grid").data("kendoGrid").dataSource.filter({});
                    BindGrid();
                },
                index: 0,
                dataSource: [                    
                    { text: "Submitted", value: "0" },
                    { text: "Pending", value: "1" }
                ]
            });

             $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"
                    }
                }
            });

             $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                 change: function (e) {                  
                     FilterGrid();
                     fCreateStoryBoard('dropdownACT', 'filterAct', 'act')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
                    }
                },dataBound: function(e) {
                    e.sender.list.width("1000");
                }
            });

             $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {    
                    FilterGrid();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                    read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            BindGrid();
        });

        function FilterGrid() {        
            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });            
           
            var Roledetails = [];
  
            //Act Details
            var Actdetails = [];
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                Actdetails.push({
                    field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                });
            }

            //Function Details
            var Functiondetails = [];
            if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                Functiondetails.push({
                    field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                });
            }

            var dataSource = $("#grid").data("kendoGrid").dataSource;

            //four
            if (locationsdetails.length > 0
                && Roledetails.length > 0
                && Functiondetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Roledetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

                //three
            else if (locationsdetails.length > 0
                && Roledetails.length > 0
                && Functiondetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Roledetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        }
                    ]
                });
            }

            else if (locationsdetails.length > 0
                && Functiondetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

            else if (Roledetails.length > 0
                && Functiondetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Roledetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

                //two
            else if (locationsdetails.length > 0
                && Roledetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Roledetails
                        }
                    ]
                });
            }

            else if (locationsdetails.length > 0
                && Functiondetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        }
                    ]
                });
            }

            else if (locationsdetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

            else if (Roledetails.length > 0
                && Functiondetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Roledetails
                        },
                        {
                            logic: "or",
                            filters: Functiondetails
                        }
                    ]
                });
            }

            else if (Roledetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Roledetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

            else if (Functiondetails.length > 0
                && Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Functiondetails
                        },
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

                //one
            else if (Functiondetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Functiondetails
                        }
                    ]
                });
            }

            else if (Actdetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Actdetails
                        }
                    ]
                });
            }

            else if (locationsdetails.length > 0) 
            {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }

        
            else if (Roledetails.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Roledetails
                        }
                    ]
                });
            }

            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }
        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            //$("#dropdownlistUserRole").data("kendoDropDownList").value([]);
            $("#dropdownfunction").data("kendoDropDownList").value([]);
            $("#dropdownACT").data("kendoDropDownList").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            //fCreateStoryBoard('dropdownlistUserRole', 'filterrole', 'role');
            fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
            fCreateStoryBoard('dropdownACT', 'filterAct', 'act');

            CheckFilterClearorNotMain();
        }

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistUserRole').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
            }
            else if (div == 'filterrole') {
                $('#' + div).append('Role&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

        
    </script>
    
  </head>
    <body>
  <form>
    <div id="example">

        <div class="row">
        <div class="col-lg-12 col-md-12 colpadding0">
            <h1 id="display" runat="server" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;color: #666;margin-bottom: 12px;font-size: 19px;padding-left:5px;padding-top:5px;font-weight:bold;"></h1>

        </div>
            </div>

            <div class="row" style="padding-bottom: 37px;">
                        <div class="toolbar"> 
                            <div class="row">
                            <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:270px;padding-right:12px;"/>                                           
                            <input id="dropdownlistUserRole" data-placeholder="Role" style="width:200px;"/> 
                            <input id="dropdownfunction" style="width:255px;margin-left:12px;"/>
                            <input id="dropdownACT" style="width:300px;margin-left:12px;"/>    
                            <button id="export" onclick="exportReport(event)" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none;" data-toggle="tooltip" title="Export to Excel"></button> 
                                                   
                        </div>               
                    </div>
                </div> 
            
            
            
            <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 13.6%;">
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 10px;">                           
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">                           
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="padding-left: 435px;">                             
                             <button id="ClearfilterMain" style="float: right;margin-top: -37px;margin-right: 42px; margin-left: 1%; height: 28px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>                                                                      
                        </div>
                    </div>
                </div>                           
       
            <div class="row" style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterrole">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterstatus">&nbsp;</div>
                
        <div id="grid" style="margin-top: 6px;"></div>       
    </div>   
  </form>
        </body>
 </html>

