﻿
function ChangeCustomer() {   
    Bind_EntityBranch();
    Bind_AssignedUsers();
    ApplyFilter();
}

function ApplyFilter() {
       
    //Customer
    var selectedCustIDs = $("#ddlCustomers").val();

    //EntityBranch
    var selectedEntityBranches = $("#ddlTreeEntityBranch").data("kendoDropDownTree")._values;

    //User/SPOC
    var selectedUsers = $("#ddlUsers").data("kendoDropDownTree")._values;

    //ComType           
    var selectedComTypes = $("#ddlComplianceType").val();

    var finalSelectedfilter = { logic: "and", filters: [] };

    if (selectedCustIDs != "" || selectedEntityBranches.length > 0 || selectedComTypes != "" || selectedUsers.length > 0) {

        if (selectedCustIDs != '' && selectedCustIDs != null && selectedCustIDs != undefined && selectedCustIDs != "-1") {
            var custFilter = { logic: "or", filters: [] };

            custFilter.filters.push({
                field: "CustomerID", operator: "eq", value: selectedCustIDs
            });

            finalSelectedfilter.filters.push(custFilter);
        }

        if (selectedEntityBranches.length > 0) {
            var branchFilter = { logic: "or", filters: [] };

            $.each(selectedEntityBranches, function (i, v) {
                branchFilter.filters.push({
                    field: "BranchID", operator: "eq", value: v
                });
            });

            finalSelectedfilter.filters.push(branchFilter);
        }

        if (selectedUsers.length > 0) {
            var userFilter = { logic: "or", filters: [] };

            $.each(selectedUsers, function (i, v) {               
                userFilter.filters.push({
                    field: "Performer", operator: "eq", value: v.UserName
                });

                userFilter.filters.push({
                    field: "Reviewer", operator: "eq", value: v.UserName
                });
            });

            finalSelectedfilter.filters.push(userFilter);
        }

        if (selectedComTypes != '' && selectedComTypes != null && selectedComTypes != undefined && selectedComTypes!="-1") {
            var comTypeFilter = { logic: "or", filters: [] };

            comTypeFilter.filters.push({
               // field: "StateID", operator: "eq", value: selectedComTypes
            });

           // finalSelectedfilter.filters.push(comTypeFilter);
        }
    }

    if (finalSelectedfilter.filters.length > 0) {
        var dataSource = $("#grid").data("kendoGrid").dataSource;
        dataSource.filter(finalSelectedfilter);
        $('#btnClearFilter').css('display', 'block');
    } else {
        var dataSource = $("#grid").data("kendoGrid").dataSource;
        dataSource.filter({});
        $('#btnClearFilter').css('display', 'none');
    }
}

function ClearFilter(e) {
    e.preventDefault();
    //e.stopPropogation();
    $("#ddlCustomers").data("kendoDropDownList").value("-1");
    $("#ddlTreeEntityBranch").data("kendoDropDownTree").value([]);
    $("#ddlUsers").data("kendoDropDownTree").value([]);
    //$("#ddlComplianceType").data("kendoDropDownList").value([]);
    //$("#ddlUserRole").data("kendoDropDownList").value("-1");
   
    $('#btnClearFilter').css('display', 'none');

    $("#grid").data("kendoGrid").dataSource.filter({});

    return false;
}
