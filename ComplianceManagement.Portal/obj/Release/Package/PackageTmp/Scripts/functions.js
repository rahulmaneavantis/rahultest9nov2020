$(function() {
	$('.field, textarea').focus(function() {
        if(this.title==this.value) {
            this.value = '';
        }
    }).blur(function(){
        if(this.value=='') {
            this.value = this.title;
        }
    });

    $("#slider").jcarousel({
        scroll: 1,
        auto: 3,
        wrap: 'both'     
    });

     $(".projects-slider").jcarousel({
        scroll: 1,
        auto: 3,
        wrap: 'both'     
    });

    if ($.browser.msie && $.browser.version == 6) {
        DD_belatedPNG.fix('#wrapper-top, #wrapper-middle, #wrapper-bottom, #search, #slider img, .jcarousel-prev, .jcarousel-next');
    }
});

Cufon.replace('#slider h2',{ fontFamily: 'HelveticaNeue' });
Cufon.replace('#slider h3', { fontFamily: 'HelveticaNeueThin'});
Cufon.replace('#slider h3 span', { fontFamily: 'HelveticaNeueBold'});