﻿<%@ Page Title="MY DASHBOARD" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LitigationManagementDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard.LitigationManagementDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Offline-->
    <script type="text/javascript" src="../../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="../../avantischarts/highcharts/js/modules/no-data-to-display.js"></script>--%>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/modules/exporting.js"></script>
    <script type="text/javascript" src="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <link href='<%# ResolveUrl("../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css")%>' rel="stylesheet" type="text/css" />
    <link href='<%# ResolveUrl("../../newcss/spectrum.css")%>' rel="stylesheet" type="text/css" />
    <link href='<%# ResolveUrl("../../NewCSS/responsive-calendar.css")%>' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/responsive-calendar.min.js"></script>

    <script type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });
        function ftotalliability() {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '650px');
            $('.modal-dialog').css('width', '88%');
            $('#showdetails').attr('src', "../aspxPages/Valueashboard.aspx");
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            imgExpandCollapse();
        });

        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {
            $("[src*=collapse]").on('click', function () {

                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {
                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }

        function ChangeRowColor(rowID) {

            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }

    </script>

    <!-- High Charts-->
    <script type="text/javascript">

        //<!-- Graph-Case/Notice Branch Wise -->
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphBranchWise',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 1,
                            }
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=NoticeCaseBranchWiseData%>
                        ],
                        size: '120%',
                        showInLegend: false,
                    }]
                },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
            });
        });
        //<!-- Graph-Case/Notice Branch Wise END-->

        //<!-- Graph-Case Stage Wise START -->
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphCaseStageWise',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                distance: 2,
                                format: '{y}',
                                //enabled: true,
                                //format: '{y}',
                                //distance: 1,
                            }
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=CaseStageWiseData%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
            });
        });
        //<!-- Graph-Case Stage Wise END-->

        //<!-- Graph-Case/Notice Winning Prospect Wise -->
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                var DeptWiseCaseNoticeColumnChart =
        Highcharts.chart('divGraphWinningProspect', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },


            xAxis: { categories: ['High', 'Medium', 'Low'] },

            yAxis: {
                title: {
                    text: '# of Notice/Cases'
                }
            },
            legend: {
                enabled: false,
                //enabled: true,
                //itemDistance: 0,
                //itemStyle: {
                //    textshadow: false,
                //},
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        style: {
                            textShadow: false,
                            // color:'red',
                        }
                    },
                },
            },

            tooltip: {
                //headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },

            series: [{
                colorByPoint: true,
                showInLegend: true,
                data: [ <%=NoticeCaseWinningProspectData%>]
            }]
        });
            });
        });
//<!-- Graph-Case/Notice Winning Prospect Wise END-->

//<!-- Graph-Case/Notice Category Wise Count -->
$(document).ready(function () {
    $(function () {
        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'divGraphCaseCategory',
                type: 'pie',
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            plotOptions: {
                pie: {
                    //innerSize: '40%',
                    dataLabels: { enabled: true, format: '{point.y}', distance: 2 }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            series: [{
                data: [
                            <%=NoticeCaseTypeWiseSeriesData%>
                ],
                size: '120%',
                showInLegend: true,
            }]
        },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
    });
});
//Graph 2-Case/Notice Type Wise Count-END

//<!-- Graph-Case/Notice Department Wise -->
$(document).ready(function () {
    $(function () {
        // Chart Global options
        Highcharts.setOptions({
            credits: {
                text: '',
                href: 'https://www.avantis.co.in',
            },
            lang: {
                drillUpText: "< Back",
            },
        });

        var DeptWiseCaseNoticeColumnChart =
Highcharts.chart('divGraphDepartmentWise', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },

            <%=strNoticeCaseDepartments%>

    yAxis: {
        min: 0,
        title: {
            text: '# of Notice/Cases'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
            }
        }
    },
    legend: {
        enabled: false,
        //enabled: false,
        //itemDistance: 0,
        //itemStyle: {
        //    fontSize: '10px'
        //},
    },
    credits: {
        enabled: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                style: {
                    textShadow: false,
                    color: 'white',
                    // fontWeight: 'bold',
                }
            }
        }
    },
    series: [
        <%--data: [ <%=NoticeCaseDepartmentWiseData%> ]  In case of No Stacked Colum--%>
        <%=NoticeCaseDepartmentWiseData%>
    ]
});
    });
});

        //<!-- Graph 4-Case/Notice Department Wise END-->

        //<!-- Graph-Expenses-Case/Notice Wise -->
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphExpenses',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 1,
                            },
                            style:
                                {
                                    textShadow: false,
                                    color: 'white',
                                },
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=NoticeCaseWiseExpensesData%>
                        ],
                        size: '120%',
                        showInLegend: false,
                    }]
                },

            function (chart) { // on complete
                var textX = chart.plotLeft + (chart.plotWidth * 0.5);
                var textY = chart.plotTop + (chart.plotHeight * 0.5);

                var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
                span += '<span style="font-size: 20px;font-family: sans-serif;">Total </span><br>';
                span += '<span style="font-size: 16px">1000</span>';
                span += '</span>';

                $("#addText").append(span);
                span = $('#pieChartInfoText');
                span.css('left', textX + (span.width() * -0.5));
                span.css('top', textY + (span.height() * -0.5));
            });
            });
        });
        //Graph-Expenses-Case/Notice Wise-END


        //<!-- Graph-Case/Notice Expense Department Wise -->
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphExpenseDeptWise',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: {
                                enabled: true, distance: -2
                            }
                        },
                        style: {
                            textShadow: false,
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=NoticeCaseExpenseDepartmentWise%>
                        ],
                        size: '120%',
                        showInLegend: false,
                    }]
                },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
            });
        });
        //Graph-Case/Notice Expense Department Wise-END  

        //<!-- Graph-Case/Notice Type Wise --> Inward - Outward
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphNoticeCaseType',
                        type: 'pie',
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            //innerSize: '40%',
                            dataLabels: { enabled: true, format: '{y}', distance: 2 }
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=NoticeCaseTypeWise%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                },

        function (chart) { // on complete
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);

            var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
            span += '<span style="font-size: 20px;font-family: sans-serif;"> </span><br>';
            span += '<span style="font-size: 16px">1000</span>';
            span += '</span>';

            $("#addText").append(span);
            span = $('#pieChartInfoText');
            span.css('left', textX + (span.width() * -0.5));
            span.css('top', textY + (span.height() * -0.5));
        });
            });
        });
        //Graph-Case/Notice Expense Department Wise-END 

        //Graph-Case/Notice Ageing

        //Graph 1- Ageing - Less than a year
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphAgeing1',
                        type: 'pie',
                        marginBottom: 10,
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Less than a year'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 2,
                                //enabled: false,
                                //distance: -2,
                                //color: 'white',
                                style: {
                                    textShadow: false,
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['50%', '75%']
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=noticeCaseAgeingLessThanYear%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                })
            });
        });
        //Graph 1- Ageing - Less than a year -END

        //Graph 2- Ageing - 1 to 2 years
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphAgeing2',
                        type: 'pie',
                        marginBottom: 10,
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '1 to 2 years'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 2,
                                //enabled: false,
                                //distance: -2,
                                //color: 'white',
                                style: {
                                    textShadow: false,
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['50%', '75%']
                        }
                    },
                    style: {
                        textShadow: false,
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=noticeCaseAgeing1to2Years%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                })
            });
        });
        //Graph 2- Ageing - 1 to 2 years -END

        //Graph 3- Ageing - 2 to 3 years
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphAgeing3',
                        type: 'pie',
                        marginBottom: 10,
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: '2 to 3 years'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 2,
                                //enabled: false,
                                //distance: -2,
                                //color: 'white',
                                style: {
                                    textShadow: false,
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['50%', '75%']
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=noticeCaseAgeing2to3Years%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                })
            });
        });
        //Graph 3- Ageing - 2 to 3 years -END

        //Graph 4- Ageing - More than 3 years
        $(document).ready(function () {
            $(function () {
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'divGraphAgeing4',
                        type: 'pie',
                        marginBottom: 10,
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'More than 3 years'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: '40%',
                            dataLabels: {
                                enabled: true,
                                format: '{y}',
                                distance: 2,
                                //enabled: false,
                                //distance: -2,
                                //color: 'white',
                                style: {
                                    textShadow: false,
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: ['50%', '75%']
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    series: [{
                        data: [
                        <%=noticeCaseAgeingMoreThan3Years%>
                        ],
                        size: '120%',
                        showInLegend: true,
                    }]
                })
            });
        });
        //Graph 4- Ageing - More than 3 years -END

        //Graph-Case/Notice Ageing-END
        //AID- AgeingID
        function ShowGraphDetail(NOC, Type, BID, PID, DID, CID, RID, SID, Status, AID) {

            $('#divGraphDetails').modal('show');
            $('.modal-dialog').css('width', '1300px');
            $('#showChartDetails').attr('src', 'about: blank');
            $('#showChartDetails').attr('width', '1250px');
            $('#showChartDetails').attr('height', '620px');
            //$('#showChartDetails').attr('height', 'auto !important');
            $('#showChartDetails').attr('src', "../Dashboard/DashboardGraphDetail.aspx?NOC=" + NOC + "&Type=" + Type + "&Status=" + Status + "&BID=" + BID + "&PID=" + PID + "&DID=" + DID + "&CID=" + CID + "&RID=" + RID + "&SID=" + SID + "&AID=" + AID);
        }

        function ShowDialog(taskID, noticeCaseInstanceID) {
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '80%');
            $('.modal-dialog').css('height', '90%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '500px');
            $('#showdetails').attr('src', "../aspxPages/TaskDetailPage.aspx?TaskID=" + taskID + "&NID=" + noticeCaseInstanceID);
        };

        function ShowNoticeCaseDialog(noticeCaseInstanceID, type) {
            if (type != '') {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '650px');
                $('.modal-dialog').css('width', '95%');
                $('.modal-dialog').css('height', '500px');

                if (type == 'C') {
                    $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + noticeCaseInstanceID + "&Flagtab=H");
                }
                else if (type == 'N') {
                    //    $('#showdetails').attr('src', "../Common/ShowNoticeDetail.aspx?AccessID=" + noticeCaseInstanceID);
                    $('#showdetails').attr('src', "../aspxPages/NoticeDetailPage.aspx?AccessID=" + noticeCaseInstanceID);
                }
            }
        };

        function ShowUpcomingHearingPop(noticeCaseInstanceID, type) {
            if (type != '') {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '500px');
                $('.modal-dialog').css('width', '95%');
                //$('.modal-dialog').css('height', '400px');

                if (type == 'C') {
                    $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + noticeCaseInstanceID + "&Flagtab=H");
                }
                else if (type == 'N') {
                    $('#showdetails').attr('src', "../aspxPages/NoticeDetailPage.aspx?AccessID=" + noticeCaseInstanceID);
                }
            }
        };

    </script>

    <style type="text/css">
        @media screen and (max-width: 750px) {
            iframe {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
        }

        .panel .panel-heading {
            border: none;
            background: #fff;
            padding: 0;
        }

            .panel .panel-heading h2 {
                font-size: 20px;
            }

        .mang-dashboard-white-widget {
            background: #fff;
            padding: 5px 0px 5px 0px;
            margin-bottom: 10px;
            border-radius: 10px;
        }

        .panel {
            margin-bottom: 10px;
        }

            .panel .panel-heading h2 {
                font-size: 20px;
            }

            .panel .panel-heading-dashboard {
                border: none;
                background: #fff;
                padding: 0;
            }

        .d-none {
            display: none !important;
        }

        .info-box {
            min-height: 115px;
            margin-bottom: 10px;
            border: 1px solid #ADD8E6;
        }
    </style>

    <%--//Calendar Start//--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $.get("/Litigation/Common/UpcomingHearingCalendar.aspx?m=3&type=1", function (data) {
                $(".responsive-calendar").html(data);
            });

            setInterval(setcolor, 1000);
        });

        function setcolor() {
            $('.Upcoming').closest('div').find('a').css('background-color', '#1d86c8');
            $('.Pending').closest('div').find('a').css('background-color', '#FFC200');
        }
        $(document).ready(function (ClassName) {
            $("a").addClass(ClassName);
            //fcal( new Date().toLocaleDateString());
            fcal(null);
        });

        function formatDate(date) {
            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        function hideloader() {
            $('#imgcaldate').hide();
        }

        function fcal(dt) {
            try {
                if (dt != null) {
                    var ddata = new Date(dt);
                    $('#clsdatel').html('Hearing for date ' + formatDate(ddata));
                }
                else {
                    $('#clsdatel').html('Hearing of all date');
                }
            } catch (e) { }
            //$('#imgcaldate').show();
            //$('#calframe').attr('src', '/Litigation/Common/UpcomingHearingGridData.aspx?m=3&date=' + dt + '&type=' + 1)
            $('#calframe').attr('src', '/Litigation/Common/calendardataAPINew.aspx?m=3&date=' + dt + '&type=' + 1);
            return;
        }

    </script>
    <style type="text/css">
        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 50px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 105px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        .info-box {
            margin-bottom: 0px !important;
            border: 1px solid #ADD8E6;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 20px;
        }
    </style>
    <%--//Calendar End//--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hiddenColor" style="display: none;" />
    <div class="row1" style="background: none;">
        <div class="dashboard1">

            <div class="col-md-12">
                <div id="countSummary" class="col-lg-12 col-md-12 colpadding0">
                    <div class="panel panel-default" style="background: none;">
                        <div class="panel-heading mb10" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCountSummary" style="background: none;">
                            <h2>Summary</h2>
                            <div class="panel-actions">
                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCountSummary"><i class="fa fa-chevron-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="collapseDivCountSummary" class="panel-collapse collapse in" style="margin-bottom: 10px">
                <div class="row col-md-12">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Notice</div>

                            <div class="col-md-6  borderright">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Open&CT=N">
                                    <div class="count" runat="server" id="divOpenNoticeCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openNoticePer = GetOpenNoticeGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openNoticePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Closed&CT=N">
                                    <div class="count" runat="server" id="divClosedNoticeCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedNoticePer = GetClosedNoticeGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedNoticePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Case</div>

                            <div class="col-md-6 borderright">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Open&CT=C">
                                    <div class="count" runat="server" id="divOpenCaseCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openCasePer = GetOpenCaseGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openCasePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Closed&CT=C">
                                    <div class="count" runat="server" id="divClosedCaseCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedCasePer = GetClosedCaseGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedCasePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Task</div>

                            <div class="col-md-6 borderright">
                                <a href="../aspxPages/TaskListNew.aspx?Status=Open">
                                    <div class="count" runat="server" id="divOpenTaskCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openTaskPer = GetOpenTaskGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openTaskPer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="../aspxPages/TaskListNew.aspx?Status=Closed">
                                    <div class="count" runat="server" id="divClosedTaskCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedTaskPer = GetClosedTaskGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedTaskPer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 89)
                     {%>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Total Liability</div>                        
                            <div class="col-md-12">
                               <%-- <a href="../aspxPages/Valueashboard.aspx"></a>--%>
                                    <div class="count" runat="server" onclick="ftotalliability()" id="divTotalLiability">0</div>                             
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                     <%}%> 
                </div>
            </div>

            <!-- Upcoming Hearing-->
            <div class="row">
                <div class="col-md-12">
                    <div id="divOuterUpcomingHearing" class="row mang-dashboard-white-widget" style="margin-top: 10px">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingHearing" style="margin-left: 10px;">
                                        <h2>Hearing Calendar</h2>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingHearing">
                                                <i class="fa fa-chevron-down"></i></a>
                                        </div>
                                    </div>
                                    <div id="collapseUpcomingHearing" class="panel-collapse collapse in">
                                        <div class="panel-body" style="max-height: 500px; overflow: auto; overflow-y: hidden;">
                                            <div class="col-md-12">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="UpcomingHearingTaskValidationGroup" />
                                                <asp:CustomValidator ID="cvUpcomingHearing" runat="server" EnableClientScript="False"
                                                    ValidationGroup="UpcomingHearingTaskValidationGroup" Display="None" />
                                            </div>

                                            <div class="col-md-5 colpadding0" style="margin-bottom: 27px;">
                                                <!-- Responsive calendar - START -->
                                                <div class="responsive-calendar" style="width: 95%; margin-top: 15px;">
                                                    <img src="../../images/processing.gif">
                                                </div>
                                                <!-- Responsive calendar - END -->
                                                <div>
                                                    <asp:LinkButton runat="server" ID="btnCreateICS" OnClick="btnCreateICS_Click" ToolTip="Download Calendar (Outlook, Google)" data-toggle="tooltip" Style="float: right; margin-right: 76px">
                                                        <img src="../../Images/Calendar_ICS.png" style="position: absolute;"/>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div id="divUpcomingHearing">
                                                    <div>
                                                        <span style="float: left; height: 20px; font-family: 'Roboto',sans-serif; color: #666; font-size: 16px;" id="clsdatel"></span>
                                                        <asp:LinkButton style="float:right;display:none;"  runat="server" ID="lnkShowAll" Text="Show All" OnClientClick="BindUpcomingGridAllData();"></asp:LinkButton>
                                                        <br />
                                                        <p style="font-family: 'Roboto',sans-serif; color: #666; float:left; width:100%">Select a date from calendar to view details</p>
                                                    </div>
                                                    <div class="clearfix" style="height: 0px;"></div>
                                                    <div id="datacal">
                                                        <img src="../../images/processing.gif" id="imgcaldate" style="position: absolute;">
                                                        <iframe id="calframe" src="about:blank" scrolling="no" frameborder="0" width="100%" height="450px"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Upcoming Hearing END-->

            <!-- Filters-->
            <div class="row">
                <div class="col-md-12">
                    <div class="row mang-dashboard-white-widget">
                        <div id="DivFilters" class="row mang-dashboard-white-widget">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#<%=collapseDivFilters.ClientID%>" style="margin-left: 5px;">
                                        <h2>Filters </h2>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#<%=collapseDivFilters.ClientID%>">
                                                <i class="fa fa-chevron-down"></i></a>
                                        </div>
                                    </div>

                                    <div id="collapseDivFilters" class="panel-collapse collapse in" runat="server">
                                        <div class="panel-body">
                                            <div class="col-md-12 colpadding0">

                                                <div class="col-md-3 colpadding0 entrycount">
                                                    <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                                        <ContentTemplate>
                                                            <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Entity/Branch/Location</label>
                                                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                                                CssClass="s" />
                                                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px;" id="divFilterLocation">
                                                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                                </asp:TreeView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Notice/Case</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Type" class="form-control" Width="95%">
                                                        <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                                                        <asp:ListItem Text="Notice" Value="N"></asp:ListItem>
                                                        <asp:ListItem Text="Case" Value="C"></asp:ListItem>
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="width: 24%;">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Notice/Case Type</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Status" class="form-control" Width="95%">
                                                        <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                                                        <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                                                        <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="width: 24%;">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Department</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Department" class="form-control" Width="95%" />
                                                </div>

                                            </div>

                                            <div class="clearfix" style="clear: both; height: 5px"></div>

                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-3 colpadding0 entrycount">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Opponent</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Opponent" class="form-control" Width="95%" />
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Status</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                        DataPlaceHolder="Select Status" class="form-control" Width="95%">
                                                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="width: 24%;">
                                                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Winning Prospect</label>
                                                    <asp:DropDownListChosen runat="server" ID="ddlWinningImpact" DataPlaceHolder="Select Risk" AllowSingleDeselect="false"
                                                        DisableSearchThreshold="5" CssClass="form-control" Width="95%">
                                                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                    </asp:DropDownListChosen>
                                                </div>

                                                <div class="col-md-3 colpadding0 entrycount" style="text-align: right;">
                                                    <label style="width: 100%; display: block; float: left; font-size: 13px; color: white">Filter</label>
                                                    <div style="float: left">
                                                        <asp:Button ID="btnFilter" class="btn btn-primary" runat="server" Text="Apply" OnClick="btnApplyFilter_Click" />
                                                    </div>
                                                    <div style="float: left; margin-left: 10px;">
                                                        <asp:Button ID="btnClearFilter" class="btn btn-primary" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Filters End-->

            <!-- First Row Start -->
            <div class="row">
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphNoticeCaseType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphNoticeCaseType" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphNoticeCaseType" style="margin-left: 10px;">
                                                <h2>Case/Notice Type</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphNoticeCaseType"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>

                                            <div id="collapseGraphNoticeCaseType" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphNoticeCaseType" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphCaseStageWise" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphCaseStageWise" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphCaseStageWise" style="margin-left: 10px;">
                                                <h2>Case Stage</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphCaseStageWise"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>

                                            <div id="collapseGraphCaseStageWise" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphCaseStageWise" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- First Row End -->

            <!-- Second Row Start -->
            <div class="row">
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphNoticeCaseCategory" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphCaseCategory" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphCaseCategory" style="margin-left: 10px;">
                                                <h2>Category Wise</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphCaseCategory"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>

                                            <div id="collapseGraphCaseCategory" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphCaseCategory" class="col-md-12" style="height: 350px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div class="col-md-6">
                    <asp:UpdatePanel ID="upDepartmentWise" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphDepartmentWise" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphDepartmentWise" style="margin-left: 10px;">
                                                <h2>Department Wise</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphDepartmentWise">
                                                        <i class="fa fa-chevron-down"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div id="collapseGraphDepartmentWise" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphDepartmentWise" class="col-md-12" style="height: 350px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- Second Row End -->

            <!-- Third Row Start -->
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphBranchWise" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphBranchWise" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphBranchWise" style="margin-left: 10px;">
                                                <h2>Branch Wise</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphBranchWise"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>
                                            <div id="collapseGraphBranchWise" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphBranchWise" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphWinningProspect" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphWinningProspect" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphWinningProspect" style="margin-left: 10px;">
                                                <h2>Winning Prospect Wise</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphWinningProspect"><i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>
                                            <div id="collapseGraphWinningProspect" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphWinningProspect" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- Third Row End -->

            <!-- Fourth Row Start -->
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upGraphExpense" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphExpenses" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphExpenses" style="margin-left: 10px;">
                                                <h2>Expenses</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphExpenses">
                                                        <i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>
                                            <div id="collapseGraphExpenses" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphExpenses" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upExpenseDeptWise" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divOuterGraphExpenseDeptWise" class="row mang-dashboard-white-widget">
                                <div class="dashboard">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphExpenseDeptWise" style="margin-left: 10px;">
                                                <h2>Expenses-Department Wise</h2>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphExpenseDeptWise">
                                                        <i class="fa fa-chevron-down"></i></a>
                                                </div>
                                            </div>
                                            <div id="collapseGraphExpenseDeptWise" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div id="divGraphExpenseDeptWise" class="col-md-12" style="height: 250px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- Fourth Row End -->

            <div class="row">
                <div class="col-md-12">
                    <div id="divOuterGraphAgeing" class="row mang-dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphAgeing" style="margin-left: 10px;">
                                        <h2>Ageing</h2>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseGraphAgeing">
                                                <i class="fa fa-chevron-down"></i></a>
                                        </div>
                                    </div>
                                    <div id="collapseGraphAgeing" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div id="divGraphAgeing1" class="col-md-12" style="height: 250px; width: 25%; float: left; overflow-y: auto;"></div>
                                            <div id="divGraphAgeing2" class="col-md-12" style="height: 250px; width: 25%; float: left; overflow-y: auto;"></div>
                                            <div id="divGraphAgeing3" class="col-md-12" style="height: 250px; width: 25%; float: left; overflow-y: auto;"></div>
                                            <div id="divGraphAgeing4" class="col-md-12" style="height: 250px; width: 25%; float: left; overflow-y: auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="divOuterTask" class="row mang-dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTask" style="margin-left: 10px;">
                                        <h2>Task</h2>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseTask">
                                                <i class="fa fa-chevron-down"></i></a>
                                        </div>
                                    </div>
                                    <div id="collapseTask" class="panel-collapse collapse in">
                                        <div class="panel-body" style="max-height: 250px; overflow: auto;">
                                            <div class="col-md-12">
                                                <asp:ValidationSummary ID="vsTaskPanel" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="DashboardTaskValidationGroup" />
                                                <asp:CustomValidator ID="cvTaskPanel" runat="server" EnableClientScript="False"
                                                    ValidationGroup="DashboardTaskValidationGroup" Display="None" />
                                            </div>
                                            <div id="divTask" class="col-md-12">
                                                <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%"
                                                    DataKeyNames="TaskID" OnRowDataBound="grdTaskActivity_RowDataBound" OnRowCreated="grdTaskActivity_RowCreated">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%--<img id="imgdiv<%# Eval("TaskID") %>" class="sample" src="/Images/add.png" alt="Show Responses" style="cursor: pointer" />--%>
                                                                <img id="imgCollapseExpand" class="sample" src="../../Images/add.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                                <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                    <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table"
                                                                        Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            data-toggle="tooltip" data-placement="bottom"
                                                                                            ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                        </asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Response Submitted yet.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblblTaskType" runat="server" Text='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":"" %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":""%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="40%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%" Visible="false">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                    <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                    <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")+","+Eval("NoticeCaseInstanceID")%>'
                                                                        AutoPostBack="true" OnClick="btnChangeStatus_Click"
                                                                        ID="lnkBtnTaskResponse" runat="server">
                                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View" title="View Task Detail/Submit Response" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <EmptyDataTemplate>
                                                        No Records Found.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="text-align: right">
                                            <asp:LinkButton runat="server" ID="lnkShowMoreTask" Text="Show more" Style="font-style: italic; text-decoration: underline;"
                                                PostBackUrl="~/Litigation/aspxPages/TaskListNew.aspx"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divGraphDetails" role="dialog" aria-labelledby="myModal" aria-hidden="true">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="width: 100%; height: 635px">
                    <iframe id="showChartDetails" src="about:blank" style="min-height: 550px !important" scrolling="auto" frameborder="0"></iframe>
                    <%--onload="" --%>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
