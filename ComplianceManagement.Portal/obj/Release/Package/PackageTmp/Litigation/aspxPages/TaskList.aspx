﻿<%@ Page Title="Task :: My Workspace" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="TaskList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.TaskList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/Task');
            BindControls();
        });

        function ShowDialog(taskID, noticeCaseInstanceID) {
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '80%');
            $('.modal-dialog').css('height', '90%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '500px');
            $('#showdetails').attr('src', "../aspxPages/TaskDetailPage.aspx?TaskID=" + taskID + "&NID=" + noticeCaseInstanceID);
        };

        function reloadTaskList() {
            //window.parent.location.reload(true); self.close();
            $('#divShowDialog').modal('hide');
            window.location.href = "../aspxPages/TaskList.aspx";
        }

        function reloadTaskListAdd() {
            //window.parent.location.reload(true); self.close();
            $('#divShowDialog').modal('hide');
            window.location.href = "../aspxPages/TaskList.aspx";
        }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=tbxTaskDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });
        }

        function ShowAddTaskModel(NoticeCaseInstanceID, TaskID) {
            $('#divShowAddTaskModal').modal('show');
            $('#IframeAddTask').attr('src', "../../Litigation/aspxPages/AddIndependentTask.aspx?NoticeCaseInstanceID=" + NoticeCaseInstanceID + "&TaskID=" + TaskID);
        }
    </script>
   

    <style type="text/css">
        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="TaskPageValidationGroup" />
                <asp:CustomValidator ID="cvTaskPage" runat="server" EnableClientScript="False"
                    ValidationGroup="TaskPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                        DataPlaceHolder="Select Type" class="form-control" Width="90%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                        <asp:ListItem Text="Task" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Notice" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Case" Value="2"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTaskStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Task Status" class="form-control" Width="90%">
                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Submitted" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTaskTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Task Type" class="form-control" Width="90%">
                        <asp:ListItem Text="All" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Case" Value="C"></asp:ListItem>
                        <asp:ListItem Text="Notice" Value="N"></asp:ListItem>
                        <asp:ListItem Text="Individual Task" Value="T"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Task Priority" CssClass="form-control" Width="90%">
                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                        <asp:ListItem Text="High" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                </div>
                <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; text-align: right; width: 25%;">
                    <asp:LinkButton Text="Apply" runat="server" ID="btnExport" Style="margin-top: 5px;" OnClick="btnExport_Click" data-toggle="tooltip" ToolTip="Export to Excel">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel"/> 
                            </asp:LinkButton>                    
<asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="LinkButton1" Width="23%" OnClick="lnkBtnApplyFilter_Click" style="line-height:27px" data-toggle="tooltip" ToolTip="Apply" />
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="lnkAdd" Width="27%" OnClick="lnkAdd_Click" data-toggle="tooltip" ToolTip="Add New Task">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    <%--<i class='fa fa-check'></i> --%>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Department" class="form-control" Width="90%" Visible="false" />
                </div>
            </div>

            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                    DataKeyNames="TaskID" OnSorting="grdTaskActivity_Sorting" OnRowCreated="grdTaskActivity_RowCreated"
                    OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" Visible="false" SortExpression="TaskType">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                    <asp:Label ID="lblblTaskType" runat="server" Text='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":"" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":""%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%" SortExpression="Priority">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%" SortExpression="TaskTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%" SortExpression="TaskDesc">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="15%" SortExpression="ScheduleOnDate">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="15%" SortExpression="AssignToName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                    <asp:Label runat="server" Visible="false" ID="lblCreatedBy" Text='<%# Eval("CreatedBy") %>'></asp:Label>
                                    <asp:Label runat="server" Visible="false" ID="lblNoticeCaseInstanceID" Text='<%# Eval("NoticeCaseInstanceID") %>'></asp:Label>

                                     <asp:LinkButton ID="lnkEditTask" runat="server" ToolTip="Edit Task Details" data-toggle="tooltip" CommandName="Edit_Task"
                                        CommandArgument='<%# Eval("TaskID")+","+Eval("NoticeCaseInstanceID")+","+Eval("TaskType")%>'>                                   
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Task Details"/></asp:LinkButton>

                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")+","+Eval("NoticeCaseInstanceID")%>'
                                        AutoPostBack="true" OnClick="btnChangeStatus_Click"
                                        ID="lnkBtnTaskResponse" runat="server" ToolTip="View/Submit Response" data-toggle="tooltip">
                                    <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Response"/>
                                    </asp:LinkButton>
                                   
                                    <asp:LinkButton ID="lnkDeleteTask" runat="server" CommandName="DELETE_Task" ToolTip="Delete Task" data-toggle="tooltip" Visible="true"
                                        CommandArgument='<%# Eval("TaskID")+","+Eval("NoticeCaseInstanceID")+","+Eval("TaskType")%>' OnClientClick="return confirm('Are you certain you want to Delete this Task?');">
                                     <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Task"/></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate></PagerTemplate>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5"/>
                        <asp:ListItem Text="10" Selected="True"/>
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
        <%--</section>--%>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowAddTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="height: 80%; width: 90%">
            <div class="modal-content" style="height: 100%;">
                <div class="modal-header">
                    <label class="modal-header-custom" id="Lawfirmmodel">
                        Add Task</label>
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="height: 100%;">
                    <iframe id="IframeAddTask" src="about:blank" width="100%" height="95%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
