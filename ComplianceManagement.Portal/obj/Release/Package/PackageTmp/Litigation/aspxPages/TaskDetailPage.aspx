﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskDetailPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.TaskDetailPage" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Notice Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">

        function OpenCaseNoticeHistoryPopup(NoticeCaseInstanceID, NoticeCaseType) {
            $('#divNoticeCaseHistoryPopup').modal('show');
            if (NoticeCaseType == 'C') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + "1");
            }
            else if (NoticeCaseType == 'N') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + "1");
            }
            else if (NoticeCaseType == 'T') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/AddIndependentTask.aspx?NoticeCaseInstanceID=" + NoticeCaseInstanceID + "&HistoryFlag=" + "1");
            }
        }

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function OpenDocviewer(file) {
            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function btnminimize(obj) {

            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function openNoticeModal() {
            $('#divAddNoticeModal').modal('show');
        }

        function reloadParentTaskListPage() {
            window.parent.reloadTaskList();
        }

    </script>

    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        .label {
            text-align: left;
            background: none !important;
            font-size: 13px;
            color: #333;
            font-weight: normal;
        }

        .Shorter {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 100%;
            display: block;
        }
        	
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container" style="background-color: #f7f7f7;">
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="row Dashboard-white-widget">
                <!--Task Detail Panel Start-->
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Task Detail">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                <a>
                                    <h2>Task Detail(s)</h2>
                                </a>
                                <div class="panel-actions">
                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div id="collapseDivTaskDetail" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="container">
                                    <asp:Panel ID="pnlTaskDetail" runat="server">
                                        <div class="row">
                                        <asp:LinkButton Text="More Details" runat="server" ID="btnMoreDetails" CssClass="btn btn-primary" 
                                         OnClick="btnMoreDetails_Click" style="float:right;"></asp:LinkButton>
                                        <asp:Label ID="LblTaskType" Visible="false" runat="server"/>
                                            </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <asp:Label ID="lblNameTag" runat="server" style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Notice/Case Title</asp:Label>
                                                <asp:Label ID="lblNoticeOrCaseTitle" runat="server" Width="85%" CssClass="label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Task Title</label>
                                                <asp:Label ID="lblTaskTitle" runat="server" Width="70%" CssClass="label"></asp:Label>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Due Date</label>
                                                <asp:Label ID="lblTaskDueDate" runat="server" Width="70%" CssClass="label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Assigned By</label>
                                                <asp:Label ID="lblAssignBy" runat="server" Width="70%" CssClass="label"></asp:Label>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Priority</label>
                                                <asp:Label ID="lblPriority" runat="server" Width="70%" CssClass="label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Task Description</label>
                                               <%-- <asp:Label ID="" runat="server" Width="85.5%" CssClass="label"></asp:Label>--%>
                                                <asp:TextBox ID="lblTaskDesc" runat="server" TextMode="MultiLine" ReadOnly="true" Width="85%" BorderStyle="None" Rows="5" style="font-size: 13px;color: #333;"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Remark</label>
                                                <asp:Label ID="lblTaskRemark" runat="server" Width="85.5%" CssClass="label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Uploaded Document</label>

                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblTaskDocuments" runat="server" CssClass="label"></asp:Label>
                                                        <asp:LinkButton ID="lnkBtnTaskDocuments" runat="server" OnClick="btnDownloadTaskDoc_Click"
                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download">
                                                                         <img src='/Images/download_icon_new.png' alt="Download" title="Download Documents" />
                                                        </asp:LinkButton>

                                                        <asp:LinkButton AutoPostBack="true" ID="lblTaskDocView" runat="server" data-toggle="tooltip"
                                                            OnClick="lblTaskDocView_Click" data-placement="bottom" ToolTip="View">
                                                         <img src='/Images/Eye.png' alt="View" title="View Document" />
                                                        </asp:LinkButton>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lnkBtnTaskDocuments" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Task Detail Panel  End-->

            <!--Task-Response Panel Start-->
            <div class="row Dashboard-white-widget">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">

                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Response Detail">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                <a>
                                    <h2>Response</h2>
                                </a>
                                <div class="panel-actions">
                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div id="collapseDivTaskResponse" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <div class="container">

                                            <div class="row">
                                                <div class="form-group col-md-12">

                                                    <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdTaskResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                OnPageIndexChanging="grdTaskResponseLog_OnPageIndexChanging" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Responded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                <asp:Label ID="lblResponseDate" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ResponseDate") %>'
                                                                                    Text='<%# Eval("ResponseDate") != DBNull.Value ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                <asp:Label ID="lblResRemark" runat="server" Text='<%# Eval("Remark") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%" Visible="false">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                </asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                        ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="ViewTaskResponseDoc"
                                                                                        ID="lnkBtnViewDocument" runat="server">
                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Documents" />
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                        AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                        ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                    </asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <EmptyDataTemplate>
                                                                    No Records Found
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlTaskResponse" runat="server">
                                                <div class="row">
                                                    <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="TaskResponseValidationGroup" />
                                                    <asp:CustomValidator ID="cvTaskResponse" runat="server" EnableClientScript="False"
                                                        ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Description</label>
                                                        <asp:TextBox ID="tbxTaskResDesc" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTaskResDesc" ErrorMessage="Provide Response Description"
                                                            ControlToValidate="tbxTaskResDesc" runat="server" ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Remark</label>
                                                        <asp:TextBox ID="tbxTaskResRemark" runat="server" CssClass="form-control" Width="85.5%"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Upload Document</label>
                                                        <div style="width: 100%;">
                                                            <div style="width: 50%; float: left;">
                                                                <asp:FileUpload ID="fuTaskResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12" style="text-align: center;">
                                                        <asp:Button CssClass="btn btn-primary" Style="background-color:#1fd9e1;border-color: #1fd9e1;" Text="Save" runat="server" ID="btnSaveTaskResponse" OnClick="btnSaveTaskResponse_Click"
                                                            ValidationGroup="TaskResponseValidationGroup"></asp:Button>
                                                        <asp:Button CssClass="btn btn-primary" Style="background-color:#1fd9e1;border-color: #1fd9e1;" Text="Clear" runat="server" ID="btnTaskResponseClear"  OnClick="btnClearTaskResponse_Click" />
                                                        <asp:Button CssClass="btn btn-primary" Style="background-color:#1fd9e1;border-color: #1fd9e1;" Text="Close Task" runat="server" ID="btnCloseTask" OnClick="btnCloseTask_Click" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveTaskResponse" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Task-Response Panel End-->

            <%--Document Viewer--%>
            <div>
                <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString() %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <%--Notice/Case History Popup--%>
            <div class="modal fade" id="divNoticeCaseHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="historyPopUpHeader">
                                Notice/Case Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameCaseHistory" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
