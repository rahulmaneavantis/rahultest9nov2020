﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="NoticeDetailPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.NoticeDetailPage" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Notice Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
            ddlStatusChange();
            rblImpactChange();
            applyCSSToNoticeDate();
            //ddlNoticeCategoryChange();
        });

        function NumberOnly() {
            var AsciiValue = event.keyCode
            if ((AsciiValue >= 48 && AsciiValue <= 57))
                event.returnValue = true;
            else
                event.returnValue = false;
        }

        $(function () {

            $('[id*=lstBoxOppositionLawyer]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=ddlLawyerTypes]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%'
            });


            $('[id*=lstBoxPerformer]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=lstBoxLawyerUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=ddlParty]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });
            $('[id*=DropDownListChosen1]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });
            $('[id*=ddlAct]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });
        });

        function OpenDocviewer(file) {
            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function OpenLawFirmPopupModel() {
            $('#AddLawFirmModelPopup').modal('show');
            $('#IFLawFirm').attr('src', "/Litigation/Masters/AddLawFirm.aspx");
        }

        function rebindLawyerUser() {
            $('[id*=lstBoxLawyerUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%'
            });
        }

        function HidShowTaskDiv() {
            $("#CollapsDivTaskFirstPanel").addClass('hide');
            $("#collapseDivTaskLogsPanelTwo").collapse('show');

            $('#tbxTaskTitle').val('');
            $('#tbxTaskDueDate').val('');
            $('#tbxTaskDesc').val('');
            $('#tbxTaskRemark').val('');

            $('#ddlTaskLawyerList').val(0);
            $('#ddlTaskUser').val(0);
            $('#ddlTaskPriority').val(0);
        }

        function HidShowTaskDivForEdit() {
            $("#CollapsDivTaskFirstPanel").addClass('hide');
            $("#collapseDivTaskLogsPanelTwo").collapse('show');
        }

        function OpposiLawerChangeAddButton() {
            var selectedPartyID = $("#lstBoxOppositionLawyer").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewOppoLawyerModal").show();
                }
                else {
                    $("#lnkShowAddNewOppoLawyerModal").hide();
                }
            }
        }
        function FirmLawyerChangeAddButton() {
            var selectedPartyID = $("#lstBoxLawyerUser").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewLawyerModal").show();
                }
                else {
                    $("#lnkShowAddNewLawyerModal").hide();
                }
            }
        }

        function OpenDoumentPopup(NoticeInstanceID) {
            $('#AddDocumentList').modal('show');
            $('#IFrameManageDocument').attr('src', "../../Litigation/aspxPages/UploadLitigationDocuments.aspx?NoticeCaseID=" + NoticeInstanceID + "&Flag=" + "Notice");
        }

        function ShowLawFirmAddbutton() {
            var selectedPartyID = $("#ddlLawFirm").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewLawFirmModal").show();
                }
                else {
                    $("#lnkShowAddNewLawFirmModal").hide();
                }
            }
        }

        function ShowTaskDiv() {
            $("#CollapsDivTaskFirstPanel").removeClass('hide');
            $("#CollapsDivTaskFirstPanel").collapse('show');
        }

        //Response
        function HidShowResponseDiv() {
            $("#CollapsDivResponceFirstPanel").addClass('hide');
            $("#collapseDivResponseLogsSecond").collapse('show');

            $('#tbxRespThrough').val('');
            $('#tbxRespRefNo').val('');
            $('#tbxResponseDesc').val('');
            $('#tbxResponseRemark').val('');
            $('#tbxResponseDate').val('');
        }

        function HidShowResponseDivForEdit() {
            $("#CollapsDivResponceFirstPanel").addClass('hide');
            $("#collapseDivResponseLogsSecond").collapse('show');
        }

        function HideFirstResponse() {
            $("#CollapsDivResponceFirstPanel").removeClass('hide');
            $("#CollapsDivResponceFirstPanel").collapse('show');
        }

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtNoticeDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxNoticeDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxDateReceipt]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxResponseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxTaskDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxNoticeCloseDate]').datepicker(
                    {
                        dateFormat: 'mm-dd-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxPaymentDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $("#ddlNoticeResponseDate").change(function () {

                    var Checkvalue = $("#ddlNoticeResponseDate").find("option:selected").val();
                    if (Checkvalue != null) {
                        if (Checkvalue == "0") {
                            $("#lblNoticeDate").text("Responded On");
                            $("#DivDueDate").hide();
                        }
                        else {
                            $("#lblNoticeDate").text("Receipt Date");
                            $("#DivDueDate").show();
                        }
                    }
                });
            });
        }

        function OpenSendMailPopup() {
            $('#divOpenSendMailPopup').modal('show');
        }

        function ShowNoticeDocumentDialog(CaseInstanceID, casetype) {
            $('#divComplianceDocumentShowDialog').modal('show');
            $('.modal-dialog').css('width', '100%');
            $('#IframeComplianceDocument').attr('width', '100%');
            $('#IframeComplianceDocument').attr('height', '475px');
            $('#IframeComplianceDocument').attr('background', '#fff;');
            $('#IframeComplianceDocument').attr('src', "../aspxPages/LitigationComplianceDocument.aspx?caseinstanceid=" + CaseInstanceID + "&Type=" + casetype);
        };

        function ClosePopComplianceDocumentDetialPage() {
            $('#divComplianceDocumentShowDialog').modal('hide');
        }

        $(document).ready(function () {
            $("#emmamiusers").hide();
        });
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function ddlActChange() {
            
            var selectedActID = $("#ddlAct").find("option:selected").text();
            strAddNew = "Add New";
            if (selectedActID != null) {
                if (selectedActID.indexOf(strAddNew) != -1) {
                    $("#lnkShowAddNewActModal").show();
                }
                else {
                    $("#lnkShowAddNewActModal").hide();
                }
            }
        }

        function ddlPartyChange() {
            var selectedPartyID = $("#ddlParty").find("option:selected").text();
            var strAddNew = "Add New";
            if (selectedPartyID != null) {
                if (selectedPartyID.indexOf(strAddNew) != -1) {
                    $("#lnkShowAddNewPartyModal").show();
                }
                else {
                    $("#lnkShowAddNewPartyModal").hide();
                }
            }
        }

        //Add criteria Popup
        function OpenCriteriaRetingPopUp(LawyerID) {
            $('#AddLayerRatingCriteriaShowDialog').modal('show');
            $('#IframeLayerRatingCriteria').attr('src', "../../Litigation/Masters/ADDCriteria.aspx?LawyerId=" + LawyerID);
        }


        function openNoticeModal() {
            $('#divAddNoticeModal').modal('show');
        }

        function openAddNewActModal() {
            $('#divAddNewActModal').modal('show');
        }

        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenAddActPopup() {
            $('#AddActPopUp').modal('show');
            $('#IframeAct').attr('src', "../../Litigation/Masters/AddAct.aspx");
        }

        function OpenAddFirmLawyerPopup() {
            $('#AddOpposiLawyerPopUp').modal('show');
            $('#IFrameOppoLawyer').attr('src', "../../Litigation/Masters/AddLawyer.aspx");
        }

        function OpenAddOppostitionLawyerPopup() {
            $('#AddOpposiLawyerPopUp').modal('show');
            $('#IFrameOppoLawyer').attr('src', "../../Litigation/Masters/AddOppositionLawyer.aspx");
        }


        function OpenPartyDetailsPopup() {
            $('#AddPartyPopUp').modal('show');
            $('#IframeParty').attr('src', "../../Litigation/Masters/AddPartyDetails.aspx");
        }

        function OpenCategoryTypePopup() {
            var a = '';
            $('#AddCategoryType').modal('show');
            $('#IframeCategoryType').attr('src', "../../Litigation/Masters/AddCaseType.aspx?CaseTypeId=" + a);
        }

        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "../../Litigation/Masters/AddUser.aspx");
        }

        function CloseNoticeDetailPage() {
            window.parent.ClosePopNoticeDetialPage();
        }

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            imgExpandCollapse();
        });
        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {
            $("[src*=collapse]").on('click', function () {

                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {

                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }

        function ChangeRowColor(rowID) {
            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            //alert(color);

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            //alert(oldColor);

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }

        function divExpandCollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "/Images/remove.png";
            } else {
                div.style.display = "none";
                img.src = "/Images/add.png";
            }
        }
    </script>

    <style type="text/css">
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }
    </style>

    <style>
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }

        span.label.label-info > label {
            color: #fff;
            font-weight: 100;
        }
    </style>
    <script>
        var view = $("#tslshow");
        var move = "100px";
        var sliderLimit = -750;


        $("#rightArrow").click(function () {
            // alert("right");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition >= sliderLimit) view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 })

        });

        $("#leftArrow").click(function () {
            // alert("left");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition < 0) view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 })

        });

    </script>

    <script type="text/javascript">

        function showHideAuditLog(divID, iID) {
            if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
                $(iID).attr("class", "fa fa-minus");
                $(divID).collapse('toggle');
            } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
                $(iID).attr("class", "fa fa-plus");
                $(divID).collapse('toggle');
            }
        }

         jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            $("html").getNiceScroll().resize();

            $('#updateProgress').hide();

            $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');

            $('input[id*=lstBoxFileTags]').hide();

            $('textarea').on('input', function () {
                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });

        });

//$('input[type="submit"]').click(function(){$('#updateProgress').show()});
//        });
//$('input[type="submit"]').click(function(){$('#updateProgress').show()});

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollDown() {
            $('html, body').animate({ scrollTop: $elem.height() }, 800);
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function hide(object) {
            if (object != null)
                object.style.display = "none";
        }

        function show(object) {
            if (object != null)
                object.style.display = "block";
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }


        function bindMultiSelect() {

            $('[id*=lstBoxTaskUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }
        var InvalidFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp",
            "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];

        function CheckValidation() {
            var fuSampleFile = $("#<%=fuTaskDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            $("#Labelmsg").css('display', 'none');
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }

        function CheckValidationForRes() {
            $("#lblValidResForDoc").css('display', 'none');
            var fuSampleFile = $("#<%=fuResponseDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#lblValidResForDoc").css('display', 'block');
                $('#lblValidResForDoc').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }


       
    </script>

    </head>
<body style="background: none !important; overflow-y: hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <input type="hidden" id="hiddenColor" style="display: none;" />

        <div style="background-color: #f7f7f7;">
            <header class="panel-heading tab-bg-primary" style="background: none !important;">
                <ul class="nav nav-tabs">
                    <li class="active" id="liNoticeDetail" runat="server">
                        <asp:LinkButton ID="lnkNoticeDetail" OnClick="lnkNoticeDetail_Click" runat="server" Style="background-color: #f7f7f7;">Notice Summary</asp:LinkButton>
                    </li>
                    <li class="" id="liDocument" runat="server">
                        <asp:LinkButton ID="lnkDocument" OnClick="TabDocument_Click" runat="server" Style="background-color: #f7f7f7;">Documents</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeTask" runat="server">
                        <asp:LinkButton ID="lnkNoticeTask" OnClick="lnkNoticeTask_Click" runat="server" Style="background-color: #f7f7f7;">Task/Activity</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeResponse" runat="server">
                        <asp:LinkButton ID="lnkNoticeResponse" OnClick="lnkNoticeResponse_Click" runat="server" Style="background-color: #f7f7f7;">Response</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeStatusPayment" runat="server">
                        <asp:LinkButton ID="lnkNoticeStatusPayment" OnClick="lnkNoticeStatusPayment_Click" runat="server" Style="background-color: #f7f7f7;">Status/Payment</asp:LinkButton>
                    </li>
                    <li class="" id="liLawyerRating" runat="server">
                        <asp:LinkButton ID="lnkLawyerRating" OnClick="lnkLawyerRating_Click" runat="server" Style="background-color: #f7f7f7;">Lawyer Rating</asp:LinkButton>
                    </li>
                    <li class="" id="liAuditLog" runat="server">
                        <asp:LinkButton ID="lnkAuditLog" OnClick="lnkAuditLog_Click" runat="server" Style="background-color: #f7f7f7;">Audit Log</asp:LinkButton>
                    </li>
                </ul>
            </header>

            <div class="clearfix" style="height: 20px;"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div style="height: 500px; overflow-y: auto;">
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="firstTabNoticeSummary" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                     <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 250px;overflow-y: auto;">
                                          <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                                               <asp:ValidationSummary ID="VSNoticePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                 ValidationGroup="NoticePopUpValidationGroup" />
                                               <asp:CustomValidator ID="cvNoticePopUp" runat="server" EnableClientScript="False"
                                                  ValidationGroup="NoticePopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </asp:Panel>
                                     </div>

                                <div id="divNoticeDetails" class="row Dashboard-white-widget">
                                    <!--NoticeDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Notice Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivNoticeDetails" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="form-group col-md-12">
                                                        <div class="col-md-6 float-left">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        </div>
                                                        <div class="col-md-6  float-left text-right" style="margin-bottom: 10px;">
                                                            <asp:UpdatePanel ID="upSendMailPop" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkLinkNotice" OnClientClick="OpenNoticeLinkingPopup();"
                                                                        data-toggle="tooltip" ToolTip="Link Notice with Other Notice(s)">
                                                                     <img src="../../Images/link-icon.png" alt="Link" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton ID="lnkActDetails" runat="server"
                                                                        OnClick="lnkActDetails_Click" data-toggle="tooltip" ToolTip="View Compliance Document(s)">
                                                                  <img src="../../Images/View-icon-new.png" alt="View" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="btnSendMailPopup" OnClientClick="OpenSendMailPopup()" data-toggle="tooltip" ToolTip="send mail with documents">
                                                                <img src="../../Images/send-mail-icon.png" alt="Send" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="btnEditNoticeDetail" OnClick="btnEditNoticeControls_Click" data-toggle="tooltip" ToolTip="Edit Notice Detail(s)">
                                                                <img src="../../Images/edit_icon_new.png" alt="Edit" title="" />
                                                                    </asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnEditNoticeDetail" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <%--  <asp:Button Text="Edit Detail(s)" runat="server" ID="btnEditNoticeDetail"
                                                                CssClass="btn btn-primary" OnClick="btnEditNoticeControls_Click" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <asp:Panel ID="pnlNotice" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Type</label>
                                                                <asp:RadioButtonList ID="rbNoticeInOutType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Inward" Value="I" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Outward" Value="O"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                              <div class="form-group col-md-6">
                                                                    <div class="form-group col-md-3 input-group date" style="width: 50%">
                                                                        <div>
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; margin-left: -14px;color: red;">*</label>
                                                                            <label style="width: 16.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Dated</label>
                                                                        </div>

                                                                        <div class="col-md-6 input-group date" style="width: 80%">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar color-black"></span>
                                                                            </span>
                                                                            <asp:TextBox runat="server" placeholder="Open Date" class="form-control" ID="txtNoticeDate" />
                                                                        </div>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Notice Date can not be empty."
                                                                            ControlToValidate="txtNoticeDate" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                    </div>

                                                                <div class="form-group col-md-3" style="width: 50%">
                                                                    <div>
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style=" display: block; float: left; font-size: 13px; color: #333;">
                                                                            Financial Year</label>
                                                                    </div>
                                                                    <div class="col-md-6 " style="width: 63%">                                                                  
                                                                       <%-- <asp:DropDownListChosen runat="server" ID="DropDownListChosen1" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                                                            CssClass="form-control" Width="100%">
                                                                        </asp:DropDownListChosen>--%>

                                                                         <asp:ListBox ID="DropDownListChosen1" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="120%" ></asp:ListBox>
                      

                                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage=" Financial Year can not be empty."
                                                                    ControlToValidate="DropDownListChosen1" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                    </div>
                                                                </div>
                                                              </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Reference No.</label>
                                                                <asp:TextBox runat="server" ID="tbxRefNo" Style="width: 60%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvRefNo" ErrorMessage="Reference No. can not be empty."
                                                                    ControlToValidate="tbxRefNo" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                <asp:RegularExpressionValidator ValidationGroup="NoticePopUpValidationGroup" Display="None" ControlToValidate="tbxRefNo" ID="revNoticeRefNo"
                                                                    ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Notice Reference No"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Notice Type</label>
                                                                <div style="float: left; width: 64%">
                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlNoticeCategory" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                OnSelectedIndexChanged="ddlNoticeCategory_SelectedIndexChanged" class="form-control" Width="100%" DataPlaceHolder="Select Notice Type" onchange="ddlNoticeCategoryChange()">
                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvNoticeCategory" ErrorMessage="Please Select Notice Type"
                                                                                ControlToValidate="ddlNoticeCategory" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlNoticeCategory" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                    <img id="lnkAddNewNoticeCategoryModal" runat="server" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCategoryTypePopup()" data-toggle="tooltip" data-placement="bottom" title="Add New Notice Type" />
                                                                    <asp:LinkButton ID="lnkBtnCategory" OnClick="lnkBtnCategory_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Act</label>
                                                                <div style="float: left; width: 60%">                                                                 
                                                                    <asp:ListBox ID="ddlAct" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlActChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act or Select 'Not Applicable'"
                                                                        ControlToValidate="ddlAct" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewActModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddActPopup()" alt="Add New Act" title="Add New Act" />
                                                                    <asp:LinkButton ID="lnkBtnAct" OnClick="lnkBtnAct_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Under Section</label>
                                                                <asp:TextBox runat="server" ID="tbxSection" Style="width: 64%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RegularExpressionValidator ValidationGroup="NoticePopUpValidationGroup" Display="None" ControlToValidate="tbxSection" ID="revSection"
                                                                    ValidationExpression="^[\s\S]{0,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Section"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opponent</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:ListBox ID="ddlParty" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlPartyChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Opponent"
                                                                        ControlToValidate="ddlParty" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewPartyModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenPartyDetailsPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                                                    <asp:LinkButton ID="lnkBtnParty" OnClick="lnkBtnParty_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opposition Lawyer</label>
                                                                <asp:ListBox ID="lstBoxOppositionLawyer" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="63.3%" onchange="OpposiLawerChangeAddButton()"></asp:ListBox>

                                                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewOppoLawyerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddOppostitionLawyerPopup()" alt="Add New Opposition Lawyer" title="Add New Opposition Lawyer" />
                                                                    <asp:LinkButton ID="lnkOpposiLawyer" OnClick="lnkOpposiLawyer_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13.2%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Notice Title
                                                                </label>
                                                                <asp:TextBox runat="server" ID="tbxTitle" Style="width: 82.5%;" CssClass="form-control" TextMode="MultiLine" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Notice Title"
                                                                    ControlToValidate="tbxTitle" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                <asp:RegularExpressionValidator ValidationGroup="NoticePopUpValidationGroup" Display="None" ControlToValidate="tbxTitle" ID="revNoticeTitle"
                                                                    ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Notice Title"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13.1%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Notice Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="width: 82.5%; min-height: 115px;" CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                                    ControlToValidate="tbxDescription" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Entity/Location</label>

                                                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 64%; background-color: #fff; cursor: pointer;"
                                                                    autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" ReadOnly="true" />
                                                                <%--onclick="txtclick()"--%>
                                                                <div style="margin-left: 28%; position: absolute; z-index: 10; width: 70%;" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                    </asp:TreeView>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranch" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>

                                                           <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Jurisdiction</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlJurisdiction" DataPlaceHolder="Select Jurisdiction" 
                                                                    AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="64%" />
                                                            </div>
                                                        </div>

                                                        <div class="row">

                                                             <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Department</label>
                                                                <div style="float: left; width: 64%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                    <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department"
                                                                        ControlToValidate="ddlDepartment" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                    <img id="lnkAddNewDepartmentModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                                                    <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                                 <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Contact Person Of Department</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCPDepartment" DataPlaceHolder="Select Contact Person Of Department"
                                                                     AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="64%" />

                                                                    <%--<asp:RequiredFieldValidator ID="rfvCPDepartment" ErrorMessage="Please Select Contact Person Of Department" InitialValue="0"
                                                                        ControlToValidate="ddlCPDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />--%>
                                                                </div>
                                                           
                                                              
                                                            </div>
                                                        <div class="row">
                                                                 <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                  Notice Term</label>
                                                            
                                                                    <asp:TextBox ID="tbxNoticeTerm" runat="server" Style="width: 64%;" onkeypress="return NumberOnly()" CssClass="form-control" MaxLength="100" autocomplete="off" onkeydown="return ((event.keyCode>=65 && event.keyCode>=96 && event.keyCode<=105)||!(event.keyCode>=65) && event.keyCode!=32);" />
                                                                    <%--onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"--%>
              
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group required col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Owner</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlOwner" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="64%" />
                                                                <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                    ControlToValidate="ddlOwner" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Winning Prospect</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlNoticeRisk" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="64%">
                                                                    <%-- <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Notice Budget</label>
                                                                <asp:TextBox runat="server" ID="tbxNoticeBudget" Style="width: 64%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                                                    ValidationGroup="NoticePopUpValidationGroup" ErrorMessage="Please enter a valid Notice Budget."
                                                                    ControlToValidate="tbxNoticeBudget" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Claimed Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxClaimedAmt" Style="width: 64%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvClaimedAmt" runat="server" ControlToValidate="tbxClaimedAmt" ErrorMessage="Only Numbers in Claimed Amount."
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    State</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlState" DataPlaceHolder="Select State" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="64%" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Probable Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxProbableAmt" Style="width: 64%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvProbableAmt" runat="server" ControlToValidate="tbxProbableAmt" ErrorMessage="Only Numbers in Probable Amount."
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>

                                                         <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Provisional Amount</label>
                                                                <asp:TextBox runat="server" ID="txtprovisionalamt" Style="width: 64%;" CssClass="form-control"  autocomplete="off" />
                                                                  <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtprovisionalamt" ErrorMessage="Only Numbers in Provisional Amount."
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            <%-- <asp:CompareValidator ValidationGroup="NoticePopUpValidationGroup" runat="server" ControlToValidate="txtprovisionalamt"  Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer"  ErrorMessage="Provisional amount cannot be less than zero" Display="Dynamic" Text="*"/>
                                                          --%>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Protest Money</label>
                                                                <asp:TextBox runat="server" ID="txtprotestmoney" Style="width: 64%;" CssClass="form-control" autocomplete="off" />
                                                             <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtprotestmoney" ErrorMessage="Only Numbers in protest money."
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            <%--<asp:CompareValidator ValidationGroup="NoticePopUpValidationGroup" runat="server" ControlToValidate="txtprotestmoney"  Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer"  ErrorMessage="Protest money cannot be less than zero" Display="Dynamic" Text="*"/>
                                                          --%>
                                                            </div>
                                                          
                                                        </div>

                                                         <div class="row">
                                                              <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Recovery Amount</label>
                                                                <asp:TextBox runat="server" ID="txtRecovery" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtRecovery" ErrorMessage="Recovery money should be numric"
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                               <%-- <asp:CompareValidator ValidationGroup="CasePopUpValidationGroup" runat="server" ControlToValidate="txtprotestmoney" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" ErrorMessage="Protest Money cannot be less than zero" Display="Dynamic" Text="*" />--%>

                                                            </div>
                                                             </div>

                                                        <div class="row">
                                                            
                                                             <div class="form-group col-md-12">
                                                                 <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 13.1%; display: block; float: left; font-size: 13px; color: #333;">
                                                                   Bank Gurantee</label>
                                                                <asp:TextBox runat="server" ID="txtbankgurantee" Style="width: 82.5%;  min-height: 115px" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                          
                                                           </div>
                                                          
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Potential Impact</label>
                                                                <asp:RadioButtonList ID="rblPotentialImpact" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Monetary" Value="M" Selected="True" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Non-Monetary" Value="N" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Both" Value="B" Selected="True" onclick="rblImpactChange()"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6" id="divMonetory">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxMonetory" Style="width: 64%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divNonMonetory">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Non-Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetory" Style="width: 64%;" CssClass="form-control" autocomplete="off" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Years</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetoryYears" Style="width: 64%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        

                                                        <div class="row">
                                                              <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>

                                                            <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Law Firm</label>
                                                                        <asp:DropDownListChosen ID="ddlLawFirm" CssClass="form-control" AutoPostBack="true" runat="server" 
                                                                            DataPlaceHolder="Select User" Width="64%"
                                                                            OnSelectedIndexChanged="ddlLawFirm_SelectedIndexChanged" onchange="ShowLawFirmAddbutton()" AllowSingleDeselect="false">
                                                                        </asp:DropDownListChosen>
                                                                        <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                            <%--OpenLawFirmPopupModel();lnkShowAddNewLawFirmModal--%>
                                                                            <img id="lnkShowAddNewLawFirmModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenLawFirmPopupModel();" alt="Add New Law Firm" title="Add New Law Firm" />
                                                                            <asp:LinkButton ID="lnkbindLawfirmp" OnClick="lnkbindLawfirmp_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlLawFirm" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="row" id="divDeposits" runat="server" visible="false">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Pre-Deposit</label>
                                                                <asp:TextBox runat="server" ID="txtPreDeposit" Style="width: 70%;" CssClass="form-control" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvPreDeposit" runat="server" ControlToValidate="txtPreDeposit" ErrorMessage="Only Numbers in Pre-Deposit"
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Post Deposit</label>
                                                                <asp:TextBox runat="server" ID="txtPostDeposit" Style="width: 70%;" CssClass="form-control" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvPostDeposit" runat="server" ControlToValidate="txtPostDeposit" ErrorMessage="Only Numbers in Post-Deposit"
                                                                    ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>

                                                        <div id="emmamiusers" class="row colpadding0" runat="server">
                                                            <div class="col-md-12 colpadding0">

                                                                <div class="row" id="divPeriod">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Period</label>
                                                                        <asp:DropDownListChosen runat="server" ID="ddlFY" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                                                            class="form-control" Width="70%">
                                                                            <asp:ListItem Text="2018-2019" Value="2018-2019"></asp:ListItem>
                                                                            <asp:ListItem Text="2017-2018" Value="2017-2018"></asp:ListItem>
                                                                            <asp:ListItem Text="2016-2017" Value="2016-2017"></asp:ListItem>
                                                                            <asp:ListItem Text="2015-2016" Value="2015-2016"></asp:ListItem>
                                                                            <asp:ListItem Text="2014-2015" Value="2014-2015"></asp:ListItem>
                                                                            <asp:ListItem Text="2013-2014" Value="2013-2014"></asp:ListItem>
                                                                            <asp:ListItem Text="2012-2013" Value="2012-2013"></asp:ListItem>
                                                                            <asp:ListItem Text="2011-2012" Value="2011-2012"></asp:ListItem>
                                                                            <asp:ListItem Text="2010-2011" Value="2010-2011"></asp:ListItem>
                                                                            <asp:ListItem Text="2009-2010" Value="2009-2010"></asp:ListItem>
                                                                            <asp:ListItem Text="2008-2009" Value="2008-2009"></asp:ListItem>
                                                                            <asp:ListItem Text="2007-2008" Value="2007-2008"></asp:ListItem>
                                                                            <asp:ListItem Text="2006-2007" Value="2006-2007"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            TaxDemand</label>
                                                                        <asp:TextBox runat="server" ID="txttaxDemand" Style="width: 65%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                                            ControlToValidate="txttaxDemand" runat="server"
                                                                            ErrorMessage="Please Insert Number only in TaxDemand" ValidationGroup="NoticePopUpValidationGroup"
                                                                            ValidationExpression="^[0-9]+(\.[0-9][0-9]?)?$" Display="None"> 
                                                                        </asp:RegularExpressionValidator>
                                                                    </div>
                                                                </div>

                                                                <div class="row" id="divpanalty">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            interest</label>
                                                                        <asp:TextBox runat="server" ID="txtIntrest" Style="width: 70%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorint"
                                                                            ControlToValidate="txtIntrest" runat="server"
                                                                            ErrorMessage="Please Insert Number in Interest" ValidationGroup="NoticePopUpValidationGroup"
                                                                            ValidationExpression="^[0-9]+(\.[0-9][0-9]?)?$" Display="None"> 
                                                                        </asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Penalty</label>
                                                                        <asp:TextBox runat="server" ID="txtPenalty" Style="width: 65%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorpenalty"
                                                                            ControlToValidate="txtPenalty" runat="server"
                                                                            ErrorMessage="Please Insert Number in Penalty" ValidationGroup="NoticePopUpValidationGroup"
                                                                            ValidationExpression="^[0-9]+(\.[0-9][0-9]?)?$" Display="None"> 
                                                                        </asp:RegularExpressionValidator>
                                                                    </div>
                                                                </div>

                                                                <div class="row" id="divProvisions">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Provision in Book</label>
                                                                        <asp:TextBox runat="server" ID="txtProvisonbook" Style="width: 70%;" CssClass="form-control" autocomplete="off" />
                                                                    </div>

                                                                   <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Amount paid</label>
                                                                        <asp:TextBox runat="server" ID="txtamountpaid" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <%-- <label style="width: 0.75%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label id="lblAddNewGround" runat="server" style="width: 27%; display: none; float: left; font-size: 13px; color: #333;">
                                                                    Ground(s) for Appeal</label>--%>
                                                                <asp:UpdatePanel ID="upCustomField" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" HeaderText="Parameter">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                                                                        </asp:DropDownListChosen>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="8%" FooterStyle-Width="8%">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtDummyFooter" runat="server" CssClass="form-control" Width="90%" Visible="false"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" ID="txtDummyFooter" CssClass="form-control" Visible="false" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value" CssClass="form-control" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" FooterStyle-Width="15%"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Custom Parameter"
                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Custom Parameter?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add Custom Parameter">
                                                                                            <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add" />
                                                                                        </asp:LinkButton>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>

                                                                        <asp:GridView runat="server" ID="grdCustomField_TaxLitigation" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%" HeaderText="Ground(s) of Appeal">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%" AutoPostBack="true">
                                                                                        </asp:DropDownListChosen>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Tax Demand">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtFieldValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Interest">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxInterestValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("Interest") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtInterestValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Penalty">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxPenaltyValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("Penalty") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtPenaltyValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Total">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxRowTotalValue" runat="server" CssClass="form-control text-right" PlaceHolder="Total" Enabled="false" Text='<%# Eval("Total") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxRowTotalValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Total" Enabled="false" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Settlement" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxSettlement" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement" Text='<%# Eval("SettlementValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxSettlement_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement"></asp:TextBox>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="15%" FooterStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Provision in Books">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxProvisionInbooks" runat="server" CssClass="form-control" PlaceHolder="Provision In Books" Text='<%# Eval("ProvisionInBook") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxProvisionInbooks_Footer" runat="server" CssClass="form-control" PlaceHolder="Provision In Books"></asp:TextBox>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%" FooterStyle-Width="5%"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField_TaxLitigation"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Custom Parameter"
                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Custom Parameter?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_TaxLitigation_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add Custom Parameter" CssClass="text-right mt5">
                                                                                            <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add" class="mt5" />
                                                                                        </asp:LinkButton>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="upCustomField_History" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField_History" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_History_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" HeaderText="Parameter">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="15%" HeaderText="Value"><%--HeaderText="Value"--%>
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("labelValue") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("labelValue") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"><%-- HeaderText="Result"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIsAllowed" runat="server" Text='<%# Eval("IsAllowed") %>' Visible="false"></asp:Label>
                                                                                        <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                            CssClass="form-control" Width="100%" Enabled="false">
                                                                                            <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>


                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>

                                                                        <asp:GridView runat="server" ID="grdCustomField_TaxLitigation_History" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_History_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="25%" HeaderText="Ground(s) of Appeal">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Result" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIsAllowed" runat="server" Text='<%# Eval("IsAllowed") %>' Visible="false"></asp:Label>
                                                                                        <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false"
                                                                                            DisableSearchThreshold="5" CssClass="form-control" Width="100%" Enabled="false">
                                                                                            <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Tax Demand">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("labelValue") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("labelValue") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Interest">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblInterest" runat="server" Text='<%# Eval("Interest") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Interest") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Penalty">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPenalty" runat="server" Text='<%# Eval("Penalty") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Penalty") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Total">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTotalValue" runat="server" Text='<%# Eval("Total") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Total") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" HeaderText="Settlement Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSettlementValue" runat="server" Text='<%# Eval("SettlementValue") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("SettlementValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>

                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--NoticeDetail Panel End-->
                                </div>

                                <div id="divNoticeAssignmentDetails" class="row Dashboard-white-widget">
                                    <!--NoticeAssignment Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view notice assignment details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeAssignmentDetails">
                                                    <a>
                                                        <h2>Notice-User Assignment</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseDivNoticeAssignmentDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivNoticeAssignmentDetails" class="panel-collapse collapse in">
                                                <div class="col-md-12 plr0">
                                                    <asp:ValidationSummary ID="vsNoticeUserAssign" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="NoticeUserAssignmentPopUpValidationGroup" />
                                                    <asp:CustomValidator ID="cvNoticeUserAssignmemt" runat="server" EnableClientScript="False"
                                                        ValidationGroup="NoticeUserAssignmentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                </div>
                                                <div class="panel-body">

                                                    <div class="row" id="divGridUserAssignment" runat="server">
                                                        <div class="col-md-12 text-right">
                                                            <asp:LinkButton runat="server" ID="lnkBtnEditUserAssignment" OnClick="btnEditNoticeControls_Click"
                                                                data-toggle="tooltip" ToolTip="Edit Notice-User Assignment Detail(s)">
                                                                <img src="../../Images/edit_icon_new.png" alt="Edit" title="" />
                                                            </asp:LinkButton>
                                                        </div>

                                                        <asp:GridView runat="server" ID="grdUserAssignment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdUserAssignment_RowCommand"
                                                            OnRowDataBound="grdUserAssignment_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="User Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("UserType") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserType") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="User" ItemStyle-Width="25%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Law Firm" ItemStyle-Width="25%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblLawFirm" runat="server" Text='<%# Eval("LawFirmName")%>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LawFirmName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Eval("RoleName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("RoleName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="upCaseUserAssignment">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")+","+Eval("NoticeCaseInstanceID") %>'
                                                                                    AutoPostBack="true" CommandName="Delete_UserAssignment"
                                                                                    ID="lnkCaseUserAssignment" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <%--<asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteCaseDoc" />--%>
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <EmptyDataTemplate>
                                                                No Records Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>

                                                    <asp:Panel ID="pnlNoticeAssignment" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Internal User</label>
                                                                <asp:ListBox ID="lstBoxPerformer" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="64%"></asp:ListBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage=" Internal User can not be empty."
                                                                    ControlToValidate="lstBoxPerformer" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Lawyer</label>
                                                                        <asp:ListBox ID="lstBoxLawyerUser" CssClass="form-control" runat="server" SelectionMode="Multiple" onchange="FirmLawyerChangeAddButton()" Width="64%"></asp:ListBox>
                                                                        <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                                                            <img id="lnkShowAddNewLawyerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddFirmLawyerPopup()" alt="Add New Lawyer" title="Add New Lawyer" />
                                                                            <asp:LinkButton ID="lnkLawyers" OnClick="lnkLawyers_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="row" style="display: none;">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Reviewer</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlReviewer" DataPlaceHolder="Select Reviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--NoticeAssignment Panel End-->
                                </div>

                                <div class="form-group col-md-12" style="text-align: center;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSaveNotice_Click"
                                        ValidationGroup="NoticePopUpValidationGroup" OnClientClick="validateForm();"></asp:Button>
                                    <asp:Button Text="Clear" runat="server" ID="btnClearNoticeDetail" CssClass="btn btn-primary" OnClick="btnClearNoticeControls_Click" />

                                </div>

                                <asp:UpdatePanel ID="upLinkedNotices" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divLinkedCases" runat="server" class="row Dashboard-white-widget">
                                            <!--Linked Notices Panel Start-->
                                            <div class="col-lg-12 col-md-12">
                                                <div class="panel panel-default">

                                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Linked Notice(s)">
                                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedNoticeDetails">
                                                            <a>
                                                                <h2>Linked Notice(s)</h2>
                                                            </a>
                                                            <div class="panel-actions">
                                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedNoticeDetails">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="collapseDivLinkedNoticeDetails" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-md-12 plr0">
                                                                <asp:ValidationSummary ID="vsLinkedNotices" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="LinkedNoticesValidationGroup" />
                                                                <asp:CustomValidator ID="cvLinkedNotices" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="LinkedNoticesValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                            </div>

                                                            <asp:Panel ID="Panel4" runat="server">
                                                                <div class="row">

                                                                    <asp:GridView runat="server" ID="grdLinkedNotices" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdLinkedNotices_RowCommand"
                                                                        OnPageIndexChanging="grdLinkedNotices_PageIndexChanging" OnRowDataBound="grdLinkedNotices_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Refence No" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblCaseNo" runat="server" Text='<%# Eval("NoticeCaseRefNo") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseRefNo") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Notice Title" ItemStyle-Width="25%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblNoticeCaseTitle" runat="server" Text='<%# Eval("NoticeCaseTitle") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseTitle") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Notice Date" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                        <asp:Label ID="lblOpenDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                            Text='<%# Eval("CaseNoticeDate") != null ? Convert.ToDateTime(Eval("CaseNoticeDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            ToolTip='<%# Eval("CaseNoticeDate") != null ? Convert.ToDateTime(Eval("CaseNoticeDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Court" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblCourt" runat="server" Text='<%# Eval("CourtName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CourtName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Opponent" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblOpponent" runat="server" Text='<%# Eval("PartyName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="UpdateHist">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("LinkedNoticeOrCaseInstanceID")+","+ Eval("NoticeCaseType")%>'
                                                                                                AutoPostBack="true" CommandName="ViewNoticeCasePopup" ID="lnkViewLinkedNotice" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Notice Detail">
                                                                                                <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("NoticeCaseInstanceID")+","+Eval("LinkedNoticeOrCaseInstanceID")+","+ Eval("NoticeCaseType")%>'
                                                                                                ID="lnkBtnDeleteNoticeLinking" AutoPostBack="true" CommandName="DeleteNoticeLinking" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Notice Linking"
                                                                                                OnClientClick="return confirm('Are you sure!! You want to Delete this Linkg with Notice?');">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Case Linked yet
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>

                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--Linked Cases  Panel End-->
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="NoticeDocumentView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">

                                <div class="col-md-12 plr0">
                                    <asp:ValidationSummary ID="vsNoticeDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="NoticeDocumentPopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvNoticeDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="NoticeDocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div id="divNoticeDocuments" class="row Dashboard-white-widget">
                                    <!--Notice Document Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="row" style="margin-top: 20px;">
                                                <%--<div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Notice Document(s)">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDocument">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDocument">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>--%>

                                                <div id="collapseDivNoticeDocument" class="panel-collapse collapse in">
                                                    <div class="panel-body">

                                                        <div class="row col-md-12" id="divNoticeDocumentControls" runat="server" style="display: none;">
                                                            <div class="col-md-3 plr0">
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Upload Document(s)</label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <asp:FileUpload ID="NoticeFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                            </div>

                                                            <div class="col-md-3 text-right plr0">
                                                                <asp:LinkButton ID="lnkNoticeDocumentUpload" runat="server" CssClass="btn btn-primary" OnClick="btnUploadNoticeDoc_Click"
                                                                    Text="<i class='fa fa-upload' aria-hidden='true'></i> Upload Document(s)" ToolTip="Upload Selected Document(s)" data-toggle="tooltip"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 colpadding0">
                                                                <%-- <div class="col-md-3 colpadding0">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDocType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Document Type" class="form-control" Width="90%" />
                                                                </div>--%>
                                                                <%-- <div class="col-md-6 colpadding0">
                                                                    <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                                                        AutoComplete="off" placeholder="Type Document related tags,filename,document typename (Use comma seperated in case of multiple)" />
                                                                </div>--%>


                                                                <div class="col-md-11 colpadding0">
                                                                    <div id="outerDivFileTags" runat="server" class="col-md-11 colpadding0">
                                                                        <div id="leftArrow" class="scroller scroller-left mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                                                                            <span class="arrow-button arrow-button-right">
                                                                                <i class="fa fa-chevron-left color-black"></i>
                                                                            </span>
                                                                        </div>
                                                                        <div id="rightArrow" class="scroller scroller-right mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                                                                            <span runat="server" class="arrow-button arrow-button-left">
                                                                                <i class="fa fa-chevron-right color-black"></i>
                                                                            </span>
                                                                        </div>
                                                                        <div class="divFileTags col-md-10 colpadding0" style="overflow-x: hidden; overflow-y: hidden; width: 92%;">
                                                                            <asp:CheckBoxList ID="lstBoxFileTags" runat="server" CssClass="mt5" RepeatDirection="Horizontal" TextAlign="Left"
                                                                                OnSelectedIndexChanged="lstBoxFileTags_SelectedIndexChanged" AutoPostBack="true">
                                                                            </asp:CheckBoxList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1 colpadding0" style="float: right;">
                                                                    <%-- <asp:LinkButton Text="Apply" CssClass="btn btn-primary" Style="margin-left: 10%"
                                                                        runat="server" ID="lnkApply" OnClick="lnkApply_Click" />--%>
                                                                    <asp:LinkButton CssClass="btn btn-primary" Style="float: right; display: none"
                                                                        runat="server" ID="lnkBindshowDocumentCase" OnClick="lnkBindshowDocumentCase_Click" />
                                                                    <asp:LinkButton Text="Add Document" CssClass="btn btn-primary" Style="float: right;"
                                                                        runat="server" ID="lnkAddNewDoctype" OnClick="lnkAddNewDoctype_Click" data-toggle="tooltip" ToolTip="Add New Document(s)">
                                                                        <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-top: 2%;">
                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="upNoticeDocUploadPopup" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdNoticeDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" PagerSettings-Position="Bottom"
                                                                            PagerStyle-HorizontalAlign="Right" ShowFooter="false"
                                                                            OnRowCommand="grdNoticeDocuments_RowCommand" OnRowDataBound="grdNoticeDocuments_RowDataBound" OnPageIndexChanging="grdNoticeDocuments_OnPageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDocType" runat="server" Text='<%# ShowNoticeDocType((string)Eval("DocType")) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>


                                                                                    <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDocType1" runat="server" Text='<%# Eval("TypeName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Document" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")  %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                   <asp:TemplateField HeaderText="Financial Year" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="18%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                            <asp:Label ID="lblFinancialYear" runat="server" Text='<%# ShowFinancialYear((string)Eval("FinancialYear"))  %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# ShowFinancialYear((string)Eval("FinancialYear"))  %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton data-toggle="tooltip" data-placement="bottom" title="Download Document"
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadNoticeDoc" ID="lnkBtnDownLoadNoticeDoc" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> <%--width="15" height="15"--%>
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="ViewNoticeDocView"
                                                                                                    ID="lblNoticeDocView" runat="server" data-toggle="tooltip" data-placement="bottom" title="View Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="DeleteNoticeDoc"
                                                                                                    OnClientClick="return confirm('Are you certain you want to delete this document?');"
                                                                                                    ID="lnkBtnDeleteNoticeDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Delete Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete"  /> <%--width="15" height="15"--%>
                                                                                                </asp:LinkButton>

                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownLoadNoticeDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteNoticeDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <div id="divDocCount_ImportRenew" class="row col-md-12 pl0" style="display: none;">
                                                                    <div class="col-md-12 text-left">
                                                                        <asp:Label runat="server" ID="lblImportDocCount" Text="" CssClass="control-label"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Notice Document Panel End-->
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="secondTabTask" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="secondTabAccordion" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view task detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this);ShowTaskDiv();" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="CollapsDivTaskFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivTaskLogs" class="panel-collapse">
                                                                <asp:UpdatePanel ID="upNoticeTaskActivity" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" runat="server" id="AddTask">
                                                                            <%--<a class="btn btn-primary" style="float: right;" data-toggle="collapse" data-target="#CollapsDivTaskFirstPanel" aria-expanded="false" aria-controls="CollapsDivTaskFirstPanel" onclick="HidShowTaskDiv()"
                                                                                data-toggle="tooltip" ToolTip="Add New Task"><span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</a>--%>
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LinkButton2" Style="float: right" OnClientClick="HidShowTaskDiv()" data-toggle="tooltip" ToolTip="Add New Task" data-target="#CollapsDivTaskFirstPanel" aria-expanded="false" aria-controls="CollapsDivTaskFirstPanel">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>
                                                                        <%--<i class='fa fa-plus'></i>&nbsp;--%>
                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary8" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                            <asp:CustomValidator ID="CvTaskSaveMsg" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                        </div>

                                                                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                            OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCreated="grdTaskActivity_RowCreated"
                                                                            OnPageIndexChanging="grdTaskActivity_OnPageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%--<img id="imgdiv<%# Eval("TaskID") %>" class="sample" src="/Images/add.png" alt="Show Responses" style="cursor: pointer" />--%>
                                                                                        <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show"
                                                                                            style="cursor: pointer" />
                                                                                        <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                            <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                                Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                OnRowDataBound="grdTaskResponseLog_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                                                                    ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                                <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                </asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:LinkButton
                                                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                        ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                    </asp:LinkButton>

                                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                                        AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                                        ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                                                    </asp:LinkButton>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                                <EmptyDataTemplate>
                                                                                                    No Response Submitted yet.
                                                                                                </EmptyDataTemplate>
                                                                                            </asp:GridView>
                                                                                        </asp:Panel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                            <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' CssClass="btn btn-primary"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to close this Task?');"
                                                                                                        AutoPostBack="true" CommandName="CloseTask" Text="Close Task"
                                                                                                        ID="lnkBtnCloseTask" runat="server">
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="EditTaskDoc"
                                                                                                        ID="lnkBtnEditTaskDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Document" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="TaskReminder"
                                                                                                        ID="lnkBtnTaskReminder" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/send_icon.png")%>' alt="Send Reminder" title="Send Reminder"  /> <%--width="15" height="15" CssClass="btn btn-primary"--%>
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteTask"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Task Detail?');"
                                                                                                        ID="lnkBtnDeleteTask" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete" />
                                                                                                    </asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnCloseTask" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteTask" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnEditTaskDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />

                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="DivTaskCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view Task details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivTaskLogsPanelTwo">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivTaskLogsPanelTwo">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseDivTaskLogsPanelTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="pnlTask" runat="server">
                                                                    <div class="row">
                                                                        <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                            ValidationGroup="NoticePopUpTaskValidationGroup" />
                                                                        <asp:CustomValidator ID="cvNoticePopUpTask" runat="server" EnableClientScript="False"
                                                                            ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                          <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Title</label>
                                                                            <asp:TextBox ID="tbxTaskTitle" runat="server" CssClass="form-control" Width="85.5%" MaxLength="100"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvTaskTitle" ErrorMessage="Provide Task Title"
                                                                                ControlToValidate="tbxTaskTitle" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                            <asp:RegularExpressionValidator ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" ControlToValidate="tbxTaskTitle" ID="revTaskTitle"
                                                                                ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Task Title."></asp:RegularExpressionValidator>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Description</label>
                                                                            <asp:TextBox ID="tbxTaskDesc" runat="server" CssClass="form-control" Style="width: 85.5%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvTaskDesc" ErrorMessage="Provide Task Description"
                                                                                ControlToValidate="tbxTaskDesc" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6 input-group date">
                                                                            <div>
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Due Date</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 70%">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Due Date" class="form-control" ID="tbxTaskDueDate" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvTaskDueDate" ErrorMessage="Provide Due Date"
                                                                                ControlToValidate="tbxTaskDueDate" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Priority</label>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" DataPlaceHolder="Select Task Priority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                CssClass="form-control" Width="70%">
                                                                                <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                            </asp:DropDownListChosen>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Expected Outcome</label>
                                                                            <asp:TextBox ID="tbxExpOutcome" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Assign To</label>
                                                                            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">Internal User</label>
                                                                            <asp:DropDownListChosen ID="ddlTaskUserInternal" CssClass="form-control" runat="server" Width="49%"
                                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlTaskUserInternal_SelectedIndexChanged" DataPlaceHolder="Select User">
                                                                            </asp:DropDownListChosen>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 30%; display: block; float: left; font-size: 13px; color: #333;">Lawyer/External</label>
                                                                            <div style="float: left; width: 64%">
                                                                                <asp:DropDownListChosen runat="server" ID="ddlTaskUserLawyerAndExternal" DataPlaceHolder="Select User" Onchange="ddlTaskUserChange()" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%" />
                                                                            </div>
                                                                            <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                                                                <img id="lnkShowAddNewOwnerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddUserDetailPop()" alt="Add New User" title="Add New User" />
                                                                                <asp:LinkButton ID="lnkBtnAssignUser" OnClick="lnkAddNewUser_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                                </asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                            <asp:TextBox ID="tbxTaskRemark" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Upload Document</label>
                                                                            <div style="width: 100%;">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass"/>
                                                                                </div>
                                                                                <asp:TextBox ID="tbxTaskID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row" runat="server" id="DivTaskEdit" visible="false">
                                                                        <asp:GridView ID="grdTaskEditDoc" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdTaskEditDoc_RowCommand"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="50%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="TaskEditDocUpdate" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadTaskEditDocument"
                                                                                                    ID="lnkDownloadTaskEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="ViewTaskEditDocument"
                                                                                                    ID="lnkViewTaskEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteTaskEditDocument"
                                                                                                    ID="lnkbtnTaskDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="delete Documents" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkbtnTaskDelete" />
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadTaskEditDocument" />
                                                                                                <asp:PostBackTrigger ControlID="lnkViewTaskEditDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Response Submitted yet.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12" style="text-align: center;">
                                                                            <asp:Button Text="Save" runat="server" ID="btnTaskSave" CssClass="btn btn-primary" OnClick="btnTaskSave_Click" OnClientClick="if(!CheckValidation())return false;"
                                                                                ValidationGroup="NoticePopUpTaskValidationGroup"></asp:Button>
                                                                            <asp:Button Text="Clear" runat="server" ID="btnTaskClear" CssClass="btn btn-primary" OnClick="btnClearTask_Click" />
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnTaskSave" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="thirdTabResponse" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="ResponceFirstView" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view notice response detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivResponceFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this); HideFirstResponse();" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivResponceFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="CollapsDivResponceFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivResponceLogs" class="panel-collapse collapse in">
                                                                <asp:UpdatePanel ID="upResponseDocUpload" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" runat="server" id="AddNewResponse">
                                                                            <%-- <a class="btn btn-primary" style="float: right;" data-toggle="collapse" data-target="#CollapsDivTaskFirstPanel" aria-expanded="false" aria-controls="CollapsDivTaskFirstPanel" onclick="HidShowResponseDiv()"
                                                                            data-toggle="tooltip" ToolTip="Add New Response"><span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</a>--%>
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LinkButton1" Style="float: right" OnClientClick="HidShowResponseDiv()" data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Response" data-target="#CollapsDivTaskFirstPanel" aria-expanded="false" aria-controls="CollapsDivTaskFirstPanel">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>
                                                                        <%--<i class='fa fa-plus'></i>&nbsp;--%>
                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary10" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                            <asp:CustomValidator ID="CvResponseSaveMsg" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                        </div>

                                                                        <asp:GridView runat="server" ID="grdResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                            OnPageIndexChanging="grdResponseLog_OnPageIndexChanging" OnRowCommand="grdResponseLog_RowCommand" OnRowDataBound="grdResponseLog_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Notice(Sent/Received)" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="13%" HeaderStyle-Width="13%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblResponseType" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ResponseType").ToString() == "0"? "Sent" : "Received" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Response Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="12%" HeaderStyle-Width="12%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblResponseDate" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ResponseDate") %>'
                                                                                                Text='<%# Eval("ResponseDate") != DBNull.Value ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowNoticeResponseDocCount((long)Eval("NoticeInstanceID"),(long)Eval("ID")) %>'>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upResDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("NoticeInstanceID")%>'
                                                                                                        AutoPostBack="true" CommandName="EditResponseDoc"
                                                                                                        ID="lnkBtnEditResponseDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Document" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton ID="lnkBtnDownLoadResponseDoc" runat="server"
                                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("NoticeInstanceID")%>' CommandName="DownloadResponseDoc">
                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("NoticeInstanceID")%>'
                                                                                                        AutoPostBack="true" CommandName="ViewNoticeResposeDocView" ID="lblNoticeResponseDocView" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteResponse" ID="lnkBtnDeleteResponse" runat="server"
                                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                                    </asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownLoadResponseDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteResponse" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnEditResponseDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="DivResponceCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view Task details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivResponseLogsSecond">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivResponseLogsSecond">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseDivResponseLogsSecond" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:Panel ID="pnlResponse" runat="server">
                                                            <asp:UpdatePanel runat="server" ID="UpdateResponse" UpdateMode="Always">
                                                                <ContentTemplate>

                                                                    <div class="row">
                                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                            ValidationGroup="NoticePopUpResponseValidationGroup" />
                                                                        <asp:CustomValidator ID="cvNoticePopUpResponse" runat="server" EnableClientScript="False"
                                                                            ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                        <asp:Label ID="lblValidResForDoc" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Notice</label>
                                                                            <asp:DropDownList runat="server" ID="ddlNoticeResponseDate" DisableSearchThreshold="5" CssClass="form-control" Width="50%">
                                                                                <asp:ListItem Text="Sent" Value="0"></asp:ListItem>
                                                                                <asp:ListItem Text="Received" Value="1"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <%--<div class="form-group col-md-6" id="DivDueDate" style="display: none">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Reply Due Date</label>
                                                                            <asp:TextBox ID="tbxNoticeDueDate" runat="server" CssClass="form-control" Style="width: 30%; display: initial;"></asp:TextBox>                                                                           
                                                                        </div>--%>
                                                                        <div class="form-group col-md-6 input-group date" id="DivDueDate">
                                                                            <div>
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 24%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Reply Due Date</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 55%">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Due Date" autocomplete="off" class="form-control" ID="tbxNoticeDueDate" />
                                                                            </div>
                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Provide Due Date"
                                                                                    ControlToValidate="tbxNoticeDueDate" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />--%>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <%--<div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label id="lblNoticeDate" style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Responded On</label>
                                                                            <asp:TextBox ID="tbxResponseDate" runat="server" CssClass="form-control" Style="width: 30%; display: initial;"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvRespDate" ErrorMessage="Provide Response/Receipt Date." runat="server"
                                                                                ControlToValidate="tbxResponseDate" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />`
                                                                        </div>--%>
                                                                        <div class="form-group col-md-6 input-group date">
                                                                            <div>
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 24.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Responded On</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 55%">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Response Date" autocomplete="off" class="form-control" ID="tbxResponseDate" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Provide Due Date"
                                                                                ControlToValidate="tbxResponseDate" runat="server" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333;">Delivery Mode</label>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlRespBy" DataPlaceHolder="Select Responded By"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="50%">
                                                                                <asp:ListItem Text="Courier" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Post" Value="2"></asp:ListItem>
                                                                                <asp:ListItem Text="Other" Value="0"></asp:ListItem>
                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvRespBy" ErrorMessage="Select Delivery Mode (i.e. Courier/ Post/ Other)." runat="server"
                                                                                ControlToValidate="ddlRespBy" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                            <asp:TextBox ID="tbxResponseID" autocomplete="off" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="display: block; float: left; font-size: 13px; color: #333;">Courier Company/Post Detail</label>
                                                                            <asp:TextBox ID="tbxRespThrough" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvRespThrough" ErrorMessage="Provide Responded through (i.e. Courier Company/ Post Office Detail)." runat="server"
                                                                                ControlToValidate="tbxRespThrough" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Ref/Tracking No.</label>
                                                                            <asp:TextBox ID="tbxRespRefNo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvRespRefNo" ErrorMessage="Provide Reference/ Courier/ Post Tracking Number, Put 'NA' if not available."
                                                                                runat="server" ControlToValidate="tbxRespRefNo" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">Description</label>
                                                                            <asp:TextBox ID="tbxResponseDesc" runat="server" CssClass="form-control" Style="width: 100%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvRespDesc" ErrorMessage="Provide Response Description"
                                                                                runat="server" ControlToValidate="tbxResponseDesc" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 20%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                            <asp:TextBox ID="tbxResponseRemark" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 20%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Relevant Document(s)</label>
                                                                            <div style="width: 100%;">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:FileUpload ID="fuResponseDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row" runat="server" id="divResposeEditdoc" visible="false">
                                                                        <asp:GridView ID="GrdResponseEditDocument" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="GrdResponseEditDocument_RowCommand"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="50%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="ResponseDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteResposeEditDocument"
                                                                                                    ID="lnkbtnResponseDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="delete Documents" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadResponseEditDocument"
                                                                                                    ID="lnkDownloadResponseEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="ViewResponseEditDocument"
                                                                                                    ID="lnkViewResponseEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkbtnResponseDelete" />
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadResponseEditDocument" />
                                                                                                <asp:PostBackTrigger ControlID="lnkViewResponseEditDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Response Submitted yet.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12" style="text-align: center;">
                                                                            <asp:Button Text="Save" runat="server" ID="btnSaveResponse" CssClass="btn btn-primary" OnClick="btnSaveResponse_Click" OnClientClick="if(!CheckValidationForRes())return false;"
                                                                                ValidationGroup="NoticePopUpResponseValidationGroup"></asp:Button>
                                                                            <asp:Button Text="Clear" runat="server" ID="btnResponseClear" CssClass="btn btn-primary" OnClick="btnClearResponse_Click" />
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnSaveResponse" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="fourthTabStatusPayment" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div class="row Dashboard-white-widget">
                                    <!--Status Log Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View/Edit Notice Status">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivStatusLogs">
                                                    <a>
                                                        <h2>Status</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivStatusLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivStatusLogs" class="panel-collapse collapse in">
                                                <div class="panel-body">

                                                    <div style="margin-bottom: 7px">
                                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                            ValidationGroup="NoticePopUpStatusValidationGroup" />
                                                        <asp:CustomValidator ID="cvNoticeStatus" runat="server" EnableClientScript="False"
                                                            ValidationGroup="NoticePopUpStatusValidationGroup" Display="None" />
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Notice Status</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlNoticeStatus" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="40%" onchange="ddlStatusChange()">
                                                                <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                              <%--  <asp:ListItem Text="In Progress" Value="2"></asp:ListItem>--%>
                                                                <asp:ListItem Text="Close" Value="3"></asp:ListItem>
                                                               <%-- <asp:ListItem Text="Settled" Value="4"></asp:ListItem>--%>
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6 input-group date" id="divClosureDetail">
                                                            <div>
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 25%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Close Date</label>
                                                            </div>

                                                            <div class="col-md-6 input-group date" style="width: 44%">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar color-black"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" placeholder="Close Date" class="form-control" ID="tbxNoticeCloseDate" />
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Provide Close Date"
                                                                ControlToValidate="tbxNoticeCloseDate" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-6" id="divddlResult">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Notice Result</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlNoticeResult" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="40%">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12" id="divClosureRemark">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                            <asp:TextBox ID="tbxCloseRemark" runat="server" CssClass="form-control" Width="85%" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <%--<div class="row float-right">
                                                        <div class="form-group col-md-12">
                                                            <div class="float-left mr10">
                                                                <asp:Button Text="Save" runat="server" ID="btnSaveStatus" CssClass="btn btn-primary" OnClick="btnSaveStatus_Click"
                                                                    ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                            </div>
                                                            <div id="divConvertToCase" class="float-left">
                                                                <asp:Button Text="Save & Convert to Case" runat="server" ID="btnSaveConvertCase" CssClass="btn btn-primary"
                                                                    OnClick="btnSaveConvertToCase_Click" ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                            </div>
                                                        </div>
                                                    </div>--%>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <div class="col-md-3">
                                                                <asp:Button Text="Save" runat="server" ID="btnSaveStatus" CssClass="btn btn-primary pull-right" OnClick="btnSaveStatus_Click"
                                                                    ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                            </div>
                                                            <div id="divConvertToCase">
                                                                <div class="col-md-3">
                                                                    <asp:Button Text="Save & Convert to Case" runat="server" ID="btnSaveConvertCase" CssClass="btn btn-primary pull-left"
                                                                        OnClick="btnSaveConvertToCase_Click" ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div>
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Court Case No.</label>
                                                                </div>
                                                                <asp:TextBox runat="server" placeholder="Court Case No." Width="70%" class="form-control" ID="tbxAppealCaseNo" />
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divCustomField" runat="server" visible="false">
                                                        <div class="form-group col-md-12">
                                                            <%-- <label style="display: block; font-size: 13px; color: #333;">
                                                                    Custom Parameters</label>--%>
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView runat="server" ID="grdCustomField_CaseTransfer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="20%" FooterStyle-Width="20%" HeaderText="Ground(s) of Appeal">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Tax Demand">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="tbxLabelValue" runat="server" Text='<%# Eval("labelValue") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Interest">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="tbxInterestValue" runat="server" Text='<%# Eval("Interest") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Penalty">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="tbxPenaltyValue" runat="server" Text='<%# Eval("Penalty") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Total">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="tbxRowTotalValue" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Provision in Books">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="tbxProvisionInbooks" runat="server" Text='<%# Eval("ProvisionInBook") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" HeaderText="Select">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                        CssClass="form-control" Width="100%" OnSelectedIndexChanged="ddlGroundResult_SelectedIndexChanged" AutoPostBack="true">
                                                                                        <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Settlement Value">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="tbxSettlementValue" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement"
                                                                                        Text='<%# Eval("SettlementValue") %>' Visible="false"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <%--<RowStyle CssClass="clsROWgrid" />--%>
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="form-group col-md-12 text-center">
                                                            <asp:Button Text="Save" runat="server"
                                                                ID="btnSaveCustomFieldCaseTransfer" CssClass="btn btn-primary" OnClick="btnSaveCustomFieldCaseTransfer_Click"></asp:Button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Status Log Panel End-->
                                </div>

                                <div class="row Dashboard-white-widget">
                                    <!--Payment Log Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Payment Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivPaymentLog">
                                                    <a>
                                                        <h2>Payment Log</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivPaymentLog">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivPaymentLog" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div style="margin-bottom: 7px">
                                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                            ValidationGroup="NoticePopUpPaymentLogValidationGroup" EnableClientScript="true" />
                                                        <asp:CustomValidator ID="cvNoticePayment" runat="server" EnableClientScript="true"
                                                            ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <asp:UpdatePanel ID="upNoticePayment" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdNoticePayment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                    OnRowCommand="grdNoticePayment_RowCommand" OnRowDataBound="grdNoticePayment_RowDataBound" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right"
                                                                    OnPageIndexChanging="grdNoticePayment_OnPageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice No." ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblInvoiceNo" runat="server" Text='<%# Eval("InvoiceNo")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxInvoiceNo" runat="server" class="form-control" PlaceHolder="InvoiceNo"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvInvoice" ErrorMessage="Provide InvoiceNo."
                                                                                    ControlToValidate="tbxInvoiceNo" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Lawyer" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLawyer" runat="server" Text='<%# Eval("Lawyer") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlLawyer" DataPlaceHolder="Lawyer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%">
                                                                                </asp:DropDownListChosen>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Date" ItemStyle-Width="16%" FooterStyle-Width="16%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate") != null ? Convert.ToDateTime(Eval("PaymentDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>

                                                                                <div class="col-md-4 plr0 input-group date" style="width: 100%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="Payment Date" class="form-control" ID="tbxPaymentDate" Style="width: 100%;" />
                                                                                </div>

                                                                                <%--<asp:TextBox ID="tbxPaymentDate" runat="server" class="form-control" PlaceHolder="Payment Date" Style="display: initial; width: 80%;"></asp:TextBox>--%>
                                                                                <asp:RequiredFieldValidator ID="rfvPaymentDate" ErrorMessage="Provide Payment Date." runat="server"
                                                                                    ControlToValidate="tbxPaymentDate" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Type" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PaymentType") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlPaymentType" DataPlaceHolder="Payment Type" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%">
                                                                                </asp:DropDownListChosen>
                                                                                <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Select Payment Type."
                                                                                    ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderText="Amount" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>' Style="text-align: right;"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxAmount" runat="server" class="form-control" PlaceHolder="Amount"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvAmount" ErrorMessage="Provide Payment Amount."
                                                                                    ControlToValidate="tbxAmount" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" HeaderText="Amount Paid" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAmountPaid" runat="server" Text='<%# Eval("AmountPaid") %>' Style="text-align: right;"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxAmountPaid" runat="server" class="form-control" PlaceHolder="AmountPaid"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvAmountPaid" ErrorMessage="Provide Payment AmountPaid."
                                                                                    ControlToValidate="tbxAmountPaid" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="25%" FooterStyle-Width="25%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxPaymentRemark" runat="server" class="form-control" PlaceHolder="Remark"></asp:TextBox>
                                                                                 <asp:TextBox ID="tbxPaymentID" runat="server" CssClass="form-control" Style="display: none"></asp:TextBox>
                                                                      
                                                                                <asp:RequiredFieldValidator ID="rfvPaymentRemark" ErrorMessage="Provide Payment Remark."
                                                                                    ControlToValidate="tbxPaymentRemark" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%"
                                                                            FooterStyle-Width="10%" FooterStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                AutoPostBack="true" CommandName="EditPayment"
                                                                                                ID="lnkBtnEditPayment" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Document" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                AutoPostBack="true" CommandName="DeletePayment"
                                                                                                OnClientClick="return confirm('Are you sure!! You want to Delete this Payment Detail?');"
                                                                                                ID="lnkBtnDeletePayment" runat="server">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" 
                                                                                                title="Delete" />
                                                                                            </asp:LinkButton>
                                                                                            <%--"~/Images/delete_icon.png"--%>
                                                                                        </div>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDeletePayment" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button CssClass="btn btn-primary" ID="btnPaymentSave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="NoticePopUpPaymentLogValidationGroup" OnClick="btnPaymentSave_Click"></asp:Button>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Payment Log Panel End-->
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="RatingView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="FifthTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="row" style="margin-top: 20px;">
                                                    <div id="collapseDivLawRating" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div style="margin-bottom: 7px">
                                                                    <asp:ValidationSummary ID="ValidationSummary6" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="ValidLayerRating" />
                                                                    <asp:CustomValidator ID="CvValidLaywRating" runat="server" EnableClientScript="False"
                                                                        ValidationGroup="ValidLayerRating" Display="None" />
                                                                </div>
                                                            </div>
                                                            <asp:Panel ID="pnlLawRating" runat="server">

                                                                <div class="form-group col-md-12">
                                                                    <div class="form-group col-md-12">
                                                                        <div class="col-md-6">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlLayerType" AutoPostBack="true" DataPlaceHolder="Select Lawyer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                CssClass="form-control" Width="50%" OnSelectedIndexChanged="ddlLayerType_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvLayerType" ErrorMessage="Select Lawyer"
                                                                                ControlToValidate="ddlLayerType" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" Style="float: right" OnClick="btnAddPromotor_Click" data-toggle="tooltip" ToolTip="Add New Criteria">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group col-md-12" style="margin-top: 40px;">
                                                                        <asp:GridView runat="server" ID="grdLawyerRating" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                            OnRowDataBound="grdLawyerRating_RowDataBound"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdLawyerRating_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Criteria" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCriteria" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Lawyer Rating" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <%--<asp:TextBox ID="tbxLawyerRating" PlaceHolder="Enter Rating" autocomplete="off" Text='<%# Eval("Rating")%>' runat="server" CssClass="form-control" Style="width: 40%; display: initial; background-color: #fff; cursor: pointer;"></asp:TextBox>--%>
                                                                                        <ajaxToolkit:Rating ID="LawyerRating" runat="server" AutoPostBack="true" StarCssClass="blankstar"
                                                                                            WaitingStarCssClass="waitingstar" FilledStarCssClass="shiningstar" EmptyStarCssClass="blankstar"
                                                                                            CurrentRating='<%# ShowLawyerRating((decimal)Eval("Rating")) %>'>
                                                                                        </ajaxToolkit:Rating>
                                                                                        <asp:Label ID="lblratingval" runat="server" Text='<%# Eval("Rating")%>' Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCriteriaID" runat="server" Text='<%# Eval("criteriaID")%>'></asp:Label>
                                                                                        <%--<asp:Label ID="lblType" runat="server" Text='<%# Eval("Type")%>'></asp:Label>
                                                                                            <asp:Label ID="lblNoticeInstanceID" runat="server" Text='<%# Eval("NoticeInstanceID")%>'></asp:Label>--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group col-md-12" style="text-align: center; margin: auto; width: 30%">
                                                                        <div style="text-align: center">
                                                                            <asp:Button Text="Save" runat="server" ID="btnSaveLawRating" CssClass="btn btn-primary" OnClick="btnSaveLawRating_Click"
                                                                                ValidationGroup="ValidLayerRating"></asp:Button>
                                                                            <asp:LinkButton Style="display: none" runat="server" ID="lnkBtnRebindRating" OnClick="lnkBtnRebindRating_Click" CausesValidation="true" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="AuditLogView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="SixthTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="row" style="margin-top: 20px;">
                                                    <%--<div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Audit Log">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#SixthTabAccordion" href="#collapseDivAuditLog">

                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#SixthTabAccordion" href="#collapseDivAuditLog">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>--%>

                                                    <div id="collapseDivAuditLog" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:UpdatePanel ID="upCaseAuditLog" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="row">
                                                                        <div style="margin-bottom: 7px">
                                                                            <asp:ValidationSummary ID="ValidationSummary11" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="validationCaseAuditLog" />
                                                                            <asp:CustomValidator ID="cvCaseAuditLog" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="validationCaseAuditLog" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <asp:Panel ID="Panel3" runat="server">
                                                                        <div class="form-group col-md-12">
                                                                            <asp:GridView runat="server" ID="gvCaseAuditLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="gvCaseAuditLog_PageIndexChanging">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="40%" HeaderStyle-Width="40%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByUser")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created On" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Records Found
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>

            <%--Compliance Document  Popup--%>
            <div class="modal fade" id="divComplianceDocumentShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 30px; margin-top: -9px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 220px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Compliance Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="ClosePopComplianceDocumentDetialPage();">&times;</button>
                        </div>

                        <div class="modal-body">
                            <iframe id="IframeComplianceDocument" src="about:blank" width="95%" height="auto" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Department Popup--%>
            <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 220px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="150px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Act Popup--%>
            <div class="modal fade" id="AddActPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Act</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAct" frameborder="0" runat="server" width="100%" height="150px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Party Details--%>
            <div class="modal fade" id="AddPartyPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Opponent</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Legal Notice category--%>
            <div class="modal fade" id="AddCategoryType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 230px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Notice Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCategoryType" frameborder="0" runat="server" width="100%" height="230px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add User--%>
            <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit User</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Document Viewer--%>
            <div>
                <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <%--<label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>--%>
                                <%--<label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                    View Documents</label>--%>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Mail Document Popup--%>
            <div class="modal fade" id="divOpenSendMailPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 280px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Send Mail with Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary7" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="CasePopUpStatusValidationGroup" />
                                        <asp:CustomValidator ID="CvDocListWithMail" runat="server" EnableClientScript="False"
                                            ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">To</label>
                                        <asp:TextBox ID="tbxMailTo" runat="server" CssClass="form-control" Width="85%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="From name can not be empty."
                                            ControlToValidate="tbxMailTo" runat="server" ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="CasePopUpStatusValidationGroup" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxMailTo" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">Message</label>
                                        <asp:TextBox ID="tbxMailMsg" runat="server" CssClass="form-control" Width="85%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Message can not be empty."
                                            ControlToValidate="tbxMailMsg" runat="server" ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                    </div>
                                    <asp:GridView runat="server" ID="grdShowDocumentList" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                        OnRowCommand="grdShowDocumentList_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdShowDocumentList_RowDataBound"
                                        OnPageIndexChanging="grdShowDocumentList_PageIndexChanging" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAllDocument" runat="server" OnCheckedChanged="chkAllDocument_CheckedChanged" AutoPostBack="true" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDocument" runat="server" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Document Uploaded
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                    <div class="row">
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; width: 15%; float: left;"></asp:Label>
                                        <asp:Button Text="Send" runat="server" ID="btnSendDocumentMail" CssClass="btn btn-primary" OnClick="btnSendDocumentMail_Click"
                                            Style="float: left;" ValidationGroup="CasePopUpStatusValidationGroup" Visible="false"></asp:Button>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <%--Link Case(s) Popup--%>
            <div class="modal fade" id="divLinkNoticePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Link Notice(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upLinkNotices" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="vsLinkCase" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="VGLinkNotices" />
                                        <asp:CustomValidator ID="cvLinkNotice" runat="server" EnableClientScript="False"
                                            ValidationGroup="VGLinkNotices" Display="None" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 colpadding0">
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right: 10px;">
                                                <asp:DropDownListChosen runat="server" ID="ddlLinkNoticeStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    DataPlaceHolder="Select Status" class="form-control" Width="100%">
                                                    <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div class="col-md-6 colpadding0" style="margin-top: 5px; width: 49.8%; margin-right: 5px;">
                                                <asp:TextBox runat="server" ID="tbxtypeTofilter" Width="100%" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 8%; float: right;">
                                            <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkLinkNoticeFilter" Style="float: right; width: 85%" OnClick="lnkLinkNoticeFilter_Click" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom" />
                                        </div>
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">Select One or More Notice(s) and Click on Save to Link Notice(s)</label>
                                    </div>
                                    <div class="row" style="margin: 10px; max-height: 350px; overflow-y: auto;">
                                        <asp:GridView runat="server" ID="grdNoticeList_LinkNotice" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="NoticeInstanceID">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNoticeInstanceID" runat="server" Text='<%# Eval("NoticeInstanceID") %>' Visible="false"></asp:Label>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeaderLinkCases" runat="server" onclick="javascript:checkAll(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRowLinkCases" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTypeName") %>' ToolTip='<%# Eval("NoticeTypeName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Reference No." ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RefNo") %>' ToolTip='<%# Eval("RefNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Notice" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTitle") %>' ToolTip='<%# Eval("NoticeTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="17%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />
                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="divLinkCaseSaveCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalCaseSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLinkNotice" CssClass="btn btn-primary" OnClick="btnSaveLinkNotice_Click" ValidationGroup="VGLinkNotices"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Link Case(s) Popup--%>

            <%--Notice/Case History Popup--%>
            <div class="modal fade" id="divNoticeCaseHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="historyPopUpHeader">
                                Case History</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameCaseHistory" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Notice/Case History Popup--%>

            <%--Notice/Case Opposition Lawyer and Lawyer--%>
            <div class="modal fade" id="AddOpposiLawyerPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 90%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Oppostitionlawyermodel">
                                Oppostion Lawyer Add/Edit</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameOppoLawyer" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Notice/Case Opposition Lawyer and Lawyer Popup--%>

            <div class="modal fade" id="AddLawFirmModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Lawfirmmodel">
                                Add/Edit Law Firm</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFLawFirm" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Notice/Case Document List--%>
            <div class="modal fade" id="AddDocumentList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Documentmodel">
                                Add Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameManageDocument" frameborder="0" runat="server" style="width:100%;height:320px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Notice/Case Document ListPopup--%>

            <%--Add Criteria --%>
            <div class="modal fade" id="AddLayerRatingCriteriaShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog pt10 pb10">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label class="modal-header-custom">
                                Criteria</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="CloseLayerRatingCriteriaPage();">&times;</button>
                        </div>

                        <div class="modal-body">
                            <iframe id="IframeLayerRatingCriteria" src="about:blank" width="95%" height="auto" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--Add Criteria End --%>
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            ddlNoticeCategoryChange();
        });

        function OpenNoticeLinkingPopup() {
            $('#divLinkNoticePopup').modal('show');
            unCheckAll();
        }

        function ddlNoticeCategoryChange() {
            var selectedCaseCatID = $('#ddlNoticeCategory :selected').text();

            if (selectedCaseCatID != null) {
                if ((selectedCaseCatID == "Direct Tax Litigation" || selectedCaseCatID == "Indirect Tax Litigation") && ("<%=CustomerID.Equals(76)%>") == "True") {

                    $('#emmamiusers').show();
                    $("#lnkAddNewNoticeCategoryModal").hide();

                }

                else if ((selectedCaseCatID != "Direct Tax Litigation") || (selectedCaseCatID != "Indirect Tax Litigation")) {
                    if (selectedCaseCatID == "Add New") {
                        $("#lnkAddNewNoticeCategoryModal").show();
                        $('#emmamiusers').hide();
                    }
                    else {
                        $("#lnkAddNewNoticeCategoryModal").hide();
                        $('#emmamiusers').hide();
                    }
                }
            }
          
        }
            function ddlDepartmentChange() {
                var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function ddlStatusChange() {
            var selectedStatusID = $("#<%=ddlNoticeStatus.ClientID %>").val();
            if (selectedStatusID != null) {
                if (selectedStatusID == 3) {
                    $("#divClosureDetail").show();
                    $("#divClosureRemark").show();
                    $("#divConvertToCase").show();
                    $("#divddlResult").show();
                }
                else {
                    $("#divClosureDetail").hide();
                    $("#divClosureRemark").hide();
                    $("#divConvertToCase").hide();
                    $("#divddlResult").hide();
                }
            }
        }

        function rblImpactChange() {
            var radioButtonlist = document.getElementsByName("<%=rblPotentialImpact.ClientID%>");
            for (var x = 0; x < radioButtonlist.length; x++) {
                if (radioButtonlist[x].checked) {
                    var selectedImpactID = radioButtonlist[x].value;
                    if (selectedImpactID != null && selectedImpactID != '') {
                        if (selectedImpactID == 'M') {
                            $("#divMonetory").show();
                            $("#divNonMonetory").hide();
                        }
                        else if (selectedImpactID == 'N') {
                            $("#divMonetory").hide();
                            $("#divNonMonetory").show();
                        }
                        else if (selectedImpactID == 'B') {
                            $("#divMonetory").show();
                            $("#divNonMonetory").show();
                        }
                    }
                    x = radioButtonlist.length;
                }
            }
        }

        function ddlTaskUserChange() {
            var selectedTaskUserID = $("#<%=ddlTaskUserLawyerAndExternal.ClientID %>").val();
            if (selectedTaskUserID != null) {
                if (selectedTaskUserID == 0) {
                    $("#lnkShowAddNewOwnerModal").show();
                }
                else {
                    $("#lnkShowAddNewOwnerModal>").hide();
                }
            }

            $('#ddlTaskUserInternal option:eq(0)').attr('selected', 'selected');

        }

        function validateForm()
        {
           
            if ($('#<%=tbxRefNo.ClientID %>').val() == '') {
                    $('#<%=tbxRefNo.ClientID %>').css('border-color', 'red');
                }
                else {
                    $('#tbxRefNo').css('border-color', '');
                }
            }

            function ClosePopDepartment() {
                $('#AddDepartmentPopUp').modal('hide');
                document.getElementById('<%= lnkBtnDept.ClientID %>').click();
            }

            function CloseUploadDocumentPopup() {
                $('#AddDocumentList').modal('hide');
                document.getElementById('<%= lnkBindshowDocumentCase.ClientID %>').click();
        }

        function CloseFirmLawyerPop() {
            $('#AddOpposiLawyerPopUp').modal('hide');
            document.getElementById('<%= lnkLawyers.ClientID %>').click();
        }

        function ReloadWindow() {
            $('#Newaddremider').modal('hide');
        }

        function CloseOppoLawyerPop() {
            $('#AddOpposiLawyerPopUp').modal('hide');
            document.getElementById('<%= lnkOpposiLawyer.ClientID %>').click();
        }

        function CloseLawFirmModal() {

            $('#AddLawFirmModelPopup').modal('hide');
            document.getElementById('<%= lnkbindLawfirmp.ClientID %>').click();
        }

        function ClosePopLawFirmPopup() {
            $('#AddActPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAct.ClientID %>').click();
        }

        function ClosePopAct() {
            $('#AddActPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAct.ClientID %>').click();
        }

        function ClosePopParty() {
            $('#AddPartyPopUp').modal('hide');
            document.getElementById('<%= lnkBtnParty.ClientID %>').click();
        }

        function CloseCaseTypePopUp() {
            $('#AddCategoryType').modal('hide');
            document.getElementById('<%= lnkBtnCategory.ClientID %>').click();
        }

        function CloseCriteriaPopUp() {

            $('#AddLayerRatingCriteriaShowDialog').modal('hide');
            document.getElementById('<%= lnkBtnRebindRating.ClientID %>').click();
        }

        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAssignUser.ClientID %>').click();
        }

        function OpenCaseNoticeHistoryPopup(NoticeCaseInstanceID, NoticeCaseType, HistoryFlag, Type) {
            if (Type == 'H')
                $('#historyPopUpHeader').text('Case History');
            else if (Type == 'L')
                $('#historyPopUpHeader').text('Linked Notice Details');

            $('#divNoticeCaseHistoryPopup').modal('show');
            if (NoticeCaseType == '1') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
            }
            else if (NoticeCaseType == '2') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
            }
        }

        $("#<%=tbxBranch.ClientID %>").unbind('click');

        $("#<%=tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });

            function checkAll(chkHeader) {

                var selectedRowCount = 0;
                var grid = document.getElementById("<%=grdNoticeList_LinkNotice.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkCase = document.getElementById("<%=btnSaveLinkNotice.ClientID %>");
                var lblTotalCaseSelected = document.getElementById("<%=lblTotalCaseSelected.ClientID %>");
                if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalCaseSelected.innerHTML = selectedRowCount + " Selected";
                        divLinkCaseSaveCount.style.display = "block";
                    }
                    else {
                        lblTotalCaseSelected.innerHTML = "";;
                        divLinkCaseSaveCount.style.display = "none";
                    }
                }
            }
        }



        function checkUncheckRow(clickedCheckBoxObj) {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdNoticeList_LinkNotice.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkCase = document.getElementById("<%=btnSaveLinkNotice.ClientID %>");
            var lblTotalCaseSelected = document.getElementById("<%=lblTotalCaseSelected.ClientID %>");
            if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalCaseSelected.innerHTML = selectedRowCount + " Selected";
                    divLinkCaseSaveCount.style.display = "block";
                }
                else {
                    lblTotalCaseSelected.innerHTML = "";;
                    divLinkCaseSaveCount.style.display = "none";
                }
            }
        }

        function unCheckAll() {
            var grid = document.getElementById("<%=grdNoticeList_LinkNotice.ClientID %>");
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function applyCSSToNoticeDate() {
            $('#<%= txtNoticeDate.ClientID %>').removeClass();
                $('#<%= txtNoticeDate.ClientID %>').addClass('form-control');
            }

    </script>

    <script type="text/javascript">
        function checkAll_Import(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdNoticeDocuments.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
                if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                        divDocCount_ImportRenew.style.display = "block";
                    }
                    else {
                        lblTotalSelected.innerHTML = "";;
                        divDocCount_ImportRenew.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow_Import(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%= grdNoticeDocuments.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
            if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divDocCount_ImportRenew.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divDocCount_ImportRenew.style.display = "none";
                }
            }
        }
        function fcheckcontract(obj) {
            var span = $(obj).parent('span.label.label - info');
            $(span).addClass('label-info-selected')
        }

        $(document).ready(function () {
            if ($("#<%=tbxBranch.ClientID %>") != null) {
                $("#<%=tbxBranch.ClientID %>").unbind('click');

                $("#<%=tbxBranch.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>
</body>
</html>
