﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="myReminders.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.myReminders" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Reminders');
        });

        function ShowReminderDialog(InstanceID) {
            $('.modal-dialog').css('width', '40%');
            $('#divShowReminderDialog').modal('show');
            $('#showReminderDetail').attr('width', '100%');
            $('#showReminderDetail').attr('height', '400px');
            $('#showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=" + InstanceID);
        };

        function CloseMyReminderPopup() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }
    </script>
    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="NoticePageValidationGroup" />
                <asp:CustomValidator ID="cvErrorPage" runat="server" EnableClientScript="False"
                    ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-left: 1%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                        DataPlaceHolder="Select Type" class="form-control" Width="90%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Case" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Notice" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Task" Value="3"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlTitle" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select" class="form-control" Width="90%">
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px; text-align: right; width: 58%;">
                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; text-align: right; width: 9%;">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" ToolTip="Apply"
                        ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click" />
                    <%--<i class='fa fa-check'></i> --%>
                </div>

                <div class="col-md-3 colpadding0" style="margin-top: 5px; text-align: right; width: 8%;">
                    <asp:LinkButton CssClass="btn btn-primary" runat="server" data-toggle="tooltip" ToolTip="Add New Reminders"
                        ID="btnAddCase" OnClick="btnAddReminderClick"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    <%--<i class='fa fa-plus'></i>--%>
                </div>
            </div>

            <div class="clearfix"></div>

            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdReminders" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ReminderID"
                    OnRowCommand="grdReminders_RowCommand" OnRowDataBound="grdReminders_RowDataBound" OnSorting="grdReminders_Sorting" OnRowCreated="grdReminders_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" SortExpression="Type">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"
                                        Text='<%# ShowReminderType(Eval("Type").ToString()) %>' ToolTip='<%# ShowReminderType(Eval("Type").ToString()) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Reminder" ItemStyle-Width="20%" SortExpression="ReminderTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ReminderTitle") %>' ToolTip='<%# Eval("ReminderTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="20%" SortExpression="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%" SortExpression="Remark">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Remark") %>' ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Remind On" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"
                                        Text='<%# Eval("RemindOn") != null ? Convert.ToDateTime(Eval("RemindOn")).ToString("dd-MM-yyyy") : "" %>'
                                        ToolTip='<%# Eval("RemindOn") != null ? Convert.ToDateTime(Eval("RemindOn")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" ID="lblStatus" data-toggle="tooltip" data-placement="bottom"
                                        Text='<%# Eval("ReminderStatus") != null ? Convert.ToString(Eval("ReminderStatus"))=="0"? "Pending" : "Sent":"" %>'
                                        ToolTip='<%# Eval("ReminderStatus") != null ? Convert.ToString(Eval("ReminderStatus"))=="0"? "Pending" : "Sent":"" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditReminder" runat="server" ToolTip="Edit Reminder" data-toggle="tooltip" OnClick="lnkEditReminder_Click"
                                    CommandArgument='<%# Eval("ReminderID") %>'>                                   
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit"/></asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteReminder" runat="server" CommandName="DELETE_Reminder" ToolTip="Delete Reminder" data-toggle="tooltip" Visible="true"
                                    CommandArgument='<%# Eval("ReminderID") %>' OnClientClick="return confirm('Are you certain you want to Delete this Reminder?');">
                                     <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete"/></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe id="showReminderDetail" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
