﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="ddd.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.ddd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary11" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="CasePopUpStatusValidationGroup" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                            ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                    </div>
                                    <div id="divCaseDetailsHistoryPop" class="row Dashboard-white-widget">
                                    <!--CaseDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Case Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetailsHistoryPop">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetailsHistoryPop">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseDetailsHistoryPop" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:Panel ID="pnlCaseHistoryPop" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Type</label>
                                                                <asp:RadioButtonList ID="rbCaseInOutTypeHistoryPop" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Defendant" Value="I" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Plaintiff " Value="O"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Open Date</label>
                                                                <asp:TextBox ID="txtCaseDateHistoryPop" autocomplete="off" runat="server" CssClass="form-control" MaxLength="100" Style="width: 50%; display: initial; background-color: #fff; cursor: pointer;"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Case Date can not be empty."
                                                                    ControlToValidate="txtCaseDateHistoryPop" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Court Case No.</label>
                                                                <asp:TextBox runat="server" ID="tbxRefNoHistoryPop" Style="width: 60%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Court Case No. can not be empty."
                                                                    ControlToValidate="tbxRefNoHistoryPop" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Internal Case No.</label>
                                                                <asp:TextBox runat="server" ID="tbxInternalCaseNoHistoryPop" Style="width: 60%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Internal Case No. can not be empty."
                                                                    ControlToValidate="tbxInternalCaseNo" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />--%>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13.2%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Title</label>
                                                                <asp:TextBox runat="server" ID="tbxTitleHistoryPop" TextMode="MultiLine" Style="width: 80%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Title can not be empty."
                                                                    ControlToValidate="tbxTitleHistoryPop" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Act</label>
                                                                <div style="float: left; width: 60%">
                                                                    <%--<asp:DropDownListChosen runat="server" ID="" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" DataPlaceHolder="Select Act" Width="100%" onchange="ddlActChange()" />--%>
                                                                    <asp:ListBox ID="ddlActHistoryPop" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlActChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Please Select Act or Select 'Not Applicable'"
                                                                        ControlToValidate="ddlActHistoryPop" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewActModalHiPopup" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddActPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                                                    <asp:LinkButton ID="lnkBtnActHistoryPop"  Style="float: right; display: none;" Width="100%" runat="server"> <%--OnClick="lnkBtnAct_Click"--%>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Under Section</label>
                                                                <asp:TextBox runat="server" ID="tbxSectionHistoryPop" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Type</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlCaseCategoryHPopup" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select Case Type" onchange="ddlCaseCategoryChange()"><%--OnSelectedIndexChanged="ddlCaseCategory_SelectedIndexChanged"--%>
                                                                    </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Please Select Case Type"
                                                                        ControlToValidate="ddlCaseCategoryHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCaseCategoryModalHPopup" style="float: right; display: none;" src="../../Images/add_icon_new.png" alt="Add New Opponent" title="Add New Opponent" /><%--onclick="OpenCategoryTypePopup()"--%>
                                                                    <asp:LinkButton ID="lnkBtnCategoryHPopup" Style="float: right; display: none;" Width="100%" runat="server"> <%--OnClick="lnkBtnCategory_Click"--%>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6"></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opponent</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:ListBox ID="ddlPartyHPopup" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlPartyChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Opponent"
                                                                        ControlToValidate="ddlPartyHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewPartyModalHPopup" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenPartyDetailsPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                                                    <asp:LinkButton ID="lnkBtnPartyHPopup" Style="float: right; display: none;" Width="100%" runat="server"> <%--OnClick="lnkBtnParty_Click"--%>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opposition Lawyer</label>
                                                                <asp:ListBox ID="lstBoxOppositionLawyerHPopup" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%"></asp:ListBox>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Court</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCourtHPopup" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select Court" onchange="ddlCourtChange()">
                                                                    </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Court"
                                                                        ControlToValidate="ddlCourtHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCourtModalHPopup" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCourtPopup()" alt="Add New Court" title="Add New Court" />
                                                                    <asp:LinkButton ID="lblCourtHPopup" Style="float: right; display: none;" Width="100%" runat="server"> <%--OnClick="lblCourt_Click"--%>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Judge</label>
                                                                <asp:TextBox runat="server" ID="tbxJudgeHPopup" Style="width: 60%" CssClass="form-control" autocomplete="off" />
                                                                <%--<asp:RequiredFieldValidator ID="rfvJudge" ErrorMessage="Provide Judge Detail."
                                                                    ControlToValidate="tbxJudge" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />--%>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescriptionHPopup" TextMode="MultiLine" Style="width: 80%; min-height: 115px;" CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Description can not be empty."
                                                                    ControlToValidate="tbxDescriptionHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Entity/Location</label>

                                                                <asp:TextBox runat="server" ID="tbxBranchHPopup" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 70%; background-color: #fff; cursor: pointer;"
                                                                    autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" ReadOnly="true" />
                                                                <%--onclick="txtclick()"--%>
                                                                <div style="margin-left: 28%; position: absolute; z-index: 10; width: 70%;" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="TreeView1" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                    </asp:TreeView><%--OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"--%>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranchHPopup" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Department</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartmentHPopup" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" class="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Please Select Department"
                                                                        ControlToValidate="ddlDepartmentHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewDepartmentModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                                                    <asp:LinkButton ID="lnkBtnDeptHPopup" Style="float: right; display: none;" Width="100%" runat="server"> <%--OnClick="lnkBtnDept_Click"--%>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Owner</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlOwnerHPopup" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ErrorMessage="Please Select Owner"
                                                                    ControlToValidate="ddlOwnerHPopup" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Winning Prospect</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseRiskHPopup" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%">
                                                                    <%-- <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Claimed Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxClaimedAmtHPopup" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="tbxClaimedAmtHPopup" ErrorMessage="Only Numbers in Claimed Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Probable Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxProbableAmtHPopup" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="tbxProbableAmtHPopup" ErrorMessage="Only Numbers in Probable Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Potential Impact</label>
                                                                <asp:RadioButtonList ID="rblPotentialImpactHPopup" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Monetary" Value="M" Selected="True" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Non-Monetary" Value="N" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Both" Value="B" onclick="rblImpactChange()"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6" id="divMonetory">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxMonetoryHPopup" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divNonMonetory">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Non-Monetory</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetoryHPopup" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Years</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetoryYearsHPopup" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Upload Documents</label>
                                                                <asp:FileUpload ID="CaseFileUploadHPopup" runat="server" AllowMultiple="true" Style="color: #8e8e93;" />
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="display: block; font-size: 13px; color: #333;">
                                                                    Custom Parameters</label>
                                                                <asp:GridView runat="server" ID="grdCustomeFieldHPopup" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                    <%--PageSize="5"OnPageIndexChanging="grdCustomeField_PageIndexChanging"--%>
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Lable ID" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Parameter Name" ItemStyle-Width="40%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Parameter Value" ItemStyle-Width="50%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" Text='<%# Eval("labelValue") %>' Width="70%"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>


                                                        <div class="row" style="margin-top: 2%;">
                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCaseDocumentsHPopup" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                            <%--<ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DownloadCaseDoc"
                                                                                                    ID="lnkBtnDownLoadCaseDoc" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download" /> 
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                    AutoPostBack="true" CommandName="ViewCaseOrder"
                                                                                                    ID="lnkBtnViewDocCase" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="DeleteCaseDoc"
                                                                                                    OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                                    ID="lnkBtnDeleteCaseDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete"  /> 
                                                                                                </asp:LinkButton>

                                                                                            </ContentTemplate>--%>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteCaseDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>

                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--CaseDetail Panel End-->
                                </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
</asp:Content>
