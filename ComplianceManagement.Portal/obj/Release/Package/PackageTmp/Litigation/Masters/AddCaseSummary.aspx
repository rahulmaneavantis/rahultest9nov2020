﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCaseSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddCaseSummary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <style>
        .mt30 {
            margin-top: 3%;
        }

        .ml2 {
            margin-left: 2%;
        }
    </style>
    <script type="text/javascript">
        function NumberOnly() {
            debugger;
            var AsciiValue = event.keyCode
            if ((AsciiValue >= 48 && AsciiValue <= 57))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
        function ShowCaseSummaryAddbtn() {
            var selectedParticularsID = $("#ddlParticulars").find("option:selected").text();
            if (selectedParticularsID != null) {
                if (selectedParticularsID == "Add New") {
                    $("#lnkShowAddNewParticulars").show();
                }
                else {
                    $("#lnkShowAddNewParticulars").hide();
                }
            }
        }

        function OpenPerticularsPopupModel() {
            $('#AddPerticularsModelPopup').modal('show');
            $('#IFPerticulars').attr('src', "../../Litigation/Masters/AddPerticulars.aspx");
        }
        function CloseMe() {
            window.parent.CloseCaseSummary();
        }

        function CloseMyADDPerticulars() {
            $('#AddPerticularsModelPopup').modal('hide');
            RefreshParent();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }

    </script>
</head>
<body style="background-color: white">
    <form id="formAddSpeciman" runat="server">
        <div>
            <div>
                <asp:ScriptManager ID="LitigationAddSpeciman" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="upAddSpeciman" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div class="row form-group">
                            <asp:ValidationSummary ID="vsAddEditCaseSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="CaseSummaryValidationGroup" />
                            <asp:CustomValidator ID="cvAddEditCaseSummary" runat="server" EnableClientScript="False"
                                ValidationGroup="CaseSummaryValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div class="row colpadding0 form-group mt30">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                         <label style="width: 3%;   font-size: 13px; color: red;">*</label>
                                        <asp:Label runat="server" ID="lblParticulars" ForeColor="Black">Particulars</asp:Label>
                                    </div>
                                    <div class="col-md-9 colpadding0 ml2">
                                        <asp:DropDownList runat="server" ID="ddlParticulars" Enabled="false" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" onchange="ShowCaseSummaryAddbtn()" />
                                        <img id="lnkShowAddNewParticulars" style="display: none; margin-top: -56px; margin-left: 72%;" src="../../Images/add_icon_new.png"
                                            onclick="OpenPerticularsPopupModel();" alt="Add New Perticulars" title="Add New Particulars" />
                                        <asp:RequiredFieldValidator ID="rfvtbxcaseSummaryPerticulars" ErrorMessage="Please Select Perticular" ControlToValidate="ddlParticulars"
                                            runat="server" ValidationGroup="CaseSummaryValidationGroup" Display="None" />
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-3">
                                        <asp:Label runat="server" ID="lblState" ForeColor="Black">State</asp:Label>
                                    </div>
                                    <div class="col-md-8 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row colpadding0 form-group mt30">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <asp:Label runat="server" ID="lblDisputedAmt" ForeColor="Black">Disputed Amount</asp:Label>
                                    </div>
                                    <div class="col-md-9 colpadding0 ml2">
                                        <asp:TextBox runat="server" ID="txtDisputedAmt" CssClass="form-control" Width="70%" ViewStateMode="Enabled" onkeypress="return NumberOnly()"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-3">
                                        <asp:Label runat="server" ID="lblInterest" ForeColor="Black">Interest</asp:Label>
                                    </div>
                                    <div class="col-md-8 colpadding0">
                                        <asp:TextBox runat="server" ID="txtInterest" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row colpadding0 form-group mt30">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <asp:Label runat="server" ID="lblPenalty" ForeColor="Black">Penalty</asp:Label>
                                    </div>
                                    <div class="col-md-9 colpadding0 ml2">
                                        <asp:TextBox runat="server" ID="txtPenalty" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-3">
                                        <asp:Label runat="server" ID="lblAmountPaid" ForeColor="Black">Amount Paid</asp:Label>
                                    </div>
                                    <div class="col-md-8 colpadding0">
                                        <asp:TextBox runat="server" ID="txtAmountPaid" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row colpadding0 form-group mt30">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <asp:Label runat="server" ID="lblFavourable" ForeColor="Black">Favourable</asp:Label>
                                    </div>
                                    <div class="col-md-9 colpadding0 ml2">
                                        <asp:TextBox runat="server" ID="txtFavourable" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-3">
                                        <asp:Label runat="server" ID="lblUnfavourable" ForeColor="Black">Unfavourable</asp:Label>
                                    </div>
                                    <div class="col-md-8 colpadding0">
                                        <asp:TextBox runat="server" ID="txtUnfavourable" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row colpadding0 form-group mt30">
                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-12 colpadding0">
                                        <asp:Label runat="server" ID="lblExpense" ForeColor="Black">Expense / Provision made in A/c</asp:Label>
                                    </div>
                                    <div class="col-md-2 colpadding0">

                                    </div>
                                    <div class="col-md-9 colpadding0 ml2">
                                        <asp:TextBox runat="server" ID="txtExpense" CssClass="form-control" Width="70%" onkeypress="return NumberOnly()"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6 colpadding0">
                                </div>
                            </div>
                        </div>
                        <div class="row colpadding0" style="margin-bottom: 7px; margin-top: 4%">
                            <div class="col-md-12 colpadding0 text-center">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="CaseSummaryValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="AddcaseSpeciman" OnClientClick="CloseMe()" />
                            </div>
                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top:8px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>


    <%--Add Perticulars--%>
    <div class="modal fade" id="AddPerticularsModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p0" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add Perticulars</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IFPerticulars" frameborder="0" runat="server" width="100%" height="200px"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%--Add Perticulars--%>
</body>
</html>
