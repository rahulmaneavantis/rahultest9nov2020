﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDepartMent.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddDepartMent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript">

        function CloseMe() {
 		
                       window.parent.ClosePopDepartment();
        }

        function RefreshParent() {
            var checkPath = window.parent.location.href.toLowerCase();
            if (checkPath.toLowerCase().indexOf("casedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('casedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
            if (checkPath.toLowerCase().indexOf("noticedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('noticedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
        }
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationAddDept" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upDepartment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPopupValidationGroup" />
                            <asp:CustomValidator ID="cvDeptPopup" runat="server" EnableClientScript="False"
                                ValidationGroup="DepartmentPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Department</label>
                            <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Department Name can not be empty." ControlToValidate="txtFName"
                                runat="server" ValidationGroup="DepartmentPopupValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="DepartmentPopupValidationGroup"
                                ErrorMessage="Please enter a valid location" ControlToValidate="txtFName"
                                ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                        </div>
                        <div style="margin-bottom: 7px; text-align: center">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="DepartmentPopupValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancelDeptPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />


                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
<%--OnClientClick="javaScript:window.close(); return false;"--%>