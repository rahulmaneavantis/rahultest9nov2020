﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPerticulars.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddPerticulars" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script type="text/javascript">

        function CloseMe() {
            window.parent.CloseMyADDPerticulars();
        }


    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <div>
                <asp:ScriptManager ID="LitigationAddperticulars" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="upPerticulars" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <div style="margin-bottom: 7px">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                    ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </div>
                            <div style="margin-bottom: 7px; align-content: center">
                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                <label style="width: 120px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                    Perticulars</label>
                                <asp:TextBox runat="server" ID="txtperticulars" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Perticulars can not be empty." ControlToValidate="txtperticulars"
                                    runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                            </div>
                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary"
                                    ValidationGroup="PromotorValidationGroup" OnClick="btnSave_Click" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe()" />
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
