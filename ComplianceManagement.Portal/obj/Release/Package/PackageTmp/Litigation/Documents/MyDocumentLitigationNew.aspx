﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="MyDocumentLitigationNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Documents.MyDocumentLitigationNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
  
      <style type="text/css">
          .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right:2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
        .selecttodiv ul li{
    display: inline-block;
    width: 2.5em;
    height: 2.5em;
}
    </style>
    
     <script type="text/javascript">

         $(document).ready(function () {
            fhead('My Documents');
             setactivemenu('leftdocumentmenu');
           
            
             BindTypeofCase();
             BindTypeofStatus();
             BindTagFilter();
             BindTypeofCaseType();
             BindTypeofLocation();
            
             BindTypeofOpponent();
             BindTypeofDepartment();
             bindFY();
             BindPriority();
             bindTaskType();
             Bindgrid();
             BindPopupGrid();
            
             var dropdownlist= $("#dropdownPriority").data("kendoDropDownTree");
             var dropdownlist3 = $("#dropdownlist3").data("kendoDropDownTree");
             var dropdownTaskType = $("#dropdownTaskType").data("kendoDropDownList");
             var dropdownDept = $("#dropdownDept").data("kendoDropDownTree");
             if ($("#dropdownType").val() == "T") {
                 dropdownlist.wrapper.show(); // call for hide kendo dropdown call
                 dropdownTaskType.wrapper.show(); // call for hide kendo dropdown call
                 dropdownlist3.wrapper.hide();
                 dropdownDept.wrapper.hide();
             }
             else {
                 dropdownlist.wrapper.hide();// to make it visible again
                 dropdownTaskType.wrapper.hide();// to make it visible again
                 dropdownlist3.wrapper.show();
                 dropdownDept.wrapper.show();
             }

             var dropdownPriority1 = $("#dropdownPriority1").data("kendoDropDownTree");
             var dropdownCaseType = $("#dropdownCaseType").data("kendoDropDownTree");
             var dropdownTaskType1 = $("#dropdownTaskType1").data("kendoDropDownList");
             var dropdownDept1 = $("#dropdownDept1").data("kendoDropDownTree");
             var dropdownOpponent = $("#dropdownOpponent").data("kendoDropDownTree");
             var listView = $('#testList').getKendoListView();
             if ($("#dropdownlistCase").val() == "T") {
                 dropdownPriority1.wrapper.show(); // call for hide kendo dropdown call
                 dropdownTaskType1.wrapper.show(); // call for hide kendo dropdown call
                 dropdownCaseType.wrapper.hide();
                 dropdownDept1.wrapper.hide();
                 dropdownOpponent.wrapper.hide();
                 listView.wrapper.hide();
                 $("#Applybtn").hide();
             }
             else {
                 dropdownPriority1.wrapper.hide();// to make it visible again
                 dropdownTaskType1.wrapper.hide();// to make it visible again
                 dropdownCaseType.wrapper.show();
                 dropdownDept1.wrapper.show();
                 dropdownOpponent.wrapper.show();
                 listView.wrapper.show();
                 $("#Applybtn").show();
             }

           
            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {
            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });

         });

         function bindTaskType() {
             $("#dropdownTaskType").kendoDropDownList({
                 autoClose: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 change: function (e) {
                     FilterAll();
                 },
                 dataSource: [
                     { text: "All", value: "All" },
                     { text: "Case", value: "C" },
                     { text: "Notice", value: "N" },
                     { text: "Individual Task", value: "T" }
                 ]
             });

             $("#dropdownTaskType1").kendoDropDownList({
                 autoClose: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 change: function (e) {
                     FilterAllAdvanced();
                 },
                 dataSource: [
                     { text: "All", value: "All" },
                     { text: "Case", value: "C" },
                     { text: "Notice", value: "N" },
                     { text: "Individual Task", value: "T" }
                 ]
             });
         }
         function bindFY() {
             $("#dropdownFY").kendoDropDownList({
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 change: function () {
                     FilterAllAdvanced();
                 },
                 dataSource: [
                     { text: "Financial Year", value: "1900-1900" },
                     { text: "2020-2021", value: "2020-2021," },
                     { text: "2019-2020", value: "2019-2020," },
                     { text: "2018-2019", value: "2018-2019," },
                     { text: "2017-2018", value: "2017-2018," },
                     { text: "2016-2017", value: "2016-2017," },
                     { text: "2015-2016", value: "2015-2016," },
                     { text: "2014-2015", value: "2014-2015," },
                     { text: "2013-2014", value: "2013-2014," },
                     { text: "2012-2013", value: "2012-2013," }

                 ]
             });

         }


         function BindPriority() {
             $("#dropdownPriority").kendoDropDownTree({
                 placeholder: "Priority",
                 checkboxes: {
                     checkChildren: true
                 },
                 autoClose: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "text",
                 change: function (e) {
                     //Bindgrid();
                     FilterAll();
                     fCreateStoryBoard('dropdownPriority', 'filtersstoryboardPriority', 'Priority');
                 },
                 dataSource: [
                     { text: "High", value: "High" },
                     { text: "Medium", value: "Medium" },
                     { text: "Low", value: "Low" }
                 ]
             });
             $("#dropdownPriority1").kendoDropDownTree({
                 placeholder: "Priority",
                 checkboxes: {
                     checkChildren: true
                 },
                 autoClose: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "text",
                 change: function (e) {
                     //Bindgrid();
                     FilterAllAdvanced();
                     fCreateStoryBoard('dropdownPriority1', 'filtersstoryboardPriority1', 'Priority1');
                 },
                 dataSource: [
                     { text: "High", value: "High" },
                     { text: "Medium", value: "Medium" },
                     { text: "Low", value: "Low" }
                 ]
             });
         }

         function BindTypeofDepartment() {
             $("#dropdownDept").kendoDropDownTree({
                 placeholder: "Department",
                 checkboxes: true,
                 checkAll: true,
                 autoClose: true,
                 checkAllTemplate: "Select All",
                 autoWidth: true,
                 dataTextField: "DepartmentName",
                 //dataValueField: "DepartmentName",
                 dataValueField: "DepartmentID",
                 change: function () {
                     FilterAll();
                     fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
                 },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                      },
                  }
              });

             $("#dropdownDept1").kendoDropDownTree({
                 placeholder: "Department",
                 checkboxes: true,
                 checkAll: true,
                 autoClose: true,
                 checkAllTemplate: "Select All",
                 autoWidth: true,
                 dataTextField: "DepartmentName",
                 dataValueField: "DepartmentID",
                 change: function () {
                     FilterAllAdvanced();
                     fCreateStoryBoard('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
                 },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });
         }

         function BindTypeofLocation() {
             $("#dropdownLocation").kendoDropDownTree({
                 placeholder: "Entity/Sub Entity/Location",
                 checkboxes: {
                     checkChildren: true
                 },
                 autoWidth: true,
                 dataTextField: "Name",
                 dataValueField: "ID",
                 change: function (e) {
                     FilterAll();
                     fCreateStoryBoard('dropdownLocation', 'filterlocation', 'Location');
                 },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                      },
                      schema: {
                          data: function (response) {
                              return response[0].locationList;
                          },
                          model: {
                              children: "Children"
                          }
                      }
                  }
             });

             $("#dropdowntree1").kendoDropDownTree({
                 placeholder: "Entity/Sub Entity/Location",
                 checkboxes: {
                     checkChildren: true
                 },
                 //checkAll: true,
                 autoWidth: true,
                 //checkAllTemplate: "Select All",
                 dataTextField: "Name",
                 dataValueField: "ID",
                 //optionLabel: "All",
                 change: function (e) {
                     FilterAllAdvanced();
                     fCreateStoryBoard('dropdowntree1', 'filterlocation1', 'Location1');
                 },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
         }

         function BindTypeofCaseType() {

             $("#dropdownlist3").kendoDropDownTree({
                 placeholder: "Type",
                 checkboxes: true,
                 checkAll: true,
                 autoClose: true,
                 checkAllTemplate: "Select All",
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 change: function () {
                     FilterAll();
                     fCreateStoryBoard('dropdownlist3', 'filtercase', 'Case');
                 },
                 dataSource: [

                     { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                     { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff

                 ]
             });

             $("#dropdownCaseType").kendoDropDownTree({
                 placeholder: "Type",
                 checkboxes: true,
                 checkAll: true,
                 autoClose: true,
                 checkAllTemplate: "Select All",
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 change: function () {
                     FilterAllAdvanced();
                    fCreateStoryBoard('dropdownCaseType', 'filtercase1', 'Case1');
                 },
                 dataSource: [

                     { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                     { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff

                 ]
             });

         }

         function BindTypeofOpponent() {
             $("#dropdownOpponent").kendoDropDownTree({
                 placeholder: "Opponent",
                 checkboxes: true,
                 checkAll: true,
                 autoClose: true,
                 checkAllTemplate: "Select All",
                 autoWidth: true,
                 dataTextField: "Name",
                 dataValueField: "Name",
                 change: function () {
                     FilterAllAdvanced();
                     fCreateStoryBoard('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
                 },
                 dataSource: {
                     severFiltering: true,
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/myDocumentFilters?CustomerID=<% =CustId%>&ddltype=2',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>Litigation/myDocumentFilters?CustomerID=<% =CustId%>&ddltype=2'
                    }
                }
             });

         }

         function BindTypeofCase() {

             $("#dropdownType").kendoDropDownList({
                 autoWidth: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 index: 0,
                 change: function (e) {
                     var dropdownlist = $("#dropdownPriority").data("kendoDropDownTree");
                     var dropdownlist3 = $("#dropdownlist3").data("kendoDropDownTree");
                     var dropdownTaskType = $("#dropdownTaskType").data("kendoDropDownList");
                     var dropdownDept = $("#dropdownDept").data("kendoDropDownTree");
                     if ($("#dropdownType").val() == "T") {
                         dropdownlist.wrapper.show(); 
                         dropdownTaskType.wrapper.show();
                         dropdownlist3.wrapper.hide();
                         dropdownDept.wrapper.hide();
                         $("#dropdownlist2").data("kendoDropDownList").setDataSource(TaskdataSource);
                     }
                     else {
                         dropdownlist.wrapper.hide();
                         dropdownTaskType.wrapper.hide();
                         dropdownlist3.wrapper.show();
                         dropdownDept.wrapper.show();
                         $("#dropdownlist2").data("kendoDropDownList").setDataSource(CNdataSource);
                     }
                     ClearAllFilterMain(e);
                 },
                 dataSource: [
                     { text: "Case", value: "C" },
                     { text: "Notice", value: "N" },
                     { text: "Task", value: "T" },

                 ]
             });

             $("#dropdownlistCase").kendoDropDownList({
                 autoWidth: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 index: 0,
                 change: function (e) {
                     var dropdownPriority1 = $("#dropdownPriority1").data("kendoDropDownTree");
                     var dropdownCaseType = $("#dropdownCaseType").data("kendoDropDownTree");
                     var dropdownTaskType1 = $("#dropdownTaskType1").data("kendoDropDownList");
                     var dropdownDept1 = $("#dropdownDept1").data("kendoDropDownTree");
                     var dropdownOpponent = $("#dropdownOpponent").data("kendoDropDownTree");
                     var listView = $('#testList').getKendoListView();
                     if ($("#dropdownlistCase").val() == "T") {
                         dropdownPriority1.wrapper.show(); // call for hide kendo dropdown call
                         dropdownTaskType1.wrapper.show(); // call for hide kendo dropdown call
                         dropdownCaseType.wrapper.hide();
                         dropdownDept1.wrapper.hide();
                         dropdownOpponent.wrapper.hide();
                         listView.wrapper.hide();
                         $("#Applybtn").hide();
                         $("#dropdownstatus").data("kendoDropDownList").setDataSource(TaskdataSource);
                     }
                     else {
                         dropdownPriority1.wrapper.hide();// to make it visible again
                         dropdownTaskType1.wrapper.hide();// to make it visible again
                         dropdownCaseType.wrapper.show();
                         dropdownDept1.wrapper.show();
                         dropdownOpponent.wrapper.show();
                         listView.wrapper.show();
                         $("#Applybtn").show();
                         $("#dropdownstatus").data("kendoDropDownList").setDataSource(CNdataSource);
                     }

                     //BindPopupGrid();
                     //BindTagFilter();
                     //FilterAllAdvanced();
                     ClearAllFilter(e);
                 },
                 dataSource: [
                     { text: "Case", value: "C" },
                     { text: "Notice", value: "N" },
                     { text: "Task", value: "T" },

                 ]
             });

         }
         

         var TaskdataSource = new kendo.data.DataSource({
             data: [
                 { text: "Status", value: "-1" },
                 { text: "Pending/Open", value: "1" },
                 { text: "Submitted", value: "2" },
                 { text: "Disposed/Closed", value: "3" },

             ]
         });
         var CNdataSource = new kendo.data.DataSource({
             data: [
                 { text: "Status", value: "-1" },
                 { text: "Pending/Open", value: "0" },
                 { text: "Disposed/Closed", value: "3" },
             ]
         });
         function BindTypeofStatus() {
         
             $("#dropdownlist2").kendoDropDownList({
                 autoWidth: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 index: 0,
                 change: function (e) {
                     FilterAll();
                 },
                 dataSource: CNdataSource
             });

             $("#dropdownstatus").kendoDropDownList({
                 filter: "startswith",
                 autoClose: true,
                 autoWidth: true,
                 dataTextField: "text",
                 dataValueField: "value",
                 index: 0,
                 change: function (e) {
                     FilterAllAdvanced();
                 },
                 dataSource: [
                     { text: "Status", value: "-1" },
                     { text: "Pending/Open", value: "0" },
                     { text: "Disposed/Closed", value: "3" },

                 ]
             });


         }

         function fCreateStoryBoard(Id, div, filtername) {

             var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
             $('#' + div).html('');
             $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
             $('#' + div).css('display', 'block');

             if (div == 'filterlocation') {
                 $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
                 $('#ClearfilterMain').css('display', 'block');
             }
             if (div == 'filtercase') {
                 $('#' + div).append('Party Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard  
                 $('#ClearfilterMain').css('display', 'block');
             }
             else if (div == 'filtersstoryboardDept') {
                 $('#' + div).append('Department&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
                 $('#ClearfilterMain').css('display', 'block');
             }
             else if (div == 'filtersstoryboardPriority') {
                 $('#' + div).append('Priority&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
                 $('#ClearfilterMain').css('display', 'block');
             }
             else if (div == 'filtersstoryboardPriority1') {
                 $('#' + div).append('priority&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
             }
             else if (div == 'filtersstoryboardOpponent') {
                 $('#' + div).append('Opponent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
             }
             else if (div == 'filtercase1') {
                 $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
             }
             else if (div == 'filterlocation1') {
                 $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
             }
             else if (div == 'filtersstoryboardDept1') {
                 $('#' + div).append('Department&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Dashboard
      
             }

             for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                 var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                 $(button).css('display', 'none');
                 $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                 var buttontest = $($(button).find('span')[0]).text();
                 if (buttontest.length > 10) {
                     buttontest = buttontest.substring(0, 10).concat("...");
                 }
                 $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
             }

             if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                 $('#' + div).css('display', 'none');
                 $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

             }

             CheckFilterClearorNotForDoc();
         }

         function CheckFilterClearorNotForDoc() {
             if (($($($('#dropdownLocation').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
                 && ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
                 && ($($($('#dropdownlist3').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
                 && ($($($('#dropdownPriority').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0))
                {
                 $('#ClearfilterMain').css('display', 'none'); 
             }
             else {
                 $('#ClearfilterMain').css('display', 'block');
             }
         }
         function fcloseStory(obj) {
             var DataId = $(obj).attr('data-Id');
             var dataKId = $(obj).attr('data-K-Id');
             var seq = $(obj).attr('data-seq');
             var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
             $(deepspan).trigger('click');
             var upperli = $('#' + dataKId);
             $(upperli).remove();

             fCreateStoryBoard('dropdownLocation', 'filterlocation', 'Location');
             fCreateStoryBoard('dropdownlist3', 'filtercase', 'Case');
             fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
             fCreateStoryBoard('dropdownPriority', 'filtersstoryboardPriority', 'Priority');
             
             fCreateStoryBoard('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
             fCreateStoryBoard('dropdownCaseType', 'filtercase1', 'Case1');
             fCreateStoryBoard('dropdowntree1', 'filterlocation1', 'Location1');
             fCreateStoryBoard('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
             fCreateStoryBoard('dropdownPriority1', 'filtersstoryboardPriority1', 'Priority1');


             CheckFilterClearorNotForDoc();
           
         };

         function FilterAll() {
             var casetype = $("#dropdownlist3").data("kendoDropDownTree")._values;

             var DeptList = $("#dropdownDept").data("kendoDropDownTree")._values;

             var location = $("#dropdownLocation").data("kendoDropDownTree")._values;

             var prioritylist = $("#dropdownPriority").data("kendoDropDownTree")._values;

             if (casetype.length > 0 || location.length > 0 || DeptList.length > 0
                 || ($("#dropdownlist2").data("kendoDropDownList") != undefined && $("#dropdownlist2").data("kendoDropDownList") != "")
                 || ($("#dropdownTaskType").data("kendoDropDownList") != undefined && $("#dropdownTaskType").data("kendoDropDownList") != "")
                 || prioritylist.length > 0)
             {
                 var finalSelectedfilter = { logic: "and", filters: [] };

                 if ($("#dropdownlist2").data("kendoDropDownList") != undefined && $("#dropdownlist2").data("kendoDropDownList") != "") {

                     var drpstatus = { logic: "or", filters: [] };
                         if ($("#dropdownlist2").val() == "-1") {
                             drpstatus.filters.push({
                                 field: "Status", operator: "neq", value: -1
                             });
                         }
                         else {
                             drpstatus.filters.push({
                                 field: "Status", operator: "eq", value: $("#dropdownlist2").val()
                             });
                         }
                     finalSelectedfilter.filters.push(drpstatus);
                 }

                 if ($("#dropdownTaskType").data("kendoDropDownList") != undefined && $("#dropdownTaskType").data("kendoDropDownList") != "") {
                     var TaskType = { logic: "or", filters: [] };
                     if ($("#dropdownTaskType").val() == "All") {
                         TaskType.filters.push({
                             field: "TaskType", operator: "neq", value: ($("#dropdownTaskType").val())
                         });
                     }
                     else {
                         TaskType.filters.push({
                             field: "TaskType", operator: "eq", value: ($("#dropdownTaskType").val())
                         });
                     }
                     finalSelectedfilter.filters.push(TaskType);
                 }

                 if (casetype.length > 0) {

                     var DeptFilter = { logic: "or", filters: [] };
                     if ($("#dropdownType").val() == "C") {
                         $.each(casetype, function (i, v) {
                             if (v == "I") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                             }

                             if (v == "O") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                             }

                         });
                         finalSelectedfilter.filters.push(DeptFilter);

                     }
                     if ($("#dropdownType").val() == "N") {
                         $.each(casetype, function (i, v) {
                             if (v == "I") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                             }

                             if (v == "O") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                             }

                         });
                         finalSelectedfilter.filters.push(DeptFilter);

                     }
                 }

                 if (prioritylist.length > 0) {
                     var locFilter = { logic: "or", filters: [] };

                     $.each(prioritylist, function (i, v) {
                         locFilter.filters.push({
                             field: "Priority", operator: "eq", value: v
                         });
                     });

                     finalSelectedfilter.filters.push(locFilter);
                 }

                 if (DeptList.length > 0) {

                     var DeptListfilter = { logic: "or", filters: [] };

                     $.each(DeptList, function (i, v) {
                         DeptListfilter.filters.push({
                             field: "DepartmentID", operator: "eq", value: v
                         });
                     });

                     finalSelectedfilter.filters.push(DeptListfilter);
                 }

                 if (location.length > 0) {

                     var locationfilter = { logic: "or", filters: [] };

                     $.each(location, function (i, v) {
                         locationfilter.filters.push({
                             field: "CustomerBranchID", operator: "eq", value: v
                         });
                     });

                     finalSelectedfilter.filters.push(locationfilter);
                 }

                 if (finalSelectedfilter.filters.length > 0) {
                     var dataSource = $("#grid").data("kendoGrid").dataSource;
                     dataSource.filter(finalSelectedfilter);
                 }
                 else {
                     $("#grid").data("kendoGrid").dataSource.filter({});
                 }
             }
             else {
                 $("#grid").data("kendoGrid").dataSource.filter({});
             }
         }

         function FilterAllAdvanced()
         {
            // var status = $("#dropdownstatus").data("kendoDropDownList")._values;

             var casetype = $("#dropdownCaseType").data("kendoDropDownTree")._values;

             var opponent = $("#dropdownOpponent").data("kendoDropDownTree")._values;

             var location = $("#dropdowntree1").data("kendoDropDownTree")._values;

             var department = $("#dropdownDept1").data("kendoDropDownTree")._values; 

             var prioritylist = $("#dropdownPriority1").data("kendoDropDownTree")._values;
                          
             if (casetype.length > 0 || location.length > 0 || opponent.length > 0 || department.length > 0
                 || ($("#dropdownstatus").data("kendoDropDownList") != undefined && $("#dropdownstatus").data("kendoDropDownList") != "")
                 || ($("#dropdownTaskType1").data("kendoDropDownList") != undefined && $("#dropdownTaskType1").data("kendoDropDownList") != "")
                 || $("#dropdownFY").data("kendoDropDownList") != undefined && $("#dropdownFY").data("kendoDropDownList") != "")
              {
                 var finalSelectedfilter = { logic: "and", filters: [] };

                 if ($("#dropdownstatus").val() != undefined && $("#dropdownstatus").val() != "" && $("#dropdownstatus").val() != "-1") {

                     var drpstatus = { logic: "or", filters: [] };

                     drpstatus.filters.push({
                         field: "Status", operator: "eq", value: $("#dropdownstatus").val()
                     });

                     //if ($("#dropdownstatus").val() != "-1") {
                     //    drpstatus.filters.push({
                     //        field: "Status", operator: "eq", value: 0
                     //    });
                     //    drpstatus.filters.push({
                     //        field: "Status", operator: "eq", value: 3
                     //    });
                     //}
                     //else {
                     //    drpstatus.filters.push({
                     //        field: "Status", operator: "eq", value: $("#dropdownstatus").val()
                     //    });
                     //}

                     finalSelectedfilter.filters.push(drpstatus);
                 }
                 if ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "") {
                     var FYFilter = { logic: "or", filters: [] };
                     if ($("#dropdownFY").val() == "1900-1900")
                     {
                         FYFilter.filters.push({
                             field: "FYName", operator: "neq", value: $("#dropdownFY").val()+','
                         });
                     }
                     else
                     {
                         FYFilter.filters.push({
                             field: "FYName", operator: "contains", value: $("#dropdownFY").val()
                         });
                     }

                     finalSelectedfilter.filters.push(FYFilter);
                 }

                 if ($("#dropdownTaskType1").data("kendoDropDownList") != undefined && $("#dropdownTaskType1").data("kendoDropDownList") != "") {
                     var TaskType = { logic: "or", filters: [] };
                     if ($("#dropdownTaskType1").val() == "All") {
                         TaskType.filters.push({
                             field: "TaskType", operator: "neq", value: ($("#dropdownTaskType1").val())
                         });
                     }
                     else {
                         TaskType.filters.push({
                             field: "TaskType", operator: "eq", value: ($("#dropdownTaskType1").val())
                         });
                     }
                     finalSelectedfilter.filters.push(TaskType);
                 }

                 if (prioritylist.length > 0) {
                     var locFilter = { logic: "or", filters: [] };

                     $.each(prioritylist, function (i, v) {
                         locFilter.filters.push({
                             field: "Priority", operator: "eq", value: v
                         });
                     });

                     finalSelectedfilter.filters.push(locFilter);
                 }

                 if (casetype.length > 0) {

                     var DeptFilter = { logic: "or", filters: [] };
                     if ($("#dropdownlistCase").val() == "C") {
                         $.each(casetype, function (i, v) {
                             if (v == "I") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                             }

                             if (v == "O") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                             }

                         });
                         finalSelectedfilter.filters.push(DeptFilter);

                     }
                     if ($("#dropdownlistCase").val() == "N") {
                         $.each(casetype, function (i, v) {
                             if (v == "I") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                             }

                             if (v == "O") {
                                 DeptFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                             }

                         });
                         finalSelectedfilter.filters.push(DeptFilter);

                     }
                 }
                 if (opponent.length > 0) {

                     var opponentfilter = { logic: "or", filters: [] };

                     $.each(opponent, function (i, v) {
                         opponentfilter.filters.push({
                             field: "PartyName", operator: "Contains", value: v
                         });
                     });

                     finalSelectedfilter.filters.push(opponentfilter);
                 }

                 if (location.length > 0) {

                     var locationfilter = { logic: "or", filters: [] };

                     $.each(location, function (i, v) {
                         locationfilter.filters.push({
                             field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                         });
                     });

                     finalSelectedfilter.filters.push(locationfilter);
                 }

                 if (department.length > 0) {

                     var DeptFilter = { logic: "or", filters: [] };
                     $.each(department, function (i, v) {
                     DeptFilter.filters.push({
                         field: "DepartmentID", operator: "eq", value: v
                     });
                 });
                     finalSelectedfilter.filters.push(DeptFilter);
                 }

                 if (finalSelectedfilter.filters.length > 0) {
                     var dataSource = $("#grid1").data("kendoGrid").dataSource;
                     dataSource.filter(finalSelectedfilter);
                 }
                 else {
                     $("#grid1").data("kendoGrid").dataSource.filter({});
                 }
             }
             else {
                 $("#grid1").data("kendoGrid").dataSource.filter({});
             }
         }
       
         function OpenAdvanceSearch(e) {

             var myWindowAdv = $("#divAdvanceSearchModel");

             function onClose() {

             }

             myWindowAdv.kendoWindow({
                 width: "85%",
                 height: "80%",
                 title: "Advanced Search",
                 visible: false,
                 actions: [
                     //"Pin",
                     //"Minimize",
                     "Maximize",
                     "Close"
                 ],
                 close: onClose
             });
             $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

             myWindowAdv.data("kendoWindow").center().open();
             e.preventDefault();
             return false;
         }

         function ClearAllFilterMain(e) {
             $("#dropdownLocation").data("kendoDropDownTree").value([]);
             $("#dropdownlist3").data("kendoDropDownTree").value([]);
             $("#dropdownDept").data("kendoDropDownTree").value([]);
             $("#dropdownPriority").data("kendoDropDownTree").value([]);
             $("#dropdownTaskType").data("kendoDropDownList").select(0);
             $("#dropdownlist2").data("kendoDropDownList").select(0);
             Bindgrid();
             $("#grid").data("kendoGrid").dataSource.filter({});
             e.preventDefault();
         }
   
         function ClearAllFilter(e) {
             $("#dropdowntree1").data("kendoDropDownTree").value([]);
             $("#dropdownDept1").data("kendoDropDownTree").value([]);
             $("#dropdownCaseType").data("kendoDropDownTree").value([]);
             $("#dropdownOpponent").data("kendoDropDownTree").value([]);
             $("#dropdownstatus").data("kendoDropDownList").select(0);
             $("#dropdownPriority1").data("kendoDropDownTree").value([]);
             $("#dropdownTaskType1").data("kendoDropDownList").select(0);
             $("#dropdownFY").data("kendoDropDownList").select(0);
             $("#testList").getKendoListView().clearSelection(); 
             TagFilterdetails = [];
             BindPopupGrid();
             $("#grid1").data("kendoGrid").dataSource.filter({});
             e.preventDefault();
         }
      
         function BindPopupGrid() {
             
             var TagFilterdetails = [];
             if ($("#testList").data("kendoListView").select().length > 0) {
                 for (var i = 0; i < $("#testList").data("kendoListView").select().length; i++) {
                     var getUID = $("#testList").data("kendoListView").select()[i].getAttribute("data-uid");
                     if ($("#testList").data("kendoListView").dataSource._data.length > 0) {
                         for (var j = 0; j < $("#testList").data("kendoListView").dataSource._data.length; j++) {
                             if ($("#testList").data("kendoListView").dataSource._data[j].uid == getUID) {
                                 //TagFilterdetails.push($(obj).attr('data-Id'));
                                 TagFilterdetails.push($("#testList").data("kendoListView").dataSource._data[j].filetag);
                                 //alert($("#testList").data("kendoListView").dataSource._data[j].filetag);
                             }
                         }
                     }
                 }
             }

             var grid = $("#grid1").data("kendoGrid");
             if (grid != undefined || grid != null) {
                 $("#grid1").empty();
             }

             if ($("#dropdownlistCase").val() == "C" || $("#dropdownlistCase").val() == "N") {
                 CaseNoticeTaskVisible = false;
                 Task = true;
             }
             else {
                 CaseNoticeTaskVisible = true;
                 Task = false;
             }
             if ($("#dropdownlistCase").val() == "C") {
                 LitigationType = "Case";
                 var ddlDataSource = [{
                     value: "A",
                     displayValue: "All"
                 },
                 {
                     value: "C",
                     displayValue: "Case Documents"
                 },
                 {
                     value: "CH",
                     displayValue: "Case Hearing Documents"
                 },
                 {
                     value: "CO",
                     displayValue: "Case Order Documents"
                 },
                 {
                     value: "CT",
                     displayValue: "Case Task Documents"
                 }
                 ];
             }
             if ($("#dropdownlistCase").val() == "N") {
                 LitigationType = "Notice";
                 var ddlDataSource = [{
                     value: "A",
                     displayValue: "All"
                 },
                 {
                     value: "N",
                     displayValue: "Notice Documents"
                 },
                 {
                     value: "NR",
                     displayValue: "Notice Response Documents"
                 },
                 {
                     value: "NT",
                     displayValue: "Notice Task Documents"
                 }

                 ];
             }

             //Kendo Grid 2
             var grid1 = $("#grid1").kendoGrid({
                 dataSource: {
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/kendomyDocument?customerID=<% =CustId%>&loggedInUserID=<% =UId %>&loggedInUserRole=<% =FlagIsApp%>&RoleID=3&divid=' + $("#dropdownlistCase").val() + '&status=-1&TagFilter=' + JSON.stringify(TagFilterdetails),
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>Litigation/kendomyDocument?customerID=<% =CustId%>&loggedInUserID=<% =UId %>&loggedInUserRole=<% =FlagIsApp%>&RoleID=3&divid=' + $("#dropdownlistCase").val() + '&status=-1&TagFilter=' + JSON.stringify(TagFilterdetails),
                     },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                columnMenuInit: function (e) {
                     var item = e.container.find(".k-columns-item");
                     item.prev(".k-separator").remove();
                     item.remove();
                },
                pageable: true,
                reorderable: true,
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.dropDownTemplate');

                        $(ddt).kendoDropDownList({
                            value: dataItem.value,
                            dataSource: ddlDataSource,
                            dataTextField: "displayValue",
                            dataValueField: "value",
                            change: onDDLChange
                        });
                    });
                },
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                 columns: [
                     {
                         title: "Sr",
                         template: "#= ++record #",
                         width: "5%",
                     },
                     {
                         field: "TypeName", title: 'Type',
                         hidden: CaseNoticeTaskVisible,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "RefNo", title: LitigationType + ' No.',
                         hidden: CaseNoticeTaskVisible,
                         attributes: {
                             style: 'white-space: nowrap;'

                         }, filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Title", title: 'Title',
                         type: "string",
                         hidden: CaseNoticeTaskVisible,
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },

                     {
                         field: "FYName", title: 'Financial Year',
                         type: "string",
                         hidden: true,
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },

                     {
                         field: "PartyName", title: 'Opponent',
                         type: "string",
                         hidden: CaseNoticeTaskVisible,
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "BranchName", title: 'Location',
                         hidden: true,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Priority", title: 'Priority',
                         hidden: Task,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Task", title: 'Task',
                         hidden: Task,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Task_Description", title: 'Task Description',
                         hidden: Task,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Due_date", title: 'Due Date',
                         hidden: Task,
                         type: "date",
                         format: "{0:dd-MM-yyyy}"
                     },
                     {
                         field: "Status", title: 'Status',
                         template: "#if(Status == '0') {#<div>Open</div>#}if(Status == '3') {#<div>Closed</div>#} if(Status == '1') {#<div>Submitted</div>#}#",
                         hidden: Task,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "Assigned_To", title: 'Assigned To',
                         hidden: Task,
                         type: "string",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "value",
                         title: "Select Document",
                         hidden: CaseNoticeTaskVisible,
                         width: "20%",
                         template: columnTemplateFunction,
                     },
                     {
                         command: [
                             { name: "edit", text: "", iconClass: "k-icon k-i-download", className: "ob-download1" },
                             { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-view1" }], title: "Action", lock: true, width: "15%;",// width: 150,
                     }
                 ]
            });
             function onDDLChange(e) {
                 var element = e.sender.element;
                 var row = element.closest("tr");
                 var grid = $("#grid1").data("kendoGrid");
                 var dataItem = grid.dataItem(row);

                 dataItem.set("value", e.sender.value());
             };
             function columnTemplateFunction(dataItem) {
                 var input = '<input class="dropDownTemplate"/>'

                 return input
             };

             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(2)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");

             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(3)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");


             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(4)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(5)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: ".k-grid-edit",
                 content: function (e) {
                     return "Download Documents";
                 }
             });
             $("#grid1").kendoTooltip({
                 filter: ".k-grid-edit1",
                 content: function (e) {
                     return "View Documents";
                 }
             });


             $(document).on("click", "#grid1 tbody tr .ob-download1", function (e) {
                 var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));

                 if (item.value != undefined && item.value != "") {
                     $('#downloadfile').attr('src', '../../Litigation/Common/DownloadMyDocument.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID  + '&DocType=' + $("#dropdownlistCase").val() + '&DocTypeDownload=' + item.value);
                 }
                 else {
                     $('#downloadfile').attr('src', '../../Litigation/Common/DownloadMyDocument.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownlistCase").val() + '&DocTypeDownload=A');
                 }
                 e.preventDefault();
                 return false;
             });
             $(document).on("click", "#grid1 tbody tr .ob-view1", function (e) {
                 var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                 $('#divViewDocument').modal('show');
                 $('.modal-dialog').css('width', '90%');

                 if (item.value != undefined && item.value != "") {
                     $('#OverViews').attr('src', '../../Litigation/Common/DocumentOverview.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownlistCase").val() + '&DocTypeDownload=' + item.value);
                 }
                 else {
                     $('#OverViews').attr('src', '../../Litigation/Common/DocumentOverview.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownlistCase").val() + '&DocTypeDownload=A');
                  }
                 //$('#OverViews').attr('src', "../../Litigation/Common/DocumentOverview.aspx?TaskID=1518");                                
                 e.preventDefault();
                 return false;
             });

         }

         var CaseNoticeTaskVisible = false;
         var Task = false;
         
         var LitigationType = 0;
         function Bindgrid() {
             var TagFilterdetails = [];

             var grid = $("#grid").data("kendoGrid");
             if (grid != undefined || grid != null) {
                 $("#grid").empty();
             }
             if ($("#dropdownType").val() == "C" || $("#dropdownType").val() == "N") {
                 CaseNoticeTaskVisible = false;
                 Task = true;
             }
             else {
                 CaseNoticeTaskVisible = true;
                 Task = false;
             }
             if ($("#dropdownType").val() == "C") {
                 LitigationType = "Case"
                 var ddlDataSource = [{
                     value: "A",
                     displayValue: "All"
                 },
                 {
                     value: "C",
                     displayValue: "Case Documents"
                 },
                 {
                     value: "CH",
                     displayValue: "Case Hearing Documents"
                 },
                 {
                    value: "CO",
                    displayValue: "Case Order Documents"
                 },
                 {
                    value: "CT",
                    displayValue: "Case Task Documents"
                 }
                 ];
             }
             if ($("#dropdownType").val() == "N") {
                 LitigationType = "Notice"
                 var ddlDataSource = [{
                     value: "A",
                     displayValue: "All"
                 },
                 {
                     value: "N",
                     displayValue: "Notice Documents"
                 },
                 {
                     value: "NR",
                     displayValue: "Notice Response Documents"
                 },
                 {
                     value: "NT",
                     displayValue: "Notice Task Documents"
                 }

                 ];
             }
             var grid = $("#grid").kendoGrid({
                 dataSource: {
                     transport: {
                         read: {
                             url: '<% =Path%>Litigation/kendomyDocument?customerID=<% =CustId%>&loggedInUserID=<% =UId %>&loggedInUserRole=<% =FlagIsApp%>&RoleID=3&divid=' + $("#dropdownType").val() + '&status=-1&TagFilter=' + JSON.stringify(TagFilterdetails),
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>Litigation/kendomyDocument?customerID=<% =CustId%>&loggedInUserID=<% =UId %>&loggedInUserRole=<% =FlagIsApp%>&RoleID=3&divid=' + $("#dropdownType").val() + '&status=-1&TagFilter=' + JSON.stringify(TagFilterdetails),
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                columnMenuInit: function (e) {
                     var item = e.container.find(".k-columns-item");
                     item.prev(".k-separator").remove();
                     item.remove();
                },
                 dataBinding: function () {
                     record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                 },
                pageable: true,
                reorderable: true,
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.dropDownTemplate');

                        $(ddt).kendoDropDownList({
                            value: dataItem.value,
                            dataSource: ddlDataSource,
                            dataTextField: "displayValue",
                            dataValueField: "value",
                            change: onDDLChange
                        });
                    });
                    for (var i = 1; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        title: "Sr",
                        template: "#= ++record #",
                        width:"5%"
                    },
                    {
                        field: "TypeName", title: 'Type',
                        hidden: CaseNoticeTaskVisible,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "RefNo", title: LitigationType + ' No.',
                        hidden: CaseNoticeTaskVisible,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        type: "string",
                        hidden: CaseNoticeTaskVisible,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "PartyName", title: 'Opponent',
                        type: "string",
                        hidden: CaseNoticeTaskVisible,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BranchName", title: 'Location',
                        hidden:true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Priority", title: 'Priority',
                        hidden: Task,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Task", title: 'Task',
                        hidden: Task,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Task_Description", title: 'Task Description',
                        hidden: Task,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Due_date", title: 'Due Date',
                        hidden: Task,
                        type: "date",
                        format:"{0:dd-MM-yyyy}"
                    },
                    {
                        field: "Status", title: 'Status',
                        template: "#if(Status == '0') {#<div>Pending/Open</div>#}if(Status == '3') {#<div>Disposed/Closed</div>#} if(Status == '1') {#<div>Submitted</div>#}#",
                        hidden: Task,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Assigned_To", title: 'Assigned To',
                        hidden: Task,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "value",
                        title: "Select Document",
                        hidden: CaseNoticeTaskVisible,
                        width:"20%",
                        template: columnTemplateFunction,
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" }], title: "Action", lock: true, width: "15%;",// width: 150,
                    }
                ]
            });
             function onDDLChange(e) {
                 var element = e.sender.element;
                 var row = element.closest("tr");
                 var grid = $("#grid").data("kendoGrid");
                 var dataItem = grid.dataItem(row);

                 dataItem.set("value", e.sender.value());
             };
             function columnTemplateFunction(dataItem) {
                 var input = '<input class="dropDownTemplate"/>'

                 return input
             };

             $("#grid").kendoTooltip({
                 filter: "td:nth-child(2)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");

             $("#grid").kendoTooltip({
                 filter: "td:nth-child(3)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");

             $("#grid").kendoTooltip({
                 filter: "td:nth-child(4)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");

             $("#grid").kendoTooltip({
                 filter: ".k-grid-edit",
                 content: function (e) {
                     return "Download Documents";
                 }
             });

             $("#grid").kendoTooltip({
                 filter: ".k-grid-edit1",
                 content: function (e) {
                     return "View Documents";
                 }
             });

             $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                 var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                 if (item.value != undefined && item.value != "") {
                     $('#downloadfile').attr('src', '../../Litigation/Common/DownloadMyDocument.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownType").val() + '&DocTypeDownload=' + item.value);
                 }
                 else {
                     $('#downloadfile').attr('src', '../../Litigation/Common/DownloadMyDocument.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownType").val() + '&DocTypeDownload=A');
                 }
                 e.preventDefault();
                 return false;
             });
             $(document).on("click", "#grid tbody tr .ob-view", function (e) {
                 var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                 $('#divViewDocument').modal('show');
                 $('.modal-dialog').css('width', '90%');

                 if (item.value != undefined && item.value != "") {
                     $('#OverViews').attr('src', '../../Litigation/Common/DocumentOverview.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownType").val() + '&DocTypeDownload=' + item.value);
                 }
                 else {
                     $('#OverViews').attr('src', '../../Litigation/Common/DocumentOverview.aspx?InstanceID=' + item.InstanceID + '&NoticeCaseInstanceID=' + item.NoticeCaseInstanceID + '&DocType=' + $("#dropdownType").val() + '&DocTypeDownload=A');
                 }
                 e.preventDefault();
                 return false;
             });

         }
         function BindTagFilter() {
             var viewModel = kendo.observable({
                 listtest:
                     new kendo.data.DataSource
                         ({
                             selectable: "multiple",
                             severFiltering: true,
                             //pageSize: 3,
                             transport: {
                                 read: {
                                     url: '<% =Path%>Litigation/myDocumentFiltersFiletag?customerID=<% =CustId%>&litigationInstanceID=0&UserID=<% =UId %>&Role=<% =FlagIsApp%>&ddtype=' + $("#dropdownlistCase").val(),
                                     dataType: "json",
                                     beforeSend: function (request) {
                                         request.setRequestHeader('Authorization', '<% =Authorization%>');
                                     },
                                 }
                                 //read: '<% =Path%>Litigation/myDocumentFiltersFiletag?customerID=<% =CustId%>&litigationInstanceID=0&UserID=<% =UId %>&Role=<% =FlagIsApp%>&ddtype=' + $("#dropdownlistCase").val()

                             },

                         }),
                 clickNext: function () {
                     var listView = $('#testList').getKendoListView();

                     var selectedItem = listView.select();
                     if (selectedItem) {
                         var index = selectedItem.index();
                         if (index < viewModel.listtest.data().length - 1) {
                             index++;
                             var newselection = viewModel.listtest.data()[index];
                             var listViewItem = listView.element.children("[data-uid='" + newselection.uid + "']");
                             listView.select(listViewItem);
                         }
                     }
                 },
                 clickPrev: function () {
                     var listView = $('#testList').getKendoListView();

                     var selectedItem = listView.select();
                     if (selectedItem) {
                         var index = selectedItem.index();
                         if (index > 0) {
                             index--;
                             var newselection = viewModel.listtest.data()[index];
                             var listViewItem = listView.element.children("[data-uid='" + newselection.uid + "']");
                             listView.select(listViewItem);
                         }
                     }
                     //return false;
                 }
             });
             kendo.bind('body', viewModel);
         }
         function ApplyBtnFilter(e) {
             e.preventDefault();
             BindPopupGrid();
             return false;
         }

   </script>

     <script type="text/x-kendo-tmpl" id="template">
                <div class="item">
                    #:filetag#                
                </span>
                </div>
            </script>

     <style type="text/css">

                #testList {
                    margin-left: 0px;
                    width: 100%;
                }

                .item.k-state-selected {
                    background-color: white;
                    color: black;
                }
                #outer {
                    width: 100%;
                }

                .left {
                    float: left;
                    display: table-cell;
                    /*height: 50px;*/
                }

                #inner {
                    display: table;
                    margin: 0 auto;
                    width: 100%;
                }

                .item {
                    width: auto;
                    height: 14px;
                    float:left;
                    margin: 3px;
                    padding: 5px;
                    text-align:center;
                    border: 1px solid #000000;
                    color: white;
                    background-color: #34aadc;
                     border-radius: .25em;
                }
                .ScrollMenu {
                    overflow: auto
                }
            </style>
  
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
           
      <div class="row" style="padding-top: 8px;margin-left: 11px;">
            <div class="toolbar">               
                <input id="dropdownType"/>  
                <input id="dropdownlist2" data-placeholder="Status" style="width:168px;"/>
                <input id="dropdownlist3" data-placeholder="Type"/>   
                 <input id="dropdownPriority" data-placeholder="Priority"/>   
                <input id="dropdownLocation" data-placeholder="Entity/Sub-Entity/Location" style="width:242px;"/>   
                <input id="dropdownDept" data-placeholder="Department"/> 
                 <input id="dropdownTaskType" data-placeholder="Type"/>   
                <button id="AdavanceSearch" style="height: 30px;width: 141px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            </div>
    </div>
      
    <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                          <button id="ClearfilterMain" style="float: right;display: none;height: 25px;width: 106px;margin-right: 10px;margin-bottom: 4px;margin-top: 5px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                      </div>
              </div>
                    <div class="row" style="padding-bottom: 4px;margin-left: 11px; font-size: 12px; display: none; color: #535b6a;" id="filtercase">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px;margin-left: 11px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboardDept">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px;margin-left: 11px; font-size: 12px; display: none; color: #535b6a;" id="filterlocation">&nbsp;</div>  
                     <div class="row" style="padding-bottom: 4px;margin-left: 11px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboardPriority">&nbsp;</div>
                                        
    
    <div class="row" style="padding-top: 12px;">
        <div id="grid" style="border: none;margin-left:10px;margin-right:10px"></div>
    </div>
    <div id="divAdvanceSearchModel" style="padding-top: 7px;z-index: 999;">
         <div class="row" style="margin-bottom: 7px;">
                   <div class="col-md-2"style="width: 16%; padding-left: 0px;">
                      <input id="dropdownlistCase" style="width:100%;"/> 
                    </div>
                   <div class="col-md-2" style="width: 16%; padding-left: 4px;">
                            <input id="dropdownstatus" data-placeholder="Status" style="width: 100%;" />
                        </div>
              <div class="col-md-2" id="dvdropdownFY" style="width: 16%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
              <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 16%; padding-left: 4px;">
                            <input id="dropdownCaseType" style="width: 106%;" />
                   <input id="dropdownPriority1" data-placeholder="Priority" style="width: 106%;"/>   
                        </div>
                            <div class="col-md-2" id="dvdropdowntree1" style="width: 28%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 107%;" />
                        </div>
                </div>

        <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                       <div class="col-md-2" style="width: 24%;padding-left: 9px;margin-bottom: 7px;">
                          <input id="dropdownDept1" style="width: 103%;" />
                           <input id="dropdownTaskType1" data-placeholder="Type" style="width: 100%;" />
                        </div>
                      <div class="col-md-4"style="width: 24.3%;padding-left: 10px;margin-bottom: 7px;">
                            <input id="dropdownOpponent" data-placeholder="Opponent" style="width: 101%;" />
                        </div>
                     
                       <div class="col-md-2"style="width: 16.7%;padding-left: 0px;">
                                  <button id="Clearfilter" style="display: block;height: 25px;width: 106px;margin-left: 6px;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                        </div>

                    </div>
                </div>
        <div class="row" style="margin-left: -9px; margin-right:9px; padding-bottom: 7px;">
           <div class="col-lg-10 col-md-10" style="max-height:67px;overflow:auto;">
           
                        <div class="ScrollMenu" id="testList" data-role="listview" data-bind="source: listtest" data-template="template" data-selectable="multiple"></div>
                 
               </div>
                <div class="col-md-1" >
                   <button id="Applybtn" style="height: 28px;" onclick="ApplyBtnFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
   
                   </div>
          </div>  

         <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                      </div>
              </div>
               <div class="row" style="padding-bottom: 4px;margin-left: 3px; font-size: 12px; display: none; color: #535b6a;" id="filterlocation1">&nbsp;</div>   
                      <div class="row" style="padding-bottom: 4px;margin-left: 3px; font-size: 12px; display: none; color: #535b6a;" id="filtercase1">&nbsp;</div>
                    <div class="row" style="padding-bottom: 4px;margin-left: 3px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboardDept1">&nbsp;</div>
                     <div class="row" style="padding-bottom: 4px;margin-left: 3px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboardOpponent">&nbsp;</div>   
                        <div class="row" style="padding-bottom: 4px;margin-left: 3px; font-size: 12px; display: none; color: #535b6a;" id="filtersstoryboardPriority1">&nbsp;</div>
                                
    
  <div id="grid1"></div>  

         </div>
    <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12 colpadding0">
                                <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe> 
</asp:Content>

