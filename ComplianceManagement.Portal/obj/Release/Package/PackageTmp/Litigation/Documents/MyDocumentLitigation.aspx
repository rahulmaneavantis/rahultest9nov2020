﻿<%@ Page Title="My Documents" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="MyDocumentLitigation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Documents.MyDocumentLitigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .RemoveBold {
            font-weight: normal;
        }

        .Addbold {
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
           
            $('input[id*=lstBoxFileTags]').hide();
            $(
                function () {
                    $("#tblComplianceDocumnets tbody tr")
                     .click(function () {
                         jQuery("#tblComplianceDocumnets tbody").find("td").removeClass('RemoveBold');
                         jQuery("#tblComplianceDocumnets tbody").find("td").removeClass('Addbold');
                         jQuery(this).find("td").addClass('Addbold');
                     });
                });
        });

        function runScript(e) {
            debugger;
            if (e.keyCode == 13) {
           
                 $("#<%= lbtsearchDocuments.ClientID%>").click();
            }
        }

        $(document).ready(function () {
            fhead('My Documents');
            setactivemenu('leftdocumentmenu');

          
            $('#AdvanceSearch').on('shown.bs.modal', function () {
                $('#divBranches').hide("blind", null, 100, function () { });
                document.getElementById('<%= tbxFilterLocation.ClientID %>').click();
            });
        });
        function fopendocfileLitigation(file) {

            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerLitigation').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
        });

    </script>

    <script type="text/javascript">
        function Scrolling() {

            $(document).ready(function () {
                $("#rightArrow").click(function () {
                    $('div.outerDivFileTags').animate({ scrollLeft: "+=1000" }, 'slow', function () {
                        reAdjust();
                    });
                });

                $("#leftArrow").click(function () {
                    $('div.outerDivFileTags').animate({ scrollLeft: "-=1000" }, 'slow', function () {
                        reAdjust();
                    });
                });
            });

            $(document).ready(function () {

                $("#rightArrow").click(function () {
                    $('div.divFileTags').animate({ scrollLeft: "+=1000" }, 'slow', function () {
                        reAdjust();
                    });
                });

                $("#leftArrow").click(function () {
                    $('div.divFileTags').animate({ scrollLeft: "-=1000" }, 'slow', function () {
                        reAdjust();
                    });
                });
            });

            $('input[id*=lstBoxFileTags]').hide();

        }
    </script>

    <style type="text/css">
        .tag .label {
            font-size: 100%;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        .panel-heading {
            background: #ffffff;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: #1fd9e1;
                background-color: #fff;
            }

        span.item.label.label-info > label {
            color: #fff;
            font-weight: 100;
        }

            span.item.label.label-info > label:hover {
                color: #007aff;
                font-weight: 100;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 colpadding0">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="NoticePageValidationGroup" />
                        <asp:CustomValidator ID="cvErrorNoticePage" runat="server" EnableClientScript="False"
                            ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <div class="row">
                        <div class="col-md-12 colpadding0" style="margin-top: 10px; margin-bottom: 50px;">
                            <div class="col-md-10 colpadding0">
                                <div class="tab-bg-primary panel-heading ">
                                    <ul class="nav nav-tabs">
                                        <li class="active" id="liCase" runat="server">
                                            <asp:LinkButton ID="lnkCase" CausesValidation="false" runat="server" OnClick="lnkCase_Click">Case</asp:LinkButton>
                                        </li>
                                        <li class="" id="liNotice" runat="server">
                                            <asp:LinkButton ID="lnkNotice" CausesValidation="false" runat="server" OnClick="lnkNotice_Click">Notice</asp:LinkButton>
                                        </li>
                                        <li class="" id="liTask" runat="server">
                                            <asp:LinkButton ID="LinkTask" CausesValidation="false" runat="server" OnClick="LinkTask_Click">Task</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2 colpadding0 text-right">
                                <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch">Advance Search</a>
                            </div>
                        </div>
                    </div>
                   
                 <% if(CustomerID.Equals(76)) { %>
<panel>
                    <div class="row colpadding0">
                        <div class="col-md-12 colpadding0 entrycount">
                            <div class="col-md-5">
                             <asp:TextBox runat="server" ID="txtsearchdoc" PlaceHolder="Search by Document Name"  CssClass="form-control" onkeydown = "return (event.keyCode!=13);"/>
                                </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-4 colpadding0">
                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lbtsearchDocuments"  OnClick="lbtsearchDocuments_Click" data-toggle="tooltip" ToolTip="Apply"  style="float:right"/>
                            </div>
                        </div>
                    </div>
                   </panel>
                 <% } %>

                    <%--Added by Ruchi--%>
                    <div class="row col-md-12 colpadding0" style="margin-top:2%">
                        <div id="outerDivFileTags" runat="server" class="col-md-12 colpadding0">
                            <div id="leftArrow" class="scroller scroller-left mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                                <span class="arrow-button arrow-button-right">
                                    <i class="fa fa-chevron-left color-black"></i>
                                </span>
                            </div>
                            <div id="rightArrow" class="scroller scroller-right mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                                <span runat="server" class="arrow-button arrow-button-left">
                                    <i class="fa fa-chevron-right color-black"></i>
                                </span>
                            </div>

                            <div class="divFileTags col-md-10 colpadding0" style="overflow-x: hidden; overflow-y: hidden; width: 92%;">
                                <asp:CheckBoxList ID="lstBoxFileTags" runat="server" RepeatDirection="Horizontal" TextAlign="Left"
                                    CssClass="list mt5 mb5" OnSelectedIndexChanged="lstBoxFileTags_SelectedIndexChanged" AutoPostBack="true">
                                </asp:CheckBoxList>
                            </div>
                        </div>


                    </div>
                    <%--Added by Ruchi--%>
         
                    <div class="clearfix" style="clear: both; height: 5px;"></div>

                    <div style="margin-bottom: 4px">
                        <asp:GridView runat="server" ID="grdMyDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                            PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="NoticeCaseInstanceID"
                            OnRowCommand="grdMyDocument_RowCommand" OnRowDataBound="grdMyDocument_RowDataBound" OnSorting="grdMyDocument_Sorting" OnRowCreated="grdMyDocument_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderText="Sr" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="center" SortExpression="TypeName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeName") %>' ToolTip='<%# Eval("TypeName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Ref No/Case No" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="RefNo">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RefNo") %>' ToolTip='<%# Eval("RefNo") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Title" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="Title">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="12%" HeaderStyle-HorizontalAlign="center">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                            <asp:Label runat="server" data-toggle="tooltip" Width="100%" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="center" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="25%" HeaderText="Select" HeaderStyle-HorizontalAlign="center">
                                    <ItemTemplate>
                                        <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DropDownListChosen runat="server" ID="ddlDocumentFile" class="form-control" Width="100%"
                                                    AllowSingleDeselect="false" DisableSearchThreshold="5">
                                                    <%--<asp:ListItem Text="All Document" Value="A" Selected="true" />
                                                    <asp:ListItem Text="Notice Document" Value="N" />
                                                    <asp:ListItem Text="Notice Responce Document" Value="NR" />
                                                    <asp:ListItem Text="Case Document" Value="C" />
                                                    <asp:ListItem Text="Case Hearing  Document" Value="CH" />
                                                    <asp:ListItem Text="CaseTask Document" Value="CT" />--%>
                                                </asp:DropDownListChosen>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="ddlDocumentFile" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoad" runat="server" ImageUrl="~/img/icon-download.png" CommandName="DownloadFile"
                                                    CommandArgument='<%# Eval("NoticeCaseInstanceID") %>'
                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Document(s)"></asp:ImageButton>
                                                <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/img/view-doc.png" CommandName="View"
                                                    CommandArgument='<%# Eval("NoticeCaseInstanceID") %>'
                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Document(s)"></asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDownLoad" />
                                                <asp:PostBackTrigger ControlID="lblViewfile" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerSettings Visible="false" />
                            <PagerTemplate>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>

                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                            DataKeyNames="TaskID" OnSorting="grdTaskActivity_Sorting" OnRowCreated="grdTaskActivity_RowCreated" OnRowCommand="grdTaskActivity_RowCommand">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" SortExpression="TaskType" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                            <asp:Label ID="lblblTaskType" runat="server" Text='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":"" %>'
                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType"))=="N"?"Notice" : "Case":""%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%" SortExpression="Priority">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%" SortExpression="TaskTitle">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%" SortExpression="TaskDesc">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%" SortExpression="ScheduleOnDate">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="15%" SortExpression="AssignToName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                            <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfile1" runat="server" ImageUrl="~/Images/download_icon_new.png" CommandName="Download"
                                                    CommandArgument='<%# Eval("TaskID") %>'
                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Document(s)"></asp:ImageButton>
                                                <asp:ImageButton ID="lblViewfile1" runat="server" ImageUrl="~/Images/Eye.png" CommandName="View"
                                                    CommandArgument='<%# Eval("TaskID") %>'
                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Document(s)"></asp:ImageButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDownLoadfile1" />
                                                <asp:PostBackTrigger ControlID="lblViewfile1" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate></PagerTemplate>
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-8 colpadding0">
                            <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                            <%--<div class="table-Selecteddownload">
                                <div class="table-Selecteddownload-text">
                                    <p>
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 15px;"></asp:Label>
                                    </p>
                                </div>
                            </div>--%>
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" />
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 colpadding0" style="float: right;">
                            <div style="float: left; width: 60%">
                                <p class="clsPageNo">Page</p>
                            </div>
                            <div style="float: left; width: 40%">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!--advance search starts-->
    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1200px">
            <div class="modal-content" style="height: 150px">

                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Filter(s)</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="clearfix"></div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 9%;">
                                <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                    DataPlaceHolder="Select Type" class="form-control" Width="90%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                                    <asp:ListItem Text="Notice" Value="N"></asp:ListItem>
                                    <asp:ListItem Text="Case" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Task" Value="T"></asp:ListItem>
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 9%;">
                                <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                    DataPlaceHolder="Select Status" class="form-control" Width="90%">
                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Submitted" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 15%;">
                                <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                    DataPlaceHolder="Select Type" class="form-control" Width="90%">
                                    <asp:ListItem Text="All" Value="B"></asp:ListItem>
                                    <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                                    <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 17%;">
                                <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                    DataPlaceHolder="Select Opponent" class="form-control" Width="90%" />
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 16%;">
                                <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                    DataPlaceHolder="Select Department" class="form-control" Width="90%" />
                            </div>

                            <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                <ContentTemplate>
                                    <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width: 18%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/Branch/Location" autocomplete="off" CssClass="clsDropDownTextBox" />
                                        <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                             <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; width: 14%;">
                               <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                     class="form-control" Width="70%">                       
                                </asp:DropDownListChosen>

                                <%--  <asp:ListBox ID="DropDownListChosen1" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage=" Financial Year can not be empty."
                                     ControlToValidate="DropDownListChosen1" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                               --%>
                             </div>

                        </div>
                    </div>
                    <div class="row mt10">
                        <div class="container" role="main">
                            <div class="col-md-12 center-block  text-center">

                                <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="LinkButton1" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply" OnClientClick="javascript:$('#updateProgress').show()" />

                                <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="lnkBtnClearFilter"
                                    OnClick="lnkBtnClearFilter_Click" OnClientClick="javascript:$('#updateProgress').show()" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--advance search ends-->
    <%--Viewer--%>
    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 95%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 12%">
                                <table width="100%" style="text-align: left; margin-left: 10%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upLitigationDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptLitigationVersionView" runat="server" OnItemCommand="rptLitigationVersionView_ItemCommand"
                                                            OnItemDataBound="rptLitigationVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th><b>File Name</b></th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <%#Container.ItemIndex+1 %>.
                                                                        <asp:UpdatePanel ID="UpLinkbuttonbold" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("NoticeCaseInstanceID") + ","+ Eval("FileName")+ ","+ Eval("DocTypeInstanceID") %>'
                                                                                    ID="lblDocumentVersionView" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName")%>'
                                                                                    Text='<%# Eval("FileName").ToString().Trim().Length > 18 ? Eval("FileName").ToString().Substring(0,18) + "..." : Eval("FileName").ToString().Trim() %>'></asp:LinkButton>

                                                                                    <%--Text='<%# Eval("FileName").ToString().Substring(0,20) + "..."%>'></asp:LinkButton>--%>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <%--<Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptLitigationVersionView" EventName="ItemCommand" />
                                                    </Triggers>--%>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: right; width: 87%; margin-left: 10px;">
                                <asp:UpdatePanel ID="updateButtonaa" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="docViewerLitigation" runat="server" width="100%" height="530px"></iframe>
                                        </fieldset>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
