﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="LitigationHelp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common.LitigationHelp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/Help.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <script type="text/javascript">
        ViewPannal();

        function ReadQuerySt(e) {
            
            try {
                for (hu = window.location.search.substring(1), gy = hu.split('&'), i = 0; i < gy.length; i++)
                    if (ft = gy[i].split('='), ft[0].toLowerCase() == e.toLowerCase()) return ft[1];
                return ''
            } catch (t) {
                return ''
            }
        }

        function fhead(val) {
            $('#pagetype').html(val);
            //  $('#sppagetype').html(val)

        }

        function ViewVideoPannal() {
            //alert("hiiiiiiiiiiiiii")
            $('.panel-body').css("display:block");
            $('#HelpVideo_Container').show();
            $('#HelpVideo_Container').css("display:block");
            $('.clsVideoClick').css('text-decoration', "underline");
            $('.clsHelpClick').css('text-decoration', "none");
            $('.clsHelpClick').css('color', "#000000");
            $('.clsVideoClick').css('color', "#1fd9e1");
            $('#videoPlayer_01,#videoPlayer_02,#videoPlayer_0').show();
            $('#Dashbord').hide();
            $('#Workspace').hide();
            $('#Reports').hide();
            $('#Documents').hide();
            $('#Reminders').hide();
            $('#Dashbord1').hide();
            // $('.container').hide();
        }

        function ViewPannal() {
            var PanelName = ReadQuerySt('id');
           
            if (PanelName == "My%20Dashboard") {
                $('#Dashbord').show();
                $('#Masters').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#CommonFAQs').hide();
            } 
            else if (PanelName.includes("My%20Workspace")) {
                $('#Dashbord').hide();
                $('#Masters').hide();
                $('#Workspace').show();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#CommonFAQs').hide();
            }
            else if (PanelName == "My%20Reports") {
                $('#Dashbord').hide();
                $('#Masters').hide();
                $('#Workspace').hide();
                $('#Reports').show();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#CommonFAQs').hide();
            }
            else if (PanelName == "My%20Documents") {
                $('#Dashbord').hide();
                $('#Masters').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').show();
                $('#Reminders').hide();
                $('#CommonFAQs').hide();
            }
            else if (PanelName == "My%20Reminders") {
                $('#Dashbord').hide();
                $('#Masters').hide();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').show();
                $('#CommonFAQs').hide();
            }
            else if (PanelName.includes("Masters")) {
                $('#Dashbord').hide();
                $('#Masters').show();
                $('#Workspace').hide();
                $('#Reports').hide();
                $('#Documents').hide();
                $('#Reminders').hide();
                $('#CommonFAQs').hide();
            }           
            else {
                $('#Dashbord').show();
                $('#Masters').show();
                $('#Workspace').show();
                $('#Reports').show();
                $('#Documents').show();
                $('#Reminders').show();
                $('#CommonFAQs').show();
            }
        }

        
    </script>

    <style type="text/css">
        li {
            font-size: 16px;
            list-style-position: outside;
            list-style: square;
            font-weight: 500;
        }

        body{
            font-family: 'Roboto',sans-serif;
        }

        div {
            text-align: justify;
        }
    </style>
</head>
<body>
    <div id="Help_MainContainer">
        <form id="form1" runat="server">

            <div id="Help_Container" class="container" style="color: #666; background-color: white; padding-left: 0px;">
                <div class="panel-body" id="CommonFAQs">
                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">Who can use Avantis Litigation Management Module?</h4>
                    </div>

                    <div style="color: #666; background-color: white;">Any user (internal or external) can use the system. The user will need to be configured from the user master along with the role that they will perform. Please refer to Master Configuration Section for additional details.</div>

                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">What roles can an user perform in the system?</h4>
                        <div style="color: #666; background-color: white;">Avantis Litigation Management Module has following user roles:</div>

                        <ul style="list-style-type: circle">
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Management</li>
                            <p style="color: #666666;">Able to view all the notices and legal cases along with history, documents, assigned / completed tasks, responses, next actions among others</p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Company Admin</li>
                            <p style="color: #666666;">Able to configure various masters such as User Master, Lawyers, Counter Parties, Courts etc and able to view all the notices and legal cases along with history, documents, assigned / completed tasks, responses, next actions among others</p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Non-Admin (Performer)</li>
                            <p style="color: #666666;">A performer can view and access all the notices, tasks, legal cases assigned to them. A performer can also create a new notice and/or legal case and can assign items / tasks to other users (internal and external)</p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">External User</li>
                            <p style="color: #666666;">These users typically are outside the firm and can be external consultants, lawyers / law firms, vendors etc, who may have to take actions / update status of various tasks assigned to them. They typically do not have credentials (user id and password) to access the system directly. They access the system using a secure link and a OTP (one time password) sent to their registered mobile number. </p>
                        </ul>
                    </div>

                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">How Can I Create Users in the system?</h4>
                    </div>

                    <div style="color: #666; background-color: white;">If you are already an existing user of Avantis Compliance Management Product, by default, the registered Users also available in Avantis Litigation Management Module for role assignment. </div>
                    <div style="color: #666; background-color: white; margin-top: 2%;">However, additional users can be manually created in Avantis Litigation Management Module by Management role or Company Admin role. Please refer to Master Configuration Section for additional details. </div>

                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">Is it possible to register external Lawyers or lawyer firm user with the litigation product and assigned them notice/case?</h4>
                    </div>

                    <div style="color: #666; background-color: white;">Yes, you can create users outside your organisation to use the system. They typically do not have credentials (user id and password) to access the system directly. They access the system using a secure link and a OTP (one time password) sent to their registered mobile number.</div>
                    <div style="color: #666; background-color: white; margin-top: 2%;">When you assign task(s) related to notice/case to external users (i.e. not registered user), they will get a secure access link in their registered mail. This mail ID is the same as registered in the system by the Company Admin role or Management role. To access the link, the system requires the external party to authenticate themselves by way of an OTP (one time password) which will be sent to their registered mobile number. The OTP will be valid for the period of 30 mins from the time of original request. If the OTP is not used within the time frame, it will expire. The user can however, close the window and request another OTP. The user can continue to access the page at wish and each time a new OTP will be generated. </div>
                    <div style="color: #666; background-color: white; margin-top: 2%;">The responses submitted / updates done by the external user will be reflected in the system against the appropriate notice / case.</div>
                    </div>

                <div class="panel-body" id="Masters" style="padding-left: 0px;">
                    <div>
                        <h1>Masters</h1>
                    </div>

                    <div style="color: #666; background-color: white;">
                        <h4 style="font-family: 'Roboto',sans-serif;">What are various masters available in the Avantis Litigation Management Module?</h4>
                        
                        <div style="color: #666; background-color: white;">The module is very flexible and offers high degree of configurability. The product has following masters:</div>
                        <ul style="list-style-type: circle">
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Organisation Structure </li>
                            <p style="color: #666666;">
                                You can map your entire organization including legal entities, sub-entities, locations etc in this master. <br />Please refer to the screenshot below for details.<br />
                                <asp:Image ImageUrl="~/Images/Litigation/BranchMaster.png" runat="server" Width="700" />
                            </p>
                           
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">User</li>
                            <p style="color: #666666;">
                                You can create different users and assign them roles by using this master. If you are already using Compliance Management Product, the users will be automatically available in the system for role assignment.
                                <asp:Image ImageUrl="~/Images/Litigation/userMaster.png" runat="server"  Width="700" />
                            </p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Law Firm</li>
                            <p style="color: #666666;">
                                You can create external law firms with external lawyers which will collaborate on any notice / legal case.
                                <asp:Image ImageUrl="~/Images/Litigation/LawyerFirmMaster.png" runat="server"  Width="700" />
                            </p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">Counterparty</li>
                            <p style="color: #666666;">
                                You can create party/opponent/counterparty for assignment to various cases / notices
                                <asp:Image ImageUrl="~/Images/Litigation/PartyMaster.png" runat="server"  Width="700" />
                            </p>

                             <li style="font-size: 16px; list-style-position: outside; list-style: square;">Courts </li>
                            <p style="color: #666666;">
                                You can configure different courts with required attributes which can be used in legal case/notice
                                <asp:Image ImageUrl="~/Images/Litigation/Court.png" runat="server" Width="700" />
                            </p>

                             <li style="font-size: 16px; list-style-position: outside; list-style: square;">Case/Notice Category</li>
                            <p style="color: #666666;">
                                You can create/edit different case categories under which your case/ notice can be classified
                                <asp:Image ImageUrl="~/Images/Litigation/CaseCategory.png" runat="server" Width="700" />
                            </p>

                            <li style="font-size: 16px; list-style-position: outside; list-style: square;font-weight: 500;">Payment</li>
                            <p style="color: #666666;">
                                you can create/update different payment/expense categories by using this master
                                <asp:Image ImageUrl="~/Images/Litigation/PaymentType.png" runat="server" Width="700" />
                            </p>
                        </ul>
                    </div>
                </div>

                <div class="panel-body" id="Dashbord" style="padding-left: 0px;">
                    <div>
                        <h1>My Dashboard</h1>
                    </div>

                    <ul>
                        <li style="font-size: 16px;">What Can I Do on My Dashboard?</li>
                        <p style="color: #666666;">
                            You can view all summarized data related to your legal notice/case in form of highly interactive (drill down capabilities) charts. You can see different types of charts as given below:
                        </p>
                        <ul style="list-style-type: circle">
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Case/Notice</li>
                            <p style="color: #666666;">Number of Case/Notice of Inward or Outward type</p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Case Stage</li>
                            <p style="color: #666666;">Number of Cases in different stages (i.e. Filing, Document Gathering etc.)</p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Winning Prospect</li>
                            <p style="color: #666666;">Number of Case/Notice by winning prospect (probability of winning) as input during case/notice creation </p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Department and Prospect of winning </li>
                            <p style="color: #666666;">Number of case/notice by different department in your organization by winning prospect</p>
                            <asp:Image ImageUrl="~/Images/Litigation/dashboard graph 1 - Case type.png" runat="server" Width="700" />
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Branch</li>
                            <p style="color: #666666;">Number of case/notice of different legal entities/sub-entities/branches</p>
                            <li style="font-size: 16px; list-style-position: outside; list-style: square;">By Case/Notice Category</li>
                            <p style="color: #666666;">Number of case/notice under different categories like criminal, financial, civil, labour etc.</p>
                            <asp:Image ImageUrl="~/Images/Litigation/Graph 2.png" runat="server" Width="700" />
                        </ul>

                        <li style="font-size: 16px;">Upcoming Hearing</li>
                        <p style="color: #666666;">
                            Here you can view upcoming case hearings
                        </p>

                        <li style="font-size: 16px;">Task</li>
                        <p style="color: #666666;">
                            You can view different task assigned to different users related to case hearing and notice responses and track the progress
                        </p>

                        <li style="font-size: 16px;">Summary</li>
                        <p style="color: #666666;">
                            Summary shows the numbers of notices, cases and their statuses.
                        </p>

                        <li style="font-size: 16px;">How Do I Use Filters?</li>
                        <p style="color: #666666;">
                            Filter options are available on my dashboard. You can able to filter dashboard graphs based on Entity/Branch/Location, Notice/Case, Notice/Case Type, Department, Party, Status, Winning Prospect.
                            You can use a single filter or a combination of the filters and click Search. You can also type keywords to search
                        </p>
                    </ul>

                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Generate Support Ticket
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="../../images/manager-support.png" kasperskylab_antibanner="on">
                            Communicate your issues or problems related to AVACOM through Support Ticket.
                        </p>
                    </ul>

                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Send Internal Emails
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="../../images/manager-email.png" kasperskylab_antibanner="on">
                            Send internal emails through AVACOM.                                
                        </p>
                    </ul>

                    <ul>
                        <li class="Help_font" style="font-size: 17px; list-style-position: outside; list-style: disc;">Chat Online
                        </li>
                        <p style="color: #666666;">
                            <img style="width: 80px; height: 80px" src="../../Images/chat2.PNG" kasperskylab_antibanner="on" />
                            Have a question or query? Chat with our experts. 
                        </p>
                    </ul>
                </div>

                <div class="panel-body" id="Workspace" style="padding-left: 0px;">
                    <div>
                        <h1>My Workspace</h1>
                    </div>
                    <ul>
                        <li style="font-size: 16px;">What is My Workspace?</li>
                        <p style="color: #666666;">
                            My Workspace typically will show you list of assigned notices / cases to the user. The user can create new cases / notices or update the details of existing notices / cases by clicking the action button available at the end of each notice / case.
                        </p>
                        <asp:Image ImageUrl="~/Images/Litigation/my workspace.png" runat="server" Width="700" />
                    </ul>
                </div>

                <div class="panel-body" id="Reports" style="padding-left: 0px;">
                    <div class="report_Heading">
                        <h1 class="Help_Heading">My Reports</h1>
                    </div>
                    <ul>
                        <li class="Help_font" style="font-size: 16px; line-height: 20px!important;">What Can I Do or See in My Reports?</li>
                        <p style="color: #666666;">
                            All the notice/case items assigned to can be viewed in My Reports. You can export the report to Excel.
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Use Filters?</li>
                        <p style="color: #666666;">
                            Filter options are available on My Reports. You can use a single filter or a combination of the filters and click apply filter.
                        </p>
                        <asp:Image ImageUrl="~/Images/Litigation/my reports.png" runat="server" Width="700" />
                    </ul>
                </div>

                <div class="panel-body" id="Documents" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading">My Documents</h1>
                    </div>
                    <p style="color: #666666;">All previously uploaded <i>notice / case documents </i>and <i>task / notice response / case hearing</i> orders can be viewed in My Documents.</p>
                    <p style="color: #666666;">
                        You can download all documents that you have access to, by selecting appropriate options and clicking <u>Download</u>.
                    </p>
                    <asp:Image ImageUrl="~/Images/Litigation/my Documents.png" runat="server" Width="700" />
                </div>

                <div class="panel-body" id="Reminders" style="padding-left: 0px;">
                    <div>
                        <h1 class="Help_Heading">My Reminders</h1>
                    </div>
                    <ul>
                        <li class="Help_font" style="font-size: 16px;">What Can I Do or See in My Reminders?
                    
                        </li>
                        <p style="color: #666666;">
                            You can view all the reminders which is created by you . You can able to create / edit reminders for the different notice / case / task assigned to you. The defined reminders will be sent on two(2) days before and on scheduled date.
                        </p>

                        <li class="Help_font" style="font-size: 16px; padding-top: 2%;">How Do I Use Filters?</li>
                        <p style="color: #666666;">
                            Two filter options are available on My Reminders. You can use a single filter or a combination of the filters and click apply filter
                        </p>
                        <li class="Help_font" style="font-size: 16px;">How Do I Add New Reminder?</li>
                        <p style="color: #666666;">
                            You can add new reminders by clicking Add New and choosing the Type (notice / case/ task), this selection will populate list of title (notice / case/ task) assigned to you, reminder, reminder description. Select a Remind On date and Save.
                        </p>
                    </ul>
                </div>
            </div>

        </form>
    </div>
</body>
</html>
