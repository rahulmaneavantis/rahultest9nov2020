﻿ <%@ Page Title="Product List" Language="C#" AutoEventWireup="true" CodeBehind="ProductMappingStructure.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping.ProductMappingStructure" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="https://avacdn.azureedge.net/newcss/jqgrids.css" rel="stylesheet" type="text/css" />
    <link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />
    <script src="https://avacdn.azureedge.net/newjs/jqgrids.js" type="text/javascript"></script>
    <!-- Bootstrap CSS -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript">

         var _paq = window._paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {
            debugger;
            try {
                _paq.push(['trackEvent', e, t, n])
            } catch (t) { } return !0
        }


        function btnInternalControldoClick() {
            document.getElementById("<%= btnInternalControl.ClientID %>").click();
        }
        function btnAuditdoClick() {
            document.getElementById("<%= btnAudit.ClientID %>").click();
        }
        function btnCompliancedoClick() {
            document.getElementById("<%= btnCompliance.ClientID %>").click();
        }
        function btnLitigationdoClick() {
            document.getElementById("<%= btnLitigation.ClientID %>").click();
        }
        function btnContractdoClick() {
            document.getElementById("<%= btnContract.ClientID %>").click();
        }
        function btnLicensedoClick() {
            document.getElementById("<%= btnLicense.ClientID %>").click();
        }
        function btnVendordoClick() {
            document.getElementById("<%= btnVendor.ClientID %>").click();
        }
        function btnSecretarialClick() {
            document.getElementById("<%= btnSecretarial.ClientID %>").click();
        }
        function btnHRProductdoClick() {
            document.getElementById("<%= btnHRProduct.ClientID %>").click();
        }
        function dvbtnPracticeMgmtdoClick() {
            document.getElementById("<%= btnPracticeMgmt.ClientID %>").click();
        }
        
        function fclearcookie() {
            settracknew('Login', 'Logout', 'Product', '');
            window.location.href = "/logout.aspx"
            return false;
        }
      
        if ($('#cid').val() == '914') {
            $('#imgc').attr('src', '../Images/zomatologo.jpg');
        }
    </script>
    <script type="text/javascript">
            function OpenPopup() {
                $('#DivReports').modal('show');
                $('#showdetails').attr('width', '1250px');
                $('#showdetails').attr('height', '600px');
                $('.modal-dialog').css('width', '1100px');
                $('#showdetails').attr('src', "../../RiskManagement/AuditTool/EndUserLicenseAgreement.html");
            }

            function closePopup() {
                debugger;
                $('#DivReports').modal('hide');
            }
    </script>
    <style>
        input#dvbtnInternalControl:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnAudit:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnCompliance:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnLicense:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }

        input#dvbtnHRProduct:hover {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            cursor: pointer;
        }
    </style>

    <style>
        body {
            font-family: arial;
        }

        .clsMain {
        }

        #head1 {
            font-size: 43px;
            font-family: arial;
            color: #666666;
        }

        #head2 {
            font-size: 54px;
            font-weight: bold;
            font-family: arial;
            color: #666666;
        }

        .clsSqMain {
        }

        #squre1 {
            background: #1FD9E1;
        }

        #sqTxt1 {
            color: #ffffff;
            text-align: center;
        }

        #dvbtnCompliance1, #squre2, #squre3 {
            float: left;
            width: 200px;
            height: 80px;
            border: 2px solid #1FD9E1;
            background: #1FD9E1;
            margin-right: 10px;
            Color: white;
            font-weight: bold;
        }

        #squre2 {
            left: 20%;
        }

        #squre3 {
            left: 40%;
        }

        #sqTxt2, #sqTxt3 {
            text-align: center;
        }

        #sqTxt1, #sqTxt2 {
            padding-top: 16%;
        }

        #sqTxt3 {
            padding-top: 15%;
        }

        #inner {
            display: table;
            margin: 0 auto;
            width: 75%;
            text-align: center;
        }

        #innerHead {
            display: table;
            margin: 0 auto;
            width: 75%;
            text-align: center;
        }

        #innerHead1 {
            display: table;
            margin: 0 auto;
            width: 75%;
            text-align: center;
        }

        .btn {
            padding: 12px 12px 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%;">
            <asp:Label ID="lblErrMsg" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
        </div>
        <div class="clsMain" style="padding-top: 1%;">
            <div id="innerHead">
                <%--<center>--%>
                <img src="../Images/avantisLogo.png" align="BOTTOM" />
                <div id="head1">Welcome to</div>
                <div id="head2">AVANTIS</div>
                <%--</center>--%>
            </div>
        </div>

        <div class="clsSqMain" style="padding-top: 1%;">
            <div id="inner">
                <div id="dvbtnCompliance" runat="server" class="btn btn-primary" onclick="btnCompliancedoClick()" style="width: 30%;">
                    <div class="imgsimple text-center">
                        <img src="../Images/Compliance.png" alt="Legal Compliance" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Legal Compliance</span></div>
                </div>
                <asp:Button ID="btnCompliance" runat="server" OnClientClick="settracknew('Login','ProductSelectiion','Compliance','')" Text="Legal Compliance" Style="display: none" 
                    OnClick="btnCompliance_Click" Width="290px" Height="120px" BackColor="#1D6DC1" Font-Bold="true" ForeColor="White" Font-Size="Large" />

                <div id="dvbtnInternalControl" runat="server" class="btn btn-primary" onclick="btnInternalControldoClick()" style="width: 30%; margin-left: 3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/Internal_financial_control.png" alt="Internal Financial Control (IFC)" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Internal Financial Control (IFC)</span></div>
                </div>
                <asp:Button ID="btnInternalControl" runat="server" Text="Internal Financial Control (IFC)" Style="display: none" Width="290px" Height="120px" BackColor="#C2185B" Font-Bold="true" ForeColor="White" Font-Size="Large" OnClick="btnInternalControl_Click" />

                <div id="dvbtnAudit" runat="server" class="btn btn-primary" onclick="btnAuditdoClick()" style="width: 30%; margin-left: 3%;">
                    <div class="imgsimple text-center">
                        <img src="../Images/Audit_control.png" alt="Audit Management" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Audit Management</span></div>
                </div>
                <asp:Button ID="btnAudit" runat="server" Text="Audit Management" Style="display: none" Width="290px" Height="120px" BackColor="#FC730A" Font-Bold="true" ForeColor="White" Font-Size="Large" OnClick="btnAudit_Click" />

                <div id="dvbtnLitigation" runat="server" class="btn btn-primary" onclick="btnLitigationdoClick()" style="width: 30%; margin-left: 3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/Litigation.png" alt="Litigation Management" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Litigation</span></div>
                </div>
                <asp:Button ID="btnLitigation" runat="server" Text="Litigation Management" OnClientClick="settracknew('Login','ProductSelectiion','Litigation','')" Style="display: none" OnClick="btnLitigation_Click" Width="290px" Height="120px" BackColor="#008fd5" Font-Bold="true" ForeColor="White" Font-Size="Large" />            
            
                <div id="dvbtnContract" runat="server" class="btn btn-primary" onclick="btnContractdoClick()" style="width: 30%;margin-left:3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/contract.png" alt="Contract Management" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Contract</span></div>
                </div>
                <asp:Button ID="btnContract" runat="server" Text="Contract Management" OnClientClick="settracknew('Login','ProductSelectiion','Contract,'')" Style="display: none" OnClick="btnContract_Click" Width="290px" Height="120px" BackColor="#008fd5" Font-Bold="true" ForeColor="White" Font-Size="Large" />            
            
                <div id="dvbtnLicense" runat="server" class="btn btn-primary" onclick="btnLicensedoClick()" style="width: 30%;margin-left:3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/lic_icon_76px.png" alt="License Management" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">License</span></div>
                </div>
                <asp:Button ID="btnLicense" runat="server" OnClientClick="settracknew('Login','ProductSelectiion','License','')" Text="License Management" Style="display: none" OnClick="btnLicense_Click" Width="290px" Height="120px" BackColor="#008fd5" Font-Bold="true" ForeColor="White" Font-Size="Large" />            


                <div id="dvbtnVendor" runat="server" class="btn btn-primary" onclick="btnVendordoClick()" style="width: 30%; margin-left: 3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/lic_icon_76px.png" alt="Vendor Management" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Vendor Management</span></div>
                </div>
                <asp:Button ID="btnVendor" runat="server" Text="Vendor Management" Style="display: none" OnClick="btnVendor_Click" Width="290px" Height="120px" BackColor="#008fd5" Font-Bold="true" ForeColor="White" Font-Size="Large" />

                <div id="dvbtnSecretarial" runat="server" class="btn btn-primary" onclick="btnSecretarialClick()" style="width: 30%; margin-left: 3%;">
                    <div class="imgdouble text-center">
                        <img src="../Images/lic_icon_76px.png" alt="Secretarial Compliance" />
                    </div>
                    <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Secretarial Compliance</span></div>
                </div>
                <asp:Button ID="btnSecretarial" runat="server" Text="Secretarial Compliance" Style="display: none" OnClick="btnSecretarial_Click" Width="290px" Height="120px" BackColor="#008fd5" Font-Bold="true" ForeColor="White" Font-Size="Large" />

                <div id="dvbtnHRProduct" runat="server" class="btn btn-primary" onclick="btnHRProductdoClick()" style="width: 30%; margin-left: 3%;">
                <div class="imgsimple text-center">
                    <img src="../Images/Compliance.png" alt="Labour Compliance" />
                </div>
                <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Labour Compliance</span></div>
                </div>
                <asp:Button ID="btnHRProduct" runat="server" OnClientClick="settracknew('Login','ProductSelectiion','HRProduct','')" Text="Labour Compliance" Style="display: none" OnClick="btnHRProduct_Click" Width="290px" Height="120px" BackColor="#1D6DC1" Font-Bold="true" ForeColor="White" Font-Size="Large" />

                 <div id="dvbtnPracticeMgmt" runat="server" class="btn btn-primary" onclick="dvbtnPracticeMgmtdoClick()" style="width: 30%; margin-left: 3%;">
                <div class="imgsimple text-center">
                    <img src="../Images/Compliance.png" alt="Labour Compliance" />
                </div>
                <div class="metro-destaque-rodape undefined"><span style="font-size: large;">Practice Management</span></div>
                </div>
                <asp:Button ID="btnPracticeMgmt" runat="server" OnClientClick="settracknew('Login','ProductSelectiion','Practice Management','')" Text="Practice Management" Style="display: none" OnClick="btnPracticeMgmt_Click" Width="290px" Height="120px" BackColor="#1D6DC1" Font-Bold="true" ForeColor="White" Font-Size="Large" />

            </div>
        </div>
        <div style="text-align: center; margin-top: 3%;">
            <asp:LinkButton ID="lnkLogOut" OnClientClick="javascript:return fclearcookie();" runat="server">Or sign in as a different user</asp:LinkButton>
        </div>
        <asp:HiddenField runat="server" ID="cid" />
        <div class="modal fade" id="DivReports" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="display: none;text-align: center;position: fixed;top: 51%;left: 50%;transform: translate(-52%, -50%);width: 50%;overflow-x: hidden;height: 100%;overflow-y: hidden;">
            <div class="modal-dialog" style="width: 90%;">
                <div class="modal-content" style="width: 60%;">

                    <div class="table-responsive" style="width: 60%;">
                        <iframe id="showdetails" src="about:blank" style="width:165% !important" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
