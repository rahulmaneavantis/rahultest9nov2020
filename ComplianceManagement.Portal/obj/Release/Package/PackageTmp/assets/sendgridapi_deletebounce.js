function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
}

function delete_record(){

	var i = 0;
	var count = 0;
	var emails = [];
	var t = document.getElementById('response2');
	$('#response2 tr').each(function(){    
		$(this).find('input[type="checkbox"]:checked').each(function () {
			emails.push(this.id)
			count += 1;
	 });
	})
	if(count == 0){
		alert("Please select one or more emails to delete");
		return false;
	}
	if (confirm('Are you sure you want to permanently delete ' + count +' emails?')) {	
			var data = JSON.stringify({
				"emails": emails
			});
			var url = "https://api.sendgrid.com/v3/suppression/bounces";

			//console.log(url);
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
				if (this.readyState === this.DONE) {
					//console.log(this.responseText);
					if (this.responseText === ""){
					alert("Email(s) successfully Deleted");
					$('#loader').show();
					getBounces();
					}
				}
			});

			
				xhr.open("DELETE", url);
				xhr.setRequestHeader("authorization", "Bearer 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");
				xhr.setRequestHeader("content-type", "application/json");

				xhr.send(data);
 
}
	else{
		return false;
	}
}