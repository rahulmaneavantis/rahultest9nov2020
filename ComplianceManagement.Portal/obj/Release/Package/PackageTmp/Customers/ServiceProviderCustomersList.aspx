﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceProviderCustomersList.aspx.cs"
    MasterPageFile="~/Compliance.Master" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.ServiceProviderCustomersList" %>

<%@ Register Src="~/Controls/CustomerDetailsControl.ascx" TagName="CustomerDetailsControl1"
    TagPrefix="vit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .HideClass
        {
            display:none;
        }
    </style>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .modal-body label {
            padding: 5px;
        }
    </style>
    <script type="text/javascript">
        function RefreshPage() {
            debugger;
            location.reload();
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div runat="server" id="testDiv" style="float: right; margin-top: 1px; margin-right: 8px; display: none;">
                <asp:LinkButton runat="server" ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click" Visible="false"><img src="../Images/excel.png" alt="Export to Excel" 
        title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
            </div>


            <table width="100%">
                <tr>
                    <div style="display: none;">
                        <asp:TextBox ID="txthiddenServiceProviderIdForReport" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtHideReportButton" runat="server"></asp:TextBox>
                    </div>
                    <td align="right" class="pagefilter" style="width: 15px; height: 25px;">
                        <div id="divFilter" runat="server">
                            Filter :
                       
                            <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                        </div>
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCustomer" Visible="false" OnClick="btnAddCustomer_Click" />
                    </td>
                </tr>
            </table>
            <div style="margin-bottom: 2px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="linkbtnServiceProviderName" runat="server" Style="color: Black; font-size: 12pt; text-decoration: underline;" OnClientClick="RefreshPage()" /> <%--OnClick="linkbtnServiceProviderName_Click"--%>
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="15" Width="100%" OnRowCommand="grdCustomer_RowCommand"
                OnRowDataBound="grdCustomer_RowDataBound" OnPageIndexChanging="grdCustomer_PageIndexChanging"
                Font-Size="12px" DataKeyNames="ID">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" Visible="false">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCustomer" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Height="20px" ItemStyle-Height="25px" SortExpression="Name" />
                    <asp:BoundField DataField="BuyerName" HeaderText="Buyer Name" SortExpression="BuyerName" />
                    <asp:BoundField DataField="BuyerContactNumber" HeaderText="Buyer Contact" ItemStyle-HorizontalAlign="Center" SortExpression="BuyerContactNumber" />
                    <asp:BoundField DataField="BuyerEmail" HeaderText="Buyer Email" SortExpression="BuyerEmail" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" SortExpression="StartDate" Visible="false">
                        <ItemTemplate>
                            <%# Eval("StartDate") != null ? ((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy")  : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center" SortExpression="EndDate" Visible="false">
                        <ItemTemplate>
                            <%# Eval("EndDate") != null ? ((DateTime)Eval("EndDate")).ToString("dd-MMM-yyyy") : " "%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DiskSpace" HeaderText="Disk Space" SortExpression="DiskSpace" Visible="false" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Customer">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") + "," + Eval("Name")%>'>Customers</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER" CommandArgument='<%# Eval("ID")  + "," + Eval("Name")%>'><img src="../Images/edit_icon.png" alt="Edit" title="Edit" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="VIEW_CUSTOMER" CommandArgument='<%# Eval("ID")  + "," + Eval("Name")%>'><img src="../Images/View-icon-new.png" alt="View Details" title="View Details" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID")  + "," + Eval("Name")%>'
                                OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../Images/delete_icon.png" alt="Disable Customer" title="Disable Customer" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
               
                </EmptyDataTemplate>
            </asp:GridView>
            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right; float: right; display: none;">
                <asp:Button Text="Notify Software Update" runat="server" ID="btnSendNotification" Width="100%" ToolTip="Click here to send server down notifications."
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div id="divNotificationTime">
        <asp:UpdatePanel ID="upServerDownTimeDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="txtDate" Style="height: 16px; width: 200px;" ReadOnly="true" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Date can not be empty." ControlToValidate="txtDate"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Time
                       
                        </label>
                        <asp:DropDownList runat="server" ID="ddlStartTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select start time." ControlToValidate="ddlStartTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                        To
                       
                        <asp:DropDownList runat="server" ID="ddlEndTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select end time." ControlToValidate="ddlEndTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="button"
                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divNotificationTime').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divModifyDepartment">
        <asp:UpdatePanel ID="upModifyDepartment" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <table style="width: 100%;">
                        <tr>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                    ValidationGroup="CustomerValidationGroup1" />
                                <asp:CustomValidator ID="CustomServiceProviderCustomerLimit" runat="server" EnableClientScript="False"
                                    ValidationGroup="CustomerValidationGroup1" Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px">
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    Service Provider Name</label>
                                <asp:Label runat="server" ID="lblServiceProviderName" Style="height: 16px; width: 390px;" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    Customer Limit</label>
                                <asp:TextBox runat="server" ID="txtCustomerLimit" Style="height: 16px; width: 390px;" MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please enter customer limit.."
                                    ControlToValidate="txtCustomerLimit" runat="server" ValidationGroup="CustomerValidationGroup1"
                                    Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    User Limit</label>
                                <asp:TextBox runat="server" ID="txtUserLimit" Style="height: 16px; width: 390px;" MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please enter user limit."
                                    ControlToValidate="txtUserLimit" runat="server" ValidationGroup="CustomerValidationGroup1"
                                    Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    Parent Branch Limit</label>
                                <asp:TextBox runat="server" ID="txtParentBranchLimit" Style="height: 16px; width: 390px;" MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please enter parent branch limit.."
                                    ControlToValidate="txtParentBranchLimit" runat="server" ValidationGroup="CustomerValidationGroup1"
                                    Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    Branch Limit</label>
                                <asp:TextBox runat="server" ID="txtBranchLimit" Style="height: 16px; width: 390px;"
                                    MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please enter branch limit."
                                    ControlToValidate="txtBranchLimit" runat="server" ValidationGroup="CustomerValidationGroup1"
                                    Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 230px; display: block; float: left; font-size: 13px; color: #333;">
                                    Employee Limit</label>
                                <asp:TextBox runat="server" ID="txtEmployeeLimit" Style="height: 16px; width: 390px;"
                                    MaxLength="15" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please enter employee limit.."
                                    ControlToValidate="txtEmployeeLimit" runat="server" ValidationGroup="CustomerValidationGroup1"
                                    Display="None" />
                            </div>
                        </tr>
                        <tr>
                            <div style="display: none;">
                                <asp:TextBox ID="txthiddenServiceProviderId" runat="server"></asp:TextBox>
                            </div>
                        </tr>
                        <tr>
                            <td colspan="4" style="margin-bottom: 7px;">
                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <div style="margin-bottom: 7px; margin-top: 10px; margin-left: 25%;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click"
                        CssClass="button" ValidationGroup="CustomerValidationGroup1" />
                    <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divModifyDepartment').dialog('close');" />
                </div>
                </div>
           
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        $(function () {
            $('#divNotificationTime').dialog({
                height: 300,
                width: 500,
                autoOpen: false,
                showOn: true,
                draggable: true,
                title: "Software Update Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        $(function () {
            $('#divModifyDepartment').dialog({
                height: 500,
                width: 580,
                top: 100,
                autoOpen: false,
                draggable: true,
                title: "Customer Limit",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeDowntimeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                minDate: startDate
            });

            if (date != null) {
                $("#<%= txtDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            alert(headerchk);
            var rolecolumn;

            var chkheaderid = headerchk.id.split("_");

            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }


    </script>

    <vit:CustomerDetailsControl1 runat="server" ID="udcInputForm" />
</asp:Content>
