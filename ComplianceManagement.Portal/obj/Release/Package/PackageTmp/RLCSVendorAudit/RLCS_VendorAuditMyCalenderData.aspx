﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="RLCS_VendorAuditMyCalenderData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCS_VendorAuditMyCalenderData" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<head runat="server">
    <title></title>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" />    
    <script type="text/javascript" src="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <link href="/avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript">
        function initializeCombobox() {
        }

    </script>

    <script type="text/javascript">
        function fopenmsgCancelschedule() {
            alert('Unable to cancel this audit it is in process');
        }


        function openModal() {
            $('#AuditPopUp').modal('show');
            return true;
        }

        function fopenpopup(flag) {
            if (flag == 0) {
                alert('Select End Date');
            } else if (flag == 5) {
                alert('Value is not a DateTime');
            } else if (flag == 4) {
                alert('End date cannot be less than start date');
            } else if (flag == 3) {
                alert('End date cannot be start date');
            }
            BindControls();
        }

        function FRemarkUpdate() {
            alert('Audit Cancellation Save Successfully.');
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        function FRescheduleUpdate() {
            alert('Audit Reschedule Save Successfully.');
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        function FRescheduledateValidation() {
            alert('End Date should be greater than Start Date.');
            BindRescheduleDate();
        }


        function FRescheduleValidation() {
            alert('Audit Reschedule not Save Successfully.');
            BindRescheduleDate();
        }
        function FAlreadyValidation() {
            alert('Audit Already Rescheduled for selected period.');
            BindRescheduleDate();
        }

        function OpenEdit() {

        }
        function fopenpopupUpdateStatus(location, period, AuditID, ScheduleOnId) {
         
            window.parent.$('#ContentPlaceHolder1_lblLocation').text(location);
            window.parent.$('#ContentPlaceHolder1_lblPeriod').text(period);
            window.parent.$('#ContentPlaceHolder1_HiddenFieldAuditId').val(AuditID);
            window.parent.$('#ContentPlaceHolder1_HiddenFieldScheduleOnId').val(ScheduleOnId);

            window.parent.$('#divOpenUpdateStatusPopup').modal('show');
        }
        function fopenpopupReschedule(location, period, AuditID, ScheduleOnId) {
            BindRescheduleDate();
            window.parent.$('#ContentPlaceHolder1_lblLocation1').text(location);
            window.parent.$('#ContentPlaceHolder1_lblPeriod1').text(period);
            window.parent.$('#ContentPlaceHolder1_HiddenFieldAuditId1').val(AuditID);
            window.parent.$('#ContentPlaceHolder1_HiddenFieldScheduleOnId1').val(ScheduleOnId);
            window.parent.$('#divOpenUpdateReschedulePopup').modal('show');
        }

        function OpenPopupUploadExcel()
        {
            window.parent.$('#ContentPlaceHolder1_ExcelFileUpload').val('');
            window.parent.$('#divUploadExcelPopup').modal('show');
        }

        function Editpopup(AuditID, AuditScheduleOnID, VendorName) {
            debugger;
            window.parent.location = '/RLCSVendorAudit/RLCSVendorAuditChecklist.aspx?AID=' + AuditID + '&SOID=' + AuditScheduleOnID + '&VendorName=' + VendorName;
        }
        function ReassignPopup(AuditID, CustomerID, UserID, Role, Period, AuditScheduleOnID) {
            window.parent.location = '/RLCSVendorAudit/RLCSVendorAuditReassignAudit.aspx?AID=' + AuditID + '&CustID=' + CustomerID + '&UserID=' + UserID + '&Role=' + Role + '&Period=' + Period + '&AuditScheduleOnID=' + AuditScheduleOnID;
        }
    </script>

    <script language="javascript" type="text/javascript">

        //sandesh code start
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
        //added by sandesh end

    </script>
    <script>
        function LocationToggle() {
            $('#divFilterLocation').toggle();
        }
        $(document).ready(function () {
            BindControls();
            BindRescheduleDate();
            //$('#tbxFilterLocation').click(function () {
            //    debugger;
            $('#divFilterLocation').toggle();
            // })
        });
        $('#divFilterLocation').hide();

        function BindControls() {
         
            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }
        function BindRescheduleDate() {
            $(function () {
                $('input[id*=txtStartDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'd-M-yy'
                    });
            });
        }



    </script>
    <script type="text/javascript">


        function CalenderPopPup(obj) {
            parent.OpenPerrevpopup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('Interimdays'), $(obj).attr('CType'), $(obj).attr('RoleID'), $(obj).attr('data-date'));
        }

        function fComplianceOverview(obj) {
            parent.OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('CType'));
        }

        $(document).ready(function () {
            $(".nav a").on("click", function () {
                $(".nav").find(".active").removeClass("active");
                $(this).parent().addClass("active");
            });
            //$('#loaderdiv').hide();


        });

        function fredv(type) {
            if (type == "P") {
                $('#liPerformer1').addClass('active');
                $('#liReviewer1').removeClass('active');
                $('#reviewerCalender').removeClass('active');
                $('#performerCalender').addClass('active');
            } else if (type == "R") {
                $('#liPerformer1').removeClass('active');
                $('#liReviewer1').addClass('active');
                $('#reviewerCalender').addClass('active');
                $('#performerCalender').removeClass('active');
            }
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $("[id*=tvBranches] input[type=checkbox]").bind("click", function () {
                var table = $(this).closest("table");
                if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                    //Is Parent CheckBox
                    var childDiv = table.next();
                    var isChecked = $(this).is(":checked");
                    $("input[type=checkbox]", childDiv).each(function () {
                        if (isChecked) {
                            $(this).attr("checked", "checked");
                        } else {
                            $(this).removeAttr("checked");
                        }
                    });
                } else {
                    //Is Child CheckBox
                    var parentDIV = $(this).closest("DIV");
                    if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                        $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                    } else {
                        $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                    }
                }
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        })
    </script>
    <style>
        .nowrap{
            white-space:nowrap;
        }
        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding: 6px;
            line-height: 1.428571429;
        }
        .mr-10 {
            margin-left: 10px;
        }

        .filterlocation {
            overflow: auto;
            border-left: 1px solid #c7c7cc;
            border-right: 1px solid #c7c7cc;
            border-bottom: 1px solid #c7c7cc;
            background-color: #ffffff;
            color: #8e8e93 !important;
        }

        .labelTohide {
            display: none;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        body {
            background: none !important;
        }

        .table {
            width: 100%;
            text-align: left;
        }



            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 1px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 1px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 1px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .table, pre.prettyprint {
            margin-bottom: 3px !important;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .today > a {
            background-color: #ccc;
            font-weight: 700;
        }
          #grdCompliancePerformer {
            table-layout: fixed;
            text-overflow: ellipsis;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
        }

        #grdCompliancePerformer tr td {
            max-width: 100px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .Srno{
            width: 36px;
        }
        input.aspNetDisabled {
            width: 100px;
        }
    </style>
</head>

<body>
    <form runat="server" id="f1">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
            OnLoad="upComplianceTypeList_Load">
            <ContentTemplate>
                <div class="row colpadding0">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                         <asp:DropDownListChosen runat="server" ID="ddlCustomer"
                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                        OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                        DataPlaceHolder="Select Customer"
                        class="form-control" Width="100%"  Enabled="true"
                        AutoPostBack="true" />
                    </div>
                     <div class="col-lg-2 col-md-2 col-sm-2">
                         <asp:DropDownListChosen runat="server" ID="ddlPeriod"
                        AllowSingleDeselect="false" DisableSearchThreshold="5"
                        OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged"
                        DataPlaceHolder="Select Period"
                        class="form-control" Width="100%" Enabled="true"
                        AutoPostBack="true" />
                        </div>
                     <div class="col-lg-3 col-md-3 col-sm-3">
                         <div id="FilterLocationdiv" runat="server" style="">

                        <asp:TextBox runat="server" ID="tbxFilterLocation" placeholder="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Entity/State/Location"
                            CssClass="form-control" autocomplete="off" />
                        <div style="position: absolute; z-index: 10;" id="divFilterLocation">
                            <asp:TreeView runat="server" ID="tvFilterLocation" CssClass="filterlocation" BackColor="White"
                                BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                                Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                            </asp:TreeView>

                            <div id="bindelement" style="background: white; height: 292px; display: none; width: 50px; border: 1px solid; overflow: auto;"></div>

                            <asp:Button ID="btnlocation" runat="server" Text="Select" OnClick="btnlocation_Click" />
                            <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />

                        </div>
                    </div>
                    </div>
                     <div class="col-lg-3 col-md-3 col-sm-3">
                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search asp-btn" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                         <asp:Button ID="btnClearFilters" CausesValidation="false" class="btn btn-search asp-btn mr-10" runat="server" Text="Clear" OnClick="btnClearFilters_Click" />
                       <asp:LinkButton ID="LnkTreeBtn" OnClick="tvFilterLocation_SelectedNodeChanged" Style="float: right; display: none;" Width="100%" runat="server">
                        </asp:LinkButton>
                          <button type="button" id="btnUpload" class="btn btn-primary" style="margin-left: 14px;" onclick="OpenPopupUploadExcel()">Upload</button>
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 " style="padding-left: 1px;">

                    <div class="tab-content" style="padding-top: 2px">
                        <div role="tabpanel" class="tab-pane" runat="server" id="performerCalender">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <asp:GridView runat="server" ID="grdCompliancePerformer" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                        GridLines="None" PageSize="5" OnPageIndexChanging="grdCompliancePerformer_PageIndexChanging" OnRowCommand="grdCompliancePerformer_RowCommand" OnRowUpdated="grdCompliancePerformer_RowUpdated"
                                        CssClass="table" AllowPaging="True" OnRowDataBound="grdCompliancePerformer_RowDataBound">

                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr" HeaderStyle-CssClass="Srno text-center"> 
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" Visible="false">
                                                <ItemTemplate>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAuditID" runat="server" CssClass="labelTohide" Text='<%# Eval("AuditID") %>'></asp:Label>
                                                    <asp:Label ID="lblAuditScheduleOnID" CssClass="labelTohide" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                    <asp:Label ID="lblAuditStatusID" CssClass="labelTohide" runat="server" Text='<%# Eval("AuditStatusID") %>'></asp:Label>
                                                    <asp:Label ID="lblFormMonth" CssClass="labelTohide" runat="server" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                    <asp:Label ID="lblCustomer" CssClass="labelTohide" runat="server" Text='<%# Eval("Customer") %>'></asp:Label>
                                                    <asp:Label ID="lblStatus" CssClass="labelTohide" runat="server" Text='<%# Eval("ComplianceStatus") %>'></asp:Label>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("Location") %>' ToolTip='<%# Eval("Location") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label ID="LabeVendorName" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("VendorName") %>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="SPOC">
                                                <ItemTemplate>
                                                        <asp:Label ID="lblgridSpoc" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("SPOCName") %>' ToolTip='<%# Eval("SPOCName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Date">
                                                <ItemTemplate>
                                                    <div style="width: 80px;">
                                                        <asp:Label ID="lblStartDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("StartDate")== DBNull.Value ? "": Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy")%>'
                                                            ToolTip='<%# Eval("StartDate")== DBNull.Value ? "" : Convert.ToDateTime(Eval("StartDate")).ToString("dd-MMM-yyyy")%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="End Date">
                                                <ItemTemplate>
                                                    <div style="">
                                                        <asp:HiddenField ID="hdnEndDate" runat="server" />
                                                        <asp:TextBox ID="txtEndDate" class="form-control endate" runat="server"
                                                            Text='<%# Eval("EndDate")!= null ?  Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : ""%>'>
                                                        </asp:TextBox>

                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Period" ItemStyle-CssClass="nowrap">
                                                <ItemTemplate>
                                                        <asp:Label ID="lblgridPeriod" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                            Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="20%" HeaderStyle-CssClass="text-left" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>


                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton runat="server" CommandName="CHANGE_EDIT" ID="btnChangeStatus" ToolTip="Edit" OnClientClick="fopenpopup()"
                                                                CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") + "," + Eval("VendorName") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Save End Date" />
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="lnkSave" runat="server" ToolTip="Save" data-toggle="tooltip"
                                                                CommandName="CHANGE_SAVE" CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/Save-icon.png")%>' alt="Save End Date" /></asp:LinkButton>

                                                            <asp:LinkButton ID="lnkCancel" runat="server" ToolTip="Change Status" data-toggle="tooltip"
                                                                CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("AuditID") + "," + Eval("AuditScheduleOnID") + "," + Eval("Location")  + "," + Eval("ForMonth") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change For Cancel" /></asp:LinkButton>

                                                           
                                                                <asp:LinkButton ID="lnkReassignAudit" runat="server" CommandName="ReassignAudit" ToolTip="Reassign Audit"
                                                                data-toggle="tooltip"
                                                                    CommandArgument='<%# Eval("AuditID") + "," + Eval("Customer") + "," + Eval("AuditScheduleOnID")  + "," + Eval("ForMonth") %>'>
                                                                    <img src='<%# ResolveUrl("~/Images/Send_icon.png")%>' alt="Reassign Audit" /></asp:LinkButton>   
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnChangeStatus" />
                                                            <asp:AsyncPostBackTrigger ControlID="lnkReassignAudit" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                    </asp:GridView>
                                </div>
                                <div class="col-md-12 colpadding0">
                                   
                                    <div class="col-md-10 colpadding0">
                                        <asp:LinkButton ID="lnkDownloadHearing" runat="server" Style="float: right;" ToolTip="Download Open Closed Audits" data-toggle="tooltip" data-placement="bottom" OnClick="lnkDownloadHearing_Click">                                   
                                    <img src='../../Images/download_icon_new.png' alt="Download Upcoming Hearing"/></asp:LinkButton>
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 20px; width: 110px;">
                                            <asp:ImageButton ID="lBPrevious" Style="width: 28px;" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />
                                            <div class="table-paging-text" style="width: 40px; margin-top: -2px;">
                                                <p>
                                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                            <asp:ImageButton ID="lBNext" Style="width: 28px;" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    <asp:HiddenField ID="TotalRowsReviewer" runat="server" Value="0" />
                </div>
            </ContentTemplate>
            <Triggers>
                 <asp:PostBackTrigger ControlID="lnkDownloadHearing" /> 
            </Triggers>
        </asp:UpdatePanel>
        
        
        <div class="modal fade" id="divOpenUpdateStatusPopup1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog p5" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom" style="font-weight: 600; font-size: 20px;">
                            Audit Cancellation</label>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="upMailDocument" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="form-group required col-md-12">
                                        <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="vsFolderDocumentValidationGroup" />
                                        <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                            ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 9px;">

                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-2" style="color: #333;">
                                            Location:
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label runat="server" ID="lblLocation" Style="color: #333;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-2" style="color: #333;">
                                            Period:                                        
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label runat="server" ID="lblPeriod" Style="color: #333;"></asp:Label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-2" style="color: #333;">
                                            <asp:Label runat="server" ID="Label3" Style="color: red;">*</asp:Label>
                                            Remark:                                        
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox runat="server" ID="txtRemark" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Remark"
                                                ControlToValidate="txtRemark" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="HiddenFieldAuditId" runat="server" Value="0" />
                                <asp:HiddenField ID="HiddenFieldScheduleOnId" runat="server" Value="0" />

                                <div class="row">
                                    <div class="form-group required col-md-12">

                                        <div class="col-md-2" style="color: #333;">
                                        </div>
                                        <div class="col-md-8 text-center">
                                            <asp:Button Text="Update" runat="server" ID="btnUpdateStatus" CssClass="btn btn-primary"
                                                ValidationGroup="vsFolderDocumentValidationGroup" OnClick="btnUpdateStatus_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="divOpenUpdateReschedulePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog p5" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom" style="font-weight: 600; font-size: 20px;">
                            Reschedule Audit</label>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="form-group required col-md-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="None"
                                            class="alert alert-block alert-danger fade in"
                                            ValidationGroup="vsFolderDocumentValidationGroup1" />
                                        <asp:CustomValidator ID="cvReschedule" runat="server" EnableClientScript="False"
                                            ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 9px;">

                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3" style="color: #333;">
                                            Location:
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label runat="server" ID="lblLocation1" Style="color: #333;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3" style="color: #333;">
                                            Period:                                        
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label runat="server" ID="lblPeriod1" Style="color: #333;"></asp:Label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">

                                        <div class="col-md-3" style="color: #333;">
                                            <asp:Label runat="server" ID="Label1" Style="color: red;">*</asp:Label>
                                            Start Date:                                        
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox runat="server" ID="txtStartDate" placeholder="Start Date" autocomplete="off" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Required Start Date"
                                                ControlToValidate="txtStartDate" runat="server" ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3" style="color: #333;">
                                            <asp:Label runat="server" ID="Label2" Style="color: red;">*</asp:Label>
                                            End Date:                                        
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox runat="server" ID="txtEndDate" autocomplete="off" placeholder="End Date" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required End Date"
                                                ControlToValidate="txtEndDate" runat="server" ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 9px;">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-3" style="color: #333;">
                                            <asp:Label runat="server" ID="Label5" Style="color: red;">*</asp:Label>
                                            Remark:                                        
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox runat="server" ID="txtRemarkReschedule" TextMode="MultiLine" Rows="3"
                                                CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                ErrorMessage="Required Remark"
                                                ControlToValidate="txtRemarkReschedule" runat="server"
                                                ValidationGroup="vsFolderDocumentValidationGroup1" Display="None" />
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="HiddenFieldScheduleOnId1" runat="server" Value="0" />
                                <asp:HiddenField ID="HiddenFieldAuditId1" runat="server" Value="0" />

                                <div class="row">
                                    <div class="form-group required col-md-12">
                                        <div class="col-md-3" style="color: #333;">
                                        </div>
                                        <div class="col-md-8 text-center">
                                            <asp:Button Text="Save" runat="server" ID="btnReschedule" CssClass="btn btn-primary"
                                                ValidationGroup="vsFolderDocumentValidationGroup1"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        </script>

        <script type="text/javascript">
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
                $('#BodyContent_tbxFilterLocation').keyup(function () {
                    FnSearch();
                });
            }
            function initializeRadioButtonsList(controlID) {
                $(controlID).buttonset();
            }
        </script>
    </form>
</body>
