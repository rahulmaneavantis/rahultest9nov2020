﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditDisplayScheduleInformation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.AuditDisplayScheduleInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <style>

        body {
    color: #f1f1f1;
    background: #fff !important;
    font-family: 'Roboto',sans-serif;
    padding: 0!important;
    margin: 0!important;
    font-size: 14px!important;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>     
            <div class="dashboard" style="margin-top: 15px;">
                <div style="margin-bottom: 4px">
                      <%--Style="border: solid 1px red; background-color: #ffe8eb;"--%>
                    <asp:ValidationSummary ID="vdsummary" runat="server"
                         class="alert alert-block alert-danger fade in"
                       
                        ValidationGroup="ComplianceValidationGroup" />
                    <asp:CustomValidator ID="cucustomentry" runat="server" EnableClientScript="False"
                        class="alert alert-block alert-danger fade in"
                        ValidationGroup="ComplianceValidationGroup" Display="None" />
                </div>
                <div runat="server" id="divStartMonth" style="margin-bottom: 7px">
                    <label style="width: 150px; display: block; margin-left: 50px; float: left; font-size: 14px; color: #333;">
                        Select Year</label>
                    <asp:DropDownList runat="server" ID="ddlStartMonth" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_SelectedIndexChanged">
                        <asp:ListItem Value="1" Text="Calender Year"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Financial Year"></asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div runat="server" style="margin-bottom: 7px; padding-left: 50px; " >
                    <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <th style="width: 150px; border: 1px solid gray; text-align: center;">For Period
                                    </th>
                                    <th style="width: 200px; border: 1px solid gray; text-align: center;">Day
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="margin-bottom: 7px">
                                <tr>
                                    <td align="center" style="border: 1px solid gray">
                                        <%# Eval("ForMonthName")%>
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                        <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                    </td>
                                    <td align="center" style="border: 1px solid gray">
                                        <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                            CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" />
                                        <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                            CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                    </td>
                                </tr>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 40%; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click"
                        CssClass="btn btn-primary"  Width="90px" />
                    <asp:Button  Text="Reset" runat="server" ID="btnReset" Visible="false" OnClick="btnReset_Click"
                       CssClass="btn btn-primary"  />
                    <asp:Button Text="Close" runat="server" ID="Button2" Visible="false" CssClass="btn btn-primary" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                </div>
            </div>     
    </form>
</body>
</html>
