﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSVendor.Master" AutoEventWireup="true" CodeBehind="RLCSVendorAuditChecklist.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCSVendorAudit.RLCSVendorAuditChecklist" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <meta name="author" content="AVANTIS - Development Team">

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <style type="text/css">
        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .info-box:hover {
            color: #FF7473;
            font-weight: 500;
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            z-index: 5;
        }

        .function-selector-radio-label {
            font-size: 12px;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }

        #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
            border: 1px solid white;
        }

        .TMB {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 16.5% !important;
        }

        .TMB1 {
            padding-left: 0px !important;
            padding-right: 0px !important;
            width: 19.5% !important;
        }

        .TMBImg {
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin-left: -7%;
            margin-right: -3%;
        }

        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 45px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 95px !important;
        }

        .Dashboard-white-widget {
            padding: 5px 10px 0px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        .TMBImg > img {
            width: 47px;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 18px;
        }

        td > label {
            padding: 6px;
        }

        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 20px;
        }

        span.input-group-addon {
            padding: 0px;
        }

        td > label {
            padding: 3px 4px 0 4px;
            margin-top: -1%;
        }

        .nav-tabs > li > a {
            color: #333 !important;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #1fd9e1 !important;
        }

        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btuploadss {
            background-image: url(../../Images/icon-updated.png);
            border: 0px;
            width: 30px;
            height: 30px;
            background-color: transparent;
            background-repeat: no-repeat;
            float: left;
        }

        .btnview {
            background-image: url(../Images/view-icon-new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .btnss {
            background-image: url(../../Images/Save-icon.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        .dd_chk_select {
            height: 81px !important;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
            height: 30px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        div.dd_chk_drop {
            background-color: white;
            border: 1px solid #CCCCCC;
            text-align: left;
            z-index: 1000;
            left: -1px;
            top: 28px !important;
            min-width: 100%;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        
      $(document).ready(function () { 
             var query = window.location.search.substring(1);
             var vars = query.split("&");
             for (var i = 0; i < vars.length; i++) {
                 var pair = vars[i].split("=");
                 if (pair[0] == "Flag")
                 {
                     if (pair[1] == "byOtp") {
                         $('#main-content').css('margin-left', '10px');
                         $('.header').css('display', 'none');
                         $('#sidebar').css('display', 'none');
                         $('#timedateProgress').css('display', 'none');
                         $('#pagetype').parent('div').parent('.wrapper').parent('div.row').hide();
                         document.getElementById('AuditChecklist').style.display = 'inherit';
                     }                    
                 }
             }
             //$("#btnDocumentUpload").on("click", function (e) {
             //    $("#divUploadDocument").modal("show");
          //});

             $("input[type=radio][name='SampleFormat']").change(function () {
                 if (this.value == 'D') {
                     $("#lnksampledownload").attr("href", "SampleFormat/DeclarationonStatutoryLabourComplaince.docx");
                 }
                 else if (this.value == 'I') {
                     $("#lnksampledownload").attr("href", "SampleFormat/Input_Format.xlsx");
                 }
             });
             $("input[type=radio][name='SampleFormat']").trigger("change");
      });

      function showUploadDocumentModal() {
          $("#divUploadDocument").modal("show");
      }
        function gotoBack() {
            window.location.href = "../RLCSVendorAudit/RLCSVendorAuditDashboard.aspx";
        }
        $('#divShowDialog').on("show", function () {
            $(this).find(".modal-body").css("max-height", height);
        });

        $('#divShowDialog').on('show.bs.modal', function () {
            $('#divShowDialog').find('.modal-body').css({
                width: 'auto', //probably not needed
                height: 'auto', //probably not needed 
                'max-height': '100%'
            });
        });

        function openperformerpopup(CHID, AID, SOID, SID, CBID) {
            
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_PerofrmerTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID);
        }

        function openreviwerpopup(CHID, AID, SOID, SID, CBID) {
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_ReviewerTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID);
        }
         function openClosedpopup(CHID, AID, SOID, SID, CBID) {
            var modalHeight = screen.height - 200;
            if (modalHeight < 0)
                modalHeight = 200;

            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', modalHeight + "px");
            $('#showdetails').attr('src', "../RLCSVendorAudit/RLCS_ClosedTransaction.aspx?CHID=" + CHID + "&AID=" + AID + "&SOID=" + SOID + "&SID=" + SID + "&CBID=" + CBID);
        }


        function fclosepopup() {
            $('#divShowDialog').modal('hide');
        }
        function closeModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }
        function fopenpopup() {
            $('#divShowAddNewStepDialog').modal('show');
            return true;
        }
        function CloseWin() {
            $('#divShowAddNewStepDialog').modal('hide');
        };
        //This is used for Close Popup after button click.
        function caller() {
            setInterval(CloseWin, 30000);
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix" style="height: 0px"></div>
    <asp:UpdatePanel ID="Upvendorchecklist" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                            <div class="clearfix"></div>
                            <div class="panel-body">
                                <div class="col-md-12 colpadding0">
                                    <asp:Label ID="AuditChecklist" Style="color: black; font-size: 25px; font-weight: 450; display: none;">Audit Checklist</asp:Label>
                                </div>
                                 <div class="col-md-12 colpadding0">
                                    <asp:Label ID="lblAuditName" runat="server" Style="color: black; font-size: 25px; font-weight: 450;"></asp:Label>
                                </div>
                                <div class="col-md-12 colpadding0">
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 colpadding0" style="padding-top: 12px;">
                                        <div class="col-md-4 colpadding0">
                                            <asp:DropDownListChosen runat="server" ID="ddlAct"
                                                DataPlaceHolder="Select Act"
                                                Height="30px" Width="100%" class="form-control m-bot15"
                                                OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="col-md-4">
                                            <asp:DropDownListChosen runat="server" ID="ddlChecklistStatus"
                                                DataPlaceHolder="Select Status"
                                                Height="30px" Width="100%" class="form-control m-bot15"
                                                OnSelectedIndexChanged="ddlChecklistStatus_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="col-md-4 colpadding0" style="float: right;">
                                             <button type="button" id="btnDocumentUpload" class="btn btn-primary pull-left"   onclick="showUploadDocumentModal();">Document</button>
                                            <asp:Button ID="btnBack" CausesValidation="false" class="btn btn-search pull-right" Style="margin-left: 14px !important; float: right;" runat="server" Text="Back" OnClientClick="gotoBack()" />

                                            <asp:Button ID="btnAddNewStep" CausesValidation="false" Visible="false" class="btn btn-search" Style="margin-left: 33px !important; float: right;display:none;" runat="server" Text="Add New Step" OnClientClick="fopenpopup()" />

                                        </div>
                                    </div>

                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="tab-content ">
                                    <div runat="server" id="performerdocuments" class="tab-pane active">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div>
                                                    <asp:ValidationSummary runat="server"
                                                        ValidationGroup="ComplianceValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                        ValidationGroup="ComplianceValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                    <asp:Label runat="server" ID="Label2" ForeColor="Red"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" AllowSorting="true"
                                                        GridLines="None" CssClass="table" OnRowCommand="grdChecklist_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="State Name">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                        <asp:Label ID="lblStateID" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("StateId") %>' ToolTip='<%# Eval("StateId") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Act Name">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                        <asp:Label ID="lblAM_ActName" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("AM_ActName") %>' ToolTip='<%# Eval("AM_ActName") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Nature Of Compliance">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblNatureOfCompliance" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("NatureOfCompliance") %>' ToolTip='<%# Eval("NatureOfCompliance") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type Of Compliance">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblTypeOfCompliance" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("TypeOfCompliance") %>' ToolTip='<%# Eval("TypeOfCompliance") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField HeaderText="Risk">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                            Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="lnkEditChecklist" runat="server" CommandName="EditChecklist" ToolTip="Edit Checklist"
                                                                                data-toggle="tooltip"
                                                                                Visible='<%# CanChangeStatus((string)Eval("Status"),(int) Eval("AuditStatusID")) %>'
                                                                                CommandArgument='<%# Eval("Id") + "," + Eval("Status") +","+Eval("CustomerBranchID") %>'>
                                                                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Checklist" /></asp:LinkButton>
                                                                               
                                                                            <asp:LinkButton ID="lnkViewChecklist" runat="server" CommandName="ClosedChecklist" ToolTip="View Checklist"
                                                                                data-toggle="tooltip"
                                                                                Visible='<%# CanChangeStatusClosed((string)Eval("Status")) %>'
                                                                                CommandArgument='<%# Eval("Id") + "," + Eval("Status") +","+Eval("CustomerBranchID") %>'>
                                                                                    <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="View Checklist" /></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="lnkEditChecklist" />
                                                                            <asp:PostBackTrigger ControlID="lnkViewChecklist" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Right" />
                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <PagerTemplate>
                                                            <table style="display: none">
                                                                <tr>
                                                                    <td>
                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div style="margin-bottom: 4px; float: left;">
                                            <table width="100%" style="height: 10px">
                                                <tr>
                                                    <td colspan="2">
                                                         <asp:TextBox ID="txtObservation" runat="server" CssClass="form-control" Visible="false" TextMode="MultiLine" Style="width:250%;"  placeholder="Observation"></asp:TextBox>
                                                   
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>&nbsp</td>
                                                      <td>&nbsp</td>
                                                </tr>
                                                <tr>
                                                     <td colspan="2">
                                                            <asp:TextBox ID="txtRecomandation" runat="server" CssClass="form-control" Visible="false" TextMode="MultiLine" Style="width:250%;" placeholder="Recommendation"></asp:TextBox>
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>&nbsp</td>
                                                      <td>&nbsp</td>
                                                </tr>
                                                <tr>
                                                     <td>
                                                        <asp:Button ID="btnSave" CausesValidation="false" class="btn btn-search"
                                                             runat="server" Visible="true"
                                                            Text="Save" OnClick="btnSave_Click"  />

                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSaveClose" CausesValidation="false" class="btn btn-search"
                                                             runat="server" Visible="false"
                                                            Text="Save & Close" OnClick="btnSaveClose_Click" />

                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>
            <div style="display: none;">

                <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                </asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgColor-gray" style="height: 35px; text-align: left;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom col-md-6 plr0">
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7; border-radius: 10px;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
                  <button type="button" data-dismiss="modal" hidden="hidden" id="close"></button>
            </div>
        </div>
    </div>


    <div class="modal fade" id="divShowAddNewStepDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 55%">
            <div class="modal-content" style="width: 100%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeModal();">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divAddNewStepDialog">
                        <asp:UpdatePanel ID="upNewStep" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                            
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none"
                                             class="alert alert-block alert-danger fade in"
                                             ValidationGroup="NewStepValidationGroup" />

                                        <asp:CustomValidator ID="cvpopup" runat="server" EnableClientScript="False"
                                            ValidationGroup="NewStepValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>

                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 90px; display: block; float: left; font-size: 13px; color: #333;">
                                            CheckList</label>
                                        <asp:DropDownCheckBoxes ID="ddlNewStepCheck" runat="server" Width="200px"
                                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                                            OnSelectedIndexChanged="ddlNewStepCheck_SelcetedIndexChanged">
                                            <Style SelectBoxWidth="500" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="250"></Style>
                                            <Texts SelectBoxCaption="Select Checklist" />
                                        </asp:DropDownCheckBoxes>

                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnPopupSave" OnClick="btnPopupSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="NewStepValidationGroup" />
                                        <%--<asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />--%>
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <%--<p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>--%>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divUploadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:95%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divUpload_Document">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                            
                                <div class="row" runat="server" id="divUploadDoc">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <input type="radio" id="sampeformart1" name="SampleFormat" value="D" checked>
                                            <label for="sampeformart1" style="color:black;">Declaration on Statutory Labour Compliance</label><br>
                                            <input type="radio" id="sampeformart2" name="SampleFormat" value="I">
                                            <label for="sampeformart2" style="color:black;">Input Form</label><br>
                                         
                                      </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                      <a id="lnksampledownload" href="">Download Sample Format </a>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <asp:FileUpload ID="MusterRollFileUpload" multiple="multiple"  runat="server" />
                                    </div>
                                     <div class="col-lg-1 col-md-1 col-sm-1">
                                          <asp:Button ID="btnMusterRoll" runat="server" Text="Upload" 
                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload"
                                                    CausesValidation="true" OnClick="btnMusterRoll_Click" />
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                        PageSize="10" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document Uploaded Date" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                        <%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                <ItemTemplate>   
                                                     <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>                                        
                                                     <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                        CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>  
                                                        </ContentTemplate>
                                                        <Triggers>  
                                                            <asp:PostBackTrigger ControlID="lnkDownloadDocument" />  
                                                        </Triggers>  
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <PagerTemplate>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>  
                                <asp:PostBackTrigger ControlID="btnMusterRoll" />  
                            </Triggers>  
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

