﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="EscalationRemindersKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.EscalationRemindersKendo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />

    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
         .k-grid-header-wrap.k-auto-scrollable {
            width: 99.9%;
        }
        table.k-selectable {
            border-right: 1px solid #ceced2;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

      .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

      .k-grouping-header 
      {
          border-right: solid 1px #ceced2;
          border-left: solid 1px #ceced2;
          border-top: solid 1px #ceced2;
      }

        .k-widget > span.k-invalid,
        textarea.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget.k-tooltip-validation {
            display: none !important;
        }

        .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
            padding-right: 2em;
            display: grid;
        }

        .k-dropdown-wrap, .k-numeric-wrap, .k-picker-wrap 
        {
            padding-right: 7em;
        }

        .k-grouping-header
        {
           color: #515967;
           font-style: italic;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-grid-toolbar {
            background: white;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em .6em .4em;
        }

        .k-grid td {
            line-height: 1em;
            /*border-bottom-width: 1px;*/
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        .k-grid tbody tr {
            height: 38px;
        }

        div.k-grid-header 
         {
            border-top-style: solid;
            border-top-width: 0px;
         }

        .k-textbox .k-icon {
            top: 50%;
            margin: -8px 0 0;
            margin-left: 56px;
            position: absolute;
        }
    </style>

     <script id="StatusTemplate" type="text/x-kendo-template">                 
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetStatusType(IntreimDays)# 
    </script>


    <script type="text/javascript">

        function GetStatusType(value) {

            if (value == null)
            {
                return '';
            }
            else
            {
                return value;
            }
        }

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Reviewer');

            Bindgrid();

            $("#txtSearchfilter").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "ActName" || x.field == "ShortDescription" || x.field == "Branch" || x.field == "Frequency" || x.field == "User" || x.field == "DueDate" || x.field == "IntreimDays" || x.field == "EscDays") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);

            });

             $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                },
                //index: 1,
                dataSource: [
                    <%--<%if (PerformerFlagID == 1)%>
                    <%{%>
                        { text: "Performer", value: "3" },
                     <%}%>--%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                        { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({
                //placeholder: "Type",
                dataTextField: "text",
                dataValueField: "value",
                //optionLabel: "Select",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Event Based", value: "1" },
                ],
                index: 0,
                change: function (e) {
                    FilterAll();

                }
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });


        });
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<% =Path%>data/GetEscalationReminder?UserId=<% =UId%>&CustomerID=<% =CustId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },
                update: {
                    url: "<% =Path%>Data/EditEscalationReminder" + '?UserID=<%=UId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "complianceID",
                    //setDefaults: SetDefaultValue ,
                    //defaultValue: SetDefaultValue ,
                    fields: {
                        ActName: { editable: false, validation: { required: true } },
                        ShortDescription: { editable: false, validation: { required: true } },
                        Branch: { editable: false, validation: { required: true } },
                        Frequency: { editable: false, validation: { required: true } },
                        User: { editable: false, validation: { required: true } },
                        DueDate: { editable: false, validation: { required: true } },
                        //IntreimDays: { editable: true, nullable: true,validation: { required: true } },
                        //EscDays: { editable: true, nullable: true, validation: { required: true } },
                    }
                }
            },
            requestEnd: onRequestEnd
        });
        function onRequestEnd(e) {
            if (e.type == "create") {
                e.sender.read();
            }
            else if (e.type == "update") {
                e.sender.read();
            }
        }

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "ActName", title: 'Act',
                        editable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                            
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Description',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Branch", title: 'Location',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Frequency", title: 'Frequency',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "User", title: 'Performer',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "DueDate", title: 'DueDateOfMonth',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        title: "Work file timeline",
                        type: "number",
                        template: "<input data-bind='value: IntreimDays' class='k-textbox' style='width: 70px;' value=#=(IntreimDays == null ? '': IntreimDays)#>",
                    },
                    {
                        title: "Escalation",
                        type: "number",
                        template: "<input data-bind='value: EscDays' class='k-textbox' data-type='int' style='width: 70px;' value=#=(EscDays == null ? '': EscDays)#>",
                        //template: kendo.template($('#StatusTemplate').html()),
                    }, 
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" } ], title: "Action", lock: true
                    }
                ], editable: "inline"
        
            });

            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the second column's cells
                position: "down",
                width: 700,
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function (e) {
                    return "Delete";
                }
            });

        }

        function FilterAll() {

            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values


            if (locationlist.length > 0 || Riskdetails.length > 0
                || ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownlistComplianceType1").val() != 0 && $("#dropdownlistComplianceType1").val() != -1 && $("#dropdownlistComplianceType1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "EventFlag", operator: "eq", value: parseInt(1)
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Riskdetails.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Riskdetails, function (i, v) {

                        CategoryFilter.filters.push({
                            field: "RiskType", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        function AddnewTest(e) {
            $('#divShowReminderDialog').modal('show');
            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Litigation/aspxPages/AddEditReminder.aspx?AccessID=0")
            e.preventDefault();
        }

        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }

        function CloseClearPopup() {
            $('#APIOverView').attr('src', "../Common/blank.html");
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistComplianceType1").data("kendoDropDownList").select(0);
            //$('#txtSearchfilter').val('');
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNotMain();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div id="example">
            <div class="row">
                <div class="row" style="padding-bottom: 4px; padding-right: 138px;">
            <div class="toolbar">   
                    <input id="dropdownlistUserRole" data-placeholder="Role"  style="width:255px;"/>   
            </div>
        </div>

                <div class="row">
                    <input id="dropdownlistComplianceType1" style="width: 255px;" />
                    <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 208px;" /> 
                    <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 409px;"/>
                    <%--<input id='txtSearchfilter' class='k-textbox' placeholder="Type to Filter" style="width: 217px; padding-right: 50px;"/>--%>
                    <%--<button id="Applyfilter" style="margin-left: 1%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button> --%>
                    <button id="ClearfilterMain" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                </div>
            </div>

            <div class="clearfix" style="height: 10px;"></div>
            <div class="row" style="padding-bottom: 0px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;color: #535b6a;font-weight:bold;" id="filtersstoryboard">&nbsp;</div>
            <div id="grid" style="border: none;"></div>
        </div>

    <div style="float:left;width:100%">
                                    <div style="float: left;color: #666666;">
                                        <b>Work File Timeline</b> - This is the day/date on which performer should start sharing/submits the complianace working file to reviewer.
                                    </div>
                                     <div style="float: left;color: #666666;">
                                        <b>Escalation</b> - From this day/date the reviewer will start receiving the mail alerts for compliance.
                                    </div>
                                </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <script>

            $(document).ready(function () {
                fhead('My Escalation');
            });

    </script>

</asp:Content>
