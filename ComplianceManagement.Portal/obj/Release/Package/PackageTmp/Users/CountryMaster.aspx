﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="CountryMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.CountryMaster" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function fopenpopup() {
            $('#divCountryDialog').modal('show');
        }
    </script>
     <style type="text/css">
        .dd_chk_select {            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 


    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('Country Master');
            fhead('Country Master');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount">
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                           
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:25%">
                                    <asp:TextBox runat="server" ID="tbxSearchCountry"  class="form-control m-bot15"  Width="90%" Height="32px"
                                    AutoPostBack="true" Style="background:none;"  PlaceHolder="Search Country" OnTextChanged="tbxSearchCountry_TextChanged">
                                    </asp:TextBox>
                                </div>
            <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" />
                    </div>
            <div style="margin-bottom: 4px"> 
           
                <asp:GridView runat="server" ID="grdCountry" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdCountry_RowDataBound" ShowHeaderWhenEmpty="true"
                   PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"  DataKeyNames="ID" OnRowCommand="grdCountry_RowCommand">
                    <Columns>
                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90%" />                        
                        <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Country" OnClientClick="fopenpopup()" ToolTip="Change Country details" data-toggle="tooltip" 
                                    CommandArgument='<%# Eval("ID") %>'>                                     
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Country" title="Edit Country Details" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Country" ToolTip="Delete Country details" data-toggle="tooltip" Visible="false"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Country Details?');">
                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Country Details" title="Delete Country Details" /></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />  
                     <PagerSettings Visible="false" />                
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>

                  

                </div>
                          <div class="col-md-12 colpadding0">
                                <div class="col-md-10 colpadding0"></div>
                                <div class="col-md-2 colpadding0" style="float:right;">
                                    <div style="float:left;width:50%">
                                        <p class="clsPageNo" style="color:#666">Page</p>
                                    </div>
                                   <div style="float:left;width:50%">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No"
                                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px" >                                   
                                     </asp:DropDownListChosen>
                                    </div>
                                </div>
                              <asp:HiddenField ID="TotalRows" runat="server" Value="0" />                    
                            </div>

                                
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divCountryDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 430px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="AddCountry">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            Country</label>
                                        <asp:TextBox runat="server" ID="tbxCountry" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Country Name can not be empty." ControlToValidate="tbxCountry"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="PromotorValidationGroup"
                                            ErrorMessage="Please enter a valid Country" ControlToValidate="tbxCountry"
                                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
