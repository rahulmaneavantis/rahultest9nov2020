﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OTPVerify.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.OTPVerify" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="Avantis - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="../Style/css/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../Style/css/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../Style/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../Style/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    
   <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../Style/css/style-responsive.css" rel="stylesheet" />
     <script type="text/javascript">
         var _paq = window._paq || [];
         /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
         _paq.push(['trackPageView']);
         _paq.push(['enableLinkTracking']);
         (function () {
             var u = "//analytics.avantis.co.in/";
             _paq.push(['setTrackerUrl', u + 'matomo.php']);
             _paq.push(['setSiteId', '1']);
             var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
             g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
         })();

         function settracknew(e, t, n, r) {
             debugger;
             try {
                 _paq.push(['trackEvent', e, t, n])
             } catch (t) { } return !0
         }

         function settracknewSucess() {
             settracknew('Login', 'VerifyOTP', 'Success', '');
         }
         function settracknewFailed() {
             settracknew('Login', 'VerifyOTP', 'Failed', '');
         }

       
         function settracknewResend() {
             settracknew('Login', 'VerifyOTP', 'ResendOTP', '');
         }
            
         function fclearcookie() {
             window.location.href = "/logout.aspx"
             return false;
         }
        
    </script>
    <style>
        div#vsLogin > ul {
    padding: 0px;
     text-align: justify;
    }
  
    </style>

</head>
<body>
    <div class="container">
        <form runat="server" class="login-form" name="login" id="Form1" autocomplete="off">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnOTP">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <img src="../Images/avantil-logo.png" />
                            </p>
                        </div>
                        <div class="login-wrap">
                             <span   style="font-family:'Roboto',san-serif;color:#555555; font-size:13.5px;" >
                            One Time Password has been sent to <br />
                            Your registered email - <asp:Label ID="email" Font-Bold="true" runat="server" ></asp:Label> <br />
                            Your registered Phone No - <asp:Label ID="mobileno" Font-Bold="true" runat="server" ></asp:Label> <br /> 
                            (Please note that the OTP is valid till <asp:Label ID="Time" runat="server" ></asp:Label>  IST)</span>
                               <div class="clearfix" style="height: 10px"></div>
                               <div class="clearfix" style="height: 10px"></div>
                            <h1 style="font-size:15px;">Please Enter One Time Password (OTP)</h1>                           
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <%--<input type="text" id="txtemail" class="form-control" runat="server" placeholder="Username" autofocus="" />--%>
                                <asp:TextBox ID="txtOTP" CssClass="form-control"  MaxLength="12"  runat="server" placeholder="Enter OTP" />
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                    ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" />--%>                               
                            </div>

                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtOTP" runat="server" 
                                    ErrorMessage="Only Numbers allowed in OTP" ValidationExpression="\d+" ValidationGroup="OTPValidationGroup"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                    ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" /> 
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="OTPValidationGroup" ErrorMessage="Please enter only 5 digit."
                                    ControlToValidate="txtOTP" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>--%>

                            <asp:Label ID="lblmsgotp" runat="server" ForeColor="Red" CssClass="pull-left" Text=""></asp:Label>
                            <asp:Button ID="btnOTP" CssClass="btn btn-primary btn-lg btn-block" Text="Verify OTP" runat="server" OnClick="btnOTP_Click" ValidationGroup="OTPValidationGroup"></asp:Button>

                            <div style="margin-top: 15px;">
                                <asp:LinkButton ID="lnkSecurityQA" OnClick="lnkSecurityQA_Click" Font-Underline="false" runat="server">Do not have access to OTP?</asp:LinkButton>
                                <asp:LinkButton ID="lnkResendOTP" Font-Underline="false" OnClick="lnkResendOTP_Click" style="float:right;" runat="server">Resend OTP</asp:LinkButton>
                            </div>
                            <div style="margin-top:3%;">
  <asp:LinkButton ID="lnkLogOut" OnClientClick="javascript:return fclearcookie();" runat="server" >Or sign in as a different user</asp:LinkButton>
</div>    
                            <%-- <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="icon-remove"></i>
                            </button>
                            <strong>Login Success !</strong> - Welcome to avantis
                        </div>--%>
                            <div class="clearfix" style="height: 10px"></div>
                            <%--  <div id="divMsg" class="alert alert-block alert-danger fade in" visible="false" runat="server">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="icon-remove"></i>
                                </button>--%>
                            <%-- <strong>Sorry!</strong> --%>
                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" ValidationGroup="OTPValidationGroup" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" ValidationGroup="OTPValidationGroup" />
                            <%-- </div>--%>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOTP" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
              <asp:HiddenField runat="server" ID="cid"  />
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
   
    <script type='text/javascript' src="../js/bootstrap.min.js"></script>
    <script type='text/javascript' src="../js/google1113_jquery.min.js"></script>
       <script>

        if ($('#cid').val() == '914') {
            $('#imgc').attr('src', '../Images/zomatologo.jpg');
}

 
  </script>
</body>
</html>
