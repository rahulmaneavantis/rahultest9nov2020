﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="PerformerOnLeave.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Users.PerformerOnLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .input-disabled {
            background-color: #f7f7f7;
            border: 1px solid #c7c7cc;
            padding: 6px 12px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .alert {
            padding: 5px 5px 5px 30px !important;
            margin-bottom: 0px !important;
            border: 1px solid transparent !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function openleavepopup() {
            $('#divLeaveDialog').modal('show');
        }
        function closeleavepopup() {
            $('#divLeaveDialog').modal('hide');
        }

        function openleaveAssignpopup() {
            debugger;
            //alert("Welcome");
            $('#divLeavetempassign').modal('show');
        }
        function closeleaveAssignpopup() {
            $('#divLeavetempassign').modal('hide');
        }
        function opencancelleavepopup() {
            $('#divCancelLeaveDialog').modal('show');
        }
        function closecancelleavepopup() {
            $('#divCancelLeaveDialog').modal('hide');
        }


        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('input[id*=txtEndDate]').datepicker({
                    dateFormat: 'dd-mm-yy',
                    minDate: startDate,
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">

                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" CssClass="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                Display="None" />
                            <asp:Label ID="lblMsg1" CssClass="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                        </div>


                        <div class="clearfix"></div>

                        <div class="col-md-2 colpadding0" style="text-align: right; float: right; width: 8%;">
                            <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" />
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <li class="active" id="liNotDone" runat="server">
                                    <asp:LinkButton ID="Tab1" class="active" OnClick="Tab1_Click1" CommandArgument="tab1value" runat="server">Apply Leave</asp:LinkButton>
                                </li>
                                <li class="" id="liSubmitted" runat="server">
                                    <asp:LinkButton ID="Tab2" OnClick="Tab2_Click1" runat="server">Temporary Assignment</asp:LinkButton>

                                </li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                        <div class="tab-content">
                            <div runat="server" id="performerdocuments" class="tab-pane active">

                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-3 colpadding0">
                                                <p style="color: #999; margin-top: 5px;">Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Text="5" />
                                                <asp:ListItem Text="10" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-md-4 colpadding0">
                                        </div>
                                        <div class="col-md-2 colpadding0" style="text-align: right; float: right; width: 5.5%; display: none;">
                                            <asp:LinkButton ID="linkbutton" runat="server" CssClass="btn btn-search" Style="float: right" OnClick="linkbutton_onclick">Back</asp:LinkButton>

                                        </div>

                                        <div class="col-md-2 colpadding0" style="text-align: right; float: right">
                                        </div>
                                        <!-- Advance Search scrum-->
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 AdvanceSearchScrum">
                                            <div runat="server" id="DivRecordsScrum" style="float: right;">
                                                <p style="padding-right: 0px !Important;">
                                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>

                                <div id="ReviewerGrids" runat="server">
                                    <div style="margin-bottom: 4px">
                                        <asp:GridView runat="server" ID="grdLeave" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                            CellPadding="4" Width="100%" OnRowCommand="grdLeave_RowCommand"
                                            OnPageIndexChanging="grdLeave_PageIndexChanging" DataKeyNames="ID" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-align: center; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblStartDate" runat="server"
                                                                Text='<%# Eval("LStartDate") != null ? Convert.ToDateTime(Eval("LStartDate")).ToString("dd-MMM-yyyy") : ""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-align: center; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblEndDate" runat="server"
                                                                Text='<%# Eval("LEndDate") != null ? Convert.ToDateTime(Eval("LEndDate")).ToString("dd-MMM-yyyy") : ""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-align: center; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                            <asp:Label ID="lblCreatedOn" runat="server"
                                                                Text='<%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString() : ""%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Edit_Leave"
                                                            CommandArgument='<%# Eval("ID") %>'
                                                            Visible='<%# CanChangeStatus((string)Eval("EditFlag")) %>'><img src="../../Images/edit_icon_new.png" alt="Edit Observation" title="Edit Leave Details" data-toggle="tooltip" /></asp:LinkButton>

                                                        <%--     src="../img/Delete_Icon.png"--%>
                                                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="Cancel_Leave" CommandArgument='<%# Eval("ID") %>'
                                                            Visible='<%# CanChangeStatus((string)Eval("cancelFlag")) %>'><img src="../img/delete_icon_new.png" alt="Cancel Leave" title="Cancel Leave" /></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0">
                                        <div class="table-Selecteddownload">
                                            <div class="table-Selecteddownload-text">
                                                <p>
                                                    <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 15px;"></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 colpadding0">
                                        <div class="table-paging" style="margin-bottom: 20px;">
                                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                            <div class="table-paging-text">
                                                <p>
                                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                                </p>
                                            </div>

                                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divLeaveDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 88%;">
                    <div class="modal-content" style="width: 98%; height: 410px;">
                        <div class="modal-header" style="background-color: white;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

                            <h1>Apply Leave</h1>
                        </div>
                        <div class="modal-body">
                            <div id="AuditAssignment">
                                <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div>
                                            <div style="margin-bottom: 7px">
                                                <asp:ValidationSummary ID="VDS" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="LeaveValidationGroup" />
                                                <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                                                    ValidationGroup="LeaveValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    Start Date</label>

                                                <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDate"
                                                    runat="server" ID="RequiredFieldValidator4" ValidationGroup="LeaveValidationGroup" Display="None" />
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    End Date</label>

                                                <asp:TextBox runat="server" ID="txtEndDate" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDate"
                                                    runat="server" ID="RequiredFieldValidator3" ValidationGroup="LeaveValidationGroup" Display="None" />
                                            </div>
                                            <div id="perdiv" runat="server" style="margin-bottom: 7px">

                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Performer</label>

                                                <asp:DropDownList runat="server" ID="ddlNewPerformerUsers" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewPerformerUsers_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewPerformerUsers" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewPerformerUsers" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroup" Display="None" />

                                            </div>
                                            <div id="revdiv" runat="server" style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Reviewer</label>

                                                <asp:DropDownList runat="server" ID="ddlNewReviewerUsers" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewReviewerUsers_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewReviewerUsers" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewReviewerUsers" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroup" Display="None" />
                                            </div>

                                            <div id="eveDiv" runat="server" style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New Event Owner</label>

                                                <asp:DropDownList runat="server" ID="ddlNewEventOwnerUsers"
                                                    Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewEventOwnerUsers_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewEventOwnerUsers" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewEventOwnerUsers" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroup" Display="None" />
                                            </div>
                                        </div>
                                        <div style="margin-bottom: 1px; margin-left: 380px; margin-top: 10px">
                                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="javascript:validateCheckBoxes()" CssClass="btn btn-primary"
                                                ValidationGroup="LeaveValidationGroup" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="divLeavetempassign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 88%;">
                    <div class="modal-content" style="width: 98%; height: 820px;">
                        <div class="modal-header" style="background-color: white;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h1>Reassign Compliance</h1>

                        </div>
                        <div class="modal-body">
                            <div id="AuditAssignmentnew">
                                <asp:UpdatePanel ID="uptempassign" runat="server">
                                    <ContentTemplate>
                                        <div>
                                            <div style="margin-bottom: 7px">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="LeaveassignValidationGroup" />
                                                <asp:CustomValidator ID="cvDuplicateassign" runat="server" EnableClientScript="False"
                                                    ValidationGroup="LeaveassignValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <asp:RadioButtonList ID="rblcompliancetype" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="true" OnSelectedIndexChanged="rblcompliancetype_SelectedIndexChanged">
                                                    <asp:ListItem Text="Statutory &nbsp;&nbsp;" Selected="True" Value="0" />
                                                    <asp:ListItem Text="Internal" Value="1" />
                                                </asp:RadioButtonList>
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    Start Date</label>
                                                <asp:TextBox runat="server" ID="txtStartDateassign" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDateassign"
                                                    runat="server" ID="RequiredFieldValidator5" ValidationGroup="LeaveassignValidationGroup" Display="None" />
                                            </div>
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    End Date</label>
                                                <asp:TextBox runat="server" ID="txtEndDateassign" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDateassign"
                                                    runat="server" ID="RequiredFieldValidator6" ValidationGroup="LeaveassignValidationGroup" Display="None" />
                                            </div>

                                            <div id="perdivAssign" style="margin-bottom: 7px" runat="server">

                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Performer</label>

                                                <asp:DropDownList runat="server" ID="ddlNewPerformerUsersassign" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewPerformerUsersassign_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewPerformerUsersassign" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveassignValidationGroup" Display="None" />

                                            </div>
                                            <div id="revdivAssign" style="margin-bottom: 7px" runat="server">

                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Reviewer</label>

                                                <asp:DropDownList runat="server" ID="ddlNewReviewerUsersassign" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewReviewerUsersassign_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewReviewerUsersassign" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveassignValidationGroup" Display="None" />

                                            </div>


                                            <div id="eveDivAssign" style="margin-bottom: 7px" runat="server">

                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New Event Owner</label>

                                                <asp:DropDownList runat="server" ID="ddlNewEventOwnerUsersassign"
                                                    Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewEventOwnerUsersassign_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Select User to Assign." ForeColor="Red" ControlToValidate="ddlNewEventOwnerUsersassign" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveassignValidationGroup" Display="None" />

                                            </div>
                                            <div style="margin-bottom: 7px">


                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    Filter:</label>

                                                <%--<asp:TextBox runat="server" ID="tbxFilter"
                                                     OnTextChanged="tbxFilter_TextChanged" 
                                                    Width="500px" MaxLength="50" AutoPostBack="true"   />--%>

                                                <asp:TextBox ID="txtFilter" runat="server" Width="225px" MaxLength="50" AutoPostBack="true" OnTextChanged="txtFilter_TextChanged" ></asp:TextBox>
                                            </div>
                                            <div style="margin-bottom: 7px">

                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    Act:</label>

                                                <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; height: 28px; width: 50%;"
                                                    class="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged" />
                                            </div>
                                        </div>

                                        <%-- grid code--%>

                                        <div>
                                            <asp:Panel ID="Panel1" runat="server" Style="margin-bottom: 4px" Width="100%" Height="300px" ScrollBars="auto">
                                                <br />
                                                <asp:GridView runat="server" ID="grdassignedcompliance" AutoGenerateColumns="false" CssClass="table"
                                                    GridLines="None" BorderWidth="0px"
                                                    CellPadding="1" Width="100%" OnPageIndexChanging="grdassignedcompliance_PageIndexChanging"
                                                    DataKeyNames="ComplianceInstanceID" AllowPaging="True" PageSize="2000" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComplianceInstenaceID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ComplianceId" SortExpression="ComplianceId">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblComplianceID" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceId") %>' ToolTip='<%# Eval("ComplianceId") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description" SortExpression="ShortDescription">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                                                    <asp:Label runat="server" ID="lblShortDescription" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Performer" SortExpression="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label runat="server" ID="lblPerformer" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Perofrmer") %>' ToolTip='<%# Eval("Perofrmer") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Reviewer" SortExpression="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label runat="server" ID="lblReviewer" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chkselection" runat="server" onclick="unCheckSelectAll(this)" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>


                                        <div style="margin-bottom: 1px; margin-left: 380px; margin-top: 10px">
                                            <asp:Button Text="Save" runat="server" ID="btnassignSave" OnClick="btnAssignSave_Click" OnClientClick="javascript:validateCheckBoxes()" CssClass="btn btn-primary"
                                                ValidationGroup="LeaveassignValidationGroup" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnAddPromotor" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="divCancelLeaveDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 600px;">
                    <div class="modal-content" style="width: 600px; height: 500px;">
                        <div class="modal-header" style="background-color: white;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h1>Cancel Leave</h1>
                        </div>
                        <div class="modal-body">
                            <div id="CANAssignment">
                                <asp:UpdatePanel ID="upPromotorC" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div>
                                            <div style="margin-bottom: 7px">
                                                <asp:ValidationSummary ID="VDSC" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                    ValidationGroup="LeaveValidationGroupC" />
                                                <asp:CustomValidator ID="cvDuplicateC" runat="server" EnableClientScript="False"
                                                    ValidationGroup="LeaveValidationGroupC" Display="none"
                                                    class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                <asp:RadioButtonList ID="rbtnlstleave" runat="server" RepeatDirection="Horizontal"
                                                    OnSelectedIndexChanged="rbtnlstleave_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="Partial &nbsp;&nbsp;" Value="0" />
                                                    <asp:ListItem Text="All" Value="1" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>

                                            <div style="margin-bottom: 20px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    Start Date</label>
                                                <asp:TextBox runat="server" ID="txtStartDateC" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDateC"
                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="LeaveValidationGroupC" Display="None" />
                                            </div>

                                            <div style="margin-bottom: 20px">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    End Date</label>
                                                <asp:TextBox runat="server" ID="txtEndDateC" placeholder="DD-MM-YYYY"
                                                    class="form-control input-disabled" Style="width: 115px;" />
                                                <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDateC"
                                                    runat="server" ID="RequiredFieldValidator2" ValidationGroup="LeaveValidationGroupC" Display="None" />
                                            </div>

                                            <div style="margin-bottom: 20px" id="perdivC" runat="server">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Performer</label>
                                                <asp:DropDownList runat="server" ID="ddlNewPerformerUsersC" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewPerformerUsersC_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewPerformerUsersC" ErrorMessage="Select User to Assign." ForeColor="Red"
                                                    ControlToValidate="ddlNewPerformerUsersC" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroupC" Display="None" />
                                            </div>
                                            <div style="margin-bottom: 15px" id="revdivC" runat="server">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New User Reviewer</label>
                                                <asp:DropDownList runat="server" ID="ddlNewReviewerUsersC" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewReviewerUsersC_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewReviewerUsersC" ErrorMessage="Select User to Assign."
                                                    ForeColor="Red" ControlToValidate="ddlNewReviewerUsersC" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroupC" Display="None" />
                                            </div>
                                            <div style="margin-bottom: 15px" id="eveDivC" runat="server">
                                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                                    New Event Owner</label>
                                                <asp:DropDownList runat="server" ID="ddlNewEventOwnerUsersC" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewEventOwnerUsersC_SelectedIndexChanged" />
                                                <asp:CompareValidator ID="cvNewEventOwnerUsersC" ErrorMessage="Select User to Assign." ForeColor="Red"
                                                    ControlToValidate="ddlNewEventOwnerUsersC" runat="server" ValueToCompare="-1"
                                                    Operator="NotEqual" ValidationGroup="LeaveValidationGroupC" Display="None" />
                                            </div>
                                            <div style="margin-bottom: 7px; margin-left: 220px; margin-top: 10px">
                                                <asp:Button Text="Save" runat="server" ID="btnCancelSave"
                                                    OnClick="btnCancelSave_Click" CssClass="btn btn-primary"
                                                    ValidationGroup="LeaveValidationGroupC" />

                                            </div>
                                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                                <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                            </div>
                                            <div class="clearfix" style="height: 50px">
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <asp:HiddenField ID="HDN" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnassignSave" />
            <asp:PostBackTrigger ControlID="btnCancelSave" />
            <asp:PostBackTrigger ControlID="btnSave" />

        </Triggers>
    </asp:UpdatePanel>






    <script type="text/javascript">
        var gridViewId = '#<%= grdassignedcompliance.ClientID %>';
        function checkAll(selectAllCheckbox) {
            //get all checkbox and select it
            $('td :checkbox', gridViewId).prop("checked", selectAllCheckbox.checked);
        }
        function unCheckSelectAll(selectCheckbox) {
            //if any item is unchecked, uncheck header checkbox as also
            if (!selectCheckbox.checked)
                $('th :checkbox', gridViewId).prop("checked", false);
        }

        function validateCheckBoxes() {
            debugger;
            var isValid = false;
            var gridView = document.getElementById('<%= grdassignedcompliance.ClientID %>');
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            //alert("Please select atleast one checkbox");
            document.getElementById("lblcompliance").innerHTML = "Please select atleast one compliance";
            return false;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Leave');
            setactivemenu('leftescalationmenu');
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).ready(function () {
            $('#ContentPlaceHolder1_txtEndDate').addClass('input-disabled')
            $('#ContentPlaceHolder1_txtStartDate').addClass('input-disabled')
        });
    </script>
</asp:Content>
