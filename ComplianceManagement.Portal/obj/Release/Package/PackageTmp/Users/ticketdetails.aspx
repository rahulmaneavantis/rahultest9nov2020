﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ticketdetails.aspx.cs" MasterPageFile="~/NewCompliance.Master" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ticketdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style>
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div   >
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserReminderList" runat="server" UpdateMode="Conditional">
        <ContentTemplate >
            <%--<div class="container-fluid">
            <div class="row">
            <div class="col-sm-2">
             <asp:Label id="lbltext" runat="server" Text="Please enter ticket Id :"/>
            </div>
            <div class="col-sm-3">
                <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"/>
            </div>
            <div class="col-sm-2">
                <asp:Button Text="Search" class="btn btn-search" OnClick="Submit" runat="server"   ValidationGroup="DocumentsValidation" />   
            </div></div>
            <div class="row">
                <div class="col-sm-1">
                    Ticket ID:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtTicketID" Width="250px" MaxLength="50"/>
                </div>
                <div class="col-sm-1">
                    Created Date:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtTicDate" Width="250px" MaxLength="50"/>
                </div>
            </div>
                <div class="row">
                <div class="col-sm-1">
                    Dept Name:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtDeptName" Width="250px" MaxLength="50"/>
                </div>
                <div class="col-sm-1">
                    Topic:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtTopic" Width="250px" MaxLength="50" />
                </div>
            </div>
                <div class="row">
                <div class="col-sm-1">
                    Status:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtStatus" Width="250px" MaxLength="50" />
                </div>
                <div class="col-sm-1">
                    Priority:
                </div>
                <div class="col-sm-3">
                    <asp:Label runat="server" ID="txtPriority" Width="250px" MaxLength="50" />
                </div>
            </div>
                <div class="row">
                    <b>Ticket Body</b><br />
                           <asp:Label runat="server" ID="lblBody" Width="250px" MaxLength="50" />
                </div>
            </div>--%>
            <table width="100%">
               <tr>
                    <td align="center" class="pagefilter">
                        Please enter ticket Id :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                           />
                    </td>
                    <td align="left" class="newlink">
                     <asp:Button Text="Search" class="btn btn-search" OnClick="Submit" runat="server"   ValidationGroup="DocumentsValidation" />   
                         
                    </td>
                </tr>
              
                 </table><br /><br />
               <table width="100%" runat="server" id="table1" visible="false" cellpadding="4"  >
               
                   <tr>
                       <td>
                           Ticket ID:
                           <asp:Label runat="server" ID="txtTicketID" Width="250px" MaxLength="50"/>
                           
                       </td>
                       <td>
                            Created Date:
                           <asp:Label runat="server" ID="txtTicDate" Width="250px" MaxLength="50"/>
                       </td>
                   </tr>
                   <tr>
                       <td>
                           Dept Name:
                           <asp:Label runat="server" ID="txtDeptName" Width="250px" MaxLength="50"/>
                       </td>
                       <td>
                           Topic:
                           <asp:Label runat="server" ID="txtTopic" Width="250px" MaxLength="50" />
                       </td>
                   </tr>
                   <tr>
                       <td>
                           Status:
                           <asp:Label runat="server" ID="txtStatus" Width="250px" MaxLength="50" />
                       </td>
                       <td>
                           Priority:
                           <asp:Label runat="server" ID="txtPriority" Width="250px" MaxLength="50" />
                       </td>
                   </tr>
                   <tr>
                       <td colspan="2">
                           <b>Ticket Threads:-</b><br />
                           <asp:Label runat="server" ID="lblBody"  MaxLength="50"/>
                       </td>
                   </tr>
            </table><br /><br />
               <br /><br />
             <table width="100%">
               <tr>
                    <td align="center" class="pagefilter">
                     <div id="data" runat="server"> </div>
                    </td>
                  
                </tr>
              
                 </table>

        </ContentTemplate>
    </asp:UpdatePanel>

        </div>
</asp:Content>
