﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddTemplateSection.aspx.cs" ValidateRequest="false" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddTemplateSection" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title></title>
    <!-- Bootstrap CSS -->
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="/NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="/NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="/NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/NewCSS/style.css" rel="stylesheet" />
    <link href="/NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
     <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <%--<link href="/NewCSS/summernote.css" rel="stylesheet" />
    <script src="/Newjs/summernote.js"></script>--%>

    <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script type="text/javascript">

        //$(document).ready(function () {
            <%--$('textarea#' + '<%=tbxContent.ClientID%>').summernote({
                height: 200
            });--%>
            
        //});  

        bind_tinyMCE();

        function bind_tinyMCE() {

                tinymce.remove();
                tinymce.init({
                    mode: "specific_textareas",
                    editor_selector: "myTextEditor",
                    width: '100%',
                    height: 400,
                    toolbar: ' undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect',
                    font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                    fontsize_formats: '8pt 10pt 11pt 12pt 13pt 14pt 15pt 18pt 24pt 36pt',
                    statusbar: false,
                    setup: function (editor) {
                        editor.on('change', function () {
                            tinymce.triggerSave();
                        });
                    }
                });                
            }

        function CloseMe() {            
            window.parent.CloseSectionModal();
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }
    </script>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="smTemplateSection" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upTemplateSection" runat="server">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCTemSec">
                            <asp:Label runat="server" ID="successmsgaCTemSec" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsTemplateSection" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                 ValidationGroup="TemplateSectionValidationGroup" />
                            <asp:CustomValidator ID="cvTemplateSection" runat="server" EnableClientScript="False"
                                ValidationGroup="TemplateSectionValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />                           
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group required col-md-8">
                                <label for="tbxTemplateName" class="control-label">Section Header</label>
                                <asp:TextBox runat="server" ID="tbxHeader" CssClass="form-control" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="rfvHeader" ErrorMessage="Please Enter Header" ControlToValidate="tbxHeader"
                                    runat="server" ValidationGroup="TemplateSectionValidationGroup" Display="None" />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="tbxVersion" class="control-label">Version</label>
                                <asp:TextBox runat="server" ID="tbxVersion" CssClass="form-control" autocomplete="off" onkeypress="return isNumberKey(event)" />
                                <%--<asp:RequiredFieldValidator ID="rfvVersion" ErrorMessage="Required Version" ControlToValidate="tbxVersion"
                                    runat="server" ValidationGroup="TemplateSectionValidationGroup" Display="None" />--%>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group required col-md-12">
                                <label for="tbxVersion" class="control-label">Section Content</label>
                                <asp:TextBox runat="server" ID="tbxContent" CssClass="form-control myTextEditor" TextMode="MultiLine" Rows="3" Width="100%"/>
                                <asp:RequiredFieldValidator ID="rfvContent" ErrorMessage="Please Enter Content" ControlToValidate="tbxContent"
                                    runat="server" ValidationGroup="TemplateSectionValidationGroup" Display="None" />
                            </div>
                        </div>

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary"
                                    ValidationGroup="TemplateSectionValidationGroup" OnClick="btnSave_Click" />                                 
                                <asp:Button Text="Close" runat="server" ID="btnCancel"
                                    CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
               
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
