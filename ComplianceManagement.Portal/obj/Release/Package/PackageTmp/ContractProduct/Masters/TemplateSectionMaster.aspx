﻿<%@ Page Title="Section Master:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="TemplateSectionMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.TemplateSectionMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        
         function OpenSectionModal(sectionID) {
            $('#TemplateSectionPopup').modal('show');
            $('#<%# IframeTemplateSection.ClientID%>').attr('src', "/ContractProduct/Masters/AddTemplateSection.aspx?accessID=" + sectionID);
        }

         function CloseSectionModal() {             
             $('#TemplateSectionPopup').modal('hide');
             document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
         }

         function CloseMe() {
              $('#TemplateSectionPopup').modal('hide');
             document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
         }

         function RefreshParent() {
             document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }
       
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Masters/ Template Section(s)');
        });
    </script>
    <style type="text/css">
        .AddNewspan1 {
    padding: 0px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                
                <asp:ValidationSummary ID="vsContractTemplatePage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ContractTemplatePageValidationGroup" />
                <asp:CustomValidator ID="cvContractTemplatePage" runat="server" EnableClientScript="False"
                    ValidationGroup="ContractTemplatePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row col-md-12 colpadding0">
                <div class="col-md-6 colpadding0">
                    <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" PlaceHolder="Type Section Name to Search" AutoPostBack="true"
                        OnTextChanged="tbxFilter_TextChanged" Width="70%" AutoComplete="off"/>
                </div>

                <div class="col-md-4 colpadding0">
                    <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                </div>

                <div class="col-md-2 colpadding0">
                    <div class="col-md-6 colpadding0">
                        <asp:LinkButton Text="Back" CssClass="btn btn-primary" ToolTip="Go to Previous (Template Master)" data-toggle="tooltip"
                            runat="server" ID="lnkbtnPrevious" Width="100%" OnClick="lnkPreviousSwitch_Click" />
                    </div>
                    <div class="col-md-6 text-right">
                        <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"
                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add a New Section">
                            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdTemplateSections" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                    AllowSorting="true" GridLines="none" Width="100%" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCommand="grdTemplateSections_RowCommand" OnRowCreated="grdTemplateSections_RowCreated" OnSorting="grdTemplateSections_Sorting">
                    <%-- OnRowDataBound="grdContractType_RowDataBound" --%>
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%"> 
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                                <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Header" HeaderText="Section" ItemStyle-Width="25%" SortExpression="Header"/> 
                        <asp:BoundField DataField="Version" HeaderText="Version" ItemStyle-Width="10%" SortExpression="Version"  HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center"/>                       
                       <%-- <asp:BoundField DataField="BodyContent" HeaderText="Section Content" ItemStyle-Width="50%" />--%>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Section Content" ItemStyle-Width="50%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label ID="lblSectionContent" runat="server" Text='<%# Eval("BodyContent") %>'
                                        ToolTip='<%# Eval("BodyContent") %>' data-toggle="tooltip"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" 
                            HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditSection" runat="server" CommandName="EDIT_Section"
                                    ToolTip="Edit Section" data-toggle="tooltip" CommandArgument='<%# Eval("ID") %>'>
                                    <img src='<%# ResolveUrl("/Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteSection" runat="server" CommandName="DELETE_Section" ToolTip="Delete Section" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Section Detail?');">
                                    <img src='<%# ResolveUrl("/Images/delete_icon_new.png")%>' alt="Delete" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">                                
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-3 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 25%; float: right; margin-right: 3%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
    </div>

      <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true" onclick="CloseMe();">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeTemplateSection" runat="server" frameborder="0" width="100%" height="450px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
