﻿<%@ Page Title="Contract Type Master:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Contract.Masters.ContractType" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fopenpopup() {
            $('#divContTypeDialog').modal('show');
        }

        function OpenCaseTypePopup(ContractTypeID) {
            $('#AddContTypePopUp').modal('show');
            $('#ContentPlaceHolder1_IframeCaseType').attr('src', "/ContractProduct/Masters/AddType.aspx?ContractTypeId=" + ContractTypeID);
        }

        function CloseContractTypePopUp() {
            $('#AddContTypePopUp').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }

        function RefreshParent() {
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }

    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Masters/ Contract Type');
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsContractTypePage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="ContractTypePageValidationGroup" />
                <asp:CustomValidator ID="cvContractTypePage" runat="server" EnableClientScript="False"
                    ValidationGroup="ContractTypePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-offset-12">
                    <div class="col-md-6 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" PlaceHolder="Type to Search" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" Width="70%"/>
                    </div>
                    <div class="col-md-4 colpadding0">
                        <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>
                    <div class="col-md-2 colpadding0 text-right">
                        <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"
                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add a New Contract Type">
                            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdContractType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCreated="grdContractType_RowCreated" OnSorting="grdContractType_Sorting" OnRowDataBound="grdContractType_RowDataBound" OnRowCommand="grdContractType_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                                <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Contract Type" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="70%" SortExpression="TypeName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("TypeName") %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sub-Type(s)" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lblSubConttype" runat="server" class="newlink" Font-Underline="True" ToolTip="View/Edit Sub-Type(s)" data-toggle="tooltip"
                                    data-placement="bottom" CommandName="VIEW_ContractType" CommandArgument='<%# Eval("ID") %>'>Sub-Type(s)</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContractType" runat="server" CommandName="EDIT_ContType"
                                    ToolTip="Edit" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>'>
                                            <img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteContractType" runat="server" Visible="false" CommandName="DELETE_ContType" ToolTip="Delete" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Contract Type? This will also deleted associated Contract Sub-Type(s).');">
                                                <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete" title="Delete Contract Type" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width:100%; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>

                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Previous" CssClass="btn btn-primary" ToolTip="Go to Previous Master (Vendor)" data-toggle="tooltip" OnClick="lnkPreviousSwitch_Click" runat="server" ID="LnkbtnPrevious" Width="100%" />
                        </div>
                        <div class="col-md-10 colpadding0">
                        </div>
                    </div>
                    <div class="col-md-6 colpadding0">
                        <div class="col-md-10 colpadding0">
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" ToolTip="Go to Next Master (Document Type)" data-toggle="tooltip" OnClick="lnkNextSwitch_Click" ID="LnkbtnNext" Style="float: right; width: 100%;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddContTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeCaseType" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
