﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="KendoReportNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Report.KendoReportNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<link rel="dns-prefetch" href="https://reports.avantis.co.in"> 
<link rel="preconnect" href="https://reports.avantis.co.in">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
        
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>  
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>

    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <%--<script type="text/javascript" src="../js/telerikReportViewer.kendo-12.1.18.620.js"></script>--%>
    <script type="text/javascript" src="../js/telerikReportViewer-12.1.18.620.js"></script>
    
<style type="text/css">
     .k-grid-header-wrap.k-auto-scrollable {
            width: 99.9%;
        }
        table.k-selectable {
            border-right: 1px solid #ceced2;
        }
    .col-md-12.col-sm-12.col-xs-12.descriptionstatus {
        padding-left: 9px;
    }

    p.title {
        padding-left: 10px !important;
    }

    .p1 {
        min-height: 100px;
    }

    div.k-grid {
        border: none;
    }

    .col-md-4.col-sm-12.col-xs-12 {
        min-height: 539px;
    }

    .k-grid tbody .k-button {
        min-width: 30px;
        min-height: 30px;
        border-radius: 35px;
        margin: 0px 25px 0px -10px !important;
    }

    pan.k-icon.k-i-pin {
        display: none;
    }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

.k-state-default > .k-select {
    border-color: #ceced2 !important;
    margin-top: 0px !important;
}

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            /*background: #f8f8f8;*/
            background: #1fd9e1;            
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        /*td.k-command-cell {
            border-width: 0 1px 0 1px;
        }*/

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-grid-content {
            min-height: 394px !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

            fhead('My Reports / Standard Report');
            setactivemenu('Kendoreport');
            fmaters1();
            $("#Startdatepicker1").kendoDatePicker({ 
                max: new Date(),
                change: changedate			
            });
            $("#Enddatepicker").kendoDatePicker({    
                max: new Date(),
                change: changeEnddate
            });

            function changedate() {
                settracknew('Standard Report', 'Filtering ', 'Start Date, Statutaory and Internal', '');			
            }
            function changeEnddate() {
                settracknew('Standard Report', 'Filtering ', 'End Date, Statutaory and Internal', '');
            }
            //exapand
            $(".panel-wrap").on("click", "span.k-i-sort-desc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-desc-sm")
                    .addClass("k-i-sort-asc-sm");

                kendo.fx(contentElement).expand("vertical").stop().play();
            });

            //collapse
            $(".panel-wrap").on("click", "span.k-i-sort-asc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-asc-sm")
                    .addClass("k-i-sort-desc-sm");

                kendo.fx(contentElement).expand("vertical").stop().reverse();
            });
        });

        function placeholder(element) {
            return element.clone().addClass("placeholder");
        }

        function hint(element) {
            return element.clone().addClass("hint")
                .height(element.height())
                .width(element.width());
        }
    </script>

    <style>
        .m-t {
            margin-top: 38px;
        }

        .mr10 {
            margin-left: 15px;
        }

        .ml5 {
            margin-left: 10px;
        }

        .map {
        }

        .columnchart {
        }

        #example, #example1 {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .dash-head {
            width: 970px;
            height: 80px;
            /*background: url('../content/web/sortable/dashboard-head.png') no-repeat 50% 50% #222222;*/
        }

        .panel-wrap {
            display: table;
        }

        .widget.placeholder {
            opacity: 0.4;
            border: 1px dashed #a6a6a6;
        }

        /* WIDGETS */
        .widget {
            padding: 0;
            background-color: #ffffff;
            border-radius: 3px;
            cursor: move;
        }

            .widget:hover {
                background-color: #fcfcfc;
                border-color: #cccccc;
            }

            .widget div {
                /*padding: 10px;
                    min-height: 50px;*/
            }

            .widget h3 {
                margin-top: 0px;
                margin-bottom: 0px;
                font-size: 23px;
                padding: 8px 10px;
                /*text-transform: uppercase;*/
                border-bottom: 1px solid #e7e7e7;
            }

                .widget h3 span {
                    float: right;
                }

                    .widget h3 span:hover {
                        cursor: pointer;
                        background-color: #e7e7e7;
                        border-radius: 20px;
                    }

        /* PROFILE */
        .profile-photo {
            /*width: 80px;
                    height: 80px;*/
            /*margin: 10px auto;*/
            border-radius: 100px;
            border: 1px solid #e7e7e7;
            background: url('../content/web/Customers/ISLAT.jpg') no-repeat 50% 50%;
        }

        #profile div {
            /*text-align: center;*/
        }

        #profile #profile1 h4 {
            color: #1f97f7;
        }

        #profile p {
            /*margin: 0 0 10px;*/
        }

        #teammates teammates1 teammates2 h4,
        #blogs h4,
        #news h4 {
            width: auto;
            font-size: 1.4em;
            color: #1f97f7;
            font-weight: normal;
        }

        .blog-info {
            font-size: .9em;
            color: #787878;
        }

          

            #mainsummery #news h4 {
                font-size: 1.2em;
                line-height: 1.4em;
                height: 40px;
            }

                #mainsummery #news h4 span {
                    display: block;
                    float: left;
                    width: 100px;
                    height: 40px;
                    color: #000;
                }

            #teammates1, #teammates2, #teammates3, #teammates4, #teammates5, #teammates6 {
                margin-left: 15px;
                margin-bottom: 15px;
            }
            /* TEAMMATES */
            .team-mate:after {
                content: ".";
                display: block;
                height: 0;
                line-height: 0;
                clear: both;
                visibility: hidden;
            }
            /*#teammates1, #teammates2,  #teammates5{
                 margin-right:15px;
                 margin-bottom:15px;
             }*/
            #teammates #teammates1 #teammates2 #teammates3 #teammates4 #teammates5 .team-mate h4 {
                font-size: 1.4em;
                font-weight: normal;
                /*margin-top: 12px;*/
            }

            .team-mate p {
            }

            .team-mate img {
                float: left;
                margin: 0 15px 0 0;
                border: 1px solid #e7e7e7;
                border-radius: 60px;
            }

            .hint {
                width: 250px;
                height: 100px;
                overflow: hidden;
            }

                .hint > h3 {
                    padding-left: 20px;
                }

            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: not-allowed;
                background-color: #ffffff;
                color: #000000;
            }

            .select_Date {
                width: 100%;
                margin-right: 0;
            }

            .icongcalender {
                color: #666;
            }

            .m10 {
                margin-left: 10px;
            }

            .k-icon-15 {
                font-size: 15px; /* Sets icon size to 32px */
            }
            .Dashboard-white-widget {
                background: none;
            }

            .pointimg {
                width: 31.5%;
                cursor: pointer;
                padding: 8px;
                border-radius: 12px;
            }
            .imgheader {
                 margin-top: 6px;
                 color:#0487c4;
                 /*font-size: 30px;*/
            }
            .keyFeature {
                  margin-top: 10px;
                  font-size: 18px;
                  padding-bottom: 10px;
                  padding-left:10px;
                  color:black;
                      /*border-top: 1px solid #e7e7e7;*/
            }
          
            .set_bullet {
                 list-style: disc;
            }

            #teammates121, #teammates2, #teammates3, #teammates412, #teammates5 {
                -webkit-box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
                -moz-box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
                box-shadow: 0px 3px 2px 0px rgba(142,143,153,1);
            }

            .imgbackshowd {
                -webkit-box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    -moz-box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    box-shadow: 1px 2px 6px 2px rgba(142,143,153,0.5);
    padding-left: -11px;
    /* border: 2px solid #e2dcd4; */
    /* border-color: #e2dcd4; */
    /* border-width: thin; */
    /* -webkit-box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6); */
    -moz-box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6);
    /* box-shadow: 1px 4px 6px 3px rgba(142,143,153,0.6); */
            }

        </style>

     <script id="TypeTemplate" type="text/x-kendo-template">
                 
            #=GetFileType(ReportType)# 
        
    </script>

    <script type="text/javascript">
                
        function myFunctionShowInternal() {
            
            var z = document.getElementById("lblStatusInternalReport");
            z.style.display = "block";

            var a = document.getElementById("lblStatusStatutoryReport");
            a.style.display = "none";

            document.getElementById("ComplianceType").value = "I";
            WindowpopupDetail1();
        }

        function myFunctionShowStatutory() {
            document.getElementById("ComplianceType").value = "S";
            WindowpopupDetail1();

            var z = document.getElementById("lblStatusInternalReport");
            z.style.display = "none";

            var a = document.getElementById("lblStatusStatutoryReport");
            a.style.display = "block";
        }   
        
        function ClearCalendar()
        {
            $("#Startdatepicker1").val('');
            $("#Enddatepicker").val('');
        }

        function GetFileType(value)
        {
            if (value == "S") {
                return "Statutory";
            }
            else if (value == "I") {
                return "Internal";
            }
            else {
                return "";
            }
            
        }

        $(document).ready(function () {

            document.getElementById("ComplianceType").value = "S";
            WindowpopupDetail1();
         //   report();
           // locationreport();
            //userreport();
            //categoryreport();
            //Summaryreport();
      });
        
        //Critical Risk
        function OpenstatupCriticalWindow() {    
            if ($("#ComplianceType").val() == "S") {
                $('#windowriskpopup').show();
                var myWindowAdv = $("#windowcriticalriskpopup");
                function onClose() {
                    myWindowAdv.data("kendoWindow").hide();
                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Critical Risk Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: function () {
                    },
                });
                CriticalRiskreport();
                myWindowAdv.data("kendoWindow").center().open();
            }
            else {
                $('#windowInternalcriticalriskpopup').show();
                var myWindowAdv = $("#windowInternalcriticalriskpopup");
                function onClose() {
                    myWindowAdv.data("kendoWindow").hide();
                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Critical Risk Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: function () {
                    },
                });
                InternalCriticalRiskreport();
                myWindowAdv.data("kendoWindow").center().open();
            }
        }

        function InternalCriticalRiskreport() {
 
            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }
            if (!$("#windowInternalcriticalriskpopup").data("telerik_ReportViewer")) 
            {
                $("#windowInternalcriticalriskpopup").telerik_ReportViewer({

                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },
                reportSource: {
                    report: "Samples/CriticalInternalReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                   
                },
                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });
            }
            else
            {
                var viewer = $("#windowInternalcriticalriskpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }
        }
        function CriticalRiskreport() {
            var SD = '';    
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }        
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }
            if (!$("#windowcriticalriskpopup").data("telerik_ReportViewer")) 
            {
                $("#windowcriticalriskpopup").telerik_ReportViewer({

                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },
                    reportSource: {
                        report: "Samples/CriticalStatutoryReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }                        
                    },
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                    },
                });
            }
            else
            {
                var viewer = $("#windowcriticalriskpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }
        }

      //Risk
      $(".close-button").click(function () {
          $("#windowriskpopup").close();
      });
      function OpenstatupWindow() {    
          if ($("#ComplianceType").val() == "S") {
              $('#windowriskpopup').show();
              var myWindowAdv = $("#windowriskpopup");
              function onClose() {
                  myWindowAdv.data("kendoWindow").hide();
              }
              myWindowAdv.kendoWindow({
                  width: "87%",
                  height: "87%",
                  title: "Overall Risk Report",
                  visible: false,
                  actions: [
                      "Pin",
                      "Close"
                  ],
                  close: function () {
                  },
              });
              report();
              myWindowAdv.data("kendoWindow").center().open();
          }
          else {
              $('#windowInternalriskpopup').show();
              var myWindowAdv = $("#windowInternalriskpopup");
              function onClose() {
                  myWindowAdv.data("kendoWindow").hide();
              }
              myWindowAdv.kendoWindow({
                  width: "87%",
                  height: "87%",
                  title: "Overall Risk Report",
                  visible: false,
                  actions: [
                      "Pin",
                      "Close"
                  ],
                  close: function () {
                  },
              });
              Internalreport();
              myWindowAdv.data("kendoWindow").center().open();
          }
      }
        function Internalreport() {
 
            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }
            if (!$("#windowInternalriskpopup").data("telerik_ReportViewer")) 
            {
             $("#windowInternalriskpopup").telerik_ReportViewer({

                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },
                reportSource: {
                    report: "Samples/InternalFFRiskSummaryReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    //parameters: { Customerid:<% =CustId%>, UserID:<% =UID%> }
                },
                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });
            }
            else
            {
                var viewer = $("#windowInternalriskpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }

           <%-- $("#windowInternalriskpopup").telerik_ReportViewer({

                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },
                reportSource: {
                    report: "Samples/InternalFFRiskSummaryReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                    //parameters: { Customerid:<% =CustId%>, UserID:<% =UID%> }
                },
                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }
        function report() {
            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }        
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }
            if (!$("#windowriskpopup").data("telerik_ReportViewer")) 
            {
                $("#windowriskpopup").telerik_ReportViewer({

                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },
                    reportSource: {
                        report: "Samples/FFRiskSummaryReport280918",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                        //parameters: { Customerid:<% =CustId%>, UserID:<% =UID%> }
                    },
                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowriskpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }



         <%--   $("#windowriskpopup").telerik_ReportViewer({

                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },
                reportSource: {
                    report: "Samples/FFRiskSummaryReport280918",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                    //parameters: { Customerid:<% =CustId%>, UserID:<% =UID%> }
                },
                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }


     // location

        function OpenstatupLocationWindow() {
            if ($("#ComplianceType").val() == "S") {
                $('#windowlocationpopup').show();
                var myWindowAdv = $("#windowlocationpopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Location Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                locationreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
            else {
                 $('#windowInternallocationpopup').show();
                var myWindowAdv = $("#windowInternallocationpopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Location Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                Internallocationreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
      }
        function Internallocationreport() {

            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }           
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }
            if (!$("#windowInternallocationpopup").data("telerik_ReportViewer")) 
            {
                $("#windowInternallocationpopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },
                    reportSource: {
                        report: "Samples/InternalMLReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                        //parameters: { Customerid:5,UserID:38 }

                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowInternallocationpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                     parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }

            <%-- $("#windowInternallocationpopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },
                    reportSource: {
                        report: "Samples/InternalMLReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                        //parameters: { Customerid:5,UserID:38 }

                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });--%>
        }
        function locationreport() {

            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }            
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }

            if (!$("#windowlocationpopup").data("telerik_ReportViewer")) 
            {
                $("#windowlocationpopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },
                    reportSource: {
                        report: "Samples/MLReport280918",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                        //parameters: { Customerid:5,UserID:38 }

                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowlocationpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }

           <%-- $("#windowlocationpopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },
                reportSource: {
                    report: "Samples/MLReport280918",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                    //parameters: { Customerid:5,UserID:38 }

                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }


      //User

        function OpenstatupUserWindow()
        {
            if ($("#ComplianceType").val() == "S") {
                $('#windowuserpopup').show();
                var myWindowAdv = $("#windowuserpopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall User Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                userreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
            else {
                 $('#windowInternaluserpopup').show();
                var myWindowAdv = $("#windowInternaluserpopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall User Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                Internaluserreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
      }
         function Internaluserreport() {
             var SD = '';  
             //var SD = '2015-01-01';  
             if (new Date().getMonth()  < 3 ) 
             {
                 SD = new Date().getFullYear() - 1 + '-04-01';                   
             }
             else
             {
                 SD = new Date().getFullYear() + '-04-01';                    
             }         
             var ED = new Date();

             if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
             {
                 if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                     SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                 }
                 if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                     ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                 }
                 else
                 {
                     ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                 }
             }

             if (!$("#windowInternaluserpopup").data("telerik_ReportViewer")) 
             {
                 $("#windowInternaluserpopup").telerik_ReportViewer({
                     serviceUrl: "<% =DiagraphPath%>",
                     reportServer: {
                         url: "<% =DiagraphPath%>",
                         username: "<% =UserIdDiagraph%>",
                         password: "<% =PassIdDiagraph%>"
                     },

                     reportSource: {
                         report: "Samples/InternalUserReport",
                         parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                     },

                     viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                     scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                     scale: 1.0,
                     enableAccessibility: true,
                     ready: function () {
                         //this.refreshReport();
                     },
                 });
             }
             else
             {
                 var viewer = $("#windowInternaluserpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                     parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
             }


           <%-- $("#windowInternaluserpopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },

                reportSource: {
                    report: "Samples/InternalUserReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }
        function userreport() {
            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }     
            var ED = new Date();

            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }

            if (!$("#windowuserpopup").data("telerik_ReportViewer")) 
            {
                $("#windowuserpopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/UserReport280918",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    PersistSession: false,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
             var viewer = $("#windowuserpopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED } 
                });
                viewer.refreshReport();
            }


            //old code
           <%-- $("#windowuserpopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },

                reportSource: {
                    report: "Samples/UserReport280918",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }


      //Category
      
        function OpenstatupCategoryWindow()
        {
            if ($("#ComplianceType").val() == "S")
            {
                $('#windowCategorypopup').show();
                var myWindowAdv = $("#windowCategorypopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Category Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                categoryreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
            else
            {
                 $('#windowInternalCategorypopup').show();
                var myWindowAdv = $("#windowInternalCategorypopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Category Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                Internalcategoryreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
      }
        function Internalcategoryreport() {


            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }           
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }

            if (!$("#windowInternalCategorypopup").data("telerik_ReportViewer")) 
            {
                $("#windowInternalCategorypopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/InternalFinalCategorytReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });

              
            }
            else
            {
                var viewer = $("#windowInternalCategorypopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }


              <%--$("#windowInternalCategorypopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/InternalFinalCategorytReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });--%>
        }

        function categoryreport() {

            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }        
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }

            if (!$("#windowCategorypopup").data("telerik_ReportViewer")) 
            {

                $("#windowCategorypopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/FinalCategorytReport280918",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowCategorypopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }

         <%--   $("#windowCategorypopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },

                reportSource: {
                    report: "Samples/FinalCategorytReport280918",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }
      
        function bindpopupDetails(ReportFlag) {
          
         var grid1 = $("#grid1").kendoGrid({
             dataSource: {
                 //serverPaging: false,
                   autoBind: true,
                   transport: {
                       read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=False&ReportFlag=' + ReportFlag + '&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                   <%--  read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=False&ReportFlag=' + ReportFlag + '&Compliancetype=' + $("#ComplianceType").val(),
                      dataType: "json",--%>
                 },
                 total: function (response) {
                     return response.count;
                 },
                 pageSize: 10,
              },
              height: 500,
              sortable: true,
              filterable: true,
              columnMenu: true,              
             //serverPaging: false,
             pageable: {
                 refresh: true,
                  pageSize: 12,
                  pageSizes: true
              },
              reorderable: true,
              resizable: true,
              multi: true,
             pageable:true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      width: "30%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                       width: "30%;",
                      template: "#= kendo.toString(kendo.parseDate(created_at, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                      //template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",

                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                                 
                              }
                          }
                      }
                  },
                    {
                     field: "ReportType", title: 'Type',   
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
             ]
          }); 

          $(document).on("click", "#grid1 tbody tr .ob-download", function (e) {
              var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
              return false;
            });

             $("#grid1").kendoTooltip({
              filter: ".k-grid-edit1",
              content: function (e) {
                  return "Download";
              }
          });
      }

      function bindbasepage() {
           var grid12 = $("#grid12").kendoGrid({
              dataSource: {
                  transport: {
                    read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=ComprehensiveReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=ComprehensiveReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= lbgotoReport.ClientID %>').style.display = "block";
                          }
                          else {
                                 // document.getElementById('<%= lbgotoReport.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
              multi: true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,                     
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                      width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                  },
                  {
                      field: "ReportType", title: 'Type',
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 
          $("#grid12").kendoTooltip({
              filter: ".k-grid-edit1",
              content: function (e) {
                  return "Download";
              }
          });

          $(document).on("click", "#grid12 tbody tr .ob-download", function (e) {

              var item = $("#grid12").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
               return false;
              //return true;
          });

         
          var grid12 = $("#gridLocation").kendoGrid({
              dataSource: {
                  transport: {
                   read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=LocationReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=LocationReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton1.ClientID %>').style.display = "block";
                          }
                          else {
                                 // document.getElementById('<%= LinkButton1.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
              },
              height: 152,
              multi: true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                      width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                  },
                 {
                     field: "ReportType", title: 'Type',   
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 
             $("#gridLocation").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

          $(document).on("click", "#gridLocation tbody tr .ob-download", function (e) {
              var item = $("#gridLocation").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
              return false;
              //return true;
          });

        
            var grid123 = $("#gridUser").kendoGrid({
              dataSource: {
                  transport: {
                   read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=UserReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=UserReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton2.ClientID %>').style.display = "block";
                          }
                          else {
                                 // document.getElementById('<%= LinkButton2.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
              multi: true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                    width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                  },
                    {
                     field: "ReportType", title: 'Type',  
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 

          $(document).on("click", "#gridUser tbody tr .ob-download", function (e) {

              var item = $("#gridUser").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
              return false;
              //return true;
          });

           $("#gridUser").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

         
            var grid12 = $("#gridCategory").kendoGrid({
              dataSource: {
                  transport: {
                  read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=CategoryReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=CategoryReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton3.ClientID %>').style.display = "block";
                          }
                          else {
                                  //document.getElementById('<%= LinkButton3.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
              multi: true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                      width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                  },
                 {
                     field: "ReportType", title: 'Type',  
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 

            $("#gridCategory").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

          $(document).on("click", "#gridCategory tbody tr .ob-download", function (e) {

              var item = $("#gridCategory").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
              return false;
              //return true;
          });

        


            var grid12 = $("#gridRisk").kendoGrid({
              dataSource: {
                  transport: {
                  read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=RiskReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=RiskReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton4.ClientID %>').style.display = "block";
                          }
                          else {
                                  //document.getElementById('<%= LinkButton4.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
                multi: true,
                columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                     width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                    },
                   {
                       field: "ReportType", title: 'Type',   
                        template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 

            $("#gridRisk").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
          });


            var grid12 = $("#gridCriticalRisk").kendoGrid({
              dataSource: {
                  transport: {
                  read: {
                           url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=CriticalRiskReport&Compliancetype=' + $("#ComplianceType").val(),
                           dataType: "json",
                           beforeSend: function (request) {
                               request.setRequestHeader('Authorization', '<% =Authorization%>');
                           },
                       }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=RiskReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton6.ClientID %>').style.display = "block";
                          }
                          else {
                                  //document.getElementById('<%= LinkButton4.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
                multi: true,
                columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                     width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                    },
                   {
                       field: "ReportType", title: 'Type',   
                        template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 

            $("#gridCriticalRisk").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
          });




            var grid12 = $("#gridDetaild").kendoGrid({
              dataSource: {
                  transport: {
                      read: {
                          url: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=DetailReport&Compliancetype=' + $("#ComplianceType").val(),
                          dataType: "json",
                          beforeSend: function (request) {
                              request.setRequestHeader('Authorization', '<% =Authorization%>');
                          },
                      }
                      //read: '<% =Path%>data/KendoMyReportData?UserId=<% =UID%>&CustomerID=<% =CustId%>&Flagfortwo=True&ReportFlag=DetailReport&Compliancetype=' + $("#ComplianceType").val(),
                  },
                  schema: {
                      data: function (response)
                      {
                          if (response.length >= 3) {
                              document.getElementById('<%= LinkButton5.ClientID %>').style.display = "block";
                          }
                          else {
                                 // document.getElementById('<%= LinkButton5.ClientID %>').style.display = "none";
                          }
                          return response;
                      }
                  },
                  pageSize: 3,
                },
                height:152,            
                multi: true,
              columns: [
                  {
                      field: "pdf_name", title: 'name',
                      hidden: true,
                      attributes: {
                          style: 'white-space: nowrap;'

                      }
                  }, {
                      field: "created_at", title: 'Week',
                      type: "date",
                      template: "#='Week of'# #= kendo.toString(kendo.parseDate(created_at, 'yyyy MM dd'), 'dd-MMM-yyyy') #",
                      width: "50%;",
                      attributes: {
                          style: 'white-space: nowrap;'

                      }, filterable: {
                          extra: false,
                          operators: {
                              string: {
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      }
                  },
                 {
                     field: "ReportType", title: 'Type',  
                      template: kendo.template($('#TypeTemplate').html())
                  },
                  {
                      command: [
                          { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                          //{ name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                      ], title: "Action", lock: true,// width: 150,

                  }
              ]
          }); 

            $("#gridDetaild").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Download";
                }
            });

          $(document).on("click", "#gridDetaild tbody tr .ob-download", function (e) {

              var item = $("#gridDetaild").data("kendoGrid").dataItem($(this).closest("tr"));
              OpenDownloadOverviewpup(item.id)
              return false;
              //return true;
          });



      }

      $(document).on("click", "#gridRisk tbody tr .ob-download", function (e) {

          var item = $("#gridRisk").data("kendoGrid").dataItem($(this).closest("tr"));
          OpenDownloadOverviewpup(item.id)
          return false;
          //return true;
      });

  

        function OpenDownloadOverviewpup(Id) {
            settracknew('Standard Report', 'Action ', 'Download (six different reports)', '');
            $('#downloadfile').attr('src', "../Common/DownloadReportDocument.aspx?Id=" + Id);

             return false;
      }

       
      //windowpopupDetail

      function WindowpopupDetail1() {
          bindbasepage();

           var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
           $('#grid1').empty();
            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });

          $("#Startdatepicker").kendoDatePicker({

              change: onChange
          });

           function onChange() {             

              
            }

      }

  

        function OpenwindowpopupLocationDetail(e) {

             var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
              $('#grid1').empty();
            bindpopupDetails("LocationReport");//change for location


               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

         function OpenwindowpopupDetaildDetail(e) {
            var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
              $('#grid1').empty();
            bindpopupDetails("DetailReport");


               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
      }
      
        function OpenwindowpopupCategoryDetail(e) {
            var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
              $('#grid1').empty();
            bindpopupDetails("CategoryReport");


               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
      }

        function OpenwindowpopupUserDetail(e) {

             var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
              $('#grid1').empty();

            bindpopupDetails("UserReport");//change for location
           
               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
      }

        
        function OpenwindowpopupCriticalRiskDetail(e) {
            var grid = $('#grid1').data("kendoGrid"); 
            if (grid != undefined || grid != null)
                $('#grid1').empty();

            bindpopupDetails("CriticalRiskReport");//change for location
           
            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenwindowpopupRiskDetail(e) {
            var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
                $('#grid1').empty();

            bindpopupDetails("RiskReport");//change for location
           
               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
      }

       
        function OpenwindowpopupDetail(e) {
           var grid = $('#grid1').data("kendoGrid"); 
                if (grid != undefined || grid != null)
              $('#grid1').empty();
       bindpopupDetails("ComprehensiveReport");

               var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "45%",
                height: "85%",
                title: "Weekly report details",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
      }




      //windowSummarypopup
        function OpenstatupSummaryWindow() {
            if ($("#ComplianceType").val() == "S") {
                $('#windowSummarypopup').show();
                var myWindowAdv = $("#windowSummarypopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Summary Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                Summaryreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
            else {
                 $('#windowInternalSummarypopup').show();
                var myWindowAdv = $("#windowInternalSummarypopup");
                function onClose() {

                }
                myWindowAdv.kendoWindow({
                    width: "87%",
                    height: "87%",
                    title: "Overall Summary Report",
                    visible: false,
                    actions: [
                        "Pin",
                        "Close"
                    ],
                    close: onClose
                });
                InternalSummaryreport();
                myWindowAdv.data("kendoWindow").center().open();

                return false;
            }
      }
        function InternalSummaryreport() 
        {
            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }        
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }


            if (!$("#windowInternalSummarypopup").data("telerik_ReportViewer")) 
            {
                $("#windowInternalSummarypopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/ComprehensiveInternalReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowInternalSummarypopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
            }

           <%-- $("#windowInternalSummarypopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },

                reportSource: {
                    report: "Samples/ComprehensiveInternalReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }

        function Summaryreport() {

            var SD = '';  
            //var SD = '2015-01-01';  
            if (new Date().getMonth()  < 3 ) 
            {
                SD = new Date().getFullYear() - 1 + '-04-01';                   
            }
            else
            {
                SD = new Date().getFullYear() + '-04-01';                    
            }          
            var ED = new Date();
            if ($("#Startdatepicker1").val() != undefined && $("#Enddatepicker").val() != undefined) 
            {
                if ($("#Startdatepicker1").val()!="" && $("#Startdatepicker1").val()!=undefined && $("#Startdatepicker1").val()!=null && $("#Startdatepicker1").val()!="null") {
                    SD = kendo.toString(kendo.parseDate($("#Startdatepicker1").val()), "yyyy-MM-dd");
                }
                if ($("#Enddatepicker").val()!="" && $("#Enddatepicker").val()!=undefined && $("#Enddatepicker").val()!=null && $("#Enddatepicker").val()!="null") {
                    ED = kendo.toString(kendo.parseDate($("#Enddatepicker").val()), "yyyy-MM-dd");
                }
                else
                {
                    ED=kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
                }
            }

            if (!$("#windowSummarypopup").data("telerik_ReportViewer")) 
            {
                $("#windowSummarypopup").telerik_ReportViewer({
                    serviceUrl: "<% =DiagraphPath%>",
                    reportServer: {
                        url: "<% =DiagraphPath%>",
                        username: "<% =UserIdDiagraph%>",
                        password: "<% =PassIdDiagraph%>"
                    },

                    reportSource: {
                        report: "Samples/ComprehensiveReport",
                        parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                    },

                    viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                    scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                    scale: 1.0,
                    enableAccessibility: true,
                    ready: function () {
                        //this.refreshReport();
                    },
                });
            }
            else
            {
                var viewer = $("#windowSummarypopup").data("telerik_ReportViewer");
                viewer.reportSource({
                    report: viewer.reportSource().report,
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>", StartDate: SD, EndDate: ED }
                });
                viewer.refreshReport();
              
            }

         <%--   $("#windowSummarypopup").telerik_ReportViewer({
                serviceUrl: "<% =DiagraphPath%>",
                reportServer: {
                    url: "<% =DiagraphPath%>",
                    username: "<% =UserIdDiagraph%>",
                    password: "<% =PassIdDiagraph%>"
                },

                reportSource: {
                    report: "Samples/ComprehensiveReport",
                    parameters: { Customerid:<% =CustId%>, UserID:<% =UID%>, CustomerName: "<% =CName%>", Flag: "<% =UserRole%>" }
                },

                viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                scale: 1.0,
                enableAccessibility: true,
                ready: function () {
                    //this.refreshReport();
                },
            });--%>
        }

    </script>
    <style>
        /* Three image containers (use 25% for four, and 50% for two, etc) */

        /* Clear floats after image containers */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }
        .spanDescription {
            text-align: center;
            font-weight: 700;
            font-size: 16px;
        }

        .img-setValue {
             text-align: center;
             margin-top:21px;
             margin-bottom:21px;
        }


        .col-md-4.img-set {
            border-right: 1px solid #aaa;
            padding: 10px;
            text-align: center;
            line-height: 100px;
           
        }

        .col-md-8.img-set {
            border-right: 1px solid #aaa;
            padding: 10px;
            text-align: center;
            line-height: 100px;
           
        }

        .keychallenge {
            /*border: 1px solid silver;*/
            border: 1px solid #1fd9e1;                        
            /*border-radius: 10px;
            padding: 10px;*/
            margin-bottom: 24px;
        }

        .descriptionstatus {
             color:black;
        }

        .Maintitle {
            font-size: 19px;
            font-weight: 700;
            color: blue;
                margin-left: 41%;
        }

        .keychallenge .title {
            /*background-color: #e4e4e4;*/
            background-color: #1fd9e1;
            text-align: left;
            line-height: 25px;
            padding: 5px;
            border-bottom: 1px solid #aaa;
            /*border-bottom: 1px solid #1fd9e1;*/
            margin: 0;
            color: black;
        }
        .col-md-8.span-desc {
            padding: 10px;
        }

        .k-button {
            float: right;
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }
        .Dashboard-white-widget {
            background: white;
        }
        .col-md-4.col-sm-12.col-xs-12 {
            min-height: 600px;
        }
        .p1 {
            min-height: 150px;
        }


      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div class="row" style="background: #fff; height: 50px;">
            <section class="wrapper">
                <div class="col-lg-5 col-md-5" id="lblStatusStatutoryReport">
                    <h1 id="pagetype" style="margin-top: 9px; font-size: 24px;">Statutory Compliance Report </h1>
                </div>
                 <div class="col-lg-5 col-md-5" style="display:none;" id="lblStatusInternalReport">
                    <h1 id="pagetype1" style="margin-top: 9px; font-size: 24px;">Internal Compliance Report </h1>
                </div>
                 <div class="col-lg-3 col-md-3 colpadding0" style="margin-top: 9px;width: 21%;">
                    <input type="text" id="ComplianceType" style="display:none;" />
                  
                </div>
                     <div class="col-lg-2 col-md-2 colpadding0" style="margin-top: 9px;">
               
                </div>
                <div class="col-lg-2 colpadding0" style="margin-top: 9px;">
                    <button type="button" style="margin-left: 16px;" class="btn btn-search" onclick="myFunctionShowStatutory()" id="btnStatutory">Statutory</button>
                    <button type="button" style="margin-left: 5px;" class="btn btn-search" onclick="myFunctionShowInternal()" id="btnInternal">Internal</button>              
                </div>
            </section>
        </div>

    <div>
    <section class="wrapper">
                <div class="col-lg-12 col-md-12" style="margin-left: 62%;">
                   
                     <div class="col-lg-2 col-md-2" style="margin-top: 9px;width: 21%;margin-right: -6%;">
                         <div id="dvStartdatepicker" style="width: 72%;">                         
                            <input id="Startdatepicker1" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;"/>
                        </div>
                    </div>
                         <div class="col-lg-2 col-md-2" style="margin-top: 9px;margin-right: -4%;">
                         <div id="dvEnddatepicker" style="width: 72%;">  
                            <input id="Enddatepicker" placeholder="End Date" CssClass="clsROWgrid" title="Enddatepicker" style="width: 100%;"/>
                        </div>
                    </div>
                    <div class="col-lg-1 colpadding0" style="margin-top: 9px;">
                      <button type="button" class="btn btn-search" onclick="ClearCalendar()" id="btncalendar">Clear</button>
                    </div>

                 </div>
            </section>
       <%-- <section class="wrapper">
                <div class="col-lg-6 col-md-6" >
                   
                </div>--%>
                <%-- <div class="col-lg-6 col-md-6" style="display:none;">
                  
                </div>--%>
               <%--  <div class="col-lg-3 col-md-3 colpadding0" style="margin-top: 9px;width: 21%;">
                     <div id="dvStartdatepicker" style="width: 72%;margin-left: 81px;">                         
                        <input id="Startdatepicker1" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;margin-left: 25px;"/>
                    </div>
                </div>
                     <div class="col-lg-2 col-md-2 colpadding0" style="margin-top: 9px;">
                     <div id="dvEnddatepicker" style="width: 72%;margin-left: 39px;">  
                        <input id="Enddatepicker" placeholder="End Date" CssClass="clsROWgrid" title="Enddatepicker" style="width: 100%;margin-left: 25px;"/>
                    </div>
                </div>
                <div class="col-lg-1 colpadding0" style="margin-top: 9px;margin-left: 3px;">
                  <button type="button" style="margin-left: 25px;" class="btn btn-search" onclick="ClearCalendar()" id="btncalendar">Clear</button>
                </div>
            </section>--%>
    </div>

     <div class="container" style="padding-top: 15px;">
            <div class="row">
               <%-- <p class="Maintitle">Standard Report</p>--%>
                <div id="divStatutory">
               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Overall Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="header1" ToolTip="Open Overall Summary Report" data-toggle="tooltip"  OnClientClick="OpenstatupSummaryWindow();return false;" ImageUrl="../Images/Comprehensive_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title" style="text-align:justify;">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus" style="text-align:justify">
                                <p class="p1">This dynamic report shows the overall summary of the status of your compliances across locations, categories, users, risk profiles, along with details of overdue compliances</p>
                            </div>
                        
                          <div id="grid12"></div> 
                        
                    </div>
                                    
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="lbgotoReport" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupDetail(event);" Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                
                </div>

               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Location Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton1" ToolTip="Open Location Summary Report" data-toggle="tooltip" OnClientClick="OpenstatupLocationWindow();return false;" ImageUrl="../Images/Location_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">
                               <p class="p1"> View the status of your compliances segregated for each of your locations across categories, risk profiles, and users, along with details of ageing of overdue compliances by location</p>
                            </div>
                         <div id="gridLocation"></div> 
                    
                    </div>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton1" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupLocationDetail(event);" Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                   </div>

               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">User Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton2" ToolTip="Open User Summary Report" data-toggle="tooltip" OnClientClick="OpenstatupUserWindow();return false;" ImageUrl="../Images/User_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">
                             <p class="p1">This report shows the status of your compliances managed by each one of your performers and reviewers across categories, risk profiles, and locations, along with details of ageing of overdue compliances</p>
                            </div>
                         <div id="gridUser"></div> 
                    
                    </div>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton2" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupUserDetail(event);"  Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                   </div>
                    
               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Category Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton3" ToolTip="Open Category Summary Report" data-toggle="tooltip" OnClientClick="OpenstatupCategoryWindow();return false;" ImageUrl="../Images/Category_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">
                            <p class="p1">View the status of your compliances segregated by category across locations, risk profiles, and users, along with details of ageing of overdue compliances by category </p> 
                             <br />
                            </div>
                          <div id="gridCategory"></div> 
                     
                    </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton3" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupCategoryDetail(event);" Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                      </div>

               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Risk Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton4" ToolTip="Open Risk Summary Report" data-toggle="tooltip" OnClientClick="OpenstatupWindow();return false;" ImageUrl="../Images/Risk_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">
                           <p class="p1">This dynamic report shows the status of your compliances segregated by risk profiles across locations, categories, and users, along with details of ageing of overdue compliances by risk</p><br />
                            </div>
                          <div id="gridRisk"></div> 
             
                    </div>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton4" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupRiskDetail(event);"  Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                   </div>
        
               <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Detailed Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton5" ImageUrl="../Images/Category_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">                                                          
                            <p class="p1">Store a contract forever with keyword tagging for easy search and retrieval with role-based access </p> <br />                              
                            </div>
                          <div id="gridDetaild"></div> 
                     
                    </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton5" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupDetaildDetail(event);" Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                      </div>
            
                 <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="keychallenge">
                        <p class="title">Critical Risk Summary Report</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-setValue">
                                <asp:ImageButton ID="ImageButton6" ToolTip="Open Critical Risk Summary Report" data-toggle="tooltip" OnClientClick="OpenstatupCriticalWindow();return false;" ImageUrl="../Images/Risk_Report.PNG" runat="server" />
                            </div>
                        </div>
                         <p class="title">Description</p>
                         <div class="col-md-12 col-sm-12 col-xs-12 descriptionstatus">
                           <p class="p1">With this report view the status of your compliances segregated across locations, categories and users, along with details of ageing of overdue compliances by Critical risk</p><br />
                            </div>
                          <div id="gridCriticalRisk"></div> 
             
                    </div>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                    <asp:LinkButton ID="LinkButton6" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                        Text="Show More..." OnClientClick="OpenwindowpopupCriticalRiskDetail(event);"  Style="float: right;font-size: 12px; font-weight: 700;color:blue;"></asp:LinkButton>
                </div>
                   </div>
            
            
            </div>
                <div id="divInternal">
             
                </div>
            </div>
    </div>

       <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
    

     <%--  Window historical data--%>
        <div id="divAdvanceSearchModel">  
         <input id="Startdatepicker" placeholder="Select Week" CssClass="clsROWgrid" title="startdatepicker" style="width: 25%;display:none;"/>
             <div class="clearfix"></div>
            <div id="grid1"></div> 
        </div>



        <%--  Risk--%>
        <div id="windowriskpopup">         
        </div>
    <div id="windowInternalriskpopup">         
        </div>

        <%--Location--%>
        <div id="windowlocationpopup">
        </div>
     <div id="windowInternallocationpopup">
        </div>

      <%--User--%>
        <div id="windowuserpopup">
        </div>
      <div id="windowInternaluserpopup">
        </div>

     <%--Category--%>
        <div id="windowCategorypopup">
        </div>
     <div id="windowInternalCategorypopup">
        </div>
    
     <%--Summary--%>
        <div id="windowSummarypopup">
        </div>
     <div id="windowInternalSummarypopup">
        </div>

           <%-- Critical Risk--%>
        <div id="windowcriticalriskpopup">         
        </div>
    <div id="windowInternalcriticalriskpopup">         
        </div>

</asp:Content>
