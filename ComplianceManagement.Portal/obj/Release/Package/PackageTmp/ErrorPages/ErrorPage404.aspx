﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage404.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ErrorPages.ErrorPage404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Page Not Found</title>    
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="/NewCSS/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .login-form-head {
    /*max-width: 350px;*/
    min-height: 80px;
    background: #ffffff;
    text-align: center;
}
        .login-wrap {
    padding: 20px;
}

        .login-form {
    /*max-width: 350px;*/
    margin: 100px auto 0;
    background: #f9f9f9;
}
    </style>
</head>
<body>
    <div class="container">
    <form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">
        <asp:Panel ID="Panel2" runat="server">
            <div class="col-md-12 login-form-head">
                <p class="login-img">
                    <a href="https://www.avantis.co.in">
                        <img src="/Images/avantil-logo.png" /></a>
                </p>
            </div>

            <div class="login-wrap">
                <div id="divLogin" class="row" runat="server">
                    <h1 class="text-center">Oops!</h1>
                    <h2 class="text-center">Page Not Found</h2>
                    <h4 class="text-center">Sorry, an error has occured, Requested page not found!</h4>
                    <h5 class="text-center">Use your browsers Back button to navigate to the page you have prevously come from</h5>
                    
                    <div class="clearfix"></div>

                    <div class="text-center" style="margin-top: 15px;">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <a href="/ProductMapping/ProductMappingStructure.aspx" class="btn btn-primary btn-lg btn-block">
                                <span class="fa fa-home"></span>
                                Take Me Home </a>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>

            </div>
        </asp:Panel>
    </form>
        </div>
</body>
</html>
