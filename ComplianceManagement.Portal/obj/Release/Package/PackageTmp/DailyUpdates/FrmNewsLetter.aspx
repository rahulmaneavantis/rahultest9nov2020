﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="FrmNewsLetter.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.FrmNewsLetter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upEventList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--  <div style="margin-bottom: 4px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                    ValidationGroup="EventValidationGroup" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="EventValidationGroup" Display="None" />
            </div>--%>
            <table width="100%">
                <tr>
                    <td align="right">
                        <%-- <asp:DropDownList runat="server" ID="ddlFilterCategory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterCategory_SelectedIndexChanged">
                        </asp:DropDownList>--%>
                    </td>
                    <td align="right" style="width: 25%">
                        <%--Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />--%>
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddEvent" OnClick="btnAddEvent_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdEventList_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdEventList_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdEventList_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdEventList_RowCommand" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Title" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Title">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Description" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Date" ItemStyle-Height="25px" HeaderStyle-Height="20px" SortExpression="NewsDate">
                            <ItemTemplate>
                                <asp:Label ID="lblHeldOn" runat="server" Text='<%# Convert.ToDateTime(Eval("NewsDate")).ToString("dd-MMM-yyyy") %>' ToolTip='<%# Convert.ToDateTime(Eval("NewsDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_ACT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Event" title="Edit NewsLetter" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_ACT" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this NewsLetter?');"><img src="../Images/delete_icon.png" alt="Delete Event" title="Delete NewsLetter" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divEventDialog">
        <asp:UpdatePanel ID="upEvent" runat="server" UpdateMode="Conditional" OnLoad="upEvent_Load">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary"
                        ValidationGroup="EventValidationGroup" />
                     <asp:Label ID="Label1" runat="server"></asp:Label>
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Title</label>
                    <asp:TextBox runat="server" ID="txtTitle" Style="width: 390px;" MaxLength="500" ToolTip="Name" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty." ControlToValidate="txtTitle"
                        runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Upload Image</label>
                            <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                            <asp:FileUpload runat="server" ID="fuSampleFile" />

                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Image can not be empty." ControlToValidate="fuSampleFile"
                        runat="server" ValidationGroup="EventValidationGroup" Display="None" />

                         <%--    <asp:RegularExpressionValidator ID="RegularExpressionValidator7"
                     runat="server" ControlToValidate="fuSampleFile"
                     ErrorMessage="Only .jpeg, .gif, .png, .jpg image formats are allowed." ForeColor="Red"
                     ValidationExpression="/^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.JPG|.jpg)$/"
                     ValidationGroup="EventValidationGroup" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                        Date
                    </label>
                    <asp:TextBox runat="server" ID="tbxStartDate" CssClass="StartDate" Style="height: 16px; width: 390px;" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Date can not be empty." ControlToValidate="tbxStartDate"
                        runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                </div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 7px" id="Div1" runat="server">
                           <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Upload Document</label>
                            <asp:Label runat="server" ID="lblSampleDoc" CssClass="txtbox" />
                            <asp:FileUpload runat="server" ID="FileUpload1" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="File can not be empty." ControlToValidate="FileUpload1"
                        runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; margin-left: 210px" id="Div3" runat="server">
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
               <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button" OnClientClick="if (!ValidateFile()) return false;if (!ValidateImage()) return false;"
                        ValidationGroup="EventValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divEventDialog').dialog('close');"  />
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divEventDialog').dialog({
                height: 350,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "News Letter",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
          <%--$("#<%= ddlCompanyType.ClientID %>").combobox();--%>
        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function UncheckComplinceHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkCompliance']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkCompliance']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ComplianceSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function checkAllComplince(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkCompliance") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

         ////////////////////////pdf file validation/////////////////
        var validImageTypes = ["JPEG", "jpg", "PNG", "png", "BMP"];
        function ValidateImage() {
            
           
            var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            <%--var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;--%>
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                var a = validImageTypes.indexOf(fileExtension);
                if (validImageTypes.indexOf(fileExtension) == -1) {
                    isValidFile = false;
                    break;
                }
               
            }

            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file uploded. Upload JPEG,jpg,png,BMP file format";
            }
            return isValidFile;
        }
        ///////////////////////////////////////////For PDF////////////////////////

         var validFilesTypes = ["pdf"];
         function ValidateFile() {
             
           
            var label = document.getElementById("<%=Label1.ClientID%>");
            <%--var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;--%>
            var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;
            var isValidFile = true;
            for (var i = 0; i < FileUpload1.length; i++) {
                var fileExtension = FileUpload1[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != 0) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file uploded. Only PDF format supported.";
            }
            return isValidFile;
        }

///////////////////////////////////////////////////////////////////////////////////////
    </script>
</asp:Content>
