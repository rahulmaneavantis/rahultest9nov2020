﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IMPStatusAuditPerformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.IMPStatusAuditPerformer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script src="../../Newjs/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 14px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 14px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">

        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtIMPNewTimeLine.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                //numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
        function initializeConfirmDatePicker(date) {
            var startDate = new Date();
            $('#<%= tbxDateIMP.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: startDate,
                //numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });
        }
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });

        function ConfirmtestViewIMPPerformer(ink) {

            $('#DocumentPopUpIMPPer').modal();
            $('#docViewerAllIMPPer').attr('src', "../../docviewer.aspx?docurl=" + ink);
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#DocumentPopUpIMPPer').modal('hide');
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <table style="width: 100%; margin-top: 5px;" runat="server" visible="false">
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFilterLocation"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFinancialYear"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlSchedulingType"
                            CssClass="form-control m-bot15" />
                    </td>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlPeriod"
                            CssClass="form-control m-bot15" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true"
                            CssClass="form-control m-bot15">
                            <asp:ListItem Value="-1">Select Status</asp:ListItem>
                            <asp:ListItem Value="1">Open</asp:ListItem>
                            <asp:ListItem Value="2">Submited</asp:ListItem>
                            <asp:ListItem Value="3">Closed</asp:ListItem>
                            <asp:ListItem Value="4">Team Review</asp:ListItem>
                            <asp:ListItem Value="5">Final Review</asp:ListItem>
                            <asp:ListItem Value="6">Auditee Review</asp:ListItem>
                        </asp:DropDownList></td>
                    <td style="width: 20%;"></td>
                    <td style="width: 30%;"></td>
                </tr>
            </table>

            <table style="width: 100%; margin-top: 5px;">
                <tr>
                    <td colspan="4">
                        <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False" ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Visible="false"> </asp:Label>
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td style="position: absolute;" valign="top" runat="server" id="TDIMPTab">
                        <header class="panel-heading tab-bg-primary" style="background: none !important;">
                            <ul id="rblRole1" class="nav nav-tabs">
                                <li class="" id="liObservationHistory" runat="server" visible="false">
                                    <asp:LinkButton ID="btnObservationHistory" OnClick="btnObservationHistory_Click" runat="server">Observation History</asp:LinkButton>
                                </li>
                                <li class="" id="liActualTestingWorkDone" runat="server" visible="false">
                                    <asp:LinkButton ID="btnActualTestingWorkDone" OnClick="btnActualTestingWorkDone_Click" runat="server">Implementation Testing</asp:LinkButton>
                                </li>
                                <li class="active" id="liFinalStatus" runat="server">
                                    <asp:LinkButton ID="btnFinalStatus" OnClick="btnFinalStatus_Click" runat="server">Final Status</asp:LinkButton>
                                </li>
                                <li class="" id="liReviewHistory" runat="server">
                                    <asp:LinkButton ID="btnReviewHistory" OnClick="btnReviewHistory_Click" runat="server">Review History & Log</asp:LinkButton>
                                </li>
                            </ul>
                        </header>

                        <asp:MultiView ID="ImplementationView" runat="server">
                            
                            <asp:View ID="View3" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary5" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ThirdIMPValidationGroup" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label10" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator5" runat="server" EnableClientScript="False"
                                                ValidationGroup="ThirdIMPValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
                                        <fieldset style="margin-top: -20px; border: 1px solid #dddddd">
                                            <legend>Details Updated by Auditee </legend>
                                            <table>
                                                <tr runat="server" id="ManagementResponce">
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Management Response 
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtIMPNewManagementResponse" TextMode="MultiLine" Enabled="false" CssClass="form-control" Style="margin-bottom: 10px; width: 945px;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Status
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlIMPFinalStatus" class="form-control m-bot15" runat="server" Style="width: 254px;"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlIMPFinalStatus_SelectedIndexChanged" Enabled="false">
                                                            <asp:ListItem Value="-1">Select Status</asp:ListItem>
                                                            <asp:ListItem Value="2">Due & Partial Implemented</asp:ListItem>
                                                            <asp:ListItem Value="3">Due But Not Implemented</asp:ListItem>
                                                            <asp:ListItem Value="4">Not Feasible</asp:ListItem>
                                                            <asp:ListItem Value="5">Implemented</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="TimeLineTr">
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label runat="server" id="lblTimeline" style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Time Line
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtIMPNewTimeLine" Enabled="false" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                    </td>
                                                </tr>


                                                <tr runat="server" id="personresponsile" visible="false">
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Person Responsible
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlIMPNewPersonresponsible" class="form-control m-bot15" runat="server" Style="width: 254px;"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Select Person Responsible."
                                                            ControlToValidate="ddlIMPNewPersonresponsible" InitialValue="-1"
                                                            runat="server" ValidationGroup="ThirdIMPValidationGroup" Display="None" />
                                                    </td>
                                                </tr>

                                                <tr runat="server" visible="false">
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">*</label>
                                                        <label style="width: 150px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Remarks</label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtIMPFinalStatusRemark1" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; width: 688px;" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Remark can not be empty."
                                                            ControlToValidate="txtIMPFinalStatusRemark"
                                                            runat="server" ValidationGroup="ThirdIMPValidationGroup" Display="None" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:UpdatePanel runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="rptIMPComplianceDocumnets" AutoGenerateColumns="false" AllowSorting="true"
                                                                    AllowPaging="true" GridLines="None" CellPadding="4" Font-Size="12px" Width="100%"
                                                                    OnRowCommand="rptIMPComplianceDocumnets_RowCommand" OnRowDataBound="rptIMPComplianceDocumnets_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Document" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>' CommandName="DownloadIMP"
                                                                                            ID="btnComplianceDocumnetsIMP" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnComplianceDocumnetsIMP" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton
                                                                                    CommandArgument='<%# Eval("FileID")%>' CommandName="DeleteIMP"
                                                                                    OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                    ID="lbtLinkDocbuttonIMP" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle BackColor="#ECF0F1" /> 
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerTemplate>
                                                                        <table style="display: none">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Records Found.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
                                        <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                            <legend>Details to be Updated by Auditor</legend>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                        <label style="width: 150px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Auditor Remarks</label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIMPFinalStatusRemark" runat="server" TextMode="MultiLine" CssClass="form-control" Style="margin-bottom: 10px; width: 945px;"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">&nbsp;</label>
                                                        <label style="width: 165px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                            Working File Upload</label>
                                                    </td>
                                                    <td>
                                                        <div style="width: 100%;">
                                                            <div style="width: 50%; float: left;">
                                                                <asp:FileUpload ID="fuIMPWorkingUpload" runat="server" AllowMultiple="true" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </asp:Panel>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button Text="Next" runat="server" ID="btnIMPNext3" CssClass="btn btn-search" OnClick="btnIMPNext3_Click"
                                                    ValidationGroup="ThirdIMPValidationGroup" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary6" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label12" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator6" runat="server" EnableClientScript="False"
                                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:Panel ID="divIMPReviewHistory" runat="server" Style="width: 100%">
                                                    <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                                        <legend>Review History</legend>
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:GridView runat="server" ID="GrdIMPRemark" AutoGenerateColumns="false" AllowSorting="true"
                                                                        AllowPaging="true" GridLines="None" CellPadding="4" ForeColor="Black"
                                                                        PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                        Font-Size="12px" Width="100%"
                                                                        OnPageIndexChanging="GrdIMPRemark_OnPageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="ID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="riskID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("ImplementationInstance") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Scheduleon" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("ImplementationScheduleOnID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="400px">
                                                                                <ItemTemplate>
                                                                                      <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'  data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Remarks") %>'></asp:Label>
                                                                                        </div>                                                                                     
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                                                <ItemTemplate>
                                                                                    <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-Width="120px" HeaderText="Documents" ItemStyle-HorizontalAlign="Left">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton ID="lblDownLoadfileIMP" runat="server" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>' OnClick="DownLoadClickIMP"></asp:LinkButton>
                                                                                            <asp:LinkButton ID="lblViewFile" runat="server" Text='<%# ShowSampleDocumentNameView((string)Eval("Name")) %>' OnClick="lblViewFile_Click"></asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lblDownLoadfileIMP" />
                                                                                            <asp:PostBackTrigger ControlID="lblViewFile" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                         <HeaderStyle BackColor="#ECF0F1" />
                                                                       
                                                                        <EmptyDataTemplate>
                                                                            No Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label style="width: 170px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                        Review Remark</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtIMPReviewRemark" runat="server" TextMode="MultiLine" CssClass="form-control" Style="margin-top: 10px; width: 945px;"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ErrorMessage="Remark Required" ControlToValidate="txtIMPReviewRemark" ForeColor="Red"
                                                                        runat="server" ID="RequiredFieldValidator12" ValidationGroup="oplValidationGroup" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label style="width: 170px; display: block; float: left; font-size: 13px; color: #8e8e93;">
                                                                        Upload File</label>
                                                                </td>
                                                                <td>
                                                                    <asp:FileUpload ID="ReviewFileUpload" runat="server" AllowMultiple="true" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label style="width: 170px; float: left; font-size: 13px; color: #8e8e93;">
                                                                            Date</label>
                                                                </td>
                                                                <td>
                                                                        <asp:TextBox runat="server" ID="tbxDateIMP" CssClass="form-control" Style="width: 115px; text-align: center" />
                                                                        <asp:RequiredFieldValidator ErrorMessage="Please Select Date." ControlToValidate="tbxDateIMP"
                                                                            runat="server" ID="RequiredFieldValidator18" ValidationGroup="oplValidationGroupIMP" Display="None" /></td>
                                                            </tr>
                                                            <tr runat="server" id="StatusTR">
                                                                <td>
                                                                    <label style="width: 10px; display: block; float: left; font-size: 14px; color: red;">*</label>
                                                                    <label style="width: 160px; display: block; float: left; font-size: 14px; color: #8e8e93;">
                                                                        Status</label>
                                                                </td>
                                                                <td>
                                                                    <div style="margin-bottom: 7px; margin-top: 10px;" id="DivStatus" runat="server">
                                                                        <asp:RadioButtonList ID="rdbtnStatusNEW" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button Text="Submit" runat="server" ID="btnIMPSave" OnClick="btnIMPSave_Click" CssClass="btn btn-search"
                                                                        ValidationGroup="oplValidationGroupIMP" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                    
                                                    <ContentTemplate>
                                                        <div style="margin: 5px; width: 100%;">
                                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                                                    <legend>Audit Log</legend>
                                                                    <asp:GridView runat="server" ID="grdTransactionIMPHistory" AutoGenerateColumns="false" AllowSorting="true" PageSize="12" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                        AllowPaging="true" GridLines="None" OnPageIndexChanging="grdTransactionIMPHistory_OnPageIndexChanging"
                                                                        CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px"
                                                                        DataKeyNames="AuditTransactionID">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="130px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Step" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px">
                                                                                <ItemTemplate>
                                                                                     <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("Remarks") %>'></asp:Label>
                                                                                         </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px">
                                                                                <ItemTemplate>
                                                                                     <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                                    <asp:Label ID="lblAuditorRemarks" runat="server" Text='<%# Eval("AuditorRemark") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("AuditorRemark") %>'></asp:Label>
                                                                                             </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90px">
                                                                                <ItemTemplate>
                                                                                    <%# Eval("StatusChangedOn")!= null?((DateTime)Eval("StatusChangedOn")).ToString("dd-MMM-yyyy"):""%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                         <HeaderStyle BackColor="#ECF0F1" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                    <asp:Label ID="Label13" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                                                </fieldset>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdlSelectedDocumentIDIMP" />
                                                            <asp:Button ID="btnDownloadIMP" runat="server" Style="display: none" OnClick="btnDownloadIMP_Click" />
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnDownloadIMP" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                        <div style="margin: 5px; width: 100%;">
                                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                                                    <legend style="font-weight: bold">TimeLine History</legend>
                                                                    <asp:GridView runat="server" ID="grdTimelineHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                                        AllowPaging="true" PageSize="10" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right" GridLines="None"
                                                                        OnPageIndexChanging="grdTimelineHistory_PageIndexChanging"
                                                                        CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="TimeLine" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                <ItemTemplate>
                                                                                    <%# Eval("TimeLine")!= null?((DateTime)Eval("TimeLine")).ToString("dd-MMM-yyyy"):""%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ManagementResponse" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px">
                                                                                <ItemTemplate>
                                                                                     <div class="text_NlinesusingCSS" style="width: 250px;">
                                                                                    <asp:Label ID="lblmgmtResponse" runat="server" Text='<%# Eval("ManagementResponse") %>'  data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("ManagementResponse") %>'></asp:Label>
                                                                                         </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Auditee" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAuditee" runat="server" Text='<%# Eval("PersonResponsible") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Created Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                <ItemTemplate>
                                                                                    <%# Eval("CreatedOn")!= null?((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yyyy"):""%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Implemented Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblImpStatus" runat="server" Text='<%# Eval("ImplementationStatus") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                         <HeaderStyle BackColor="#ECF0F1" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </div>

        <div class="modal fade" id="DocumentPopUpIMPPer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="docViewerAllIMPPer" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
