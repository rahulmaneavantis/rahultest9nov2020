﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalAuditCheckList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AdditionalAuditCheckList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        label {
            font-weight: 400 !important;
        }

        legend {
            margin-bottom: 2px !important;
            font-size: 19px;
            color: #666 !important;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        /*.chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }*/
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div style="margin-bottom: 1px">
                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <div style="margin-bottom: 5px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Audit Step</label>
                        <asp:TextBox runat="server" ID="txtATBD" Width="350px" Height="100px" TextMode="MultiLine" CssClass="form-control" Style="margin-left: 12px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="please enter audit step" ControlToValidate="txtATBD"
                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                    </div>

                    <div style="margin-bottom: 5px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Audit Methodology</label>
                        <asp:TextBox runat="server" ID="txtAuditObjective" Width="350px" Height="100px" TextMode="MultiLine" CssClass="form-control" Style="margin-left: 12px;" />
                    </div>
                    <div style="margin-bottom:5px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer data Request Document</label>
                        <asp:TextBox runat="server" ID="txtPreRequisiteDocument" Width="350px" Height="100px" TextMode="MultiLine" CssClass="form-control" Style="margin-left: 12px;" />
                    </div>
                    <div style="margin-bottom: 5px; display:none">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Audit Step Rating</label>
                        <asp:DropDownList ID="ddlRatingItemTemplate" runat="server" class="form-control m-bot15" Style="width: 220px; margin-left: 127px;">
                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                            <asp:ListItem Value="1">High</asp:ListItem>
                            <asp:ListItem Value="2">Medium</asp:ListItem>
                            <asp:ListItem Value="3">Low</asp:ListItem>
                        </asp:DropDownList>
                        <%--<asp:CompareValidator ErrorMessage="select audit step rating." ControlToValidate="ddlRatingItemTemplate"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="PromotorValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div style="margin-bottom: 5px; margin-left: 42%; margin-top: 10px">
                        <asp:Button Text="Save" runat="server" ID="btnPopupSave" CssClass="btn btn-primary"
                            ValidationGroup="PromotorValidationGroup" OnClick="btnPopupSave_Click" />
                    </div>
                    <div style="margin-bottom: 1px; float: left; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
