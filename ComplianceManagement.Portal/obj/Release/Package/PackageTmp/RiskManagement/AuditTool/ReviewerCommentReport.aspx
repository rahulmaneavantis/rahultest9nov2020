﻿<%@ Page Title="Reviewer Comment Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ReviewerCommentReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.ReviewerCommentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
      
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('Reviewer Comment Report');
            fhead('My Report / Reviewer Comment Report ');
        });


    </script>

    <style type="text/css">
        .td1 {
            width: 5%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 5%;
        }

        .td4 {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">   
                            <header class="panel-heading tab-bg-primary ">
                                <ul id="rblRole1" class="nav nav-tabs">
                                    <%if (roles.Contains(3))%>
                                    <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer"  PostBackUrl="../../RiskManagement/AuditTool/TestingStatusReport.aspx"  runat="server">Testing Status</asp:LinkButton>                                           
                                        </li>
                                        <li class="" id="li1" runat="server">
                                            <asp:LinkButton ID="LinkButton1" PostBackUrl="../../RiskManagement/AuditTool/PendingTestingReport.aspx"    runat="server">Pending Testing</asp:LinkButton>                                           
                                        </li>
                                    <%}%>
                                    <%if (roles.Contains(4))%>
                                    <%{%>
                                        <li class="active"  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" PostBackUrl="../../RiskManagement/AuditTool/ReviewerCommentReport" runat="server">Reviewer Comments</asp:LinkButton>                                        
                                        </li>
                                        <%--<li class=""  id="li2" runat="server">
                                            <asp:LinkButton ID="LinkButton2" PostBackUrl="../../RiskManagement/AuditTool/FailedControlReportReviewer" runat="server">Failed Control</asp:LinkButton>                                        
                                        </li>--%>
                                    <%}%>
                                </ul>
                            </header>     
                            <div class="clearfix"></div>      
                            <div style="float:left;width:100%">
                                <div style="float:left;width:13%">                                   
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:150px">
                                        <div class="col-md-3 colpadding0">
                                            <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>           
                                </div>     
                                <div style="float:left;width:87%">       
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Entity" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                  
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount">               
                                <asp:Button  ID="lbtnExportExcel" runat="server" ValidationGroup="ComplianceInstanceValidationGroup" Text="Export to Excel" CssClass="btn btn-search" OnClick="lbtnExportExcel_Click" style="margin-top: 5px;  width: 126px;"/>                   
                                </div> 
                                <div class="clearfix"></div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Sub Entity" OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                  
                                </div>                   
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial"  AutoPostBack="true"  class="form-control m-bot15" Width="80%" Height="32px"
                                DataPlaceHolder="Financial Year" OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                                <asp:CompareValidator ErrorMessage="Select Financial Year" ControlToValidate="ddlFilterFinancial"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ForeColor="Red" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>                        
                                </div> 
                             
                                </div>
                            </div>

                            <div style="margin-bottom: 4px">
                            <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" 
                                 OnSorting="grdComplianceRoleMatrix_Sorting" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound" 
                                PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None" Width="100%" AllowSorting="true" 
                                DataKeyNames="RiskCreationId" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                    <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RiskCategoryCreationId" SortExpression="RiskCreationId" Visible="false">
                                        <ItemTemplate>                                            
                                                <asp:Label ID="lblRiskCategoryCreationId" runat="server" Text='<%# Eval("RiskCreationId")%>'></asp:Label>                                            
                                             <asp:Label ID="Label2" runat="server" Text='<%# Eval("Branch")%>'></asp:Label>                                        
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("FinancialYear")%>'></asp:Label>   
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="ControlNo">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 125px;">
                                            <asp:Label ID="lblControlNo" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlNo")%>' tooltip='<%# Eval("ControlNo")%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process" SortExpression="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# Eval("Process")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubProcess" SortExpression="SubProcess">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblSubProcess" runat="server" Text='<%# Eval("SubProcess")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ActivityDescription">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label runat="server" Text='<%# Eval("ActivityDescription")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ControlDescription">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblControlDescription" runat="server" Text='<%# Eval("ControlDescription")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Strategy">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblTestStrategy" runat="server" Text='<%# Eval("TestStrategy")%>' ToolTip='<%# Eval("TestStrategy")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents Examined">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblDocumentsExamined" runat="server" Text='<%# Eval("DocumentsExamined")%>' ToolTip='<%# Eval("DocumentsExamined")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                      
                                    <asp:TemplateField HeaderText="Performer" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("PerformerName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>                                    
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                </PagerTemplate>                                
                            </asp:GridView>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 750px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
