﻿<%@ Page Title="Failed Control Testing" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FailedControlTesting.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.FailedControlTesting" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
      <style type="text/css">
         
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
    <style type="text/css">
        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
     <script type="text/javascript">
         $('[data-toggle="tooltip"]').tooltip();   

         
    </script>
    <script type="text/javascript">
        function CloseWin3() {
            $('#divDetailsDialog').modal('hide');
            window.location.reload();
        };

        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }
        function setDate() {
            $(".StartDate").datepicker();
        }

        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Failed Control Testing');
        });

        function AddCustomerPopup() {
            $('#divDetailsDialog').modal('show');
            return true;
        }

        //This is used for Close Popup after save or update data.
        function CloseWin4() {
            $('#divDetailsDialog').modal('hide');
        };

        //This is used for Close Popup after button click.
        function caller2() {
            setInterval(CloseWin4, 3000);
        };

        function ShowDialog(ScheduledOnID, RiskCreationId, FinancialYear, CustomerBranchID, Period) {            
            $('#divDetailsDialog').modal('show');
            $('#showdetails').attr('width', '1050px');
            $('#showdetails').attr('height', '840px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/FailedAuditStatusTransaction.aspx?ScheduledOnID=" + ScheduledOnID + "&RiskCreationId=" + RiskCreationId + "&FinancialYear=" + FinancialYear + "&CustomerBranchID=" + CustomerBranchID + "&Period=" + Period);
        }
    </script>


    <style type="text/css">
        .td1 {
            width: 10%;
        }

        .td2 {
            width: 23%;
        }

        .td3 {
            width: 10%;
        }

        .td4 {
            width: 23%;
        }

        .td5 {
            width: 10%;
        }

        .td6 {
            width: 23%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
     <div style="margin-bottom: 4px">
               <asp:ValidationSummary runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label></div>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
           
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div style="float:left;width:100%">
                       <div style="float:left;width:13%">
                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;width:150px;">
                    <div class="col-md-2 colpadding0" style="width:40px">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5"  />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" Selected="True"/>
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>                    
                </div>    </div>     
                       <div style="float:left;width:87%">     
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">  
                                    <asp:DropDownListChosen ID="ddlLegalEntity" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Unit" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity1" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                     DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen ID="ddlSubEntity2" runat="server" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                      DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                       DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                 </div>              
                       
                    <div class="clearfix"></div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity4" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                        DataPlaceHolder="Sub Unit" OnSelectedIndexChanged="ddlSubEntity4_SelectedIndexChanged">
                                    </asp:DropDownListChosen>                                  
                                </div>
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                      
                            <asp:DropDownListChosen runat="server" ID="ddlFilterFinancial" AutoPostBack="true"
                                 class="form-control m-bot15" Width="80%" Height="32px" DataPlaceHolder="Financial Year"
                                OnSelectedIndexChanged="ddlFilterFinancial_SelectedIndexChanged">
                            </asp:DropDownListChosen>
                        </div>                          
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                           <asp:DropDownListChosen runat="server" ID="ddlQuarter" AutoPostBack="true" class="form-control m-bot15"
                               DataPlaceHolder="Period"
                                OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged" Width="80%" Height="32px">
                               <asp:ListItem Value="-1">Period</asp:ListItem>
                                <asp:ListItem Value="1">Apr - Jun</asp:ListItem>
                                <asp:ListItem Value="2">Jul - Sep</asp:ListItem>
                                <asp:ListItem Value="3">Oct - Dec</asp:ListItem>
                                <asp:ListItem Value="4">Jan - Mar</asp:ListItem>
                            </asp:DropDownListChosen>
                            <asp:CompareValidator ErrorMessage="Select Quarter" ControlToValidate="ddlQuarter"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                            </div>                                            
                             <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <asp:DropDownListChosen runat="server" ID="ddlKeyNonKey" AutoPostBack="true" 
                                class="form-control m-bot15" Width="80%" Height="32px" DataPlaceHolder="Key NonKey"
                                OnSelectedIndexChanged="ddlKeyNonKey_SelectedIndexChanged" >
                                 <asp:ListItem Value="-1">Key NonKey</asp:ListItem>
                                <asp:ListItem Value="1">Key</asp:ListItem>
                                <asp:ListItem Value="2">Non Key</asp:ListItem>
                            </asp:DropDownListChosen>
                            </div>         
                    <div class="clearfix"></div>                
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <asp:DropDownListChosen runat="server" ID="ddlStatus" AutoPostBack="true" class="form-control m-bot15" Width="80%" Height="32px"
                                OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" DataPlaceHolder="Status" >
                                <asp:ListItem Value="-1">Status</asp:ListItem>
                                <asp:ListItem Value="1">Open</asp:ListItem>
                                <asp:ListItem Value="2">Submited</asp:ListItem>
                                <asp:ListItem Value="4">Review Comment</asp:ListItem>
                                <asp:ListItem Value="3">Closed</asp:ListItem>                                
                            </asp:DropDownListChosen>                                                    
                             
                                </div>    
                       </div>
                            </div>
                        
                            <div style="margin-bottom: 4px;">
                                <div class="panel"> 
                            <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" 
                                PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="None"  AllowSorting="true"
                                 DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                                OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                    <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task ID" ItemStyle-Width="3%" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("RiskCreationId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Branch">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="Label21" runat="server" Text='<%# Eval("branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Risk Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Control Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblControlDescription" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ControlDescription")%>' ToolTip='<%# Eval("ControlDescription")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Test Strategy">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;" >
                                                <asp:Label ID="lblTestStrategy" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TestStrategy")%>' 
                                                    ToolTip='<%# Eval("TestStrategy")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%#GetQuarterName((int)Eval("QuaterCount"))%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Frequency" Visible="false">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FrequencyName")%>' ToolTip='<%# Eval("FrequencyName")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewer((int)Eval("AuditStatusID") ,(long?)Eval("ScheduledOnID"), (long)Eval("RiskCreationId")) %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>                                 
                                    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>                                            
                                            <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("AuditStatusID")) %>'
                                                CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("RiskCreationId") + "," + Eval("FinancialYear") +"," + Eval("CustomerBranchID") +"," + Eval("QuaterCount") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                                        </ItemTemplate>                                        
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                 <PagerSettings Visible="false" />  
                                <PagerTemplate>
                                       <%-- <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                                </PagerTemplate> 
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>                            
                            </asp:GridView>
                                      <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                           </div></div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float:right;">
                                        <p>Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<vit:FailedAuditStatusTransaction runat="server" ID="udcStatusTranscatopn" />--%>

    <div class="modal fade" id="divDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 950px;">
            <div class="modal-content" style="width: 100%;"  style="background-color:#eee">
                <div class="modal-header" style="background-color:#eee">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  
                        style="margin-top: -6px;" onclick="javascript:window.location.reload()">&times;</button>
                </div>
                <div class="modal-body" style="background-color:#eee">
                    <iframe id="showdetails" src="about:blank" width="950px" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
