﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.WebForm1" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:DropDownCheckBoxes ID="ddlQuarter" runat="server"  Visible="true" 
        CssClass="form-control m-bot15"
        AddJQueryReference="false" AutoPostBack="false" UseButtons="false" UseSelectAllNode="True"
        Style="padding: 0px; margin: 0px; width: 90%; height: 50px;"
         OnSelectedIndexChanged="ddlQuarter_SelectedIndexChanged">
        <Style SelectBoxWidth="225" DropDownBoxBoxWidth="200" DropDownBoxBoxHeight="130" />
        <Texts SelectBoxCaption="Select Quarter" />
    </asp:DropDownCheckBoxes>     
</asp:Content>
