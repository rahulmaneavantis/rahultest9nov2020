﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditCustomer_FileUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Customers.AuditCustomer_FileUpload" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
     
     <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
        .EDIT_CUSTOMER_BRANCH a:hover{
            background-color:blue;
        }
         .table {
             margin-bottom: 0px;
         }
    </style> 

     <script type="text/javascript">
         function OpenDocviewer(file) {
             $('#docviewerCustomer').modal('show');
             $('#ContentPlaceHolder1_DocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
         }
         function openAddModalControl() {
             $('#divCustomersDialog').modal('show');
             return true;
         }
         function closeWin() {
             $('#divCustomersDialog').hide();
             location.reload();
         }
     </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-12 plr0">
                                    <asp:ValidationSummary ID="vsDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="DocumentPopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="DocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                  <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="DocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                       
        </div>
  
    <asp:UpdatePanel ID="upCustomerDocuments" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                              <div class="col-md-12 colpadding0" style="margin-bottom: 5px;">
                                  <div style="text-align:left;" >
                                    <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                        RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                                runat="server" Style="text-decoration: none; color: #666;" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Size="12" ForeColor="#666" Font-Underline="true" />
                                        <SeparatorStyle Font-Size="12" />
                                        <SeparatorTemplate>
                                            <span style="color: #666;">&gt;</span>
                                        </SeparatorTemplate>
                                    </asp:DataList>
                                  </div>
                                </div>

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-6 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 68px;float: left;margin-left: -35px;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" Selected="True" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div> 
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlCustomerFilter" runat="server" DataPlaceHolder="Select Customer"
                                             class="form-control m-bot15" Width="90%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                               AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerFilter_SelectedIndexChanged"></asp:DropDownListChosen>
                            </div>                          
                            <div class="col-md-3 colpadding0 entrycount" id="divFilter" runat="server" style="margin-top: 5px;">
                                
                                <asp:TextBox runat="server" ID="tbxFilter" class="form-control" Style="Width:278px" MaxLength="50" placeholder="Type to search" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                             <div style="text-align: right;">
                                <asp:Button Text="Add New" runat="server" CssClass="btn btn-primary" onclick="btnCustomerDocuments_Click" Style="margin-top: 5px;" OnClientClick="openAddModalControl();" ID="btnCustomerDocuments"/>
                            </div>
                            <div style="margin-bottom: 4px;"> 
                               <asp:GridView runat="server" ID="grdCustomerFile" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                                OnRowDataBound="grdCustomerFile_RowDataBound" DataKeyNames="ID" OnRowCommand="grdCustomerFile_RowCommand" OnPageIndexChanging="grdCustomerFile_OnPageIndexChanging" >
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" />
                                    <asp:TemplateField HeaderText="Created On">
                                        <ItemTemplate>
                                           <%# ((DateTime)Eval("CreatedDate")).ToString("dd-MMM-yy HH:mm")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%">
                                  <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:LinkButton data-toggle="tooltip" data-placement="bottom" title="Download Document" AutoPostBack="true" 
                                            CommandArgument='<%# Eval("Id")%>' CommandName="DownloadDoc" ID="lnkBtnDownLoadDoc" runat="server">
                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> 
                                        </asp:LinkButton>
                                        
                                        <asp:LinkButton CommandArgument='<%# Eval("Id")%>' AutoPostBack="true" CommandName="ViewDocView"
                                            ID="lblDocView" runat="server" data-toggle="tooltip" data-placement="bottom" title="View Document">
                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                        </asp:LinkButton>

                                        <asp:LinkButton CommandArgument='<%# Eval("Id")%>' AutoPostBack="true" CommandName="DeleteDoc"
                                            OnClientClick="return confirm('Are you certain you want to delete this document?');"
                                            ID="lnkBtnDeleteDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Delete Document">
                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete"  /> 
                                        </asp:LinkButton>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lnkBtnDownLoadDoc" />
                                        <asp:PostBackTrigger ControlID="lnkBtnDeleteDoc" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                                </ItemTemplate>
                                     </asp:TemplateField>
                                 
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" /> 
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />             
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                </asp:GridView>
                                  <div style="float: right;">
                                 <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                                      </div>
                            </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                        </p>
                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

      <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog p0" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Documentmodel">
                                Add Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;height:230px;border:0;" runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin-bottom: 7px" id="div1" runat="server">
                                                <asp:ValidationSummary ID="ValidationSummary1" Display="None" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="CustomerValidationGroup5" />
                                                <asp:CustomValidator ID="cvAddcustomer" runat="server" EnableClientScript="False"
                                                    ValidationGroup="CustomerValidationGroup5" Display="None" class="alert alert-block alert-danger fade in" />
                                            </div>

                                            <div style="margin-bottom: 7px">
                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                    Customer</label>
                                               <asp:DropDownListChosen ID="drpdownCustomer" runat="server" DataPlaceHolder="Select Customer"
                                             class="form-control m-bot15" Width="59%" Height="32px" AllowSingleDeselect="false" DisableSearchThreshold="3"></asp:DropDownListChosen>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please select Customer."
                                                    ControlToValidate="drpdownCustomer" runat="server" ValidationGroup="CustomerValidationGroup5"
                                                    Display="None" />
                                                </div>
                                          
                                            <div style="margin-bottom: 7px">
                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="width: 26.5%;display: block;float: left;font-size: 13px;color: #333;">Upload Document</label>
                                                <div style="width: 100%;">
                                                    <div style="margin-left: -13px;float: left;height: 43px;">
                                                        <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" ForeColor="#666"/>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select file for upload."
                                                    ControlToValidate="fuTaskDocUpload" runat="server" ValidationGroup="CustomerValidationGroup5"
                                                    Display="None" />--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <asp:Button class="btn btn-primary" Style="margin-left: 250px" Text="Save" ID="btnSave" OnClick="btnSave_Click" runat="server" ValidationGroup="CustomerValidationGroup5"/>
                                            <asp:Button class="btn btn-primary" Text="Close" ID="btnCancel" runat="server" OnClick="btnCancel_Click" OnClientClick="closeWin();"/>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSave" />
                                              <asp:PostBackTrigger ControlID="btnCancel" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                    </div>
                </div>
            </div>

    <%--Document Viewer--%>
            <div>
                <div class="modal fade" id="docviewerCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="DocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <script type="text/javascript">
    
        $(document).ready(function () {
            setactivemenu('Permanent Customer Files');
            fhead('Permanent Customer Files');
        });
    </script>
</asp:Content>
