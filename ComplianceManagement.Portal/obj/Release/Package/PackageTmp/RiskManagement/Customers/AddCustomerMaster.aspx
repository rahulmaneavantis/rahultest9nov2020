﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCustomerMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.AddCustomerMaster" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

    <script type="text/javascript">
        function closeWin() {
            window.parent.closeWin();
        }
    </script>
    <style type="text/css">
        div#ddlProductType_sl {
            height: 35px !important;
        }

        div#ddlProductType_dv {
            margin-top: 15px;
        }

        .DropDownListChosenMargin{
          margin-left: -11px !important;
        }
        .chosen-container-single.chosen-single{
            margin-left: -11px !important;
        }
    </style>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-bottom: 7px" id="divMessage" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary15" Display="None" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="CustomerValidateGroup" />
                        <asp:CustomValidator ID="cvAddcustomer" runat="server" EnableClientScript="False"
                            ValidationGroup="CustomerValidateGroup" Display="None" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer Name</label>
                        <asp:TextBox runat="server" ID="tbxCustomerName" AutoComplete="off" Style="width: 390px;" CssClass="form-control" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="rfv" ErrorMessage="Customer Name can not be empty." ControlToValidate="tbxCustomerName"
                            runat="server" ValidationGroup="CustomerValidateGroup"
                            Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Address</label>
                        <asp:TextBox runat="server" ID="tbxAddress" AutoComplete="off" Style="width: 390px;" CssClass="form-control"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Address can not be empty."
                            ControlToValidate="tbxAddress" runat="server" ValidationGroup="CustomerValidateGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Contact Person</label>
                        <asp:TextBox runat="server" ID="tbxContactPerson" AutoComplete="off" Style="width: 390px;" CssClass="form-control"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Contact person name can not be empty."
                            ControlToValidate="tbxContactPerson" runat="server" ValidationGroup="CustomerValidateGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Contact Number</label>
                        <asp:TextBox runat="server" ID="tbxcontactNumber" AutoComplete="off" Style="width: 390px;" CssClass="form-control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Please select Contact Number."
                            ControlToValidate="tbxcontactNumber" runat="server" ValidationGroup="CustomerValidateGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                            ValidationGroup="CustomerValidateGroup" ErrorMessage="Please enter a valid contact number."
                            ControlToValidate="tbxcontactNumber" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxcontactNumber" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                            ValidationGroup="CustomerValidateGroup" ErrorMessage="Please enter only 10 digit."
                            ControlToValidate="tbxcontactNumber" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Email</label>
                        <asp:TextBox runat="server" ID="tbxEmail" AutoComplete="off" Style="width: 390px;" CssClass="form-control"
                            MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please enter Email."
                            ControlToValidate="tbxEmail" runat="server" ValidationGroup="CustomerValidateGroup"
                            Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                            ValidationGroup="CustomerValidateGroup" ErrorMessage="Please enter a valid email."
                            ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                    </div>
                    <div runat="server" style="margin-bottom: 7px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Product Type</label>
                        <asp:DropDownCheckBoxes ID="ddlProductType" runat="server"
                            CssClass="form-control m-bot15"
                            AddJQueryReference="false" UseButtons="True" UseSelectAllNode="True"
                            Style="padding: 0px; margin: 0px; width: 90%; height: 200px;">
                            <Style SelectBoxWidth="390px" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="180" />
                            <Texts SelectBoxCaption="Select Product" />
                        </asp:DropDownCheckBoxes>
                    </div>
                    <div style="margin-bottom: 7px;">
                       <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Status</label>
                            <asp:DropDownListChosen runat="server" ID="ddlCustomerStatus1" CssClass="form-control" Width="61%" AllowSingleDeselect="false"  />
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Select Status."
                            ControlToValidate="ddlCustomerStatus1" ValidationGroup="CustomerValidateGroup"
                            Display="None"></asp:RequiredFieldValidator>
                    </div>
                    <div style="margin-bottom: 19px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">Permanent Customer Files</label>
                        <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" ForeColor="#666" />
                    </div>
                    <div style="margin-bottom: 15px">
                        <asp:Button class="btn btn-primary" Style="margin-left: 70px" Text="Save" ID="btnSave" runat="server" OnClick="btnSave_Click"
                            ValidationGroup="CustomerValidateGroup" />
                        <%--<button type="button" id="btnclose" class="btn btn-primary" data-dismiss="modal" OnClientClick="closeWin()">Close</button>--%>
                        <asp:Button class="btn btn-primary" Text="Close" ID="btnCancel" runat="server" OnClientClick="closeWin()" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <%--<asp:PostBackTrigger ControlID="btnCancel" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
