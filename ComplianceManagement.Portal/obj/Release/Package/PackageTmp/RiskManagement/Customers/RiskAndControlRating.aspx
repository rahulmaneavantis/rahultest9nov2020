﻿<%@ Page Title="Risk And Control Rating" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="RiskAndControlRating.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.RiskAndControlRating" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 


    <script type="text/javascript">
        function fopenpopup() {
            $('#divRiskCategoryDialog').modal('show');
        }
    </script>

    <script type="text/javascript">
        function initializeCombobox() {

        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        $(document).ready(function () {
            //setactivemenu('leftremindersmenu');
            fhead('Risk And Control Rating');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upProcessList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" Selected="True" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div  class="col-md-4 colpadding0 entrycount" style="color: #999; padding-top: 5px;">                   
                    <div class="col-md-3 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Rating :</p> 
                         </div>                  
                        <asp:DropDownList runat="server" ID="ddlFilterCategory" class="form-control m-bot15" Style="width: 250px;"
                             AutoPostBack="true" OnSelectedIndexChanged="ddlFilterCategory_SelectedIndexChanged">   
                             <asp:ListItem Value="A"> ALL </asp:ListItem>
                            <asp:ListItem Value="C">Control Rating</asp:ListItem>
                            <asp:ListItem Value="R">Risk Rating</asp:ListItem>                            
                        </asp:DropDownList>                     
                    </div> 
                     <div style="text-align:right;padding-top: 5px;">
                    <asp:LinkButton Text="Add New" runat="server" OnClientClick="fopenpopup()" ID="btnAddProcess" CssClass="btn btn-primary" OnClick="btnAddProcess_Click" />
                </div>
                 <div style="margin-bottom: 4px">
            <%--<asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdRiskcategoryList" AutoGenerateColumns="false"  OnRowDataBound="grdRiskcategoryList_RowDataBound"
                    OnSorting="grdRiskcategoryList_Sorting" AllowSorting="true"
                    PageSize="20" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                    DataKeyNames="ID" OnRowCommand="grdRiskcategoryList_RowCommand" OnPageIndexChanging="grdRiskcategoryList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr"  ItemStyle-Width="10%">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name"  ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Value" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Value") %>' ToolTip='<%# Eval("Value") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">                                   
                                    <asp:Label ID="lblApprover" runat="server" Text='<%# ShowProcessNonProcess((string)Eval("IsRiskControl")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Category" OnClientClick="fopenpopup()" CommandArgument='<%# Eval("ID") %>'><img src="../../Images/edit_icon_new.png" alt="Edit Risk Control Rating" title="Edit Risk Control Rating" /></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Category" CommandArgument='<%# Eval("ID") %>' Visible="false"
                                    OnClientClick="return confirm('Are you certain you want to delete this Risk Control Rating?');"><img src="../../Images/delete_icon_new.png" alt="Delete Risk Control Rating" title="Delete Risk Control Rating" /></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                   <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />     
                       <PagerSettings Visible="false" />            
                    <PagerTemplate>
                                      <%--  <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                      </PagerTemplate>
                </asp:GridView>
                           <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                     </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 750px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                           <%-- <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <%-- </asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="divRiskCategoryDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 450px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="dvAuditorDialog">
                        <asp:UpdatePanel ID="upProcess" runat="server" UpdateMode="Conditional" OnLoad="upProcess_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <asp:RadioButtonList runat="server" ID="rdRiskProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 110px"
                                            RepeatLayout="Flow" ForeColor="Black" OnSelectedIndexChanged="rdRiskProcess_SelectedIndexChanged">
                                            <asp:ListItem Text="Risk Rating" Value="Risk" Selected="True" />
                                            <asp:ListItem Text="Control Rating" Value="Control" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" Style="width: 250px;" MaxLength="100" ToolTip="Name" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                            Rating</label>
                                        <asp:TextBox runat="server" ID="tbxrating" CssClass="form-control" Style="width: 250px;" MaxLength="100" ToolTip="Name" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Rating can not be empty." ControlToValidate="tbxrating"
                                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                        <asp:CompareValidator ID="cv" runat="server" ControlToValidate="tbxrating" Type="Integer" Operator="DataTypeCheck"
                                            ErrorMessage="Shares must be NUMERIC! or Do not use comma seperated values" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 160px; margin-top: 25px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
