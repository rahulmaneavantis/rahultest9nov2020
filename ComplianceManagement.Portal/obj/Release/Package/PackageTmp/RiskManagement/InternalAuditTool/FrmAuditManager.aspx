﻿<%@ Page Title="Audit Manager" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="FrmAuditManager.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.FrmAuditManager" %>

<%@ Register Src="~/RiskManagement/Controls/InternalAuditComtrolAuditManager.ascx" TagPrefix="vit" TagName="InternalAuditComtrolAuditManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Manager/Final Review');
        });

        function ShowDialog(ATBDid, BID, FinYear, ForMonth, SID, PID) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '1000px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditControlAuditManager.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ATBDID=" + ATBDid + "&SID=" + SID + "&PID=" + PID);
        };

        function ShowIMPDialog(Resultid, BID, FinYear, ForMonth) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '1000px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#showdetails').attr('src', "../AuditTool/IMPStatusAuditManager.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ResultID=" + Resultid);
        };

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" > <%--OnLoad="upComplianceDetails_Load"--%>
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                            <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">                                           
                                        <li class="active" id="liProcess" runat="server">
                                            <asp:LinkButton ID="lnkProcess" OnClick="ShowProcessGrid" runat="server">Process</asp:LinkButton>                                           
                                        </li>
                                          
                                        <li class=""  id="liImplementation" runat="server">
                                            <asp:LinkButton ID="lnkImplementation" OnClick="ShowImplementationGrid"  runat="server">Implementation</asp:LinkButton>                                        
                                        </li>                                        
                                    </ul>
                                </header> 

                            <div class="clearfix"></div> 
                             <div class="panel-body">

                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="AuditValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="AuditValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <div class="col-md-2 colpadding0">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                         <asp:DropDownList runat="server" ID="ddlLegalEntity" PlaceHolder="Select Entity"  class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownList runat="server" ID="ddlSubEntity1" PlaceHolder="Select Sub Entity" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownList>      
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                      <asp:DropDownList runat="server" ID="ddlSubEntity2" PlaceHolder="Select Sub Entity" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownList>                                     
                                    </div>
                                 </div> 

                                  <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                        <asp:DropDownList runat="server" ID="ddlSubEntity3" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                                                          
                                        <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                         class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownList>              
                                     </div>

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                                        <asp:DropDownList ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                        OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"> 
                                        </asp:DropDownList>                                                           
                                    </div>

                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                          <asp:DropDownList runat="server" ID="ddlFilterStatus" AutoPostBack="true" class="form-control m-bot15 select_location"
                                                Style="float: left; width:90%;" OnSelectedIndexChanged="ddlFilterStatus_SelectedIndexChanged">                                                                        
                                                <asp:ListItem Value="5" Selected="True">Final Review</asp:ListItem>
                                                <asp:ListItem Value="3">Closed</asp:ListItem>
                                            </asp:DropDownList>
                                       </div>                                    
                                      
                                </div>
                                                              
                                 <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">
                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                            <asp:DropDownList runat="server" ID="ddlSchedulingType" class="form-control m-bot15 select_location" Visible="false"                           
                                                OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" AutoPostBack="true" Style="float: left; width:90%;" />
                                         </div>

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                         <asp:DropDownList runat="server" ID="ddlPeriod"  Style="float: left; width:90%;" Visible="false"
                                              CssClass="form-control m-bot15 select_location" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" />
                                          </div>                                      
                                     </div>

                                 <div class="clearfix"></div>

                                  <div id="ProcessGrid" runat="server">
                                      
                                      <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" AllowSorting="true" CssClass="table" GridLines="None"
                                        AllowPaging="true" PageSize="20" Width="100%"
                                           DataKeyNames="ATBDID"> <%--OnPageIndexChanging="grdSummaryDetailsAuditCoverage_PageIndexChanging"--%>
                                        <Columns>
                                             <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Financial Year">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblForPeriod" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="ProcessName" HeaderText="Process" />
                                            <asp:BoundField DataField="SubProcessName" HeaderText="Sub Process" />

                                            <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="btnChangeStatus" runat="server"
                                                      CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ATBDID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID")+ "," + Eval("ProcessId") %>'>                                                         
                                                         <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>  <%-- --%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                       <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />                    
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate>                                         
                                    </asp:GridView>

                                  </div>

                                <div id="ImplementationGrid" runat="server">
                                     <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverageIMP" AutoGenerateColumns="false" AllowSorting="true" CssClass="table" GridLines="None"
                                        AllowPaging="true" PageSize="20" Width="100%"
                                           DataKeyNames="ResultID"> <%--OnPageIndexChanging="grdSummaryDetailsAuditCoverage_PageIndexChanging"--%>
                                        <Columns>
                                             <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>   

                                            <asp:TemplateField HeaderText="Financial Year">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFinancialYear" runat="server" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblForPeriod" runat="server" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                           
                                            <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="btnChangeStatus" runat="server"
                                                      CommandName="CHANGE_STATUS" OnClick="btnChangeStatusIMP_Click" CommandArgument='<%# Eval("ResultID") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear")+ "," + Eval("ForMonth") + "," + Eval("AuditStatusID") %>'>                                                         
                                                         <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>  <%-- --%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                       <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />                    
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate>                                         
                                    </asp:GridView>

                                     </div>

                                 <div class="clearfix"></div>

                                 <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>   

                                 </div>

                            </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="about:blank" width="1100px" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>

                </div>
            </div>
        </div>
    </div>

    <%--<vit:InternalAuditComtrolAuditManager runat="server" ID="udcReviewStatusTranscatopn" />--%>
</asp:Content>
