﻿<%@ Page Title="Audit Scheduling Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="AuditSchedulingReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.AuditSchedulingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('Audit Scheduling Report');
        });
    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                            
                        <div class="col-md-6 colpadding0 entrycount" style="margin-top: 7px;"> 
                          <div class="col-md-2 colpadding0 entrycount">                   
                        <p style="color: #999; margin-top: 5px;">Select Location :</p>
                              </div>
                        <asp:DropDownList runat="server" ID="ddlFilterLocation" class="form-control m-bot15" Style="width: 250px; float: left"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" />                                        
                         </div>                    
                    <div style="float:right;">
                     <%--<asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddAuditAssignment" OnClick="btnAddAuditAssignment_Click" /> --%>
                        <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-search" style="margin-top: 5px;  width: 126px;" OnClick="lbtnExportExcel_Click" runat="server"/>                    
                     <%--<asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click">
                            <img src="../../Images/excel.png" alt="Export to Excel"  title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>--%>   
                    </div>


                           <%-- <table width="100%">
                <tr>
                    <td style="width: 10%">Select Location</td>
                    <td style="width: 35%">
                        <asp:DropDownList runat="server" ID="ddlFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 10%"></td>
                    <td style="width: 35%">
                        
                    </td>
                </tr>
            </table>--%>
            <%-- <asp:Panel runat="server" ID="Panel1" ScrollBars="Auto" Height="362px">--%>
            <div style="margin-bottom: 4px">
            <asp:GridView ID="grdTestQ" runat="server" AutoGenerateColumns="true" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="20" Width="100%">
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
             
            </asp:GridView>
            <%--   </asp:Panel>--%>
                    </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
