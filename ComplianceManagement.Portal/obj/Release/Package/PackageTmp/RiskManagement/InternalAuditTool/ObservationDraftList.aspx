﻿<%@ Page Title="Draft Observation Report" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ObservationDraftList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.InternalAuditTool.ObservationDraftList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        //function Check_Click(objRef) {
        //    var GridView = objRef.parentNode.parentNode.parentNode;
        //    var inputList = GridView.getElementsByTagName("input");
        //    var count = 0;
        //    for (var i = 0; i < inputList.length; i++) {
        //        var row = inputList[i].parentNode.parentNode;
        //        if (inputList[i].type == "checkbox") {
        //            if (objRef.checked) {
        //                inputList[i].checked = true;
        //            }
        //            else {
        //                inputList[i].checked = false;
        //            }
        //        }
        //    }
        //}
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Draft Observation Report');
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            var now = new Date();
            var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var diffDays = now.getDate() - firstDayPrevMonth.getDate();
            if (diffDays > 15) {
                var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 1);
            }

            $(function () {
                var startDate = new Date();
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    minDate: startDate,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    minDate: firstDayPrevMonth,
                });
            });
        }

        function ShowDialog(ATBDid, BID, FinYear, ForMonth, SID, PID, VerticalID, AuditID, CustomerId) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '98%');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            $('#showdetails').attr('src', "../AuditTool/InternalAuditControlAuditManager_ObservationList.aspx?FinYear=" + FinYear + "&ForMonth=" + ForMonth + "&BID=" + BID + "&ATBDID=" + ATBDid + "&SID=" + SID + "&PID=" + PID + "&VID=" + VerticalID + "&AuditID=" + AuditID + "&Type=AuditClosed" + "&CustomerId=" + CustomerId);
        };

        function getConfirmation() {
            var retVal = confirm(" Alert: Are you sure you want to delete this Observation?");
            if (retVal == true) {
                return true;
            } else {
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                                    <div class="clearfix"></div>    
                                     <header class="panel-heading tab-bg-primary" style="display:none;">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                               <%if (roles.Contains(3))%>
                                               <%{%>
                                            <li class="active" id="liPerformer" runat="server">
                                                <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" CausesValidation="false" runat="server">Performer</asp:LinkButton>                                           
                                            </li>
                                               <%}%>
                                                <%if (roles.Contains(4))%>
                                               <%{%>
                                            <li class=""  id="liReviewer" runat="server">
                                                <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" CausesValidation="false" runat="server">Reviewer</asp:LinkButton>                                        
                                            </li>
                                              <%}%>      
                                              <%if (roles.Contains(5))%>
                                               <%{%>
                                            <li class=""  id="liReviewer2" runat="server">
                                                <asp:LinkButton ID="lnkReviewer2" OnClick="ShowReviewer2" CausesValidation="false" runat="server">Reviewer2</asp:LinkButton>                                        
                                            </li>
                                              <%}%>                         
                                        </ul>
                                    </header> 
                                     <div class="clearfix"></div> 
                                     <div class="panel-body">
                                            <div class="col-md-12 colpadding0">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                                ValidationGroup="ComplianceInstanceValidationGroup" />
                                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                            </div>

                                            <div class="col-md-12 colpadding0">
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                                    <div class="col-md-2 colpadding0" style="width: 20%;">
                                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                                    </div>
                                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                                    <asp:ListItem Text="5" />
                                                    <asp:ListItem Text="10" />
                                                    <asp:ListItem Text="20"  />
                                                     <asp:ListItem Text="50"  Selected="True"/>
                                                    </asp:DropDownList>
                                                </div> 
                                                
                                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; ">                     
                                                    <asp:DropDownListChosen ID="ddlProcess" runat="server" AutoPostBack="true"  Width="100%"
                                                        class="form-control m-bot15 select_location" AllowSingleDeselect="false" DisableSearchThreshold="3" 
                                                        OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged">
                                                    </asp:DropDownListChosen>                   
                                                </div>
                                                  <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; padding-left: 10px;">
                                                     <%-- --%>
                                                       <asp:DropDownListChosen ID="ddlStatus" runat="server" AutoPostBack="true"  Width="100%"
                                                        class="form-control m-bot15 select_location" AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" >
                                                           <asp:ListItem Value="-1">All</asp:ListItem>
                                                           <asp:ListItem Value="2">Submited</asp:ListItem>
                                                            <asp:ListItem Value="4">Team Review</asp:ListItem>
                                                            <asp:ListItem Value="6">Auditee Review</asp:ListItem>
                                                            <asp:ListItem Value="5">Final Review</asp:ListItem>
                                                      </asp:DropDownListChosen> 

                                                      </div>
                                                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; padding-left: 55px;">
                                                <asp:LinkButton runat="server" ID="lblExport" Text="Export To Excel" CssClass="btn btn-primary"  OnClick="lblExport_Click"/>    
                                                <asp:LinkButton runat="server" ID="btnBack" Text="Back" CssClass="btn btn-primary"  OnClick="btnBack_Click"/>
                                            </div> 
                                            </div>                                              
                                     </div>
                                        
                                    <div class="clearfix"></div>                                                                 
                                    <div style="margin-bottom: 4px; margin-top:10px;">                                      
                                    <asp:GridView runat="server" ID="grdObservationList" AutoGenerateColumns="false" 
                                    OnRowDataBound="grdObservationList_RowDataBound" PageSize="50" 
                                    AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdObservationList_PageIndexChanging" ShowFooter="true"
                                    OnRowCommand="grdObservationList_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server"/> <%--onclick = "Check_Click(this)"--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>    
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                                <asp:Label ID="lblProcessId" Visible="false" Width="160px" runat="server" Text='<%# Eval("ProcessId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Audit Steps">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="lblAuditSteps" Width="160px" runat="server" Text='<%# Eval("AuditSteps") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("AuditSteps") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                        <asp:TemplateField HeaderText="ATBDID" Visible="false">
                                        <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblATBDId" runat="server" Text='<%# Eval("ATBDId") %>'></asp:Label>
                                        </div>
                                        </ItemTemplate>
                                        </asp:TemplateField>  
                                        <asp:TemplateField HeaderText="Observation">
                                        <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 190px;">
                                        <asp:TextBox ID="tbxObservation" runat="server" class="form-control" Text='<%# Eval("Observation") %>' data-toggle="tooltip" data-placement="top" Width="190px" ToolTip='<%# Eval("Observation") %>'></asp:TextBox>
                                        </div>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Management Response">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 190px;">
                                                <asp:TextBox ID="tbxManagementResponse" runat="server" class="form-control" Text='<%# Eval("ManagementResponse") %>' data-toggle="tooltip" data-placement="top" Width="190px" ToolTip='<%# Eval("ManagementResponse") %>'></asp:TextBox>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time Line">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 130px;">
                                                <asp:TextBox ID="txtStartDate" class="form-control" style="width: 130px; text-align: center;" runat="server" Text='<%# Eval("TimeLine")!=null? Convert.ToDateTime(Eval("TimeLine")).ToString("dd-MMM-yyyy"):"" %>'></asp:TextBox>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="Person Responsible">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="tbxPersonResponsible" runat="server" Visible="false" Text='<%# Eval("PersonResponsible") %>' data-toggle="tooltip" data-placement="top" Width="100px" ToolTip='<%# Eval("PersonResponsible") %>'></asp:TextBox>
                                                    <asp:DropDownList runat="server" ID="ddlPersonResponsible" class="form-control m-bot15" Style="width: 150px;"
                                                    AutoPostBack="true">                                                                               
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="ddlPersonResponsible" />
                                                </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate> 
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>               
                                                <asp:LinkButton ID="lnkViewAudit" runat="server" CommandName="ViewObservation" CommandArgument='<%# Eval("ATBDId") %>'
                                                CausesValidation="false" ToolTip="View Audit Detail" data-toggle="tooltip" data-placement="top"><img src="../../Images/View-icon-new.png" alt="View Audit Detail"  /></asp:LinkButton>                                 
                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" OnClientClick="javascript:return getConfirmation();" CommandName="DeleteObservation" CommandArgument='<%# Eval("ATBDId") %>'
                                                CausesValidation="false" ToolTip="Remove Observation" data-toggle="tooltip" data-placement="top"><img src="../../Images/delete_icon_new.png" alt="Delete"  /></asp:LinkButton>                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                     <HeaderStyle BackColor="#ECF0F1" />       
                                    <PagerSettings Visible="false" />              
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate> 
                                    </asp:GridView>

                                        <div style="float: right;">
                                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                            class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                            </asp:DropDownListChosen>  
                                        </div>
                                    </div>
                              
                                    <div class="col-md-12 colpadding0">
                                        <div class="col-md-5 colpadding0">
                                            <div class="table-Selecteddownload">
                                                <div class="table-Selecteddownload-text">
                                                    <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                                </div>                                   
                                            </div>
                                        </div>

                                        <div class="col-md-6 colpadding0" style="float:right;">
                                            <div class="table-paging" style="margin-bottom: 10px;">
                                                <div class="table-paging-text" style="float: right;">
                                                    <p>
                                                        Page
                                                    </p>
                                                </div>
                                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                            </div>
                                        </div>
                                    </div> 
                                    <div>                                            
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-search"></asp:Button>
                                   </div>   
                           </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lblExport" />
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 25px;"></iframe>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
