﻿<%@ Page Title="MY DOCUMENTS" Language="C#" Async="true" EnableEventValidation="false" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDocumentList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument.ComplianceDocumentList" %>

<%--EnableEventValidation="false"--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function fComplianceOverview(obj) {

            OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
        }

        function fComplianceOverviewInternal(obj) {
           
            OpenOverViewpupInternal($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
        }

        $(document).on("click", "#ContentPlaceHolder1_upDocumentDownload", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });

            }
        });
        function fopendocfileReview(file) {
            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerStatutory').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        function fopendocfileReviewInternal(file) {
            $('#divViewInternalDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerInternal').attr('src', "../docviewer.aspx?docurl=" + file);
        }
       
        function initializeDatePicker11(date2) {
            var startDate = new Date();
            $('#<%= txtAdvStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: $('#<%= txtAdvStartDate.ClientID %>').val(),
                maxDate: $('#<%= txtAdvEndDate.ClientID %>').val(),
                numberOfMonths: 1
            });

            if (date2 != null) {
                $("#<%= txtAdvStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
            }
        }

        function initializeDatePicker12(date1) {
            var startDate = new Date();
            $('#<%= txtAdvEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: $('#<%= txtAdvStartDate.ClientID %>').val(),
                maxDate: $('#<%= txtAdvEndDate.ClientID %>').val(),
                numberOfMonths: 1
            });

            if (date1 != null) {
                $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "defaultDate", date1);
            }
        }

    
    function ShowDownloadDocument () {
        $('#divDownloadDocument').modal('show');
        return true;
    };

    function ShowViewDocument() {
        $('#divViewDocument').modal('show');
        return true;
    };

    
    function ShowInternalDownloadDocument() {
        $('#divInternalDownloadDocument').modal('show');
        return true;
    };

    function ViewInternalDocument() {
        $('#divViewInternalDocument').modal('show');
    };

      <%--  $(function () {
            $("#<%= txtAdvStartDate.ClientID %>").datepicker();
            $("#<%= txtAdvEndDate.ClientID %>").datepicker();           
        });--%>

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function showdiv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "block";
            $('.modal-backdrop').show();
            return true;
        }

       

        //$("[id*=chkCompliancesHeader]").live("click", function () {
        //    var chkHeader = $(this);
        //    var grid = $(this).closest("table");
        //    $("input[type=checkbox]", grid).each(function () {
        //        if (chkHeader.is(":checked")) {
        //            $(this).attr("checked", "checked");
        //        } else {
        //            $(this).removeAttr("checked");
        //        }
        //    });
        //});

        


        function SelectheaderCheckboxes(headerchk, gridname) {
            
            if (gridname == "grdComplianceDocument") {
                gvcheck = document.getElementById("<%=grdComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdInternalComplianceDocument") {
                gvcheck = document.getElementById("<%=grdInternalComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdReviewerComplianceDocument") {
                gvcheck = document.getElementById("<%=grdReviewerComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdReviewerInternalComplianceDocument") {
                gvcheck = document.getElementById("<%=grdReviewerInternalComplianceDocument.ClientID %>");
            }
            else {
               <%-- //gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");--%>
            }

    var i;

    if (headerchk.checked) {
        for (i = 0; i < gvcheck.rows.length; i++) {
            gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = true;
        }
    }

    else {
        for (i = 0; i < gvcheck.rows.length; i++) {
            gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = false;
        }
    }
    PageMethods
}

function Selectchildcheckboxes(header, gridname) {
    var i;
    var count = 0;
    var rolecolumn;
    var gvcheck;
    if (gridname == "grdComplianceDocument") {
        gvcheck = document.getElementById("<%=grdComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdInternalComplianceDocument") {
                gvcheck = document.getElementById("<%=grdInternalComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdReviewerComplianceDocument") {
                gvcheck = document.getElementById("<%=grdReviewerComplianceDocument.ClientID %>");
            }
            else if (gridname == "grdReviewerInternalComplianceDocument") {
                gvcheck = document.getElementById("<%=grdReviewerInternalComplianceDocument.ClientID %>");
            }
            else {
               <%-- gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");--%>
            }
    var headerchk = document.getElementById(header);
    var chkheaderid = header.split("_");

    var rowcount = gvcheck.rows.length;

    for (i = 1; i < gvcheck.rows.length - 1; i++) {
        if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
            count++;
        }
    }

    if (count == gvcheck.rows.length - 2) {
        headerchk.checked = true;
    }
    else {
        headerchk.checked = false;
    }
}

function initializeJQueryUI(textBoxID, divID) {
    $("#" + textBoxID).unbind('click');

    $("#" + textBoxID).click(function () {
        $("#" + divID).toggle("blind", null, 500, function () { });
    });
}

function checkAll(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
            cbox.checked = cb.checked;
        }
    }
}

function UncheckHeader() {
    var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
    var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}

    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>           
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">

                              <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">                                         
                                        <li class="active" id="li1" runat="server">
                                            <asp:LinkButton ID="LinkButton1"   runat="server">Compliance Document(s)</asp:LinkButton>                                           
                                        </li>
                                        <%-- <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID ==72 || com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID==5)
                                         {%>--%>
                                            <li class=""  id="li2" runat="server">
                                                <asp:LinkButton ID="LinkButton2"  PostBackUrl="~/Compliances/DocumentShareListNew.aspx" runat="server">Other Critical Document(s)</asp:LinkButton>                                        
                                            </li>
                                         <%--<%}%>  --%>
                                    </ul>
                                </header>
                            <div style="clear:both;height:15px;"></div>
                              <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer"  Style="font-size: 18px;" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                           <%}%>
                                            <%if (roles.Contains(4))%>
                                           <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" Style="font-size: 18px;" runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                          <%}%>
                                    </ul>
                                </header>

                        <div class="clearfix"></div>

<div class="panel-body" style="padding-top:5px;padding-bottom:5px;">
    <div class="col-md-12 colpadding0">

    <div class="col-md-2 colpadding0 entrycount" style="width:11%;">
        <div class="col-md-3 colpadding0" style=" margin-right: 13px;">
            <p style="color: #999; margin-top: 5px;   ">Show </p>
        </div>

        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 60px; float: left" 
            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
            <asp:ListItem Text="5" Selected="True"/>
            <asp:ListItem Text="10" />
            <asp:ListItem Text="20" />
            <asp:ListItem Text="50" />
        </asp:DropDownList> 

      
    </div>

    <div class="col-md-10 colpadding0" style="text-align: right; float: left;width:89%;">
        <div class="col-md-8 colpadding0" style="width:73%;">  
            
            <div style="float:left;margin-right: 1%;">
             <asp:DropDownList runat="server" ID="ddlDocType" class="form-control m-bot15 search-select" style="width:105px;" 
                 OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged" AutoPostBack="true">                
                <asp:ListItem Text="Statutory" Value="-1" />
                <asp:ListItem Text="Internal" Value="0" />
                <asp:ListItem Text="Event Based" Value="1" />
                <asp:ListItem Text="Statutory CheckList" Value="2" />
                <asp:ListItem Text="Internal CheckList" Value="3" />
            </asp:DropDownList>         
            </div>
            <div style="float:left;margin-right: 1%;">
            <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select"  style="width:80px;" >               
                <asp:ListItem Text="Risk" Value="-1" />
                <asp:ListItem Text="High" Value="0" />
                <asp:ListItem Text="Medium" Value="1" />
                <asp:ListItem Text="Low" Value="2" />
                
            </asp:DropDownList>
                  </div>
            <div style="float:left;margin-right: 1%;">
            <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select"  style="width:105px;" >
            </asp:DropDownList>
                        </div>
            <div style="float:left;margin-right: 1%;">
             <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 310px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                CssClass="txtbox" />   
                                                    
            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                </asp:TreeView>
            </div>  

            </div>
            <%--<asp:DropDownList runat="server" ID="ddlLocation" class="form-control m-bot15 select_location"  style="width:135px;" >
            </asp:DropDownList>--%>
        </div>

        <div class="col-md-2 colpadding0" style="width:27%;">
            <div class="col-md-6 colpadding0" style="margin-left: 10px;width: 41%;">               
                <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
            </div>
            <div class="col-md-6 colpadding0">
                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role!="AUDT"){%> 
                 <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.TaskApplicable=="1")%>
                 <%{%>
                    <button class="form-control m-bot15" type="button" style="margin-top: -40px; position: absolute; width: 150px;"  data-toggle="dropdown">More Documents
                    <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                    <ul class="dropdown-menu" style="top:0px !important;width:151px !important;margin: -8px 0px 0 !important; min-width:150px !important; ">
                        <li>  <a id="btntaskDocument" runat="server"  href="../Task/TaskDocumentList.aspx">Task Documents</a></li>                                                                                                                                               
                    </ul> 
                <%}%> 
                  <%}%>   
               <%-- <asp:Button Text="Advanced Search" class="btn btn-advanceSearch" runat="server" OnClientClick="return showdiv();" ValidationGroup="DocumentsValidation" />--%>
               <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search" style="width:150px;">Advanced Search</a>
            </div>
        </div>
    </div>  

 <div class="col-md-10 colpadding0" style="text-align: right; float: right">
 <div id="divEvent" class="col-md-12 colpadding0" style="text-align: right; float: right" runat="server" visible="false">
            <div style="float:left;margin-right: 2%;">
             <asp:DropDownList runat="server" ID="ddlEvent"  OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15" style="width:255px;">                
            </asp:DropDownList>         
            </div>
            <div style="float:left;margin-right: 2%;">
            <asp:DropDownList runat="server" ID="ddlEventNature"  class="form-control m-bot15" style="width:255px;" >               
            </asp:DropDownList>
            </div>
        </div>
    </div>  

    <!--advance search starts-->
    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>

                <div class="modal-body">
                    <h2 style="text-align: center;margin-top:10px;">Advanced Search</h2>
                    <div class="col-md-12 colpadding0">
                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                        <div class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>

                       <asp:Panel ID="PanelAct" runat="server">
                        <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                            <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                            </asp:DropDownList>
                        </div>
                        </asp:Panel>
                         <asp:Panel ID="Panelsubtype" runat="server">
                                    <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                        <asp:DropDownList runat="server" ID="ddlComplianceSubType" class="form-control m-bot15">
                                        </asp:DropDownList>
                                    </div>
                                </asp:Panel>
                        <asp:Panel ID="PanelSearchType" runat="server">
                            <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                 <asp:TextBox runat="server"  style="padding-left:7px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown = "return (event.keyCode!=13);"/>
                            </div></asp:Panel>
                        <asp:Panel ID="Panel1" runat="server">
                                 <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                  <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtAdvStartDate" CssClass="StartDate"/>
                               </div>  </asp:Panel>

                             <asp:Panel ID="Panel2" runat="server">
                                 <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                 <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtAdvEndDate" CssClass="StartDate"/>
                                </div> 
                         </asp:Panel>
                         <div class="clearfix"></div>

                <div class="table-advanceSearch-buttons" style="height:30px;margin:10px auto;">
                        <asp:Button Text="Search" class="btn btn-search" OnClick="Submit" runat="server" OnClientClick="return hidediv();" ValidationGroup="DocumentsValidation" />   
                            <button type="button" class="btn btn-search" data-dismiss="modal">Close</button> 
                                                            </div>

                        <div class="clearfix"></div>

                          <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-block alert-danger fade in"
                        ValidationGroup="DocumentsValidation" />
                    <asp:CustomValidator ID="cvDocuments" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="DocumentsValidation" Display="None" />

                        <br />
                      
                    </div>   
                </div>

            </div>
        </div>
    </div>
    <!--advance search ends-->

<!-- Advance Search scrum-->
          <div class="clearfix"></div>

         <div class="col-md-12 AdvanceSearchScrum">
                    <div id="divAdvSearch" runat="server" visible="false">
                 <p><asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label></p>
                <p> <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click"  runat="server">Clear Advanced Search Filter(s)</asp:LinkButton> </p>
            </div>

                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                       <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
          </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>


                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        </div>

                        <%--   <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right">
                <asp:Button Text="Set Filter" runat="server" ID="btnSetFilter" OnClick="btnSetFilter_Click"
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
                <asp:Button Text="Download" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click here to download documnet for selected compliances."
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>--%>
                        <div id="PerformerGrids" runat="server">
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                    OnRowCommand="grdComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdComplianceDocument_RowDataBound"
                                    OnPageIndexChanging="grdComplianceDocument_PageIndexChanging" DataKeyNames="ID" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                    <Columns>
                                       <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true"  /> <%--onclick="javascript:SelectheaderCheckboxes(this,'grdComplianceDocument')" CssClass="dummyHcheckbox" />--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCompliances" runat="server" OnCheckedChanged="chkCompliances_CheckedChanged" AutoPostBack="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:200px; ">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>                                           
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Reviewer">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">    
                                                <asp:Label ID="lblReviewer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetReviewer((long)Eval("ComplianceInstanceID"),"S") %>' ToolTip='<%#Reviewername%>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Eval("ForMonth")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     
                                           <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                  <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                       <asp:Label ID="lblStatus" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                  </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                                   <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                    CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ID") +" , "+Eval("FileID") %>' ToolTip="Download"></asp:ImageButton>
                                                         <asp:ImageButton ID="lblViewfile" runat="server" ImageUrl="~/Images/View-doc.png" CommandName="View" 
                                                    CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ID") +" , "+Eval("FileID") %>' ToolTip="View"></asp:ImageButton>
                                                  <asp:ImageButton ID="lblOverView1" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                                    OnClientClick='fComplianceOverview(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                                   </ContentTemplate>
                                                    <Triggers>
                                                    <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                    <asp:PostBackTrigger ControlID="lblViewfile" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                      <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdInternalComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                    OnRowCommand="grdInternalComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdInternalComplianceDocument_RowDataBound"
                                    OnPageIndexChanging="grdInternalComplianceDocument_PageIndexChanging" DataKeyNames="ID" AllowPaging="True" PageSize="5">
                                    <Columns>                                      

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkInternalCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true"  /> <%--onclick="javascript:SelectheaderCheckboxes(this,'grdInternalComplianceDocument')"--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkInternalCompliances" runat="server" OnCheckedChanged="chkCompliances_CheckedChanged" AutoPostBack="true"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Reviewer">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblReviewerInternal" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewer((long)Eval("InternalComplianceInstanceID"),"I") %>' ToolTip='<%#Reviewername%>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInternalScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                                              
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Eval("ForMonth")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                         <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lblStatusInternal" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status")%>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                     
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                            <asp:UpdatePanel ID="upDownloadInternal" runat="server">
                                            <ContentTemplate>
                                            <asp:ImageButton ID="lblDownLoadfileInternal" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                CommandArgument='<%# Eval("InternalScheduledOnID") + " , " + Eval("ID") +" , "+Eval("FileID") %>' ToolTip="Download"></asp:ImageButton>
                                             <asp:ImageButton ID="lblViewfileInternal" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="View" 
                                                    CommandArgument='<%# Eval("InternalScheduledOnID") + " , " + Eval("ID")+" , "+Eval("FileID")  %>' ToolTip="View"></asp:ImageButton>
                                             <asp:ImageButton ID="lblOverView2" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID")  %>'
                                                    OnClientClick='fComplianceOverviewInternal(this)' ToolTip="Click to OverView"></asp:ImageButton>

                                            </ContentTemplate>
                                            <Triggers>
                                            <asp:PostBackTrigger ControlID="lblDownLoadfileInternal" />
<asp:PostBackTrigger ControlID="lblViewfileInternal" />
                                            </Triggers>
                                            </asp:UpdatePanel>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                         <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>

                        <div id="ReviewerGrids" runat="server">
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    OnRowCommand="grdReviewerComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdReviewerComplianceDocument_RowDataBound"
                                    OnPageIndexChanging="grdComplianceDocument_PageIndexChanging" DataKeyNames="ID" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                    <Columns>
                                         <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkReviewerCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true"  /><%-- onclick="javascript:SelectheaderCheckboxes(this,'grdReviewerComplianceDocument')"--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkReviewerCompliances" runat="server" OnCheckedChanged="chkCompliances_CheckedChanged" AutoPostBack="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                          <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width:200px; ">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle Wrap="false" Width="200" /> 
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Performer">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((long)Eval("ComplianceInstanceID"),"S") %>' ToolTip='<%#Performername%>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                     

                                        <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>                                              
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Eval("ForMonth")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                     

                                         <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:Label ID="lblStatusReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:UpdatePanel ID="upDownloadFileReviewer" runat="server">
                                                   <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfileReviewer" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                    CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ID") +" , "+Eval("FileID") %>' ToolTip="Download"></asp:ImageButton>
                                                   <asp:ImageButton ID="lblViewfileReviewer" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="View" 
                                                    CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ID") +" , "+Eval("FileID") %>' ToolTip="View"></asp:ImageButton>
                                                <asp:ImageButton ID="lblOverView3" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                                    OnClientClick='fComplianceOverview(this)' ToolTip="Click to OverView"></asp:ImageButton>

                                                   </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lblDownLoadfileReviewer" />
                                                        <asp:PostBackTrigger ControlID="lblViewfileReviewer" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerInternalComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    OnRowCommand="grdReviewerInternalComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdReviewerInternalComplianceDocument_RowDataBound"
                                    OnPageIndexChanging="grdInternalComplianceDocument_PageIndexChanging" DataKeyNames="ID" AllowPaging="True" PageSize="5">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkReviewerInternalCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true"/> <%--onclick="javascript:SelectheaderCheckboxes(this,'grdReviewerInternalComplianceDocument')" --%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkReviewerInternalCompliances" runat="server" OnCheckedChanged="chkCompliances_CheckedChanged" AutoPostBack="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                          <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblShortDesc" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                      

                                        <asp:TemplateField HeaderText="Performer">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:Label ID="lblPerformerInternal" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# GetPerformer((long)Eval("InternalComplianceInstanceID"),"I") %>' ToolTip='<%#Performername%>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                     
                                        <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                 <asp:Label ID="lblInternalScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Period" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Eval("ForMonth")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                         <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:Label ID="lblStatusReviewerInternal" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                <asp:UpdatePanel ID="upDownLoadReviewerInternal" runat="server">
                                                <ContentTemplate>
                                                <asp:ImageButton ID="lblDownLoadfileReviewerInternal" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download" 
                                                    CommandArgument='<%# Eval("InternalScheduledOnID") + " , " + Eval("ID")+" , "+Eval("FileID")  %>' ToolTip="Download"></asp:ImageButton>
                                                  <asp:ImageButton ID="lblViewfileReviewerInternal" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="View" 
                                                    CommandArgument='<%# Eval("InternalScheduledOnID") + " , " + Eval("ID")+" , "+Eval("FileID")  %>' ToolTip="View"></asp:ImageButton>
                                                <asp:ImageButton ID="lblOverView4" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID")  %>'
                                                    OnClientClick='fComplianceOverviewInternal(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                                </ContentTemplate>
                                     <Triggers>
                                        <asp:PostBackTrigger ControlID="lblDownLoadfileReviewerInternal" />
                                        <asp:PostBackTrigger ControlID="lblViewfileReviewerInternal" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                    </div>
                                     <asp:Button Text="Download" ID="btnDownload" class="btn btn-search" runat="server" OnClick="btnDownload_Click" />   
                                    <%--<a class="btn btn-search" href="" title="Bootstrap 3 themes generator"><span class="icon_download"></span>Download </a>--%>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                         </section>
                    </div>
                </div>
            </div>      
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <%-- <asp:AsyncPostBackTrigger ControlID="ddlType" />     --%>
        </Triggers>
    </asp:UpdatePanel>

    

    <div>
        <div class="modal fade" id="divDownloadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 500px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <table width="100%" style="text-align: left; margin-left: 25%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                            OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblComplianceDocumnets">
                                                    <thead>
                                                        <th>Versions</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                            runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                            ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td valign="top">
                                        <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                            OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblComplianceDocumnets">
                                                    <thead>
                                                        <th>Compliance Related Documents</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("FileID")%>'
                                                            OnClientClick='javascript:enableControls()'
                                                            ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                            OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblWorkingFiles">
                                                    <thead>
                                                        <th>Compliance Working Files</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' class="btn btn-search"
                                                            ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
    

     <div>
        <div class="modal fade" id="divInternalDownloadDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 500px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">                      
                         <table width="100%" style="text-align: left; margin-left:25%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:Repeater ID="rptIComplianceVersion" runat="server" OnItemCommand="rptIComplianceVersion_ItemCommand"
                                            OnItemDataBound="rptIComplianceVersion_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblIComplianceDocumnets">
                                                    <thead>
                                                        <th>Versions</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' ID="lblIDocumentVersion"
                                                            runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                    <td>
                                                        <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                            ID="btnIComplinceVersionDoc" runat="server" Text="Download" style="margin-top:10%;">
                                                        </asp:LinkButton></td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td valign="top">
                                        <asp:Repeater ID="rptIComplianceDocumnets" runat="server" OnItemCommand="rptIComplianceVersion_ItemCommand"
                                            OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblIComplianceDocumnets">
                                                    <thead>
                                                        <th>Compliance Related Documents</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("FileID")%>'
                                                            OnClientClick='javascript:enableControls()'
                                                            ID="btnIComplianceDocumnets" runat="server" 
                                                            Text='<%# Eval("FileName") %>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                            OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblWorkingFiles">
                                                    <thead>
                                                        <th>Compliance Working Files</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton
                                                            CommandArgument='<%# Eval("FileID")%>'
                                                            ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                        </asp:LinkButton></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </thead>
                       </table>                       

                   </div>
                </div>
            </div>
        </div>
    </div>

 <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 25%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                            OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>                                                     
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                    </td>                                                                    
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>                                                        
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                                </fieldset>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="modal fade" id="divViewInternalDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 25%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <asp:Repeater ID="rptIComplianceVersionView" runat="server" OnItemCommand="rptIComplianceVersionView_ItemCommand"
                                                    OnItemDataBound="rptIComplianceVersionView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Versions</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version")+ ","+ Eval("FileID") %>' ID="lblIDocumentVersionView"
                                                                    runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                  <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdatleMode="Conditional">
                                <ContentTemplate>
                                <asp:Label runat="server" ID="lblMessageInternal" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerInternal" runat="server" width="100%" height="510px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--  <div id="divDownloadList">
        <asp:UpdatePanel ID="upDownloadList" runat="server">         
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="ModifyAsignmentValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Type</label>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Category</label>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <div style="margin-bottom: 7px">
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Act
                            </label>
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; margin-left: 50px; width: 390px;"
                                ReadOnly="true" CssClass="txtbox" Text="< Select >" />
                            <div style="margin-left: 150px; margin-left: 200px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <div runat="server" id="div3" style="margin-bottom: 7px;">
                            <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnRowDataBound="grdComplianceInstances_RowDataBound"
                                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                                BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px"
                                DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkFilterCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdComplianceInstances')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkFilterCompliances" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px">
                                                <asp:Label ID="lblShortDe" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                <PagerSettings Position="Top" />
                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div style="margin-bottom: 30px; margin-top: 15px;">
                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                Start Date</label>
                            <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; float: left; width: 200px;" ReadOnly="true"
                                MaxLength="200" />
                            <label style="width: 100px; padding-left: 30px; display: block; float: left; font-size: 13px; color: #333;">
                                End Date</label>
                            <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; float: left; width: 200px;" ReadOnly="true"
                                MaxLength="200" />
                        </div>
                        <div style="margin-bottom: 7px; float: right; margin-top: 30px;">
                            <asp:Button Text="Apply" runat="server" ID="btnApply" OnClick="btnApply_Click"
                                CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divDownloadList').dialog('close');" />
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>

    <script type="text/javascript">

        function OpenOverViewpup(scheduledonid, instanceid) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

        }

        function OpenOverViewpupInternal(scheduledonid, instanceid) {
          
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1050px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1100px');
            $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

        }

        $(document).ready(function () {
             <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID ==72 || com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID==5)
             {%>
                 fhead('My Document/Compliance Document(s)');
            <%} else {%>
                 fhead('My Document');
              <%}%>

            setactivemenu('leftdocumentsmenu');
        });


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        <%-- $("body").click(function (e) {
           
            if (e.target !== "divFilterLocation" && e.target.id !== '<%= tbxFilterLocation.ClientID %>'&& e.target.id !== '<%= tvFilterLocation.ClientID %>'+1)
            {
                
                $("#divFilterLocation").hide();
            }
        });--%>
    </script>
</asp:Content>
