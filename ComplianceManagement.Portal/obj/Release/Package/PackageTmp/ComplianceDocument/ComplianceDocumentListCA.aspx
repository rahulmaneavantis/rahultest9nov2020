﻿<%@ Page Title="Compliance Document" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDocumentListCA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument.ComplianceDocumentListCA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#divDownloadList').dialog({
                height: 600,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Manage Document Filter",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divDownloadDocument').dialog({
                height: 400,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Download Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

        });

        function SelectheaderCheckboxes(headerchk, gridname) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");

            if (gridname == "grdComplianceDocument") {
                gvcheck = document.getElementById("<%=grdComplianceDocument.ClientID %>");
         } else {
             gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
         }

         var i;

         if (headerchk.checked) {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
             }
         }

         else {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
             }
         }
     }

     function Selectchildcheckboxes(header, gridname) {
         var i;
         var count = 0;
         var rolecolumn;
         var gvcheck;
         if (gridname == "grdComplianceDocument") {
             gvcheck = document.getElementById("<%=grdComplianceDocument.ClientID %>");
         } else {
             gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
         }
         var headerchk = document.getElementById(header);
         var chkheaderid = header.split("_");

         var rowcount = gvcheck.rows.length;

         for (i = 1; i < gvcheck.rows.length - 1; i++) {
             if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                 count++;
             }
         }

         if (count == gvcheck.rows.length - 2) {
             headerchk.checked = true;
         }
         else {
             headerchk.checked = false;
         }
     }

     function initializeCombobox() {

         $("#<%= ddlComplinceCatagory.ClientID %>").combobox();
         $("#<%= ddlFilterComplianceType.ClientID %>").combobox();

     }

     function initializeDatePicker(date) {

         var startDate = new Date();
         $("#<%= txtStartDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             numberOfMonths: 1,
             onClose: function (startDate) {
                 $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
             }
         });

             $("#<%= txtEndDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             defaultDate: startDate,
             numberOfMonths: 1,
             minDate: startDate,

         });


         if (date != null) {
             $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
             $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
         }
     }

     function initializeJQueryUI(textBoxID, divID) {
         $("#" + textBoxID).unbind('click');

         $("#" + textBoxID).click(function () {
             $("#" + divID).toggle("blind", null, 500, function () { });
         });
     }

     function checkAll(cb) {
         var ctrls = document.getElementsByTagName('input');
         for (var i = 0; i < ctrls.length; i++) {
             var cbox = ctrls[i];
             if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                 cbox.checked = cb.checked;
             }
         }
     }

     function UncheckHeader() {
         var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
         var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
         var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
         if (rowCheckBox.length == rowCheckBoxSelected.length) {
             rowCheckBoxHeader[0].checked = true;
         } else {

             rowCheckBoxHeader[0].checked = false;
         }
     }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upDocumentDownload" runat="server">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right">
                <asp:Button Text="Set Filter" runat="server" ID="btnSetFilter" OnClick="btnSetFilter_Click"
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
                <asp:Button Text="Download" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click here to download documnet for selected compliances."
                    CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>
            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdComplianceDocument" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdComplianceDocument_RowCommand"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnRowDataBound="grdComplianceDocument_RowDataBound"
                    OnPageIndexChanging="grdComplianceDocument_PageIndexChanging" Font-Size="12px" DataKeyNames="ID">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdComplianceDocument')" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkCompliances" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="ShortDescription" HeaderText="Compliance Name"  ItemStyle-Height="20px"  HeaderStyle-Height="20px" SortExpression="ShortDescription"/>--%>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="Description">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="ScheduledOn" HeaderText="Scheduled On" SortExpression="ScheduledOn"/>--%>
                        <asp:TemplateField HeaderText="Scheduled On" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                            <ItemTemplate>
                                <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="For Month" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                            <ItemTemplate>
                                <%# Eval("ForMonth")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="Dated" HeaderText="Dated" SortExpression="Dated"/>--%>
                        <%-- <asp:TemplateField HeaderText="Dated" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="Dated" >
                    <ItemTemplate>
                        <%# Eval("Dated") != null ? ((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy") : ""%>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%--<asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>--%>
                                        <asp:LinkButton ID="lblDownLoadfile" runat="server" CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + " , " + Eval("ID") %>'
                                            ToolTip="Please download file from here.">Download</asp:LinkButton>
                                    <%--</ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lblDownLoadfile" />

                                    </Triggers>
                                </asp:UpdatePanel>--%>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:AsyncPostBackTrigger ControlID="grdComplianceDocument" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="divDownloadList">
        <asp:UpdatePanel ID="upDownloadList" runat="server" OnLoad="upDownloadList_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="ModifyAsignmentValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Type</label>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Category</label>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                        <div style="margin-bottom: 7px">
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Act
                            </label>
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; margin-left: 50px; width: 390px;"
                                ReadOnly="true" CssClass="txtbox" Text="< Select >" />
                            <div style="margin-left: 150px; margin-left: 200px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>

                                </asp:Repeater>

                            </div>

                        </div>
                        <div runat="server" id="div3" style="margin-bottom: 7px;">
                            <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnRowDataBound="grdComplianceInstances_RowDataBound"
                                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                                BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px"
                                DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkFilterCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdComplianceInstances')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkFilterCompliances" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                <PagerSettings Position="Top" />
                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div style="margin-bottom: 30px; margin-top: 15px;">
                            <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                Start Date</label>
                            <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; float: left; width: 200px;" ReadOnly="true"
                                MaxLength="200" />
                            <label style="width: 100px; padding-left: 30px; display: block; float: left; font-size: 13px; color: #333;">
                                End Date</label>
                            <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; float: left; width: 200px;" ReadOnly="true"
                                MaxLength="200" />
                        </div>
                        <div style="margin-bottom: 7px; float: right; margin-top: 30px;">
                            <asp:Button Text="Apply" runat="server" ID="btnApply" OnClick="btnApply_Click"
                                CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divDownloadList').dialog('close');" />
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divDownloadDocument">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="upDownloadList_Load">
            <ContentTemplate>
                <table width="100%" style="text-align: left">
                    <thead>
                        <tr>
                            <td valign="top">
                                <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                    OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblComplianceDocumnets">
                                            <thead>
                                                <th>Versions</th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                    runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                            <td>
                                                <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                    ID="btnComplinceVersionDoc" runat="server" Text="Download">
                                                </asp:LinkButton></td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                            <td valign="top">
                                <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                    OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblComplianceDocumnets">
                                            <thead>
                                                <th>Compliance Related Documents</th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton
                                                    CommandArgument='<%# Eval("FileID")%>'
                                                    OnClientClick='javascript:enableControls()'
                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                </asp:LinkButton></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                    OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tblWorkingFiles">
                                            <thead>
                                                <th>Compliance Working Files</th>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton
                                                    CommandArgument='<%# Eval("FileID")%>'
                                                    ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                </asp:LinkButton></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </thead>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
