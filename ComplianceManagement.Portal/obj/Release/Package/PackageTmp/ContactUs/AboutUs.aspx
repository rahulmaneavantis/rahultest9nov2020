﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.AboutUs" %>

<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Avantis Main 1">
<meta name="keywords" content="Homepage, Avantis">
<!-- Title -->
<title>About Us | Avantis</title>
<!-- Favicon -->

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700,800' rel='stylesheet' type='text/css'>
<!-- Stylesheets -->
<link rel='stylesheet' id='twitter-bootstrap-css' href='../Style/NewCss/bootstrap.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='fontello-css' href='../Style/NewCss/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' id='prettyphoto-css-css' href='../Scripts/New/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' id='animation-css' href='../Style/NewCss/animation.css' type='text/css' media='all'/>
<link rel='stylesheet' id='flexSlider-css' href='../Style/NewCss/flexslider.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='../Style/NewCss/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='../Style/NewCss/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='../Style/NewCss/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='../Style/NewCss/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='../Style/NewCss/mobilenav.css' type='text/css' media="screen and (max-width: 838px)"/>
<!-- jQuery -->
<script src="../Scripts/New/jquery-1.11.1.min.js"></script>
<!-- <script src="js/hightlight.js"></script> -->
<!-- Google Maps -->
<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.0'></script>
<!--[if lt IE 9]>
            <script>
                document.createElement("header");
                document.createElement("nav");
                document.createElement("section");
                document.createElement("article");
                document.createElement("aside");
                document.createElement("footer");
                document.createElement("hgroup");
            </script>
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/html5.js"></script>
        <![endif]-->
<!--[if lt IE 7]>
            <script src="js/icomoon.js"></script>
        <![endif]-->
<!--[if lt IE 9]>
            <link href="css/ie.css" rel="stylesheet">
        <![endif]-->
<!--[if lt IE 9]>
            <script src="js/jquery.placeholder.js"></script>
            <script src="js/script_ie.js"></script>
        <![endif]-->
</head>
<body class="w1170 headerstyle1 preheader-on">
<!-- Content Wrapper -->
<div id="Avantis-content-wrapper">
	<header id="header" class="style1">
<!--div id="upper-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="item left hidden-separator">
					<ul id="menu-shop-header" class="menu">
						
						<li class="menu-item"><a href="#">Wishlist</a></li>
						<li class="menu-item"><a href="#">My Account</a></li>
					</ul>
				</div>
				<div class="item right hidden-separator">
					<div class="cart-menu-item ">
						<a href="#">0 ITEMS(S) - <span class="amount">&pound;0.00</span></a>
						<div class="shopping-cart-dropdown">
							<div class="sc-header">
								<h4>Cart is empty</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Main Header -->
<div id="main-header">
	<div class="container">
		<div class="row">
			<!-- Logo -->
			<div class="col-lg-4 col-md-4 col-sm-4 logo">
				<a href="~/Login.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="col-sm-8 align-right">
				<!-- Text List -->
				<!--<ul class="text-list">
					<li>Call Us: +91 98234 92350</li>
				</ul>-->
				<!-- Social Media -->
				<!--<ul class="social-media">
					<li><a target="_blank" href="#"><i class="icon-facebook"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-twitter"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-google"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-linkedin"></i></a>
					</li>
					<li><a target="_blank" href="#"><i class="icon-instagram"></i></a>
					</li>
				</ul>-->
                <ul  class="menu top_menu" style="border-right:none;margin: 25px 0px 0px;">
					<li class="menu-item"><a href="Index.aspx">JOIN US</a></li>
                    <li class="menu-item"><a href="../Login.aspx">CUSTOMER LOGIN</a></li>
					<li class="menu-item"><a href="ContactUs.aspx">CONTACT US</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /Main Header -->
<!-- Lower Header -->
<div id="lower-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="lower-logo">
					<a href="~/Login.aspx" title="Avantis" rel="home"><img class="logo" src="../Images/img/logo.png" alt="Avantis"></a>
				</div>
				<!-- Main Navigation -->
				<ul id="main-nav" class="menu">
					<li class="menu-item"><a href="Index.aspx" >Home</a></li>
                    <li class="menu-item"><a href="Product.aspx">Product</a></li>
                    <li class="menu-item"><a href="AboutUs.aspx" style="color: #0294D3;">About</a></li>
                    <li class="menu-item"><a href="#">Case Studies</a></li>
					<li class="menu-item"><a href="#">Blog</a></li>
				</ul>
				<!-- /Main Navigation -->
				<!-- Search Box -->
				<div id="search-box" class="align-right">
					<i class="icons icon-search"></i>
					<form role="search" method="get" id="searchform" action="#">
						<input type="text" name="s" placeholder="Search here..">
						<div class="iconic-submit">
							<div class="icon">
								<i class="icons icon-search"></i>
							</div>
							<input type="submit" value="">
						</div>
					</form>
				</div>
				<!-- /Search Box -->
			</div>
		</div>
	</div>
</div>
<!-- /Lower Header -->
</header>
	<!-- /Header -->
	
	<div id="Avantis-content-inner">
	<!-- Main Content -->
    
	<section id="main-content">
	<!-- Container -->
	
		<!-- Google Map -->
		
			
				
					
				
		<div class="container">	
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 small-padding" style="padding-top:0px !important">
				
				<!--<div class="page-heading style3 wrapper border-bottom ">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h1>About</h1>
				</div>
				
			</div>
		</div>-->
				
				

<!--<div class="clearfix clearfix-height20"></div>
--><h2 style="padding:30px 0px 0px;text-align: center; font-weight: bold;">Avantis</h2>
<h6>Avantis is a products company born out of the experience of its founders

and advisors Sandeep, Chetan and Rishi. Avantis makes intuitive, scalable 

and flexible products to alleviate business pain-points by leveraging 

technology cost-effectively.</h6>
<hr style="color:rgba(240, 241, 243, 1); margin: 30px 0px;">

<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">                <img src="../Images/img/new/sandeep.jpg"><br>
<div style="text-align:center"><h4>Sandeep Agrawal</h4><h6 style="line-height: 12px;">Founder Director</h6><p class="text-justify">Sandeep has extensive experience as

in-house counsel for one of the Big Four accounting firms. A Chartered 

Accountant (ICAI) and Certified Information Systems Auditor (ISACA, 

USA), he has been a risk management and compliance professional for 

more than a decade and has supported over 100+ medium and large 

companies in aligning their risk and processes. He has also conducted 

Risk Assessment Workshops for a few large companies in India. He knew 

from experience the agony of managing compliances in diverse industries 

in India and abroad. He has envisaged technological solutions to help 

businesses sail through compliance with ease.</p></div></div>
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">                <img src="../Images/img/new/chetan.jpg"><br>
<div style="text-align:center"><h4>Chetan Paranjpe</h4><h6 style="line-height: 12px;">Director</h6><p class="text-justify">Chetan is an investor and an entrepreneur.

Over the last decade, he has worked with CXOs across a broad section of 

Indian industries. He brings rich sales and marketing experience across 

financial and credit risk industries. He has worked with leading 

corporates in India such as Airtel, Axis Bank, Yes Bank, CARE (Credit 

Analysis and Research Ltd) prior to his current venture. He is passionate 

about growth and execution. He has lead firms to exponential revenue 

and business growth</p></div></div>

<div class="col-lg-4 col-md-4 col-sm-4 text-center">                <img src="../Images/img/new/rishi.jpg"><br>
<div style="text-align:center"><h4>Rishi Agrawal</h4><h6 style="line-height: 12px;">Advisor</h6><p class="text-justify">Rishi has an MBA from Indian Institute of

Management, Calcutta (IIM Calcutta) and B.Tech from IIT Varanasi. He 

brings deep leadership and capacity building experience from his time in 

Executive Leadership at BNY Mellon. He led application development and 

delivery, global infrastructure services and data centers there. He is your 

go-to guy for strategy and bottom-line enhancement. He pursued an 

advanced Leadership Development Program at Wharton School of 

Business and has since turned entrepreneur.</p></div></div>

                
                </div>	</div>
		

<p>Enterprise governance, risk management, and compliance have become

critical to business management. At Avantis we believe that the center of 

an effective compliance program is the compliance software application 

that is easily used with minimal overhead throughout the organization. 

 Compliance software should enable effective and responsive 

management of compliance activities related to laws, regulations, 

standards and internal policies. Avantis team understands that 

organizational needs change depending on its stage of evolution and the 

prevailing market conditions. We, therefore, engage closely with the legal 

and business teams of organization to understand their environment and 

customize our product. Now organisations can stay compliant with 

minimum effort while they focus on their core business. We offer a 

scalable point of reference to manage and monitor compliance processes 

across all business units (entities) in India or abroad.</p>
<hr style="color:rgba(240, 241, 243, 1); margin: 30px 0px;">

<h2 style="text-align:center; font-size: 30px;
    font-weight: 400;">The Avantis Advantage</h2><br>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-users" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">Highly qualified team</h5>
									
								</div>
                                <div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-thumbs-up" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">International standards of professionalism</h5>
									
								</div>
                               <div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-clock" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">Timely response</h5>
									
								</div>
</div><br>
<br>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-target" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">High degree of client-focus and flexibility</h5>
									
								</div>
                                <div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-desktop" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">Systems focus ensures uninterrupted client experience</h5>
									
								</div>
                                <div class="col-lg-4 col-md-4 col-sm-6" style="text-align:center">
									<i class="icons icon-back-in-time" style=" font-size:91px; color:#3EC1DB"></i>
									<h5 class="special-text Avantis-special-services-h3" data-animation="">24 x 7 x 365 support</h5>
									
								</div>
</div>
<!--<h2>Avacom</h2>
<p>Avacom provides highly automated and flexible compliance solution to organisations in 

diverse industries like Avantis, manufacturing, IT, media and financial institutions. It 

allows access to compliance monitoring dashboards from anywhere on the planet. If you 

can launch a browser you can access avacom.</p>
<p>AVACOM, a Statutory Compliance Tool, is a highly adaptable

software product designed to improve compliance management that replaces people 

dependent manual processes. Avacom manages your compliances, stores your 

documents, and enables audits and seamless monitoring across the globe.</p><a href="product.html" class="button small"><i class="icons icon-info-circled-alt"></i>READ MORE</a>-->				<!--div class="container">
					<div class="row">
						<section class="col-lg-12 col-md-12 col-sm-12 small-padding">
						<div class="full_bg full-width-bg Avantis-container-bg" data-animation="">
							<h2 class="align-center"><span class="Avantis-header6-h3-color">Our Team</span></h2>
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 1">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 1</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 2">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 2</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Sandra Green">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Sandra Green</h4>
										<span class="job-title Avantis-main-h4-span">Manager</span>
										<div class="text-content Avantis-text-content">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="team-member " data-animation="">
										<a href="#" target="_blank" title="Person 3">
										<img src="http://placehold.it/190x182" alt="img-responsive"/>
										</a>
										<a class="read-more"></a>
										<h4 class="post-title Avantis-team-h4">Person 3</h4>
										<span class="job-title Avantis-main-h4-span">Lorem ipsum</span>
										<div class="text-content Avantis-text-content ">
											<p>
												 Vivamus pretium imperdiet dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius ali luctus. Praesent
											</p>
										</div>
										<span class="small-line"></span>
										<ul class="social-media">
											<li><a href="#" class="Avantis-text-content"><i class="icon-facebook"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-twitter"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-skype"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-google"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-vimeo"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-linkedin"></i></a></li>
											<li><a href="#" class="Avantis-text-content"><i class="icon-instagram"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="clearfix">
							</div>
						</div>
						<div class="clearfix clearfix-height30">
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6">
								<img src="http://placehold.it/199x106" class="alignnone dont_scale wow animated fadeInLeft" data-animation="fadeInLeft" alt="">
							</div>
						</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Container -->
	</section>
	<!-- /Main Content -->
</div>
	<!-- /Avantis Conten Inner -->
	<!-- Footer -->
<footer id="footer">
<!-- Upper Footer -->

<!-- /Upper Footer -->
<!-- Main Footer -->
<div id="main-footer" class="smallest-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-2" class="widget widget_text">
					<div class="textwidget">
						<img src="../Images/img/logo.png" alt="logo">
						<p>
							Avantis’ Compliance Product provides a comprehensive Solution to Organization’s complete compliance requirements. It is a secured web based product which can be accessed by all business and management users over the internet
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div id="text-4" class="widget widget_text">
					<h4>Contact</h4>
					<div class="textwidget">
						<ul class="iconic-list">
							<li>
							<i class="icons icon-location-7"></i>
							Pune, Maharashtra, India
							<li>
							<i class="icons icon-mobile-6"></i>
							Phone: +91 98234 92350 <br>
							 Fax: +91 98234 92350 </li>
							<li>
							<i class="icons icon-mail-7"></i>
							sandeep@avantis.co.in </li>
						</ul>
					</div>
				</div>
			</div>
			<%--<div class="col-lg-4 col-md-4 col-sm-4">
				<h4>Login</h4>
				<form class="get-in-touch contact-form wow animated fadeInUp align-center animated" method="post" style="visibility: visible; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
						<input type="hidden" id="contact_nonce" name="contact_nonce" value="68592e213c"><input type="hidden" name="" value="/">
						<input type="hidden" name="contact-form-value" value="1">
						<div class="iconic-input">
							<input type="text" name="name" placeholder="Name*">
							<i class="icons icon-user-1"></i>
						</div>
						<div class="iconic-input">
							<input type="password" name="passowrd" placeholder="passowrd*">
							<i class="icons icon-email"></i>
						</div>
						<input type="submit" value="Login" style="float:right;">
						
					</form>
			</div>--%>
		</div>
	</div>
</div>
<!-- /Main Footer -->
<!-- Lower Footer -->
<div id="lower-footer">
	<div class="container">
		<span class="copyright">© 2014 Avantis. All Rights Reserved</span>
	</div>
</div>
<!-- /Lower Footer -->
</footer>
	<!-- /Footer -->
</div>
<!-- /Content Wrapper -->
<!-- JavaScript -->
<script type='text/javascript' src='../Scripts/New/bootstrap.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery-ui.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/SmoothScroll.min.js'></script>
<script type='text/javascript' src='../Scripts/New/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='../Scripts/New/modernizr.js'></script>
<script type='text/javascript' src='../Scripts/New/wow.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.sharre.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.knob.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='../Scripts/New/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='../Scripts/New/jquery.fitvids.js'></script>
<script type='text/javascript' src='../Scripts/New/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='../Scripts/New/jquery.validity.min.js'></script>
<script type='text/javascript' src='../Scripts/New/tweetie.min.js'></script>
<script type='text/javascript' src='../Scripts/New/script.js'></script>
</body>
</html>
