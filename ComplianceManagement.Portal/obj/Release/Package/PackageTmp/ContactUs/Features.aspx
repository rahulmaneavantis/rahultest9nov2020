﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Features.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.Features" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Features</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="../Style/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="../Style/tab.css" type="text/css" media="all" />

<script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {

        //Default Action
        $(".tab_content").hide(); //Hide all content
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content

        //On Click Event
        $("ul.tabs li").click(function () {
            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active content
            return false;
        });

    });
</script>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper">
  <div class="shell">
    <div id="wrapper-top"></div>
    <!-- Wrapper Middle -->
    <div id="wrapper-middle"> 
      <!-- Header -->
      <div id="header"> 
        <!-- Logo -->
        <div id="logo"> <img src="../Images/avantis.png" height="" width="200"/> </div>
        <div class="socials"> <a title="Facebook" class="facebook" href="#">facebook</a> <a title="Twitter" class="twitter" href="#">twitter</a> 
        <a title="LinkedIn" class="LinkedIn" href="#">LinkedIn</a></div>
        <div id="navigation">
          <ul>
            <li><asp:HyperLink class="selected" ID="hlhome" runat="server" NavigateUrl="~/Login.aspx">Home</asp:HyperLink></li>
            <li><asp:HyperLink class="selected" ID="hlFeatures" runat="server" NavigateUrl="~/ContactUs/Features.aspx">Features</asp:HyperLink></li>
            <li><asp:HyperLink class="selected" ID="hlsignUp" runat="server" NavigateUrl="~/ContactUs/SignUp.aspx">Sign Up</asp:HyperLink></li>
            <li><asp:HyperLink  class="selected" ID="hlContactUs" runat="server" NavigateUrl="~/ContactUs/ContactUs.aspx">Contact Us</asp:HyperLink></li>
          </ul>
          <div class="cl"></div>
        </div>
      </div>
      <div id="main">
        <div class="heading">
          <h1> Features, Benefits & Approach</h1>
		  </div>
	<div class="container">
    <ul class="tabs">
    <li><a href="#tab1">Features</a></li>
    <li><a href="#tab2">Benefits</a></li>
    <li><a href="#tab3">Approach</a></li>
  </ul>
  <div class="tab_container">
    <div id="tab1" class="tab_content">
      <h2>Features</h2>
             <div class="widgets-tab">
      
       <!--  <div class="one-half-img"> <img src="css/images/dummy-img.png"  alt="" /></div>-->
<p>Avantis’ Compliance Product provides a comprehensive Solution to Organization’s complete compliance requirements. 
It is a secured web based product which can be accessed by all business and management users over the internet.</p>
<div class="heading-mn">Our Product provides the following services:</div>   
<ul>      
 <li><span class="bg-para">Compliance Repository :</span>
<p>The product provides a repository of Laws applicable to the various functions of a company across locations in India. The repository includes the regulations pertaining to Central and State legislations.
</p><p>The product also provides select legislative forms for reference.</p>
</li>
 <li><span class="bg-para">Customer’s Organization Structure :</span>
</p><p>Product has the capabilities to replicate complete Compliance Organogram (i.e. organizational hierarchy from a compliance perspective). The product has been designed to accommodate significant changes to the client’s organizational structure (especially for clients growing inorganically).
</li>
<li><span class="bg-para">User Profiles and Assignments :</span>
<p>This is a unique feature where the organization can create multiple user profiles and also assign multiple profiles to a single user. The Product is highly flexible wherein multiple permutations and combinations are permissible with respect to assignment of compliances and locations.
</p></li>
<li><span class="bg-para">Compliance Workflows :</span>
<p>Workflows are assigned for each compliance item and product facilitates workflow based escalations and compliance approval mechanism.
</p></li>
<li><span class="bg-para">Automated Notifications :</span>
<p>Automated e-mail Notifications for compliance activities whereby Organization can create a support mechanism for the Users for timely compliance.
</p><p>Product also provides an auto-trigger mechanism to reporting managers for the non-compliance or delayed compliance.
</p></li>
<li><span class="bg-para">Management Reporting :</span>
<p>Product offers a comprehensive Dashboard for each user profile with features of risk-wise and location-wise. These Dashboards are dynamically updated and provides a comprehensive overview of the compliance status for the entire Group as well.
</p><p>One of the important reporting tools offered by the product is the Performance Rating of each unit/location based on the actual compliance status. This report highlights the performing / non-performing entities which can be further drilled down to the performing / non-performing. 
</p></li>
<li><span class="bg-para">Audit Tool and Audit Trails :</span>
<p>Online auditing of the compliance recorded has been provided in the product. The product has been designed in a manner where the compliance workflows do not end until the compliance and related documents have been reviewed by the reporting managers.
</p>
<p>Product also tracks the details of the workflow at each step and required Audit Trails to track compliance at every step is available through the Product.
</p></li>
<li><span class="bg-para">Administrative Module :</span>
<p>Organization can assign Administrative rights to one its IT team member to manage user groups and compliances.
</p></li>
</ul>
     
          <div class="cl"></div>
          
          
        </div>
    </div>
    <div id="tab2" class="tab_content">
      <h2>Benefits</h2>
      <p> <img src="../Images/benefits.jpg" alt="" width="910"/></p>
    </div>
    <div id="tab3" class="tab_content">
      <h2>Approach</h2>
     <%-- <p> <img src="../Images/Implementation-Approach–Option1.jpg" alt="" width="910"/></p>--%>
      <p> <img src="../Images/Implementation-Approach–Option2.jpg" alt="" width="910"/></p>
    </div>
  </div>
</div>
        <!-- Content -->
        <div class="cl"></div>

      </div>
      <!-- END Main --> 
    </div>
    <!-- END Wrapper Middle -->
    <div id="wrapper-bottom"></div>
    <!-- Footer -->
    <div id="footer">
      <p class="copy">© 2014</p>
      <p id="bottom-nav"><asp:HyperLink class="selected" ID="HyperLink1" runat="server" NavigateUrl="~/Login.aspx">Home</asp:HyperLink><span>|</span><asp:HyperLink class="selected" ID="HyperLink2" runat="server" NavigateUrl="~/ContactUs/Features.aspx">Features</asp:HyperLink><span>|</span><asp:HyperLink class="selected" ID="HyperLink3" runat="server" NavigateUrl="~/ContactUs/SignUp.aspx">Sign Up</asp:HyperLink><span>|</span><asp:HyperLink  class="selected" ID="HyperLink4" runat="server" NavigateUrl="~/ContactUs/ContactUs.aspx">Contact Us</asp:HyperLink></p>
      <div class="cl"></div>
    </div>
    <!-- END Footer --> 
  </div>
</div>
<!-- END Wrapper --> 
<script type="text/javascript">    Cufon.now(); </script>
</body>
</html>
