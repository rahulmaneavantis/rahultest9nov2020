﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContactUs.SignUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sign Up</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="../Style/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="../Style/validationEngine.jquery1.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<link rel="stylesheet" href="../Style/template.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>
		<script src="../Scripts/jquery.validationEngine-en1.js" type="text/javascript"></script>
		<script src="../Scripts/jquery.validationEngine1.js" type="text/javascript"></script>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper">
  <div class="shell">
    <div id="wrapper-top"></div>
    <!-- Wrapper Middle -->
    <div id="wrapper-middle"> 
      <!-- Header -->
      <div id="header"> 
        <!-- Logo -->
        <div id="logo"> <img src="../Images/avantis.png" height="" width="200"/> </div>
        <div class="socials"> <a title="Facebook" class="facebook" href="#">facebook</a> <a title="Twitter" class="twitter" href="#">twitter</a> 
        <a title="LinkedIn" class="LinkedIn" href="#">LinkedIn</a></div>
        <div id="navigation">
         <ul>
             <li><asp:HyperLink class="selected" ID="hlhome1" runat="server" NavigateUrl="~/Login.aspx">Home</asp:HyperLink></li>
            <li><asp:HyperLink class="selected" ID="hlFeatures1" runat="server" NavigateUrl="~/ContactUs/Features.aspx">Features</asp:HyperLink></li>
            <li><asp:HyperLink class="selected" ID="hlsignUp1" runat="server" NavigateUrl="~/ContactUs/SignUp.aspx">Sign Up</asp:HyperLink></li>
            <li><asp:HyperLink  class="selected" ID="hlContactUs1" runat="server" NavigateUrl="~/ContactUs/ContactUs.aspx">Contact Us</asp:HyperLink></li>
          </ul>
          <div class="cl"></div>
        </div>
      </div>
      <div id="main">
        <div class="heading">
          <h1> Signup</h1>
        </div>
        <!-- Content -->
        <div class="cl"></div>
        <div class="widgets" style="text-align:center">
          <h2 style="text-align:center">This page is under construction.</h2>
    </div>
    </div>
      <!-- END Main --> 
    </div>
    <div id="wrapper-bottom"></div>
    <div id="footer">
      <p class="copy">© 2014</p>
      <p id="bottom-nav"><asp:HyperLink class="selected" ID="HyperLink11" runat="server" NavigateUrl="~/Login.aspx">Home</asp:HyperLink><span>|</span><asp:HyperLink class="selected" ID="HyperLink21" runat="server" NavigateUrl="~/ContactUs/Features.aspx">Features</asp:HyperLink><span>|</span><asp:HyperLink class="selected" ID="HyperLink31" runat="server" NavigateUrl="~/ContactUs/SignUp.aspx">Sign Up</asp:HyperLink><span>|</span><asp:HyperLink  class="selected" ID="HyperLink41" runat="server" NavigateUrl="~/ContactUs/ContactUs.aspx">Contact Us</asp:HyperLink></p>
      <div class="cl"></div>
    </div>
    </div>
    
     </div>
</body>
</html>
