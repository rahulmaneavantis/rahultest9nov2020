﻿<%@ Page Title="Upload Customer" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadCustomer.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .col-sm-6 {
            height: 64px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                    class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" ForeColor="red" />
                <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                    ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
            </asp:Panel>
            <br />
            <br />
            <table align="left" width="100%">
                <tr>
                    <td width="20%">
                        <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                        <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                            runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                    </td>
                    <td width="20%">
                        <asp:Button ID="btnUploadFile" runat="server" OnClick="btnUploadFile_Click" Style="margin-left: 75px;" Text="Upload" ValidationGroup="oplValidationGroup" />
                    </td>
                    <td width="40%"></td>
                    <td width="20%">
                        <%--<asp:LinkButton ID="lnkSampleFormat" runat="server" class="newlink" data-placement="bottom" data-toggle="tooltip" Font-Underline="True" Text="Sample Format(Excel)" ToolTip="Download Sample Excel Document Format for Customer Upload"></asp:LinkButton>
                        --%>
                        <u><a href="../../AuditSampleDocument/Service Provider_Sample.xlsx">Sample Format(Excel)</a></u>                                    
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
