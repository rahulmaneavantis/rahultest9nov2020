﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DumpMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.DumpMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Newjs/jquery.js"></script>
     

</head>
<body>
    <form id="form1" runat="server">

        <div style="margin-bottom: 7px">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                Requested User Name</label>
            <asp:DropDownList ID="drpUserlst" style="width:275px;" runat="server" AutoPostBack="true"></asp:DropDownList>
        </div>
        <div style="margin-bottom: 7px">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                Act ID List</label>
            <asp:TextBox ID="txtactid" style="width:575px; height:300px;" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div style="margin-bottom: 7px; margin-left: 152px; margin-top: 10px">
            <asp:Button Text="Export Report" runat="server" ID="btnexcel" OnClick="btnexcel_Click" CssClass="button"
                ValidationGroup="EventValidationGroup" />
        </div>

    </form>
</body>

     <script type="text/javascript">
        
         function initializeCombobox() {
             $("#<%= drpUserlst.ClientID %>").combobox();
         }       
    </script>

</html>
