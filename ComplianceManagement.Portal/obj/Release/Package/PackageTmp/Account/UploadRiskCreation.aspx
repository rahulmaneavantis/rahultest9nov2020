﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="UploadRiskCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadRiskCreation" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }

        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
    </style>

    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    
        $(document).ready(function () {         
           // setactivemenu('Import / Export Utility');
            fhead('Standard Audit Checklist');
        });
    </script>

       <script type="text/javascript">
 
           function showProgress() {              
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
    
    <script language="javascript" type="text/javascript">
        function OnTreeClick(evt) {            
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }

    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }

    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;

        if (parentNodeTable) {
            var checkUncheckSwitch;

            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any(one or more) child not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
            {
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }

</script> 

    <script type="text/javascript">
        $(function () {            
        $("[id*=tvFilterLocation] input[type=checkbox]").bind("click", function () {
            var table = $(this).closest("table");
            if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                //Is Parent CheckBox
                var childDiv = table.next();
                var isChecked = $(this).is(":checked");
                $("input[type=checkbox]", childDiv).each(function () {
                    if (isChecked) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                //Is Child CheckBox
                var parentDIV = $(this).closest("DIV");
                if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                    $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                } else {
                    $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                }
            }
        });
    })
 
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upComplianceDetails">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>


    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel"> 
                             <header class="panel-heading tab-bg-primary ">

                                 <ul id="rblRole" class="nav nav-tabs">                                                                                                                                           
                                    <li class="active" id="liRCM" runat="server">
                                        <asp:LinkButton ID="lnkRCM"  PostBackUrl="../Account/UploadRiskCreation.aspx"   runat="server">Step 1 - Risk Control Matrix</asp:LinkButton>                                           
                                    </li>                                                                                         
                                    <li class="" id="liAuditchecklist" runat="server">
                                        <asp:LinkButton ID="lnkAuditchecklist" PostBackUrl="../Account/UploadAdditionalRiskCreation.aspx" runat="server">Step 2 - Audit Steps</asp:LinkButton>                                           
                                    </li>                                                                                                                                                                                                    
                                 </ul>                                                                         
                                </header> 
                            
                             <div class="clearfix"></div>
                             
                       <%-- <div style="margin-bottom: 4px; width: auto">--%>
                             <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>

                      <%--  <div style="margin-bottom: 4px; width: auto">--%>
                     <%--<div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup1" />
                            <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                <asp:Label ID="lblMessage1" runat="server" Text=""></asp:Label>
                                <asp:Label ID="LblErormessage1" runat="server" Text="" ForeColor="red"></asp:Label>
                                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                    ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                            </div>
                        </div>  --%>                                             
                           
                            
                           <div class="clearfix"></div> 

                           <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">  
                                 <ul id="rblRole1" class="nav nav-tabs">                                                                                          
                                        <li class="active" id="liNotDone" runat="server">                                                
                                            <asp:LinkButton ID="Tab1" OnClick="Tab1_Click1" class="active"  runat="server">Import Utility</asp:LinkButton>
                                        </li>                                              
                                        <li class="" id="liSubmitted" runat="server">                                                     
                                            <asp:LinkButton ID="Tab2" OnClick="Tab2_Click1" class=""  runat="server">Export Utility</asp:LinkButton>
                                        </li>                                                                                                                                                                                                      
                                   </ul>
                            </div>
                            <div class="clearfix"></div> 
                            <div class="clearfix"></div> 
                            <div class="tab-content">                                 
                                   <div runat="server" id="performerdocuments"  class="tab-pane active">
                                    <div style="width: 100%; float: left; margin-top: 1em;margin-bottom: 15px">
                                         <div class="col-md-2 colpadding0" style="margin-top: 5px; color:#333; width:15%">
                                           <asp:RadioButton ID="rdoRCMUpload" runat="server" AutoPostBack="True" Text="Risk Control Matrix" OnCheckedChanged="rdoCompliance_CheckedChanged" GroupName="uploadContentGroup" />
                                         </div>

                                         <div class="col-md-2 colpadding0" style="margin-top: 5px; color:#333;width:15%">
                                             <asp:RadioButton ID="rdoSubProcess" runat="server" AutoPostBack="True" Text="Sub Process" OnCheckedChanged="rdoSubProcess_CheckedChanged" GroupName="uploadContentGroup" />
                                          </div>

                                        <div class="col-md-2 colpadding0" style="margin-top: 5px; color:#333;width:20%">
                                             <asp:RadioButton ID="rdoRCMUpdate" runat="server" AutoPostBack="True" Text="Update Risk Control Matrix" OnCheckedChanged="rdoSubProcess_CheckedChanged" GroupName="uploadContentGroup" />
                                          </div> 

                                        <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px; color:#333;width:50%">
                                            <div class="col-md-3 colpadding0 entrycount" style="width:50%">
                                                <asp:FileUpload ID="MasterFileUpload" runat="server"/>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                            </div>
                                             <div class="col-md-3 colpadding0 entrycount" style="text-align: right;width:50%">
                                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                                    Style="margin-left: 75px;" class="btn btn-primary" OnClick="btnUploadFile_Click" OnClientClick="showProgress()" />
                                            </div>
                                        </div>                                         
                                    </div>
                                     
                                       <div class="clearfix"></div>

                                        <div style="width: 100%; float:left;margin-right: 2%;" runat="server" id="Divbranchlist">
                                            <div style="width: 50%; float: left; margin-bottom: 15px">
                                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" PlaceHolder="Select Applicable Location" autocomplete="off"
                                                    Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 325px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                CssClass="txtbox" />  <%-- --%>
                                                    
                                                <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                    <asp:TreeView runat="server"  ID="tvFilterLocation" ShowCheckBoxes="All" Width="325px" NodeStyle-ForeColor="#8e8e93" 
                                                    Style="overflow: auto; height:250px; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; 
                                                     background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" onclick="OnTreeClick(event)">
                                                    </asp:TreeView> <%-- onclick="OnTreeClick(event)"--%>
                                                </div> 
                                            </div>

                                            <div style="width: 50%; float: right;">
                                               
                                            </div>
                                          </div>  
                                       
                                        <div style="width: 100%; text-align:right;margin-right: 2%;" runat="server">
                                          <asp:LinkButton ID="LinkButton_sampleForm"  class="newlink" Font-Underline="True" OnClick="sampleForm_Click"  runat="server">Sample Format - Risk Control Matrix</asp:LinkButton><br />
                                            <u> <a href="../../AuditSampleDocument/SubProcess_Sample.xlsx">Sample Format - SubProcess</a></u>
                                            </div>                                    
                                       </div>
                                   </div>
                                   
                            
                                  <%--//////////////////////////////////////////////////Tab 2////////////////////////////////////////////////////////////--%>

                                   <div runat="server" id="reviewerdocuments" class="tab-pane">
                                       <div class="clearfix"></div>
                                       <div class="clearfix" style="height:15px"></div>

                                       <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount">
                                        <div class="col-md-3 colpadding0" >
                                            <p style="color: #999; ">Show </p>
                                        </div>
                                           <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                            <asp:ListItem Text="5"/>
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" Selected="True"/>
                                            <asp:ListItem Text="100" />
                                        </asp:DropDownList>                                       
                                    </div>
                                    <div class="col-md-3 colpadding0" style="margin-top: 5px; color:#999;" runat="server" visible="false">  
                                        <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged" Enabled="false">
                                        <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                        <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                        </asp:RadioButtonList>    
                                     </div>

                                     <div class="col-md-6 colpadding0" style="margin-top: 5px; color:#999;">
                                    
                                     </div>

                                 <div class="col-md-3 colpadding0" style="text-align:right;color:#999; float:right;padding-right: 2%;">
                                      <asp:Button ID="lbtnExportExcel" Text="Export to Excel" class="btn btn-primary" CausesValidation="false" 
                                          OnClick="lbtnExportExcel_Click" runat="server" />                                     
                                     </div>

                                    <div style="text-align:right">                                       
                                    </div>

                                    <div style="float:right; margin-top:5px;">                                                               
                                                                        
                                    </div>
                              </div>

                              <div class="clearfix"></div>
                              
                             <div class="col-md-12 colpadding0">
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity"  class="form-control m-bot15"  Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" Style="background:none;" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" class="form-control m-bot15" Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged" DataPlaceHolder="Sub Unit 1">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2"  class="form-control m-bot15"  Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged" DataPlaceHolder="Sub Unit 2">
                                    </asp:DropDownListChosen>
                                </div>
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15" Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged" DataPlaceHolder="Sub Unit 3">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>  

                              <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0">                                                                                                                                                                                                                                                                                  
                                <div class="col-md-3 colpadding0" style="margin-top:5px;">                                 
                                    <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" DataPlaceHolder="Location"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" Width="90%" Height="32px" class="form-control m-bot15">
                                    </asp:DropDownListChosen>                          
                                </div>                                 
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                               
                                    <asp:DropDownListChosen ID="ddlFilterProcess" runat="server" AutoPostBack="true"  DataPlaceHolder="Process" class="form-control m-bot15"   Width="90%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                </div>
                                <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                  
                                    <asp:DropDownListChosen ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" DataPlaceHolder="Sub Process" class="form-control m-bot15"  Width="90%" Height="32px"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                                </div>   
                                 <div class="col-md-3 colpadding0" style="margin-top: 5px;">                                
                                   <asp:TextBox runat="server" ID="txtFilter" PlaceHolder="Type to Search" AutoPostBack="true" OnTextChanged="txtFilter_TextChanged" CssClass="form-control" Style="margin-bottom: 10px; width: 90%;" />                                 
                                </div>                                                                                                                                                                 
                           </div>

                            <div class="clearfix"></div>
                            <div class="clearfix"></div>

                            <%--//////////////////////////////////////////Gridview/////////////////////////--%>

                             <div style="margin-bottom: 4px">  
                                <div class="col-lg-12 col-md-12">
                                    &nbsp;        
                                        <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" GridLines="none" CssClass="table" 
                                            ShowHeaderWhenEmpty="true" BorderWidth="0px" AllowPaging="True"
                                            PageSize="50" DataKeyNames="RiskActivityID" OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging">                        
                                        <Columns>                      
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Task Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltaskidItemTemplate" runat="server" Text='<%# Eval("RiskID") %>'></asp:Label>
                                                    <asp:Label ID="lblRiskActivityIDItemTemplate" runat="server" Text='<%# Eval("RiskActivityID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField HeaderText="Branch">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                     <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                    </div>
                                                         </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Control#">
                                                <ItemTemplate>
                                                     <asp:Label ID="lblControlNo" runat="server" Text='<%# Eval("ControlNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                                                                              
                                            <asp:TemplateField HeaderText="Risk Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblRiskActivityDescriptionItemTemplate" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                                    </div>                                
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Control Objective">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="lblControlObjectiveItemTemplate" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ControlObjective") %>' ToolTip='<%# Eval("ControlObjective") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                               
                                          
                                            <asp:TemplateField HeaderText="Control Description" ItemStyle-Width="200px">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="txtExistingControlDescriptionItemTemplate" data-toggle="tooltip"  data-placement="top" runat="server" Text='<%# Eval("ControlDescription") %>' ToolTip='<%# Eval("ControlDescription") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                      
                                            
                                        </Columns>

                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" /> 
                                          <HeaderStyle BackColor="#ECF0F1" />                                        
                                        <PagerStyle HorizontalAlign="Right" />
                                             <PagerSettings Visible="false" />   
                                        <PagerTemplate>
                                           <%-- <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>--%>
                                        </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                          <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                                  </div>
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                        </div>                                   
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0" style="margin-left: 500px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="margin-left: 500px;">
                                        <p>Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                      </div>

                                       </div>     
                            
                                               
                            </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            <asp:PostBackTrigger ControlID="LinkButton_sampleForm" />

         <%--   <asp:AsyncPostBackTrigger ControlID="btnUploadFile" />
            <asp:AsyncPostBackTrigger ControlID="lbtnExportExcel" />--%>
        </Triggers>
    </asp:UpdatePanel>
    
   
</asp:Content>
