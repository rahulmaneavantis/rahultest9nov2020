﻿<%@ Page Title="Send NewsLetter" Language="C#" ValidateRequest="false" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="SendNewsLetter.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.SendNewsLetter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <%--<script type="text/javascript" src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>--%>
    <script type="text/javascript">
        tinymce.init(
            {
                mode: "specific_textareas",
                editor_selector: "myTextEditor",
                //selector: '"#<%= txtMessagebody.ClientID %>"',
                width: '95%',
                height: 400,
                toolbar: ' undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect | fontsizeselect',
                //toolbar:'fontselect| fontsizeselect| link image',
                 font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                //font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
                fontsize_formats: '8pt 10pt 11pt 12pt 13pt 14pt 15pt 18pt 24pt 36pt'
               
            });

        <%--function initializeCombobox() {             
             $("#<%= ddlType.ClientID %>").combobox();           
        }

        $(document).ready(function () {
            initializeCombobox();
        });--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <%--   <asp:UpdatePanel ID="upMassMailList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>

    <div style="margin-top: 40px; margin-left: 10px;">
        <div style="margin-bottom: 4px">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                ValidationGroup="MassEmailingValidationGroup" />
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="MassEmailingValidationGroup" Display="None" />
        </div>

        <div style="width: 95%; margin-bottom: 7px; text-align: right;">
            <asp:DropDownList runat="server" ID="ddlType" Style="padding: 0px; margin: 0px; height: 22px; width: 175px !important;"
                CssClass="txtbox">
                <asp:ListItem Text="Both" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Compliance Product User" Value="1"></asp:ListItem>
                <asp:ListItem Text="Zoho CRM" Value="2"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button Text="Get Email-IDs" runat="server" ID="btnGetMailIDs" OnClick="btnGetMailIds_Click"
                CssClass="button" ToolTip="This will populate Leads Email from Zoho CRM and Compliance Product Users." />
            <asp:Button Text="Refresh Email List" runat="server" ID="btnRefreshMail" Style="width: 160px;"
                OnClick="btnRefreshMail_Click" CssClass="button" ToolTip="By Clicking, it will fetch Leads Email from Zoho CRM and Compliance Product Users." />
        </div> 
        
        <div style="margin-bottom: 7px;">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                From (Name)</label>
            <asp:TextBox runat="server" ID="txtFromName" Style="width: 80%;" Height="25px" ToolTip="This is the name appended to the from email field.(i.e.- Your name or company name" />
        </div>        

        <div style="margin-bottom: 7px;">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                To</label>
            <asp:TextBox runat="server" ID="tbxTo" Style="width: 80%;" Height="25px" ToolTip="To" DataPlaceHolder="Please write comma(,) seperate emails" />
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="To can not be empty." ControlToValidate="tbxTo"
                runat="server" ValidationGroup="MassEmailingValidationGroup" Display="None" />
        </div>

        <div style="margin-bottom: 7px;">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                Cc</label>
            <asp:TextBox runat="server" ID="tbxCC" Style="width: 80%;" MaxLength="100" Height="25px" ToolTip="Cc" DataPlaceHolder="Please write comma(,) seperate emails"/>
        </div>        

        <div style="margin-bottom: 7px; width: 100%; float: left">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                Bcc</label>
            <asp:TextBox runat="server" AutoPostBack="true" ID="tbxBcc" Style="width: 80%;" OnTextChanged="txtFilter_TextChanged" TextMode="multiline" Height="150px" ToolTip="Bcc" DataPlaceHolder="Please write comma(,) seperate emails"/>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="bcc can not be empty." ControlToValidate="tbxBcc"
                runat="server" ValidationGroup="MassEmailingValidationGroup" Display="None" />
        </div>

        <div style="margin-bottom: 7px; width: 100%; float: left">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">&nbsp;</label>
            <asp:Label runat="server" ID="lblEmailCount" Style="width: 80%;" Font-Bold="true" />
        </div>

        <div style="margin-bottom: 7px">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                Subject</label>
            <asp:TextBox runat="server" ID="tbxSubject" Style="width: 80%;" Height="25px" MaxLength="70" ToolTip="Subject" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Subject can not be empty." ControlToValidate="tbxSubject"
                runat="server" ValidationGroup="MassEmailingValidationGroup" Display="None" />
        </div>
    </div>

    <div style="margin-bottom: 7px; width: 100%; float: left">
        <div style="width: 16%; float: left;">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
            <label style="width: 90%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                Message
            </label>
        </div>
        <div style="width: 83%; float: left;">
            <asp:TextBox ID="txtMessagebody" CssClass="myTextEditor" TextMode="multiline" runat="server" />
        </div>
    </div>

    <div class="clearfix"></div>

    <div style="margin-bottom: 7px; margin-top: 20px; width: 100%; float: left">
        <div style="width: 15%; float: left;">
            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
            <label style="display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">
                Attachment(s)</label>
        </div>
        <div style="width: 85%; float: right;">
            <asp:FileUpload ID="fuAttachments" AllowMultiple="true" Style="width: 80%;" runat="server" />
        </div>
    </div>
    <br />
    <br />
    <div style="margin-bottom: 7px; width: 100%; text-align: center;">
        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="button" OnClick="btnSend_Click"
            ValidationGroup="MassEmailingValidationGroup" />
        <asp:Button Text="Clear" runat="server" ID="btnCancel" OnClick="btnClear_Click" CssClass="button" />
    </div>

</asp:Content>
