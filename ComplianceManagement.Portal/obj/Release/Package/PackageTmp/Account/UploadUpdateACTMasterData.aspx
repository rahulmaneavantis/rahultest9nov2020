﻿<%@ Page Title="Upload Master Data" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadUpdateACTMasterData.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadUpdateACTMasterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Download Excel Format" BorderStyle="None" ID="btnExcelFormat" CssClass="Initial" runat="server"
                    OnClick="btnExcelFormat_Click" />
                <div style="width: 100%; float: left; margin-bottom: 15px">
                    <div style="margin-bottom: 4px; width: auto">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                            <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </div>
                    </div>

                    <table align="left" cellpadding="2" style="margin-left: 55px;">
                        <tr>
                            <td>
                                <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                            </td>
                            <td>
                                <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; padding-left: 8px;">
                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                    Style="margin-left: 75px;"
                                    OnClick="btnUploadFile_Click" />
                            </td>
                            <td style="text-align: left; padding-left: 8px;">                                
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
