﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class LogoutSuccessfully : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
           
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    //LoginsRepository login = new LoginsRepository();
                    //login.UserId = AuthenticationHelper.UserID;
                    //login.SessionId = System.Web.HttpContext.Current.Session.SessionID; ;
                    //login.LoggedIn = false;
                    //login.IS_Start_Stop = "LT";
                    //UserManagement.SessionLog(login);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                FormsAuthentication.SignOut();
                Session.Abandon();              
                FormsAuthentication.RedirectToLoginPage();    

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }


    }
}