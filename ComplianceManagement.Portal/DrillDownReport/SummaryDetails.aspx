﻿<%@ Page Title="Summary Details" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="SummaryDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DrillDownReport.SummaryDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            <div>
              <asp:Button ID="btnBack" OnClick="btnBack_OnClick" Text="Back" style="margin:10px;" runat="server"/>
            </div>
            <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" 
            CellPadding="4" ForeColor="Black" AllowPaging="false" PageSize="12" Width="100%" 
            Font-Size="12px" DataKeyNames="ComplianceInstanceID" >
            <Columns>
                <asp:BoundField DataField="ShortDescription" HeaderText="Compliance"  ItemStyle-Height="20px"  HeaderStyle-Height="20px" SortExpression="ShortDescription"/>
                <asp:BoundField DataField="Branch" HeaderText="Location" SortExpression="Branch"/>
                 <asp:TemplateField HeaderText="Start Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn" >
                    <ItemTemplate>
                        <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ForMonth" HeaderText="ForMonth" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth"/>
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"/>
                <asp:BoundField DataField="RiskCategory" HeaderText="RiskCategory" SortExpression="RiskCategory"/>
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <pagersettings position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
