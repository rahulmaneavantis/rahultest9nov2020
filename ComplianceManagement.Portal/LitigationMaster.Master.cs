﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class LitigationMaster : System.Web.UI.MasterPage
    {
        protected string user_Roles;
        protected List<Int32> roles;

        protected string LastLoginDate;
        protected string CustomerName;
        protected int customerid;
        protected int userid;
        protected string CompanyAdmin = "";
        protected int hideshow_Pageval;
        protected List<Int32> PageID;
        public List<tbl_PageAuthorizationMaster> authRecords;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                user_Roles = AuthenticationHelper.Role;
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                userid = AuthenticationHelper.UserID;
                PageID = CaseManagement.GetMasterPageDtls(userid);
                if (!IsPostBack)
                {
                    String key = "Authenticate"+ AuthenticationHelper.UserID;
                    userid = AuthenticationHelper.UserID;
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        authRecords = (List<tbl_PageAuthorizationMaster>) HttpContext.Current.Cache[key];
                        if (HttpContext.Current.Cache[key] == null || authRecords.Count() == 0)
                        {
                            authRecords = (from row in entities.tbl_PageAuthorizationMaster
                                       where row.isActive == true && row.UserID == userid
                                       select row).ToList();

                        HttpContext.Current.Cache.Insert(key, authRecords, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero); // add it to cache
                    }
                        else
                            authRecords = (List<tbl_PageAuthorizationMaster>) HttpContext.Current.Cache[key];
                }
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                    Page.Header.DataBind();

                    if (LoggedUser != null)
                    {
                        if (LoggedUser.ImagePath != null)
                        {
                            ProfilePic.Src = LoggedUser.ImagePath;
                            ProfilePicTop.Src = LoggedUser.ImagePath;
                            // ProfilePicSide.Src = LoggedUser.ImagePath;
                        }
                        else
                        {
                            ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                            ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                            //  ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                        }
                    }

                    if (Session["LastLoginTime"] != null)
                    {
                        LastLoginDate = Session["LastLoginTime"].ToString();
                    }
                    if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        //if (AuthenticationHelper.UserID != -1)
                        //CustomerName = CustomerManagement.GetByID(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)).Name;
                    }

                    // Bind Sitemap According to User Role
                    if (Session["ChangePassword"] != null)
                    {
                        if (Convert.ToBoolean(Session["ChangePassword"]) != true)
                        {
                            roles = NoticeManagement.GetLitigationAssignedRoles(AuthenticationHelper.UserID);

                            if (AuthenticationHelper.Role.Equals("CADMN"))
                            {
                                CompanyAdmin = "CADMN";
                            }
                            if (AuthenticationHelper.Role.Equals("SADMN"))
                            {
                                CMPServicesSiteMap.SiteMapProvider = "LitigationMangProvider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN"))//ICFR
                            {
                                CMPServicesSiteMap.SiteMapProvider = "LitigationMangProvider";

                                //var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);
                                //roles = a;
                            }
                            else if (AuthenticationHelper.Role.Equals("MGMT"))//ICFR
                            {
                                CMPServicesSiteMap.SiteMapProvider = "LitigationMangProvider";

                                //var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);
                                //roles = a;
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            CMPServicesSiteMap.SiteMapProvider = null;
                            CMPMenuBar.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        [WebMethod]
        public static void AbandonSession()
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}