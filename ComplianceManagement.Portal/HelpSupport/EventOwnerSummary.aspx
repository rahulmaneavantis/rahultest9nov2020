﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <%--<link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>

    <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;

      
    }
    .MainContainer {
     
      /*height: 1000px;*/
      border:groove;
      border-color:#f1f3f4;
      
    }
       .img1{
        border:ridge;
        border-color:lightgrey;
    }
   
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
      <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1170px;
 

  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>
  
      <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		color:black;
		background-color: white;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: black;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>    
</head>
<body>
    <form id="form1" runat="server">
         <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                           <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul><br/><br/>
                
           </div>

      <div class="col-sm-9 MainContainer">
          <h3>Event</h3>
          <p style="font-size:14px;">You can view, activate and close Event through this section. This section features, Assigned, Activated and Closed Events.</p>
          <br />
             <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
             

                 <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b>Assigned Event</b>
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
                              
            <b>Question-</b> How can I use this section? <br />
            <b>Answer-</b> In following manner you can use this section,
                  <ul >
                    <li style="list-style-type:decimal">You can view and activate event through this section.</li>
                    <li style="list-style-type:decimal">To view Assigned Event, click on Assigned Event section, you will be redirected to page holding Event or list of Event. </li><br /><br />
                      <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_2_Assigned%20Event_1_Screenshot.png" /><br /><br />
                    <li style="list-style-type:decimal">On this page you can search for particular Event with the help of Filter. </li>
                    <li style="list-style-type:decimal">Also, you can select Secretarial/Non-Secretarial from dropdown to view Events following particular type. And similarly, you can select Entity/Sub-Entity/Location for Events following under selected Entity/Sub-Entity/Location.</li><br /><br />
                      <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_3_Assigned%20Event_2_Screenshot.png" /><br /><br />
                    <li style="list-style-type:decimal">To Activate Event, follow below process, 
                        <ul >
                            <li style="list-style-type:lower-roman">Enter Nature of event and Activate date. </li><br /><br />
                            <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_4_Assigned%20Event_3_Screenshot.png" /><br /><br />
                            <li style="list-style-type:lower-roman">Click on Activate icon or Save button.</li><br /><br />
                            <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_5_Assigned%20Event_4_Screenshot.png" /><br /><br />
                            <li style="list-style-type:lower-roman">To activate multiple event, tick the given checkbox, enter Nature of event and Activate date. And click on Save button. </li><br /><br />
                            <img class="img1" style="width: 700px;margin-left:30px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_6_Assigned%20Event_5_Screenshot.png" /><br /><br />
                            <li style="list-style-type:lower-roman">For all the enter Nature of event and Activate date at one go for Events, enter the values in textbox given above table. And then click on Save button to active.</li><br /><br />
                            <img class="img1" style="width: 700px;margin-left:30px;height: 300px" src="../ImagesHelpCenter/Event%20Owner_7_Assigned%20Event_6_Screenshot.png" /><br /><br />
                          </ul>
					 </div>
                </div>
             </div>

                <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
						<i class="short-full glyphicon glyphicon-plus"></i>
						<b>Activated Event</b>
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
                      <p><b>Question-</b> How can I use this section? <br />
                 <b>Answer-</b> In following manner you can use this section, </p>
               <ul >
                   <li style="list-style-type:decimal">To access activated Event, click on Activate Events section, you will be redirected to page that holds activated Event or list of Events.</li><br /><br />
                   <img class="img1" style="width: 650px;margin-left:10px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_8_Activated%20Events_1_Screenshot.png" /><br /><br />
                   <li style="list-style-type:decimal">On this page you can search for particular Event with the help of Filter.</li>
                   <li style="list-style-type:decimal">Also, you can select Secretarial/Non-Secretarial from dropdown to view Events following particular type. And similarly, you can select Entity/Sub-Entity/Location for Events following under selected Entity/Sub-Entity/Location.  </li><br /><br />
                   <img class="img1" style="width: 650px;margin-left:10px; height: 300px"style="width: 1000px; height: 500px" src="../ImagesHelpCenter/Event%20Owner_9_Activated%20Events_2_Screenshot.png" /><br /><br />
                   <li style="list-style-type:decimal">To view the Event details, click on View icon. And you will be redirected to page that holds details related to Event. </li><br /><br />
                   <img class="img1" style="width: 650px;margin-left:10px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_10_Activated%20Events_3_Screenshot.png" /><br /><br />
                   <li style="list-style-type:decimal">Click on Edit icon to update compliance date, add Internal compliance in Statutory Event and mark any compliance Not applicable.  Follow the below process,
                       <ul >
                           <li style="list-style-type:lower-alpha">Update 
                               <ul >
                                   <li style="list-style-type:lower-roman">You can update the compliance performing date only when it has not passed the due date. </li>
                                   <li style="list-style-type:lower-roman"> To update click on textbox, select the date and click on Update button. </li><br /><br />
                                   <img class="img1" style="width: 650px;margin-left:5px; height: 300px" src="../ImagesHelpCenter/Event%20Owner_11_Activated%20Events_4_Screenshot.png" /><br /><br />
                               </ul>
                               
                                   <li style="list-style-type:lower-alpha"> Add Internal compliance with Statutory event   </li>
                               <ul >
                                   <li style="list-style-type:lower-roman"> To add internal compliance with statutory event, click on plus icon</li><br /><br />
                                   <img class="img1" style="width: 650px;margin-left:5px; height: 350px" src="../ImagesHelpCenter/Event%20Owner_12_Activated%20Events_5_Screenshot.png" /><br /><br />
                                   <li style="list-style-type:lower-roman">A pop-up will appear, here select internal compliance, enter days and select OneTime/Recurring. </li><br /><br />
                                   <img class="img1" style="width: 650px;margin-left:5px; height: 350px" src="../ImagesHelpCenter/Event%20Owner_13_Activated%20Events_6_Screenshot.png" /><br /><br />
                                    <li style="list-style-type:lower-roman">Not Applicable- To mark any compliance not applicable, click on NA button. To reconfirm your action a pop-up will appear, click on Ok button to reconfirm. The compliance that are not performed can only be marked NA.</li><br /><br />
                                   <img class="img1" style="width: 650px;margin-left:5px; height: 340px" src="../ImagesHelpCenter/Event%20Owner_14_Activated%20Events_7_Screenshot.png" /><br /><br />
                                  </ul> 
                           </li>
                       </ul>
              
					 </div>
                </div>
             </div>

                <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
						<i class="short-full glyphicon glyphicon-plus"></i>
					<b>Closed Event</b>
					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body">
                      <p><b>Question-</b> How can I use this section? <br />
                          <b>Answer-</b> In following manner you can use this section,</p>
                     <ul >
                         <li style="list-style-type:decimal">You can view closed event in this section, click on Closed Event section and you will be redirected to page that holds closed Event or list of events.   </li><br /><br />
                         <img class="img1" style="width: 650px;margin-left:30px; height: 350px" src="../ImagesHelpCenter/Event%20Owner_15_Closed%20Events_1_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">On this page you can search for particular Event with the help of Filter. </li>
                         <li style="list-style-type:decimal">Also, you can select Secretarial/Non-Secretarial from dropdown to view Events following particular type. And similarly, you can select Entity/Sub-Entity/Location for Events following under selected Entity/Sub-Entity/Location.</li><br /><br />
                         <img class="img1" style="width: 700px;margin-left:30px; height: 400px" src="../ImagesHelpCenter/Event%20Owner_16_Closed%20Events_2_Screenshot.png" /><br /><br />
                         <li style="list-style-type:decimal">To view details, click on View icon. </li>
                        
                     </ul>
					 </div>
                </div>
             </div>
            </div>
       </div>
    </div>
  </div>
        
    </form>
       

<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>
</html>
