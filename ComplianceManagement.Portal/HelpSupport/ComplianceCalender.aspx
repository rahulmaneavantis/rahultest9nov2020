﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  

   <%-- <link href="NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="Newjs/Helpjs/bootstrap.min.js"></script>--%>
      <link href="/NewCSS/helpcss/all.css" rel="stylesheet" />
    <link href="/NewCSS/helpcss/bootstrap.min.css" rel="stylesheet" />
    <script src="/Newjs/Helpjs/jquery-1.11.3.min.js"></script>
    <script src="/Newjs/Helpjs/bootstrap.min.js"></script>
      <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px;
                  /*margin:125px;*/
    }
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color:#F5F2F4;
      height: 1000px;
      
    }
     .MainContainer {
     
      height: 1000px;
      border:groove;
      border-color:#f1f3f4;
      
    }
      .img1{
        border:ridge;
        border-color:lightgrey;
    }
   
   
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        *height: auto;
        *padding: 15px;
      
      }
      .row.content {height: auto;} 
    }
  </style>
    
    <style>
       
 .container {
  /*padding: 16px;*/
  background-color: white;
  margin:10px;
  /*margin-right:50px;
  margin-top:50px;*/
  height:1000px;
  width:1170px;
 
  
}
 body {
  font-family: Arial, Helvetica, sans-serif;
  /*background-color:#1fd9e1;*/
}

* {
  box-sizing: border-box;
}

    </style>


  <style>

	.panel-group .panel {
		border-radius: 5px;
		border-color: #EEEEEE;
        padding:0;
	}

	.panel-default > .panel-heading {
		/*color: #fff;*/
		/*background-color: #346767;*/
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}

	.short-full {
		float: right;
		color: #fff;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border: solid 1px #EEEEEE;
        /*background-color: #B7FFB7;*/
        
	}

</style>   
</head>
<body>
    <form id="form1" runat="server">
   <div class="container">
      
      <div class="row content"> 

        <div class="col-sm-3 sidenav">
                     
                  <ul class="nav nav-pills nav-stacked"  >
                             <li style="font-size:18px"><a href="../NewHelp.aspx" style="color:#333"><b>Standard Dashboard</b></a>
                          <li style="font-size:14px" ><a href="../NewHelp.aspx" style="color:#333" ><i class='fas fa-desktop'></i>&nbsp;&nbsp;&nbsp;<b>My Dashboard</b></a>
                            <ul style="line-height:25px;font-size:13px">
                                <li ><a href="../HelpSupport/PerformerSummary.aspx" style="color:#333">Performer Summary</a></li>
                                <li><a href="../HelpSupport/PerformerTaskSummary.aspx" style="color:#333">Performer Task Summary</a></li>
                                <li><a href="../HelpSupport/PerformerLocation.aspx" style="color:#333">Performer Location</a></li>
                                <li> <a href="../HelpSupport/ReviewerSummary.aspx" style="color:#333">Reviewer Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerTaskSummary.aspx" style="color:#333">Reviewer Task Summary</a></li>
                                <li> <a href="../HelpSupport/ReviewerLocation.aspx" style="color:#333">Reviewer Location</a></li>
                                <li><a href="../HelpSupport/EventOwnerSummary.aspx" style="color:#333">Event Owner Summary</a></li>
                                <li><a href="../HelpSupport/ComplianceCalender.aspx" style="color:#333">My Compliance Calender</a></li>
                                <li><a href="../HelpSupport/DailyUpdates.aspx" style="color:#333">Daily Updates</a></li>
                                <li><a href="../HelpSupport/NewsLetter.aspx" style="color:#333">Newsletter</a></li>
                            </ul>
                        </li>
                        <li style="font-size:14px" ><a href="../HelpSupport/Compliance.aspx" style="color:#333;"><i class='fas fa-briefcase'></i>&nbsp;&nbsp;&nbsp;<b>My Workspace</b></a>
                            <ul style="line-height:25px;font-size:13px">
                              <li><a href="../HelpSupport/Compliance.aspx" style="color:#333">Compliance</a></li>
                              <li><a href="../HelpSupport/License.aspx" style="color:#333">License</a></li>
                            </ul>
                        </li>
                  
                  
                        <li style="font-size:14px" ><a href="../HelpSupport/MyReports.aspx" style="color:#333" > <i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Reports</b> </a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/MyDocuments.aspx" style="color:#333"><i class='far fa-file-alt'></i>&nbsp;&nbsp;&nbsp;<b>My Documents</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Reminders.aspx" style="color:#333"><i class='far fa-clock'></i>&nbsp;&nbsp;&nbsp;<b>My Reminders</b></a></li>
                        <li style="font-size:14px"><a href="../HelpSupport/Escalation.aspx" style="color:#333"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;<b>My Escalation</b></a></li>

                     </ul><br/><br/>
           </div>

          <div class="col-sm-9 MainContainer">
    
               <h3>My Compliance Calendar</h3>
    



              <p style="font-size:14px;">All Statutory and Internal compliances to be performed in current month will be shown under My Compliance Calendar section. <br />
                 This section depicts the schedule of the month. And month wise you can view the schedule of current calendar/financial year. <br />
                 Also, this section allows you to perform compliance directly. </p>
               <br /><br />
                              <%--  <img class="img1" style="width: 750px;margin-left:40px;height: 350px" src="../ImagesHelpCenter/Performer_My%20Compliance%20Calendar_Screenshot.png" />--%>
              <img src="../perfreviewerUpdatedscreens/Performer_My%20Compliance%20Calendar_Screenshot.png" style="width: 750px;margin-left:40px;height: 350px" />
              <br /><br /><br />
                              <b>Question -</b> How can I use this section? <br />   
                             <b>  Answer-</b> In following manner you can use this section,<br />
                                   <ul>
                                       <li style="list-style-type:decimal">By default, this section will show current month compliance to be performed. Each date of the calendar will showcase a number that
                                          specify the number of compliances to be performed on that date.</li>
                                       <li style="list-style-type:decimal">When you select the date, in right hand side compliance or list of compliance to be performed will be featured.</li>
                                       <li style="list-style-type:decimal">To perform the compliance, you can click on Action icon and perform. </li>
                                       <li style="list-style-type:decimal">
                                         Color code to understand the status, following are the color codes to help you recognize the status,
                                       <ul>
                                               <li style="list-style-type:circle">Green - All compliances completed within due date.</li>
                                               <li style="list-style-type:circle">Red - All overdue compliances.</li>
                                               <li style="list-style-type:circle">Blue - All upcoming compliances.</li>
                                               <li style="list-style-type:circle">Yellow - All compliances completed after due date.</li>
                                           </ul>
                                         
                                       </li>
                                       </ul>
                                       <li>To view compliance schedule month wise of current calendar/financial year click on side arrow featured above the calendar.</li>
                                  	</div>
					 </div>
                </div>
       
    </form>

    
<%--<script>

	function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".short-full")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>--%>
</body>
</html>
