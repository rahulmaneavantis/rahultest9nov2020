﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class Call_Exe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string locn = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];

                Process.Start(locn + "CacheService.exe");
                //Execute(ConfigurationManager.AppSettings["WORKINGDIRECTORY"].ToString());
            }
        }
        private void Execute(string sPath)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.LoadUserProfile = true;
            //proc.StartInfo.UserName = "administrador";
            //string pass = ".............";

           // System.Security.SecureString secret = new System.Security.SecureString();
           // foreach (char c in pass) secret.AppendChar(c);

           // proc.StartInfo.Password = secret;
            proc.StartInfo.WorkingDirectory = ConfigurationManager.AppSettings["WORKINGDIRECTORY"].ToString();
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.FileName = sPath;
            proc.Start();
            proc.WaitForExit();
            string result = proc.StandardOutput.ReadToEnd();
            Response.Write(result + " - " + proc.ExitCode);
            proc.Close();
        }
    }
}