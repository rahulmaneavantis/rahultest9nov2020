﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using AjaxControlToolkit;
using Common.Logging.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class RLCSVendorAuth : System.Web.UI.Page
    {
        string ipaddress = string.Empty;
        string Macaddress = string.Empty;
        protected string Approveruser_Roles;
        protected List<Int32> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    bool ValidUserFlag = false;
                    //string TL_Profile_ID_Decrypted = string.Empty;
                    string TL_User_ID_Decrypted = string.Empty;
                    //string TL_AuthKey_Decrypted = string.Empty;

                    //string TL_Profile_ID = Request.QueryString["ID"];
                    //string TL_AuthKey_Encrypted = Request.QueryString["Auth"];
                    string TL_User_ID = Request.QueryString["ID"];
                     
                    if (!String.IsNullOrEmpty(TL_User_ID))
                    {
                        string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                        //Comment before Live/Publish
                        //TL_User_ID = CryptographyHandler.encrypt(TL_User_ID, TLConnectKey);

                        TL_User_ID_Decrypted = CryptographyHandler.decrypt(TL_User_ID, TLConnectKey);

                        if (TL_User_ID_Decrypted.Equals("Error"))
                        {
                            string strURL = Page.Request.Url.AbsoluteUri;
                            if (!string.IsNullOrEmpty(strURL))
                            {
                                TL_User_ID = After(strURL, "ID=");
                                //int startIndex =   strURL.IndexOf("ID=");
                                //int endIndex = strURL.IndexOf("&Auth");

                                //TL_User_ID = strURL.Substring(startIndex + 3, (endIndex - startIndex) - 3);
                                TL_User_ID_Decrypted = CryptographyHandler.decrypt(TL_User_ID, TLConnectKey);
                            }
                        }

                        //TL_AuthKey_Decrypted = CryptographyHandler.decrypt(TL_AuthKey_Encrypted, TLConnectKey);

                        if (!string.IsNullOrEmpty(TL_User_ID_Decrypted.Trim()))
                        {
                            ValidUserFlag = Business.RLCS.RLCSManagement.CheckProfileID_Authentication(TL_User_ID_Decrypted);

                            if (ValidUserFlag)
                            {
                                ProcessAuthenticationInformation(TL_User_ID_Decrypted.Trim());
                            }                            
                        }
                    }

                    if (!ValidUserFlag)
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Authentication Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private string After(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        private void ProcessAuthenticationInformation(string TL_User_ID)
        {
            try
            {
                #region If No EmailID
                User user = null;
                // if (UserManagement.IsValidProfileID(TL_User_ID, out user))
                if (UserManagement.IsValidRLCSUserID(TL_User_ID, out user))                
                {
                    if (user != null)
                    {
                        try
                        {
                            Macaddress = Util.GetMACAddress();
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        if (user.IsActive)
                        {
                            if (user.VendorRoleID != null)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                Session["RLCS_userID"] = null;
                                //Session["RLCS_userID"] = user.UserID;

                                Session["RLCS_ProfileID"] = null;
                                //Session["RLCS_ProfileID"] = user.ProfileID;

                                MaintainLoginDetail objData = new MaintainLoginDetail()
                                {
                                    UserId = Convert.ToInt32(user.ID),
                                    Email = user.Email,
                                    CreatedOn = DateTime.Now,
                                    IPAddress = ipaddress,
                                    MACAddress = Macaddress,
                                    LoginFrom = "WV",
                                    //ProfileID = user.ID
                                };

                                UserManagement.Create(objData);

                                //Update LastLoginTime 
                                User userToUpdate = new User();
                                userToUpdate.ID = user.ID;
                                userToUpdate.LastLoginTime = DateTime.UtcNow;
                                userToUpdate.WrongAttempt = 0;
                                UserManagement.Update(userToUpdate);

                                Session["RM"] = false;
                                Session["EA"] = Util.CalculateAESHash(user.Email.Trim());
                                Session["MA"] = Util.CalculateAESHash(Macaddress.Trim());

                                RLCSProcessAuthenticationInformation(user, TL_User_ID);

                                Session["RM"] = null;
                                Session["EA"] = null;
                                Session["MA"] = null;
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Authentication Failed, You are not assigned to any Vendor Audit.";
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Your Account is Disabled.";
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void RLCSProcessAuthenticationInformation(User user, string profileID)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(Convert.ToInt32(user.VendorRoleID)).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkLabelApplicable = 0;
                int checkVerticalapplicable = 0;
                bool IsPaymentCustomer = false;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));                    
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }                    
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    //if (c.IsPayment != null)
                    //{
                    //    IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    //}
                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }
                }

                FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable), false);                
                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                //Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);
                Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                Response.Redirect("~/RLCSVendorAudit/RLCSVendorAuditDashboard.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAutontication_Click(object sender, EventArgs e)
        {

        }



    }
}