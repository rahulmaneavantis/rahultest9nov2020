﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ComplianceDocument
{
    public partial class ComplianceDocumentList : System.Web.UI.Page
    {
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;
        protected string InternalReviewername;
        protected string InternalPerformername;
        static int CustomerID;
        static int UserRoleID;
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        Session["ComType"] = "Internal";
                        ddlDocType.SelectedValue = "0";
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("-1"));
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("1"));
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("2"));
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("3"));
                    }
                    else
                    {
                        Session["ComType"] = "Statutory";
                    }
                    PerformerFlag = false;
                    ReviewerFlag = false;
                    ddlAct.Enabled = true;

                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            BindCategoriesInternalAll();
                            BindTypesInternalAll();
                            BindActListAllInternal();
                        }
                        else
                        {
                            BindCategoriesAll();
                            BindTypesAll();
                            BindActListAll();
                        }
                        BindComplianceSubTypeListAll();
                    }
                    else
                    {

                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            BindActListInternal();
                            BindTypesInternal();
                            BindCategoriesInternal();
                        }
                        else
                        {
                            BindTypes();
                            BindCategories();
                            BindActList();
                        }
                        BindComplianceSubTypeList();

                    }
                    BindEvents();
                    BindStatus();
                    BindLocationFilter();

                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    if (AuthenticationHelper.Role == "AUDT")
                    {
                        txtAdvStartDate.Text = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                        txtAdvEndDate.Text = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                    }
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;                       
                    }

                    btnDownload.Visible = false;                   
                    btnSearch_Click(sender, e);

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindComplianceSubTypeList()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();
                List<SP_GetComplianceSubTypeID_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserID(AuthenticationHelper.UserID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();
                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Sub type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceSubTypeListAll()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();
                List<SP_GetComplianceSubTypeIDAll_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserIDAll(CustomerID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();
                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Sub type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDownloadList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", string.Format("initializeDatePicker12(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", "initializeDatePicker12(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";
                if (ddlDocType.SelectedItem.Text == "Statutory" || ddlDocType.SelectedItem.Text == "Event Based")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlDocType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);            
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }        
        private void BindStatus()
        {
            try
            {
                foreach (DocumentFilterNewStatus r in Enum.GetValues(typeof(DocumentFilterNewStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(DocumentFilterNewStatus), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        protected void chkCompliancesHeader_CheckedChanged(object sender, EventArgs e)
        {
            try
             {
                if (PerformerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdComplianceDocument.HeaderRow.FindControl("chkCompliancesHeader");

                        foreach (GridViewRow row in grdComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkCompliances");

                            if (ChkBoxHeader.Checked)
                                chkCompliances.Checked = true;
                            else
                                chkCompliances.Checked = false;
                        }
                    }

                    if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdInternalComplianceDocument.HeaderRow.FindControl("chkInternalCompliancesHeader");

                        foreach (GridViewRow row in grdInternalComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkInternalCompliances");

                            if (ChkBoxHeader.Checked)
                                chkCompliances.Checked = true;
                            else
                                chkCompliances.Checked = false;
                        }
                    }


                }

                else if (ReviewerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdReviewerComplianceDocument.HeaderRow.FindControl("chkReviewerCompliancesHeader");

                        foreach (GridViewRow row in grdReviewerComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkReviewerCompliances");

                            if (ChkBoxHeader.Checked)
                                chkCompliances.Checked = true;
                            else
                                chkCompliances.Checked = false;
                        }
                    }

                    if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdReviewerInternalComplianceDocument.HeaderRow.FindControl("chkReviewerInternalCompliancesHeader");

                        foreach (GridViewRow row in grdReviewerInternalComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkReviewerInternalCompliances");

                            if (ChkBoxHeader.Checked)
                                chkCompliances.Checked = true;
                            else
                                chkCompliances.Checked = false;
                        }
                    }
                }

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkCompliances_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;

                if (PerformerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdComplianceDocument.HeaderRow.FindControl("chkCompliancesHeader");

                        foreach (GridViewRow row in grdComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkCompliances");

                            if (chkCompliances.Checked)
                                Count++;
                        }

                        if (Count == grdComplianceDocument.Rows.Count)
                            ChkBoxHeader.Checked = true;
                        else
                            ChkBoxHeader.Checked = false;
                    }

                    else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdInternalComplianceDocument.HeaderRow.FindControl("chkInternalCompliancesHeader");

                        foreach (GridViewRow row in grdInternalComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkInternalCompliances");

                            if (chkCompliances.Checked)
                                Count++;
                        }

                        if (Count == grdInternalComplianceDocument.Rows.Count)
                            ChkBoxHeader.Checked = true;
                        else
                            ChkBoxHeader.Checked = false;
                    }
                }

                else if (ReviewerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdReviewerComplianceDocument.HeaderRow.FindControl("chkReviewerCompliancesHeader");

                        foreach (GridViewRow row in grdReviewerComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkReviewerCompliances");

                            if (chkCompliances.Checked)
                                Count++;
                        }

                        if (Count == grdReviewerComplianceDocument.Rows.Count)
                            ChkBoxHeader.Checked = true;
                        else
                            ChkBoxHeader.Checked = false;
                    }

                    else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                    {
                        CheckBox ChkBoxHeader = (CheckBox) grdReviewerInternalComplianceDocument.HeaderRow.FindControl("chkReviewerInternalCompliancesHeader");

                        foreach (GridViewRow row in grdReviewerInternalComplianceDocument.Rows)
                        {
                            CheckBox chkCompliances = (CheckBox) row.FindControl("chkReviewerInternalCompliances");

                            if (chkCompliances.Checked)
                                Count++;
                        }

                        if (Count == grdReviewerInternalComplianceDocument.Rows.Count)
                            ChkBoxHeader.Checked = true;
                        else
                            ChkBoxHeader.Checked = false;
                    }
                }

                ShowSelectedRecords(sender, e);
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersion.DataBind();

                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();
                          
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);                            
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();                               
                                ComplianceFileData = ComplianceDocument;
                              
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                            
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetInternalComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetInternalComplianceDocuments(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersion.DataBind();

                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowInternalDownloadDocument();", true);                            
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetInternalFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                              
                                ComplianceFileData = ComplianceDocument;
                               

                                var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                    }
                }
                if (e.CommandName == "View")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetInternalComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetInternalComplianceDocuments(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersionView.DataBind();
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);

                                        lblMessageInternal.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);

                                }
                                break;
                            }                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersion.DataBind();

                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "none", "<script>$('#divDownloadDocument').modal('show');</script>", false);
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();                               
                                ComplianceFileData = ComplianceDocument;
                            

                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();

                            //Added by rahul on 15 JUNE 2018 reviewer document not shown by defaule
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                break;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);       
            }
        }

        protected void grdReviewerInternalComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetInternalComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetInternalComplianceDocuments(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersion.DataBind();

                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowInternalDownloadDocument();", true);

                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpendivDownloadDocument", "$(\"#divDownloadDocument\").dialog('open');", true);
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetInternalFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                                //if (commandArgs[1].Equals("1.0"))
                                //{
                                //    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                                //    if (ComplianceFileData.Count <= 0)
                                //    {
                                //        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                                //    }
                                //}
                                //else
                                //{
                                ComplianceFileData = ComplianceDocument;
                                //}

                                var ComplianceData = DocumentManagement.GetForMonthInternal(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }

                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                    }
                }
                if (e.CommandName == "View")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetInternalComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetInternalComplianceDocuments(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.InternalComplianceScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersionView.DataBind();
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);

                                        lblMessageInternal.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);

                                }
                                break;
                            }
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatus");
                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblInternalScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatusInternal");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatusReviewer");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerInternalComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblInternalScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatusReviewerInternal");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSetFilter_Click(object sender, EventArgs e)
        {
            try
            {
                //ddlComplinceCatagory.SelectedIndex = -1;
                //ddlFilterComplianceType.SelectedIndex = -1;
                ViewState["checkedCompliancesInstances"] = null;
                //BindCompliances();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDownloadList\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        // Zip all files from folder
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList) ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1") //Stautory 
                                {
                                    var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                    string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                else if(ddlDocType.SelectedValue == "0") //Internal 
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthInternal(ScheduledOnID);
                                    List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetInternalFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                    string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                else if (ddlDocType.SelectedValue == "2") //Stautory Checklist
                                {
                                    var ComplianceData = DocumentManagement.GetForMonthChecklist(ScheduledOnID);
                                    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;

                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                    string directoryName = CustomerBranch + "/" + ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                else if (ddlDocType.SelectedValue == "3") //Internal CheckList
                                {
                                    var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(ScheduledOnID);
                                    List<GetInternalComplianceDocumentsView> fileData = DocumentManagement.GetInternalFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name;
                                    string directoryName = CustomerBranch + "/" +  ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;

                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                            }

                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;


                        //byte[] data = zipMs.ToArray();
                        //Response.BinaryWrite(data);
                        //Response.BinaryWrite(@"C:\Users\devlp1\AppData\Local\Temp\document.txt", File.WriteAllBytes(data));

                        byte[] data = zipMs.ToArray();
                        //File.WriteAllBytes(@"C:\Users\devlp1\AppData\Local\Temp\Compliance\document.zip", data);
                        //Response.BinaryWrite(data);
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpSaveCheckedValues();
            FillComplianceDocuments();
            //BindComplianceDocument(DateTime.Now, DateTime.Now, pageIndex: e.NewPageIndex);
            CmpPopulateCheckedValues();
        }

        protected void grdInternalComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpSaveCheckedValues();
            FillComplianceDocuments();
            //BindComplianceDocument(DateTime.Now, DateTime.Now, pageIndex: e.NewPageIndex);
            CmpPopulateCheckedValues();
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            CmpSaveCheckedValues();

            CmpPopulateCheckedValues();
        }

        private void CmpPopulateCheckedValues()
        {
            try
            {

                ArrayList complianceDetails = (ArrayList) ViewState["checkedCompliances"];

                if (complianceDetails != null && complianceDetails.Count > 0)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        if (PerformerFlag)
                        {
                            foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfile");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkCompliances");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                        else if (ReviewerFlag)
                        {
                            foreach (GridViewRow gvrow in grdReviewerComplianceDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewer");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkReviewerCompliances");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                    }
                    else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                    {
                        if (PerformerFlag)
                        {
                            foreach (GridViewRow gvrow in grdInternalComplianceDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileInternal");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkInternalCompliances");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                        else if (ReviewerFlag)
                        {
                            foreach (GridViewRow gvrow in grdReviewerInternalComplianceDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewerInternal");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkReviewerInternalCompliances");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                    }

                    if (complianceDetails.Count > 0)
                    {
                        lblTotalSelected.Text = complianceDetails.Count + " Selected";
                        btnDownload.Visible = true;
                    }
                    else if (complianceDetails.Count == 0)
                    {
                        lblTotalSelected.Text = "";
                        btnDownload.Visible = false;
                    }
                }
                else
                {
                    lblTotalSelected.Text = "";
                    btnDownload.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CmpSaveCheckedValues()
        {
            try
            {
                ArrayList userdetails = new ArrayList();
                int index = -1;

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfile");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkCompliances")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }

                    else if (ReviewerFlag)
                    {
                        foreach (GridViewRow gvrow in grdReviewerComplianceDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewer");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkReviewerCompliances")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (PerformerFlag)
                    {
                        foreach (GridViewRow gvrow in grdInternalComplianceDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileInternal");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkInternalCompliances")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }
                    else if (ReviewerFlag)
                    {
                        foreach (GridViewRow gvrow in grdReviewerInternalComplianceDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewerInternal");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkReviewerInternalCompliances")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>      

        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();
                int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
                int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActListInternal()
        {
            try
            {
                ddlAct.Items.Clear();
                List<SP_GetInternalActsByUserID_Result> ActList = ActManagement.GetInternalActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListAll()
        {
            try
            {
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs(CustomerID, -1);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListAllInternal()
        {
            try
            {
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedInternalActs(CustomerID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategoriesAll()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedCategoryAll(CustomerID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCategories()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                var CatList = ActManagement.GetAllAssignedCategory(CustomerID, AuthenticationHelper.UserID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCategoriesInternal()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedInternalCategory(CustomerID, AuthenticationHelper.UserID);

                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCategoriesInternalAll()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedInternalCategoryAll(CustomerID);

                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTypesAll()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypeList = ActManagement.GetAllAssignedTypeAll(CustomerID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypeList = ActManagement.GetAllAssignedType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEvents()
        {
            try
            {
                ddlEvent.DataSource = null;
                ddlEvent.DataBind();
                ddlEvent.DataTextField = "EventName";
                ddlEvent.DataValueField = "eventid";
                var TypeList = ActManagement.GetActiveEvents(10, CustomerID, AuthenticationHelper.UserID, UserRoleID);
                ddlEvent.DataSource = TypeList;
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("Event Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEventNature()
        {
            try
            {
                ddlEventNature.DataSource = null;
                ddlEventNature.DataBind();
                ddlEventNature.DataTextField = "EventNature";
                ddlEventNature.DataValueField = "EventScheduleOnid";
                var TypeList = ActManagement.GetEventNature(10, CustomerID, AuthenticationHelper.UserID, Convert.ToInt32(ddlEvent.SelectedValue), UserRoleID);
                ddlEventNature.DataSource = TypeList;
                ddlEventNature.DataBind();

                ddlEventNature.Items.Insert(0, new ListItem("Event Nature", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypesInternal()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypleList = ActManagement.GetAllAssignedInternalType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypleList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesInternalAll()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypleList = ActManagement.GetAllAssignedInternalTypeAll(CustomerID);
                ddlType.DataSource = TypleList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["checkedCompliances"] = null;

                SelectedPageNo.Text = "1";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdComplianceDocument.PageIndex = 0;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = 0;
                    }
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (PerformerFlag)
                    {
                        grdInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdInternalComplianceDocument.PageIndex = 0;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerInternalComplianceDocument.PageIndex = 0;
                    }
                }

                FillComplianceDocuments();

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            btnDownload.Visible = false;
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            FillComplianceDocuments();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            FillComplianceDocuments();
        }

        protected void Submit(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                ViewState["checkedCompliances"] = null;

                SelectedPageNo.Text = "1";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                        grdComplianceDocument.PageIndex = 0;
                    else if (ReviewerFlag)
                        grdReviewerComplianceDocument.PageIndex = 0;
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (PerformerFlag)
                        grdInternalComplianceDocument.PageIndex = 0;
                    else if (ReviewerFlag)
                        grdReviewerInternalComplianceDocument.PageIndex = 0;
                }

                int type = 0;
                int Category = 0;
                int Act = 0;

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text == "" || txtAdvStartDate.Text == "" && txtAdvEndDate.Text != "")
                {
                    if (txtAdvStartDate.Text == "")
                        txtAdvStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtAdvEndDate.Text == "")
                        txtAdvEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtAdvEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtAdvEndDate.Text;
                    }
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += " " + "<b>Type to Filter: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    FillComplianceDocuments();
                    ShowSelectedRecords(sender, e);
                }
                else
                {
                    divAdvSearch.Visible = false;
                    FillComplianceDocuments();
                    ShowSelectedRecords(sender, e);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        public void FillComplianceDocuments()
        {
            try
            {
                var UserDetails = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                bool DeptHead = false;
                if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                {
                    DeptHead = (bool)UserDetails.IsHead;
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int RoleID = (from row in entities.Users
                                  where row.ID == AuthenticationHelper.UserID
                                  select row.RoleID).Single();

                    if (RoleID == 8)
                    {
                        PerformerFlag = true;
                    }
                }

                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);

                //String Status = ddlStatus.SelectedItem.Text;
                DocumentFilterNewStatus Status = (DocumentFilterNewStatus) Convert.ToInt16(ddlStatus.SelectedIndex);

                String location = tvFilterLocation.SelectedNode.Text;

                int type = Convert.ToInt32(ddlType.SelectedValue);

                int category = Convert.ToInt32(ddlCategory.SelectedValue);

                int ActID = Convert.ToInt32(ddlAct.SelectedValue);

                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                int SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);

                string StringType = "";

                int EventScheduleID;
                if (ddlEventNature.SelectedValue.ToString() != "")
                {
                    EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                else
                {
                    EventScheduleID = -1;
                }

                DateTime? DateFrom = null;
                DateTime? DateTo = null;

                if (txtAdvStartDate.Text != "")
                {
                    DateFrom = GetDate(txtAdvStartDate.Text);
                    //DateFrom = Convert.ToDateTime(txtAdvStartDate.Text);
                }
                if (txtAdvEndDate.Text != "")
                {
                    DateTo = GetDate(txtAdvEndDate.Text);
                    //DateTo = Convert.ToDateTime(txtAdvEndDate.Text);
                }

                StringType = txtSearchType.Text;

                Session["TotalRows"] = 0;

                if (PerformerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1")
                    {
                       
                            var ComplianceDocs = DocumentManagement.GetFilteredComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, SubTypeID, StringType);
                            grdComplianceDocument.DataSource = ComplianceDocs;
                            Session["TotalRows"] = ComplianceDocs.Count;
                            grdComplianceDocument.DataBind();

                            grdComplianceDocument.Visible = true;
                            grdInternalComplianceDocument.Visible = false;

                            grdReviewerComplianceDocument.Visible = false;
                            grdReviewerInternalComplianceDocument.Visible = false;
                        
                    }
                    else if (ddlDocType.SelectedValue == "1")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredEventComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, EventID, EventScheduleID, SubTypeID, StringType);
                        grdComplianceDocument.DataSource = ComplianceDocs;
                        Session["TotalRows"] = ComplianceDocs.Count;
                        grdComplianceDocument.DataBind();

                        grdComplianceDocument.Visible = true;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = false;
                    }
                    else if (ddlDocType.SelectedValue == "0")
                    {
                        //if (DeptHead)
                        //{
                        //    var ComplianceDocs = DepartmentHeadManagement.GetFilteredInternalComplianceDocuments_DeptHead(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        //    grdInternalComplianceDocument.DataSource = ComplianceDocs;
                        //    Session["TotalRows"] = ComplianceDocs.Count;
                        //    grdInternalComplianceDocument.DataBind();

                        //    grdComplianceDocument.Visible = false;
                        //    grdInternalComplianceDocument.Visible = true;

                        //    grdReviewerComplianceDocument.Visible = false;
                        //    grdReviewerInternalComplianceDocument.Visible = false;
                        //}
                        //else
                        //{
                            var ComplianceDocs = DocumentManagement.GetFilteredInternalComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                            grdInternalComplianceDocument.DataSource = ComplianceDocs;
                            Session["TotalRows"] = ComplianceDocs.Count;
                            grdInternalComplianceDocument.DataBind();

                            grdComplianceDocument.Visible = false;
                            grdInternalComplianceDocument.Visible = true;

                            grdReviewerComplianceDocument.Visible = false;
                            grdReviewerInternalComplianceDocument.Visible = false;
                        //}
                    }
                    if (ddlDocType.SelectedValue == "2")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredCheckListDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        grdComplianceDocument.DataSource = ComplianceDocs;
                        Session["TotalRows"] = ComplianceDocs.Count;
                        grdComplianceDocument.DataBind();

                        grdComplianceDocument.Visible = true;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = false;
                    }
                    if (ddlDocType.SelectedValue == "3")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredInternalCheckListComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        grdInternalComplianceDocument.DataSource = ComplianceDocs;
                        Session["TotalRows"] = ComplianceDocs.Count;
                        grdInternalComplianceDocument.DataBind();

                        grdComplianceDocument.Visible = false;
                        grdInternalComplianceDocument.Visible = true;

                        grdReviewerComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = false;
                    }
                }
                else if (ReviewerFlag)
                {
                    if (ddlDocType.SelectedValue == "-1")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, risk, Status, location, type, category, ActID, DateFrom, DateTo, SubTypeID, StringType);
                        grdReviewerComplianceDocument.DataSource = ComplianceDocs;
                        grdReviewerComplianceDocument.DataBind();
                        Session["TotalRows"] = ComplianceDocs.Count;

                        grdComplianceDocument.Visible = false;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = true;
                        grdReviewerInternalComplianceDocument.Visible = false;

                    }
                    else if (ddlDocType.SelectedValue == "1")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredEventComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, risk, Status, location, type, category, ActID, DateFrom, DateTo, EventID, EventScheduleID, SubTypeID, StringType);
                        grdReviewerComplianceDocument.DataSource = ComplianceDocs;
                        grdReviewerComplianceDocument.DataBind();
                        Session["TotalRows"] = ComplianceDocs.Count;

                        grdComplianceDocument.Visible = false;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = true;
                        grdReviewerInternalComplianceDocument.Visible = false;

                    }
                    else if (ddlDocType.SelectedValue == "0")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredInternalComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        grdReviewerInternalComplianceDocument.DataSource = ComplianceDocs;
                        grdReviewerInternalComplianceDocument.DataBind();
                        Session["TotalRows"] = ComplianceDocs.Count;

                        grdComplianceDocument.Visible = false;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = true;
                    }
                    else if (ddlDocType.SelectedValue == "2")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredCheckListReviewerDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        grdReviewerComplianceDocument.DataSource = ComplianceDocs;
                        Session["TotalRows"] = ComplianceDocs.Count;
                        grdReviewerComplianceDocument.DataBind();
                        grdReviewerComplianceDocument.Visible = true;
                        grdComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = false;
                    }
                    if (ddlDocType.SelectedValue == "3")
                    {
                        var ComplianceDocs = DocumentManagement.GetFilteredInternalCheckListComplianceDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);
                        grdReviewerInternalComplianceDocument.DataSource = ComplianceDocs;
                        grdReviewerInternalComplianceDocument.DataBind();
                        Session["TotalRows"] = ComplianceDocs.Count;

                        grdComplianceDocument.Visible = false;
                        grdInternalComplianceDocument.Visible = false;

                        grdReviewerComplianceDocument.Visible = false;
                        grdReviewerInternalComplianceDocument.Visible = true;
                    }
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceInstances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    CheckBox headerchk = (CheckBox)grdComplianceInstances.HeaderRow.FindControl("chkFilterCompliancesHeader");
                //    CheckBox childchk = (CheckBox)e.Row.FindControl("chkFilterCompliances");
                //    childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdComplianceInstances')");
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected string GetPerformer(long complianceinstanceid, string ComType)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetUserName(complianceinstanceid, 3, ComType);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetReviewer(long complianceinstanceid, string ComType)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetUserName(complianceinstanceid, 4, ComType);
                Reviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

     
        protected void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();

                // ScriptManager.RegisterStartupScript(upDocumentDownload, upDocumentDownload.GetType(), "show", "$(function () { $('#" + Panel1.ClientID + "').modal('show'); });", true);
                upDocumentDownload.Update();

                //BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (!IsValid()) { return; };

                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    if (PerformerFlag)
                    {
                        grdComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (!IsValid()) { return; };

                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    if (PerformerFlag)
                    {
                        grdInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }

                //Reload the Grid
                FillComplianceDocuments();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {


                if (ddlDocType.SelectedValue == "2" || ddlDocType.SelectedValue == "3")
                {
                    Session["ComType"] = "CheckList";
                }
                else
                {
                    Session["ComType"] = "Statutory";
                }
                divEvent.Visible = false;
                ddlEventNature.SelectedValue = "-1";
                ddlEvent.SelectedValue = "-1";
                Panelsubtype.Enabled = false;
                PanelAct.Enabled = true;
                if (ddlDocType.SelectedValue == "0")
                {
                    ddlEventNature.SelectedValue = "-1";
                    ddlEvent.SelectedValue = "-1";
                    ddlAct.ClearSelection();

                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        PanelAct.Enabled = true;
                        DivAct.Attributes.Remove("disabled");
                    }
                    else
                    {
                        PanelAct.Enabled = false;
                        DivAct.Attributes.Add("disabled", "true");
                    }

                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindCategoriesInternalAll();
                        BindTypesInternalAll();
                    }
                    else
                    {
                        BindCategoriesInternal();
                        BindTypesInternal();
                    }
                }
                else
                {
                    Panelsubtype.Enabled = true;
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    DivAct.Attributes.Remove("disabled");

                    if (ddlDocType.SelectedValue == "-1")
                    {
                        ddlEventNature.SelectedValue = "-1";
                        ddlEvent.SelectedValue = "-1";
                    }
                    if (ddlDocType.SelectedValue == "2")
                    {
                        ddlEventNature.SelectedValue = "-1";
                        ddlEvent.SelectedValue = "-1";
                        Panelsubtype.Enabled = false;
                    }

                    if (ddlDocType.SelectedValue == "3")
                    {
                        ddlEventNature.SelectedValue = "-1";
                        ddlEvent.SelectedValue = "-1";
                        Panelsubtype.Enabled = false;
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            PanelAct.Enabled = true;
                        }
                        else
                        {
                            PanelAct.Enabled = false;
                        }
                    }

                    if (ddlDocType.SelectedValue == "1")
                    {
                        divEvent.Visible = true;
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            PanelAct.Enabled = true;
                        }
                        else
                        {
                            PanelAct.Enabled = false;
                        }
                    }


                    if (UserRoleID == 8 || UserRoleID == 9)
                    {
                        BindTypesAll();
                        BindCategoriesAll();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;

                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtAdvStartDate.Text = "";
                txtAdvEndDate.Text = "";
                txtSearchType.Text = string.Empty;
                divAdvSearch.Visible = false;


                SelectedPageNo.Text = "1";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                        grdComplianceDocument.PageIndex = 0;
                    else if (ReviewerFlag)
                        grdReviewerComplianceDocument.PageIndex = 0;
                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    if (PerformerFlag)
                        grdInternalComplianceDocument.PageIndex = 0;
                    else if (ReviewerFlag)
                        grdReviewerInternalComplianceDocument.PageIndex = 0;
                }

                FillComplianceDocuments();

                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                //BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
                //BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindCompliances();
        }



        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (PerformerFlag)
                    {
                        grdInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdReviewerInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }

                //Reload the Grid
                FillComplianceDocuments();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid()) { return; };

                CmpSaveCheckedValues();

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                else if (ddlDocType.SelectedValue == "0" || ddlDocType.SelectedValue == "3")
                {
                    if (PerformerFlag)
                    {
                        grdInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerInternalComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerInternalComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }

                //Reload the Grid
                FillComplianceDocuments();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // throw ex;
            }
        }

      

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton) e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // LinkButton btnComplinceVersionDocView = (LinkButton)e.Item.FindControl("btnComplinceVersionDocView");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(btnComplinceVersionDocView);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }

        //Download module popup
        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();
                        List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(Session["ScheduleOnID"]);
                                entitiesData.Add(entityData);
                            }
                            rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersion.DataBind();
                        }

                        //UpdatePanel1.Update();
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        if (ddlDocType.SelectedValue == "2")
                        {
                            var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(commandArgs[0]));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                        }


                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
                else if (e.CommandName.Equals("View"))
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    lblMessage.Text = "";
                                    lblMessage.Text = "Zip file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;

                                    CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptIComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                //List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();GetInternalComplianceDocumentsView
                //List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                //ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptIComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptIComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();
                        //List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        List<GetInternalComplianceDocumentsView> CMPDocuments = Business.InternalComplianceManagement.GetInternalDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        if (CMPDocuments != null)
                        {
                            //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.InternalComplianceScheduledOnID = Convert.ToInt64(Session["ScheduleOnID"]);
                                entitiesData.Add(entityData);
                            }
                            rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersion.DataBind();
                        }
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        if (ddlDocType.SelectedValue == "3")
                        {
                            var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(commandArgs[0]));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }                           
                        }
                        else
                        {
                            var ComplianceData = DocumentManagement.InternalComplianceGetForMonth(Convert.ToInt32(commandArgs[0]));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument_" + commandArgs[1] + ".zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }
                }
                else if (e.CommandName.Equals("View"))
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    lblMessageInternal.Text = "";
                                    lblMessageInternal.Text = "Zip file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowInternalDownloadDocument();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;

                                    CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    lblMessageInternal.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnIComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton) e.Item.FindControl("lblIDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

            }
        }

        protected void rptIComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton) e.Item.FindControl("lblIDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }

        protected void rptIComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnIComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEventNature();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";

                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    lblMessageInternal.Text = "";
                                    lblMessageInternal.Text = "Zip file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;
                                    lblMessageInternal.Text = "";
                                    CompDocReviewPath = FileName;
                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                lblMessageInternal.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                            }
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}