﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ICFRObservationDetails : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static string ApplyFilter;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerList();
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                BindProcess("P");              
                BindFinancialYear();
                BindLegalEntityData();                
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindData();
                bindPageNumber();                                             
            }
        }

        private void BindCustomerList()
        {
            long userId = Portal.Common.AuthenticationHelper.UserID;
            int ServiceProviderID = Portal.Common.AuthenticationHelper.ServiceProviderID;
            int RoleID = 0;
            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
            {
                RoleID = 2;
            }
            var customerList = RiskCategoryManagement.GetCustomerListForDDL(userId, ServiceProviderID, RoleID);
            if (customerList.Count > 0)
            {
                ddlCustomer.DataTextField = "CustomerName";
                ddlCustomer.DataValueField = "CustomerId";
                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomer.SelectedIndex = 2;
            }
            else
            {
                ddlCustomer.DataSource = null;
                ddlCustomer.DataBind();
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;            
            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);            
            BindData();
        }

        private void BindProcess(string flag)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                string IsMGMTORAM = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ISMGMT"]))
                {
                    IsMGMTORAM = Request.QueryString["ISMGMT"];
                }
                else
                {
                    IsMGMTORAM = Convert.ToString(ViewState["ISMGMT"]);
                }
                ddlProcess.Items.Clear();
                ddlProcess.DataTextField = "Name";
                ddlProcess.DataValueField = "Id";

                if (IsMGMTORAM == "AM")
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownAuditManager(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                }
                else if (IsMGMTORAM == "MG")
                {
                    ddlProcess.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                }
                ddlProcess.DataBind();
                ddlProcess.Items.Insert(0, new ListItem(" Select Process ", "-1"));                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       
        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(CustomerId);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            string IsMGMTORAM = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["ISMGMT"]))
            {
                IsMGMTORAM = Request.QueryString["ISMGMT"];
            }
            else
            {
                IsMGMTORAM = Convert.ToString(ViewState["ISMGMT"]);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (IsMGMTORAM == "AM")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (IsMGMTORAM == "MG")
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

     
        private void BindData()
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                int keynonkeyid = -1;
                string FinancialYear = string.Empty;
                string Type = string.Empty;
                string keynonkey = string.Empty;
                string period = string.Empty;
                string ISAHYQ = string.Empty;
                string IsMGMTORAM = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    ViewState["PassFail"] = null;
                    Type = Request.QueryString["Type"];
                    ViewState["PassFail"] = Type;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["keynonkey"]))
                {
                    keynonkey = Request.QueryString["keynonkey"];
                }                               
                if (!string.IsNullOrEmpty(Request.QueryString["ISQHYA"]))
                {
                    ViewState["ISQHYA"] = null;
                    ISAHYQ = Request.QueryString["ISQHYA"];
                    ViewState["ISQHYA"] = ISAHYQ;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ISMGMT"]))
                {
                    ViewState["ISMGMT"] = null;
                    IsMGMTORAM = Request.QueryString["ISMGMT"];
                    ViewState["ISMGMT"] = IsMGMTORAM;
                }                
                if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    ViewState["FinancialYear"] = null;
                    FinancialYear = Convert.ToString(Request.QueryString["FinYear"]);
                    ViewState["FinancialYear"] = FinancialYear.ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["period"]))
                {
                    ViewState["period"] = null;
                    period = Request.QueryString["period"];
                    ViewState["period"] = period.ToString();
                }
                ApplyFilter = "N";
                List<int?> BranchList = new List<int?>();
                if (Session["BranchList"] != null)
                {
                    BranchList = (List<int?>) (Session["BranchList"]);
                }
                if (keynonkey== "Non Key")
                {
                    keynonkeyid = 2;
                }
                else if (keynonkey == "Key")
                {
                    keynonkeyid = 1;
                }              
                var detailView = GetDetailView(CustomerId, BranchList, FinancialYear, period, keynonkeyid, Type,-1, ISAHYQ, IsMGMTORAM);               
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();

                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<AuditInstanceTransactionView>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<AuditInstanceTransactionView> GetDetailView(int customerid, List<int?> BranchIDList, string FinancialYear, string Period,int keyid,string type,int ProcessId,string ISAHYQFLAG,string IsMGMTORAM)
        {
            List<AuditInstanceTransactionView> detailView = new List<AuditInstanceTransactionView>();
            List<string> quarter = new List<string>();
           
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // detailView = (from row in entities.AuditInstanceTransactionViews
                //   where row.CustomerID == customerid && row.RoleID == 3
                ////   && row.ForMonth == Period
                //   select row).ToList();

                if (IsMGMTORAM=="MG")
                {
                    detailView = (from c in entities.AuditInstanceTransactionViews
                                  join EAAMR in entities.EntitiesAssignmentManagementRisks
                                  on c.ProcessId equals EAAMR.ProcessId
                                  where c.CustomerBranchID == EAAMR.BranchID
                                  && c.CustomerID == customerid && c.RoleID == 3
                                  && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                  && EAAMR.ISACTIVE == true
                                  select c).ToList();
                }
                else if (IsMGMTORAM=="AM")
                {
                    detailView = (from c in entities.AuditInstanceTransactionViews
                                  join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                  on c.ProcessId equals EAAMR.ProcessId
                                  where c.CustomerBranchID == EAAMR.BranchID
                                  && c.CustomerID == customerid && c.RoleID == 3
                                  && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                  && EAAMR.ISACTIVE == true
                                  select c).ToList();
                }
                

                if (ISAHYQFLAG == "A")
                {
                    quarter.Clear();
                    quarter.Add("Apr - Jun");
                    quarter.Add("Jul - Sep");
                    quarter.Add("Oct - Dec");
                    quarter.Add("Jan - Mar");

                    detailView = detailView.Where(entry => quarter.Contains(entry.ForMonth)).ToList();

                }
                else if (ISAHYQFLAG == "HF1")
                {
                    quarter.Add("Apr - Jun");
                    quarter.Add("Jul - Sep");
                    detailView = detailView.Where(entry => quarter.Contains(entry.ForMonth)).ToList();
                }
                else if (ISAHYQFLAG == "HF2")
                {
                    quarter.Add("Oct - Dec");
                    quarter.Add("Jan - Mar");
                    detailView = detailView.Where(entry => quarter.Contains(entry.ForMonth)).ToList();
                }
                else if (ISAHYQFLAG == "Q")
                {
                    detailView = detailView.Where(entry => entry.ForMonth== Period).ToList();
                }


                if (BranchIDList.Count>0)
                    detailView = detailView.Where(entry => BranchIDList.Contains(entry.CustomerBranchID)).ToList();

                if (FinancialYear != "")
                    detailView = detailView.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (ProcessId != -1)
                    detailView = detailView.Where(entry => entry.ProcessId == ProcessId).ToList();

                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                
                detailView = detailView.Where(a => a.KeyId == keyid).ToList().ToList();
                if (type== "Not Tested")
                {
                    detailView = detailView.Where(a => a.TOD == null && a.TOE == null).ToList();
                    //detailView = detailView.Where(a => ((a.TOD == 2 && a.TOE == -1 && a.AuditStatusID == 3) || (a.TOD == null && a.TOE == null))).ToList();
                }
                else if (type == "Fail")
                {
                    //detailView = detailView.Where(a => (a.TOD == 2 && a.TOE == -1) || (a.TOD == 1 && a.TOE == 2) || (a.TOD == 3 && a.TOE == 2)).ToList();
                    detailView = detailView.Where(a => //a.AuditStatusID == 3 && 
                    ((a.TOD == 2 && a.TOE == -1)
                    || (a.TOD == 1 && a.TOE == 2) 
                    || (a.TOD == 3 && a.TOE == 2)
                    || (a.TOD == 2 && a.TOE == 2)
                    )).ToList();
                }
                else if (type == "Pass")
                {
                    detailView = detailView.Where(a => (a.TOD == 1 && a.TOE == 1) 
                    || (a.TOD == 1 && a.TOE == 3) || (a.TOD == 3 && a.TOE == 1) || (a.TOD == 3 && a.TOE == 3)).ToList();
                }              
            }
            return detailView;
        }
        private void BindDataFilter()
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                int ProcessId = -1;
                int CustBranchID = -1;
                int keynonkeyid = -1;
                string IsMGMTORAM = string.Empty;
                string FinancialYear = string.Empty;
                string Type = string.Empty;
                string keynonkey = string.Empty;
                string Period = string.Empty;
                string ISAHYQ = string.Empty;
                
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        ProcessId = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                        ViewState["FinancialYear"] = null;
                        ViewState["FinancialYear"] = FinancialYear.ToString();

                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["FinYear"]))
                    {
                        FinancialYear = Convert.ToString(Request.QueryString["FinYear"]);
                        ViewState["FinancialYear"] = null;
                        ViewState["FinancialYear"] = FinancialYear.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["period"]))
                {
                    Period = Request.QueryString["period"];
                    ViewState["period"] = null;
                    ViewState["period"] = Period.ToString();
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ISQHYA"]))
                {
                    ViewState["ISQHYA"] = null;
                    ISAHYQ = Request.QueryString["ISQHYA"];
                    ViewState["ISQHYA"] = ISAHYQ;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ISMGMT"]))
                {
                    ViewState["ISMGMT"] = null;
                    IsMGMTORAM = Request.QueryString["ISMGMT"];
                    ViewState["ISMGMT"] = IsMGMTORAM;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    ViewState["PassFail"] = null;
                    Type = Request.QueryString["Type"];
                    ViewState["PassFail"] = Type;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["keynonkey"]))
                {
                    keynonkey = Request.QueryString["keynonkey"];
                }
                if (keynonkey == "Non Key")
                {
                    keynonkeyid = 2;
                }
                else if (keynonkey == "Key")
                {
                    keynonkeyid = 1;
                }

                List<int?> ListofBranch = new List<int?>();

                if (CustBranchID == -1)
                {
                    if (Session["BranchList"] != null)
                    {
                        ListofBranch = (List<int?>) (Session["BranchList"]);
                    }
                }
                else
                {
                    Branchlist.Clear();
                    GetAllHierarchy(CustomerId, CustBranchID);

                    if (Branchlist.Count > 0)
                    {
                        ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                    }
                }

                var detailView = GetDetailView(CustomerId, ListofBranch, FinancialYear, Period, keynonkeyid, Type, ProcessId, ISAHYQ, IsMGMTORAM);
                grdSummaryDetailsAuditCoverage.DataSource = detailView;
                Session["TotalRows"] = detailView.Count;
                grdSummaryDetailsAuditCoverage.DataBind();

                Session["grdDetailData"] = (grdSummaryDetailsAuditCoverage.DataSource as List<AuditInstanceTransactionView>).ToDataTable();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ApplyFilter = "Y";
                BindDataFilter();
                bindPageNumber();                
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {           
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();                    
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();                    
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                   
                }
            }            
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();                    
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                   
                }
            }            
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();                    
                }
            }            
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        public static string GetUserName(int riskcreationid, long processid, long subprocessid, long branchid, int Roleid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditAssignments
                                         join mu in entities.mst_User on row.UserID equals mu.ID
                                         where row.RiskCreationId == riskcreationid
                                         && row.RoleID == Roleid
                                         && row.ProcessId == processid && row.SubProcessId == subprocessid
                                         && row.CustomerBranchID == branchid
                                         select mu.FirstName + " " + mu.LastName).FirstOrDefault();

                return transactionsQuery;
            }
        }
        protected string GetReviewer(int riskcreationid, long processid, long subprocessid, long branchid)
        {
            try
            {
                string result = "";
                result = GetUserName(riskcreationid, processid, subprocessid, branchid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetPerformer(int riskcreationid, long processid, long subprocessid, long branchid)
        {
            try
            {
                string result = "";
                result = GetUserName(riskcreationid, processid, subprocessid, branchid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetManagementResponse(int riskcreationid, long Scheduledonid, long branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.ActionPlanAssignments
                                         where row.RiskCreationId == riskcreationid
                                         && row.AuditScheduleOnID == Scheduledonid
                                         && row.CustomerBranchId == branchid
                                         select row).ToList();
                if (transactionsQuery != null)
                {
                    transactionsQuery = transactionsQuery.OrderByDescending(entry => entry.Id).ToList();
                    return transactionsQuery.Select(entry => entry.ActionPlan).FirstOrDefault();
                }
                else
                {
                    return "";
                }
            }
        }
        protected string GetReasonForPassOrFail(int riskcreationid, long Scheduledonid, long branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                // select ResonForPassFail,* from Mst_riskResult where AuditScheduleOnID = 75 and Customerbranchid = 62 and RiskCreationId = 1
                var transactionsQuery = (from row in entities.Mst_RiskResult
                                         where row.RiskCreationId == riskcreationid
                                         && row.AuditScheduleOnID == Scheduledonid
                                         && row.CustomerBranchId == branchid
                                         select row).ToList();
                if (transactionsQuery != null)
                {
                    transactionsQuery = transactionsQuery.OrderByDescending(entry => entry.ID).ToList();
                    return transactionsQuery.Select(entry => entry.ResonForPassFail).FirstOrDefault();
                }
                else
                {
                    return "";
                }
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {

            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;
                    string financialyear = "";
                    string PassFail = "";
                    string Period = "";
                    if (!string.IsNullOrEmpty(ViewState["FinancialYear"].ToString()))
                    {
                        financialyear = ViewState["FinancialYear"].ToString();
                    }
                    else
                    {
                        financialyear = ddlFinancialYear.SelectedItem.Text;
                    }
                    if (!string.IsNullOrEmpty(ViewState["period"].ToString()))
                    {
                        Period = ViewState["period"].ToString();
                    }
                    if (!string.IsNullOrEmpty(ViewState["PassFail"].ToString()))
                    {
                        PassFail = ViewState["PassFail"].ToString();
                    }


                    FileName = "Testing Result For-" + Period + " (FY " + financialyear + ")";



                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                    ExcelData = view.ToTable("Selected", false, "ControlNo", "Branch", "ProcessName", "SubProcessName", "ActivityDescription", "ControlDescription", "KeyName", "RiskCreationId", "ProcessId", "SubProcessId", "CustomerBranchId", "ScheduledOnID");
                    ExcelData.Columns.Add("Performer");
                    ExcelData.Columns.Add("Reviewer");
                    ExcelData.Columns.Add("PassFail");
                    ExcelData.Columns.Add("ManagementReponse");
                    ExcelData.Columns.Add("Remark");
                    foreach (DataRow item in ExcelData.Rows)
                    {
                        item["Performer"] = GetPerformer(Convert.ToInt32(item["RiskCreationId"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["SubProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                        item["Reviewer"] = GetReviewer(Convert.ToInt32(item["RiskCreationId"].ToString()), Convert.ToInt32(item["ProcessId"].ToString()), Convert.ToInt32(item["SubProcessId"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                        item["PassFail"] = PassFail;
                        item["ManagementReponse"] = GetManagementResponse(Convert.ToInt32(item["RiskCreationId"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                        item["Remark"] = GetReasonForPassOrFail(Convert.ToInt32(item["RiskCreationId"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()), Convert.ToInt32(item["CustomerBranchId"].ToString()));
                    }
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                    }

                    var customerName = CustomerManagement.CustomerGetByIDName(CustomerId);
                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customerName;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    ExcelData.Columns.Remove("RiskCreationId");
                    ExcelData.Columns.Remove("ProcessId");
                    ExcelData.Columns.Remove("SubProcessId");
                    ExcelData.Columns.Remove("CustomerBranchId");
                    ExcelData.Columns.Remove("ScheduledOnID");

                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A3"].Value = FileName;
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Control No";
                    exWorkSheet.Cells["A5"].AutoFitColumns(15);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Location";
                    exWorkSheet.Cells["B5"].AutoFitColumns(25);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Process";
                    exWorkSheet.Cells["C5"].AutoFitColumns(25);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Sub Process";
                    exWorkSheet.Cells["D5"].AutoFitColumns(25);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "Risk Description";
                    exWorkSheet.Cells["E5"].AutoFitColumns(50);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Control Description";
                    exWorkSheet.Cells["F5"].AutoFitColumns(50);
                    
                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Key/Non Key";
                    exWorkSheet.Cells["G5"].AutoFitColumns(20);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Performer";
                    exWorkSheet.Cells["H5"].AutoFitColumns(25);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Reviewer";
                    exWorkSheet.Cells["I5"].AutoFitColumns(25);

                    exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J5"].Value = "Pass Or Fail";
                    exWorkSheet.Cells["J5"].AutoFitColumns(20);

                    exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K5"].Value = "Management Response";
                    exWorkSheet.Cells["K5"].AutoFitColumns(20);

                    exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L5"].Value = "Reason For Pass or Fail";
                    exWorkSheet.Cells["L5"].AutoFitColumns(20);
                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 12])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

      

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //Reload the Grid
                if (ApplyFilter == "Y")
                {
                    BindDataFilter();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                else
                {
                    BindData();
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
               // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        if (ApplyFilter == "Y")
        //        {
        //            BindDataFilter();
        //        }
        //        else
        //        {
        //            BindData();
        //        }
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
               
        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdSummaryDetailsAuditCoverage.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }

        //        //Reload the Grid
        //        if (ApplyFilter=="Y")
        //        {
        //            BindDataFilter();
        //        }
        //        else
        //        {
        //            BindData();
        //        }
               
        //        GetPageDisplaySummary();
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}