﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class MangementDashboard : System.Web.UI.Page
    {
        int checkInternalapplicable = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                lblErrorMessage.Text = string.Empty;
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    User user = UserManagement.GetByID(userID);
                    
                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }                   
                }
                if (checkInternalapplicable == 1)
                {
                    Tab2.Visible = true;
                }
                else if (checkInternalapplicable == 0)
                {
                    Tab1.Visible = true;
                    Tab2.Visible = false;
                }             
                Tab1_Click(sender, e);
            }
        }

        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ucSatutoryManagementDashboard.Visible = true;
                ucSatutoryManagementDashboard.DrowGraphs(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        protected void rdInternalaRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                UCInternalManagementDashboard.Visible = true;
                UCInternalManagementDashboard.DrowGraphs(customerid,true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessageInternal.Text = "Server Error Occured. Please try again.";
            }
        }             
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }             
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            ucSatutoryManagementDashboard.Visible = true;
            ucSatutoryManagementDashboard.DrowGraphs(true);
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
            UCInternalManagementDashboard.Visible = true;
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

            UCInternalManagementDashboard.DrowGraphs(customerid,true);
        }        
    }
}