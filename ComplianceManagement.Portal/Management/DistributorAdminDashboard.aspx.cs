﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class DistributorAdminDashboard : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static int CustId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers();
                ddlDateSummery_SelectedIndexChanged(sender, e);
                //LoadDashboardCustomerWise(-1);

                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
        }
        public static List<Customer> GetAllCustomers(string rolecode, int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var clist = (from row in entities.Customers
                             where row.IsDeleted == false
                              && row.ComplianceProductType != 1
                             select row);

                if (rolecode == "DADMN")
                {
                    clist = clist.Where(entry => entry.ParentID == distributorid);
                }
                else if (rolecode == "EXCT")
                {                  
                    clist = clist.Where(entry => entry.ParentID == distributorid);
                }
                return clist.OrderBy(entry => entry.Name).ToList();
            }
        }
        protected void btnSelectAndProceed_Click(object sender, EventArgs e)
        {
            ProductMappingStructure _obj = new ProductMappingStructure();
            string role = string.Empty;
            string prodApplLogin = string.Empty;
            if (AuthenticationHelper.Role == "DADMN")
            {
                role = "MGMT"; prodApplLogin = "DFM";
            }
            if (AuthenticationHelper.Role == "EXCT")
            {
                role = "EXCT"; prodApplLogin = "DFM";
            }
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
            {
                _obj.ReAuthenticate_UserNew(Convert.ToInt32(ddlCustomer.SelectedValue), role, prodApplLogin);
            }
        }
        private void BindCustomers()
        {
            try
            {
                int distributorID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    var CustIds = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    distributorID = ICIAManagement.GetParentIDforCustomer(CustIds);
                }
                var data = GetAllCustomers(AuthenticationHelper.Role, distributorID);
                if (data.Count > 0)
                {                  
                    ddlCustomer.DataTextField = "Name";
                    ddlCustomer.DataValueField = "ID";
                    ddlCustomer.DataSource = data;
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, new ListItem("Group/Client - All", "-1"));
                    if (data.Count>=1)
                    {
                        ddlCustomer.Items[1].Selected = true;
                        btnSelectAndProceed.Visible = true;
                        //btnSelectAndProceed_Click(null,null);
                    }                    
                }            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void SetTopBoxesCounts(List<DFM_LegalComplianceInstanceTransactionCount_Result> MastertransactionsQueryList, List<SP_RLCS_GetAssignedCustomers_Result> custmerlist) /*UserAllCustomer*/
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (custmerlist.Count > 0)
                    {
                        divGroupClientCount.InnerText = Convert.ToString(custmerlist.Count);
                    }
                    else
                    {
                        divGroupClientCount.InnerText = "0";
                    }


                    entities.Database.CommandTimeout = 180;
                    divEntitiesCount.InnerText = Convert.ToString(MastertransactionsQueryList.Select(row => row.CustomerBranchID).Distinct().Count());


                    if (MastertransactionsQueryList.Count > 0)
                    {
                        //Upcoming                                     
                        divUpcomingCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Upcoming").Count());

                        //DueToday                                     
                        divOverdueCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "Overdue").Count());

                        //Overdue                                     
                        divPendingReviewCount.InnerText = Convert.ToString(DashboardData_ComplianceDisplayCount(MastertransactionsQueryList, "PendingForReview").Count());
                    }
                    else
                    {
                        divUpcomingCount.InnerText = "0";
                        divOverdueCount.InnerText = "0";
                        divPendingReviewCount.InnerText = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static List<DFM_LegalComplianceInstanceTransactionCount_Result> DashboardData_ComplianceDisplayCount(List<DFM_LegalComplianceInstanceTransactionCount_Result> MastertransactionsQuery, string filter)
        {
            List<DFM_LegalComplianceInstanceTransactionCount_Result> transactionsQuery = new List<DFM_LegalComplianceInstanceTransactionCount_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ScheduledOn > now && row.ScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.ScheduledOn < now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case "PendingForReview":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }
        private void LoadDashboardCustomerWise(int customerID)
        {
            int distributorID = -1;

            if (AuthenticationHelper.Role == "DADMN")
            {
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                var CustIds = Convert.ToInt32(AuthenticationHelper.CustomerID);
                distributorID = ICIAManagement.GetParentIDforCustomer(CustIds);
            }

            List<SP_RLCS_GetAssignedCustomers_Result> lstMyAssignedCustomers = new List<SP_RLCS_GetAssignedCustomers_Result>();
            List<int> lstSelectedCustomers = new List<int>();

            lstMyAssignedCustomers = UserCustomerMappingManagement.Get_AssignedCustomers(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role);


            using (Business.Data.ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;


                var MastertransactionsQuery = (entities.DFM_LegalComplianceInstanceTransactionCount(AuthenticationHelper.UserID, distributorID, AuthenticationHelper.Role)
                                        .Where(entry => entry.IsActive == true
                                            && entry.IsUpcomingNotDeleted == true)).ToList();

                if (customerID != -1)
                {
                    MastertransactionsQuery = MastertransactionsQuery.Where(entry => entry.CustomerID == customerID).ToList();
                }
                SetTopBoxesCounts(MastertransactionsQuery, lstMyAssignedCustomers);


                GetManagementCompliancesSummary(customerID, distributorID, null, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MastertransactionsQuery, null, null);
            }
        }
        public static List<DFM_Legal_ComplianceAssignedCategory_Result> GetNewAll(int customerid, int distributorID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<DFM_Legal_ComplianceAssignedCategory_Result> complianceCategorys = new List<DFM_Legal_ComplianceAssignedCategory_Result>();

                complianceCategorys = (from row in entities.DFM_Legal_ComplianceAssignedCategory(AuthenticationHelper.UserID, customerid, distributorID, AuthenticationHelper.Role)
                                       select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }



        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlDateSummery_SelectedIndexChanged(sender, e);
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                {
                    btnSelectAndProceed.Visible = true;
                    LoadDashboardCustomerWise(Convert.ToInt32(ddlCustomer.SelectedValue));
                }
                else
                {
                    btnSelectAndProceed.Visible = false;
                    LoadDashboardCustomerWise(-1);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void ddlDateSummery_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDateSummery.SelectedValue.Equals("0"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (ddlDateSummery.SelectedValue.Equals("1"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-2);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (ddlDateSummery.SelectedValue.Equals("2"))
                {
                    string dtfrom = Convert.ToString("01-01-1900");
                    string dtto = Convert.ToString("01-01-1900");
                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                {
                    LoadDashboardCustomerWise(Convert.ToInt32(ddlCustomer.SelectedValue));
                }
                else
                {
                    LoadDashboardCustomerWise(-1);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        public void GetManagementCompliancesSummary(int customerid, int distributorID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<DFM_LegalComplianceInstanceTransactionCount_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;

                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;

                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }

                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                }


                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<DFM_LegalComplianceInstanceTransactionCount_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (Branchlist != null && Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<DFM_LegalComplianceInstanceTransactionCount_Result> transactionsQuery = new List<DFM_LegalComplianceInstanceTransactionCount_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {

                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            if (AuthenticationHelper.Role == "AUDT")
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                     || row.ScheduledOn < FIEndDate)
                                                     select row).ToList();
                            }
                            else
                            {
                                //For Financial Year
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.ScheduledOn >= FIFromDate
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        else
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.ScheduledOn < FIEndDate
                                                 select row).ToList();
                        }

                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }

                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;


                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<DFM_Legal_ComplianceAssignedCategory_Result> CatagoryList = new List<DFM_Legal_ComplianceAssignedCategory_Result>();
                    CatagoryList = GetNewAll(customerid, distributorID);


                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (DFM_Legal_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'FH_summary')" +
                                "}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;


                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;



                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;


                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;



                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;

                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;




                    #region Previous Working
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_INL_Psummary')" +
                                         "},},},{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_INC_Psummary')" +


                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_AFL_Psummary')" +
                                         " },},},{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_AFC_Psummary')" +


                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_NCL_Psummary')" +
                                        " },},},{name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'P_NCC_Psummary')" +

                                        " },},}],}],},";
                    #endregion



                    if (1 == 2)
                    {
                        if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       
    }
}