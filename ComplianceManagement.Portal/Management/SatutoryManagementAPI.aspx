﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SatutoryManagementAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.SatutoryManagementAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;margin-right: 16px;">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min1.1.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <script src="../Scripts/KendoPage/StatutoryMGMT_v1.js"></script>

    <style type="text/css">
        span.k-icon.k-i-window-minimize {
    display: none;
}
/*.k-treeview .k-item {
    display: block;
    border-width: 0;
    margin: 0;
    padding: 0 0 0 0px;
}*/
        .k-grid-content {
            min-height:400px !important;
             overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-grouping-header {
            border-right: 1px solid;
            border-left: 1px solid;
            color: #515967;
           font-style: italic;
        }

        .k-button {
            margin-right: 5px;
            margin-top: 5px;
            border-radius: 4px;
            border-color: #ceced2;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #ceced2;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        /*.k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }*/

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: auto;
        }

        .k-grid-header {
            padding-right: 0px !important;
            /*margin-right: -17px;*/
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-grid-header th.k-with-icon .k-link {
            background-color: #F8F8F8;
        }

        .k-grid-header-wrap 
        {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 0px 0 0;
            zoom: 1;
        }

        form.k-filter-menu {
            width: 98% !important;
        }

        form.k-filter-menu .k-textbox 
        {
            width: 94%;
            margin-bottom: 3px;
        }

    </style>



    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        $(window).resize(function(){
            window.parent.forchild($("body").height()+15);  
        });
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
       
        function BindGrid() {
            
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(12)').remove();
                },
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetStatutoryManagementDashboardPages?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetStatutoryManagementDashboardPages?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                        
                        <%--read: '<% =Path%>/Data/GetStatutoryManagementDashboardPages?Uid=<% =UId%>&ptname=<% =pointname%>&attr=<% =attribute%>&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=<% =functionid%>&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=<% =Status%>&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val() --%>
                       <%-- read: '<% =Path%>/Data/GetStatutoryManagementDashboardPages?Uid=<% =UId%>&ptname='+pointname1+'&attr='+attribute1+'&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=<% =functionid%>&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                        --%>
                    },
                    schema: {
                        data: function (response) {
                          
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart;
                            }                          
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                    return response[0].DeptRiskBarChart;
                                }
                            
                            //return response[0].FunctionPieChart.length;
                        },
                        total: function (response) {
                           
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart.length;
                            }                                                     
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                    return response[0].DeptRiskBarChart.length;
                                }
                            
                            //return response[0].FunctionPieChart.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ShortForm", title: "Short&nbsp;Form" },
                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Performer", title: "Performer" },
                    { hidden: true, field: "Reviewer", title: "Reviewer" },                                
                    {
                        field: "Branch", title: 'Location',
                        width: "17%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act&nbsp;Name',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                    //    field: "ScheduledOn", title: 'Due&nbsp;Date',
                    //    type: "date",

                    //    template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    //    filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                        //},

                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",    
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }    
                    },        
                    {                        
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {                        
                        field: "Status", title: 'Status', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "edit5", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            { name: "edit2", text: "  ", iconClass: "k-icon k-i-eye", className: "ob-overview"  }
                        ], title: "Action", headerAttributes: {
                            style: "border-right: solid 1px #ceced2;" 
                        } 
                    }
                ]
            });

    $("#grid").kendoTooltip({                
        filter: "th",
        content: function (e) {
            var target = e.target; // element for which the tooltip is shown 
            return $(target).text();
        }
    });

    $("#grid").kendoTooltip({
        filter: "td",                
        content: function (e) {
            var target = e.target; // element for which the tooltip is shown 
            if ($(target).text() == "  ") {

                return "Action";
            }
            else
            {
                return $(target).text();
            }                   
        }
    }).data("kendoTooltip")

    $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
        return true;
    });

    $(document).on("click", "#grid tbody tr .ob-download", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        $('#downloadfile').attr('src', "../ComplianceDocument/DownloadMGMTDocAPI.aspx?ComplianceScheduleID=" + item.ScheduledOnID + "&IsFlag=-1");
        return true;
    });

    $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
        var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));           
        OpenDocumentOverviewpup(item.ScheduledOnID,item.ID)
        return true;
    });
            window.parent.forchild($("body").height()+15);  
}

function OpenDocumentOverviewpup(scheduledonid,transactionId) {           
    $('#divOverView').modal('show');
    $('#OverViews').attr('width', '1150px');
    $('#OverViews').attr('height', '600px');
    $('.modal-dialog').css('width', '1200px');
    $('#OverViews').attr('src', "../Common/DocumentOverviewAPI.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID="+transactionId+"&ISStatutoryflag=-1");
}

function onChangeSD() {
          
}
function onChangeLD() {
          
}

function BindGridApply(e) {    
    var setStartDate = $("#Startdatepicker").val();
    var setEndDate = $("#Lastdatepicker").val();

            
    BindGrid();
    //Added by Mrunali on 24 jan 2020
    FilterGridDemo();
                 
    if (setStartDate != null) {
        $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
    }
    if (setEndDate != null) {
        $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
    }

    //comment by rahul on 24 jan 2020
    //FilterGrid();

    e.preventDefault();
}
function FilterGridDemo()
{
   
     
    var Riskdetails = [];
    if($("#dropdownlistRisk").data("kendoDropDownTree") != undefined)
    {
        Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    }

    //status Details
    var Statusdetails = [];
    if($("#dropdownlistStatus").data("kendoDropDownTree") != undefined)
    {
        var Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;

        if(Statusdetails.length >= 1)
        {
            for (var i = 0; i < Statusdetails.length; i++) 
            { 
                if (Statusdetails[i] === "No" ||  Statusdetails[i] === "" ) {
                    Statusdetails.splice(i, 1); 
                }
            }                
        }
    }
            

    //user Details      
    var Userdetails = [];
    if($("#dropdownUser").data("kendoDropDownTree") != undefined)
    {
        Userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
    }


    //location details
    var locationsdetails = [];
    if($("#dropdowntree").data("kendoDropDownTree") != undefined)
    {
        locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
    }

    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
        });
    }

    var Departmentdetails = -1;
    if (DisplayDepartment.value > 0) 
    {
        Departmentdetails = DisplayDepartment.value;
    }

                                 
    var finalSelectedfilter = { logic: "and", filters: [] };

    if (Riskdetails.length > 0 || Statusdetails.length > 0 || locationsdetails.length > 0 || 
        Userdetails.length > 0 || datedetails.length > 0 ||
        ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) ||
        ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) ||
        ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") ||
        Departmentdetails > 0 ) 
    {
        if ( $("#dropdownSequence").val() != undefined  && $("#dropdownSequence").val() != "" && $("#dropdownSequence").val() != null)  
        {           
          
            var SeqFilter = { logic: "or", filters: [] };

            SeqFilter.filters.push({
                field: "SequenceID", operator: "eq", value:$("#dropdownSequence").val()
            });

            finalSelectedfilter.filters.push(SeqFilter);
        }

        if (Departmentdetails > 0) {
    
            var FunctionDepartmentFilter = { logic: "or", filters: [] };

            FunctionDepartmentFilter.filters.push({
                field: "DepartmentID", operator: "eq", value: parseInt(Departmentdetails)
            });
               
            finalSelectedfilter.filters.push(FunctionDepartmentFilter);
        }
        if (datedetails.length > 0) 
        {
            var DateFilter = { logic: "or", filters: [] };

            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
            {
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            finalSelectedfilter.filters.push(DateFilter);
        }
        if (Userdetails.length > 0) 
        {
            var UserFilter = { logic: "or", filters: [] };

            $.each(Userdetails, function (i, v) {
                UserFilter.filters.push({
                    field: "UserID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(UserFilter);
        }

        if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)  
        {
            var ActFilter = { logic: "or", filters: [] };

            ActFilter.filters.push({
                field: "ActID", operator: "eq", value:parseInt($("#dropdownACT").val())
            });

            finalSelectedfilter.filters.push(ActFilter);
        }
        if (locationsdetails.length > 0) 
        {
            var LocationFilter = { logic: "or", filters: [] };

            $.each(locationsdetails, function (i, v) {
                LocationFilter.filters.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(LocationFilter);
        }
        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) 
        {
            if ($("#dropdownfunction").val() != 0) {
    
                var FunctionFilter = { logic: "or", filters: [] };

                FunctionFilter.filters.push({
                    field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                });
               
                finalSelectedfilter.filters.push(FunctionFilter);
            }
        }
        if (Statusdetails.length > 0) 
        {
            var StatusFilter = { logic: "or", filters: [] };

            $.each(Statusdetails, function (i, v) 
            {
                StatusFilter.filters.push({
                    field: "FilterStatus", operator: "eq", value: v
                });                
            });

            finalSelectedfilter.filters.push(StatusFilter);
        }
    
        if (Riskdetails.length > 0) 
        {
            var CategoryFilter = { logic: "or", filters: [] };

            $.each(Riskdetails, function (i, v) {
               
                CategoryFilter.filters.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(CategoryFilter);
        }
        if (finalSelectedfilter.filters.length > 0) {
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.filter(finalSelectedfilter);
        }
        else {
            $("#grid").data("kendoGrid").dataSource.filter({});
        }
    }
    else 
    {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }


}
$(document).ready(function () {

    $("#Startdatepicker").kendoDatePicker({
        change: onChangeSD
    });
    $("#Lastdatepicker").kendoDatePicker({
        change: onChangeLD
    });

    $("#dropdownfunction").kendoDropDownList({
        filter: "startswith",
        autoClose: false,
        autoWidth: true,
        dataTextField: "Name",
        dataValueField: "Id",
        optionLabel: "Select Category",
        change: function (e) {
            BindGrid();
            FilterGridDemo();
            fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
        },
        dataSource: {
            severFiltering: true,
            transport: {
                read: {
                    url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                }
                //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT"
                    }
                }
            });

            $("#dropdownfunction").data("kendoDropDownList").value('<%=functionid%>');
       
            $("#dropdownfunction").val('<%=functionid%>'); 
            


            BindGrid();


            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                        fCreateStoryBoard('dropdownFY', 'filterFY', 'FY'); 
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },                             
                ]
            });

       
            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",                    
                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk'); 
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });            
           $("#dropdownlistRisk").data("kendoDropDownTree").value('<%= riskid%>');
            //$("#dropdownlistRisk").val('<%= riskid%>');
            
            
            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status'); 
                },
                dataSource: [
                    <%--    { text: "Overdue", value: "1" },
                    { text: "Pending For Review", value: "2" },
                    { text: "Rejected", value: "3" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "6" },
                    <%}%>
                    { text: "Closed Timely", value: "4" },
                    { text: "Closed Delayed", value: "5" }--%>

                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "NotComplied" },
                    <%}%>
                    { text: "Closed Timely", value: "ClosedTimely" },
                    { text: "Closed Delayed", value: "ClosedDelayed" },
                    { text: "In Progress", value: "InProgress" }
                ]
            });
     
            $("#dropdownlistStatus").data("kendoDropDownTree").value(["<%=StatusID%>", "<%=StatusID1%>", "<%=StatusID2%>", "<%=StatusID3%>"]);

       
            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });


    
            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');  
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>"
            }
        }, dataBound: function (e) {
            e.sender.list.width("900");
        }
    });
    

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
            }
        }, dataBound: function (e) {
            e.sender.list.width("900");
        }
    });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownUser', 'filterUser1', 'user');
  
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>"
            },                   
        }
    });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S"
            },
            schema: {
                data: function (response) {
                    return response[0].locationList;
                },
                model: {
                    children: "Children"
                }
            }
        }
    });

        });

  
function CloseClearPopup() {
    $('#APIOverView').attr('src', "../Common/blank.html");
}

function OpenOverViewpupMain(scheduledonid, instanceid) {
    $('#divApiOverView').modal('show');
    $('#APIOverView').attr('width', '1150px');
    $('#APIOverView').attr('height', '600px');
    $('.modal-dialog').css('width', '1200px');
    $('#APIOverView').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
}
function ClearAllFilterMain(e) {
    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
    $("#dropdownUser").data("kendoDropDownTree").value([]);
    $("#dropdownACT").data("kendoDropDownList").value([]);
    if ($("#dropdownSequence").data("kendoDropDownList") != undefined) {
        $("#dropdownSequence").data("kendoDropDownList").value([]);
    } 
    //$("#dropdownSequence").data("kendoDropDownList").value([]);
    $("#dropdownPastData").data("kendoDropDownList").value([]);
    $("#dropdownFY").data("kendoDropDownList").value([]);
    $("#dropdownfunction").data("kendoDropDownList").value([]);
    $('#ClearfilterMain').css('display', 'none');
    $("#grid").data("kendoGrid").dataSource.filter({});
    BindGrid();
    e.preventDefault();
}
        
    </script>

</head>
<body style="overflow-x: hidden;">
    <form>

        <div class="row">
            <div class="col-lg-12 col-md-12 colpadding0">
                <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px; padding-left: 5px; padding-top: 5px;">Compliance List</h1>

                <%-- <h1 style="height: 30px;background-color: #f8f8f8;height: 30px;background-color: #f8f8f8;margin-top: 0px;font-weight: 100;font-size: 25px;color: #666;font-weight: 500;margin-bottom: 12px;">Compliance List </h1>--%>
                <h1 id="display" runat="server" style="display: none; margin-top: 2px; font-size: 20px; font-weight: 100; font-size: 17px; color: #666; font-weight: 500; margin-bottom: 17px;"></h1>
            </div>
        </div>

        <div id="example">
            <div class="row">
                <div class="toolbar">
                    <div class="row" style="margin-bottom: -7px; margin-top: -6px;">
                        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 290px; padding-right: 7px;" />
                        <input id="dropdownFY" style="width: 150px;" />
                        <input id="dropdownlistStatus" data-placeholder="Status" style="width: 195px; padding-left: 7px;" />
                        <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 191px; padding-left: 7px; padding-right: 7px;" />
                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 140px; margin-right: 7px;" />
                        <input id="Lastdatepicker" placeholder="End Date" cssclass="clsROWgrid" title="Lastdatepicker" style="width: 140px;" />
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 12px; padding-bottom: 6px;" />
            <input id="CustName" type="hidden" value="<% =CustomerName%>" />
            <input id="DisplayDepartment" type="hidden" value="<% =Departmentid%>" />
            <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
            <input id="dropdownACT" style="width: 290px; float: left; margin-right:0.9%;" />
            <input id="dropdownPastData" style="width: 150px; float: left; margin-right:0.9%;" />
            <input id="dropdownfunction" style="width: 195px; float: left;  margin-right:0.9%;" />
            <input id="dropdownUser" data-placeholder="User" style="width: 191px; float: left;  margin-right:0.5%;" />



<button id="export" onclick="exportReport(event)" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png);  background-repeat: no-repeat; width: 35px; height: 30px; margin-top: -1px; background-color: white; border: none; float:left;" data-toggle="tooltip" title="Export to Excel"></button>


            <button id="Applyfilter" style="float: left;height: 29px;padding-bottom: 3px; margin-right: 0.9%; margin-left: 0%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
            <button id="ClearfilterMain" style="float: left; margin-right: 1px;height: 29px;padding-bottom: 3px;  display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
        </div>
            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
            <%{%>
            <div class="row" style="padding-top: 0px; padding-bottom: 7px;">
                <input id="dropdownSequence" style="width: 355px; margin-right: 7px;" />
            </div>
            <%}%>
<div class="clearfix"></div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: 1px;" id="filterrisk">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: -1px;" id="filterstatus">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; color: #535b6a; margin-top: -1px;" id="filterUser1">&nbsp;</div>
        <div id="grid" style="border: none;"></div>



        <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
    </form>
</body>
</html>


