﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MgmtDashboardDeptHead.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.MgmtDashboardDeptHead" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="AVANTIS - Products that simplify"/>
        <meta name="author" content="AVANTIS - Development Team"/>

        <title>DASHBOARD :: AVANTIS - Products that simplify</title>
        <!-- Offline-->
        <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
        <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
        <script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>
        <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
        <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
        <link href="../newcss/spectrum.css" rel="stylesheet" />
        <script type="text/javascript" src="../newjs/spectrum.js"></script>

            <%--<script type="text/javascript" src="../tree/jquery-3.3.1.slim.min.js"></script>--%> 
        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
        <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
        <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

        <style type="text/css">

             /*dailyupdates*/
          
        .tags > * {
                display: inline-block;
                list-style: none;
            }
            .card-text-sm {
                margin-bottom: -1rem !important;
                -webkit-margin-before: -30px;
                display: block;
                margin-block-start: 0;
                margin-block-end: 0;
                margin-inline-start: 0;
                margin-inline-end: 0;
                font-weight: bold;
            }
            .tags {
                margin: 0;
                padding: 0;
                margin-bottom: 0;
            }
            .h-100 {
                height: 100% !important;
            }
            .text-black-50 {
                font-size: 10px;
                color: rgba(0,0,0,.5) !important;
                margin-top: 15px;
                font-weight: 420;
            }
            .box {
                border: 1px solid transparent;
                padding: 10px;
                border-radius: 5px;
                background-color: #FFFFFF;
                box-shadow: 0 7px 20px 0 rgba(210,210,210,0.5);
            }
            .a-tag {
                color: #37A8F1;
                background-color: rgba(55, 168, 241, 0.20);
                font-size: 12px;
                font-weight: bold;
                padding: 6px 10px 7px;
                height: 28px;
                border-radius: 14px;
                display: inline-block;
                margin: 0 4px 10px;
                max-width: 130px;
                min-width: 104px;

                width: 100px;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
                text-align: center;
            }
            /*end dailyupdates*/ 
            #dailyupdates .bx-viewport {
                height: 326px !important;
            }

            .info-box:hover {
                color: #FF7473;
                font-weight: 500;
                -webkit-transform: scale(1.1);
                -ms-transform: scale(1.1);
                transform: scale(1.1);
                z-index: 5;
            }

            .function-selector-radio-label {
                font-size: 12px;
            }

            .colorPickerWidget {
                padding: 10px;
                margin: 10px;
                text-align: center;
                width: 360px;
                border-radius: 5px;
                background: #fafafa;
                border: 2px solid #ddd;
            }

            #ContentPlaceHolder1_grdGradingRepportSummary.table tr td {
                border: 1px solid white;
            }

            .TMB {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 16.5% !important;
            }

            .TMB1 {
                padding-left: 0px !important;
                padding-right: 0px !important;
                width: 19.5% !important;
            }

            .TMBImg {
                padding-left: 0px !important;
                padding-right: 0px !important;
                margin-left: -7%;
                margin-right: -3%;
            }

            .clscircle {
                margin-right: 7px !important;
            }

            .fixwidth {
                width: 20% !important;
            }

            .badge {
                font-size: 10px !important;
                font-weight: 200 !important;
            }

            .responsive-calendar .day {
                width: 13.7% !important;
                height: 45px;
            }

                .responsive-calendar .day.cal-header {
                    border-bottom: none !important;
                    width: 13.9% !important;
                    font-size: 17px;
                    height: 25px;
                }

            #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
                left: 0%;
            }

            .bx-viewport {
                height: 285px !important;
                margin-left: -41px;
                width: 110% !important;
            }

            /*#dailyupdates .bx-viewport {
                height: 190px !important;
            }*/

            .graphcmp {
                margin-left: 36%;
                font-size: 16px;
                margin-top: -3%;
                color: #666666;
                font-family: 'Roboto';
            }

            .days > div.day {
                margin: 1px;
                background: #eee;
            }

            .overdue ~ div > a {
                background: red;
            }

            .info-box {
                min-height: 95px !important;
            }

            .Dashboard-white-widget {
                padding: 5px 10px 0px !important;
            }

            .dashboardProgressbar {
                display: none;
            }

            .TMBImg > img {
                width: 47px;
            }

            #reviewersummary {
                height: 150px;
            }

            #performersummary {
                height: 150px;
            }

            #eventownersummary {
                height: 150px;
            }

            #performersummarytask {
                height: 150px;
            }

            #reviewersummarytask {
                height: 150px;
            }

            div.panel {
                margin-bottom: 12px;
            }

            .panel .panel-heading .panel-actions {
                height: 25px !important;
            }

            hr {
                margin-bottom: 8px;
            }

            .panel .panel-heading h2 {
                font-size: 18px;
            }

            td > label {
                padding: 6px;
            }

            .radioboxlist radioboxlistStyle {
                font-size: x-large;
                padding-right: 20px;
            }

            span.input-group-addon {
                padding: 0px;
            }

            td > label {
                padding: 3px 4px 0 4px;
                margin-top: -1%;
            }

            .nav-tabs > li > a {
                color: #333 !important;
            }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #1fd9e1 !important;
            }
        </style>

        <script type="text/javascript">

            function fComplianceOverview(obj) {
                OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'), $(obj).attr('Ctype'));
            }

            function OpenOverViewpup(scheduledonid, instanceid) {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1150px');
                $('#OverViews').attr('height', '600px');
                $('.modal-dialog').css('width', '1200px');
                $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

            }

            function initializeDatePickerTopStartdate(TopStartdate) 
            {
                var startDate = new Date();
                $('#<%= txtAdvStartDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true
                });

                if (TopStartdate != null) 
                {
                    $("#<%= txtAdvStartDate.ClientID %>").datepicker("option", "defaultDate", TopStartdate);
                }
            }

            function initializeDatePickerTopEnddate(TopEnddate) 
            {
                var startDate = new Date();
                $('#<%= txtAdvEndDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true
                });

                if (TopEnddate != null) 
                {
                    $("#<%= txtAdvEndDate.ClientID %>").datepicker("option", "defaultDate", TopEnddate);
                }
            }

            function initializeDatePickerFunctionStartDate(FunctionStartDate) 
            {
                var startDate = new Date();
                $('#<%= txtFunctionStartDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,                   
                });

                if (FunctionStartDate != null) 
                {
                    $("#<%= txtFunctionStartDate.ClientID %>").datepicker("option", "defaultDate", FunctionStartDate);
                }
            }

            function initializeDatePickerFunctionEndDate(FunctionEndDate) 
            {
                var startDate = new Date();
                $('#<%= txtFunctionEndDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,                   
                });

                if (FunctionEndDate != null) 
                {
                    $("#<%= txtFunctionEndDate.ClientID %>").datepicker("option", "defaultDate", FunctionEndDate);
                }
            }

            function initializeDatePickerRiskStartDate(RiskStartDate) 
            {
                var startDate = new Date();
                $('#<%= txtRiskStartDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true
                });

                if (RiskStartDate != null) 
                {
                    $("#<%= txtRiskStartDate.ClientID %>").datepicker("option", "defaultDate", RiskStartDate);
                }
            }

            function initializeDatePickerRiskEndDate(RiskEndDate) 
            {
                var startDate = new Date();
                $('#<%= txtRiskEndDate.ClientID %>').datepicker({
                    dateFormat: 'dd-mm-yy',
                    maxDate: $('#<%=  txtAdvEndDate.ClientID  %>').val(),
                    minDate: $('#<%=  txtAdvStartDate.ClientID  %>').val(),
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true
                });

                if (RiskEndDate != null) 
                {
                    $("#<%= txtRiskEndDate.ClientID %>").datepicker("option", "defaultDate", RiskEndDate);
                }
            }

            function complianceSummaryDetail() {
                if ($('#ContentPlaceHolder1_ddlStatus').val() == 0) {
                    $('#complianceframe').attr('src', '../Management/StautoryComplianceMgmtDeptHeadSummary.aspx');
                    $("#lnkOverdueCompliance").show();
                    $("#lnkInternalOverdueCompliance").hide();
                }
                else {
                    $('#complianceframe').attr('src', '../Management/InternalComplianceMgmtDeptHeadSummary.aspx');
                    $("#lnkInternalOverdueCompliance").show();
                    $("#lnkOverdueCompliance").hide();
                }
                return;
            }

            function OpenOverdueComplainceList(e) {
                $('#divreports').modal('show');
                $('#showdetails').attr('width', '1150px');
                $('#showdetails').attr('height', '680px');
                $('.modal-dialog').css('width', '1200px');
                $('#showdetails').attr('src', "../Management/OverdueComplianceDeptAPI.aspx?IsDeptHead=1&RiskId=0");
                e.preventDefault();
                
            }

            function OpenInternalOverdueComplainceList(e) {
                $('#divreports').modal('show');
                $('#showdetails').attr('width', '1150px');
                $('#showdetails').attr('height', '680px');
                $('.modal-dialog').css('width', '1200px');
                $('#showdetails').attr('src', "../Management/InternalOverdueComplianceDeptAPI.aspx?IsDeptHead=1&RiskId=0");
                e.preventDefault();
            }

        </script>
        <script type="text/javascript">
            $(document).ready(function () {   
                complianceSummaryDetail();

                $('#basic').simpleTreeTable({                
                    collapsed: true
                });                       
            });
        </script>
        <style type="text/css">
            table#basic > tr, td {
                border-radius: 5px;
            }

            table#basic {
                border-collapse: unset;
                border-spacing: 3px;
            }

            .locationheadbg {
                background-color: #999;
                color: #fff;
                border: #666;
            }

            .locationheadLocationbg {
                background-color: #fff;
                border: 1px solid #e0e0e0;
            }

            td.locationheadLocationbg > span.tree-icon {
                background-color: #1976d2 !important;
                padding-right: 12px;
                color: white;
            }

            .GradingRating1 {
                background-color: #8fc156;
                border: 1px solid #4a8407;
            }

            .GradingRating2 {
                background-color: #ffc107;
                border: 1px solid #ba8b00;
            }

            .GradingRating3 {
                background-color: #ef9a9a;
                border: 1px solid #b65454;
            }

            .GradingRating4 {
                background-color: #e6e6e6;
                border: 1px solid #c4c4c4;
            }
        
        </style>
    </head>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- container section start -->
    <section id="container" class="">

            <header class="panel-heading tab-bg-primary " style="background: #eee;">
               <% if (roles != null && (roles.Contains(3) || roles.Contains(4) || roles.Contains(10)))
                   {%>
                        <ul id="rblRole1" class="nav nav-tabs" >                                         
                        <li class="active" id="li1" runat="server" style="padding-left: 5px;padding-right: 6px;margin-right: 6px!important;">
                        <asp:LinkButton ID="lnkManagmentDashboard" PostBackUrl="~/Management/MgmtDashboardDeptHead.aspx" runat="server" Style="font-size: 16px;line-height: 1.0;">Department Head</asp:LinkButton>                                           
                       </li>                     
                        <li class=""  id="li2" runat="server" style="padding-left: 5px;padding-right: 6px;">
                            <asp:LinkButton ID="lnkPerformerReviewer" PostBackUrl="~/common/Dashboard.aspx" Style="font-size: 16px;line-height: 1.0;" runat="server">Performer/Reviewer</asp:LinkButton>                                        
                        </li>
                        </ul>
              
          </header>
          <div style="clear:both;height: 9px;"></div>
           <%}%>
        </section>
    
   <!-- Top Count start -->
    <div id="divTabs" runat="server" visible="false" class="row">

        <div id="entitycount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box indigo-bg">
                <div class="div-location" onclick="fEntity('<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/manager-ico-location.png" alt="No Image" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Entities</div>
                        <div id="divEntitesCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="locationcount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box seablue-bg">
                <div class="div-location" style="cursor: pointer" onclick="PopulateGraphdata('<%=IsApprover%>')">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/manager-ico-location.png" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Locations</div>
                        <div id="divLocationCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="functioncount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box bluenew-bg">
                <div class="div-location" style="cursor: pointer" onclick="fFunctions(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/manager-ico-functions.png" alt="No Image" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Departments</div>
                        <div id="divFunctionCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="compliancecount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box rednew-bg">
                <div class="div-location" style="cursor: pointer" onclick="fCompliances(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/manager-ico-compliances.png" />
                    </div>
                    <div class="col-md-7 colpadding0" style="margin-left: -4%;">
                        <div class="titleMD" style="text-align: left; font-size: 17px;">Compliances</div>
                        <div id="divCompliancesCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="usercount" runat="server" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box greennew-bg">
                <div class="div-location" onclick="fusers(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/manager-ico-users.png" alt="No Image"  />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Users</div>
                        <div id="divUsersCount" runat="server" class="countMD" style="text-align: left">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
        <!--/.col-->

        <div id="penaltycount" runat="server" visible="false" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB">
            <div class="info-box amber-bg">
                <div class="div-location" onclick="fPenaltyDetails(<%=customerID%>,<%=BID %>,'<%=IsSatutoryInternal%>','<%=IsApprover%>')" style="cursor: pointer;">
                    <div class="col-md-5 TMBImg">
                        <img src="../img/RupeesSymbols.png" alt="No Image" />
                    </div>
                    <div class="col-md-7 colpadding0">
                        <div class="titleMD" style="text-align: left">Penalty</div>
                        <div id="divPenalty" runat="server" class="countMD" style="text-align: left; font-size: 30px;">0</div>
                    </div>
                </div>
            </div>
            <!--/.info-box-->
        </div>
    </div>
   <!-- Top Count End -->

    <div class="clearfix"></div>
    <!-- Overdue start -->
    <div id="DivOverDueCompliance" runat="server" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivOverDueCompliance" style="height: 269px;">
                            <h2 style="margin-left: 8px;">Summary of Overdue Compliances</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivOverDueCompliance"><i class="fa fa-chevron-down"></i></a>
                            <div style="float: right;">
                                <button id="lnkOverdueCompliance" onclick="OpenOverdueComplainceList(event)" title="Maximize"><i class="fa fa-window-maximize"></i></button>
                                <button id="lnkInternalOverdueCompliance" onclick="OpenInternalOverdueComplainceList(event)" style="display:none;"  title="Maximize"><i class="fa fa-window-maximize"></i></button>
                        </div>
                        </div>
                    </div>

                    <div id="collapseDivOverDueCompliance" runat="server">
                        <div id="internalcompliance">
                            <iframe id="complianceframe" src="about:blank" scrolling="no" frameborder="0" width="100%" height="130px"></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Overdue End -->

    <div class="clearfix"></div>
    <!-- Filter start -->
    <div id="DivFilters" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-bottom: 5px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivFilters">
                            <h2>Filters </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                 href="#ContentPlaceHolder1_collapseDivFilters"><i class="fa fa-chevron-down"></i></a>
                            <a href="javascript:closeDiv('DivFilters')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapseDivFilters" class="panel-collapse collapse in" runat="server">
                        <div class="col-md-12" style="padding-left: 0px;">
                            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 11%;">
                                <asp:DropDownList runat="server" ID="ddlStatus" Width="100%" Height="33px" class="form-control m-bot15 select_Date">
                                    <asp:ListItem Text="Statutory" Value="0" Selected="True" />
                                    <asp:ListItem Text="Internal" Value="1" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3" style="padding-left: 0px; padding-right: 0px; width: 25%;">
                                <div class="col-md-2 input-group date" style="padding-left: 7px; padding-right: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 3px !important;color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="33px" placeholder="Start Date" Width="89px"
                                        Style="padding-left: 5px;" class="form-control m-bot15 select_Date" ID="txtAdvStartDate" />
                                </div>
                                <div class="col-md-2 input-group date" style="padding-left: 7px; padding-right: 0px;">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" style="padding: 3px !important;color: black;"></span>
                                    </span>
                                    <asp:TextBox runat="server" Height="33px" placeholder="End Date" Width="89px"
                                       Style="padding-left: 5px; margin-left: 0px;"
                                        class="form-control m-bot15 select_Date" ID="txtAdvEndDate" />  
                                </div>
                            </div>

                            <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="padding-left: 0px; padding-right: 0px; width: 24%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" CssClass="txtbox" autocomplete="off" />
                                        <div style="margin-left: 1px; display:none; position: absolute; z-index: 10" id="divFilterLocation">
                                            <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="239px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="col-md-3" style="margin-left: -7px; width: 36%;">
                                <fieldset runat="server" id="fldrbFinancialYearFunctionSummery" style="border-style: solid; border-radius: 5px; border-width: 1px; border-color: #c7c7cc; width: 100%; height: 33px;">
                                    <asp:UpdatePanel ID="uprdo" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncontainer" style="margin-top: -1%;">
                                                <asp:RadioButtonList ID="rbFinancialYearFunctionSummery" runat="server" CssClass="radioboxlist"
                                                    RepeatDirection="Horizontal" AutoPostBack="true" Width="303px"
                                                    OnSelectedIndexChanged="rbFinancialYearFunctionSummery_SelectedIndexChanged">
                                                    <asp:ListItem Text="Current YTD" Value="0" Selected="True"  title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Current Financial Year to date"/>
                                                    <asp:ListItem Text="Current + Previous YTD" Value="1"  title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Current + Previous Financial Year to date"/>
                                                    <asp:ListItem Text="All" Value="2" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="All Financial Year to date" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rbFinancialYearFunctionSummery" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <div style="clear:both;height:5px"></div>
                                <div style="float:left;color: #666666;">YTD: Year to Date</div>
                            </div>

                            <div class="col-md-1" style="margin-left: 0.5%; width: 3%;">
                                <asp:Button ID="btnTopSearch" class="btn btn-search" runat="server" Text="Apply" OnClick="btnTopSearch_Click"></asp:Button>
                            </div>

                        </div>
                        <div class="col-md-12" style="float: left; margin-bottom: 10px; padding-left: 0px;margin-top: -14px;">                            
                                <h5>Select Your Risk Color Preference</h5>                            
                                <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                    <input id="High" style="display: none;">
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <h5 class="colpadding0">High</h5>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                    <input id="medium" style="display: none;"/>
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <h5 class="colpadding0">Medium</h5>
                                </div>

                                <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                    <input id="low" style="display: none;"/>
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <h5 class="colpadding0">Low</h5>

                                </div >
                            
                                <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                    <input id="critical" style="display: none;">
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <h5 class="colpadding0">Critical</h5>
                                </div>


                               
                            
                             <div class="col-md-5 colpadding0">                                                                                                  
                                </div>
                             <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                 
                                 </div>

                         <%--   
                            --%>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Filter End -->

    <!-- Functions Summary start -->
    <div id="FunctionSummary" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12" style="padding-left: 5px; padding-right: 5px;">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFunctionSummary">
                            <h2>Performance Summary </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseFunctionSummary"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('FunctionSummary')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <div id="collapseFunctionSummary" class="panel-collapse collapse in">
                        <fieldset style="display: none; border-style: solid; border-radius: 10px; border-width: 1px; border-color: #dddddd; width: 100%; height: 80px; margin-top: 10px; padding: 22px;">
                            <div class="col-md-2 input-group date" style="/* margin-left: -30px; */padding: 0; width: 15%;">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox runat="server" Height="31px" placeholder="Start Date" Width="100px"
                                    Style="padding-left: 8px; margin-left: 0px;" class="form-control m-bot15 select_Date" ID="txtFunctionStartDate" />
                            </div>
                            <div class="col-md-2 input-group date" style="float: left; padding: 0; width: 15%;">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox runat="server" Height="31px" placeholder="End Date" Width="100px" Style="padding-left: 5px; margin-left: 0px;"
                                    class="form-control m-bot15 select_Date" ID="txtFunctionEndDate" />

                            </div>

                            <div class="col-md-4" style="float: left; padding: 0; width: 33%;">
                            </div>

                            <asp:UpdatePanel ID="upDivLocationFuction" runat="server" UpdateMode="Conditional" OnLoad="upDivLocationFunction_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="width: 28%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocationFunction" Style="padding: 0px; padding-left: 10px; /* margin: 16px 0px; */height: 31px; width: 100%; /* min-width: 250px; */border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93;"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 1px; position: absolute; z-index: 10" id="divFilterLocationFunction">
                                            <asp:TreeView runat="server" ID="tvFilterLocationFunction" SelectedNodeStyle-Font-Bold="true" Width="332px" NodeStyle-ForeColor="#8e8e93"
                                                Style="margin: -16px 0px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocationFunction_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="col-md-1" style="padding: 0;">
                                <div class="col-md-12">
                                    <asp:Button ID="btnFunctionSearch" class="btn btn-search" runat="server" Text="Apply" OnClientClick="fposition()" OnClick="btnFunctionSearch_Click"></asp:Button>
                                </div>
                            </div>
                        </fieldset>

                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="perStatusPieChartDiv" class="col-md-5" style="margin-left: -30px; width: 36.6%">
                                </div>
                                <div id="perFunctionChartDiv" class="col-md-7" style="margin-left: -8px; width: 65.3%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Functions Summary End -->

    <!-- Functions Risk Criteria start -->
    <div id="RiskCriteria" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseRiskCriteria">
                            <h2>Risk Summary </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseRiskCriteria"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('RiskCriteria')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapseRiskCriteria" class="panel-collapse collapse in">
                        <fieldset style="display: none; border-style: solid; border-radius: 10px; border-width: 1px; border-color: #dddddd; width: 100%; height: 80px; margin-top: 10px; padding: 22px;">
                            <div class="col-md-2 input-group date" style="/* margin-left: -30px; */padding: 0; width: 15%;">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox runat="server" Height="31px" placeholder="Start Date" Width="100px"
                                    Style="padding-left: 8px; margin-left: 0px;" class="form-control m-bot15 select_Date" ID="txtRiskStartDate" />
                            </div>
                            <div class="col-md-2 input-group date" style="float: left; padding: 0; width: 15%;">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                                </span>
                                <asp:TextBox runat="server" Height="31px" placeholder="End Date" Width="100px" Style="padding-left: 5px; margin-left: 0px;"
                                    class="form-control m-bot15 select_Date" ID="txtRiskEndDate" />

                            </div>

                            <div class="col-md-4" style="float: left; padding: 0; width: 33%;">
                                <fieldset runat="server" id="fldrbFinancialYearRiskSummery" style="border-style: solid; border-radius: 5px; border-width: 1px; border-color: #c7c7cc; width: 100%; height: 31px;">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncontainer" style="margin-top: -1%;">
                                                <asp:RadioButtonList ID="rbFinancialYearRiskSummery" runat="server" CssClass="radioboxlist" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbFinancialYearRiskSummery_SelectedIndexChanged">
                                                    <asp:ListItem Text="Current FY" Value="0" Selected="True" />
                                                    <asp:ListItem Text="Current + Previous FY" Value="1" />
                                                    <asp:ListItem Text="All" Value="2" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rbFinancialYearRiskSummery" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </fieldset>
                            </div>

                            <asp:UpdatePanel ID="upDivLocationRisk" runat="server" UpdateMode="Conditional" OnLoad="upDivLocationRisk_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="width: 28%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocationRisk" Style="padding: 0px; padding-left: 10px; /* margin: 16px 0px; */height: 31px; width: 100%; /* min-width: 250px; */border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93;"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 1px; position: absolute; z-index: 10" id="divFilterLocationRisk">
                                            <asp:TreeView runat="server" ID="tvFilterLocationRisk" SelectedNodeStyle-Font-Bold="true" Width="332px" NodeStyle-ForeColor="#8e8e93"
                                                Style="margin: -16px 0px; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocationRisk_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="col-md-1" style="padding: 0;">
                                <div class="col-md-12">
                                    <asp:Button ID="btnRiskSearch" class="btn btn-search" runat="server" Text="Apply" OnClientClick="fposition()" OnClick="btnRiskSearch_Click"></asp:Button>
                                </div>
                            </div>
                        </fieldset>

                        <div style="clear: both; height: 2px;"></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div id="perRiskStackedColumnChartDiv" class="col-md-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Functions Risk Criteria end -->

    <div id="PenaltyCriteria" runat="server" visible="false" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePenalty">
                            <h2>Penalty Summary </h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePenalty"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('Penalty')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapsePenalty" class="panel-collapse collapse in">
                        <div class="col-md-12" style="margin-top: 10px;">

                            <asp:UpdatePanel ID="upDivLocationPenalty" runat="server" UpdateMode="Conditional" OnLoad="upDivLocationPenalty_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="padding-left: 0px; margin-left: 0px; width: 36%;">
                                        <asp:TextBox runat="server" ID="tbxFilterLocationPenalty" AutoCompleteType="None" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 1px; position: absolute; z-index: 10" id="divFilterLocationPenalty">
                                            <asp:TreeView runat="server" ID="tvFilterLocationPenalty" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="tvFilterLocationPenalty_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="col-md-4" style="padding-left: 4px;">
                                <div class="col-md-2" style="width: 37%;">
                                    <asp:DropDownList runat="server" ID="ddlFinancialYear" Width="150%" class="form-control m-bot15 select_Date">
                                        <asp:ListItem Text="2015-2016" />
                                        <asp:ListItem Text="2016-2017" />
                                        <asp:ListItem Text="2017-2018" />
                                        <asp:ListItem Text="2018-2019" />
                                        <asp:ListItem Text="2019-2020"   />
                                        <asp:ListItem Text="2020-2021" Selected="True" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-left: -1%;">
                                <asp:Button ID="btnSearchPenalty" class="btn btn-search" Style="margin-left: 257px;" runat="server" Text="Apply" OnClientClick="fposition()" OnClick="btnSearchPenalty_Click"></asp:Button>
                            </div>
                            <div class="col-md-4">
                                &nbsp;
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div id="perPenaltyStackedColumnChartDiv" class="col-md-12">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
    <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
    <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1" />
    <asp:HiddenField ID="criticalcolor" runat="server" Value="#CC0900" />

    <!-- Functions Grading start -->
    <div id="compliancesummary" class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="margin-left: 10px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsecompliancesummary">
                            <h2>Grading Reports</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsecompliancesummary"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('compliancesummary')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div id="collapsecompliancesummary" class="panel-collapse collapse in">
                        <div class="col-md-12" style="margin-top: 10px;">
                            <asp:UpdatePanel ID="upateGradingReport" runat="server" UpdateMode="Conditional" OnLoad="upateGradingReport_Load">
                                <ContentTemplate>
                                    <div class="col-md-3" style="padding-left: 0px; margin-left: 0px; width: 36%;">
                                        <asp:TextBox runat="server" ID="TbxFilterLocationGridding" AutoCompleteType="None" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                            CssClass="txtbox" />
                                        <div style="margin-left: 1px; position: absolute; z-index: 10" id="divFilterLocationGradding">
                                            <asp:TreeView runat="server" ID="TreeGraddingReport" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                                OnSelectedNodeChanged="TreeGraddingReport_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="col-md-2" style="padding-left: 0px; margin-left: 0px;">
                                <asp:DropDownList runat="server" ID="ddlYearGrading" class="form-control m-bot15 select-location-dropdown" OnSelectedIndexChanged="ddlYearGrading_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <div class="col-md-0">
                                    <asp:DropDownList runat="server" ID="ddlmonthsGrading" class="form-control m-bot15 select_Date">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-0">
                                    <asp:DropDownList runat="server" ID="ddlPeriodGrading" class="form-control m-bot15 select_Date">
                                        <asp:ListItem Value="12" Text="12 Months" />
                                        <asp:ListItem Value="6" Text="06 Months" />
                                        <asp:ListItem Value="3" Text="03 Months" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <asp:Button ID="btnGradingSearch" class="btn btn-search" Style="margin-left: 90px;" runat="server" Text="Apply" OnClientClick="fposition()" OnClick="btnGradingSearch_Click"></asp:Button>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>

                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div style="width: 100%; margin-top: 15px;">
                                        <%=perDeptHead%>
                                    </div>
                                    <div style="width: 100%; margin-top: 15px;">
                                        <div style="background-color: <%=lowcolor.Value%>; width: 50px; height: 20px; float: left" data-toggle="tooltip" data-placement="bottom" title="Indicates 100% High Risk and Medium Risk compliances are completed in time and atleast 75% Low Risk compliances are completed in time."></div>
                                        <div style="background-color: <%=mediumcolor.Value%>; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Indicates 75% High Risk compliances are completed in time and atleast 50% Medium Risk and Low Risk compliances are completed in time."></div>
                                        <div style="background-color: <%=highcolor.Value%>; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Otherwise red."></div>
                                        <div style="background-color: #deface; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="No Schedule for this month or Not Applicable."></div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <%--<div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table style="margin-top: 20px;" width="100%">
                                        <tr>
                                            <th class="ui-widget-header" style="display: none;">
                                                <asp:Label ID="lblTableTitel" runat="server" Text="Past 12 Months's Summary"></asp:Label>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width: 99%; margin-top: 15px;">

                                                    <asp:GridView runat="server" ID="grdGradingRepportSummary" AutoGenerateColumns="true"
                                                        AllowSorting="false" OnRowDataBound="grdGradingRepportSummary_RowDataBound"
                                                        AllowPaging="false" CellSpacing="5" GridLines="none" CssClass="table" Width="100%">

                                                        <RowStyle CssClass="clsROWgrid" />
                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                                        <EmptyDataTemplate>
                                                            No Records Found.
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>


                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="20" style="">
                                                <table>
                                                    <tr>
                                                        <td>

                                                            <div style="background-color: #8fc156; border: 1px solid #4a8407; width: 50px; height: 20px; float: left" data-toggle="tooltip" data-placement="bottom" title="Indicates 100% High Risk and Medium Risk compliances are completed in time and atleast 75% Low Risk compliances are completed in time."></div>
                                                            <div style="background-color: #ffc107; border: 1px solid #ba8b00; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Indicates 75% High Risk compliances are completed in time and atleast 50% Medium Risk and Low Risk compliances are completed in time."></div>
                                                            <div style="background-color:#ef9a9a; border: 1px solid #b65454; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="Otherwise red."></div>
                                                            <div style="background-color:#e6e6e6; border: 1px solid #c4c4c4; width: 50px; margin-left: 10px; height: 20px; float: left;" data-toggle="tooltip" data-placement="bottom" title="No Schedule for this month or Not Applicable."></div>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Functions Grading end -->

    <!-- Calender start -->
    <div id="ComplianceCalender" runat="server" class="row Dashboard-white-widget" style="margin-top: 10px;">
        <div class="dashboard">

            <div class="col-lg-12 col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerCalender">
                            <h2>My Compliance Calendar</h2>
                        </a>
                        
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapsePerformerCalender"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('ComplianceCalender')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>                                           
                </div>
                <div id="collapsePerformerCalender" class="panel-collapse collapse in">
                    <div class="row">                         
                        <div class="col-md-5 colpadding0">
                            <div  style="float:left;width:90%;">
                                <fieldset runat="server" id="fldsCalender" style="border-style: solid; border-radius: 5px; border-width: 1px; border-color: #c7c7cc; width: 100%; height: 33px;">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="radiobuttoncalendercontainer">
                                                <asp:RadioButtonList ID="rdbcalender" runat="server" CssClass="radioboxcalenderlist"
                                                    RepeatDirection="Horizontal" AutoPostBack="false" Width="100%"
                                                    OnSelectedIndexChanged="rdbcalender_SelectedIndexChanged">
                                                    <asp:ListItem Text="Statutory + Internal " Value="0" Selected="True" />
                                                    <asp:ListItem Text="All(Including Checklist)" Value="1" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rdbcalender" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </fieldset>
                            </div>       
                            <div style="clear:both;height:5px;"></div>     
                            <!-- Responsive calendar - START -->
                            <div class="responsive-calendar" style="width: 95%">
                                <img src="../images/processing.gif">
                            </div>
                            <!-- Responsive calendar - END -->
                        </div>
                        <div class="col-md-7">
                            <div>
                                <span style="float: left; height: 20px; font-family: 'Roboto',sans-serif; color: #666; font-size: 16px;" id="clsdatel"></span>
                                <br />
                                <i style="font-family: 'Roboto',sans-serif; color: #666;">Select a date from calendar to view details</i>
                            </div>
                            <div class="clearfix" style="height: 0px;"></div>
                            <div id="datacal">
                                <img src="../images/processing.gif" id="imgcaldate" style="position: absolute;">
                                <iframe id="calframe" src="about:blank" scrolling="no" frameborder="0" width="100%" height="350px"></iframe>
                            </div>
                        </div>                      
                    </div>
                </div>
            </div>
              <div class="clearfix" style="height: 10px"></div>
        </div>
    </div>
    <!-- Calender End -->

    <!-- Daily Updates - START -->
    <div class="row Dashboard-white-widget" id="dailyupdates">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates">
                            <h2>Daily Updates</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDailyUpdates"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('dailyupdates')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div id="collapseDailyUpdates" class="panel-collapse collapse in">
                    <div class="row dailyupdates">
                        <ul id="DailyUpdatesrow" class="bxslider bxdaily" style="width: 100%; max-width: none;">
                        </ul>
                    </div>
                    <!--/.row-->
                    <div class="clearfix" style="height: 10px"></div>
                </div>
                <div class="modal fade" id="NewsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 900px">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                            </div>
                            <div class="modal-body" id="dailyupdatecontent">
                                <h2 id="dailyupdatedate" style="margin-top: 1px;">Daily Updates: November 3, 2016</h2>
                                <h3 id="dailyupdatedateInner">Daily Updates: November 3,</h3>
                                <p id="dailytitle"></p>
                                <div id="contents" style="color: #666666;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-search" style="float: right;" href="../Users/DailyUpdateList.aspx" title="View">View All</a>
                    <div class="clearfix" style="height: 50px"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Daily Updates - End -->

    <!-- News Letter - START -->
    <div class="row Dashboard-white-widget" id="newsletter">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter">
                            <h2>News Letter</h2>
                        </a>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseNewsletter"><i class="fa fa-chevron-up"></i></a>
                            <a href="javascript:closeDiv('newsletter')" class="btn-close"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div id="collapseNewsletter" class="panel-collapse collapse in">
                    <div class="row dailyupdates">
                        <ul id="Newslettersrow" class="bxslider bxnews" style="width: 100%; max-width: none;">
                        </ul>
                    </div>
                    <!--/.row-->
                    <div class="modal fade" id="Newslettermodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 550px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <img id="newsImg" src="../Images/xyz.png" style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; width: 100%" />
                                    <h2 id="newsTitle"></h2>
                                    <div class="clearfix" style="height: 10px;"></div>
                                    <div id="newsDesc"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="btn btn-search" style="float: right;" href="../Users/NewsLetterList.aspx" title="View">View All</a>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- News Letter - End -->

    <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1200px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="blank.html" width="1150px" height="100%" frameborder="0" scrolling="no"></iframe>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <h3 style="text-align: center">Notifications</h3>
                    <br />
                    <div id="divNotificationData"></div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none; height: 20px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">

                        <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>

    </section>       
    </section>
    <asp:HiddenField ID="hidposition" runat="server" Value="" />
    <script type="text/javascript" src="../Newjs/responsive-calendar.min.js"></script>
    <script type="text/javascript">
        function fposition()
        {
            $('#<%=hidposition.ClientID%>').val(document.body.scrollTop);
        }
        function fsetscroll()
        {            
            document.body.scrollTop=$('#<%=hidposition.ClientID%>').val();
        }
        //carousel
        $(document).ready(function () {          
            $('.bxslider-map').bxSlider({                
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 250,
                slideMargin: 50,
                moveSlides: 1,
                auto: false,

            });
        });
        //custom select box
        $(function () {
            $('select.styled').customSelect();
        });
    </script>
    <script type="text/javascript">
        $(function () {

            $(".knob").knob({

                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = 1;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                            && (sat = eat - 0.3)
                            && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.v);
                            this.o.cursor
                                && (sa = ea - 0.3)
                                && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.pColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();
                        return false;
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        /*script to showHide Div*/
        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }

        function showDiv(id) {
            if (document.getElementById(id).style.display != 'none') {
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
                document.getElementById(id).style.display = 'none';
            }
            else {
                //if (document.getElementById(id + 'Checkbox').className == 'menucheckbox-checked') {
                document.getElementById(id).style.display = 'block';
                document.getElementById(id + 'Checkbox').className = 'menucheckbox-checked';

                // }
            }
        }
    </script>
    <script type="text/javascript">
        // replace/populate colors from user saved profile        
        var perFunctionChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>"
        };
        var perRiskStackedColumnChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
           critical: "<%=criticalcolor.Value %>"
        };
        var perStatusChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium:"<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>"
        };


        // function executes when page is ready
        // main documentReady function starts
        $(function () {
            // Chart Global options
            Highcharts.setOptions({
                credits: {
                    text: '',
                    href: 'https://www.avantis.co.in',
                },
                lang: {
                    drillUpText: "< Back",
                },
            });

            //perFunctionChart
            var perFunctionChart = Highcharts.chart('perFunctionChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            this.subtitle.update({ text: 'Click on graph to view documents' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Function' });
                            this.subtitle.update({ text: 'Completion Status - Overall' });
                        }
                    },
                },
                title: {
                    text: 'Per Function',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall',
                    style: {
                        "font-family": 'Roboto',
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances'
                    },
                    //labels: {
                    //    enabled:false,
                    //},
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray',
                            }
                        },
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.key}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },              
             <%=perFunctionChart%>
            });            
            
            var perStatusPieChart = Highcharts.chart('perStatusPieChartDiv', {
                chart: {
                    type: 'pie',
                    events: {
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            // this.subtitle.update({ text: 'Click on graph to view documents' });
                            this.subtitle.update({ text: 'Completion Status - Overall Risk' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Status' });
                            //this.subtitle.update({ text: 'Click on graph to drilldown' });
                            this.subtitle.update({ text: 'Completion Status - Overall' });
                        }
                    },
                },
                title: {
                    text: 'Per Status',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall',                   
                    style: {
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            //format: '{y} <br>{point.name}',
                            format: '{y}',
                            distance: 5,
                        },
                        showInLegend: true,
                    },
                  
                },
                legend: {
                    itemDistance: 2,
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    formatter: function () {
                        if (this.series.name == 'Status')
                            return 'Click to Drilldown';	// text before drilldown
                        else
                            return 'Click to View Documents';		// text after drilldown
                    }
                },
                <%=perFunctionPieChart%>                
            });

          
            var perRiskStackedColumnChart = Highcharts.chart('perRiskStackedColumnChartDiv', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Per Risk',
                    style: {
                        // "font-family": 'Helvetica',
                        display: 'none'
                    },
                },
                subtitle: {
                    //text: 'Click on graph to view documents',
                    text: '',
                    style: {
                        // "font-family": 'Helvetica',
                    }
                },
                xAxis: {
                    categories: ['High', 'Medium', 'Low', 'Critical']
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances',                     
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: 'gray'
                        }
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: 'white',
                            style:{
                              
                                textShadow:false,
                            }
                        },
                        cursor: 'pointer'
                    },
                },
              <%=perRiskChart%>
            });


            var perPenaltyStatusPieChart = Highcharts.chart('perPenaltyStackedColumnChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        drillup: function () {
                            this.setTitle({ text: 'Per Function' });
                            this.subtitle.update({ text: 'Completion Status - Overall Functions' });
                        }
                    },
                },
                title: {
                    text: 'Per Function',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Quarterly Penalty',
                    style: {
                        "font-family": 'Roboto',
                        fontWeight: '300',
                        fontSize:'15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    title: {
                        text: 'Penalty Amount'
                    },
                    //labels: {
                    //    enabled:false,
                    //},
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray',
                            }
                        },
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.key}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },              
               <%=perPenaltyStatusPieChart%>
            });


            // change color in color picker according to chart selected
            $('input[name=radioButton]').change(function () {
                // destroy all color pickers
                $('#highColorPicker').simplecolorpicker('destroy');
                $('#mediumColorPicker').simplecolorpicker('destroy');
                $('#lowColorPicker').simplecolorpicker('destroy');
                $('#criticalColorPicker').simplecolorpicker('destroy');

                // setting the colors for risks according to chart selected
                var chart = $('input[name=radioButton]:checked').val();
                switch (chart) {
                    case "perFunction":
                        $('#highColorPicker').val(perFunctionChartColorScheme.high);
                        $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                        $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                        $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                        break;
                    case "perRisk":
                        $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                        $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                        $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                        $('#criticalColorPicker').val(perRiskStackedColumnChartColorScheme.critical);
                        break;
                    case "perStatus":
                        $('#highColorPicker').val(perStatusChartColorScheme.high);
                        $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                        $('#lowColorPicker').val(perStatusChartColorScheme.low);
                        $('#criticalColorPicker').val(perStatusChartColorScheme.critical);
                        break;
                    default:
                        $('#highColorPicker').val('#7CB5EC');
                        $('#mediumColorPicker').val('#434348');
                        $('#lowColorPicker').val('#90ED7D');
                        $('#criticalColorPicker').val('#CC0900');
                }

                // initialise the color piskers again
                $('#highColorPicker').simplecolorpicker({ picker: true });
                $('#mediumColorPicker').simplecolorpicker({ picker: true });
                $('#lowColorPicker').simplecolorpicker({ picker: true });
                $('#criticalColorPicker').simplecolorpicker({ picker: true });
            });

            // chart type selector radio buttons
            try	{
                $("#radioButtonGroup").buttonset();
                $(".chart-selector-radio-buttons").checkboxradio({
                    icon: false
                });
            }catch(e){}
            //apply color scheme to all charts [Apply to All] click event handler
            $('#applyToAllButton').click(function () {
                // value of colors selected at the time in color picker
                // $('#highColorPicker').val();
                // $('#mediumColorPicker').val();
                // $('#lowColorPicker').val();
            });

            $('#showModal').click(function () {
                $('#modalDiv').modal();
            });

        });   //	main documentReady function END
        var btnclosemgt=$('.modal-header').find('button');
        $(btnclosemgt).click(function () { 
            $('#showdetails').attr('src','blank.html');
        });
        function fpopulateddata(type,attribute,customerid,branchid,fromdate,enddate,filter,functionid,internalsatutory,chartname,listcategoryid,userid,isapprover)
        {            
            $('#divreports').modal('show');         
            if (internalsatutory == "Statutory") {
                $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','800px'); 
                $('.modal-dialog').css('width','98%'); 
                $('#showdetails').attr('src', "../Management/SatutoryManagementAPI.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }
            else if (internalsatutory == "Internal") {
               $('#showdetails').attr('width','100%'); 
                $('#showdetails').attr('height','800px'); 
                $('.modal-dialog').css('width','98%');  
                $('#showdetails').attr('src', "../Management/InternalManagementAPI.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory+"&listcategoryid="+listcategoryid+"&Userid="+userid+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }                       
        }

        function fpopulatedPenaltydata(type,attribute,customerid,branchid,startdate,enddate,isapprover,IsSatutoryInternal)
        {
            $('#divreports').modal('show');                 
            $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');  
            $('#showdetails').attr('src', "../Penalty/PenaltyDetailsAPI.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid+ "&startdate=" + startdate + "&enddate=" + enddate+"&Internalsatutory="+IsSatutoryInternal+"&IsDeptHead=1");            
        }

        function PopulateGraphdata(isapprover)
        {            
            if(Displays() ==true)
            {
                $('#divreports').modal('show');       
                $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');  ;  
                $('#showdetails').attr('src', "../Management/ManagementLocations.aspx?ctype="+ $('#ContentPlaceHolder1_ddlStatus').val()+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }                
        }
        function fusers(customerid,branchid,internalsatutory,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');  
                $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');  
                $('#showdetails').attr('src', "../Management/ManagementusersAPI.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory=" + internalsatutory+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }
                                  
        }
        function fEntity(internalsatutory,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');  
                $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');    
                $('#showdetails').attr('src', "../Management/ManageEntity.aspx?Internalsatutory=" + internalsatutory+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }                   
        }
        function fusersRahul()
        {            
            $('#divreports').modal('show');  
            $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');                                                 
        }
        function fFunctions(customerid,branchid,IsSatutoryInternal,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');      
                $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');     
                $('#showdetails').attr('src', "../Management/DEPTFunctionDetails.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }
                                         
        }
   
        function fCompliancesRahul() {            
            $('#divreports').modal('show');
             $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');  
            return true;
           
        }
        function fCompliances(customerid,branchid,IsSatutoryInternal,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');    
               $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');    
                $('#showdetails').attr('src', "../Management/ComplianceDetailsAPI.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }                                         
        }
        function fPenaltyDetails(customerid,branchid,IsSatutoryInternal,isapprover)
        {
            if(Displays() ==true)
            {                
                $('#divreports').modal('show');    
               $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');   
                $('#showdetails').attr('src', "../Penalty/PenaltyDetailsAPI.aspx?customerid="+customerid+"&branchid="+branchid+"&Internalsatutory="+IsSatutoryInternal+"&IsApprover="+isapprover+"&IsDeptHead=1");
            }                                         
        }

        function GradingReportPopPup(customerid,type,IsSatutoryInternal,startdate,enddate,Customerbanchid)
        {
            if (type=="NA") {
                return;
            }
            $('#divreports').modal('show');    
           $('#showdetails').attr('width','100%'); 
            $('#showdetails').attr('height','650px'); 
            $('.modal-dialog').css('width','98%');  
            $('#showdetails').attr('src', "../Management/GradingDisplay.aspx?customerid="+customerid+"&type="+type+"&Internalsatutory="+IsSatutoryInternal+"&StartDate="+startdate+"&EndDate="+enddate+"&Customerbanchid="+Customerbanchid+"&IsDeptHead=1");              
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('Dashboard');           
        });
    </script>
    <script type="text/javascript">       
        var dailyupadatepageno = 1;
        var Newslettersrowpageno = 1;
        function fdailyclick() { dailyupadatepageno += 1; binddailyupdatedata(); }
        function fnewsclick() { Newslettersrowpageno += 1; bindnewsdata(); }
        var dailyupdateJson;
        var NewsLetterJson;
        function bindnewsdata() {
            var k = 0;
            $.ajax({
                async: true,
                type: 'Get',                
                url: 'https://login.avantis.co.in/apadr/Data/GetNewsletterData?page=' + Newslettersrowpageno,
                content: 'application/json;charset=utf-8',
                processData: true,
                headers: {
                    "Authorization": "Bearer"
                },
                success: function (result) {
                    NewsLetterJson = result;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.length; step++) {
                        var objDate = new Date(result[step].NewsDate);
                        str += '<li><img style="width:250px;height:166px" src="../NewsLetterImages/' + result[step].FileName + '" /><h3>' + result[step].Title + '</h3><p><a class="view-pdf" data-toggle="modal" style="cursor:pointer" onclick="viewpdf(this)" data-title="' + result[step].Title + '"  data-href="../NewsLetterImages/' + result[step].DocFileName + '"> Issue ' + getmonths(objDate) + ' ' + objDate.getFullYear() + '</a></p></li>'
                    }
                    $("#Newslettersrow").html(str);
                    var ker = $('.bxnews').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 250,
                        slideMargin: 50,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'newsletternext',
                        nextclickFunction:'fnewsclick()',
                    });
                },
                error: function (e, t) { }
            })
        }

        function binddailyupdatedata() {
        
              var data = { Email: '<%=UserInformation%>' };
            var k = 0;
            $.ajax({
                async: true,
                type: 'post',
                //url: "https://cors-anywhere.herokuapp.com/https://api.avantis.co.in/api/v2/legalupdates/params/",
                //url: "https://tunnel2.avantisregtec.in/api/v2/legalupdates/params/",
                url: "https://api.avantis.co.in/api/v2/legalupdates/params/",
                content: 'application/json;charset=utf-8',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token 04f7a4e33a692196c39ef9ba54c53459c9a9b25b'
                },
                processData: true,
                body: JSON.stringify(data),
                success: function (result) {
                    dailyupdateJson = result.data;
                    var step = 0;
                    var str = '';
                    for (step = 0; step < result.data.length; step++) {
                        var titledaily = '';
                        var Descriptiondaily = '';
                        if (result.data[step].title.length > 35) {
                            titledaily = result.data[step].title.substring(0, 35) + '...'
                        } else {
                            titledaily = result.data[step].title;
                        }
                        if (result.data[step].description.length > 150) {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description.substring(0, 150) + '...'
                            }
                        } else {
                            if (result.data[step].description.indexOf('<div') == -1 || result.data[step].description.indexOf('<span') == -1) {
                                Descriptiondaily = result.data[step].description;
                            }
                        }
                        var category = '';
                        if (result.data[step].Categories.length > 12) {
                            category = result.data[step].Categories.substring(0, 12) + '...'
                        } else {
                            category = result.data[step].Categories;
                        }

                        var objDate = new Date(result.data[step].date);
                        var GetDateDetail = getmonths(objDate) + ' ' + objDate.getDate() + ', ' + objDate.getFullYear();
                        
                        str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '" target="_blank">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '" target="_blank">' + result.data[step].Types + '</a></li></ul><li><a href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank"><h4 class="card-text-sm"> ' + titledaily + ' </h4></a><a href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank"><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p></a><br /><a data-toggle="modal" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank">Read More</a></p></li></div></div>'
                        //str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '" target="_blank">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '" target="_blank">' + result.data[step].Types + '</a></li></ul><li><h4 class="card-text-sm"> ' + titledaily + ' </h4><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p><br /><a data-toggle="modal" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update" target="_blank">Read More</a></p></li></div></div>'
                        //str += '<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 c-col mb-2" style="padding-top: 12px;"><div class="box h-100 focusable" style="min-height: 270px!important;max-height:270px!important;"><ul class="tags"><li><a class="a-tag" title=' + result.data[step].Categories + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?cat=' + result.data[step].Categories + '">' + category + '</a></li><li><a class="a-tag" title=' + result.data[step].Types + ' hreflang="en" href="https://www.avantis.co.in/legalupdates/?state=' + result.data[step].Types + '">' + result.data[step].Types + '</a></li></ul><li><h4 class="card-text-sm"> ' + titledaily + ' </h4><p class="text-black-50">' + GetDateDetail + '</p><p>' + Descriptiondaily + '</p><p><br /><a data-toggle="modal" href="https://www.avantis.co.in/updates/article/' + result.data[step].id + '/update">Read More</a></p></li></div></div>'
                                               
                    }
                    $("#DailyUpdatesrow").append(str);
                    var ite = $('.bxdaily').bxSlider({
                        minSlides: 3,
                        maxSlides: 3,
                        slideWidth: 300,
                        slideMargin: 0,
                        moveSlides: 1,
                        auto: false,
                        nextCss: 'dailyupdatenext',
                        nextclickFunction: 'fdailyclick()',
                    });
                },
                error: function (e, t) {
                    //alert('false');
                }
            });
            
            
            //var k = 0;
            //$.ajax({
            //    async: true,
            //    type: 'Get',
            //    url: "https://login.avantis.co.in/apadr/Data/GetDailyupdateData?page="+dailyupadatepageno+"&searchKey=",
            //    content: 'application/json;charset=utf-8',
            //    processData: true,
            //    headers: {
            //        "Authorization": "Bearer"
            //    },
            //    success: function (result) {
            //        dailyupdateJson = result;
            //        var step = 0;
            //        var str = '';
            //        for (step = 0; step < result.length; step++) {
            //            var titledaily = '';
            //            var Descriptiondaily = '';
            //            if (result[step].Title.length > 150) {
            //                titledaily = result[step].Title.substring(0, 150) + '...'
            //            } else {
            //                titledaily = result[step].Title;
            //            }
            //            if (result[step].Description.length > 150) {
            //                if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
            //                    Descriptiondaily = result[step].Description.substring(0, result[step].Description) + '...'
            //                }
            //            } else {
            //                if (result[step].Description.indexOf('<div') == -1 || result[step].Description.indexOf('<span') == -1) {
            //                    Descriptiondaily = result[step].Description;
            //                }
            //            }
            //            str += '<li><h3 style="height: 70px;"> ' + titledaily + ' </h3> <p><br />   <a data-toggle="modal" onclick="Binddailyupdatepopup(' + result[step].ID + ');" href="#NewsModal">Read More</a></p></li>'
            //        }
            //        $("#DailyUpdatesrow").append(str);
            //        var ite = $('.bxdaily').bxSlider({
            //            minSlides: 3,
            //            maxSlides: 3,
            //            slideWidth: 250,
            //            slideMargin: 50,
            //            moveSlides: 1,
            //            auto: false,
            //            nextCss: 'dailyupdatenext',
            //            nextclickFunction: 'fdailyclick()',
            //        });
                 
            //    },
            //    error: function (e, t) { }
            //})
        
        
        }
        $(document).ready(function () {
           
            binddailyupdatedata();
            bindnewsdata();
        });
       
        function timeconvert(ds) {
            var D, dtime, T, tz, off,
            dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
            T = parseInt(dobj[0]);
            tz = dobj[1];
            off = dobj[2];
            if (off) {
                off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                 (parseInt(off.substring(2), 10) * 60000);
                if (tz == '-') off *= -1;
            }
            else off = 0;
            return new Date(T += off).toUTCString();
        }
        function getmonths(mon) {
            try {
                return mon.toString().split(' ')[1];
            } catch (e) { }
        }

        function Binddailyupdatepopup(DID) {
            for (var i = 0; i < dailyupdateJson.length; i++) {
                if (DID == dailyupdateJson[i].ID) {
                    var objDate = new Date(timeconvert(dailyupdateJson[i].CreatedDate));
                    var mon_I = getmonths(objDate);
                    $("#dailyupdatedate").html('Daily Updates:' + mon_I + ' ' + objDate.getDate() + ', ' + objDate.getFullYear());
                    $("#dailyupdatedateInner").html('Daily Updates:' + mon_I + ' ' + objDate.getDate());
                    $("#contents").html(dailyupdateJson[i].Description);
                    $("#dailytitle").html(dailyupdateJson[i].Title);
                    break;
                }
            }

        }
        function BindNewsLetterpopup(DID) {
            for (var i = 0; i < NewsLetterJson.length; i++) {
                if (DID == NewsLetterJson[i].ID) {

                    $("#newsImg").attr('src', '../Images/' + NewsLetterJson[i].FileName);
                    $("#newsTitle").html(NewsLetterJson[i].Title);
                    $("#newsDesc").html(NewsLetterJson[i].Description);

                    break;
                }
            }

        }
        function forchild(hgt)
        {
            $('#showdetails').attr('height',75 * (hgt));
        }
       
        $("#High").spectrum({
            color:"<%=highcolor.Value %>",
            showInput: true,
            className: "full-spectrum",
            showInitial: true,
            showPalette: true,
            showSelectionPalette: true,
            maxSelectionSize: 10,
            preferredFormat: "hex",
            containerClassName: "highcls",
            move: function (color) {

            },
            show: function () {

            },
            beforeShow: function () {

            },
            hide: function () {

            },
            change: function () {
                 
            },
            palette: [
                ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        });
            $("#medium").spectrum({
                color: "<%=mediumcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName: "mediumcls",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function () {

                },
                change: function () {
            
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
            var lowchange = false;
            $("#low").spectrum({
                color:"<%=lowcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName:"lowcls",
                move: function (color) {
              
                },
                show: function () {
            
                },
                beforeShow: function () {
              
                },
                hide: function () {
            
                },
                change: function () {
               
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

        var criticalchange = false;
            $("#critical").spectrum({
                color:"<%=criticalcolor.Value %>",
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                containerClassName:"criticalcls",
                move: function (color) {
              
                },
                show: function () {
            
                },
                beforeShow: function () {
              
                },
                hide: function () {
            
                },
                change: function () {
               
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
            var mediumbtn = $(".mediumcls").find('button');
            $(mediumbtn).click(function () {
            
                var inputclr = $(".mediumcls").find('input.sp-input');
                if( $('#ContentPlaceHolder1_mediumcolor').val()!=$(inputclr).val()){
                    upcolor();
                    $('#ContentPlaceHolder1_mediumcolor').val($(inputclr).val());
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
            });
            var highbtn = $(".highcls").find('button');
            $(highbtn).click(function () {
                var inputclr1 = $(".highcls").find('input.sp-input');
          
         
                if( $('#ContentPlaceHolder1_highcolor').val()!=$(inputclr1).val()){
                    $('#ContentPlaceHolder1_highcolor').val($(inputclr1).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
      
          
            });
            var lowbtn=$(".lowcls").find('button');
            $(lowbtn).click(function () {
         
                var inputclr2 = $(".lowcls").find('input.sp-input');
        
                if($('#ContentPlaceHolder1_lowcolor').val()!=$(inputclr2).val())
                {   
                    $('#ContentPlaceHolder1_lowcolor').val($(inputclr2).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
           
            });
         var criticalbtn=$(".criticalcls").find('button');
            $(criticalbtn).click(function () {
         
                var inputclr3 = $(".criticalcls").find('input.sp-input');
        
                if($('#ContentPlaceHolder1_criticalcolor').val()!=$(inputclr3).val())
                {   
                    $('#ContentPlaceHolder1_criticalcolor').val($(inputclr3).val());
                    upcolor();
                    document.getElementById("<%=btnTopSearch.ClientID %>").click();
                }
           
            });
            function upcolor()
            {
            
                var k = 0;
                $.ajax({
                    async: true,
                    type: 'Post',
                    url: '/dailyupdateservice.svc/upcolor',                       
                    data: JSON.stringify({"high": $('#ContentPlaceHolder1_highcolor').val(),"sender": <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>, "medium":  $('#ContentPlaceHolder1_mediumcolor').val(), "low":  $('#ContentPlaceHolder1_lowcolor').val(), "critical":  $('#ContentPlaceHolder1_criticalcolor').val()}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function (result) {
                    
                    },
                    error: function (e, t) { }
                });
            }
    
            function initializeJQueryUI(textBoxID, divID) {
                $("#" + textBoxID).unbind('click');

                $("#" + textBoxID).click(function () {
                    $("#" + divID).toggle("blind", null, 500, function () { });
                });
            }
         
 
            (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog"  style="width:1000px;">';html+='<div class="modal-content" style="width:1100px;">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);
    
            /*
            * Here is how you use it
            */
 
            function viewpdf(obj)  {
         
                var pdf_link = $(obj).attr('data-href');
                var pdf_title = $(obj).attr('data-title');          
                var iframe = '<object type="application/pdf" data="' + pdf_link + '"#toolbar=1&navpanes=0&statusbar=0&messages=0 width="100%" height="500">No Support</object>'
                $.createModal({
                    title: pdf_title,
                    message: iframe,
                    closeButton:true,
                    scrollable: false
                });
                return false;        
            } 

            function openNotificationModal() {            

                if (sessionStorage.getItem("Notify") == null) {
                    $('#divNotification').modal('show');
                    SetNotifyKey();
                    return true;
                }
            
            }       

            function BindNewNotifications() {
                var k = 0;
                $.ajax({
                    async: true,
                    type: 'Get',
                    url: '/dailyupdateservice.svc/BindNotifications?userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>',
                    content: 'application/json;charset=utf-8',
                    processData: true,
                    success: function (result) {                    
                        var step = 0;
                        var str = '';
                        for (step = 0; step < result.length; step++) {
                        
                            str += '<li><p><a href="../DailyUpdates/Notification.aspx">' + result[step] + '</a></p></li>'
                        }
                        $("#divNotificationData").html(str);
                   
                    },
                    error: function (e, t) { }
               
                })                    
            
            }
            $(document).ready(function () {            
                $.get("/controls/calendarstandalone.aspx?m=8&type=0&dhead=1", function(data){
                    $(".responsive-calendar").html(data);
                });        
                setInterval(setcolor, 1000);
            });
            var strop=0;
            $('#ContentPlaceHolder1_rdbcalender').change(function () {
                //$(this).click(function () {
                var val=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
                if(strop==0)                        
                    fcallcal(val);
                //});
            });
            function fcallcal(val){                
                strop=1
                $(".responsive-calendar").html('<img src="../images/processing.gif">');
                $.get("/controls/calendarstandalone.aspx?m=8&type="+val+"&dhead=1", function(data){
                    $(".responsive-calendar").html(data);
                    strop=0;
                });     
            }

            function setcolor() {
                $('.overdue').closest('div').find('a').css('background-color', '#FF0000');
                $('.complete').closest('div').find('a').css('background-color', '#006500');
                $('.pending').closest('div').find('a').css('background-color', '#00008d');
                $('.delayed').closest('div').find('a').css('background-color', '#ffcd70');
                
            }
            $(document).ready(function (ClassName) {
                $("a").addClass(ClassName);
            });
            function formatDate(date) {
                var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                ];
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                return day + ' ' + monthNames[monthIndex] + ' ' + year;
            }
            function fclosepopcal(dt) {
                $('#divreports').modal('hide');
                fcal(dt)
            }
            function hideloader()
            {
                $('#imgcaldate').hide();
            }
            function fcal(dt) {

                try {                
                    var ddata = new Date(dt);
                    $('#clsdatel').html('Compliance items for date ' + formatDate(ddata));
                } catch (e) { }

                $('#imgcaldate').show();
                var valtype=  $('#ContentPlaceHolder1_rdbcalender input:checked').val()
                $('#calframe').attr('src', '/controls/calendardataAPI.aspx?m=8&date=' + dt+'&type='+valtype+'&dhead=1')
                return;               
            }
            function OpenOverViewpup(scheduledonid, instanceid, CType) {
                if (CType == 'Statutory' || CType == 'Statutory CheckList' || CType == 'Event Based') {
                    $('#divOverView').modal('show');
                    $('#OverViews').attr('width', '1150px');
                    $('#OverViews').attr('height', '600px');
                    $('.modal-dialog').css('width', '1200px');
                    $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
                else 
                {
                    $('#divOverView').modal('show');
                    $('#OverViews').attr('width', '1050px');
                    $('#OverViews').attr('height', '600px');
                    $('.modal-dialog').css('width', '1100px');
                    $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
            }
            function OpenPerrevpopup(scheduledonid, instanceid, Interimdays, CType, RoleID, dtdate) {
                $('#divreports').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '650px');
                $('.modal-dialog').css('width', '85%');
                $('#showdetails').attr('src', "../Compliances/CalenderPopup.aspx?scheduleonId=" + scheduledonid + "&InstanceId=" + instanceid + "&Interimdays=" + Interimdays + "&CType=" + CType + "&RoleID=" + RoleID + "&dt=" + dtdate);
            }
            function OpenOverdueComplainceList(customerid,UserID,isapprover){                      
                $('#divreports').modal('show');    
                $('#showdetails').attr('width','1150px'); 
                $('#showdetails').attr('height','580px'); 
                $('.modal-dialog').css('width','1200px');    
                $('#showdetails').attr('src', "../Management/OverdueComplianceDeptAPI.aspx?IsDeptHead=1");
            }
            function OpenInternalOverdueComplainceList(customerid,UserID,isapprover){
                $('#divreports').modal('show');    
                $('#showdetails').attr('width','1150px'); 
                $('#showdetails').attr('height','580px'); 
                $('.modal-dialog').css('width','1200px');    
                $('#showdetails').attr('src', "../Management/InternalOverdueComplianceDeptAPI.aspx?IsDeptHead=1");
            }
            function SetNotifyKey() {
                sessionStorage.setItem("Notify", "1");
            }
            BindNewNotifications();
            $("#DivFilters").click(function (event) {
 
                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                        $("#divFilterLocation").show();
                    } else {
                        $("#divFilterLocation").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#divFilterLocation").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                    $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                    $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                        $("#divFilterLocation").toggle("blind", null, 500, function () { });
                    });

                }
            });



            $("#ContentPlaceHolder1_PenaltyCriteria").click(function (event) {
 
                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocationPenalty') > -1) {
                        $("#divFilterLocationPenalty").show();
                    } else {
                        $("#divFilterLocationPenalty").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocationPenalty") {
                    $("#divFilterLocationPenalty").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationPenalty') > -1) {
                    $("#divFilterLocationPenalty").show();
                } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocationPenalty") {
                    $("#ContentPlaceHolder1_tbxFilterLocationPenalty").unbind('click');

                    $("#ContentPlaceHolder1_tbxFilterLocationPenalty").click(function () {
                        $("#divFilterLocationPenalty").toggle("blind", null, 500, function () { });
                    });

                }
            });


            $("#compliancesummary").click(function (event) {
 
                if (event.target.id == "") {
                    var idvid = $(event.target).closest('div');
                    if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('TreeGraddingReport') > -1) {
                        $("#divFilterLocationGradding").show();
                    } else {
                        $("#divFilterLocationGradding").hide();
                    }
                }
                else if (event.target.id != "ContentPlaceHolder1_TbxFilterLocationGridding") {
                    $("#divFilterLocationGradding").hide();
                } else if (event.target.id != "" && event.target.id.indexOf('divFilterLocationGradding') > -1) {
                    $("#divFilterLocationGradding").show();
                } else if (event.target.id == "ContentPlaceHolder1_TbxFilterLocationGridding") {
                    $("#ContentPlaceHolder1_TbxFilterLocationGridding").unbind('click');

                    $("#ContentPlaceHolder1_TbxFilterLocationGridding").click(function () {
                        $("#divFilterLocationGradding").toggle("blind", null, 500, function () { });
                    });

                }
            });
    </script>
    
</asp:Content>
