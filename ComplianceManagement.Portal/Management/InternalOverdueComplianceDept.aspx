﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalOverdueComplianceDept.aspx.cs"
     Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.InternalOverdueComplianceDept" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <link href="../NewCSS/stylenew.css" rel="stylesheet" />
     <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
  <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
     <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <style type="text/css">
        .ui-widget-header{border:0px !important; background:inherit;font-size: 20px;color: #666666;font-weight: normal;padding-top: 0px;margin-top: 5px;}
        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color:#666666 !important;
                text-decoration:none !important; 
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    
<script type="text/javascript">

    function fComplianceOverview(obj) {

        OpenOverViewpup($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
    }

    function OpenOverViewpup(scheduledonid, instanceid) {
        $('#divOverView').modal('show');
        $('#OverViews').attr('width', '1080px');
        $('#OverViews').attr('height', '600px');
        $('.modal-dialog').css('width', '1130px');
        $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);

    }

    $(document).ready(function () {
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
       // window.parent.forchild($('#grdSummaryDetails').find('tr').length);
      
    });

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
   
    $(document).on("click", "#upDivLocation_Load", function (event) {
        
        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else {
                $("#divFilterLocation").hide();
            }
        }
        else if (event.target.id != "tbxFilterLocation") {
            $("#divFilterLocation").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
            $("#divFilterLocation").show();
        } else if (event.target.id == "tbxFilterLocation") {
            $("#tbxFilterLocation").unbind('click');

            $("#tbxFilterLocation").click(function () {
                $("#divFilterLocation").toggle("blind", null, 500, function () { });
            });

        }
    });
</script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

 
        <div class="col-md-12">
            
   <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
        <ContentTemplate>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="col-md-2 colpadding0 entrycount">
                <div class="col-md-3 colpadding0">
                    <p style="color: #999; margin-top: 5px;">Show</p>
                </div>
                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"> 
                    <%--<asp:ListItem Text="5" Selected="True" />--%>
                    <asp:ListItem Text="10" Selected="True" />
                    <asp:ListItem Text="20" />
                    <asp:ListItem Text="50" />
                </asp:DropDownList>
            </div>

            <div class="col-md-10 colpadding0">
                <div style="float:left;margin-right:10px">
                <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 33px; width: 325px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                   CssClass="txtbox"/>
                <div style="margin-left: 1px; position: absolute; z-index: 10" id="divFilterLocation">
                    <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                        Style="overflow: auto; border-left: 1px solid #c7c7cc;    margin-top: -22px; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                    </asp:TreeView>
                </div>
                </div>
                 <div style="float: left; margin-right: 10px">
                    <asp:DropDownList runat="server" ID="ddlFunctions" class="form-control m-bot15 select_location" Style="width: 150px;">
                        </asp:DropDownList>
                </div>
               <div style="float: left; margin-right: 10px">
                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                </div>
                <div style="float: right;">
                    <asp:UpdatePanel ID="upExport" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton runat="server" Text="Export to Excel" CssClass="btn btn-search" ID="lbtnExportExcel" ToolTip="Export All to Excel" OnClick="lbtnExportExcel_Click">          
                            </asp:LinkButton>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
           </div>
           <div class="col-md-13 AdvanceSearchScrum">
                <div id="divAdvSearch" runat="server" visible="false">
                    <p>
                        <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Search</asp:LinkButton>
                    </p>
                </div>

                <div runat="server" id="DivRecordsScrum" style="float: right;">
                    <p style="padding-right: 0px !Important;">
                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                    </p>
                </div>
            </div>

            <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
            </div>

             <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                        CellPadding="4" AllowPaging="true" OnRowDataBound="grdSummaryDetails_RowDataBound" PageSize="10" Width="100%" 
                        CssClass="table" OnRowCreated="grdSummaryDetails_RowCreated" OnSorting="grdSummaryDetails_Sorting" DataKeyNames="InternalComplianceInstanceID">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="Short Description" SortExpression="ShortDescription">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                        <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField HeaderText="Entity/Location" SortExpression="Branch">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblLocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Due Date" SortExpression="InternalScheduledOn">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                        <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>' ToolTip='<%# Eval("InternalScheduledOn")!= null?((DateTime)Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy"):""%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                  <asp:TemplateField HeaderText="OverdueBy(Days)" SortExpression="OverdueBy">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;text-align: center">                          
                                 <asp:Label ID="lblOverdueBy" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("OverdueBy") %>' ToolTip='<%# Eval("OverdueBy") %>'></asp:Label>
                             </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                            <asp:TemplateField HeaderText="Period" SortExpression="ForMonth">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                          
                                     <asp:Label ID="lblForMonth" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="Performer">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px;">
                                            <asp:Label ID="lblInternalPerformer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetPerformerInternal((long)Eval("InternalComplianceInstanceID")) %>' ToolTip='<%#InternalPerformername%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                                       
                             <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                        <ItemTemplate>
                            <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="lblOverView1" runat="server" ImageUrl="~/Images/edit_icon_new.png" ScheduledOnID='<%# Eval("InternalScheduledOnID")%>' instanceId='<%#Eval("InternalComplianceInstanceID") %>'
                                        OnClientClick='fComplianceOverview(this)' ToolTip="Click to OverView"></asp:ImageButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ItemTemplate>
                    </asp:TemplateField>  
                       </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
            
             <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-6 colpadding0">
                    <div class="table-Selecteddownload">
                        <div class="table-Selecteddownload-text">
                            <p>
                                <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 15px;"></asp:Label></p>
                        </div>
                      <%--  <asp:Button Text="Download" CssClass="btn btn-search" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click Here to Download Document(s) of Selected Compliances."
                            OnClientClick="javascript:return DownloadDoc();" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />--%>
                    </div>
                </div>

                <div class="col-md-6 colpadding0">
                    <div class="table-paging" style="margin-bottom: 20px;">
                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                        <div class="table-paging-text">
                            <p>
                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                            </p>
                        </div>

                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </div>
                </div>
            </div>            
                        
        </ContentTemplate>
       <Triggers>
           <%--<asp:PostBackTrigger ControlID="btnDownload" />
           <asp:PostBackTrigger ControlID="lbtnExportExcel" />--%>
       </Triggers>
    </asp:UpdatePanel>
            
  </div>

        <div>
                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1150px;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">

                                <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
                </div> 
    </form>
</body>
</html>
