﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using System.Data;
using iTextSharp.text.pdf;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Configuration;
using System.Net.Mail;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ManagementGradingReport : System.Web.UI.Page
    {
        int checkInternalapplicable = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindMonth(DateTime.UtcNow.Year);
                //BindYear();
                //BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    User user = UserManagement.GetByID(userID);

                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }
                }
                if (checkInternalapplicable == 1)
                {
                    Tab2.Visible = true;
                }
                else if (checkInternalapplicable == 0)
                {
                    Tab1.Visible = true;
                    Tab2.Visible = false;
                }             

                Tab1_Click(sender,  e);
            }
        }

        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            BindMonth(DateTime.UtcNow.Year);
            BindYear();
            BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));
            
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {            
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
            BindMonth(DateTime.UtcNow.Year);
            BindYear();
            BindGradingReportSummaryInternal(Convert.ToInt32(ddlPeriod.SelectedValue));
           
        }
        protected void ddlyear_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (Tab1.CssClass == "Clicked")
            {
                BindMonth(Convert.ToInt32(ddlyear.SelectedValue));
                BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));
            }
            else if (Tab1.CssClass == "Initial")
            {
                BindMonth(Convert.ToInt32(ddlyear.SelectedValue));
                BindGradingReportSummaryInternal(Convert.ToInt32(ddlPeriod.SelectedValue));
            }

        }

        protected void ddlmonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (Tab1.CssClass == "Clicked")
            {
                BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));
            }
            else if (Tab1.CssClass == "Initial")
            {
                BindGradingReportSummaryInternal(Convert.ToInt32(ddlPeriod.SelectedValue));
            }

        }

        private void BindMonth(int year)
        {
            try
            {
                ddlmonths.DataTextField = "Name";
                ddlmonths.DataValueField = "ID";
                int selectedMonth = 0;
                List<NameValue> months;
                if (year == DateTime.UtcNow.Year)
                {
                    months = Enumerations.GetAll<Month>().Where(entry => entry.ID <= DateTime.UtcNow.Month).ToList();
                    selectedMonth = months.Count;
                }
                else
                {
                    months = Enumerations.GetAll<Month>();
                    selectedMonth = 1;
                }

                ddlmonths.DataSource = months;
                ddlmonths.DataBind();
                ddlmonths.SelectedValue = Convert.ToString(selectedMonth);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindYear()
        {

            int CurrentYear = DateTime.Now.Year;
            for (int i = 1; i < 100; ++i)
            {
                System.Web.UI.WebControls.ListItem tmp = new System.Web.UI.WebControls.ListItem();
                tmp.Value = CurrentYear.ToString();
                tmp.Text = CurrentYear.ToString();
                ddlyear.Items.Add(tmp);
                CurrentYear = DateTime.Now.AddYears(-i).Year;

            }

        }

        private void BindGradingReportSummary(int period)
        {
            try
            {
                int BranchID = -1;
                int year = Convert.ToInt32(ddlyear.SelectedValue);
                int month = Convert.ToInt32(ddlmonths.SelectedValue);
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                grdGradingRepportSummary.DataSource = AssignEntityManagement.GetGradingReportOfManagement(customerid,AuthenticationHelper.UserID, year, month, period, BranchID,null);
                grdGradingRepportSummary.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdGradingRepportSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int period = Convert.ToInt32(ddlPeriod.SelectedValue) + 1;
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "Legal Entity/Location";
                    e.Row.Cells[1].Text = "Approver Name";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    e.Row.Cells[0].Style.Add("width", "300px");
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    int margin = Convert.ToInt32(e.Row.Cells[0].Text.Split('#')[1]) * 20;
                    e.Row.Cells[0].Style.Add("padding-left", margin + "px");
                    e.Row.Cells[0].Text = "-" + e.Row.Cells[0].Text.Split('#')[0];
                    e.Row.Cells[1].Style.Add("width", "200px");
                    e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;

                    for (int i = 2; i <= period; i++)
                    {
                        if (e.Row.Cells[i].Text == "1")
                        {
                            // e.Row.Cells[i].BackColor = Color.Green;
                            //green
                            e.Row.Cells[i].Style.Add("background", "#00CC33");

                        }
                        else if (e.Row.Cells[i].Text == "2")
                        {
                            //e.Row.Cells[i].BackColor = Color.RosyBrown;
                            //brown
                            e.Row.Cells[i].Style.Add("background", "#FF9933");
                        }
                        else
                        {
                            //e.Row.Cells[i].BackColor = Color.Red;
                            //red
                            e.Row.Cells[i].Style.Add("background", "#FF0033");
                        }
                        e.Row.Cells[i].Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Tab1.CssClass == "Clicked")
            {
                lblTableTitel.Text = "Past " + ddlPeriod.SelectedValue + " Months's Summary";
                BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));
            }
            else if (Tab1.CssClass == "Initial")
            {
                lblTableTitelInternal.Text = "Past " + ddlPeriod.SelectedValue + " Months's Summary";
                BindGradingReportSummaryInternal(Convert.ToInt32(ddlPeriod.SelectedValue));
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (Tab1.CssClass == "Clicked")
            {
                try
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Dashboard");
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "GradingReport.xls"));
                        Response.ContentType = "application/ms-excel";
                        StringWriter sw = new StringWriter();
                        HtmlTextWriter htw = new HtmlTextWriter(sw);

                        string DashboardAsOn = "<h3>Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text + "</h3>";
                        sw.Write(DashboardAsOn);
                        sw.Write("<br/>");
                        sw.Write("Past " + ddlPeriod.SelectedValue + " Months's Summary");
                        grdGradingRepportSummary.HeaderRow.Style.Add("background-color", "#5c9ccc");
                        grdGradingRepportSummary.HeaderRow.BorderStyle = BorderStyle.Solid;
                        grdGradingRepportSummary.HeaderRow.BorderColor = Color.White;
                        grdGradingRepportSummary.RenderControl(htw);

                        Response.Write(sw.ToString());
                        Response.End();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            else if (Tab1.CssClass == "Initial")
            {
                try
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Dashboard");
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "InternalGradingReport.xls"));
                        Response.ContentType = "application/ms-excel";
                        StringWriter sw = new StringWriter();
                        HtmlTextWriter htw = new HtmlTextWriter(sw);

                        string DashboardAsOn = "<h3>Internal Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text + "</h3>";
                        sw.Write(DashboardAsOn);
                        sw.Write("<br/>");
                        sw.Write("Past " + ddlPeriod.SelectedValue + " Months's Summary");
                        grdGradingRepportSummaryInternal.HeaderRow.Style.Add("background-color", "#5c9ccc");
                        grdGradingRepportSummaryInternal.HeaderRow.BorderStyle = BorderStyle.Solid;
                        grdGradingRepportSummaryInternal.HeaderRow.BorderColor = Color.White;
                        grdGradingRepportSummaryInternal.RenderControl(htw);

                        Response.Write(sw.ToString());
                        Response.End();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (Tab1.CssClass == "Clicked")
            {
                try
                {
                    string Script = string.Empty;
                    StringWriter stringWrite = new StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
                    if (grdGradingRepportSummary is WebControl)
                    {
                        Unit w = new Unit(100, UnitType.Percentage); ((WebControl)grdGradingRepportSummary).Width = w;
                    }
                    Page pg = new Page();
                    pg.EnableEventValidation = false;
                    if (Script != string.Empty)
                    {
                        pg.ClientScript.RegisterStartupScript(pg.GetType(), "PrintJavaScript", Script);
                    }

                    HtmlForm frm = new HtmlForm();
                    pg.Controls.Add(frm);
                    frm.Attributes.Add("runat", "server");
                    Label management = new Label();
                    management.Text = "Management Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text;
                    management.Style.Add("margin-bottom", "10px");
                    management.Font.Bold = true;
                    frm.Controls.Add(management);
                    Label space = new Label();
                    space.Width = Unit.Percentage(100);
                    space.Height = Unit.Pixel(20);
                    Label location = new Label();
                    location.Text = "Logged in as on " + AuthenticationHelper.User;
                    location.Font.Bold = true;
                    frm.Controls.Add(location);
                    frm.Controls.Add(space);
                    frm.Controls.Add(grdGradingRepportSummary);
                    pg.DesignerInitialize();
                    pg.RenderControl(htmlWrite);
                    string strHTML = stringWrite.ToString();
                    strHTML = strHTML + "<style type='text/css'>th{background: #5c9ccc;}</style>";
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Write(strHTML);
                    HttpContext.Current.Response.Write("<script>window.onload= function () { window.print();window.close();   }  </script>");
                    HttpContext.Current.Response.End();
                }
                catch (Exception)
                {
                    throw;
                    //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            else if (Tab1.CssClass == "Initial")
            {
                try
                {
                    string Script = string.Empty;
                    StringWriter stringWrite = new StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
                    if (grdGradingRepportSummaryInternal is WebControl)
                    {
                        Unit w = new Unit(100, UnitType.Percentage); ((WebControl)grdGradingRepportSummaryInternal).Width = w;
                    }
                    Page pg = new Page();
                    pg.EnableEventValidation = false;
                    if (Script != string.Empty)
                    {
                        pg.ClientScript.RegisterStartupScript(pg.GetType(), "PrintJavaScript", Script);
                    }

                    HtmlForm frm = new HtmlForm();
                    pg.Controls.Add(frm);
                    frm.Attributes.Add("runat", "server");
                    Label management = new Label();
                    management.Text = "Internal Management Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text;
                    management.Style.Add("margin-bottom", "10px");
                    management.Font.Bold = true;
                    frm.Controls.Add(management);
                    Label space = new Label();
                    space.Width = Unit.Percentage(100);
                    space.Height = Unit.Pixel(20);
                    Label location = new Label();
                    location.Text = "Logged in as on " + AuthenticationHelper.User;
                    location.Font.Bold = true;
                    frm.Controls.Add(location);
                    frm.Controls.Add(space);
                    frm.Controls.Add(grdGradingRepportSummaryInternal);
                    pg.DesignerInitialize();
                    pg.RenderControl(htmlWrite);
                    string strHTML = stringWrite.ToString();
                    strHTML = strHTML + "<style type='text/css'>th{background: #5c9ccc;}</style>";
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Write(strHTML);
                    HttpContext.Current.Response.Write("<script>window.onload= function () { window.print();window.close();   }  </script>");
                    HttpContext.Current.Response.End();
                }
                catch (Exception)
                {
                    throw;
                    //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            if (Tab1.CssClass == "Clicked")
            {
                try
                {
                    if (!string.IsNullOrEmpty(emailids.Text))
                    {
                        string[] emailIsd = emailids.Text.Split(',');

                        BindGradingReportSummary(Convert.ToInt32(ddlPeriod.SelectedValue));

                        Document pdfDoc = new Document(PageSize.A4_LANDSCAPE, 20, 20, 20, 20);
                        DataTable dataToSummary = grdGradingRepportSummary.DataSource as DataTable;

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            writer.CloseStream = false;
                            pdfDoc.Open();

                            try
                            {
                                pdfDoc.Add(iTextSharp.text.Chunk.NEWLINE);
                                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Grading Report"));
                                paragraph.SpacingAfter = 10f;
                                pdfDoc.Add(paragraph);
                                paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text));
                                paragraph.SpacingAfter = 5f;
                                pdfDoc.Add(paragraph);

                                pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Past " + ddlPeriod.SelectedValue + " Months's Summary")) { SpacingAfter = 5f });

                                PdfPTable dataTable = new PdfPTable(dataToSummary.Columns.Count);

                                pdfDoc.Add(SetStyleForPDFDoc(dataTable, dataToSummary));

                            }
                            finally
                            {
                                pdfDoc.Close();
                            }

                            memoryStream.Position = 0;

                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();
                            List<Attachment> attachment = new List<Attachment>();
                            attachment.Add(new Attachment(new MemoryStream(bytes), "ManagementGradingReport.pdf"));

                            int customerID = -1;
                            string ReplyEmailAddressName = "";
                            if (AuthenticationHelper.Role == "SADMN")
                            {
                                ReplyEmailAddressName = "Avantis";
                            }
                            else
                            {
                                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                            }
                            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                             + "Dear Sir,<br /><br />"
                                             + "Here is Attached Management Grading Report pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Management Grading Report.", message, attachment);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#sendManagementEmail\").dialog('close')", true);
                        }
                    }
                    emailids.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            else if (Tab1.CssClass == "Initial")
            {
                try
                {
                    if (!string.IsNullOrEmpty(emailids.Text))
                    {
                        string[] emailIsd = emailids.Text.Split(',');

                        BindGradingReportSummaryInternal(Convert.ToInt32(ddlPeriod.SelectedValue));

                        Document pdfDoc = new Document(PageSize.A4_LANDSCAPE, 20, 20, 20, 20);
                        DataTable dataToSummary = grdGradingRepportSummaryInternal.DataSource as DataTable;

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                            writer.CloseStream = false;
                            pdfDoc.Open();

                            try
                            {
                                pdfDoc.Add(iTextSharp.text.Chunk.NEWLINE);
                                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatPageHeaderPhrase("Internal Grading Report"));
                                paragraph.SpacingAfter = 10f;
                                pdfDoc.Add(paragraph);
                                paragraph = new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Internal Grading Report as on " + ddlmonths.SelectedItem.Text + " " + ddlyear.SelectedItem.Text));
                                paragraph.SpacingAfter = 5f;
                                pdfDoc.Add(paragraph);

                                pdfDoc.Add(new iTextSharp.text.Paragraph(PDFExportHelper.FormatHeaderPhrase("Past " + ddlPeriod.SelectedValue + " Months's Summary")) { SpacingAfter = 5f });

                                PdfPTable dataTable = new PdfPTable(dataToSummary.Columns.Count);

                                pdfDoc.Add(SetStyleForPDFDoc(dataTable, dataToSummary));

                            }
                            finally
                            {
                                pdfDoc.Close();
                            }

                            memoryStream.Position = 0;

                            byte[] bytes = memoryStream.ToArray();
                            memoryStream.Close();
                            List<Attachment> attachment = new List<Attachment>();
                            attachment.Add(new Attachment(new MemoryStream(bytes), "InternalManagementGradingReport.pdf"));

                            int customerID = -1;
                            string ReplyEmailAddressName = "";
                            if (AuthenticationHelper.Role == "SADMN")
                            {
                                ReplyEmailAddressName = "Avantis";
                            }
                            else
                            {
                                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                                ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                            }
                            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                                             + "Dear Sir,<br /><br />"
                                             + "Here is Attached Internal Management Grading Report pdf.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(emailIsd), null, null, "Internal Management Grading Report.", message, attachment);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#sendManagementEmail\").dialog('close')", true);
                        }
                    }
                    emailids.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected PdfPTable SetStyleForPDFDoc(PdfPTable dataTable, DataTable dataToExport)
        {
            dataTable.DefaultCell.Padding = 3;
            dataTable.WidthPercentage = 100;//in percentage
            dataTable.DefaultCell.BorderWidth = 1;
            dataTable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            dataTable.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

            for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
            {
                dataTable.AddCell(PDFExportHelper.FormatHeaderPhrase(dataToExport.Columns[colIndex].ColumnName));
            }

            dataTable.HeaderRows = 0;  // this is the end of the table header

            for (int rowIndex = 0; rowIndex < dataToExport.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataToExport.Columns.Count; colIndex++)
                {
                    if (colIndex == 0)
                        dataTable.AddCell(PDFExportHelper.FormatPhrase(dataToExport.Rows[rowIndex][dataToExport.Columns[colIndex].ColumnName].ToString().Split('#')[0]));
                    else
                        dataTable.AddCell(PDFExportHelper.FormatPhrase(dataToExport.Rows[rowIndex][dataToExport.Columns[colIndex].ColumnName].ToString()));
                }
            }

            return dataTable;
        }


        //Internal 
        private void BindGradingReportSummaryInternal(int period)
        {
            try
            {
                int year = Convert.ToInt32(ddlyear.SelectedValue);
                int month = Convert.ToInt32(ddlmonths.SelectedValue);
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                grdGradingRepportSummaryInternal.DataSource = AssignEntityManagement.GetGradingReportOfManagementInternal(customerID,AuthenticationHelper.UserID, year, month, period,-1,null);
                grdGradingRepportSummaryInternal.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdGradingRepportSummaryInternal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int period = Convert.ToInt32(ddlPeriod.SelectedValue) + 1;
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "Legal Entity/Location";
                    e.Row.Cells[1].Text = "Approver Name";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    e.Row.Cells[0].Style.Add("width", "300px");
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    int margin = Convert.ToInt32(e.Row.Cells[0].Text.Split('#')[1]) * 20;
                    e.Row.Cells[0].Style.Add("padding-left", margin + "px");
                    e.Row.Cells[0].Text = "-" + e.Row.Cells[0].Text.Split('#')[0];
                    e.Row.Cells[1].Style.Add("width", "200px");
                    e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;

                    for (int i = 2; i <= period; i++)
                    {
                        if (e.Row.Cells[i].Text == "1")
                        {
                            // e.Row.Cells[i].BackColor = Color.Green;
                            //green
                            e.Row.Cells[i].Style.Add("background", "#00CC33");

                        }
                        else if (e.Row.Cells[i].Text == "2")
                        {
                            //e.Row.Cells[i].BackColor = Color.RosyBrown;
                            //brown
                            e.Row.Cells[i].Style.Add("background", "#FF9933");
                        }
                        else
                        {
                            //e.Row.Cells[i].BackColor = Color.Red;
                            //red
                            e.Row.Cells[i].Style.Add("background", "#FF0033");
                        }
                        e.Row.Cells[i].Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}