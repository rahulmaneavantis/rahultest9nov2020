﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class TopCountAPIDFM : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int DistID;        
        protected static int UId;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Path;       
        protected static bool IsNotCompiled;
        protected static string DisplayName;
        protected static string riskid;
        protected static string Status;               
        protected static string Authorization;
        protected static string RCode;
        protected static string FromDate;
        protected static string Enddate;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!IsPostBack)
            {                
                IsNotCompiled = false;
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);                             
                RCode = AuthenticationHelper.Role;                
                if (!string.IsNullOrEmpty(Request.QueryString["DistID"]))
                {
                    if (Request.QueryString["DistID"].ToString() != "-1")
                    {
                        DistID = Convert.ToInt32(Request.QueryString["DistID"]);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    if (Request.QueryString["customerid"].ToString() != "-1")
                    {
                        CustId = Convert.ToInt32(Request.QueryString["customerid"]);
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["STS"]))
                {
                    Status = Convert.ToString(Request.QueryString["STS"]);                    
                }


                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate = Convert.ToString(Request.QueryString["FromDate"]);
                }
                else
                {
                    FromDate = "01-01-1900";
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Enddate"]))
                {
                    Enddate = Convert.ToString(Request.QueryString["Enddate"]);
                }
                else
                {
                    Enddate = "01-01-1900";
                }
            }
        }
    }
}