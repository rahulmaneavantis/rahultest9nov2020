﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="MangementDashboard.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.MangementDashboard" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/SatutoryManagementDashboard.ascx" TagName="SatutoryManagementDashboard"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalManagementDashboard.ascx" TagName="InternalManagementDashboard"
    TagPrefix="vit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Dashboard.css" rel="stylesheet" type="text/css" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Statutory" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="Internal" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server" Visible="false"
                    OnClick="Tab2_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table style="width: 100%; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div>
                                        <div style="margin: 10px 20px 10px 105px">
                                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                            <div>
                                                <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                    OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="upComplianceDashboard" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <vit:SatutoryManagementDashboard ID="ucSatutoryManagementDashboard" runat="server" Visible="false"></vit:SatutoryManagementDashboard>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table style="width: 100%; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div style="margin: 10px 20px 10px 90px">
                                        <asp:Label ID="lblErrorMessageInternal" runat="server" Style="color: Red"></asp:Label>
                                        <div>
                                            <asp:RadioButtonList runat="server" ID="rdInternalaRoleList" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                OnSelectedIndexChanged="rdInternalaRoleList_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <vit:InternalManagementDashboard ID="UCInternalManagementDashboard" runat="server" Visible="false"></vit:InternalManagementDashboard>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
            initializeRadioButtonsList($("#<%= rdInternalaRoleList.ClientID %>"));
        });
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
