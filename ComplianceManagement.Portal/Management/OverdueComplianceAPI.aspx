﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OverdueComplianceAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.OverdueComplianceAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
  
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min1.1.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script src="../Scripts/KendoPage/OverdueCompliance_v9.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
          

    <style type="text/css">
 /*.k-treeview .k-item {
    display: block;
    border-width: 0;
    margin: 0;
    padding: 0 0 0 0px;
}*/
      .k-grid-content
        {
            min-height:400px !important;
             overflow: hidden  !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            margin: 0px 5px 0px 0px;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
        .myKendoCustomClass {
            z-index: 999 !important;
        }
        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }        
        .k-grid td {
            line-height: 2.0em;
        }
        .k-i-more-vertical:before {
            content: "\e006";
        }
        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #ceced2;
            background-color: #f6f6f6;
        }
        #grid .k-grid-toolbar {
            background: white;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: auto;
        }

        .k-grid-header-wrap 
        {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 0px 0 0;
            zoom: 1;
        }


        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 24%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {            
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
            margin-top: -3px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
          .change-condition {
            color: blue;
        }

        .k-grid-header th.k-with-icon .k-link {
            background-color: #F8F8F8;
        }

	 .k-grouping-header
     {
         margin-top: 0.5em;
         border-top: solid 1px;
         border-right: 1px solid;
         border-left: 1px solid;
         border-color: #ceced2;
         color: #515967;
           font-style: italic;
		}
        
        .k-state-default > .k-select {
        border-color: #ceced2;
        margin-top: 0px;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            font-stretch: 100%;
            font-style: normal;
            font-weight: 400;
        }

        k-multicheck-wrap 
        {
            overflow: auto;
            overflow-x: hidden;
            white-space: nowrap;
            max-height: 300px;
            width: 620px;
       }

        form.k-filter-menu {
           width: 96% !important;
        }
    </style>
   
   
    <script type="text/javascript">
        $(window).resize(function () {
            window.parent.forchild($("body").height() + 15);
        });
        function BindGrid()
         {
              var gridexist = $('#grid').data("kendoGrid");
             if (gridexist != undefined || gridexist != null)
                 $('#grid').empty();
 
             var grid = $("#grid").kendoGrid({
                 columnMenuInit(e) {
                     e.container.find('li[role="menuitemcheckbox"]:nth-child(11)').remove();
                 },
                
                 dataSource: {
                     transport: {
                         read: {
                             url: '<% =Path%>Data/GetMGMTOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val() + '&Isdept=0&IsApprover=<% =isapprover%>&RiskId=-1',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>Data/GetMGMTOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val() + '&Isdept=0&IsApprover=<% =isapprover%>&RiskId=-1'
                     },
                     schema: {
                         data: function (response) {
                             return response[0].SList;
                         },
                         total: function (response) {
                             return response[0].SList.length;
                         }
                     },
                     pageSize: 10
                 },
 
                filterable: true, 
                
                 height: 513,
                 sortable: true,
                 groupable: true,
                                               
                 
                 pageable: true,
                 reorderable: true,
                 resizable: true,
                 multi: true,
                 selectable: true,                
                 columnMenu: true,
 	        filterMenuInit: function(e){
                         if(e.field == "ActName")
                         {
                           e.container.css({"width": "200px"});
                         }
                       }, 
                 
                 columns: [
                    
                     { hidden: true, field: "ShortForm", title: "Short&nbsp;Form" },
                     { hidden: true, field: "RiskCategory", title: "Risk" },
                     { hidden: true, field: "CustomerBranchID", title: "BranchID" },  
                     { hidden: true, field: "Branch", title: "Location" },                         
                      {
                          field: "ActName", title: 'Act&nbsp;Name',
                          width: "25%",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {                             
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }                                               
                      },
                     {
                         field: "ShortDescription", title: 'Compliance',
                         width: "25%",
                         attributes: {
                             style: 'white-space: nowrap;'
                         },
                         filterable: {
                             multi: true,
                             extra: false,
                             search: true,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }                        
                     },
                      {
                          field: "Performer", title: 'Performer',                         
                        width: "15%;",
                         attributes: {
                             style: 'white-space: nowrap;'
 
                         }, filterable: {
                             multi: true,
                             search: true,
                            extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }                       
                     },
                     {
                     //    field: "ScheduledOn", title: 'Due&nbsp;Date',
                     //    type: "date",    
                     //    format: "{0:dd-MM-yyyy}",
                         
                         //},    
                         field: "ScheduledOn", title: 'Due&nbsp;Date',
                         type: "date",
                         format: "{0:dd-MMM-yyyy}",
                         filterable: {
                             multi: true,
                             search: true,
                             extra: false,
                             operators: {
                                 string: {
                                     type: "date",
                                     format: "{0:dd-MMM-yyyy}",
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                         
                     {
                         field: "OverdueBy", title: 'OverdueBy(Days)', 
                        filterable: {
                             multi: true,
                             search: true,
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                     {
                         field: "ForMonth", title: 'Period',                        
                         attributes: {
                             style: 'white-space: nowrap;'
 
                         }, filterable: {
                             multi: true,
                             search: true,
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }                      
                     },
                     {
                         command: [
                             {
                                 name: "edit2", text: "  ",title:"Action", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview",
                                 
                             }
                         ], title: "Action", width: "7%;", headerAttributes: {
                             style: "border-right: solid 1px #ceced2;"
                         }                       
                     }
                 ] 
 	
                
             });           
             $("#grid").kendoTooltip({                
                 filter: "th",
                 content: function (e) {
                     var target = e.target; // element for which the tooltip is shown 
                     return $(target).text();
                 }
             });
             $("#grid").kendoTooltip({
                 filter: "td",                
                 content: function (e) {
                     var target = e.target; // element for which the tooltip is shown 
                     if ($(target).text() == "  ") {
 
                         return "Action";
                     }
                     else
                     {
                         return $(target).text();
                     }
                    
 
                 }
             }).data("kendoTooltip")
          
               $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                 var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                 OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                 return true;
               });
               window.parent.forchild($("body").height() + 15);
        }

        function OpenOverViewpupMain(scheduledonid, instanceid) {         
            $('#divApiOverView').modal('show');
            $('#APIOverView').attr('width', '1150px');
            $('#APIOverView').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#APIOverView').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
        }
  
        function BindGridApply(e) {
            BindGrid();
            FilterGridStatutory();
            e.preventDefault();
        }

        $(document).ready(function () {          
            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            }); 

            BindGrid();

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                            { text: "2020-2021", value: "2020-2021" },
		            { text: "2019-2020", value: "2019-2020" },
		            { text: "2018-2019", value: "2018-2019" },
		            { text: "2017-2018", value: "2017-2018" },
                            { text: "2016-2017", value: "2016-2017" }                                                           
                ]
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                                     
                },
                dataSource: [
                     { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

          $("#dropdownlistRisk").data("kendoDropDownTree").value('0'); 
          $("#ClearfilterMain").show();
            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) { 
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
            
             $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<% =Flag%>"
                    }
                }
            });

            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("900");
                }
            });
    

             $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                 change: function (e) { 
                     FilterGridStatutory();
                     fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT"
                    }
                },dataBound: function(e) {
                    e.sender.list.width("1000");
                }
            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=MGMT"
                    },
                }
            });

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {    
                    FilterGridStatutory();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            
        });
        $('#ClearfilterMain').click(function (e) { ClearAllFilterMain(e) });
    </script>
    
    <title></title>
  </head>
    <body style="overflow-x:hidden;">
  <form>

    <div class="row">
        <div class="col-lg-12 col-md-12 colpadding0">
             <h1 style="height: 30px;
 background-color: #f8f8f8;margin-top: 0px;font-weight: bold;font-size: 19px;padding-top:5px;padding-left:5px;
 color: #666;margin-bottom: 12px;">Overdue Compliance</h1>
        </div>
            </div>

     <div id="example">
	            <div class="row">
	                <div class="toolbar">
	                    <input id="CustName" type="hidden" value="<% =CustomerName%>" />
	                    <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
	                    <div class="row" style="margin-bottom: -7px; margin-top: -6px;">
	                        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 370px;" />
	                        <input id="dropdownFY" style="width: 200px;" />
	                        <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 200px;" />
	                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 140px;" />
	                        <input id="Lastdatepicker" placeholder="End Date" cssclass="clsROWgrid" title="Lastdatepicker" style="width: 140px;" />
	                    </div>
	                </div>
	            </div>

         <div class="row" style="padding-top: 12px; padding-bottom: 6px;">
             <div style="float: left;"> <input id="dropdownACT" style="width: 370px;" />
             <input id="dropdownPastData" style="width: 200px;" />
             <input id="dropdownfunction" style="width: 200px;" />
             <input id="dropdownUser" data-placeholder="User" style="width: 200px;" /> </div>
            <div  style="float: left;">
                 <button id="export" onclick="exportReport(event)" class="k-button k-button-icontext hidden-on-narrow" style="float: left;background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none;" data-toggle="tooltip" title="Export to Excel"></button>
             <button id="Applyfilter" style="margin-left: 1%;float: left;margin-left: 1px; margin-right: 5px; height: 29px;padding-bottom: 5px; border-style: groove;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
             <%-- <button id="ClearfilterMain" style="float:right;margin-left: 1%;border-radius: 13px;margin-right: 114px;margin-left: 14px; height: 30px;padding-bottom: 5px; border-style: groove;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>--%>
            <button id="ClearfilterMain" style="float: left; margin-top: 0px;padding-top: 2px;padding-bottom: 3px;display: none;" ><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button><%--onclick="ClearAllFilterMain(event)"--%>
            </div>
         </div>
          <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
        <%{%>  
            <div class="row" style="padding-top: 2px;padding-bottom: 18px;">
            <input id="dropdownSequence" style="width: 355px; margin-right: 7px;" />
            </div>
          <%}%>
            
          
        <%--<div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;padding-top:3px;margin-top: 4px;"  id="filtersstoryboard">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" id="filtertype">&nbsp;</div>
        <div class=row style="padding-bottom: 0px;font-size: 12px;display:none;padding-top: 0px;margin-top: -6px;font-weight:bold;" id="filterrisk">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" id="filterstatus">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;padding-top:5px;margin-top: -7px;" id="filterUser">&nbsp;</div>
        <div id="grid" style="border: none;"></div> 
        
           <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>--%>
         <div class=row style="clear:both;height:15px;"></div>
        <div class=row style="padding-bottom: 15px;font-size: 12px;display:none;font-weight:bold;padding-top:3px;margin-top: -15px;" Id="filtersstoryboard">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtertype">&nbsp;</div>
        <div class=row style="padding-bottom: 0px;font-size: 12px;display:none;padding-top: 0px;margin-top: -11px;font-weight:bold;" Id="filterrisk">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterstatus">&nbsp;</div>
        <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;padding-top:5px;" Id="filterUser">&nbsp;</div>
        <div id="grid" style="border: none;"></div> 
        
           <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1220px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
    </div>   
  </form>
        </body>
 </html>

