﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class PersonResponsible_Dashboard : System.Web.UI.Page
    {
        int checkInternalapplicable = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblErrorMessage.Text = string.Empty;
                //Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;
                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    mst_User user = UserManagementRisk.GetByID(userID);

                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        mst_Customer customer = CustomerManagementRisk.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }
                }
                if (checkInternalapplicable == 1)
                {
                    //Tab2.Visible = true;
                }
                else if (checkInternalapplicable == 0)
                {
                    //Tab1.Visible = true;
                    //Tab2.Visible = false;
                }
                Tab1_Click(sender, e);
            }
        }

        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ucPersonResDashboard.Visible = true;
                //ucSatutoryManagementDashboard.DrowGraphs(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        protected void rdInternalaRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    UCInternalManagementDashboard.Visible = true;
            //    UCInternalManagementDashboard.DrowGraphs(true);

            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    lblErrorMessageInternal.Text = "Server Error Occured. Please try again.";
            //}
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            //Tab1.CssClass = "Clicked";
            //Tab2.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            ucPersonResDashboard.Visible = true;
            //ucSatutoryManagementDashboard.DrowGraphs(true);
        }

        //protected void Tab2_Click(object sender, EventArgs e)
        //{
        //    Tab1.CssClass = "Initial";
        //    //Tab2.CssClass = "Clicked";
        //    //MainView.ActiveViewIndex = 1;
        //    //UCInternalManagementDashboard.Visible = true;
        //    //UCInternalManagementDashboard.DrowGraphs(true);
        //}
    }
}