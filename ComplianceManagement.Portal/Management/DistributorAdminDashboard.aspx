﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="DistributorAdminDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.DistributorAdminDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Newjs/responsive-calendar.min.js"></script>
    <link href="../Content/css/Dashboard.css" rel="stylesheet" />
    <link href="../Content/css/calendar-custom.css" rel="stylesheet" />

    <style>
        #divComplianceStatusNew {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .k-grid-norecords {
            width: 100%;
            height: 80%;
            /* text-align: center; */
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: .2;
            filter: alpha(opacity=60);
            background-color: #6495ed;
            text-align: center;
        }

        .k-multiselect:after {
            content: "\25BC";
            position: absolute;
            top: 30%;
            right: 25px;
            font-size: 12px;
        }

        .k-multiselect.opened:after {
            content: "\25C0";
        }

        ::-webkit-input-placeholder {
            color: red;
        }

        .k-multiselect-wrap > .k-input, .k-input, .k-list .k-item {
            font-family: Montserrat !important;
            font-size: 14px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #444444;
        }

        .k-multiselect-wrap, .k-multiselect-wrap:hover {
            border-color: transparent;
        }

            .k-multiselect-wrap .k-readonly {
                opacity: 1;
            }
    </style>

    <!-- Calendar grid-->
    <style>
        #gridComplianceDetailsFromCalendar .pager-wrap {
            border-color: white;
        }

        #gridComplianceDetailsFromCalendar .k-pager-wrap .k-link.k-state-disabled {
            border-color: white;
        }

        #gridComplianceDetailsFromCalendar.k-pager-wrap > .k-link {
            border-style: none;
            border-radius: 0px;
        }

        #gridComplianceDetailsFromCalendar a.k-link.k-pager-nav {
            border-radius: 0px;
            border-style: none;
        }

        #gridComplianceDetailsFromCalendar .k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            border-color: #eeeeee;
        }

        #gridComplianceDetailsFromCalendar .k-state-selected {
            color: #0090d6;
            background-color: #e3f6ff;
            border-color: #e3f6ff;
            border-radius: 0px;
        }

        #gridComplianceDetailsFromCalendar.k-grid td {
            border-style: none;
        }

        #gridComplianceDetailsFromCalendar.k-widget {
            border-style: none;
        }

        #gridComplianceDetailsFromCalendar tr.k-alt {
            box-shadow: none;
            background-color: white;
        }

        #gridComplianceDetailsFromCalendar.k-alt {
            background-color: white;
        }

        #gridComplianceDetailsFromCalendar .k-pager-numbers .k-link {
            border-radius: 0px;
        }

        #gridComplianceDetailsFromCalendar.k-grid tr {
            box-sizing: border-box;
            box-shadow: #fff 0px 1px 0px 0px;
        }

        /*#gridComplianceDetailsFromCalendar .k-header {
        background-color: #F9F9F9;*/
        /*background-color: #f5f5f5;*/
        /*}*/

        #gridComplianceDetailsFromCalendar .k-grid-header-wrap {
            border-color: #f5f5f5;
        }

        #gridComplianceDetailsFromCalendar .k-grid-header {
            background-color: #f5f5f5;
            border-left-style: none;
            border-right-style: none;
            border-top-style: none;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        #gridComplianceDetailsFromCalendar.k-header {
            width: 125px;
            height: 18px;
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #000000;
        }

        #gridComplianceDetailsFromCalendar.k-grid td {
            font-family: Montserrat;
            font-size: 14px;
            color: #000000;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
        }

        #gridComplianceDetailsFromCalendar .k-pager-info {
            display: none;
        }


        .responsive-calendar .day {
            width: 11.5% !important;
            /*height: 52px !important;*/
        }
    </style>

    <!-- task grid-->
    <style>
        #gridKendoTask .k-grid-header-wrap {
            border-color: #f5f5f5;
        }

        #gridKendoTask .k-grid-header {
            background-color: #f5f5f5;
            border-left-style: none;
            border-right-style: none;
            border-top-style: none;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        #gridKendoTask.k-header {
            width: 125px;
            height: 18px;
            font-family: Montserrat;
            font-size: 14px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #000000;
        }

        #gridKendoTask.k-grid td {
            font-family: Montserrat;
            font-size: 14px;
            color: #000000;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
        }

        #gridKendoTask .k-pager-info {
            display: none;
        }

        #gridKendoTask.k-grid table {
            table-layout: fixed;
        }
    </style>

    <style>
        .Compliances-due-on-heading {
            height: 19px;
            font-family: Montserrat;
            font-size: 15px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #000000;
        }

        .Compliances-due-on-status {
            height: 18px;
            font-family: Montserrat;
            font-size: 14px;
            font-weight: normal;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #000000;
        }

        .Compliances-due-on-status-count {
            height: 19px;
            font-family: Montserrat;
            font-size: 16px;
            font-weight: 600;
            font-stretch: normal;
            font-style: normal;
            line-height: normal;
            letter-spacing: normal;
            color: #000000;
            padding-left: 10px;
        }

        input.myHiddenText {
            color: transparent;
            user-select: none;
        }
    </style>
    <script type="text/javascript">
        // replace/populate colors from user saved profile        
        var perFunctionChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium: "<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>"

        };
        var perRiskStackedColumnChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium: "<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>"
        };
        var perStatusChartColorScheme = {
            high: "<%=highcolor.Value %>",
            medium: "<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>",
            critical: "<%=criticalcolor.Value %>"
        };

        $(function () {
            // Chart Global options
            Highcharts.setOptions({
                credits: {
                    text: '',
                    href: 'https://www.avantis.co.in',
                },
                lang: {
                    drillUpText: "< Back",
                },
            });

            //perFunctionChart
            var perFunctionChart = Highcharts.chart('perFunctionChartDiv', {
                chart: {
                    type: 'column',
                    events: {
                        drilldown: function (e) {
                            this.setTitle({ text: e.point.name });
                            this.subtitle.update({ text: 'Click on graph to view documents' });
                        },
                        drillup: function () {
                            this.setTitle({ text: 'Per Function' });
                            this.subtitle.update({ text: 'Completion Status - Overall Functions' });
                        }
                    },
                },
                title: {
                    text: 'Per Function',
                    style: {
                        display: 'none'
                    },
                },
                subtitle: {
                    text: 'Completion Status - Overall Functions',
                    style: {
                        "font-family": 'Roboto',
                        fontWeight: '300',
                        fontSize: '15px'
                    }
                },
                xAxis: {
                    type: 'category',
                },
                yAxis: {
                    title: {
                        text: 'Number of Compliances'
                    },
                    //labels: {
                    //    enabled:false,
                    //},
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray',
                            }
                        },
                    }
                },
                tooltip: {
                    hideDelay: 0,
                    backgroundColor: 'rgba(247,247,247,1)',
                    headerFormat: '<b>{point.key}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
             <%=perFunctionChart%>
            });

                  var perStatusPieChart = Highcharts.chart('perStatusPieChartDiv', {
                      chart: {
                          type: 'pie',
                          events: {
                              drilldown: function (e) {
                                  this.setTitle({ text: e.point.name });
                                  // this.subtitle.update({ text: 'Click on graph to view documents' });
                                  this.subtitle.update({ text: 'Completion Status - By Risk' });
                              },
                              drillup: function () {
                                  this.setTitle({ text: 'Per Status' });
                                  //this.subtitle.update({ text: 'Click on graph to drilldown' });
                                  this.subtitle.update({ text: 'Completion Status' });
                              }
                          },
                      },
                      title: {
                          text: 'Per Status',
                          style: {
                              display: 'none'
                          },
                      },
                      subtitle: {
                          text: 'Completion Status - Overall',
                          style: {
                              fontWeight: '300',
                              fontSize: '15px'
                          }
                      },
                      xAxis: {
                          type: 'category',
                      },
                      plotOptions: {
                          series: {
                              dataLabels: {
                                  enabled: true,
                                  //format: '{y} <br>{point.name}',
                                  format: '{y}',
                                  distance: 5,
                              },
                              showInLegend: true,
                          },

                      },
                      legend: {
                          itemDistance: 2,
                      },
                      tooltip: {
                          hideDelay: 0,
                          backgroundColor: 'rgba(247,247,247,1)',
                          formatter: function () {
                              if (this.series.name == 'Status')
                                  return 'Click to Drilldown';	// text before drilldown
                              else
                                  return 'Click to View Documents';		// text after drilldown
                          }
                      },
                <%=perFunctionPieChart%>
            });



                  // change color in color picker according to chart selected
                  $('input[name=radioButton]').change(function () {
                      // destroy all color pickers
                      $('#highColorPicker').simplecolorpicker('destroy');
                      $('#mediumColorPicker').simplecolorpicker('destroy');
                      $('#lowColorPicker').simplecolorpicker('destroy');
                      $('#criticalColorPicker').simplecolorpicker('destroy');
                      // setting the colors for risks according to chart selected
                      var chart = $('input[name=radioButton]:checked').val();
                      switch (chart) {
                          case "perFunction":
                              $('#highColorPicker').val(perFunctionChartColorScheme.high);
                              $('#mediumColorPicker').val(perFunctionChartColorScheme.medium);
                              $('#lowColorPicker').val(perFunctionChartColorScheme.low);
                              $('#criticalColorPicker').val(perFunctionChartColorScheme.critical);
                              break;
                          case "perRisk":
                              $('#highColorPicker').val(perRiskStackedColumnChartColorScheme.high);
                              $('#mediumColorPicker').val(perRiskStackedColumnChartColorScheme.medium);
                              $('#lowColorPicker').val(perRiskStackedColumnChartColorScheme.low);
                              $('#criticalColorPicker').val(perRiskStackedColumnChartColorScheme.critical);
                              break;
                          case "perStatus":
                              $('#highColorPicker').val(perStatusChartColorScheme.high);
                              $('#mediumColorPicker').val(perStatusChartColorScheme.medium);
                              $('#lowColorPicker').val(perStatusChartColorScheme.low);
                              $('#criticalColorPicker').val(perStatusChartColorScheme.critical);
                              break;
                          default:
                              $('#highColorPicker').val('#7CB5EC');
                              $('#mediumColorPicker').val('#434348');
                              $('#lowColorPicker').val('#90ED7D');
                              $('#criticalColorPicker').val('#CC0900');
                      }

                      // initialise the color piskers again
                      $('#highColorPicker').simplecolorpicker({ picker: true });
                      $('#mediumColorPicker').simplecolorpicker({ picker: true });
                      $('#lowColorPicker').simplecolorpicker({ picker: true });
                      $('#criticalColorPicker').simplecolorpicker({ picker: true });
                  });
                

                  function fpopulateddata(type, attribute, customerid, branchid, fromdate, enddate, filter, functionid, internalsatutory, chartname, listcategoryid, userid, isapprover, DisplayName) {
                      $('#divreports').modal('show');
                      debugger;
                      $('#showdetails').attr('width', '100%');
                      $('#showdetails').attr('height', '690px');
                      $('.modal-dialog').css('width', '98%');
                      $('#showdetails').attr('src', "../Management/SatutoryManagementAPIDFM.aspx?pointname=" + type + "&attrubute=" + attribute + "&customerid=" + customerid + "&branchid=" + branchid + "&FromDate=" + fromdate + "&Enddate=" + enddate + "&Filter=" + filter + "&functionid=" + functionid + "&ChartName=" + chartname + "&Internalsatutory=" + internalsatutory + "&listcategoryid=" + listcategoryid + "&Userid=" + userid + "&IsApprover=false&DisplayName=" + DisplayName);
                  }
                  
                  //function fFunctions(customerid, branchid, IsSatutoryInternal, isapprover) {
                  //    if (Displays() == true) {
                  //        $('#divreports').modal('show');
                  //        $('#showdetails').attr('width', '100%');
                  //        $('#showdetails').attr('height', '690px');
                  //        $('.modal-dialog').css('width', '98%');
                  //        $('#showdetails').attr('src', "../Management/TopCountAPIDFM.aspx?customerid=" + customerid + "&branchid=" + branchid + "&Internalsatutory=" + IsSatutoryInternal + "&IsApprover=" + isapprover);
                  //    }
                  //}
        });

        function forchild(hgt) {
            $('#showdetails').attr('height', 10 + (hgt));
            $('#showdetails').css('height', 10 + (hgt));
        }

        function fFunctions(status, DistID, fromdate, enddate, customerid) {
                    
            //if (Displays() == true) {
            //$('#showdetails').attr('src', "../Penalty/PenaltyDetailsAPI.aspx?customerid=" + customerid + "&branchid=" + branchid + "&Internalsatutory=" + IsSatutoryInternal + "&IsApprover=" + isapprover + "&DisplayName=topPnsummary");
            $('#divreports').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '690px');
            $('.modal-dialog').css('width', '98%');
            $('#showdetails').attr('src', "../Management/TopCountAPIDFM.aspx?STS=" + status + "&customerid=" + customerid + "&DistID=" + DistID + "&FromDate=" + fromdate + "&Enddate=" + enddate);
            //}
        }

        function fEntitys() {
            //if (Displays() == true) {
            //$('#showdetails').attr('src', "../Penalty/PenaltyDetailsAPI.aspx?customerid=" + customerid + "&branchid=" + branchid + "&Internalsatutory=" + IsSatutoryInternal + "&IsApprover=" + isapprover + "&DisplayName=topPnsummary");
            $('#divreports').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '690px');
            $('.modal-dialog').css('width', '98%');
            $('#showdetails').attr('src', "../Management/TOPEntityCount.aspx");
            //}
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <%--   <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
    <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952" />
    <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1" />
    <asp:HiddenField ID="criticalcolor" runat="server" Value="#f1c232" />--%>

      <%-- <asp:HiddenField ID="highcolor" runat="server" Value="#ff0000" />
    <asp:HiddenField ID="mediumcolor" runat="server" Value="#ffd966" />
    <asp:HiddenField ID="lowcolor" runat="server" Value="#6aa84f" />
    <asp:HiddenField ID="criticalcolor" runat="server" Value="#CC0900" />--%>
     <asp:HiddenField ID="highcolor" runat="server" Value="#e16e6a" />
    <asp:HiddenField ID="mediumcolor" runat="server" Value="#ffd877" />
    <asp:HiddenField ID="lowcolor" runat="server" Value="#5ECAB9" />
    <asp:HiddenField ID="criticalcolor" runat="server" Value="#CC0900" />
    
    <div class="row shapeCounts" style="background-color: white;">
        <div class="col-md-12 colpadding0">
            <div class="">
                <table style="width: 100%; margin-top: 30px;">
                    <tr>
                        <td style="width: 20%;">
                           <%-- <a href="" role="button">--%>
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-6 colpadding0">
                                        <div style="float: right;">
                                            <img src="../Areas/BM_Management/img/Dashboard/entities.svg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 borderright">
                                        <div id="divGroupClientCount" class="dashboardCount" runat="server">
                                            0
                                        </div>
                                        <div class="top-count-label">Group/Client</div>
                                    </div>
                                </div>
                          <%--  </a>--%>
                        </td>
                        <td style="width: 20%;">
                          <%--  <a href="" role="button"> fEntitys --%>
                                <div class="col-md-12 colpadding0" onclick="fEntitys()">
                                    <div class="col-md-6 colpadding0">
                                        <div style="float: right;">
                                            <img src="../Areas/BM_Management/img/Dashboard/directors.svg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 borderright">
                                        <div id="divEntitiesCount" class="dashboardCount" runat="server">
                                            0
                                        </div>
                                        <div class="top-count-label">Entities</div>
                                    </div>
                                </div>
                         <%--   </a>--%>
                        </td>
                        <td style="width: 20%;">
                          <%--  <a href="">--%>
                                <div class="col-md-12 colpadding0" onclick="fFunctions('Upcoming',<%=com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID%>,'<%=FromFinancialYearSummery%>','<%=ToFinancialYearSummery%>',<%=ddlCustomer.SelectedValue%>)">
                                    <div class="col-md-6 colpadding0">
                                        <div style="float: right;">
                                            <img src="../Areas/BM_Management/img/Dashboard/meetings.svg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 borderright" >
                                        <div id="divUpcomingCount" class="dashboardCount" runat="server"  >
                                            0
                                        </div>
                                        <div class="top-count-label">Upcoming</div>
                                    </div>
                                </div>
                           <%-- </a>--%>
                        </td>
                        <td style="width: 20%;">
                          <%--  <a href="" role="button">--%>
                                <div class="col-md-12 colpadding0" onclick="fFunctions('PR',<%=com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID%>,'<%=FromFinancialYearSummery%>','<%=ToFinancialYearSummery%>',<%=ddlCustomer.SelectedValue%>)">
                                    <div class="col-md-6 colpadding0">
                                        <div style="float: right;">
                                            <img src="../Areas/BM_Management/img/Dashboard/agendas.svg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 borderright" >
                                        <div id="divPendingReviewCount" class="dashboardCount" runat="server">
                                            0
                                        </div>
                                        <div class="top-count-label">Pending Review</div>
                                    </div>
                                </div>
                          <%--  </a>--%>
                        </td>
                        <td style="width: 20%;">
                         <%--   <a href="">--%>
                                <div class="col-md-12 colpadding0" onclick="fFunctions('Overdue',<%=com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID%>,'<%=FromFinancialYearSummery%>','<%=ToFinancialYearSummery%>',<%=ddlCustomer.SelectedValue%>)">
                                    <div class="col-md-5 colpadding0">
                                        <div style="float: right;">
                                            <img src="../Areas/BM_Management/img/Dashboard/draft-minutes.svg" />
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="divOverdueCount" class="dashboardCount" runat="server">
                                            0
                                        </div>
                                        <div class="top-count-label">Overdue</div>
                                    </div>
                                </div>
                          <%--  </a>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div id="divDashboard" style="padding-left: 15px; padding-right: 15px;">
        <div class="row mt30">
            <div class="col-md-12 colpadding0">
                <label class="box-header">Filters</label>
            </div>
        </div>
        <div class="row box-shape" style="height: 50px; margin-top: 10px;">
            <div class="col-md-12 vertical-center">
                <div class="col-md-2 colpadding0" style="padding-left: 0px; margin-bottom: 15px; width: 20%">
                   
                    <asp:DropDownListChosen ID="ddlCustomer" CssClass="form-control" runat="server" Width="100%" AutoPostBack="true"  AllowSingleDeselect="false"
                        OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownListChosen>
                </div>

                <div class="col-md-2 colpadding0" style="padding-left: 10px; margin-bottom: 15px; width: 20%">
                     <asp:DropDownListChosen ID="ddlDateSummery" CssClass="form-control" runat="server" AutoPostBack="true" Width="100%" AllowSingleDeselect="false"
                        OnSelectedIndexChanged="ddlDateSummery_SelectedIndexChanged">
                        <asp:ListItem Value="0">Current Financial YTD</asp:ListItem>
                        <asp:ListItem Value="1">Current + Previous Financial YTD</asp:ListItem>
                        <asp:ListItem Value="2">All</asp:ListItem>
                      </asp:DropDownListChosen>
                </div>

                <div class="col-md-2 colpadding0">                                                                           
                </div>

               
               
                <div class="col-md-2 text-right">
                    <asp:Button ID="btnSelectAndProceed" runat="server" Text="Go to Client" visible="false"  class="btn btn-primary fa fa-arrow-right" OnClick="btnSelectAndProceed_Click"></asp:Button>
                   <%-- <a class="btn btn-primary" id="btnSelectAndProceed" runat="server" visible="false"   >Go to Client<span class="AddNewspan1">
                        <i class="fa fa-arrow-right"></i></span></a>--%>
                </div>
            </div>
        </div>

        <div class="row mt30">
            <div class="col-md-12 colpadding0">
                <label class="box-header">Compliance Performance</label>
            </div>
            <div class="col-md-12">
                <div id="perStatusPieChartDiv" class="col-md-5" style="margin-left: -30px; width: 36.6%">
                </div>
                <div id="perFunctionChartDiv" class="col-md-7" style="margin-left: -8px; width: 65.3%">
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/highcharts.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/drilldown.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/avantischarts/exporting.js"></script>

       <div class="modal fade" id="divreports" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 98%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="margin-top:-5px;" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">

                    <iframe id="showdetails" src="blank.html"  width="100%" height="100%" frameborder="0" style="height: 692px"></iframe>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
