﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class SatutoryManagementAPI : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Path;
        protected static string listcategoryid;
        protected static string pointname;
        protected static string attribute;
        protected static string branchid;
        protected static string FromDate;
        protected static string Enddate;
        protected static string Filter;
        protected static string ChartName;
        protected static string Internalsatutory;
        protected static string IsDeptHead;
        protected static string CustomerName;
        protected static int functionid;
        protected static int Departmentid;
        protected static int ChartNameID;
        protected static bool IsApprover;
        protected static string rolename;
        protected static bool IsNotCompiled;
        protected static string DisplayName;
        protected static string riskid;
        protected static string StatusID;
        protected static string StatusID1;
        protected static string StatusID2;
        protected static string StatusID3;
        protected static string StatusID4;
        protected static string NewStatus;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!IsPostBack)
            {
                Departmentid = 0;
                IsNotCompiled = false;
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                RoleFlag = 0;
                CustomerName = CustomerManagement.CustomerGetByIDName(CustId);

                if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                {
                    pointname = Request.QueryString["pointname"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["attrubute"]))
                {
                    attribute = Request.QueryString["attrubute"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    branchid = Convert.ToString(Request.QueryString["branchid"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate = Convert.ToString(Request.QueryString["FromDate"]);
                }
                else
                {
                    FromDate = "01-01-1900";
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Enddate"]))
                {
                    Enddate = Convert.ToString(Request.QueryString["Enddate"]);
                }
                else
                {
                    Enddate = "01-01-1900";
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Filter"]))
                {
                    Filter = Request.QueryString["Filter"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["functionid"]))
                {
                    functionid = Convert.ToInt32(Request.QueryString["functionid"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    Internalsatutory = Request.QueryString["Internalsatutory"];
                }
                IsDeptHead = "0";
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    IsDeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                if (IsDeptHead == "1")
                {
                    rolename = "DEPT";
                }
                else
                {
                    rolename = "MGMT";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ChartName"]))
                {
                    ChartName = Request.QueryString["ChartName"];
                    if (!string.IsNullOrEmpty(ChartName))
                    {
                        if (ChartName.Equals("FunctionPiechart"))
                            ChartNameID = 1;
                        else if (ChartName.Equals("Functionbarchart"))
                            ChartNameID = 2;
                        else if (ChartName.Equals("RiskBARchart"))
                            ChartNameID = 3;
                        else if (ChartName.Equals("DeptFunctionPieChart"))
                            ChartNameID = 4;
                        else if (ChartName.Equals("DeptFunctionBarChart"))
                            ChartNameID = 5;
                        else if (ChartName.Equals("DeptRiskBarChart"))
                            ChartNameID = 6;

                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["listcategoryid"]))
                {
                    listcategoryid = Request.QueryString["listcategoryid"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }

                Departmentid = 0;
                string CategoryDepartmentName = string.Empty;
                if (functionid != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["DisplayName"]))
                        {
                            DisplayName = Convert.ToString(Request.QueryString["DisplayName"]);
                        }
                        if (DisplayName == "DH_summary" || DisplayName == "DL_summary" || DisplayName == "DM_summary" || DisplayName == "DC_summary")
                        {
                            CategoryDepartmentName = (from row in entities.Departments
                                                      where row.ID == functionid
                                                      select row.Name).FirstOrDefault();
                           Departmentid = functionid;
                           functionid = 0;
                        }
                        else
                        {
                            CategoryDepartmentName = (from row in entities.ComplianceCategories
                                                      where row.ID == functionid
                                                      select row.Name).FirstOrDefault();

                        }
                    }
                }
                if (!string.IsNullOrEmpty(CategoryDepartmentName))
                {
                    CategoryDepartmentName = " > " + CategoryDepartmentName;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["DisplayName"]))
                {
                    DisplayName = Convert.ToString(Request.QueryString["DisplayName"]);


                    if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                    {
                        if (DisplayName == "FH_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not completed > High";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not complied > High";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > High";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > High";
                            }
                        }
                        if (DisplayName == "FM_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not completed > Medium";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not complied > Medium";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Medium";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Medium";
                            }
                        }
                        if (DisplayName == "FL_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not completed > Low";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not complied > Low";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Low";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Low";
                            }
                        }
                        if (DisplayName == "FC_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not completed > Critical";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > Not complied > Critical";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Critical";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Critical";
                            }
                        }


                        else if (DisplayName == "P_INH_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > High ";
                        }
                        else if (DisplayName == "P_INM_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Medium";
                        }
                        else if (DisplayName == "P_INL_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Low";
                        }
                        else if (DisplayName == "P_INC_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > In Time > Critical";
                        }


                        else if (DisplayName == "P_AFH_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > High";
                        }

                        else if (DisplayName == "P_AFM_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Medium";
                        }

                        else if (DisplayName == "P_AFL_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Low";
                        }

                        else if (DisplayName == "P_AFC_Psummary")
                        {
                            display.InnerText = "Overall completion status " + CategoryDepartmentName + " > After due date > Critical";
                        }


                        else if (DisplayName == "P_NCH_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not completed > High";
                        }
                        else if (DisplayName == "P_NCM_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not completed > Medium";
                        }
                        else if (DisplayName == "P_NCL_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not completed > Low";
                        }
                        else if (DisplayName == "P_NCC_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not completed > Critical";
                        }

                        else if (DisplayName == "P_NCOH_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not complied > High";
                        }
                        else if (DisplayName == "P_NCOM_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not complied > Medium";
                        }
                        else if (DisplayName == "P_NCOL_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not complied > Low";
                        }
                        else if (DisplayName == "P_NCOC_Psummary")
                        {
                            display.InnerText = "Overall completion status > Not complied > Critical";
                        }

                        else if (DisplayName == "R_INH_Rsummary")
                        {
                            display.InnerText = "Risk summary > In Time > High";
                        }
                        else if (DisplayName == "R_INM_Rsummary")
                        {
                            display.InnerText = "Risk summary > In Time > Medium ";
                        }
                        else if (DisplayName == "R_INL_Rsummary")
                        {
                            display.InnerText = "Risk summary > In Time > Low";
                        }
                        else if (DisplayName == "R_INC_Rsummary")
                        {
                            display.InnerText = "Risk summary > In Time > Critical";
                        }


                        else if (DisplayName == "R_AFH_Rsummary")
                        {
                            display.InnerText = "Risk summary > After due date > High";
                        }
                        else if (DisplayName == "R_AFM_Rsummary")
                        {
                            display.InnerText = "Risk summary > After due date > Medium";
                        }
                        else if (DisplayName == "R_AFL_Rsummary")
                        {
                            display.InnerText = "Risk summary for > After due date > Low";
                        }
                        else if (DisplayName == "R_AFC_Rsummary")
                        {
                            display.InnerText = "Risk summary for > After due date > Critical";
                        }


                        else if (DisplayName == "R_NCH_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not completed > High";
                        }
                        else if (DisplayName == "R_NCM_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not completed > Medium";
                        }
                        else if (DisplayName == "R_NCL_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not completed > Low";
                        }
                        else if (DisplayName == "R_NCC_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not completed > Critical";
                        }

                        else if (DisplayName == "R_NCOH_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not complied > High";
                        }
                        else if (DisplayName == "R_NCOM_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not complied > Medium";
                        }
                        else if (DisplayName == "R_NCOL_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not complied > Low";
                        }
                        else if (DisplayName == "R_NCOC_Rsummary")
                        {
                            display.InnerText = "Risk summary > Not complied > Critical";
                        }

                        if (DisplayName == "DH_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not completed > High";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not complied > High";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > After due date > High";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > In Time > High";
                            }
                        }

                        if (DisplayName == "DM_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not completed > Medium";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not complied > Medium";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > After due date > Medium";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > In Time > Medium";
                            }
                        }

                        if (DisplayName == "DL_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not completed > Low";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not complied > Low";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > After due date > Low";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > In Time > Low";
                            }
                        }

                        if (DisplayName == "DC_summary")
                        {
                            if (pointname == "Not completed")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not completed > Critical";
                            }
                            if (pointname == "Not complied")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > Not complied > Critical";
                            }
                            if (pointname == "After due date")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > After due date > Critical";
                            }
                            if (pointname == "In Time")
                            {
                                display.InnerText = "Department summary " + CategoryDepartmentName + " > In Time > Critical";
                            }
                        }

                    }
                    

                    if (display.InnerText.Contains("High"))
                    {
                        riskid = "0";
                    }
                    else if (display.InnerText.Contains("Medium"))
                    {
                        riskid = "1";
                    }
                    else if (display.InnerText.Contains("Low"))
                    {
                        riskid = "2";
                    }
                    else if (display.InnerText.Contains("Critical"))
                    {
                        riskid = "3";
                    }


                    if (display.InnerText.Contains("Not completed"))
                    {
                        StatusID = "Overdue";
                        StatusID1 = "PendingForReview";
                        StatusID2 = "Rejected";
                        StatusID3 = "InProgress";
                        StatusID4 = "Not  Applicable";
                    }
                    else if (display.InnerText.Contains("In Time"))
                    {
                        StatusID = "ClosedTimely";
                        StatusID1 = "Not Applicable";
                        StatusID2 = "";
                        StatusID3 = "";
                        StatusID4 = "";
                    }
                    else if (display.InnerText.Contains("After due date"))
                    {
                        StatusID = "ClosedDelayed";
                        StatusID1 = "No";
                        StatusID2 = "No";
                        StatusID3 = "No";
                        StatusID4 = "No";
                    }
                    else if (display.InnerText.Contains("Not complied"))
                    {
                        StatusID = "Not Complied";
                        StatusID1 = "No";
                        StatusID2 = "No";
                        StatusID3 = "No";
                        StatusID4 = "No";
                    }
                    else
                    {
                        StatusID = "-1";
                    }
                }
                //Status = "All";
                // STT Change- Add Status
                string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == CustId.ToString())
                        {
                            IsNotCompiled = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}
