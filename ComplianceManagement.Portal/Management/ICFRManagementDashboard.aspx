﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="ICFRManagementDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ICFRManagementDashboard" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- Offline-->
    <script type="text/javascript" src="../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="../avantischarts/highcharts/js/modules/exporting.js"></script>--%>
    <script type="text/javascript" src="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../newjs/spectrum.js"></script>

    <link href="../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../newcss/spectrum.css" rel="stylesheet" />
    <style type="text/css">
        .dd_chk_select {
            height: 34px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            width:90% !important;            
        }
        div.dd_chk_drop {
                top: 32px !important; 
        }
          .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
    
    <style type="text/css">
        #ContentPlaceHolder1_grdAuditTrackerSummary.table tr td {
            border: 1px solid white;
        }

        .colorPickerWidget {
            padding: 10px;
            margin: 10px;
            text-align: center;
            width: 360px;
            border-radius: 5px;
            background: #fafafa;
            border: 2px solid #ddd;
        }
    </style>

    <style type="text/css">
        ol.progtrckr {
            margin: 0;
            padding: 0;
            list-style-type none;
        }

            ol.progtrckr li {
                display: inline-block;
                text-align: center;
                line-height: 3em;
            }

            ol.progtrckr[data-progtrckr-steps="2"] li {
                width: 49%;
            }

            ol.progtrckr[data-progtrckr-steps="3"] li {
                width: 33%;
            }

            ol.progtrckr[data-progtrckr-steps="4"] li {
                width: 24%;
            }

            ol.progtrckr[data-progtrckr-steps="5"] li {
                width: 19%;
            }

            ol.progtrckr[data-progtrckr-steps="6"] li {
                width: 16%;
            }

            ol.progtrckr[data-progtrckr-steps="7"] li {
                width: 14%;
            }

            ol.progtrckr[data-progtrckr-steps="8"] li {
                width: 12%;
            }

            ol.progtrckr[data-progtrckr-steps="9"] li {
                width: 11%;
            }

            ol.progtrckr li.progtrckr-done {
                color: black;
                border-bottom: 4px solid yellowgreen;
            }

            ol.progtrckr li.progtrckr-todo {
                color: silver;
                border-bottom: 4px solid silver;
            }

            ol.progtrckr li.progtrckr-current {
                color: black;
                border-bottom: 4px solid #A16BBE;
            }

            ol.progtrckr li:after {
                content: "\00a0\00a0";
            }

            ol.progtrckr li:before {
                position: relative;
                bottom: -2.5em;
                float: left;
                left: 50%;
                line-height: 1em;
            }

            ol.progtrckr li.progtrckr-done:before {
                content: "\2714";
                color: white;
                background-color: yellowgreen;
                height: 1.2em;
                width: 1.2em;
                line-height: 1.2em;
                border: none;
                border-radius: 1.2em;
            }

            ol.progtrckr li.progtrckr-todo:before {
                content: "\039F";
                color: silver;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }

            ol.progtrckr li.progtrckr-current:before {
                content: "\039F";
                color: #A16BBE;
                background-color: white;
                font-size: 1.5em;
                bottom: -1.6em;
            }
    </style>

    <script type="text/javascript">

        function fpopulateddataprocess(keynonkey, type, customerid, branchid, FinYear, period, processid, subprocessid, IsMgmt) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../Management/ICFRProcessObservationDetails.aspx?Type=" + type + "&keynonkey=" + keynonkey + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + FinYear + "&period=" + period + "&processid=" + processid + "&subprocessid=" + subprocessid + "&ISMGMT=" + IsMgmt);
        }
        function fpopulateddataQuarter(keynonkey, type, customerid, branchid, FinYear, period, isqhya, IsMgmt) {
            $('#DivReports').modal('show');
            $('#showdetails').attr('width', '1150px');
            $('#showdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#showdetails').attr('src', "../Management/ICFRObservationDetails.aspx?Type=" + type + "&keynonkey=" + keynonkey + "&customerid=" + customerid + "&branchid=" + branchid + "&FinYear=" + FinYear + "&period=" + period + "&ISQHYA=" + isqhya + "&ISMGMT=" + IsMgmt);
        }


        function closeDiv(id) {
            document.getElementById(id + 'Checkbox').className = 'menucheckbox-notchecked';
            document.getElementById(id).style.display = 'none';
            // do something
        }

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

        var ObservationStatusColorScheme = {
            high: "<%=highcolor.Value %>",
            medium: "<%=mediumcolor.Value %>",
            low: "<%=lowcolor.Value %>"
        };


        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                <%=ProcessWiseObservationStatusChart%>
            });
        });

    </script>
    <script type="text/javascript">
        $(function () {
            // Chart Global options
            Highcharts.setOptions({
                credits: {
                    text: '',
                    href: 'https://www.avantis.co.in',
                },
                lang: {
                    drillUpText: "< Back",
                },
            });
            var perStatusPieChartQ1 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivAPRJUN', {
                <%=perStatusPieChartAPRJUN%>
            });
            var perStatusPieChartQ2 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivJULSEP', {
                <%=perStatusPieChartJULSEP%>
            });
            var perStatusPieChartQ3 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivOCTDEC', {
                <%=perStatusPieChartOCTDEC%>
            });
            var perStatusPieChartQ4 = Highcharts.chart('ContentPlaceHolder1_perStatusPieChartDivJANMAR', {
                <%=perStatusPieChartJANMAR%>
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Quaterly Wise- Failed Control   

                var QuarterlyFailedControlChart = Highcharts.chart('QuarterlyFailedControlColumnChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    <%=QuarterlyFailedControlStatusChart%>

                });
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                // Chart Global options
                Highcharts.setOptions({
                    credits: {
                        text: '',
                        href: 'https://www.avantis.co.in',
                    },
                    lang: {
                        drillUpText: "< Back",
                    },
                });

                //Process Wise- Failed Control   

                var ProcessWiseFailedControlChart = Highcharts.chart('DivProcessWiseFailedControlColumnChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },

                    <%=ProcessWiseFailedControlStatusChart%>

                });
            });
        });
    </script>

    <style type="text/css">
        .progressno {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="container" class="">
            <section class="wrapper">       
                
                 <asp:UpdatePanel ID="upDivFilters" runat="server" UpdateMode="Conditional">       
                          <ContentTemplate>  
                                   
                          <div id="DivFilters" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Filters </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#ContentPlaceHolder1_collapseDivFilters"><i class="fa fa-chevron-down"></i></a>
                                        <a href="javascript:closeDiv('DivFilters')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseDivFilters" class="panel-collapse collapse" runat="server">     
                                 <div class="panel-body">
                           <div class="col-md-12 colpadding0">

                               <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width:20%">                                    
                                     <%--<asp:DropDownList runat="server" ID="ddlLegalEntity" PlaceHolder="Select Legal Entity"  class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownList>--%>
                                    <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Legal Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                            AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">
                                    
                                    <%--<asp:DropDownList runat="server" ID="ddlSubEntity1" PlaceHolder="Select Sub Entity" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownList>   --%> 
                                        <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                  
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                   
                                 <%--   <asp:DropDownList runat="server" ID="ddlSubEntity2" PlaceHolder="Select Sub Entity" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownList> --%> 
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                         
                                </div> 
                                                                                                
                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                                    
                                     <%--<asp:DropDownList runat="server" ID="ddlSubEntity3" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownList>   --%> 
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Entity" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                               AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>                                   
                                </div>

                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                        <%--<asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownList>--%>
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" DataPlaceHolder="Sub Entity" AutoPostBack="true"
                                  AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                    </div> 

                          </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0"> 
                                                                  
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                      <%--  <asp:DropDownList ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Style="float: left; width:90%;"
                                         OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownList> --%>   
                                             <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                     AllowSingleDeselect="false" DisableSearchThreshold="3"   OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged" DataPlaceHolder="Financial Year">
                                        </asp:DropDownListChosen>                                                        
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                         <asp:DropDownList runat="server" ID="ddlSchedulingType" 
                                         class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                            <asp:ListItem>Annually</asp:ListItem>
                                            <asp:ListItem>Half Yearly</asp:ListItem>
                                            <asp:ListItem Selected="True">Quarterly</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>                                                                        
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                        <%--<asp:DropDownList runat="server" ID="ddlProcess" AutoPostBack="true" 
                                            OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" 
                                             class="form-control m-bot15 select_location" 
                                            Style="float: left; width:90%;">
                                        </asp:DropDownList>--%>
                                          <asp:DropDownListChosen runat="server" ID="ddlProcess" DataPlaceHolder="Process" AutoPostBack="true"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlProcess_SelectedIndexChanged" class="form-control m-bot15 select_location" Width="90%" Height="32px">
                                    </asp:DropDownListChosen>
                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width:20%">                     
                                       <%-- <asp:DropDownList runat="server" ID="ddlSubProcess"  class="form-control m-bot15 select_location" 
                                        Style="float: left; width:90%;">
                                        </asp:DropDownList>--%>
                                         <asp:DropDownListChosen ID="ddlSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  DataPlaceHolder="Sub Process">
                                    </asp:DropDownListChosen>  
                                    </div> 
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float:right;width: 13%;"> 
                                    <asp:Button ID="btnFilter" class="btn btn-search" runat="server" OnClick="btnTopSearch_Click" Text="Apply Filters" />                          
                                 </div>                                                          
                               </div> 
                                               

                   <div class="clearfix"></div>                                    
                  
                    <div class="col-md-12 colpadding0" style="float: left;">

                        <div class="col-md-6 colpadding0 entrycount" style="display:none;">                         
                        <h5 style="font-weight:bold;">Select Your Risk Color Preference</h5>
                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                                <input id="High" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">High</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                              <input id="medium" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Medium</h5>
                            </div>

                            <div class="col-md-1 colpadding0" style="width: 4.333333%;">
                             <input id="low" style="display: none;"/> 
                            </div>
                            <div class="col-md-3 colpadding0">
                                <h5 class="colpadding0">Low</h5>
                            </div>
                         </div>

                         <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">  

                         </div>
                    </div>
                </div>    
                              </div>
                        </div>
                    </div>
                </div>     
                           </div>                            
                        </ContentTemplate>      
                     <Triggers>
                         <asp:PostBackTrigger ControlID="btnFilter" />
                         <asp:AsyncPostBackTrigger EventName="SelectedIndexChanged" ControlID="ddlLegalEntity" />
                     </Triggers>                    
                 </asp:UpdatePanel> 

                <div class="clearfix"></div>

                <asp:HiddenField ID="highcolor" runat="server" Value="#FF7473" />
                <asp:HiddenField ID="mediumcolor" runat="server" Value="#FFC952"/>
                <asp:HiddenField ID="lowcolor" runat="server" Value="#1FD9E1"/>

                 <!-- ObservationStatus Start -->
                <div id="ObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Test Results </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       <%-- <div class="col-md-2"></div>--%>
                                       
                                        <div id="perStatusPieChartDivAPRJUN" runat="server">                                            
                                        </div>

                                   <%--<div id="perStatusPieChartDivAPRJUN" runat="server" class="col-md-5" style="width:25%; height:300px;" >                                            
                                        </div>--%>

                                        <div id="perStatusPieChartDivJULSEP" runat="server">                                            
                                        </div>

                                        <div id="perStatusPieChartDivOCTDEC" runat="server">                                            
                                        </div>

                                        <div id="perStatusPieChartDivJANMAR" runat="server">                                            
                                        </div> 

                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                 <!-- ObservationStatus End -->

                 <div class="clearfix" style="height:10px;"></div>
               
                 <!-- Quarterly TestResults Start -->
                <div id="QuarterlyFailedControlStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Failed Controls </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseQuarterlyFailedControlStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('QuarterlyFailedControlStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseQuarterlyFailedControlStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                       
                                        <div id="QuarterlyFailedControlColumnChart" class="col-md-5" style="width:100%; height:300px;">      

                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>  
                     </div>               
                 <!--  Quarterly TestResults  End -->               

                 <div class="clearfix" style="height:10px;"></div>

                 <!-- Process Wise Observation Status Start -->
                <div id="ProcessWiseObservationStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Process Wise - Test Results </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseProcessObsStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ObservationStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseProcessObsStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-2"></div>
                                        <div id="DivGraphProcessObsStatus" class="col-md-12" runat="server" style="height: 350px; overflow-y: auto;">                                           
                                        </div>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                  <!-- Process Wise Observation Status End -->     
                    
                    <div class="clearfix" style="height:10px;"></div>

                 <!-- Process Wise Failed Controls Status Start -->
                <div id="ProcessWiseFailedControlStatus" class="row Dashboard-white-widget">
                    <div class="dashboard">                        
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="margin-left: 10px;">
                                    <h2>Process Wise - Failed Controls </h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseProcessWiseFailedControlStatus"><i class="fa fa-chevron-up"></i></a>
                                        <a href="javascript:closeDiv('ProcessWiseFailedControlStatus')" class="btn-close"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>

                               <div id="collapseProcessWiseFailedControlStatus" class="panel-collapse collapse in">     
                                 <div class="panel-body">
                                    <div class="col-md-12">
                                        
                                        <div id="DivProcessWiseFailedControlColumnChart" style="height: 350px; overflow-y: auto; margin: 0 auto"></div>
                                       <%-- <div id="DivProcessWiseFailedControlColumnChart" class="col-md-12" runat="server" style="height: 350px; overflow-y: auto;">                                           
                                        </div>--%>
                                    </div>                                   
                                </div>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>               
                  <!-- Process Wise Failed Controls Status End -->  
                                                       
                <div class="modal fade" id="DivReports" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
                        <div class="modal-dialog" style="width:90%;">          
                            <div class="modal-content" style="width:100%;">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>

                                <div class="table-responsive" style="width:100%;">              
                                    <iframe id="showdetails" src="about:blank" width="1150px" height="100%" frameborder="0"></iframe>                                      
                                </div>
                            </div>
                        </div>
                   </div>

            </section>       
    </section>
</asp:Content>
