﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class InternalComplianceOverdueSummary : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected string CId;
        protected string UserId;
        protected int CustId;
        protected int UId;
        protected List<Int32> roles;
        protected int RoleID;
        protected int UserRoleID;
        protected int StatusFlagID;
        protected int RoleFlag;
        protected string Flag;
        protected bool DisableFalg;
        protected string Path;
        protected int isapprover;
        protected string CustomerName;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
            {
                isapprover = 0;
                Flag = "MGMT";
            }
            else
            {
                var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                           where row.RoleID == 6
                                           && row.UserID == AuthenticationHelper.UserID
                                           select row).ToList();
                if (GetApproverInternal.Count > 0)
                {
                    isapprover = 1;
                    Flag = "APPR";
                }
            }
            if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || isapprover == 0 || AuthenticationHelper.ComplianceProductType == 3)
            {

                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                RoleFlag = 0;
                CustomerName = CustomerManagement.CustomerGetByIDName(CustId);
            }
        }
    }
    }
}