﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="managementusersAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.managementusersAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <style type="text/css">
        span.k-icon.k-i-window-minimize {
    display: none;
}
         .k-grid-toolbar:first-child, .k-grouping-header + .k-grid-toolbar {
            border-width: 0 0 0px;
            padding: 0px;
        }

        .k-grid-content {
            min-height: 394px !important;
	        height: auto !important;
              overflow: hidden  !important;	
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-grouping-header 
        {
            border-right: 0px solid;
            border-left: 0px solid;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }
            

        .k-grid td {
            line-height: 2.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        /*.k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }*/

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {            
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
            margin-top: -1px;
            margin-left: -1px;
            margin-right: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-grid table 
        {
            width: 100%;
            max-width: none;
            border-collapse: separate;
            empty-cells: show;
            margin: -1px;
            border-spacing: 0px;
            border-width: 0px;
            outline: 0px;
        }


        .change-condition {
            color: blue;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
        $(document).ready(function () {
            window.parent.forchild($("body").height() + 50);
        });
         function BindGrid() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindUserList?UserID=<% =UId%>&customerID=<% =CustId%>&branchid=<% =BID%>&depthead=<% =dhead%>&categoryid=<% =catid%>&approver=<% =isapprover%>&compliancetypeflag=<% =FlagID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/BindUserList?UserID=<% =UId%>&customerID=<% =CustId%>&branchid=<% =BID%>&depthead=<% =dhead%>&categoryid=<% =catid%>&approver=<% =isapprover%>&compliancetypeflag=<% =FlagID%>'
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },

                toolbar: kendo.template($("#template").html()),
                height: 488,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    { hidden: true, field: "UserID", title: "ID" },
                    //{ hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    //{ hidden: true, field: "OverdueBy", title: "OverdueBy(Days)" },
                    //field: "OverdueBy", title: 'OverdueBy(Days)'
                    {
                        field: "UserName", title: 'Name',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },filterable: { multi: true, search: true}                        
                    },
                    {
                        field: "RoleName", title: 'Role',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: { multi: true, search: true}
                    },                    
                    {
                        field: "BranchName", title: 'Location',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: { multi: true, search: true }
                    },
                    {
                        field: "Email", title: 'Email',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }
                    },                                        
                    {
                        field: "ContactNumber", title: 'ContactNumber',
                        width: "20%",
                        filterable: { multi: true, search: true }
                    }
                ]
            });
        }

        function onChangeSD() {
            //FilterGrid();
        }
        function onChangeLD() {
            //FilterGrid();
        }

        function BindGridApply(e) {
            BindGrid();
            FilterGrid();
            e.preventDefault();
        }
        $(document).ready(function () {
            BindGrid();
            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });
            window.parent.forchild($("body").height() + 50);
        });
        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
        };
    </script>

</head>
<body style="overflow-x:hidden;">
    <form>
        <div class="col-lg-5 col-md-5 colpadding0">
                                    <h1 id="pagetype" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;color: #666;margin-bottom: 5px;font-size: 19px;padding-left:5px;padding-top:5px;font-weight: bold;" >Users</h1>
                                </div>

        <div id="example">           
            <div id="grid" "></div>
        </div>
        <script type="text/javascript">
          function fhead(Users)
          { 
              $('#pagetype').html(val);
              //  $('#sppagetype').html(val)   
          }
      </script>       
  
    </form>
</body>
</html>

