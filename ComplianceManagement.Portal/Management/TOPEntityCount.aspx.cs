﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class TOPEntityCount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgrid();
            }            
        }
        public void bindgrid()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int distributorID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    var CustIds = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    distributorID = ICIAManagement.GetParentIDforCustomer(CustIds);
                }

                var query = (from row in  entities.Customers                                                          
                             select row).ToList();
                if (AuthenticationHelper.Role == "DADMN")
                {
                    query = query.Where(entry => entry.ParentID == distributorID).ToList();
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    query = query.Where(entry => entry.ParentID == distributorID).ToList();
                }
                grdentity.DataSource = query;
                grdentity.DataBind();
            }
        }
    }
}