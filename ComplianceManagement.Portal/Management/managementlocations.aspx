﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="managementlocations.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.managementlocations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6QVEs3x5HCb4r-jC8hy8FJZxT5syQI4Y"></script>

    <style type="text/css">
    .h1, h1 
    {
      font-size: 25px;
      margin-inline-end: -726px;
    }
    </style>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <%--<div class="col-lg-5 col-md-5 colpadding0">
            <h1 id="pagetype" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;font-weight:bold;color: #666;font-size: 19px;margin-bottom: 12px;padding-left:5px;padding-top:5px;">Locations</h1>
        </div>--%>
        <div class="clearfix"></div>
        <div>
            <div id="collapsePerformerLoc" class="panel-collapse collapse in">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colpadding0">
                        <div class="panel-body-map" style="margin-top: 45;">
                            <div id="map" style="height: 450px; "></div>
                        </div>
                    </div>                  
                </div>
                <!--/.row-->
                <div class="clearfix" style="height: 10px"></div>
            </div>
        </div>
       
    </form>
    <script>

        var geocoder, geocoder1;
        var map, map2;
        var bounds = new google.maps.LatLngBounds();
        var bounds1 = new google.maps.LatLngBounds();
        function initialize() {

            if (document.getElementById("map") != null && document.getElementById("map") != undefined && locations != null && locations != undefined) {
                map = new google.maps.Map(
                  document.getElementById("map"), {
                      center: new google.maps.LatLng(37.4419, -122.1419),
                      zoom: 13,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  });
                geocoder = new google.maps.Geocoder();
                for (i = 0; i < locations.length; i++) {
                    geocodeAddress(locations, i);
                }
            }

        }
        google.maps.event.addDomListener(window, "load", initialize);

        function geocodeAddress(locations, i) {
            var title = locations[i][0];
            var address = locations[i][1];
            var url = locations[i][2];
            geocoder.geocode({
                'address': locations[i][1]
            },
              function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      var marker = new google.maps.Marker({
                          icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
                          map: map,
                          position: results[0].geometry.location,
                          title: title,
                          animation: google.maps.Animation.DROP,
                          address: address,
                          url: url
                      });

                      // infoWindow(marker, map, title, address, url);
                      bounds.extend(marker.getPosition());
                      map.fitBounds(bounds);

                  } else {
                      //  alert("geocode of " + address + " failed:" + status);
                  }
              });
        }




        function infoWindow(marker, map, title, address, url) {
            google.maps.event.addListener(marker, 'click', function () {
                var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "'>View location</a></p></div>";
                iw = new google.maps.InfoWindow({
                    content: html,
                    maxWidth: 350
                });
                iw.open(map, marker);
            });
        }
        function createMarker(results) {
            var marker = new google.maps.Marker({
                icon: 'https://maps.google.com/mapfiles/ms/icons/blue.png',
                map: map,
                position: results[0].geometry.location,
                title: title,
                animation: google.maps.Animation.DROP,
                address: address,
                url: url
            })
            bounds.extend(marker.getPosition());
            map.fitBounds(bounds);
            infoWindow(marker, map, title, address, url);
            return marker;
        }


    </script>
</body>
</html>
