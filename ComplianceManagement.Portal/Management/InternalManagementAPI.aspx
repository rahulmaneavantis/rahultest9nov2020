﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalManagementAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.InternalManagementAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>


    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <script src="../Scripts/KendoPage/InternalMGMT.js"></script>
    
    <style type="text/css">

        span.k-icon.k-i-window-minimize {
    display: none;
}

        .k-grid-content {
            min-height:400px !important;
             overflow: hidden;
        }
        .k-state-default > .k-select {
        margin-top: 0px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-button {
            margin-right: 5px;
            margin-top: 5px;
            border-radius: 4px;
            border-color: #ceced2;
        }
        
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

         .k-grouping-header 
         {
            border-right: 1px solid;
            border-left: 1px solid;
         }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }
        
        #grid .k-grid-toolbar {
            background: white;
        }


        /*.k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }*/

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
        .k-grid-header th.k-with-icon .k-link {
            background-color: #F8F8F8;
        }

    </style>



    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        $(window).resize(function(){
            window.parent.forchild($("body").height()+50);  
        });

         $(document).ready(function () {

             BindLocation();
             BindRisk();
             BindUser();
             BindFunction();
             BindFinancialYear();
             BindStatus();
             BindPastData();
             BindGrid();
             FilterGrid();
             

            $("#Startdatepicker").kendoDatePicker({
                change: onChangeSD
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: onChangeLD
            });

                $("#dropdownSequence").kendoDropDownList({
            filter: "startswith",
            autoClose: false,
            autoWidth: true,
            dataTextField: "Name",
            dataValueField: "ID",
            optionLabel: "Select Label",
            change: function (e) {
                FilterGrid();
            },
            dataSource: {
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                     //read: "<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>"
                }
            }, dataBound: function (e) {
                e.sender.list.width("900");
            }
        });

         });

        
        function BindGrid()
        {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(10)').remove();
                },
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetInternalManagementDashboardPages?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>/Data/GetInternalManagementDashboardPages?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                      //  read: '<% =Path%>/Data/GetInternalManagementDashboardPages?Uid=<% =UId%>&ptname=<% =pointname%>&attr=<% =attribute%>&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=<% =functionid%>&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=<% =Status%>&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                        //read: '<% =Path%>/Data/GetInternalManagementDashboardPages?Uid=<% =UId%>&ptname='+pointname1+'&attr='+attribute1+'&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=<% =functionid%>&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=<% =Status%>&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val()
                    },
                    schema: {
                        data: function (response) {                            
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart;
                            }
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                return response[0].DeptRiskBarChart;
                            }
                            //return response[0].FunctionPieChart.length;
                        },
                        total: function (response) {
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                return response[0].DeptRiskBarChart.length;
                            }
                            //return response[0].FunctionPieChart.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),
                height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Performer", title: "Performer" },
                    { hidden: true, field: "Reviewer", title: "Reviewer" },                                             
                    {
                        field: "Branch", title: 'Location',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                    
                    {                       
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,                            
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                   {                        
                          field: "Status", title: 'Status', filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                    },
                    {
                        command: [
                            {
                                name: "edit5", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit2", text: "  ", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview",
                            }
                        ], title: "Action", headerAttributes: {
                            style: "border-right: solid 1px #ceced2;"
                        } 
                    }
                ]
            });
            $("#grid").kendoTooltip({                
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#grid").kendoTooltip({
                filter: "td",                
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    if ($(target).text() == "  ") {

                        return "Action";
                    }
                    else
                    {
                        return $(target).text();
                    }
                   

                }
            }).data("kendoTooltip")
            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#downloadfile').attr('src', "../ComplianceDocument/DownloadMGMTDocAPI.aspx?ComplianceScheduleID=" + item.ScheduledOnID + "&IsFlag=0");
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));           
                OpenDocumentOverviewpup(item.ScheduledOnID)
                return true;
            });

        }

    function OpenDocumentOverviewpup(scheduledonid) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/DocumentOverviewAPI.aspx?ComplianceScheduleID=" + scheduledonid + "&ISStatutoryflag=0");
        }

    function onChangeSD() {
        //FilterGrid();
    }
    function onChangeLD() {
        //FilterGrid();
    }

    function BindGridApply(e) {

        var setStartDate = $("#Startdatepicker").val();
        var setEndDate = $("#Lastdatepicker").val(); 

        BindGrid();
        FilterGrid();

        if (setStartDate != null) {
            $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
        }
        if (setEndDate != null) {
            $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
        }  

        //FilterGrid();

        e.preventDefault();
    }

    function FilterGrid()
    {
        
        //Risk Details
        var Riskdetails = [];
        if($("#dropdownlistRisk").data("kendoDropDownTree") != undefined)
        {
            Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
        }

        //status Details
        var Statusdetails = [];
        if($("#dropdownlistStatus").data("kendoDropDownTree") != undefined)
        {
            var Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;

            if(Statusdetails.length >= 1)
            {
                for (var i = 0; i < Statusdetails.length; i++) 
                { 
                    if (Statusdetails[i] === "No" ||  Statusdetails[i] === "" ) 
                    {
                        Statusdetails.splice(i, 1); 
                    }
                }                
            }
        }
        
        //user Details  
        var Userdetails = [];
        if($("#dropdownUser").data("kendoDropDownTree") != undefined)
        {
             Userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
        }
       
        //location details
        var locationsdetails = [];
        if($("#dropdowntree").data("kendoDropDownTree") != undefined)
        {
            locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
        }

        //datefilter
        var datedetails = [];
        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
            datedetails.push({
                field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
            });
        }
        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
            datedetails.push({
                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
            });
        }
                       
        var finalSelectedfilter = { logic: "and", filters: [] };

        if (Riskdetails.length > 0 || Statusdetails.length > 0 || locationsdetails.length > 0 || Userdetails.length > 0 || datedetails.length > 0 || 
        ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") ||
        ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined))
        {    
            if ( $("#dropdownSequence").val() != undefined  && $("#dropdownSequence").val() != "" && $("#dropdownSequence").val() != null)  
            {   
                var SeqFilter = { logic: "or", filters: [] };
                SeqFilter.filters.push({
                    field: "SequenceID", operator: "eq", value:$("#dropdownSequence").val()
                });

                finalSelectedfilter.filters.push(SeqFilter);
            }
            if (datedetails.length > 0) 
            {
                var DateFilter = { logic: "or", filters: [] };

                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                {
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                finalSelectedfilter.filters.push(DateFilter);
            }
            if (Userdetails.length > 0) 
            {
                var UserFilter = { logic: "or", filters: [] };

                $.each(Userdetails, function (i, v) {
                    UserFilter.filters.push({
                        field: "UserID", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(UserFilter);
            }
            if (locationsdetails.length > 0) 
            {
                var LocationFilter = { logic: "or", filters: [] };

                $.each(locationsdetails, function (i, v) {
                    LocationFilter.filters.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(LocationFilter);
            }
            if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) 
            {
                if ($("#dropdownfunction").val() != 0) {
    
                    var FunctionFilter = { logic: "or", filters: [] };

                    FunctionFilter.filters.push({
                        field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                    });
               
                    finalSelectedfilter.filters.push(FunctionFilter);
                }
            }
            if (Statusdetails.length > 0) 
            {
                var StatusFilter = { logic: "or", filters: [] };

                $.each(Statusdetails, function (i, v) {
                    StatusFilter.filters.push({
                        field: "FilterStatus", operator: "eq", value: v
                    });
                });

                finalSelectedfilter.filters.push(StatusFilter);
            }
            if (Riskdetails.length > 0) 
            {
                var CategoryFilter = { logic: "or", filters: [] };

                $.each(Riskdetails, function (i, v) {

                    CategoryFilter.filters.push({
                        field: "Risk", operator: "eq", value: parseInt(v)
                    });
                });

                finalSelectedfilter.filters.push(CategoryFilter);
            }
            if (finalSelectedfilter.filters.length > 0) 
            {
                if ($("#grid").data("kendoGrid") != undefined) 
                {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
            }
            else 
            {
                if ($("#grid").data("kendoGrid") != undefined) 
                {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
        }
        else 
        {
            $("#grid").data("kendoGrid").dataSource.filter({});
        }

    }

    function fCreateStoryBoard(Id, div, filtername) {

        var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
        $('#' + div).html('');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
        $('#' + div).css('display', 'block');

        if (div == 'filtersstoryboard') {
            $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            $('#ClearfilterMain').css('display', 'block');
        }
        else if (div == 'filtertype') {
            $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
        }
        else if (div == 'filterrisk') {
            $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            $('#ClearfilterMain').css('display', 'block');
        }
        else if (div == 'filterstatus') {
            $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            $('#ClearfilterMain').css('display', 'block');
        }
        else if (div == 'filterUser') {
            $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#ClearfilterMain').css('display', 'block');
        }
        else if (div == 'filterpstData1') {
            $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCategory') {
            $('#' + div).append('Category&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterAct') {
            $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCompSubType') {
            $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterCompType') {
            $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filtersstoryboard1') {
            $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filtertype1') {
            $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterrisk1') {
            $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterFY') {
            $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }
        else if (div == 'filterstatus1') {
            $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            $('#Clearfilter').css('display', 'block');
        }

        for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
            var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
            $(button).css('display', 'none');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
            var buttontest = $($(button).find('span')[0]).text();
            if (buttontest.length > 10) {
                buttontest = buttontest.substring(0, 10).concat("...");
            }
            $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
            //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
        }

        if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
            $('#' + div).css('display', 'none');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

        }
        CheckFilterClearorNotMain();
    }


    function CloseClearPopup() {
            $('#APIOverView').attr('src', "../Common/blank.html");
        }

    function OpenOverViewpupMain(scheduledonid, instanceid) {
            $('#divApiOverView').modal('show');
            $('#APIOverView').attr('width', '1150px');
            $('#APIOverView').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#APIOverView').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
    }

        function BindFinancialYear(){
            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },                    
                    { text: "2020-2021", value: "2020-2021" },
		    { text: "2019-2020", value: "2019-2020" },
		    { text: "2018-2019", value: "2018-2019" },
		    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }  
                ]
            });
        }

        function BindRisk()
        {
             $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');

                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
             });
             $("#dropdownlistRisk").data("kendoDropDownTree").value('<%= riskid%>');

        }

        function BindFunction()
        {
            $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT"
                    }
                }
            });
            $("#dropdownfunction").data("kendoDropDownList").value('<%=functionid%>');
       
            $("#dropdownfunction").val('<%=functionid%>'); 
        }
        
          

        function BindPastData()
        {
            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

     
    

        function BindStatus(){
             $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
                },
         
                dataSource: [
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                      <%if (IsNotCompiled == true)%>
                    <%{%>
                       { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Closed Timely", value: "ClosedTimely" },
                    { text: "Closed Delayed", value: "ClosedDelayed" },
                    { text: "In Progress", value: "InProgress" }
                ]
            });
            $("#dropdownlistStatus").data("kendoDropDownTree").value(["<%=StatusID%>", "<%=StatusID1%>", "<%=StatusID2%>", "<%=StatusID3%>"]);  
        }


        function BindUser()
        {
            
            $("#dropdownUser").kendoDropDownTree({
        placeholder: "User",
        checkboxes: true,
        checkAll: true,
        autoClose: true,
        checkAllTemplate: "Select All",
        autoWidth: true,
        dataTextField: "FullName",
        dataValueField: "UID",

        change: function () {
            FilterGrid();
                    
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
  
        },
        dataSource: {
            severFiltering: true,
            transport: {
                read: {
                    url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                }
                //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>"
                    },                   
                }
            });
        }
        function BindLocation()
        {
              $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }
            

           

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

    </script>

</head>
<body style="overflow-x:hidden;">
    <form>
            <div class="row">
            <div class="col-lg-12 col-md-12 colpadding0">
                <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px;padding-left:5px;padding-top:5px;">Internal Compliance List</h1>
                <%--<h1 style="height: 30px;background-color: #f8f8f8;height: 30px;background-color: #f8f8f8;margin-top: 0px;font-weight: 100;color: #666;font-weight: 500;margin-bottom: 12px;">Compliance List </h1>--%>
            <h1 id="display" runat="server" style="display:none; margin-top: 2px; font-size: 20px; font-weight: 100; font-size: 17px; color: #666; font-weight: 500; margin-bottom: 17px;"></h1>
        </div>
        </div>
        <div id="example">
            <div class="row">
                <div class="toolbar">
                   <div class="row" style="margin-bottom:-7px;margin-top:-6px;">
                        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 256px; padding-right: 7px;"/>
                        <input id="dropdownFY" style="width: 242px;"/>
                        <input id="dropdownlistStatus" data-placeholder="Status" style="padding-left: 7px;" />
                        <input id="dropdownlistRisk" data-placeholder="Risk" style="padding-left: 7px; padding-right: 7px;" />
                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 140px; margin-right: 7px;" />
                        <input id="Lastdatepicker" placeholder="End Date" cssclass="clsROWgrid" title="Lastdatepicker" style="width: 140px;" />                             
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 12px; padding-bottom: 6px;"> 
                <input id="CustName" type="hidden" value="<% =CustomerName%>"/>
                <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>"/>
                <input id="dropdownPastData" style="width: 256px; margin-right: 7px;"/>
                <input id="dropdownfunction" style="width: 242px; margin-right: 7px;"/> 
                <input id="dropdownUser" data-placeholder="User" style="width: 173px;" />
                <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
                <%{%>  
                    
                        <input id="dropdownSequence" style="width: 242px; margin-right: 7px;" />
                    
                <%}%>     
                <button id="export" onclick="exportReport(event)" data-toggle="tooltip" title="Export to Excel" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:45px; height:30px; background-color:white;border: none;"></button>        
                <button id="Applyfilter" style="margin-left: 0%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="ClearfilterMain" style="float: right; margin-right: 1px; margin-top: 3px; display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>

            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;font-weight:bold;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;font-weight:bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;font-weight:bold;margin-top:-6px;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;font-weight:bold;margin-top:-6px;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;font-weight:bold; color: #535b6a;margin-top:-6px;" id="filterUser">&nbsp;</div>

            <div id="grid" style="border: none;"></div>
        </div>
        <div>
            <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
    </form>
</body>
</html>


