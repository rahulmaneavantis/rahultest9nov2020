﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ObservationDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ObservationDetails" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Observation Details</title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <script type="text/javascript">

        function ShowDetailsDialog() {
            $('#divATBDDetailsDialog').modal('show');
            return true;
        };

        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        $('.btn-search').on('click', function () {

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        });
    </script>
    <style type="text/css">
        .search-choice-close {
            visibility: hidden !important;
        }

        * .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
    <style type="text/css">
        .modal-dialog {
            display: table;
            overflow-y: auto;
            overflow-x: auto;
            width: 1000px;
            min-width: 700px;
        }

        /*.modal-body {           
            overflow-y: auto;
            max-height: 600px;
            padding: 15px;
        }*/

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
                <%--OnLoad="upDivLocation_Load"--%>
                <ContentTemplate>
                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" Selected="True" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlCustomer" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                    DataPlaceHolder="Customer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" >
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">

                                <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" Style="background: none;"
                                    OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged" DataPlaceHolder="Unit">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15" Width="95%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">

                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>

                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                    DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                                    DataPlaceHolder="Financial Year" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                    <%--OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"--%>
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">

                                <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged"
                                    DataPlaceHolder="Scheduling Type" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">

                                <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true"
                                    DataPlaceHolder="Period" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlProcess" AutoPostBack="true" DataPlaceHolder="Process" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                            <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                            <%{%>
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" DataPlaceHolder="Vertical" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                            <%}%>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div style="margin-top: 8px; margin-bottom: 8px; float: right;">
                                <div>
                                    <asp:Button ID="btnFilter" class="btn btn-search" runat="server" Text="Apply Filter(s)" OnClick="btnTopSearch_Click" Style="margin-right: 10px;" />
                                    <asp:Button ID="lbtnExportExcel" class="btn btn-search" runat="server" Text="Export To Excel" OnClick="lbtnExportExcel_Click"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 10px;">
                            <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" GridLines="None" AllowSorting="true"
                                CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="20" Width="100%"
                                Font-Size="12px" DataKeyNames="ATBDId">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Financial Year">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("FinancialYear") %>' ToolTip='<%# Eval("FinancialYear") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Period">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 70px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ForMonth") %>' ToolTip='<%# Eval("ForMonth") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubProcess">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="lblSubProcessName" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observation">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Observation") %>' ToolTip='<%# Eval("Observation") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Observation Category">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">     
                                            <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("ObservationCategoryName") %>' ToolTip='<%# Eval("ObservationCategoryName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Recommendation">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 120px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Recomendation") %>' ToolTip='<%# Eval("Recomendation") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Management Response">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 150px;">
                                                <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("ManagementResponse") %>' ToolTip='<%# Eval("ManagementResponse") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Activity Description">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 100px;">     
                                            <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="top"  Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="Time Line">
                                        <ItemTemplate>
                                            <div class="text_NlinesusingCSS" style="width: 80px;">
                                                <asp:Label ID="lblTimeline" runat="server" Text='<%# Eval("TimeLine")!=null?Convert.ToDateTime(Eval("TimeLine")).ToString("dd-MMM-yyyy"):"" %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnChangeStatus" runat="server" CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" Visible='<%# CanChangeStatus("OS") %>'
                                                CommandArgument='<%# Eval("ATBDId") + "," + Eval("CustomerBranchID")+ "," + Eval("FinancialYear") %>'>
                                                <img src='<%# ResolveUrl("~/Images/change_status_icon_new.png")%>' alt="Click to See Details" title="Click to See Details" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <HeaderStyle BackColor="#ECF0F1" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                    <%-- <table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <div style="float: right;">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                    <div class="table-paging-text" style="margin-top: -35px; margin-left: 19px;">
                                        <p>
                                            Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnFilter" />
                    <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="modal fade" id="divATBDDetailsDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="table-responsive" style="width: 90%; padding-left: 5%;">
                    <%--<div class="modal-dialog" style="width: 1000px; height:auto;"> --%>
                    <div class="modal-content" style="height: 600px; overflow-y: auto;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body">

                            <asp:UpdatePanel ID="upATBDDetails" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <table style="width: 100%; margin-top: 5px;">
                                        <tr>
                                            <td>
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceValidationGroup" />
                                                <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="true"
                                                    ValidationGroup="ComplianceValidationGroup" />
                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="position: absolute;" valign="top" runat="server" id="TDTab">
                                                <header class="panel-heading tab-bg-primary" style="background: none; !important;">
                                                    <ul id="rblRole1" class="nav nav-tabs">
                                                        <li class="active" id="liAuditCoverage" runat="server">
                                                            <asp:LinkButton ID="lnkAuditCoverage" OnClick="Tab1_Click" runat="server">Audit Coverage</asp:LinkButton>
                                                        </li>

                                                        <li class="" id="liActualTesting" runat="server">
                                                            <asp:LinkButton ID="lnkActualTesting" OnClick="Tab2_Click" runat="server">Actual Testing/ Work Done</asp:LinkButton>
                                                        </li>

                                                        <li class="" id="liObservation" runat="server">
                                                            <asp:LinkButton ID="lnkObservation" OnClick="Tab3_Click" runat="server">Observations</asp:LinkButton>
                                                        </li>

                                                        <li class="" id="liReviewLog" runat="server">
                                                            <asp:LinkButton ID="lnkReviewLog" OnClick="Tab4_Click" runat="server">Review History & Log</asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </header>

                                                <asp:MultiView ID="MainView" runat="server">
                                                    <asp:View ID="FirstView" runat="server">
                                                        <div style="width: 100%; float: left; margin-bottom: 15px">
                                                            <table style="width: 100%; margin-top: 0px;">
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333; margin-bottom: 10px;">
                                                                            Audit Objective</label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtAuditObjective" CssClass="form-control" TextMode="MultiLine"
                                                                            Style="margin-bottom: 10px; height: 100px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 185px; display: block; float: left; font-size: 13px; color: #333; margin-bottom: 10px;">
                                                                            Audit Steps</label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtAuditSteps" CssClass="form-control" TextMode="MultiLine"
                                                                            Style="margin-bottom: 10px; height: 100px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Analyis To Be Performed</label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtAnalysisToBePerformed" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; height: 100px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:View>

                                                    <asp:View ID="SecondView" runat="server">
                                                        <div style="width: 100%; float: left; margin-bottom: 15px">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Process  Walkthrough 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtWalkthrough" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Actual Work Done 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtActualWorkDone" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Population 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtpopulation" CssClass="form-control" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Sample 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtSample" CssClass="form-control" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2" style="width: 100%;">
                                                                        <div runat="server" id="divDeleteDocument" style="margin-left: 175px;">
                                                                            <fieldset style="margin-top: 5px;">
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td style="width: 50%">

                                                                                            <asp:UpdatePanel runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:GridView runat="server" ID="rptComplianceDocumnets" AutoGenerateColumns="false" AllowSorting="true"
                                                                                                        AllowPaging="true" GridLines="None" OnRowDataBound="rptComplianceDocumnets_RowDataBound"
                                                                                                        CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%" CssClass="table">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField HeaderText="Document" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                                                        <ContentTemplate>
                                                                                                                            <asp:LinkButton
                                                                                                                                CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' CommandName="Download"
                                                                                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                                            </asp:LinkButton>
                                                                                                                        </ContentTemplate>
                                                                                                                        <Triggers>
                                                                                                                            <asp:PostBackTrigger ControlID="btnComplianceDocumnets" />
                                                                                                                        </Triggers>
                                                                                                                    </asp:UpdatePanel>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                                                                        <PagerTemplate>
                                                                                                            <table style="display: none">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </PagerTemplate>
                                                                                                        <EmptyDataTemplate>
                                                                                                            No Records Found.
                                                                                                        </EmptyDataTemplate>
                                                                                                    </asp:GridView>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:View>

                                                    <asp:View ID="ThirdView" runat="server">
                                                        <div style="width: 100%; float: left; margin-bottom: 15px">

                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation Number
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtObservationNumber" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation Title
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtObservationTitle" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtObservation" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Risk 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtRisk" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Root Cost 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtRootcost" CssClass="form-control" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Financial Impact 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtfinancialImpact" CssClass="form-control" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Recommendation
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtRecommendation" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label id="M" runat="server" style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Management Response 
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtMgtResponse" CssClass="form-control" TextMode="MultiLine" Style="margin-bottom: 10px; width: 688px;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label id="T" runat="server" style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">&nbsp;</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Time Line
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtTimeLine" CssClass="form-control" Style="margin-bottom: 10px; width: 115px; text-align: center;" />
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Person Responsible
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlPersonresponsible" runat="server" class="form-control m-bot15" Style="margin-bottom: 10px;"
                                                                            Width="50%" Height="32px" DataPlaceHolder="Person Responsible">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation Rating
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlobservationRating" runat="server" class="form-control m-bot15" Style="margin-bottom: 10px;"
                                                                            Width="50%" Height="32px" DataPlaceHolder="Observation Rating">
                                                                            <asp:ListItem Value="-1">Select Rating</asp:ListItem>
                                                                            <asp:ListItem Value="1">High</asp:ListItem>
                                                                            <asp:ListItem Value="2">Medium</asp:ListItem>
                                                                            <asp:ListItem Value="3">Low</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation Category
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlObservationCategory" runat="server" class="form-control m-bot15" Width="50%" Height="32px" Style="margin-bottom: 10px;"
                                                                            DataPlaceHolder="Observation Category" AutoPostBack="true" OnSelectedIndexChanged="ddlObservationCategory_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red; margin-bottom: 10px;">*</label>
                                                                        <label style="width: 165px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Observation SubCategory
                                                                        </label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlObservationSubCategory" runat="server" class="form-control m-bot15" Width="50%" Height="32px"
                                                                            DataPlaceHolder="Observation SubCategory" Style="margin-bottom: 10px;">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Date</label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="tbxDate" Style="width: 115px; text-align: center;" Enabled="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:View>

                                                    <asp:View ID="FourthView" runat="server">
                                                        <div style="width: 100%; float: left; margin-bottom: 15px">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Panel ID="divReviewHistory" runat="server" Style="width: 100%">
                                                                            <fieldset style="margin-top: 5px;">
                                                                                <legend>Review History</legend>
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:GridView runat="server" ID="GridRemarks" AutoGenerateColumns="false" AllowSorting="true"
                                                                                                AllowPaging="true" GridLines="None" CssClass="table"
                                                                                                CellPadding="4" ForeColor="Black" Font-Size="12px" Width="100%">
                                                                                                <%--OnPageIndexChanging="GridRemarks_OnPageIndexChanging"--%>
                                                                                                <Columns>

                                                                                                    <asp:TemplateField HeaderText="ID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblATBDId" runat="server" Text='<%# Eval("ATBDId") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="ID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="riskID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblriskID" runat="server" Text='<%# Eval("InternalAuditInstance") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Scheduleon" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblAuditScheduleOnId" runat="server" Text='<%# Eval("AuditScheduleOnID") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                                        <ItemTemplate>
                                                                                                            <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Left">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:LinkButton ID="lblDownLoadfile" runat="server" OnClick="DownLoadClick" Text='<%# ShowSampleDocumentName((string)Eval("Name")) %>'></asp:LinkButton>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                                <HeaderStyle BackColor="#ECF0F1" />
                                                                                                <PagerTemplate>
                                                                                                    <table style="display: none">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </PagerTemplate>
                                                                                                <EmptyDataTemplate>
                                                                                                    No Records Found.
                                                                                                </EmptyDataTemplate>
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div style="margin: 5px; width: 100%;">
                                                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                                                <fieldset style="margin-top: 5px;">
                                                                                    <legend>Audit Log</legend>
                                                                                    <asp:GridView runat="server" ID="grdTransactionHistory"
                                                                                        AutoGenerateColumns="false" AllowSorting="true"
                                                                                        AllowPaging="true" PageSize="12" GridLines="None" CssClass="table"
                                                                                        CellPadding="4" ForeColor="Black" Width="99%" Font-Size="12px"
                                                                                        DataKeyNames="AuditTransactionID">

                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblCreatedByText" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="300px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("Dated")!= null?((DateTime)Eval("Dated")).ToString("dd-MMM-yyyy"):""%>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                                        <HeaderStyle BackColor="#ECF0F1" />
                                                                                        <PagerTemplate>
                                                                                            <table style="display: none">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </PagerTemplate>
                                                                                        <EmptyDataTemplate>
                                                                                            No Records Found.
                                                                                        </EmptyDataTemplate>
                                                                                    </asp:GridView>
                                                                                    <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                                                                                </fieldset>
                                                                            </div>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </td>
                                        </tr>
                                    </table>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
