﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using System.Text;
using System.Text.RegularExpressions;
using StackExchange.Redis;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{

    public partial class ManagementDashboardNew : System.Web.UI.Page
    {
        protected static DateTime FromFinancialYearSummery;
        protected static DateTime ToFinancialYearSummery;
        protected string Performername;
        protected string Reviewername;
        protected string InternalPerformername;
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static string perPenaltyStatusPieChart;
        protected static string perRiskChart;
        protected static string perFunctionHIGHPenalty;
        protected static string perFunctionMEDIUMPenalty;
        protected static string perFunctionLOWPenalty;
        protected static string perFunctionCRITICALPenalty;
        protected static int customerID;
        protected static int BID;
        protected static string IsSatutoryInternal;
        protected static bool IsApprover = false;
        public static List<long> Branchlist = new List<long>();
        public static List<long> BranchlistGrading = new List<long>();
        bool IsPenaltyVisible = true;              
        protected List<Int32> roles;        
        protected static string perFunctionChartDEPT;
        bool IsNotCompiled = false;
        protected static string UserInformation;
        protected static string ProcessWiseObservationStatusChart;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var plainBytes = Encoding.UTF8.GetBytes(Session["Email"].ToString());
                UserInformation = Convert.ToBase64String(CryptographyHandler.Encrypt(plainBytes, CryptographyHandler.GetRijndaelManaged("avantis")));
            }
            catch
            {

            }
            if (!IsPostBack)
            {
                try
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        if (Session["User_comp_Roles"] != null)
                        {
                            roles = Session["User_comp_Roles"] as List<int>;
                        }
                        else
                        {
                            roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                            Session["User_comp_Roles"] = roles;
                        }
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {                            
                            if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                            {
                                IsApprover = false;
                            }
                            else
                            {
                                if (roles.Contains(6))
                                {
                                    IsApprover = true;
                                }
                                else
                                {
                                    IsApprover = false;
                                }                                
                            }                                                                                                              
                            if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || IsApprover == true || AuthenticationHelper.ComplianceProductType == 3)
                            {
                              
                                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                                home.Value = "true";
                                BindMonth(DateTime.UtcNow.Year);
                                BindYear();
                                BindUserColors();
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                fldsCalender.Visible = true;
                                if (customerID == 63)
                                {
                                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("0"));
                                    ddlStatus.SelectedValue = "1";
                                    fldsCalender.Visible = false;
                                }                                
                                Branchlist.Clear();
                                BranchlistGrading.Clear();
                                BindLocationFilter();                                
                                rbFinancialYearFunctionSummery_SelectedIndexChanged(sender, e);
                                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                                if (PenaltyNotDisplayCustomerList.Count > 0)
                                {
                                    foreach (string PList in PenaltyNotDisplayCustomerList)
                                    {
                                        if (PList == customerID.ToString())
                                        {
                                            IsPenaltyVisible = false;
                                        }
                                    }
                                }
                                if (IsPenaltyVisible == true)
                                {
                                    PenaltyCriteria.Visible = true;
                                    penaltycount.Visible = true;
                                    penaltycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    entitycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    locationcount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    functioncount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    compliancecount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    usercount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                }
                                else
                                {
                                    PenaltyCriteria.Visible = false;
                                    entitycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    locationcount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    functioncount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    compliancecount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    usercount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    penaltycount.Visible = false;
                                }
                                BindLocationCount(customerID, IsPenaltyVisible);                              
                                BindGradingReportSummaryTree();
                                TbxFilterLocationGridding.Text = "Select Entity/Sub-Entity/Location";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                                
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPenalty", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPenalty');", tbxFilterLocationPenalty.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);

                                int UnreadNotifications = Business.ComplianceManagement.GetUnreadUserNotification(AuthenticationHelper.UserID);

                                if (UnreadNotifications > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divNotification", "openNotificationModal();", true);
                                }
                                divTabs.Visible = true;
                                ComplianceCalender.Visible = true;
                                DivOverDueCompliance.Visible = true;
                                fldrbFinancialYearFunctionSummery.Visible = true;                              
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    divTabs.Visible = false;
                                    ComplianceCalender.Visible = false;
                                    DivOverDueCompliance.Visible = false;
                                    fldrbFinancialYearFunctionSummery.Visible = false;                                  
                                }
                                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                                string cashTimeval = string.Empty;
                                if (objlocal == "Local")
                                {
                                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                                }
                                else
                                {
                                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                                }
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                                    TimeSpan span = DateTime.Now - ss;
                                    if (span.Hours == 0)
                                    {
                                        Label1.Text = "Last updated within the last hour";
                                    }
                                    else
                                    {
                                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                                    }
                                }
                                else
                                {
                                    Label1.Text = "Last updated within the last hour";
                                }
                            }
                            else
                            {
                                //added by rahul on 12 June 2018 Url Sequrity
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                FormsAuthentication.RedirectToLoginPage();
                            }                            
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                catch (Exception ex)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    }
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {
                        //Label1.Text = "last updated :" + ss.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        protected void lnkShowCustomerWiseDashboard_Click(object sender, EventArgs e)
        {
        }
        protected void lnkSPOCWiseDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RLCS/RLCS_HRMDashboardNew.aspx",true);
        }
        
        #region Common Function   

       protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
       
        private void BindPenaltyAmount(int customerID, List<SP_GetManagementCompliancesSummary_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }                    
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }
        private void BindPenaltyAmount(int customerID, List<SP_GetPenaltyQuarterWise_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }                 
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }
        private void BindPenaltyAmountApprover(int customerID, List<SP_GetPenaltyQuarterWiseApprover_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }

        public void GetWidgetDashboardNoCompliedCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProcessWiseObservationStatusChart = string.Empty;
                String newDivName = String.Empty;
                String ProcesswiseObservationSeries = String.Empty;
                List<WidgetMaster> ProcessList = new List<WidgetMaster>();

                ProcessList = (from C in entities.WidgetMasters
                               where C.CustomerID == CustomerID
                               select C).ToList();

                List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
                master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
                          select C).ToList();

                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null)
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                }

                if (Branchlist.Count > 0)
                {
                    master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                DateTime? PassEndValue;
                if (FromDate != null && EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate
                                         && row.ScheduledOn <= EndDateF
                                         select row).ToList();

                    PassEndValue = EndDateF;
                }
                else if (FromDate != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                         select row).ToList();

                    PassEndValue = EndDate;
                }
                else if (EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID && row.ScheduledOn <= EndDateF
                                         select row).ToList();


                    PassEndValue = EndDateF;
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in master
                                             where row.CustomerID == CustomerID
                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                             || row.ScheduledOn <= EndDate)
                                             select row).ToList();
                    }
                    else
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            if (AuthenticationHelper.Role == "AUDT")
                            {
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                     || row.ScheduledOn <= FIEndDate)
                                                     select row).ToList();
                            }
                            else
                            {
                                //For Financial Year
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && row.ScheduledOn >= FIFromDate
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        else
                        {
                            transactionsQuery = (from row in master
                                                 where row.CustomerID == CustomerID
                                                 && row.ScheduledOn <= FIEndDate
                                                 select row).ToList();
                        }
                    }
                    PassEndValue = FIEndDate;
                }
                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                    {
                        PassEndValue = FIEndDate;
                        FromDate = FIFromDate;
                    }
                }
                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }
                try
                {

                    int Count = 1;
                    ProcessList.ForEach(EachProcess =>
                    {

                        //transactionsQuery = (from row in master
                        //                     where row.CustomerID == CustomerID
                        //                     && row.WidgetID == EachProcess.ID
                        //                     select row).ToList();

                        var fltertransactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
                        if (fltertransactionsQuery.Count > 0)
                        {
                            long totalPieCompletedCritical = 0;
                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateCritical = 0;
                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedCritical = 0;
                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            long totalPieNotCompliedCritical = 0;
                            long totalPieNotCompliedHIGH = 0;
                            long totalPieNotCompliedMEDIUM = 0;
                            long totalPieNotCompliedLOW = 0;

                            System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                            newDivName = "DivProcessObs" + Count;
                            dynDiv.ID = newDivName;
                            dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
                            dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                            if (Count != 1)
                            {
                                dynDiv.Style["margin-top"] = "50px";
                            }
                            dynDiv.Attributes.Add("class", "FirstDiv");
                            DivGraphProcessObsStatus.Controls.Add(dynDiv);


                            fltertransactionsQuery = fltertransactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //Critical
                            totalPieAfterDueDateCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
                            || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



                            //High
                            totalPieAfterDueDateHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

                            //Medium
                            totalPieAfterDueDateMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


                            //Low
                            totalPieAfterDueDateLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


                            //EachProcess.WidgetName
                            //ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                            ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                                "{ chart:{type: 'column'},title:{ align:'left',text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
                                "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
                                "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
                                "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
                                "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

                            //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                            ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

                                     // Not Completed - Critical
                                     "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{" +

                                     // Not Completed - High
                                     "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{" +

                                     // Not Completed - Medium
                                     " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +

                                     // Not Completed - Low

                                     "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                     "}}}]}," +


                                    "{name: 'Not Complied',color: '#2195f2',data: [{ " +

                                    // Not Complied - Critical

                                    "y: " + totalPieNotCompliedCritical + ",events:{click: function(e) { " +
                                    " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',3)" +

                                    "}}},{   " +

                                    // Not Complied - High

                                    "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',0)" +

                                    "}}},{   " +

                                    // Not Complied - Medium
                                    "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',1)" +

                                    "}}},{   " +
                                    // Not Complied - Low
                                    "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                                    " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',2)" +

                                    "}}}]}," +


                                      "{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
                                     // After Due Date - Critical

                                     "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{   " +


                                     // After Due Date - High

                                     "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{   " +

                                     // After Due Date - Medium
                                     "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{   " +
                                     // After Due Date - Low
                                     "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


                                     "}}}]}," +
                                     "{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +


                                     // In Time - Critical
                                     "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{  " +


                                     // In Time - High
                                     "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{  " +


                                     // In Time - Medium
                                     "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +
                                     // In Time - Low
                                     "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                                      " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                        "}}}]}]});";



                            ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                            Count++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

            }

        }

        //public void GetWidgetDashboardNoCompliedCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        String newDivName = String.Empty;
        //        String ProcesswiseObservationSeries = String.Empty;
        //        List<WidgetMaster> ProcessList = new List<WidgetMaster>();

        //        ProcessList = (from C in entities.WidgetMasters
        //                       where C.CustomerID == CustomerID
        //                       select C).ToList();

        //        List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
        //        master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
        //                  select C).ToList();

        //        if (AuthenticationHelper.Role != "AUDT")
        //        {
        //            if (FromDate == null && EndDateF == null)
        //            {
        //                if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //                {
        //                    master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
        //                }
        //            }

        //        }

        //        if (Branchlist.Count > 0)
        //        {
        //            master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
        //        }

        //        List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
        //        DateTime EndDate = DateTime.Today.Date;
        //        DateTime? PassEndValue;
        //        if (FromDate != null && EndDateF != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID
        //                                 && row.ScheduledOn >= FromDate
        //                                 && row.ScheduledOn <= EndDateF
        //                                 select row).ToList();

        //            PassEndValue = EndDateF;
        //        }
        //        else if (FromDate != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID
        //                                 && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
        //                                 select row).ToList();

        //            PassEndValue = EndDate;
        //        }
        //        else if (EndDateF != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID && row.ScheduledOn <= EndDateF
        //                                 select row).ToList();


        //            PassEndValue = EndDateF;
        //        }
        //        else
        //        {
        //            if (approver == true)
        //            {
        //                transactionsQuery = (from row in master
        //                                     where row.CustomerID == CustomerID
        //                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
        //                                     || row.ScheduledOn <= EndDate)
        //                                     select row).ToList();
        //            }
        //            else
        //            {
        //                if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //                {
        //                    if (AuthenticationHelper.Role == "AUDT")
        //                    {
        //                        transactionsQuery = (from row in master
        //                                             where row.CustomerID == CustomerID
        //                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
        //                                             || row.ScheduledOn <= FIEndDate)
        //                                             select row).ToList();
        //                    }
        //                    else
        //                    {
        //                        //For Financial Year
        //                        transactionsQuery = (from row in master
        //                                             where row.CustomerID == CustomerID
        //                                             && row.ScheduledOn >= FIFromDate
        //                                             && row.ScheduledOn <= FIEndDate
        //                                             select row).ToList();
        //                    }
        //                }
        //                else
        //                {
        //                    transactionsQuery = (from row in master
        //                                         where row.CustomerID == CustomerID
        //                                         && row.ScheduledOn <= FIEndDate
        //                                         select row).ToList();
        //                }
        //            }
        //            PassEndValue = FIEndDate;
        //        }
        //        if (AuthenticationHelper.Role != "AUDT")
        //        {
        //            if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //            {
        //                PassEndValue = FIEndDate;
        //                FromDate = FIFromDate;
        //            }
        //        }
        //        if (approver == true)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
        //        }
        //        try
        //        {

        //            int Count = 1;
        //            ProcessList.ForEach(EachProcess =>
        //            {

        //                //transactionsQuery = (from row in master
        //                //                     where row.CustomerID == CustomerID
        //                //                     && row.WidgetID == EachProcess.ID
        //                //                     select row).ToList();

        //                transactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
        //                if (transactionsQuery.Count > 0)
        //                {
        //                    long totalPieCompletedCritical = 0;
        //                    long totalPieCompletedHIGH = 0;
        //                    long totalPieCompletedMEDIUM = 0;
        //                    long totalPieCompletedLOW = 0;

        //                    long totalPieAfterDueDateCritical = 0;
        //                    long totalPieAfterDueDateHIGH = 0;
        //                    long totalPieAfterDueDateMEDIUM = 0;
        //                    long totalPieAfterDueDateLOW = 0;

        //                    long totalPieNotCompletedCritical = 0;
        //                    long totalPieNotCompletedHIGH = 0;
        //                    long totalPieNotCompletedMEDIUM = 0;
        //                    long totalPieNotCompletedLOW = 0;

        //                    long totalPieNotCompliedCritical = 0;
        //                    long totalPieNotCompliedHIGH = 0;
        //                    long totalPieNotCompliedMEDIUM = 0;
        //                    long totalPieNotCompliedLOW = 0;

        //                    System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
        //                    newDivName = "DivProcessObs" + Count;
        //                    dynDiv.ID = newDivName;
        //                    dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
        //                    dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
        //                    if (Count != 1)
        //                    {
        //                        dynDiv.Style["margin-top"] = "50px";
        //                    }
        //                    dynDiv.Attributes.Add("class", "FirstDiv");
        //                    DivGraphProcessObsStatus.Controls.Add(dynDiv);


        //                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                    //Critical
        //                    totalPieAfterDueDateCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
        //                    || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



        //                    //High
        //                    totalPieAfterDueDateHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

        //                    //Medium
        //                    totalPieAfterDueDateMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


        //                    //Low
        //                    totalPieAfterDueDateLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


        //                    //EachProcess.WidgetName
        //                    //ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
        //                    ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
        //                        "{ chart:{type: 'column'},title:{ text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
        //                        "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
        //                        "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
        //                        "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
        //                        "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

        //                    //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

        //                    ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

        //                             // Not Completed - Critical
        //                             "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

        //                             "}}},{" +

        //                             // Not Completed - High
        //                             "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{" +

        //                             // Not Completed - Medium
        //                             " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{  " +

        //                             // Not Completed - Low

        //                             "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

        //                             "}}}]}," +


        //                            "{name: 'Not Complied',color: '#2195f2',data: [{ " +

        //                            // Not Complied - Critical

        //                            "y: " + totalPieNotCompliedCritical + ",events:{click: function(e) { " +
        //                            " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',3)" +

        //                            "}}},{   " +

        //                            // Not Complied - High

        //                            "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
        //                             " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',0)" +

        //                            "}}},{   " +

        //                            // Not Complied - Medium
        //                            "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',1)" +

        //                            "}}},{   " +
        //                            // Not Complied - Low
        //                            "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
        //                            " fpopulatedwidgetdata('Critical','Not Complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',2)" +

        //                            "}}}]}," +


        //                              "{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
        //                             // After Due Date - Critical

        //                             "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
        //                             " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

        //                             "}}},{   " +


        //                             // After Due Date - High

        //                             "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
        //                             " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{   " +

        //                             // After Due Date - Medium
        //                             "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{   " +
        //                             // After Due Date - Low
        //                             "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


        //                             "}}}]}," +
        //                             "{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +


        //                             // In Time - Critical
        //                             "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

        //                             "}}},{  " +


        //                             // In Time - High
        //                             "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{  " +


        //                             // In Time - Medium
        //                             "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{  " +
        //                             // In Time - Low
        //                             "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
        //                              " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

        //                                "}}}]}]});";



        //                    ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

        //                    Count++;
        //                }
        //            });
        //        }
        //        catch (Exception ex)
        //        {
        //            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        }

        //    }

        //}

        public void GetWidgetDashboardCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProcessWiseObservationStatusChart = string.Empty;
                String newDivName = String.Empty;
                String ProcesswiseObservationSeries = String.Empty;
                List<WidgetMaster> ProcessList = new List<WidgetMaster>();

                ProcessList = (from C in entities.WidgetMasters
                               where C.CustomerID == CustomerID
                               select C).ToList();

                List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
                master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
                          select C).ToList();

                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null)
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                }

                if (Branchlist.Count > 0)
                {
                    master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                DateTime? PassEndValue;
                if (FromDate != null && EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate
                                         && row.ScheduledOn < EndDateF
                                         select row).ToList();

                    PassEndValue = EndDateF;
                }
                else if (FromDate != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                         select row).ToList();

                    PassEndValue = EndDate;
                }
                else if (EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID && row.ScheduledOn < EndDateF
                                         select row).ToList();


                    PassEndValue = EndDateF;
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in master
                                             where row.CustomerID == CustomerID
                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                             || row.ScheduledOn < EndDate)
                                             select row).ToList();
                    }
                    else
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            if (AuthenticationHelper.Role == "AUDT")
                            {
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                     || row.ScheduledOn < FIEndDate)
                                                     select row).ToList();
                            }
                            else
                            {
                                //For Financial Year
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && row.ScheduledOn >= FIFromDate
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        else
                        {
                            transactionsQuery = (from row in master
                                                 where row.CustomerID == CustomerID
                                                 && row.ScheduledOn < FIEndDate
                                                 select row).ToList();
                        }
                    }
                    PassEndValue = FIEndDate;
                }
                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                    {
                        PassEndValue = FIEndDate;
                        FromDate = FIFromDate;
                    }
                }
                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }
                try
                {

                    int Count = 1;
                    ProcessList.ForEach(EachProcess =>
                    {
                        var fltertransactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
                        //transactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
                        if (fltertransactionsQuery.Count > 0)
                        {
                            long totalPieCompletedCritical = 0;
                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateCritical = 0;
                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedCritical = 0;
                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            long totalPieNotCompliedCritical = 0;
                            long totalPieNotCompliedHIGH = 0;
                            long totalPieNotCompliedMEDIUM = 0;
                            long totalPieNotCompliedLOW = 0;

                            System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                            newDivName = "DivProcessObs" + Count;
                            dynDiv.ID = newDivName;
                            dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
                            dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                            if (Count != 1)
                            {
                                dynDiv.Style["margin-top"] = "50px";
                            }
                            dynDiv.Attributes.Add("class", "FirstDiv");
                            DivGraphProcessObsStatus.Controls.Add(dynDiv);


                            fltertransactionsQuery = fltertransactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //Critical
                            totalPieAfterDueDateCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
                            || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



                            //High
                            totalPieAfterDueDateHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

                            //Medium
                            totalPieAfterDueDateMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


                            //Low
                            totalPieAfterDueDateLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


                            //EachProcess.WidgetName
                            //ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                            ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                                "{ chart:{type: 'column'},title:{ align:'left',text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
                                "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
                                "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
                                "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
                                "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

                            //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                            ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

                                     // Not Completed - Critical
                                     "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +
                                     //" fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +
                                     "}}},{" +

                                     // Not Completed - High
                                     "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{" +

                                     // Not Completed - Medium
                                     " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +

                                     // Not Completed - Low

                                     "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                     "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +


                                     // After Due Date - Critical

                                     "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{   " +


                                     // After Due Date - High

                                     "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{   " +

                                     // After Due Date - Medium
                                     "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{   " +
                                     // After Due Date - Low
                                     "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


                                     "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +


                                     // In Time - Critical
                                     "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{  " +


                                     // In Time - High
                                     "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{  " +


                                     // In Time - Medium
                                     "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +
                                     // In Time - Low
                                     "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                                      " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                        "}}}]}]});";

                            ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                            Count++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        //public void GetWidgetDashboardCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        String newDivName = String.Empty;
        //        String ProcesswiseObservationSeries = String.Empty;
        //        List<WidgetMaster> ProcessList = new List<WidgetMaster>();

        //        ProcessList = (from C in entities.WidgetMasters
        //                       where C.CustomerID == CustomerID
        //                       select C).ToList();

        //        List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
        //        master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
        //                  select C).ToList();

        //        if (AuthenticationHelper.Role != "AUDT")
        //        {
        //            if (FromDate == null && EndDateF == null)
        //            {
        //                if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //                {
        //                    master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
        //                }
        //            }

        //        }

        //        if (Branchlist.Count > 0)
        //        {
        //            master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
        //        }

        //        List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
        //        DateTime EndDate = DateTime.Today.Date;
        //        DateTime? PassEndValue;
        //        if (FromDate != null && EndDateF != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID
        //                                 && row.ScheduledOn >= FromDate
        //                                 && row.ScheduledOn <= EndDateF
        //                                 select row).ToList();

        //            PassEndValue = EndDateF;
        //        }
        //        else if (FromDate != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID
        //                                 && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
        //                                 select row).ToList();

        //            PassEndValue = EndDate;
        //        }
        //        else if (EndDateF != null)
        //        {
        //            transactionsQuery = (from row in master
        //                                 where row.CustomerID == CustomerID && row.ScheduledOn <= EndDateF
        //                                 select row).ToList();


        //            PassEndValue = EndDateF;
        //        }
        //        else
        //        {
        //            if (approver == true)
        //            {
        //                transactionsQuery = (from row in master
        //                                     where row.CustomerID == CustomerID
        //                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
        //                                     || row.ScheduledOn <= EndDate)
        //                                     select row).ToList();
        //            }
        //            else
        //            {
        //                if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //                {
        //                    if (AuthenticationHelper.Role == "AUDT")
        //                    {
        //                        transactionsQuery = (from row in master
        //                                             where row.CustomerID == CustomerID
        //                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
        //                                             || row.ScheduledOn <= FIEndDate)
        //                                             select row).ToList();
        //                    }
        //                    else
        //                    {
        //                        //For Financial Year
        //                        transactionsQuery = (from row in master
        //                                             where row.CustomerID == CustomerID
        //                                             && row.ScheduledOn >= FIFromDate
        //                                             && row.ScheduledOn <= FIEndDate
        //                                             select row).ToList();
        //                    }
        //                }
        //                else
        //                {
        //                    transactionsQuery = (from row in master
        //                                         where row.CustomerID == CustomerID
        //                                         && row.ScheduledOn <= FIEndDate
        //                                         select row).ToList();
        //                }
        //            }
        //            PassEndValue = FIEndDate;
        //        }
        //        if (AuthenticationHelper.Role != "AUDT")
        //        {
        //            if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
        //            {
        //                PassEndValue = FIEndDate;
        //                FromDate = FIFromDate;
        //            }
        //        }
        //        if (approver == true)
        //        {
        //            transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
        //        }
        //        try
        //        {

        //            int Count = 1;
        //            ProcessList.ForEach(EachProcess =>
        //            {
        //                transactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
        //                if (transactionsQuery.Count > 0)
        //                {
        //                    long totalPieCompletedCritical = 0;
        //                    long totalPieCompletedHIGH = 0;
        //                    long totalPieCompletedMEDIUM = 0;
        //                    long totalPieCompletedLOW = 0;

        //                    long totalPieAfterDueDateCritical = 0;
        //                    long totalPieAfterDueDateHIGH = 0;
        //                    long totalPieAfterDueDateMEDIUM = 0;
        //                    long totalPieAfterDueDateLOW = 0;

        //                    long totalPieNotCompletedCritical = 0;
        //                    long totalPieNotCompletedHIGH = 0;
        //                    long totalPieNotCompletedMEDIUM = 0;
        //                    long totalPieNotCompletedLOW = 0;

        //                    long totalPieNotCompliedCritical = 0;
        //                    long totalPieNotCompliedHIGH = 0;
        //                    long totalPieNotCompliedMEDIUM = 0;
        //                    long totalPieNotCompliedLOW = 0;

        //                    System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
        //                    newDivName = "DivProcessObs" + Count;
        //                    dynDiv.ID = newDivName;
        //                    dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
        //                    dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
        //                    if (Count != 1)
        //                    {
        //                        dynDiv.Style["margin-top"] = "50px";
        //                    }
        //                    dynDiv.Attributes.Add("class", "FirstDiv");
        //                    DivGraphProcessObsStatus.Controls.Add(dynDiv);


        //                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

        //                    //Critical
        //                    totalPieAfterDueDateCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
        //                    || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



        //                    //High
        //                    totalPieAfterDueDateHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

        //                    //Medium
        //                    totalPieAfterDueDateMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


        //                    //Low
        //                    totalPieAfterDueDateLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

        //                    totalPieCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

        //                    totalPieNotCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
        //                    || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

        //                    totalPieNotCompliedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


        //                    //EachProcess.WidgetName
        //                    //ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
        //                    ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
        //                        "{ chart:{type: 'column'},title:{ text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
        //                        "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
        //                        "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
        //                        "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
        //                        "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

        //                    //" fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

        //                    ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

        //                             // Not Completed - Critical
        //                             "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +
        //                             //" fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +
        //                             "}}},{" +

        //                             // Not Completed - High
        //                             "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{" +

        //                             // Not Completed - Medium
        //                             " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{  " +

        //                             // Not Completed - Low

        //                             "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

        //                             "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +


        //                             // After Due Date - Critical

        //                             "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
        //                             " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

        //                             "}}},{   " +


        //                             // After Due Date - High

        //                             "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
        //                             " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{   " +

        //                             // After Due Date - Medium
        //                             "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{   " +
        //                             // After Due Date - Low
        //                             "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


        //                             "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +


        //                             // In Time - Critical
        //                             "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

        //                             "}}},{  " +


        //                             // In Time - High
        //                             "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

        //                             "}}},{  " +


        //                             // In Time - Medium
        //                             "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
        //                             " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

        //                             "}}},{  " +
        //                             // In Time - Low
        //                             "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
        //                              " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

        //                                "}}}]}]});";

        //                    ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

        //                    Count++;
        //                }
        //            });
        //        }
        //        catch (Exception ex)
        //        {
        //            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        }
        //    }
        //}
        private void BindLocationCount(int customerID, bool IsPenaltyVisible)
        {
            try
            {
                if (AuthenticationHelper.Role == "AUDT")
                {
                    try
                    {
                        txtAdvStartDate.Text = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                        txtAdvEndDate.Text = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");                                          
                    }
                    catch
                    {
                    }
                }
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                }

                bool IsApproverInternal = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    // STT Change- Add Status
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == customerID.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    if (ddlStatus.SelectedItem.Text == "Statutory")
                    {                                               
                        #region Statutory
                        IsSatutoryInternal = "Statutory";                                                
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {                            
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                                {
                                    try
                                    {
                                        MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Statutory Error exception :" + cashstatutoryval);
                                        if (IsApprover == true)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApprover == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }

                            if (IsPenaltyVisible == true)
                            {
                                GetManagementCompliancePenalitySummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApprover);
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardNoCompliedCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                GetManagementCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }                            
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible);
                        }
                        else
                        {
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                                {
                                    try
                                    {

                                        MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                    }
                                    catch (Exception ex)
                                    {

                                        LogLibrary.WriteErrorLog("GET Statutory Error exception : " + cashstatutoryval);
                                        if (IsApprover == true)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApprover == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }

                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }                                       
                            if (IsPenaltyVisible == true)
                            {
                                GetManagementCompliancePenalitySummary(customerID, Branchlist, -1, ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApprover);
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardNoCompliedCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                  "T", FromFinancialYearSummery, ToFinancialYearSummery
                                  , IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                GetManagementCompliancesSummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                          //  BindOverDueCOmplianceList(MasterManagementCompliancesSummaryQuery, MasterManagementInternalCompliancesSummaryQuery);
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible);
                        }                      
                        #endregion
                    }
                    else
                    {
                        
                        #region Internal
                        divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                        IsSatutoryInternal = "Internal";
                        var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                   where row.RoleID == 6
                                                   && row.UserID == AuthenticationHelper.UserID
                                                   select row).ToList();
                        if (GetApproverInternal.Count > 0)
                        {                            
                            IsApprover = true;
                            IsApproverInternal = true;
                        }
                        else
                        {                            
                            IsApprover = false;
                            IsApproverInternal = false;
                        }                       
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                                {
                                    try
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Internal Error exception :" + cashinternalval);
                                        if (IsApprover == true)
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApprover == true)
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Internal Error exception :" + cashinternalval);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Internal Error exception :" + cashinternalval);
                                if (IsApprover == true)
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            else
                            {
                                GetManagementInternalCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            //BindOverDueCOmplianceList(MasterManagementCompliancesSummaryQuery, MasterManagementInternalCompliancesSummaryQuery);
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible);
                        }
                        else
                        {
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                                {
                                    try
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Internal Error exception :" + cashinternalval);
                                        if (IsApprover == true)
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApprover == true)
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Internal Error exception :" + cashinternalval);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Internal Error exception :" + cashinternalval);
                                if (IsApprover == true)
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            else
                            {
                                GetManagementInternalCompliancesSummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text),IsApprover);
                            }
                           // BindOverDueCOmplianceList(MasterManagementCompliancesSummaryQuery, MasterManagementInternalCompliancesSummaryQuery);
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible);
                        }
                        #endregion
                    }


                    #region Top Count
                    if (ddlStatus.SelectedItem.Text == "Internal")
                    {
                        long internalcategorycount = 0;
                        long internalcompliancecount = 0;
                        long internallocationcount = 0;
                        long internalentitycount = 0;
                        long internalucount = 0;

                        #region Internal                       
                        if (IsApproverInternal)
                        {
                            #region IsApprover  
                            
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "I"
                                                && row.userRole== "APPR"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {                                                                                             
                                entities.Database.CommandTimeout = 180;
                                var InternalApproverCompliancedetails = (entities.SP_InternalManagementDashboardApproverComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();                                
                                InternalApproverCompliancedetails = (from row in InternalApproverCompliancedetails
                                                                     select row).Distinct().ToList();

                                if (InternalApproverCompliancedetails.Count > 0)
                                {                                    
                                    if (Branchlist.Count > 0)
                                    {
                                        InternalApproverCompliancedetails = InternalApproverCompliancedetails.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        internalcategorycount = InternalApproverCompliancedetails.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = InternalApproverCompliancedetails.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = InternalApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        internalcategorycount = InternalApproverCompliancedetails.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = InternalApproverCompliancedetails.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = InternalApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    internalentitycount = (entities.sp_EntitycountApprover((int)AuthenticationHelper.UserID, "INT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    internalucount = (from row in entities.Users
                                                  where row.CustomerID == AuthenticationHelper.CustomerID
                                                  && row.IsActive == true && row.IsDeleted == false
                                                  select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(internalcategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(internalentitycount);
                                    divLocationCount.InnerText = Convert.ToString(internallocationcount);
                                    divUsersCount.InnerText = Convert.ToString(internalucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }                                                                
                            }
                            else
                            {                                
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Management                            
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "I"
                                                && row.userRole == "MGMT"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_InternalManagementDashboardManagerComplianceCount_Result> ManagerInternalComplianceCount = new List<SP_InternalManagementDashboardManagerComplianceCount_Result>();
                                entities.Database.CommandTimeout = 180;
                                var ManagerInternalCompliancedetails = (entities.SP_InternalManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                ManagerInternalComplianceCount = (from row in ManagerInternalCompliancedetails
                                                                  select row).Distinct().ToList();
                                if (ManagerInternalComplianceCount.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ManagerInternalComplianceCount = ManagerInternalComplianceCount.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        internalcategorycount = ManagerInternalComplianceCount.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = ManagerInternalComplianceCount.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = ManagerInternalComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        internalcategorycount = ManagerInternalComplianceCount.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = ManagerInternalComplianceCount.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = ManagerInternalComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    internalentitycount = (entities.sp_Entitycount((int)AuthenticationHelper.UserID, "INT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    internalucount = (from row in entities.Users
                                              where row.CustomerID == AuthenticationHelper.CustomerID
                                              && row.IsActive == true && row.IsDeleted == false
                                              select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(internalcategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(internalentitycount);
                                    divLocationCount.InnerText = Convert.ToString(internallocationcount);
                                    divUsersCount.InnerText = Convert.ToString(internalucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        long statutorycategorycount = 0;
                        long statutorycompliancecount = 0;
                        long statutorylocationcount = 0;
                        long statutoryentitycount = 0;
                        long statutoryucount = 0;
                        #region Statutory
                        if (IsApprover == true)
                        {
                            #region IsApprover
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "S"
                                                && row.userRole == "APPR"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {                                
                                List<SP_ManagementDashboardApproverComplianceCount_Result> ApproverComplianceCount = new List<SP_ManagementDashboardApproverComplianceCount_Result>();
                                var ApproverCompliancedetails = (entities.SP_ManagementDashboardApproverComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                                ApproverCompliancedetails = (from row in ApproverCompliancedetails
                                                             select row).Distinct().ToList();
                                if (ApproverCompliancedetails.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ApproverCompliancedetails = ApproverCompliancedetails.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        statutorycategorycount = ApproverCompliancedetails.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ApproverCompliancedetails.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        statutorycategorycount = ApproverCompliancedetails.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ApproverCompliancedetails.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    statutoryentitycount = (entities.sp_EntitycountApprover((int)AuthenticationHelper.UserID, "SAT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    statutoryucount = (from row in entities.Users
                                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                                       && row.IsActive == true && row.IsDeleted == false
                                                       select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(statutorycategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(statutoryentitycount);
                                    divLocationCount.InnerText = Convert.ToString(statutorylocationcount);
                                    divUsersCount.InnerText = Convert.ToString(statutoryucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }                                                                
                                                              
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Management
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "S"
                                                && row.userRole == "MGMT"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_ManagementDashboardManagerComplianceCount_Result> ManagerComplianceCount = new List<SP_ManagementDashboardManagerComplianceCount_Result>();
                                entities.Database.CommandTimeout = 180;
                                var ManagerCompliancedetails = (entities.SP_ManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                ManagerComplianceCount = (from row in ManagerCompliancedetails
                                                          select row).Distinct().ToList();
                                if (ManagerComplianceCount.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ManagerComplianceCount = ManagerComplianceCount.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    statutoryentitycount = (entities.sp_Entitycount((int)AuthenticationHelper.UserID, "SAT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    statutoryucount = (from row in entities.Users
                                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                                       && row.IsActive == true && row.IsDeleted == false
                                                       select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(statutorycategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(statutoryentitycount);
                                    divLocationCount.InnerText = Convert.ToString(statutorylocationcount);
                                    divUsersCount.InnerText = Convert.ToString(statutoryucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                            }
                            #endregion
                        }
                        #endregion
                    }                    
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Dept
        public void GetManagementCompliancesSummaryDEPT(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChartDEPT;                                
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChartDEPT = string.Empty;                       
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }                   
                }
                else
                {
                    perFunctionChartDEPT = string.Empty;                  
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;


                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));


                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S");
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;


                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;


                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;


                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;


                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();


                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;


                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;


                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;


                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;


                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;


                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;

                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChartDEPT = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;
                    
                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            perFunctionChartDEPT = tempperFunctionChart;                            
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        public void GetManagementInternalCompliancesSummaryDEPT(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false)
        {
            try
            {
                string tempperFunctionChart = perFunctionChartDEPT;           
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                }
                else
                {
                    perFunctionChartDEPT = string.Empty;                
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    entities.Database.CommandTimeout = 180;
                    if (approver == true)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                    }
                    else
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT"))
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                        else
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "DEPT")).ToList();
                    }
                    if (Branchlist.Count > 0)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = MasterManagementInternalCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate
                                             && row.InternalScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate && row.InternalScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.InternalScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                 || row.InternalScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                         || row.InternalScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.InternalScheduledOn >= FIFromDate
                                                         && row.InternalScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.InternalScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();




                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I");
                    }                  
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {

                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;


                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DH_summary')" +
                                "}}},";




                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;


                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['In Time', " + CompletedCountLow + "]," +
                                " ['After due date'," + AfterDueDatecountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DL_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['In Time', " + CompletedCountCritical + "]," +
                                " ['After due date'," + AfterDueDatecountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DC_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChartDEPT = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;
                
                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            perFunctionChartDEPT = tempperFunctionChart;                        
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false)
        {
            try
            {
                string tempperFunctionChart = perFunctionChartDEPT;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                }
                else
                {
                    perFunctionChartDEPT = string.Empty;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    entities.Database.CommandTimeout = 180;
                    if (approver == true)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                    }
                    else
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT"))
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                        else
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "DEPT")).ToList();
                    }
                    if (Branchlist.Count > 0)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = MasterManagementInternalCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate
                                             && row.InternalScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate && row.InternalScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.InternalScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                 || row.InternalScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                         || row.InternalScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.InternalScheduledOn >= FIFromDate
                                                         && row.InternalScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.InternalScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotCompliedCount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL= 0;

                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I");
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcountHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.InternalComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not complied', " + NotCompliedcountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DH_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcountHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.InternalComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "], " +
                                " ['Not complied', " + NotCompliedcountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.InternalComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['In Time', " + CompletedCountLow + "]," +
                                " ['After due date'," + AfterDueDatecountLow + "]," +
                                " ['Not complied'," + NotCompliedcountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DL_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;


                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7 || entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3
                            || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.InternalComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['In Time', " + CompletedCountCritical + "]," +
                                " ['After due date'," + AfterDueDatecountCritical + "]," +
                                " ['Not complied'," + NotCompliedcountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Internal','DeptFunctionBarChart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','DC_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;
                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChartDEPT = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            perFunctionChartDEPT = tempperFunctionChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChartDEPT;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChartDEPT = string.Empty;
                    }
                }
                else
                {
                    perFunctionChartDEPT = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotCompliedCount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S");
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcountHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not complied', " + NotCompliedcountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddepartmentdata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcountHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not complied', " + NotCompliedcountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not complied'," + NotCompliedcountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;

                            //Low
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3
                            || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not complied'," + NotCompliedcountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddepartmentdata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;

                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChartDEPT = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            perFunctionChartDEPT = tempperFunctionChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationCount2(List<SP_GetManagementCompliancesSummary_Result> masterquery, bool IsPenaltyVisible)
        {
            try
            {
                // STT Change- Add Status
                string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == customerID.ToString())
                        {
                            IsNotCompiled = true;
                            break;
                        }
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ddlStatus.SelectedItem.Text == "Statutory")
                    {
                        #region Statutory
                        IsSatutoryInternal = "Statutory";
                        List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {
                            MasterManagementCompliancesSummaryQuery = (from row in masterquery
                                                                       join row1 in entities.DepartmentMappings
                                                                       on row.DepartmentID equals row1.DepartmentID
                                                                       select row).ToList();
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                GetManagementCompliancesSummaryDEPT(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                        }
                        else
                        {

                            MasterManagementCompliancesSummaryQuery = (from row in masterquery
                                                                       join row1 in entities.DepartmentMappings
                                                                       on row.DepartmentID equals row1.DepartmentID
                                                                       select row).ToList();
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                GetManagementCompliancesSummaryDEPT(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Internal
                        perPenaltyStatusPieChart = string.Empty;
                        divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                        IsSatutoryInternal = "Internal";
                        //if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        //{
                        //    GetManagementInternalCompliancesSummaryDEPT(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                        //}
                        //else
                        //{
                        //    GetManagementInternalCompliancesSummaryDEPT(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                        //}

                        if (IsNotCompiled == true)
                        {
                            if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                            {
                                GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            else
                            {
                                GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                        }
                        else
                        {
                            if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                            {
                                GetManagementInternalCompliancesSummaryDEPT(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            else
                            {
                                GetManagementInternalCompliancesSummaryDEPT(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #endregion

        #region Top DropDown Selected Index Change

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);
                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                    criticalcolor.Value = Cmd.Critical;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (customerID == -1)
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                    {
                        Branchlist.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        Branchlist.ToList();
                    }
                    else
                    {
                        BindLocationFilter();
                    }
                    BindLocationCount(customerID, IsPenaltyVisible);
                    BindGradingReportSummaryTree();
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //protected void btnTopSearch_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {

        //            if (customerID == -1)
        //            {
        //                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //            }
        //            Branchlist.Clear();
        //            GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
        //            BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
        //            Branchlist.ToList();                    
        //            BindLocationCount(customerID, IsPenaltyVisible);

        //            BindGradingReportSummaryTree();
        //        }
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                }               
                try
                {
                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                    StackExchangeRedisExtensions.Remove(cashinternalval);
                    StackExchangeRedisExtensions.Remove(cashTimeval);
                }
                catch (Exception)
                {
                    LogLibrary.WriteErrorLog("Remove btnRefresh_Click Error exception :" + cashstatutoryval);
                }
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == customerID.ToString())
                        {
                            IsPenaltyVisible = false;
                        }
                    }
                }
                BindLocationCount(customerID, IsPenaltyVisible);
            
                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                {
                    //CacheHelper.Get<DateTime>(cashTimeval, out ss);                                  
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {
                        //Label1.Text = "last updated :" + ss.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                }
                else
                {
                    Label1.Text = "Last updated within the last hour";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

            #region  Function Report

        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "";                 
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }
                    var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);

                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);               
                    BindLocationFilterPenalty(bracnhes, LocationList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilterPenalty(List<NameValueHierarchy> bracnhes, List<int> LocationList)
        {
            try
            {
                tvFilterLocationPenalty.Nodes.Clear();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPenalty.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPenalty.Nodes.Add(node);
                }

                tvFilterLocationPenalty.CollapseAll();
                tvFilterLocationPenalty_SelectedNodeChanged(null, null);
                BindLocationFilterGraddingReport(bracnhes, LocationList);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       
        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }

        }
      
        public void GetManagementInternalCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementInternalCompliancesSummary_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                }
                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                   
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;                   
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementInternalCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();                    
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                 
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<InternalCompliancesCategory> CatagoryList = new List<InternalCompliancesCategory>();

                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID);
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {

                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;


                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12  || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')" +
                                "}}},";




                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;


                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['In Time', " + CompletedCountLow + "]," +
                                " ['After due date'," + AfterDueDatecountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['In Time', " + CompletedCountCritical + "]," +
                                " ['After due date'," + AfterDueDatecountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;



                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;


                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',}],}],";
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, " +
                        " series: [{id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // In time - High " +
                        // " e.point.name;"+
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                        "},},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // In time - Medium" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // In time - Low" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +
                         "},},},{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // In time - Critical" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +


                        " },},}],},{id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // After Due Date - High "+
                         " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // After Due Date - Medium "+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // After Due Date - Low"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +
                         " },},},{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // After Due Date - Critical"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +


                        " },},}],},{id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                       // " // Not Completed - High "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                       // " // Not Completed - Medium "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                       // " // Not Completed - Low"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +
                        " },},},{name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                       // " // Not Completed - Critical"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +


                       "},},}],}],},";


                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                  // Not Completed - High
                  "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                   " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                  "}}},{" +

                  // Not Completed - Medium
                  " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                  "}}},{  " +

                  // Not Completed - Low

                  "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +

                     "}}},{  " +

                  // Not Completed - Critical

                  "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                  "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +

                  // After Due Date - High

                  "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                  "}}},{   " +

                  // After Due Date - Medium
                  "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                  "}}},{   " +
                  // After Due Date - Low
                  "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +

                   "}}},{   " +
                  // After Due Date - Critical
                  "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                  "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                  // In Time - High
                  "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                  " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                  "}}},{  " +
                  // In Time - Medium
                  "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                  "}}},{  " +
                  // In Time - Low
                  "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +
                  
                  "}}},{  " +
                  // In Time - Critical
                  "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +


                  "}}}]}]";

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementInternalCompliancesSummary_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                }
                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementInternalCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieNotCompliedCount = 0;
                    long totalPieAfterDueDatecount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<InternalCompliancesCategory> CatagoryList = new List<InternalCompliancesCategory>();

                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID);
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";


                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;
                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcountHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not complied', " + NotCompliedcountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcountHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcountHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "], " +
                                " ['Not complied', " + NotCompliedcountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['In Time', " + CompletedCountLow + "]," +
                                " ['After due date'," + AfterDueDatecountLow + "]," +
                                " ['Not complied'," + NotCompliedcountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['In Time', " + CompletedCountCritical + "]," +
                                " ['After due date'," + AfterDueDatecountCritical + "]," +
                                " ['Not complied'," + NotCompliedcountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;
                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;

                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'Not Complied',y: " + totalPieNotCompliedCount + ",color: '#2195f2',drilldown: 'notComplied',},{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',}],}],";
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, " +
                        " series: [{id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // In time - High " +
                        // " e.point.name;"+
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                        "},},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // In time - Medium" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // In time - Low" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +
                        "},},},{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // In time - Critical" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +

                        " },},}],},{id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // After Due Date - High "+
                         " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // After Due Date - Medium "+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // After Due Date - Low"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +
                         " },},},{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // After Due Date - Critical"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +

                        " },},}],},{id: 'notComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompliedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // Not Complied - High "+
                         " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompliedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // Not Complied - Medium "+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompliedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Not Complied - Low"+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOL_Psummary')" +
                         " },},},{name: 'Critical',y: " + totalPieNotCompliedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Not Complied - Critical"+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOC_Psummary')" +

                        " },},}],},{id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                       // " // Not Completed - High "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                       // " // Not Completed - Medium "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                       // " // Not Completed - Low"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +
                        " },},},{name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                       // " // Not Completed - Critical"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +


                       "},},}],}],},";


                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                  // Not Completed - High
                  "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                   " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                  "}}},{" +

                  // Not Completed - Medium
                  " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                  "}}},{  " +

                  // Not Completed - Low

                  "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +

                     "}}},{  " +

                  // Not Completed - Critical

                  "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                     "}}}]},{name: 'Not Complied',color: '#2195f2',data: [{" +

                  // Not Complied - High

                  "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOH_Rsummary')" +

                  "}}},{   " +

                  // Not Complied - Medium
                  "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOM_Rsummary')" +

                  "}}},{   " +
                  // Not Complied - Low
                  "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOL_Rsummary')" +
                   
                  "}}},{   " +
                  // Not Complied - Critical
                  "y: " + totalPieNotCompliedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOC_Rsummary')" +

                  "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +

                  // After Due Date - High

                  "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                  "}}},{   " +

                  // After Due Date - Medium
                  "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                  "}}},{   " +
                  // After Due Date - Low
                  "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +

                   "}}},{   " +
                  // After Due Date - Critical
                  "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                  "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                  // In Time - High
                  "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                  " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                  "}}},{  " +
                  // In Time - Medium
                  "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                  "}}},{  " +
                  // In Time - Low
                  "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +
                  
                  "}}},{  " +
                  // In Time - Critical
                  "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                  "}}}]}]";

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion



        #region Statutory
        public void GetPenaltyQuarterWise(int customerid, int CustomerbranchID, string QuarterName, List<SP_GetManagementCompliancesSummary_Result> masterquarterquery, DateTime? startDate = null, DateTime? EndDate = null, bool approver = false)
        {
            List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (approver == true)
                {
                    transactionsQuery = (from row in masterquarterquery
                                         where row.CustomerID == customerid
                                         && row.ScheduledOn >= startDate
                                         && row.ScheduledOn <= EndDate
                                         && row.PenaltySubmit == "S" && row.RoleID == 4
                                         select row).ToList();

                }
                else
                {
                    transactionsQuery = (from row in masterquarterquery
                                         where row.CustomerID == customerid
                                         && row.ScheduledOn >= startDate
                                         && row.ScheduledOn <= EndDate
                                         && row.PenaltySubmit == "S" && row.RoleID == 4
                                         && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         select row).ToList();

                }

                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }

                transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                decimal highcount;
                decimal mediumCount;
                decimal lowcount;
                decimal criticalcount;
                decimal totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Quarter", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Critical", typeof(long));

                string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                totalcount = highcount + mediumCount + lowcount + criticalcount;
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternal = "Internal";
                }
                if (totalcount != 0)
                {
                    //High
                    decimal AfterDueDatecountHigh;
                    decimal CompletedCountHigh;
                    decimal NotCompletedcountHigh;
                    if (ColoumnNames[2] == "High")
                    {
                        perFunctionHIGHPenalty += "{ name:'" + QuarterName + "', y: " + highcount + ",drilldown: 'high' + '" + QuarterName + "',events:{click: function(e){fpopulatedPenaltydata(e.point.name, 'High', " + customerid + ", " + CustomerbranchID + ", '" + startDate + "', '" + EndDate + "', '" + approver + "','" + IsSatutoryInternal + "','PNH_psummary')}},},";
                    }
                    AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);
                    //Medium
                    decimal AfterDueDatecountMedium;
                    decimal CompletedCountMedium;
                    decimal NotCompletedcountMedium;

                    if (ColoumnNames[3] == "Medium")
                    {
                        perFunctionMEDIUMPenalty += "{ name:'" + QuarterName + "', y: " + mediumCount + ",drilldown: 'mid' + '" + QuarterName + "',events: { click: function(e){fpopulatedPenaltydata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNM_psummary')}},},";

                    }
                    AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                    //Low
                    decimal AfterDueDatecountLow;
                    decimal CompletedCountLow;
                    decimal NotCompletedcountLow;

                    if (ColoumnNames[4] == "Low")
                    {
                        perFunctionLOWPenalty += "{ name:'" + QuarterName + "', y: " + lowcount + ",drilldown: 'low' + '" + QuarterName + "',events: { click: function(e){ fpopulatedPenaltydata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNL_psummary')}},},";

                    }

                    AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                    //Critical
                    decimal AfterDueDatecountCritical;
                    decimal CompletedCountCritical;
                    decimal NotCompletedcountCritical;

                    if (ColoumnNames[5] == "Critical")
                    {
                        perFunctionCRITICALPenalty += "{ name:'" + QuarterName + "', y: " + criticalcount + ",drilldown: 'critical' + '" + QuarterName + "',events: { click: function(e){ fpopulatedPenaltydata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNC_psummary')}},},";

                    }

                    AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                }
            }
        }        
        public void GetManagementCompliancePenalitySummary(int customerid, List<long> Branchlist, int CustomerbranchID, string financialYear, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver, int userId = -1)
        {
            try
            {
                string QuarterName = string.Empty;
                int year = 0;
                List<string> q = new List<string>();
                q.Add("Q1");
                q.Add("Q2");
                q.Add("Q3");
                q.Add("Q4");
                perPenaltyStatusPieChart = string.Empty;
                perPenaltyStatusPieChart = string.Empty;
                string[] y = financialYear.Split('-');
                year = Convert.ToInt32(y[0]);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 180;
                    var MasterPenaltyQuarterWiseQuery = (from row in MasterQuery.Where(entry => entry.PenaltySubmit == "S"
                                                        && entry.IsPenaltySave == false && entry.RoleID == 4
                                                        && (entry.NonComplianceType == 2 || entry.NonComplianceType == 0)
                                                        && (entry.Penalty != null || entry.Interest != null))
                                                         select row).ToList();

                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    BindPenaltyAmount(customerID, MasterPenaltyQuarterWiseQuery);
                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    if (approver == true)
                    {
                        approver = true;
                        IsApprover = true;
                    }
                    else
                    {
                        approver = false;
                        IsApprover = false;
                    }


                    perFunctionHIGHPenalty = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUMPenalty = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOWPenalty = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICALPenalty = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    DateTime startDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                    DateTime EndDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");


                    for (int i = 0; i < q.Count; i++)
                    {
                        if (q[i] == "Q1")
                        {
                            QuarterName = "Apr-Jun";
                            startDate = new DateTime(year, 4, 1);
                            EndDate = new DateTime(year, 6, DateTime.DaysInMonth(year, 6));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q2")
                        {
                            QuarterName = "Jul-Sep";
                            startDate = new DateTime(year, 7, 1);
                            EndDate = new DateTime(year, 9, DateTime.DaysInMonth(year, 9));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q3")
                        {
                            QuarterName = "Oct-Dec";
                            startDate = new DateTime(year, 10, 1);
                            EndDate = new DateTime(year, 12, DateTime.DaysInMonth(year, 12));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q4")
                        {
                            QuarterName = "Jan-Mar";
                            startDate = new DateTime((year + 1), 1, 1);
                            EndDate = new DateTime((year + 1), 3, DateTime.DaysInMonth((year + 1), 3));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }

                    }
                    perFunctionHIGHPenalty += "],},";
                    perFunctionMEDIUMPenalty += "],},";
                    perFunctionLOWPenalty += "],},";
                    perFunctionCRITICALPenalty += "],},],";

                    perPenaltyStatusPieChart = perFunctionHIGHPenalty + "" + perFunctionMEDIUMPenalty  + "" + perFunctionLOWPenalty + "" + perFunctionCRITICALPenalty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        public void GetManagementCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;
                    
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn < FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;

                 
                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;



                  

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                          
                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                         
                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;                         

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                          
                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();

                        
                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;

                       
                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;

                          
                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;

                          

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();

                         
                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;


                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;



                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }
                            
                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;

                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;


                    

                    #region Previous Working
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +
                                         "},},},{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +


                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +
                                         " },},},{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +


                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +
                                        " },},},{name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +

                                        " },},}],}],},";
                    #endregion

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    // Not Completed - High
                    "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                     " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                    "}}},{" +

                    // Not Completed - Medium
                    " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                    "}}},{  " +

                    // Not Completed - Low

                    "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                      " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +

                       "}}},{  " +

                    // Not Completed - Critical

                    "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                      " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +


                    "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +

                    // After Due Date - High

                    "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - Medium
                    "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Low
                    "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +

                     "}}},{   " +

                    // After Due Date - Critical
                    "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +


                    "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                    // In Time - High
                    "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                    " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                    "}}},{  " +
                    // In Time - Medium
                    "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                    "}}},{  " +
                    // In Time - Low
                    "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +
                    
                    "}}},{  " +
                    // In Time - Critical
                    "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                    "}}}]}]";

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementCompliancesSummaryAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn < FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn < EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn < EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn < EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn < EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn < FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn < FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn < FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotCompliedCount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0; 

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionHIGH = "series: [ {name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                    perFunctionCRITICAL = "{name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";

                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcounttHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcounttHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['In Time', " + CompletedCountHigh + "], " +
                                " ['After due date', " + AfterDueDatecountHigh + "]," +
                                " ['Not complied', " + NotCompliedcounttHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcounttHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcounttHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['In Time', " + CompletedCountMedium + "]," +
                                " ['After due date', " + AfterDueDatecountMedium + "]," +
                                " ['Not complied', " + NotCompliedcountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['In Time', " + CompletedCountLow + "], " +
                                " ['After due date'," + AfterDueDatecountLow + "], " +
                                " ['Not complied', " + NotCompliedcountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['In Time', " + CompletedCountCritical + "], " +
                                " ['After due date'," + AfterDueDatecountCritical + "], " +
                                " ['Not complied', " + NotCompliedcountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "],], " +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'Statutory','Functionbarchart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;
                        }
                    }
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},";
                    perFunctionCRITICAL += "],},],";
                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + "" + perFunctionCRITICAL + perFunctiondrilldown;

                    #region Previous Working
                    //perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Not Complied',y: " + totalPieNotCompliedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notComplied',},{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',}," +
                        "data: [" +
                               "{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',}," +
                               "{name: 'Not Complied',y: " + totalPieNotCompliedCount + ",color: '#2195f2',drilldown: 'notComplied',}," +
                               "{name: 'After Due Date',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',}, " +
                               "{name: 'In Time',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'In Time',cursor: 'pointer',data: [{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +
                                         "},},},{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +

                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'After Due Date',cursor: 'pointer',data: [{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +
                                          " },},},{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +

                                        " },},}],}," +
                                        " {id: 'notComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompliedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Complied - High
                                        " fpopulateddata(e.point.name,'Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompliedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Complied - Medium
                                        " fpopulateddata(e.point.name,'Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompliedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Complied - Low
                                        " fpopulateddata(e.point.name,'Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOL_Psummary')" +
                                         " },},},{name: 'Critical',y: " + totalPieNotCompliedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Complied - Critical
                                        " fpopulateddata(e.point.name,'Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOC_Psummary')" +

                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +
                                         " },},},{name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +

                                        " },},}],}],},";
                    #endregion

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    // Not Completed - High
                    "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                     " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                    "}}},{" +

                    // Not Completed - Medium
                    " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                    "}}},{  " +

                    // Not Completed - Low

                    "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                      " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +

                      "}}},{  " +

                    // Not Completed - Critical

                    "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                      " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                    "}}}]},{name: 'Not Complied',color: '#2195f2',data: [{" +

                    // Not Complied - High

                    "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOH_Rsummary')" +

                    "}}},{   " +

                    // Not Complied - Medium
                    "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOM_Rsummary')" +

                    "}}},{   " +
                    // Not Complied - Low
                    "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOL_Rsummary')" +

                     "}}},{   " +
                    // Not Complied - Critical
                    "y: " + totalPieNotCompliedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not Complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOC_Rsummary')" +

                    "}}}]},{name: 'After due date',color: perRiskStackedColumnChartColorScheme.low,data: [{" +

                    // After Due Date - High

                    "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - Medium
                    "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Low
                    "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Critical
                    "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                    "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                    // In Time - High
                    "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                    " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                    "}}},{  " +
                    // In Time - Medium
                    "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                    "}}},{  " +
                    // In Time - Low
                    "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +
                    
                    "}}},{  " +
                    // In Time - Critical
                    "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                    "}}}]}]";

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Grading Report
        private void BindMonth(int year)
        {
            try
            {
                ddlmonthsGrading.DataTextField = "Name";
                ddlmonthsGrading.DataValueField = "ID";
                int selectedMonth = 0;
                List<NameValue> months;
                if (year == DateTime.UtcNow.Year)
                {
                    months = Enumerations.GetAll<Month>().Where(entry => entry.ID <= DateTime.UtcNow.Month).ToList();
                    selectedMonth = months.Count;
                }
                else
                {
                    months = Enumerations.GetAll<Month>();
                    selectedMonth = 1;
                }
                ddlmonthsGrading.DataSource = months;
                ddlmonthsGrading.DataBind();
                ddlmonthsGrading.SelectedValue = Convert.ToString(selectedMonth);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindYear()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                for (int i = 1; i < 10; ++i)
                {
                    System.Web.UI.WebControls.ListItem tmp = new System.Web.UI.WebControls.ListItem();
                    tmp.Value = CurrentYear.ToString();
                    tmp.Text = CurrentYear.ToString();
                    ddlYearGrading.Items.Add(tmp);
                    CurrentYear = DateTime.Now.AddYears(-i).Year;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnGradingSearch_Click(object sender, EventArgs e)
        {
            try
            {
                
                BindGradingReportSummaryTree();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFiltergradingscroll1", "fsetscroll();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindGradingReportSummaryTree()
        {
            try
            {
                string statutoryinternal = string.Empty;
                string perRahul = string.Empty;
                int year = Convert.ToInt32(ddlYearGrading.SelectedValue);
                int month = Convert.ToInt32(ddlmonthsGrading.SelectedValue);
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int customerBranchID = -1;
                if (TreeGraddingReport.SelectedValue != "-1")
                {
                    customerBranchID = Convert.ToInt32(TreeGraddingReport.SelectedValue);
                }
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    perRahul = string.Empty;
                    #region Statutory Grading Report
                    statutoryinternal = "Statutory";
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int uid = Convert.ToInt32(AuthenticationHelper.UserID);
                        int period = 12;
                        if (ddlPeriodGrading.SelectedValue != "12")
                        {
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        else
                        {
                            int numberofmonthdifference = 12;
                            if (IsApprover)
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "APPR", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            else
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "MGMT", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            if (numberofmonthdifference <= 3)
                            {
                                ddlPeriodGrading.SelectedValue = "3";
                            }
                            else if (numberofmonthdifference > 3 && numberofmonthdifference < 9)
                            {
                                ddlPeriodGrading.SelectedValue = "6";
                            }
                            else
                            {
                                ddlPeriodGrading.SelectedValue = "12";
                            }
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        var MasterQuery = (from row in entities.GradingPreFillDatas
                                           where row.CustomerID == customerID
                                           && row.UserID == uid && row.IsStatutory == "S"
                                           select row).ToList();
                        if (MasterQuery.Count == 0)
                        {
                            bool sucess = StatutoryGrading.FillStatutoryGrading(customerID, uid);
                            if (sucess)
                            {
                                MasterQuery = (from row in entities.GradingPreFillDatas
                                               where row.CustomerID == customerID
                                               && row.UserID == uid && row.IsStatutory == "S"
                                               select row).ToList();
                            }
                        }

                        if (customerBranchID != -1)
                        {
                            entities.Database.CommandTimeout = 180;
                            var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerBranchID, (int)customerID)
                                             select row).ToList();


                            MasterQuery = MasterQuery.Where(entry => branchIDs.Contains((int)entry.LocationID)).ToList();
                        }
                        var transactionsQuery = MasterQuery;
                        DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                        List<Tuple<string, long, string>> list = new List<Tuple<string, long, string>>();

                        DataTable table = new DataTable();
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Node", typeof(long));
                        table.Columns.Add("PNode", typeof(long));
                        table.Columns.Add("LocationID", typeof(long));
                        table.Columns.Add("Year", typeof(long));
                        for (int i = 1; i <= period; i++)
                        {
                            list.Add(new Tuple<string, long, string>("C" + EndDate.AddMonths(-i).Month, Convert.ToInt64(EndDate.AddMonths(-i).ToString("yyyy")), EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")));
                            table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                        }

                        var alist = transactionsQuery.Where(entry => entry.Year == year).ToList();
                        foreach (var item in alist)
                        {
                            DataRow tableRow = table.NewRow();
                            tableRow["Location"] = item.LocationName;
                            tableRow["Node"] = item.Cnode;
                            tableRow["PNode"] = item.Pnode;
                            tableRow["LocationID"] = item.LocationID;
                            tableRow["Year"] = item.Year;
                            foreach (var item1 in list)
                            {
                                var columname = item1.Item1;
                                var fYear = item1.Item2;
                                var griddisplayname = item1.Item3;
                                var transactionfilter = transactionsQuery.Where(aa => aa.Year == fYear && aa.LocationID == item.LocationID).FirstOrDefault();

                                if (columname == "C1")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C1;
                                }
                                else if (columname == "C2")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C2;
                                }
                                else if (columname == "C3")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C3;
                                }
                                else if (columname == "C4")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C4;
                                }
                                else if (columname == "C5")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C5;
                                }
                                else if (columname == "C6")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C6;
                                }
                                else if (columname == "C7")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C7;
                                }
                                else if (columname == "C8")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C8;
                                }
                                else if (columname == "C9")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C9;
                                }
                                else if (columname == "C10")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C10;
                                }
                                else if (columname == "C11")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C11;
                                }
                                else if (columname == "C12")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C12;
                                }
                            }
                            table.Rows.Add(tableRow);
                        }

                        System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                        stringbuilder.Append(@"<table id='basic' width='100%' cellpadding='5' cellspacing='5' ><tr><td class='locationheadbg'>Legal Entity / Location </td>");
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            string temp2 = table.Columns[i].ColumnName.ToString();
                            if (temp2 != "Location" && temp2 != "Node" && temp2 != "PNode" && temp2 != "LocationID" && temp2 != "Year")
                            {
                                string fstring = string.Empty;
                                List<string> headerval = table.Columns[i].ToString().Split('/').ToList();
                                if (headerval[0] == "1")
                                {
                                    fstring = "Jan" + " " + headerval[1];
                                }
                                else if (headerval[0] == "2")
                                {
                                    fstring = "Feb" + " " + headerval[1];
                                }
                                else if (headerval[0] == "3")
                                {
                                    fstring = "Mar" + " " + headerval[1];
                                }
                                else if (headerval[0] == "4")
                                {
                                    fstring = "Apr" + " " + headerval[1];
                                }
                                else if (headerval[0] == "5")
                                {
                                    fstring = "May" + " " + headerval[1];
                                }
                                else if (headerval[0] == "6")
                                {
                                    fstring = "Jun" + " " + headerval[1];
                                }
                                else if (headerval[0] == "7")
                                {
                                    fstring = "Jul" + " " + headerval[1];
                                }
                                else if (headerval[0] == "8")
                                {
                                    fstring = "Aug" + " " + headerval[1];
                                }
                                else if (headerval[0] == "9")
                                {
                                    fstring = "Sep" + " " + headerval[1];
                                }
                                else if (headerval[0] == "10")
                                {
                                    fstring = "Oct" + " " + headerval[1];
                                }
                                else if (headerval[0] == "11")
                                {
                                    fstring = "Nov" + " " + headerval[1];
                                }
                                else if (headerval[0] == "12")
                                {
                                    fstring = "Dec" + " " + headerval[1];
                                }
                                stringbuilder.Append("<td class='locationheadbg'>" + fstring + "</td>");
                            }
                        }
                        stringbuilder.Append("</tr>");


                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow dr = table.Rows[i];
                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(dr[1]) + " data-node-pid=" + Convert.ToString(dr[2]) + " >" +
                            " <td class='locationheadLocationbg'>" + Convert.ToString(dr[0]) + "</td>");


                            #region 5
                            List<string> splitvalue = table.Columns[5].ToString().Split('/').ToList();
                            string years = 20 + "" + splitvalue[1];
                            DateTime date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            DateTime date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));
                            if (Convert.ToString(dr[5]) == "4")
                            {
                                //stringbuilder.Append("<td style='background-color: #e6e6e6; border:1px solid #c4c4c4;' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'>" + Convert.ToString(dr[5]) + "</td>");
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 6
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[6].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[6]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 7
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[7].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[7]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion
                            if (period == 12 || period == 6)
                            {
                                #region 8
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[8].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));



                                if (Convert.ToString(dr[8]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 9
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[9].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[9]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 10

                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[10].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[10]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            if (period == 12)
                            {
                                #region 11
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[11].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[11]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 12
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[12].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[12]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 13
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[13].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[13]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 14
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[14].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[14]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 15
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[15].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[15]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 16
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[16].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[16]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            stringbuilder.Append("</tr>");
                        }
                        stringbuilder.Append("</table>");
                        perRahul = stringbuilder.ToString().Trim();

                        g_rading.InnerHtml = perRahul.ToString();
                        //= perRahul.ToString();
                    }//using End
                    #endregion
                }
                else
                {
                    perRahul = string.Empty;
                    #region Internal Grading Report
                    statutoryinternal = "Internal";
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int uid = Convert.ToInt32(AuthenticationHelper.UserID);
                        int period = 12;
                        if (ddlPeriodGrading.SelectedValue != "12")
                        {
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        else
                        {
                            int numberofmonthdifference = 12;
                            if (IsApprover)
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "APPR", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            else
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "MGMT", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            if (numberofmonthdifference <= 3)
                            {
                                ddlPeriodGrading.SelectedValue = "3";
                            }
                            else if (numberofmonthdifference > 3 && numberofmonthdifference < 9)
                            {
                                ddlPeriodGrading.SelectedValue = "6";
                            }
                            else
                            {
                                ddlPeriodGrading.SelectedValue = "12";
                            }
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        var MasterQuery = (from row in entities.GradingPreFillDatas
                                           where row.CustomerID == customerID
                                           && row.UserID == uid && row.IsStatutory == "I"
                                           select row).ToList();
                        if (MasterQuery.Count == 0)
                        {
                            bool sucess = InternalGrading.FillInternalGrading(customerID, uid);
                            if (sucess)
                            {
                                MasterQuery = (from row in entities.GradingPreFillDatas
                                               where row.CustomerID == customerID
                                               && row.UserID == uid && row.IsStatutory == "I"
                                               select row).ToList();
                            }
                        }
                        if (customerBranchID != -1)
                        {
                            entities.Database.CommandTimeout = 180;
                            var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerBranchID, (int)customerID)
                                             select row).ToList();


                            MasterQuery = MasterQuery.Where(entry => branchIDs.Contains((int)entry.LocationID)).ToList();
                        }
                        var transactionsQuery = MasterQuery;
                        DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                        List<Tuple<string, long, string>> list = new List<Tuple<string, long, string>>();

                        DataTable table = new DataTable();
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Node", typeof(long));
                        table.Columns.Add("PNode", typeof(long));
                        table.Columns.Add("LocationID", typeof(long));
                        table.Columns.Add("Year", typeof(long));
                        for (int i = 1; i <= period; i++)
                        {
                            list.Add(new Tuple<string, long, string>("C" + EndDate.AddMonths(-i).Month, Convert.ToInt64(EndDate.AddMonths(-i).ToString("yyyy")), EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")));
                            table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                        }

                        var alist = transactionsQuery.Where(entry => entry.Year == year).ToList();
                        foreach (var item in alist)
                        {
                            DataRow tableRow = table.NewRow();
                            tableRow["Location"] = item.LocationName;
                            tableRow["Node"] = item.Cnode;
                            tableRow["PNode"] = item.Pnode;
                            tableRow["LocationID"] = item.LocationID;
                            tableRow["Year"] = item.Year;
                            foreach (var item1 in list)
                            {
                                var columname = item1.Item1;
                                var fYear = item1.Item2;
                                var griddisplayname = item1.Item3;
                                var transactionfilter = transactionsQuery.Where(aa => aa.Year == fYear && aa.LocationID == item.LocationID).FirstOrDefault();

                                if (columname == "C1")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C1;
                                }
                                else if (columname == "C2")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C2;
                                }
                                else if (columname == "C3")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C3;
                                }
                                else if (columname == "C4")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C4;
                                }
                                else if (columname == "C5")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C5;
                                }
                                else if (columname == "C6")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C6;
                                }
                                else if (columname == "C7")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C7;
                                }
                                else if (columname == "C8")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C8;
                                }
                                else if (columname == "C9")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C9;
                                }
                                else if (columname == "C10")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C10;
                                }
                                else if (columname == "C11")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C11;
                                }
                                else if (columname == "C12")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C12;
                                }
                            }
                            table.Rows.Add(tableRow);
                        }

                        System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                        //stringbuilder.Append(@"<table width='100%' id='basic'><tr><td>Legal Entity / Location </td>");
                        stringbuilder.Append(@"<table id='basic' width='100%' cellpadding='5' cellspacing='5'><tr><td class='locationheadbg'>Legal Entity / Location </td>");
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            string temp2 = table.Columns[i].ColumnName.ToString();
                            if (temp2 != "Location" && temp2 != "Node" && temp2 != "PNode" && temp2 != "LocationID" && temp2 != "Year")
                            {

                                string fstring = string.Empty;
                                List<string> headerval = table.Columns[i].ToString().Split('/').ToList();
                                if (headerval[0] == "1")
                                {
                                    fstring = "Jan" + " " + headerval[1];
                                }
                                else if (headerval[0] == "2")
                                {
                                    fstring = "Feb" + " " + headerval[1];
                                }
                                else if (headerval[0] == "3")
                                {
                                    fstring = "Mar" + " " + headerval[1];
                                }
                                else if (headerval[0] == "4")
                                {
                                    fstring = "Apr" + " " + headerval[1];
                                }
                                else if (headerval[0] == "5")
                                {
                                    fstring = "May" + " " + headerval[1];
                                }
                                else if (headerval[0] == "6")
                                {
                                    fstring = "Jun" + " " + headerval[1];
                                }
                                else if (headerval[0] == "7")
                                {
                                    fstring = "Jul" + " " + headerval[1];
                                }
                                else if (headerval[0] == "8")
                                {
                                    fstring = "Aug" + " " + headerval[1];
                                }
                                else if (headerval[0] == "9")
                                {
                                    fstring = "Sep" + " " + headerval[1];
                                }
                                else if (headerval[0] == "10")
                                {
                                    fstring = "Oct" + " " + headerval[1];
                                }
                                else if (headerval[0] == "11")
                                {
                                    fstring = "Nov" + " " + headerval[1];
                                }
                                else if (headerval[0] == "12")
                                {
                                    fstring = "Dec" + " " + headerval[1];
                                }

                                stringbuilder.Append("<td>" + fstring + "</td>");
                            }
                        }
                        stringbuilder.Append("</tr>");
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow dr = table.Rows[i];
                            //stringbuilder.Append("<tr data-node-id=" + Convert.ToString(dr[1]) + " data-node-pid=" + Convert.ToString(dr[2]) + " >" +
                            //" <td>" + Convert.ToString(dr[0]) + "</td>");
                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(dr[1]) + " data-node-pid=" + Convert.ToString(dr[2]) + " >" +
                           " <td class='locationheadLocationbg'>" + Convert.ToString(dr[0]) + "</td>");



                            #region 5
                            List<string> splitvalue = table.Columns[5].ToString().Split('/').ToList();
                            string years = 20 + "" + splitvalue[1];
                            DateTime date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            DateTime date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));
                            if (Convert.ToString(dr[5]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 6
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[6].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[6]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 7
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[7].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[7]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion
                            if (period == 12 || period == 6)
                            {
                                #region 8
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[8].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[8]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 9
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[9].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[9]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 10

                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[10].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[10]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            if (period == 12)
                            {
                                #region 11
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[11].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[11]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 12
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[12].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[12]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 13
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[13].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[13]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 14
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[14].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[14]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 15
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[15].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[15]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 16
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[16].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[16]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + customerID + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + customerID + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + customerID + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            stringbuilder.Append("</tr>");
                        }
                        stringbuilder.Append("</table>");
                        perRahul = stringbuilder.ToString().Trim();
                    }
                    #endregion
                    g_rading.InnerHtml = perRahul.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void ddlYearGrading_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonth(Convert.ToInt32(ddlYearGrading.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void TreeGraddingReport_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                if (TreeGraddingReport.SelectedNode != null)
                {
                    TbxFilterLocationGridding.Text = TreeGraddingReport.SelectedNode.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upateGradingReport_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterGraddingReport", string.Format("initializeJQueryUI('{0}', 'divFilterLocationGradding');", TbxFilterLocationGridding.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterGraddingReport", "$(\"#divFilterLocationGradding\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilterGraddingReport(List<NameValueHierarchy> bracnhes, List<int> LocationList)
        {
            try
            {
                TreeGraddingReport.Nodes.Clear();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                TreeGraddingReport.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    TreeGraddingReport.Nodes.Add(node);
                }
                TreeGraddingReport.CollapseAll();
                TreeGraddingReport_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                //Top
                DateTime TopStartdate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopStartdate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", string.Format("initializeDatePickerTopStartdate(new Date({0}, {1}, {2}));", TopStartdate.Year, TopStartdate.Month - 1, TopStartdate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", "initializeDatePickerTopStartdate(null);", true);
                }

                DateTime TopEnddate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopEnddate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", string.Format("initializeDatePickerTopEnddate(new Date({0}, {1}, {2}));", TopEnddate.Year, TopEnddate.Month - 1, TopEnddate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", "initializeDatePickerTopEnddate(null);", true);
                }

                //Function

                //DateTime FunctionStartDate = DateTime.MinValue;
                //if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionStartDate))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", string.Format("initializeDatePickerFunctionStartDate(new Date({0}, {1}, {2}));", FunctionStartDate.Year, FunctionStartDate.Month - 1, FunctionStartDate.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionStartDate", "initializeDatePickerFunctionStartDate(null);", true);
                //}

                //DateTime FunctionEndDate = DateTime.MinValue;
                //if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FunctionEndDate))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", string.Format("initializeDatePickerFunctionEndDate(new Date({0}, {1}, {2}));", FunctionEndDate.Year, FunctionEndDate.Month - 1, FunctionEndDate.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerFunctionEndDate", "initializeDatePickerFunctionEndDate(null);", true);
                //}
                

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void upDivLocationPenalty_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPenalty", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPenalty');", tbxFilterLocationPenalty.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void tvFilterLocationPenalty_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPenalty.Text = tvFilterLocationPenalty.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearchPenalty_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                Branchlist.Clear();

                if (tvFilterLocationPenalty.SelectedValue != "-1")
                {
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocationPenalty.SelectedValue));
                    Branchlist.ToList();
                }

                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;                
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;                    
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;                    
                }              
                if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {                                             
                        List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                        try
                        {
                            if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                            {
                                try
                                {
                                    MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("GET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                    if (IsApprover == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }                                
                            }
                            else
                            {
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                                try
                                {
                                    StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                    if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                    {
                                        StackExchangeRedisExtensions.Remove(cashTimeval);
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                    else
                                    {
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("SET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                }
                               
                            }
                        }
                        catch (Exception)
                        {
                            LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                            if (IsApprover == true)
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                            }
                            else
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                            }
                        }
                       
                        if (IsPenaltyVisible == true)
                        {
                            GetManagementCompliancePenalitySummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocationPenalty.SelectedValue), ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApprover);
                        }                        
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenaltyscroll", "fsetscroll();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void rbFinancialYearFunctionSummery_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                if (rbFinancialYearFunctionSummery.SelectedValue.Equals("0"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);                    
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);                      
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (rbFinancialYearFunctionSummery.SelectedValue.Equals("1"))
                {
                    if (DateTime.Today.Month > 3)
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);                      
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-2);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);                    
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (rbFinancialYearFunctionSummery.SelectedValue.Equals("2"))
                {
                    string dtfrom = Convert.ToString("01-01-1900");
                    string dtto = Convert.ToString("01-01-1900");
                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);                  
                    ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }        
        protected void btnCreateICS_Click(object sender, EventArgs e)
        {
            try
            {                
                DataTable statutorydatatable = new DataTable();
                DataTable Internaldatatable = new DataTable();
                DataTable mgmtstatutorydatatable = new DataTable();
                DataTable mgmtInternaldatatable = new DataTable();

                string finalitem = string.Empty;
                string CalendarItem = string.Empty;
                string FileName = "CalendarItem";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int userid = AuthenticationHelper.UserID;

                    var masterbranchlist = (from row in entities.CustomerBranches
                                            where row.CustomerID == customerid
                                            select row).ToList();
                 
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    string cashstatutoryval = string.Empty;
                    string cashinternalval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                        cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                        cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                        cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                        cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                    }
                    #region Statutory                                      
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    try
                    {
                        if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                        {
                            try
                            {
                                MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("GET btnCreateICS_Click Statutory Error exception :" + cashstatutoryval);
                                entities.Database.CommandTimeout = 180;
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            }                            
                        }
                        else
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            try
                            {
                                StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    StackExchangeRedisExtensions.Remove(cashTimeval);
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                                else
                                {
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("SET btnCreateICS_Click Statutory Error exception :" + cashstatutoryval);
                            }
                            
                        }
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                    }
                    

                    //var QueryStatutory = (from row in entities.ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, "All")
                    //                      select row).ToList();

                    var QueryStatutory = MasterManagementCompliancesSummaryQuery.ToList();
                    if (QueryStatutory.Count > 0)
                    {
                        //QueryStatutory = QueryStatutory.Where(entry => entry.ComplianceType != 1).ToList();
                        QueryStatutory = QueryStatutory.Where(entry => entry.ComplianceStatusID == 1).ToList();
                        QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryStatutory.Count > 0)
                    {
                        var Statutory = (from row in QueryStatutory
                                         select new
                                         {
                                             ScheduledOn = row.PerformerScheduledOn,
                                             ComplianceStatusID = row.ComplianceStatusID,
                                             RoleID = row.RoleID,
                                             ScheduledOnID = row.ScheduledOnID,
                                             CustomerBranchID = row.CustomerBranchID,
                                             ShortDescription = row.ShortDescription,
                                         }).Distinct().ToList();


                        statutorydatatable = Statutory.ToDataTable();
                    }
                    #endregion

                    #region Internal                                                        

                 
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    try
                    {
                        if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                        {
                            try
                            {
                                MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                            }
                            catch (Exception)
                            {
                                entities.Database.CommandTimeout = 180;
                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                LogLibrary.WriteErrorLog("GET btnCreateICS_Click Internal Error exception :" + cashinternalval);
                            }
                            
                        }
                        else
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            try
                            {
                                StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    StackExchangeRedisExtensions.Remove(cashTimeval);
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                                else
                                {
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("SET btnCreateICS_Click Internal Error exception :" + cashinternalval);
                            }                           
                        }
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Internal Error exception :"+ cashinternalval);
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                    }
                    

                    //var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, "All")
                    //                     select row).ToList();
                    var QueryInternal = MasterManagementInternalCompliancesSummaryQuery.ToList();
                    if (QueryInternal.Count > 0)
                    {                        
                        QueryInternal = QueryInternal.Where(entry => entry.ComplianceStatusID == 1).ToList();
                        QueryInternal = QueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryInternal.Count > 0)
                    {
                        var Internal = (from row in QueryInternal
                                        select new
                                        {
                                            ScheduledOn = row.ScheduledOn,
                                            ComplianceStatusID = row.ComplianceStatusID,
                                            RoleID = row.RoleID,
                                            ScheduledOnID = row.ScheduledOnID,
                                            CustomerBranchID = row.CustomerBranchID,
                                            ShortDescription = row.ShortDescription,
                                        }).Distinct().ToList();

                        Internaldatatable = Internal.ToDataTable();
                    }
                    #endregion
                    DataTable newtable = new DataTable();
                    // Performer or reviewer
                    if (statutorydatatable.Rows.Count > 0)
                    {
                        newtable.Merge(statutorydatatable);
                    }
                    if (Internaldatatable.Rows.Count > 0)
                    {
                        newtable.Merge(Internaldatatable);
                    }
                    var listEmployee = newtable;
                    if (listEmployee.Rows.Count > 0)
                    {
                        string removableChars = Regex.Escape(@"@&'()<>#,");
                        string pattern = "[" + removableChars + "]";

                        //create a new stringbuilder instance
                        StringBuilder sb = new StringBuilder();
                        //start the calendar item
                        sb.AppendLine("BEGIN:VCALENDAR");
                        sb.AppendLine("VERSION:2.0");
                        sb.AppendLine("PRODID:avantis.co.in");
                        sb.AppendLine("CALSCALE:GREGORIAN");
                        sb.AppendLine("METHOD:PUBLISH");                     
                        foreach (DataRow item in listEmployee.Rows)
                        {
                            var branchname = masterbranchlist.Where(entry => entry.ID == Convert.ToInt32(item["CustomerBranchID"])).Select(en => en.Name).FirstOrDefault();
                            DateTime DateStart = Convert.ToDateTime(item["ScheduledOn"]);
                            DateTime DateEnd = DateStart.AddDays(1);
                            string Summary = Regex.Replace(item["ShortDescription"].ToString(), pattern, ""); //item["ShortDescription"].ToString();// item.Item1;
                            string Location = Regex.Replace(branchname.ToString(), pattern, ""); // branchname.Replace(',', ' ').Trim();//item.Item3;
                            string Description = Regex.Replace(item["ShortDescription"].ToString(), pattern, ""); //item["ShortDescription"].ToString().Replace(',', ' ').Trim();// item.Item1;                                                  

                            string finalsummary = string.Empty;
                            var nsummary = Regex.Replace(Summary, @"\t|\n|\r", "");
                            if (nsummary.Length > 75)
                            {
                                finalsummary = nsummary.Substring(0, 75);
                            }
                            else
                            {
                                finalsummary = nsummary;
                            }
                          
                            //create a new stringbuilder instance
                            StringBuilder sbevent = new StringBuilder();
                            //add the event
                            sbevent.AppendLine("BEGIN:VEVENT");
                            sbevent.AppendLine("DTSTART:" + DateStart.ToString("yyyyMMddTHHmm00"));
                            sbevent.AppendLine("DTEND:" + DateEnd.ToString("yyyyMMddTHHmm00"));
                            sbevent.AppendLine("SUMMARY:" + finalsummary + "");
                            sbevent.AppendLine("LOCATION:" + Location + "");
                            //  sbevent.AppendLine("DESCRIPTION:" + Regex.Replace(Description, @"\t|\n|\r", "")   + "");
                            sbevent.AppendLine("DESCRIPTION:" + finalsummary + "");
                            sbevent.AppendLine("PRIORITY:3");
                            sbevent.AppendLine("END:VEVENT");
                            //create a string from the stringbuilder
                            CalendarItem += sbevent.ToString();
                        }

                        StringBuilder endval = new StringBuilder();
                        endval.AppendLine("END:VCALENDAR");

                        finalitem = sb.ToString() + CalendarItem.ToString() + endval.ToString();


                        //send the calendar item to the browser
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ContentType = "text/calendar";                        
                        Response.AddHeader("content-disposition", "attachment; filename=\"" + FileName + ".ics\"");
                        Response.Write(finalitem);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkShowTLDashboard_Click(object sender, EventArgs e)
        {
            try
            {
                var userRecord = RLCSManagement.GetRLCSUserRecord(Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.ProfileID);

                if (userRecord != null)
                {
                    if (userRecord.AVACOM_UserRole != null)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        ProductMappingStructure _obj = new ProductMappingStructure();

                        //customerID --- User's CustomerID not Selected CustomerID
                        _obj.ReAuthenticate_User(customerID, userRecord.AVACOM_UserRole);
                        Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}