﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ManageEntity : System.Web.UI.Page
    {
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!IsPostBack))
            {
                try
                {
                    BindLocationFilter();
                    if (tvLocation.Nodes.Count > 0)
                    {
                        string IFlag = "";
                        if (tvLocation.SelectedNode.Parent == null)
                        {
                            IFlag = "P";
                        }
                        else
                        {
                            IFlag = "C";
                        }
                        ShowCompanySummary(Convert.ToInt32(tvLocation.Nodes[0].Value), IFlag);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                tvLocation.Nodes.Clear();
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);        
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                string isstatutoryinternal = "";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (ComplianceTypeFlag == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    //BindBranchesHierarchy(node, item);
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvLocation.Nodes.Add(node);
                    tvLocation.Nodes[0].Select();
                }
                tvLocation.CollapseAll();

                //tvLocation.ExpandAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
        protected void tvLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                string IFlag = "";
                if (tvLocation.SelectedNode.Parent == null)
                {
                    IFlag = "P";
                }
                else
                {
                    IFlag = "C";
                }
                ShowCompanySummary(Convert.ToInt32(tvLocation.SelectedNode.Value), IFlag);
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowCompanySummary(int barnchID, string IFlag)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string DeptHead = null;
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                    {
                        DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                    }

                    if (ComplianceTypeFlag == "Statutory")
                    {
                        if (DeptHead == "1")
                        {
                            List<SP_GetEntitySummary_DeptHead_Result> mastertransactionsQuery = new List<SP_GetEntitySummary_DeptHead_Result>();
                            if (IsApprover)
                            {
                                mastertransactionsQuery = (entities.SP_GetEntitySummary_DeptHead(Convert.ToInt32(AuthenticationHelper.UserID),
                                   Convert.ToInt32(AuthenticationHelper.CustomerID), "APPR")).ToList();
                            }
                            else
                            {
                                mastertransactionsQuery = (entities.SP_GetEntitySummary_DeptHead(Convert.ToInt32(AuthenticationHelper.UserID),
                                    Convert.ToInt32(AuthenticationHelper.CustomerID), "DEPT")).ToList();
                            }
                            string reviewerNameslist = "";
                            string ApproverNameslist = "";
                            lblLocation.Text = tvLocation.SelectedNode.Text;
                            if (IFlag == "P")
                            {
                                //lblTotalNoofUsers.Text = Convert.ToString(ConsolidatedManagement.GetCompanyOverview(barnchID, IsApprover, AuthenticationHelper.UserID));
                                if (mastertransactionsQuery.Count > 0)
                                {
                                    var userCount = mastertransactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    //IEnumerable<dynamic> reviwerNames = ConsolidatedManagement.GetReviwersName(barnchID, IsApprover, AuthenticationHelper.UserID);
                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;

                                    //IEnumerable<dynamic> ApproverName = ConsolidatedManagement.GetApproverNameSatutoryNew(Convert.ToInt32(barnchID));
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                            else
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {

                                    //int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    //lblTotalNoofUsers.Text = Convert.ToString(AssignEntityManagement.GetCompanyOverview(customerid, barnchID));

                                    var userCount = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);


                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    //IEnumerable<dynamic> reviwerNames = AssignEntityManagement.GetReviwersName(customerid, barnchID);
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;
                                    //IEnumerable<dynamic> ApproverName = AssignEntityManagement.GetApproverNameSatutory(customerid, Convert.ToInt32(barnchID));
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                        }
                        else
                        {
                            List<SP_GetEntitySummary_Result> mastertransactionsQuery = new List<SP_GetEntitySummary_Result>();
                            if (IsApprover)
                            {
                                mastertransactionsQuery = (entities.SP_GetEntitySummary(Convert.ToInt32(AuthenticationHelper.UserID),
                                   Convert.ToInt32(AuthenticationHelper.CustomerID), "APPR")).ToList();
                            }
                            else
                            {
                                mastertransactionsQuery = (entities.SP_GetEntitySummary(Convert.ToInt32(AuthenticationHelper.UserID),
                                    Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT")).ToList();
                            }
                            string reviewerNameslist = "";
                            string ApproverNameslist = "";
                            lblLocation.Text = tvLocation.SelectedNode.Text;
                            if (IFlag == "P")
                            {
                                //lblTotalNoofUsers.Text = Convert.ToString(ConsolidatedManagement.GetCompanyOverview(barnchID, IsApprover, AuthenticationHelper.UserID));
                                if (mastertransactionsQuery.Count > 0)
                                {
                                    var userCount = mastertransactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    //IEnumerable<dynamic> reviwerNames = ConsolidatedManagement.GetReviwersName(barnchID, IsApprover, AuthenticationHelper.UserID);
                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;

                                    //IEnumerable<dynamic> ApproverName = ConsolidatedManagement.GetApproverNameSatutoryNew(Convert.ToInt32(barnchID));
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                            else
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {

                                    //int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    //lblTotalNoofUsers.Text = Convert.ToString(AssignEntityManagement.GetCompanyOverview(customerid, barnchID));

                                    var userCount = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);


                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    //IEnumerable<dynamic> reviwerNames = AssignEntityManagement.GetReviwersName(customerid, barnchID);
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;
                                    //IEnumerable<dynamic> ApproverName = AssignEntityManagement.GetApproverNameSatutory(customerid, Convert.ToInt32(barnchID));
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                        }
                    }
                    else //Internal
                    {
                        if (DeptHead=="1")
                        {
                            List<SP_GetInternalEntitySummary_DeptHead_Result> mastertransactionsQuery = new List<SP_GetInternalEntitySummary_DeptHead_Result>();
                            if (IsApprover)
                            {
                                mastertransactionsQuery = (entities.SP_GetInternalEntitySummary_DeptHead(Convert.ToInt32(AuthenticationHelper.UserID),
                                   Convert.ToInt32(AuthenticationHelper.CustomerID), "APPR")).ToList();
                            }
                            else
                            {
                                mastertransactionsQuery = (entities.SP_GetInternalEntitySummary_DeptHead(Convert.ToInt32(AuthenticationHelper.UserID),
                                    Convert.ToInt32(AuthenticationHelper.CustomerID), "DEPT")).ToList();
                            }
                            string reviewerNameslist = "";
                            string ApproverNameslist = "";
                            lblLocation.Text = tvLocation.SelectedNode.Text;
                            if (IFlag == "P")
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {
                                    var userCount = mastertransactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;

                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                            else
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {

                                    var userCount = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                        }
                        else
                        {
                            List<SP_GetInternalEntitySummary_Result> mastertransactionsQuery = new List<SP_GetInternalEntitySummary_Result>();
                            if (IsApprover)
                            {
                                mastertransactionsQuery = (entities.SP_GetInternalEntitySummary(Convert.ToInt32(AuthenticationHelper.UserID),
                                   Convert.ToInt32(AuthenticationHelper.CustomerID), "APPR")).ToList();
                            }
                            else
                            {
                                mastertransactionsQuery = (entities.SP_GetInternalEntitySummary(Convert.ToInt32(AuthenticationHelper.UserID),
                                    Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT")).ToList();
                            }
                            string reviewerNameslist = "";
                            string ApproverNameslist = "";
                            lblLocation.Text = tvLocation.SelectedNode.Text;
                            if (IFlag == "P")
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {
                                    var userCount = mastertransactionsQuery.GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;

                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                            else
                            {
                                if (mastertransactionsQuery.Count > 0)
                                {

                                    var userCount = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList().Count;
                                    lblTotalNoofUsers.Text = Convert.ToString(userCount);

                                    IEnumerable<dynamic> reviwerNames = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 4).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    reviwerNames = reviwerNames.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in reviwerNames)
                                    {
                                        reviewerNameslist += rn + "<br />";
                                    }
                                    divReviewerName.InnerHtml = reviewerNameslist;
                                    IEnumerable<dynamic> ApproverName = mastertransactionsQuery.Where(a => a.CustomerbranchID == barnchID && a.RoleID == 6).GroupBy(entry => entry.UserID).Select(g => g.Last()).ToList();
                                    ApproverName = ApproverName.Select(a => a.UName).Distinct().ToList();
                                    foreach (var rn in ApproverName)
                                    {
                                        ApproverNameslist += rn + "<br />";
                                    }
                                    divApproverName.InnerHtml = ApproverNameslist;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
    }
}