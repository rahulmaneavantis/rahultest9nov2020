﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class managementusers : System.Web.UI.Page
    {
        protected  int customerID;
        protected  string ComplianceTypeFlag;
        protected  bool IsApprover = false;
        protected  int BID;
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);                   
                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["branchid"]);                       
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    }

                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(BID));
                    Branchlist.ToList();

                    BindDetailView(customerID, Branchlist);                                     
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public void BindDetailView(int CustomerID, List<long> branchlist)
        {
            try
            {
                string DeptHead = null;
                grdSummaryDetails.DataSource = null;
                grdSummaryDetails.DataBind();
                int CustomerBranchId = -1;
                int CategoryID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                {
                    CategoryID = Convert.ToInt32(Request.QueryString["Category"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["branchid"]);
                }

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<User> UserDetails = new List<User>();

                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    }
                    if (CategoryID != -1)
                    {
                        if (ComplianceTypeFlag == "Statutory")
                        {
                            if (!string.IsNullOrEmpty(DeptHead))
                            {
                                #region Statutory DeptHead
                                var masterquery = (from row in entities.ComplianceAssignedInstancesView_DeptHead
                                                   where row.CustomerID == CustomerID
                                                   select row).ToList();

                                if (Branchlist.Count > 0)
                                {
                                    masterquery = masterquery.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                }
                                if (CategoryID != -1)
                                {
                                    masterquery = masterquery.Where(entry => entry.DepartmentID == CategoryID).ToList();
                                }
                                if (IsApprover == true)
                                {
                                    UserDetails = (from row in masterquery
                                                   join row1 in entities.CustomerBranches
                                                    on row.CustomerBranchID equals row1.ID
                                                   join row2 in entities.Users
                                                    on row.UserID equals row2.ID
                                                   where row.UserID == AuthenticationHelper.UserID &&
                                                   row.RoleID == 6
                                                   && row2.IsDeleted == false && row1.IsDeleted == false
                                                   select row2).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();
                                }
                                else
                                {
                                    UserDetails = (from row in masterquery
                                                   join row1 in entities.EntitiesAssignment_IsDept
                                                   on (long)row.CustomerBranchID equals row1.BranchID
                                                   join row2 in entities.Users
                                                   on row.UserID equals row2.ID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == AuthenticationHelper.UserID
                                                   && row2.IsDeleted == false
                                                   && row1.IsStatutoryInternal == "S"
                                                   && row.DepartmentUserID == AuthenticationHelper.UserID
                                                   select row2).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();
                                }
                                #endregion
                            }
                            else
                            {
                                #region Statutory

                                if (IsApprover == false)
                                {
                                    if (branchlist.Count > 0)
                                    {
                                        UserDetails = (from CI in entities.ComplianceInstances
                                                       join CA in entities.ComplianceAssignments
                                                       on CI.ID equals CA.ComplianceInstanceID
                                                       join C in entities.Compliances
                                                       on CI.ComplianceId equals C.ID
                                                       join A in entities.Acts
                                                       on C.ActID equals A.ID
                                                       join EA in entities.EntitiesAssignments
                                                       on CI.CustomerBranchID equals EA.BranchID
                                                       join U in entities.Users
                                                       on CA.UserID equals U.ID
                                                       where
                                                           A.ComplianceCategoryId == EA.ComplianceCatagoryID
                                                           && U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && CI.IsDeleted == false
                                                           && A.ComplianceCategoryId == CategoryID
                                                           && EA.UserID == AuthenticationHelper.UserID
                                                           && branchlist.Contains((long)CI.CustomerBranchID)
                                                       select U).Distinct().ToList();
                                    }
                                    else
                                    {
                                        UserDetails = (from CI in entities.ComplianceInstances
                                                       join CA in entities.ComplianceAssignments
                                                       on CI.ID equals CA.ComplianceInstanceID
                                                       join C in entities.Compliances
                                                       on CI.ComplianceId equals C.ID
                                                       join A in entities.Acts
                                                       on C.ActID equals A.ID
                                                       join EA in entities.EntitiesAssignments
                                                       on CI.CustomerBranchID equals EA.BranchID
                                                       join U in entities.Users
                                                       on CA.UserID equals U.ID
                                                       where
                                                           A.ComplianceCategoryId == EA.ComplianceCatagoryID
                                                           && U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && CI.IsDeleted == false
                                                           && A.ComplianceCategoryId == CategoryID
                                                           && EA.UserID == AuthenticationHelper.UserID
                                                       select U).Distinct().ToList();
                                    }
                                }
                                else
                                {
                                    if (branchlist.Count > 0)
                                    {
                                        UserDetails = (from CI in entities.ComplianceInstances
                                                       join CA in entities.ComplianceAssignments
                                                       on CI.ID equals CA.ComplianceInstanceID
                                                       join C in entities.Compliances
                                                       on CI.ComplianceId equals C.ID
                                                       join A in entities.Acts
                                                       on C.ActID equals A.ID
                                                       join U in entities.Users
                                                       on CA.UserID equals U.ID
                                                       where
                                                           U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && CI.IsDeleted == false
                                                           && A.ComplianceCategoryId == CategoryID
                                                           && CA.UserID == AuthenticationHelper.UserID
                                                           && branchlist.Contains((long)CI.CustomerBranchID)
                                                           && CA.RoleID == 6
                                                       select U).Distinct().ToList();
                                    }
                                    else
                                    {
                                        UserDetails = (from CI in entities.ComplianceInstances
                                                       join CA in entities.ComplianceAssignments
                                                       on CI.ID equals CA.ComplianceInstanceID
                                                       join C in entities.Compliances
                                                       on CI.ComplianceId equals C.ID
                                                       join A in entities.Acts
                                                       on C.ActID equals A.ID
                                                       join U in entities.Users
                                                       on CA.UserID equals U.ID
                                                       where
                                                           U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && CI.IsDeleted == false
                                                           && A.ComplianceCategoryId == CategoryID
                                                           && CA.UserID == AuthenticationHelper.UserID
                                                           && CA.RoleID == 6
                                                       select U).Distinct().ToList();
                                    }
                                }
                                #endregion
                            }
                        }
                        else if (ComplianceTypeFlag == "Internal")
                        {
                            if (!string.IsNullOrEmpty(DeptHead))
                            {
                                #region Internal DeptHead

                                var masterquery = (from row in entities.InternalComplianceAssignedInstancesView_DeptHead
                                                   where row.CustomerID == CustomerID
                                                   select row).ToList();

                                if (Branchlist.Count > 0)
                                {
                                    masterquery = masterquery.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                }
                                if (CategoryID != -1)
                                {
                                    masterquery = masterquery.Where(entry => entry.DepartmentID == CategoryID).ToList();
                                }
                                if (IsApprover == true)
                                {

                                    UserDetails = (from row in masterquery
                                                   join row1 in entities.CustomerBranches
                                                    on row.CustomerBranchID equals row1.ID
                                                   join row2 in entities.Users
                                                    on row.UserID equals row2.ID
                                                   where row.UserID == AuthenticationHelper.UserID &&
                                                   row.RoleID == 6
                                                   && row2.IsDeleted == false && row1.IsDeleted == false

                                                   select row2).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();
                                }
                                else
                                {


                                    UserDetails = (from row in masterquery
                                                   join row1 in entities.EntitiesAssignment_IsDept
                                                   on (long)row.CustomerBranchID equals row1.BranchID
                                                   join row2 in entities.Users
                                                   on row.UserID equals row2.ID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   && row1.UserID == AuthenticationHelper.UserID
                                                   && row2.IsDeleted == false
                                                   && row1.IsStatutoryInternal == "I"
                                                   && row.DepartmentUserID == AuthenticationHelper.UserID
                                                   select row2).GroupBy(Entry => Entry.ID).Select(a => a.FirstOrDefault()).ToList();

                                }
                                #endregion
                            }
                            else
                            {
                                #region Internal

                                if (IsApprover == false)
                                {
                                    if (branchlist.Count > 0)
                                    {
                                        UserDetails = (from ICI in entities.InternalComplianceInstances
                                                       join ICA in entities.InternalComplianceAssignments
                                                       on ICI.ID equals ICA.InternalComplianceInstanceID
                                                       join IC in entities.InternalCompliances
                                                       on ICI.InternalComplianceID equals IC.ID
                                                       join EAI in entities.EntitiesAssignmentInternals
                                                       on ICI.CustomerBranchID equals EAI.BranchID
                                                       join U in entities.Users
                                                       on ICA.UserID equals U.ID
                                                       where
                                                           IC.IComplianceCategoryID == EAI.ComplianceCatagoryID
                                                           && U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && ICI.IsDeleted == false
                                                           && IC.IComplianceCategoryID == CategoryID
                                                           && EAI.UserID == AuthenticationHelper.UserID
                                                           && branchlist.Contains((long)ICI.CustomerBranchID)
                                                       select U).Distinct().ToList();
                                    }
                                    else
                                    {
                                        UserDetails = (from ICI in entities.InternalComplianceInstances
                                                       join ICA in entities.InternalComplianceAssignments
                                                       on ICI.ID equals ICA.InternalComplianceInstanceID
                                                       join IC in entities.InternalCompliances
                                                       on ICI.InternalComplianceID equals IC.ID
                                                       join EAI in entities.EntitiesAssignmentInternals
                                                       on ICI.CustomerBranchID equals EAI.BranchID
                                                       join U in entities.Users
                                                       on ICA.UserID equals U.ID
                                                       where
                                                           IC.IComplianceCategoryID == EAI.ComplianceCatagoryID
                                                           && U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && ICI.IsDeleted == false
                                                           && IC.IComplianceCategoryID == CategoryID
                                                           && EAI.UserID == AuthenticationHelper.UserID
                                                       select U).Distinct().ToList();
                                    }
                                }
                                else
                                {
                                    if (branchlist.Count > 0)
                                    {
                                        UserDetails = (from ICI in entities.InternalComplianceInstances
                                                       join ICA in entities.InternalComplianceAssignments
                                                       on ICI.ID equals ICA.InternalComplianceInstanceID
                                                       join IC in entities.InternalCompliances
                                                       on ICI.InternalComplianceID equals IC.ID
                                                       //join EAI in entities.EntitiesAssignmentInternals
                                                       //on ICI.CustomerBranchID equals EAI.BranchID
                                                       join U in entities.Users
                                                       on ICA.UserID equals U.ID
                                                       where
                                                            //IC.IComplianceCategoryID == EAI.ComplianceCatagoryID &&
                                                            U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && ICI.IsDeleted == false
                                                           && IC.IComplianceCategoryID == CategoryID
                                                           && ICA.UserID == AuthenticationHelper.UserID
                                                           && ICA.RoleID == 6
                                                           && branchlist.Contains((long)ICI.CustomerBranchID)
                                                       select U).Distinct().ToList();
                                    }
                                    else
                                    {
                                        UserDetails = (from ICI in entities.InternalComplianceInstances
                                                       join ICA in entities.InternalComplianceAssignments
                                                       on ICI.ID equals ICA.InternalComplianceInstanceID
                                                       join IC in entities.InternalCompliances
                                                       on ICI.InternalComplianceID equals IC.ID
                                                       //join EAI in entities.EntitiesAssignmentInternals
                                                       //on ICI.CustomerBranchID equals EAI.BranchID
                                                       join U in entities.Users
                                                       on ICA.UserID equals U.ID
                                                       where
                                                           //IC.IComplianceCategoryID == EAI.ComplianceCatagoryID &&
                                                           U.CustomerID == customerID
                                                           && U.IsDeleted == false
                                                           && ICI.IsDeleted == false
                                                           && IC.IComplianceCategoryID == CategoryID
                                                           && ICA.UserID == AuthenticationHelper.UserID
                                                           && ICA.RoleID == 6
                                                       select U).Distinct().ToList();
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        UserDetails = (from row in entities.Users
                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       select row).ToList();

                    }
                    if (UserDetails != null)
                    {
                        Session["TotalRows"] = UserDetails.Count;
                        grdSummaryDetails.DataSource = UserDetails;
                        grdSummaryDetails.DataBind();
                    }

                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }         
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                //Reload the Grid
                BindDetailView(customerID, Branchlist);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                lTotalCount.Text = GetTotalPagesCount().ToString();
                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == ""|| SelectedPageNo.Text == "0"|| SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";
                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }                    
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;                
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }                
                if(!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue)> Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);
                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;
                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);
                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";
                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);                
                //Reload the Grid
                BindDetailView(customerID, Branchlist);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);
                if (StartRecord < 1)
                    StartRecord = 1;
                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;
                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);
                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";
                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);                
                //Reload the Grid
                BindDetailView(customerID, Branchlist);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected String GetUser(long UserID)
        {
            try
            {
                string Name = String.Empty;
                if (UserID != 0)
                {
                    var UserDetails = UserManagement.GetUserName(Convert.ToInt32(UserID));
                    if (UserDetails != null)
                    {
                        Name = UserDetails;
                    }
                }
                return Name;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected String GetRole(long UserID)
        {
            try
            {
                string Role = String.Empty;
                if (UserID != 0)
                {                    
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var users = (from row in entities.Users
                                     where row.CustomerID == AuthenticationHelper.CustomerID
                                     && row.IsDeleted == false                                     
                                     && row.ID == UserID
                                     select row).FirstOrDefault();

                        if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                        {
                            ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                        }
                        List<int> DistinctRoles = new List<int>();
                        if (ComplianceTypeFlag == "Statutory")
                        {
                            DistinctRoles = (from row in entities.ComplianceAssignments
                                             where row.UserID == UserID
                                             select row).GroupBy(Entry => Entry.RoleID)
                                            .Select(a => a.FirstOrDefault())
                                            .Select(a => a.RoleID).ToList();
                        }
                        else if (ComplianceTypeFlag == "Internal")
                        {
                            DistinctRoles = (from row in entities.InternalComplianceAssignments
                                             where row.UserID == UserID
                                             select row).GroupBy(Entry => Entry.RoleID)
                                            .Select(a => a.FirstOrDefault())
                                            .Select(a => a.RoleID).ToList();
                        }

                        if (users != null)
                        {
                            if (users.RoleID == 2)
                            {
                                if (!String.IsNullOrEmpty(Role))
                                    Role += ", " + "Company Admin";
                                else
                                    Role += "Company Admin";
                            }
                            else if (users.RoleID == 8)
                            {
                                if (!String.IsNullOrEmpty(Role))
                                    Role += ", " + "Management";
                                else
                                    Role += "Management";
                            }
                            else if (users.RoleID == 9)
                            {
                                if (!String.IsNullOrEmpty(Role))
                                    Role += ", " + "Auditor";
                                else
                                    Role += "Auditor";
                            }
                            
                        }
                        if (DistinctRoles.Count > 0)
                        {
                            DistinctRoles.ForEach(EachRole =>
                            {
                                
                                if (EachRole == 3)
                                {
                                    if (!String.IsNullOrEmpty(Role))
                                        Role += ", " + "Performer";
                                    else
                                        Role += "Performer";
                                }
                                else if (EachRole == 4)
                                {
                                    if (!String.IsNullOrEmpty(Role))
                                        Role += ", " + "Reviewer";
                                    else
                                        Role += "Reviewer";
                                }
                                else if (EachRole == 6)
                                {
                                    if (!String.IsNullOrEmpty(Role))
                                        Role += ", " + "Approver";
                                    else
                                        Role += "Approver";
                                }
                            });
                        }                                              
                    }
                }
                return Role;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }     
        protected String GetCustomerBranch(long UserID)
        {
            try
            {
               string Name = String.Empty;
                if (UserID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<string> customerBranchlst = new List<string>();
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                        {
                            ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                        }
                        if (ComplianceTypeFlag == "Statutory")
                        {
                            customerBranchlst = (from CI in entities.ComplianceInstances
                                                 join CA in entities.ComplianceAssignments
                                                 on CI.ID equals CA.ComplianceInstanceID
                                                 join CB in entities.CustomerBranches
                                                 on CI.CustomerBranchID equals CB.ID
                                                 where CB.CustomerID == customerID
                                                 && CA.UserID == UserID
                                                 && CB.IsDeleted == false
                                                 && CI.IsDeleted == false
                                                 select CB.Name).Distinct().ToList();

                        }
                        else if (ComplianceTypeFlag == "Internal")
                        {

                            customerBranchlst = (from ICI in entities.InternalComplianceInstances
                                                 join ICA in entities.InternalComplianceAssignments
                                                 on ICI.ID equals ICA.InternalComplianceInstanceID
                                                 join CB in entities.CustomerBranches
                                                 on ICI.CustomerBranchID equals CB.ID
                                                 where CB.CustomerID == customerID
                                                 && ICA.UserID == UserID
                                                 && CB.IsDeleted == false
                                                 && ICI.IsDeleted == false
                                                 select CB.Name).Distinct().ToList();
                        }
                        if (customerBranchlst.Count>0)
                        {
                            customerBranchlst.ForEach(EachBName =>
                            {
                                if (!String.IsNullOrEmpty(Name))
                                    Name += ", " + EachBName.Trim();
                                else
                                    Name += EachBName.Trim();                                
                            });
                        }                        
                        //return Name;
                    }                  
                }
                return Name;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }      
        protected void upDivLocation_Load(object sender, EventArgs e)
        {            
        }
    }
}