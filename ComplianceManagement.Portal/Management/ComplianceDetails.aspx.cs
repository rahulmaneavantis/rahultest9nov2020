﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class ComplianceDetails : System.Web.UI.Page
    {
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected static int BID;
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["branchid"]);
                        Branchlist.Clear();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        GetAllHierarchy(customerID, Convert.ToInt32(BID));
                        Branchlist.ToList();
                    }
                    BindLocationFilter();
                    //BindCategories();
                    BindActList();
                    BindDetailView();
                    GetPageDisplaySummary();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindActList()
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                int CategoryID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (ddlFunctions.SelectedValue == "-1")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        CategoryID = Convert.ToInt32(Request.QueryString["Category"]);
                        ddlFunctions.SelectedValue = Convert.ToString(CategoryID);
                    }
                }
                else
                {
                    CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                }
                ddlAct.Items.Clear();
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                if (!string.IsNullOrEmpty(DeptHead))
                {
                    ddlAct.DataSource = ActManagement.GetAllAssignedActs(customerid, -1);
                    ddlAct.DataBind();
                }
                else
                {
                    ddlAct.DataSource = ActManagement.GetAllAssignedActs(customerid, CategoryID);
                    ddlAct.DataBind();
                }
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;              
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (ComplianceTypeFlag == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;            
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }    
        private void BindCategories()
        {
            try
            {
                string DeptHead = null;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                ddlFunctions.DataTextField = "Name";
                ddlFunctions.DataValueField = "ID";
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (ComplianceTypeFlag == "Statutory")
                {
                    int CustBranchID = -1;
                    if (tvFilterLocation.SelectedValue != null && tvFilterLocation.SelectedValue != "")
                    {
                        CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(DeptHead))
                    {                        
                        ddlFunctions.DataSource = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "S");
                        ddlFunctions.DataBind();
                    }
                    else
                    {
                        ddlFunctions.DataSource = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, IsApprover);
                        ddlFunctions.DataBind();
                    }
                    
                }
                else
                {                    
                    int CustBranchID = -1;
                    if (tvFilterLocation.SelectedValue != null && tvFilterLocation.SelectedValue != "")
                    {
                        CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(DeptHead))
                    {
                        ddlFunctions.DataSource = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "I");
                        ddlFunctions.DataBind();
                    }
                    else
                    {
                        ddlFunctions.DataSource = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustBranchID, IsApprover);
                        ddlFunctions.DataBind();
                    }
                }                
                ddlFunctions.Items.Insert(0, new ListItem("Function", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }


        private void BindDetailView()
        {
            try
            {
                string DeptHead = null;
                lblAdvanceSearchScrum.Text = String.Empty;
                int RoleID = 3;
                int CustomerBranchId = -1;
                int ActId = -1;
                int CategoryID = -1;
                int UserID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (ddlFunctions.SelectedValue == "-1")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        CategoryID = Convert.ToInt32(Request.QueryString["Category"]);
                        ddlFunctions.SelectedValue = Convert.ToString(CategoryID);
                    }
                }
                else
                {                    
                    CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                }               
                if (ddlAct.SelectedValue == "-1")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                    {
                        ActId = Convert.ToInt32(Request.QueryString["ActID"]);
                        ddlFunctions.SelectedValue = Convert.ToString(ActId);
                    }
                }
                else
                {
                    ActId = Convert.ToInt32(ddlAct.SelectedValue);
                }               
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);                    
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.ChildNodes.Count > 0)
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                if (child.Value == Convert.ToString(CustomerBranchId))
                                {
                                    child.Selected = true;
                                }
                            }
                        }
                        else if (node.Value == Convert.ToString(CustomerBranchId))
                        {
                            node.Selected = true;
                        }
                    }
                }
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                    int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                    CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    if (CustomerBranchlength >= 20)
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }
                if (ddlFunctions.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(DeptHead))
                    {
                        int Categorylength = ddlFunctions.SelectedItem.Text.Length;
                        CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                        else
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                    }
                    else
                    {
                        int Categorylength = ddlFunctions.SelectedItem.Text.Length;
                        CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                        else
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                    }
                }
                if (ddlRole.SelectedValue != "-1")
                {
                    RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {

                        lblAdvanceSearchScrum.Text += "     " + "<b>Role: </b>" + ddlRole.SelectedItem.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Role: </b>" + ddlRole.SelectedItem.Text;
                    }
                }
                if (ddlAct.SelectedValue != "-1")
                {
                    int actlength = ddlAct.SelectedItem.Text.Length;
                    ActId = Convert.ToInt32(ddlAct.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                if (ComplianceTypeFlag == "Statutory")
                {
                    if (!string.IsNullOrEmpty(DeptHead))
                    {
                        DivAct.Visible = true;
                        var detailsview = Business.DepartmentHeadManagement.GetComplianceDetailsDashboard_DeptHead(customerid, Branchlist, RoleID, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, IsApprover);

                        if (UserID != -1)
                            detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                        Session["TotalRows"] = detailsview.Count;

                        GridStatutory.DataSource = detailsview;
                        GridStatutory.DataBind();

                        GridStatutory.Visible = true;
                        GridInternalCompliance.Visible = false;
                    }
                    else
                    {
                        DivAct.Visible = true;
                        var detailsview = Business.ComplianceManagement.GetComplianceDetailsDashboard(customerid, Branchlist, RoleID, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, IsApprover);

                        if (UserID != -1)
                            detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                        Session["TotalRows"] = detailsview.Count;

                        GridStatutory.DataSource = detailsview;
                        GridStatutory.DataBind();

                        GridStatutory.Visible = true;
                        GridInternalCompliance.Visible = false;
                    }
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    if (!string.IsNullOrEmpty(DeptHead))
                    {
                        ddlAct.ClearSelection();
                        DivAct.Visible = false;
                        var detailsview = Business.DepartmentHeadManagement.GetInternalComplianceDetailsDashboard_DeptHead(customerid, Branchlist, RoleID, CustomerBranchId, CategoryID, AuthenticationHelper.UserID, IsApprover);
                        if (UserID != -1)
                            detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                        Session["TotalRows"] = detailsview.Count;
                        GridInternalCompliance.DataSource = detailsview;
                        GridInternalCompliance.DataBind();
                        GridStatutory.Visible = false;
                        GridInternalCompliance.Visible = true;
                    }
                    else
                    {
                        ddlAct.ClearSelection();
                        DivAct.Visible = false;
                        var detailsview = Business.InternalComplianceManagement.GetInternalComplianceDetailsDashboard(customerid, Branchlist, RoleID, CustomerBranchId, CategoryID, AuthenticationHelper.UserID, IsApprover);
                        if (UserID != -1)
                            detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                        Session["TotalRows"] = detailsview.Count;
                        GridInternalCompliance.DataSource = detailsview;
                        GridInternalCompliance.DataBind();
                        GridStatutory.Visible = false;
                        GridInternalCompliance.Visible = true;
                    }
                }
                UpDetailView.Update();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;

                ddlFunctions.ClearSelection();
                ddlRole.ClearSelection();
                ddlAct.ClearSelection();
                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;

                tvFilterLocation_SelectedNodeChanged(sender, e);

               
                
                string satutoryinternal = string.Empty;
                string DeptHead = null;
                int CategoryID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IsDeptHead"])))
                {
                    DeptHead = Convert.ToString(Request.QueryString["IsDeptHead"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    satutoryinternal = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    BID = Convert.ToInt32(Request.QueryString["branchid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                {
                    CategoryID = Convert.ToInt32(Request.QueryString["Category"]);                    
                }                
                if (DeptHead == "1")
                {                    
                    Response.Redirect("~/Management/ComplianceDetails.aspx?customerid=" + customerid + "&branchid=" + BID + "&Internalsatutory=" + satutoryinternal + "&IsApprover=" + IsApprover + "&IsDeptHead=1", false);
                }
                else
                {
                    Response.Redirect("~/Management/ComplianceDetails.aspx?customerid=" + customerid + "&branchid=" + BID + "&Internalsatutory=" + satutoryinternal + "&IsApprover=" + IsApprover + "", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ComplianceTypeFlag == "Statutory")
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else if (ComplianceTypeFlag == "Internal")
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                BindDetailView();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {                
                DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                BindCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

          
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFunctions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindActList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}