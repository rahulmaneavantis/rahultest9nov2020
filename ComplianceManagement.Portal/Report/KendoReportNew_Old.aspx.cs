﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Report
{
    public partial class KendoReportNew_Old : System.Web.UI.Page
    {
        protected static int UID;
        protected static int CustId;
        protected static string CName;
        protected static string DiagraphPath;
        protected static string UserIdDiagraph;
        protected static string PassIdDiagraph;
        protected static string UserRole;
        protected void Page_Load(object sender, EventArgs e)
        {
            UID = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            CName = Convert.ToString(RoleManagement.GetCustNameByID(CustId));
          
            DiagraphPath = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphPath"]);
            UserIdDiagraph = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphUserID"]);  
            PassIdDiagraph = Convert.ToString(ConfigurationManager.AppSettings["KendoGraphPass"]);

            UserRole = Convert.ToString(AuthenticationHelper.Role);


        }
    }
}