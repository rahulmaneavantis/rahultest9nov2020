﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class AuditTool : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected string user_Roles;
        protected List<Int32> roles;
        protected int customerid;
        protected int userid;
        protected long PersonResponsible_RolesARS = 0;
        protected long PersonResponsible_RolesICFR = 0;
        protected string Depthead_Roles;
        protected string AuditHeadOrManager = "";
        protected bool DepartmentHead = false;
        protected string CompanyAdmin = "";
        protected bool prerqusite = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.CustomerID != -1)
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                userid = AuthenticationHelper.UserID;
                Page.Header.DataBind();
                if (Session["LastLoginTime"] != null)
                {
                    LastLoginDate = Session["LastLoginTime"].ToString();
                }
                if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
                {
                    if (AuthenticationHelper.UserID != -1)
                        CustomerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(customerid));
                }

                if (CustomerManagementRisk.GetPreRequsite(userid))
                {
                    prerqusite = true;
                }
                else
                {
                    prerqusite = false;
                }

                if (Session["ChangePassword"] != null)
                {
                    DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                    if (Convert.ToBoolean(Session["ChangePassword"]) != true)
                    {
                        if (AuthenticationHelper.Role.Equals("CADMN"))
                        {
                            CompanyAdmin = "CADMN";
                        }
                        if (AuthenticationHelper.Role.Equals("SADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//ICFR
                        {
                            CMPServicesSiteMap.SiteMapProvider = "AuditAdminProvoider";
                        }
                        else if (AuthenticationHelper.Role.Equals("SADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//Audit Reporting System
                        {
                            CMPServicesSiteMap.SiteMapProvider = "InternalControlAdminProvoider";
                        }
                        else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//ICFR
                        {
                            CMPServicesSiteMap.SiteMapProvider = "AuditAdminProvoider";
                            var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);
                            roles = a;
                        }
                        else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//Audit Reporting System
                        {
                            PersonResponsible_RolesARS = CustomerManagementRisk.GetInternalPersonResponsibleid(AuthenticationHelper.UserID);
                            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                            var a = CustomerManagementRisk.ARSGetAssignedRolesBOTH(AuthenticationHelper.UserID);
                            roles = a;
                            if (AuditHeadOrManager != null)
                            {
                                if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                                {
                                    if (a.Contains(2))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditMgrCompanyAdminProvoider_AM";
                                    }
                                    else if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerReviewerProvoider_AM";
                                    }
                                    else if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerReviewerProvoider_AM";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerformerProvoider_AM";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerReviewerProvoider_AM";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerReviewerProvoider_AM";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerProvider_AM";
                                    }
                                }
                            }
                            else
                            {
                                CMPServicesSiteMap.SiteMapProvider = "InternalControlAdminProvoider";
                            }
                        }
                        else if (DepartmentHead)
                        {
                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadProvoider";
                            //var a = CustomerManagementRisk.ARSGetAssignedRolesBOTH(AuthenticationHelper.UserID);
                            //roles = a;
                        }
                        else if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//ICFR
                        {
                            CMPServicesSiteMap.SiteMapProvider = "AuditManagementProvoider";
                            var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);
                            roles = a;
                        }
                        else if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//Audit Reporting System
                        {
                            CMPServicesSiteMap.SiteMapProvider = "InternalControlManagementProvoider";
                            var a = CustomerManagementRisk.ARSGetAssignedRolesBOTH(AuthenticationHelper.UserID);
                            roles = a;
                        }
                        else
                        {
                            if (AuthenticationHelper.ProductApplicableLogin.Equals("A"))//ICFR                            
                            {
                                PersonResponsible_RolesICFR = CustomerManagementRisk.GetPersonResponsibleid(AuthenticationHelper.UserID);
                                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                                DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                                var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);
                                roles = a;
                                if (AuditHeadOrManager != null)
                                {
                                    if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "ICFRAuditManagerProvoider";
                                    }
                                }
                                else if (PersonResponsible_RolesICFR != 0 && DepartmentHead)  //Function Head and Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespBothProvider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespReviewerProvider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespProvider";
                                    }
                                }
                                else if (PersonResponsible_RolesICFR != 0)  // Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespBothProvider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespReviewerProvider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespProvider";
                                    }
                                }
                                else if (DepartmentHead) //Function Head
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadBothProvider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadReviewerProvider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadProvider";
                                    }
                                }
                                else
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminBothProvider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminBothProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminReviewerProvoider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminPerformerProvoider";
                                    }
                                }
                            }
                            else if (AuthenticationHelper.ProductApplicableLogin.Equals("I"))//Audit Reporting System                            
                            {
                                PersonResponsible_RolesARS = CustomerManagementRisk.GetInternalPersonResponsibleid(AuthenticationHelper.UserID);
                                AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                                DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                                var a = CustomerManagementRisk.ARSGetAssignedRolesBOTH(AuthenticationHelper.UserID);
                                roles = a;
                                if (AuditHeadOrManager != null)
                                {
                                    if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                                    {
                                        if (a.Contains(2))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditMgrCompanyAdminProvoider";
                                        }
                                        else if (a.Contains(3) && a.Contains(4))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerReviewerProvoider";
                                        }
                                        else if (a.Contains(3) && a.Contains(5))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerReviewerProvoider";
                                        }
                                        else if (a.Contains(3))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerformerProvoider";
                                        }
                                        else if (a.Contains(4))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerReviewerProvoider";
                                        }
                                        else if (a.Contains(5))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerReviewerProvoider";
                                        }
                                        else
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerProvider";
                                        }
                                    }
                                }
                                else if (DepartmentHead)//Departmet Head
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadBothProvoider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadBothProvoider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadPerformerProvoider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadReviewerProvoider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadProvoider";
                                    }
                                }
                                else if (PersonResponsible_RolesARS != 0)  // Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleBothProvoider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleBothProvoider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsiblePerformerProvoider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleReviewerProvoider";
                                    }
                                    else if (a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleProvoider";
                                    }
                                }

                                else
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminBothProvider";
                                    }
                                    if (a.Contains(3) && a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminBothProvider";
                                    }
                                    else if (a.Contains(4) || a.Contains(5))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminPerformerProvoider";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        CMPServicesSiteMap.SiteMapProvider = null;
                        CMPMenuBar.Visible = false;
                    }
                }
                user_Roles = AuthenticationHelper.Role;
                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);

                if (LoggedUser.ImagePath != null)
                {
                    ProfilePic.Src = LoggedUser.ImagePath;
                    ProfilePicTop.Src = LoggedUser.ImagePath;
                    ProfilePicSide.Src = LoggedUser.ImagePath;
                }
                else
                {
                    ProfilePic.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicTop.Src = "~/UserPhotos/DefaultImage.png";
                    ProfilePicSide.Src = "~/UserPhotos/DefaultImage.png";
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'DivSearch');", txtSearch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#DivSearch\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            StringBuilder stringBuilder = new StringBuilder();

            StringWriter stringWriter = new StringWriter(stringBuilder);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            base.Render(htmlWriter);
            string html = stringBuilder.ToString();

            //  html = html.Replace("onmouseover=\"Menu_HoverStatic(this)\"\"","onclick = \"Menu_HoverStatic(this)\"\"");
            //  html = html.Replace("new Sys.WebForms.Menu({ element: 'CMPMenuBar', disappearAfter: 500, orientation: 'vertical', tabIndex: 0, disabled: false });", "");
            writer.Write(html);
        }

    }
}