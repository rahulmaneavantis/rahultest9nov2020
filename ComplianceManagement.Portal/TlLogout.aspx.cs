﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class TlLogout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                FormsAuthentication.SignOut();
                Session.Abandon();

                Response.Redirect("https://tlconnect.teamlease.com", false);

            }
            catch (Exception ex)
            {
               // LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}