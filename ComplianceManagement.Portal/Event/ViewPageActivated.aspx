﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPageActivated.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.ViewPageActivated" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>--%>


    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />



    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <%--  <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>--%>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


    <script type="text/javascript">
        //$(document).on("click", "div#ContentPlaceHolder1_ucActiveEventDashboards_upEventInstanceList", function (event) {
        //    if (event.target.id == "") {
        //        var idvid = $(event.target).closest('div');
        //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocationActive') > -1) {
        //            $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").show();
        //        } else {
        //            $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").hide();
        //        }
        //    }
        //    else if (event.target.id != "ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive") {
        //        $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").hide();
        //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationActive') > -1) {
        //        $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").show();
        //    } else if (event.target.id == "ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive") {
        //        $("#ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive").unbind('click');

        //        $("#ContentPlaceHolder1_ucActiveEventDashboards_tbxFilterLocationActive").click(function () {
        //            $("#ContentPlaceHolder1_ucActiveEventDashboards_divFilterLocationActive").toggle("blind", null, 500, function () { });
        //        });
        //    }
        //});

        function Confirm() {
            var a = document.getElementById("BodyContent_tbxStartDate").value;
            if (a != "") {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                confirm_value.value = "";
                confirm_value
                if (confirm("Start date is " + Date + ", to continue saving click OK!!")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert("Start date should not be blank..!");
            }
        };
        function initializeDatePicker(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function postbackOnCheck() {
            var o = window.event.srcElement;
            if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
            { __doPostBack("", ""); }
        }

        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
        function divexpandcollapseChild(divname) {
            var div1 = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div1.style.display == "none") {
                div1.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div1.style.display = "none";
                img.src = "../Images/plus.gif";;
            }
        }

        function ClosemodalPopup() {
            $('#modalPopup').modal('hide');

            return true;
        }
        function ClosemodaelShortNotice() {
            $('#modalShortNotice').modal('hide');

            return true;
        }
        function openmodalPopup() {
            if (Displays() == true) {
                $('#modalPopup').modal('show');
            }
            return true;
        }
        function openmodaelShortNotice() {
            $('#modalShortNotice').modal('show');
            return true;
        }

    </script>

    <style>
        .hiddencol {
            display: none;
        }

        .circle {
            background-color: green;
            width: 29px;
            height: 30px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 5px;
        }

        .ui-widget-header {
            border-bottom: 2px solid #dddddd;
            background: #999 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
        }

            .ui-widget-header > th {
                color: #fff;
            }

        .ui-widget-header {
            border: none;
        }

        .Inforamative {
            color: Chocolate !important;
        }

        tr.Inforamative > td {
            color: Chocolate !important;
        }

        .Actionable {
            color: SlateBlue !important;
        }

        tr.Actionable > td {
            color: SlateBlue !important;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }


        .btnview {
            background-image: url(../Images/view-icon-new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .modal-header {
            padding: 15px;
            border-bottom: 0px solid #e5e5e5;
            min-height: 16.43px;
        }
    </style>
    <script type="text/javascript">
        function DeleteItem() {
            if (confirm("Are you sure you want to delete ...?")) {
                return true;
            }
            return false;
        }
    </script>
</head>
<body style="background: none !important;">
   
    <form id="form1" runat="server">
         
        <div>
            
            <%--        <div class="modal fade" id="modalPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
            <%--<div class="modal-dialog" style="width: 922px">--%>
            <%--  <div class="modal-content">--%>
            <%-- <div class="modal-header" style="width: 900px">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>--%>
            <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>
                <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

            <div class="modal-body" style="width: 900px">
                <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceValidationGroup" Display="None" />
                            <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Green"></asp:Label>
                        </div>
                        <div style="margin: 5px">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="margin-bottom: 2px; margin-top: -45px" runat="server" id="divSubEventmode">
                                            <div style="z-index: 10" id="divSubevent">
                                                <asp:gridview id="gvParentGrid" runat="server" onrowupdating="gvParentGrid_RowUpdating" autogeneratecolumns="false"
                                                    showfooter="true" width="790px" borderwidth="0px" gridlines="None" class="tablenew" datakeynames="Type,Date"
                                                    onrowdatabound="gvParentGrid_OnRowDataBound" xmlns:asp="#unknown">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                    src="../Images/plus.gif" alt="" /></a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name"  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true"/>
                                                    <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                         <ItemTemplate>
                                                          <asp:TextBox ID="txtgvParentGridDate" CssClass="StartDate" style="border-radius: 9px;border-color: #c7c7cc; text-align :center" BackColor ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy") %>' Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                       <ItemTemplate>
                                                            <asp:Button ID="BtnAddgvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleParentEventAddButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Add" />
                                                            <asp:Button ID="BtnUpdategvParentGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleParentEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                        <asp:GridView ID="gvParentToComplianceGrid" borderwidth="0px"  gridlines="None" style="color: #666666;font-size:14px;" runat="server" Width="98%" DataKeyNames="EventType,SequenceID" onrowdatabound="gvParentToComplianceGrid_OnRowDataBound"
                                                                            AutoGenerateColumns="false" CellSpacing="5" >
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblParentStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="100%">
                                                                    <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                        <asp:GridView ID="gvChildGrid" runat="server" Width="98%" borderwidth="0px" gridlines="None" style="color: #666666;font-size:14px;"
                                                                            AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                            OnRowDataBound="gvChildGrid_OnRowDataBound" OnRowUpdating="gvChildGrid_RowUpdating" OnRowCommand="gvChildGrid_RowCommand" xmlns:asp="#unknown">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                    <ItemTemplate>
                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                alt="" /></a>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub event name" HeaderStyle-HorizontalAlign="Left"/>
                                                                                <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtgvChildGridDate" CssClass="StartDate" style=" text-align :center; border-radius: 9px;border-color: #c7c7cc;" Visible='<%# visibleTxtgvChildGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Request.QueryString["EventScheduledOnId"])) %>' BackColor ='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'  Enabled ='<%# enableSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  Width="100px" runat="server"></asp:TextBox>
                                                                                     </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                 <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="260px" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="BtnAddgvChildGrid" runat="server" style="margin-left: 6px;" class="btn btn-search" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" class="btn btn-search" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnID"])) %>' CommandName="Update" />
                                                                                        <asp:Button ID="btnNASubEvent" runat="server" style="margin-left: 7px;" class="btn btn-search" Text="NA" ValidationGroup="ComplianceValidationGroup" CommandName="NotApplicable" OnClientClick="return confirm('‘Not Applicable’?');"  CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Request.QueryString["EventScheduledOnId"]) %>' Visible='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>'  />
                                                                                        <asp:LinkButton ID="btnAddIComplianceChildGrid" runat="server" style="margin-left: -9px;" ToolTip="Add internal compliance to event" data-toggle="tooltip" data-placement="bottom"  ValidationGroup="ComplianceValidationGroup" CommandName="InternalCompliance" CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) + ","+ Convert.ToDateTime(Eval("Date"))%>' Visible='<%# visibleInternalComplianceAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' >

                                                                                       <%--  <asp:Button ID="BtnAddgvChildGrid" runat="server" style="margin-left: 6px;" class="btn btn-search" Text="Save" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventAddButton(ParentID,Convert.ToInt32(Eval("SubEventID")),EventScheduledOnID) %>' CommandName="Update" />
                                                                                        <asp:Button ID="BtnUpdategvChildGrid" runat="server" class="btn btn-search" Text="Update" ValidationGroup="ComplianceValidationGroup" Visible='<%# visibleSubEventUpdateButton(ParentID,Convert.ToInt32(Eval("SubEventID")),EventScheduledOnID) %>' CommandName="Update" />
                                                                                        <asp:Button ID="btnNASubEvent" runat="server" style="margin-left: 7px;" class="btn btn-search" Text="NA" ValidationGroup="ComplianceValidationGroup" CommandName="NotApplicable" OnClientClick="return confirm('Are you sure you want to not applicable event step?');"  CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + EventScheduledOnID %>' Visible='<%# CheckSubEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("SubEventID")), EventScheduledOnID) %>'  />
                                                                                        <asp:LinkButton ID="btnAddIComplianceChildGrid" runat="server" style="margin-left: -9px;" ToolTip="Add internal compliance to event" data-toggle="tooltip" data-placement="bottom"  ValidationGroup="ComplianceValidationGroup" CommandName="InternalCompliance" CommandArgument='<%# Eval("ParentEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Request.QueryString["EventScheduledOnId"]) + ","+ Convert.ToDateTime(Eval("Date"))%>' Visible='<%# visibleInternalComplianceAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), EventScheduledOnID) %>' >--%>
                                                                                             <img src='<%# ResolveUrl("~/Images/Add new icon.png")%>' alt="View"/>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                     <%--Intermdaite Event--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td colspan="100%">
                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                    <asp:GridView ID="gvIntermediateSubEventGrid" borderwidth="0px" gridlines="None" class="tablenew"  OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound"
                                                                                                        OnRowUpdating="gvIntermediateSubEventGrid_RowUpdating" OnRowCommand="gvIntermediateSubEventGrid_RowCommand" runat="server" Width="98%" DataKeyNames="IntermediateEventID,ParentEventID"
                                                                                                        AutoGenerateColumns="false">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                <ItemTemplate>
                                                                                                                    <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                        <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif" alt="" /></a>
                                                                                                                </ItemTemplate>
                                                                                                                <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate> 
                                                                                                                    <asp:TextBox ID="txtgvIntermediateSubEventGridDate" style="border-radius: 9px; border-color: #c7c7cc; text-align:center;"  Visible='<%# visibleTxtgvIntermediateSubEventGridDate(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' BackColor ='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) == true ? System.Drawing.Color.White:System.Drawing.Color.LightGreen %>'   Text='<%# Eval("Date") != null? Convert.ToDateTime(Eval("Date")).ToString("dd-MM-yyyy"):"" %>'  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="240px" ItemStyle-HorizontalAlign="Center">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Button ID="BtnAddIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Save" Visible='<%# visibleIntermediateSubEventAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="BtnUpdateIntermediateSubEventGrid" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Update" Visible='<%# visibleIntermediateSubEventUpdateButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' CommandName="Update" />
                                                                                                                    <asp:Button ID="btnNAIntermediateSubEvent" runat="server" style="margin-left: 7px;" class="btn btn-search" Text="NA" ValidationGroup="ComplianceValidationGroup" CommandName="IntermediateNotApplicable" OnClientClick="return confirm('Are you sure you want to not applicable event step?');"  CommandArgument='<%# Eval("ParentEventID") + "," + Eval("IntermediateEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) %>' Visible='<%# CheckIntermediateEventTransactionComplete(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>'  />
                                                                                                                    <asp:LinkButton ID="btnAddIComplianceIntermediateSubEventGrid" runat="server" style="margin-left: -9px;" ToolTip="Add internal compliance to event" data-toggle="tooltip" data-placement="bottom"  ValidationGroup="ComplianceValidationGroup" CommandName="IntermediateInternalCompliance" CommandArgument='<%# Eval("ParentEventID") + "," + Eval("IntermediateEventID") + "," + Eval("SubEventID") + "," + Convert.ToInt32(Session["EventScheduledOnId"]) +","+ Convert.ToDateTime(Eval("Date"))%>' Visible='<%# visibleIntermediateEventInternalComplianceAddButton(Convert.ToInt32(Eval("ParentEventID")),Convert.ToInt32(Eval("IntermediateEventID")),Convert.ToInt32(Eval("SubEventID")), Convert.ToInt32(Session["EventScheduledOnId"])) %>' >
                                                                                                                         <img src='<%# ResolveUrl("~/Images/Add new icon.png")%>' alt="View"/>
                                                                                                                    </asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <tr>
                                                                                                                        <td colspan="100%">
                                                                                                                            <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                <asp:GridView ID="gvIntermediateComplainceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvIntermediateComplainceGrid_OnRowDataBound"
                                                                                                                                    AutoGenerateColumns="false" OnRowUpdating="gvIntermediateComplainceGrid_RowUpdating">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                                                        <asp:BoundField ItemStyle-Width="700px" DataField="Name" HeaderText="Compliance Name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                                                        <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        <asp:BoundField DataField="IsIntermediateInternal" HeaderText="IsIntermediateInternal" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                                                                                                        <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                                                                                        <ItemTemplate>
                                                                                                                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                                                                                            <ContentTemplate>
                                                                                                                                               <%-- Visible='<%# visibleClosure(Convert.ToInt32(Eval("ComplianceID"))) %>'--%>
                                                                                                                                             <asp:Button ID="btngvIntermediateComplainceGridClosure" class="btn btn-search" ValidationGroup="ComplianceValidationGroup" runat="server" Text="Closure" CommandName="Update" />
                                                                                                                                          </ContentTemplate>
                                                                                                                                          </asp:UpdatePanel>
                                                                                                                                           <asp:Label ID="lblIntermediateStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateParentEventID" runat="server" Visible="false" Text='<%# Eval("ParentEventID") %>'></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>
                                                                                                                                           <asp:Label ID="lblgvIIntermediateSubEventID" runat="server" Visible="false" Text='<%# Eval("SubEventID") %>'></asp:Label>
                                                                                                                                         </ItemTemplate>
                                                                                                                                       </asp:TemplateField>
                                                                                                                                    </Columns>
                                                                                                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                </asp:GridView>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                    </asp:GridView>
                                                                                                     <%--Complaince--%>
                                                                                                      <asp:GridView ID="gvComplianceGrid" borderwidth="0px" gridlines="None" class="tablenew" runat="server" Width="98%" OnRowDataBound="gvComplianceGrid_OnRowDataBound"
                                                                                                        AutoGenerateColumns="false" DataKeyNames="SequenceID" OnRowUpdating="gvComplianceGrid_RowUpdating">
                                                                                                        <Columns>
                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="Id" HeaderStyle-HorizontalAlign="Left"/>
                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance name" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" />
                                                                                                            <asp:BoundField DataField="IsInternal" HeaderText="IsInternal" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                                                                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="" ItemStyle-HorizontalAlign="Center" >
                                                                                                            <ItemTemplate>
                                                                                                              <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                                                                              <ContentTemplate>
                                                                                                                 <%-- Visible='<%# visibleClosure(Convert.ToInt32(Eval("ComplianceID"))) %>'--%>
                                                                                                              <asp:Button ID="btngvComplianceGridClosure" class="btn btn-search" ValidationGroup="ComplianceValidationGroup"  runat="server" Text="Closure" CommandName="Update" />
                                                                                                              </ContentTemplate>
                                                                                                             </asp:UpdatePanel>
                                                                                                             <asp:Label ID="lblStatus" runat="server" class="circle" Text="" Width="10px" Height="10px"></asp:Label>
                                                                                                             <asp:Label ID="lblgvSParentEventID" runat="server" Visible="false" Text='<%# Eval("ParentEventID") %>'></asp:Label>
                                                                                                             <asp:Label ID="lblgvSIntermediateEventID" runat="server" Visible="false" Text='<%# Eval("IntermediateEventID") %>'></asp:Label>
                                                                                                             <asp:Label ID="lblgvSSubeventID" runat="server" Visible="false" Text='<%# Eval("SubEventID") %>'></asp:Label>
                                                                                                            </ItemTemplate>
                                                                                                           </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                               </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="ui-widget-header" ></HeaderStyle>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:gridview>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="clearfix"></div>
                                <tr>
                                    <td style="width: 400px;">
                                        <div style="margin-bottom: 7px;" runat="server" id="div1111">
                                            <label style="width: 390px; display: block; float: left; font-size: 16px; color: #333;">
                                                Nature of event
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 400px;">
                                        <asp:TextBox ID="txtDescription" class="form-control" Rows="3" Width="785px" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <table width="100%" id="eventnaturelegend" style="margin-top: 10px">
                                            <tr>
                                                <td>
                                                    <label style="width: 50px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Note :
                                                    </label>
                                                </td>
                                                <td style="font-size: 10px">
                                                    <div class="circle" style="float: left"></div>
                                                    <div style="float: left; color: #666666; padding: 9px; font-family: 'Roboto',sans-serif;">
                                                        Completed / Closed compliance
                                                    </div>
                                                </td>
                                                <td style="font-size: 10px; color: #666666;">
                                                    <div style="height: 30px; width: 81px; border-radius: 14px; background-color: lightgreen; float: left; margin-right: 5px; padding: 9px;">DD-MM-YYYY </div>
                                                    <div style="float: left; padding: 9px;">
                                                        Completed steps
                                                    </div>
                                                </td>
                                                <td style="font-size: 10px">
                                                    <div style="height: 30px; width: 30px; background-color: Chocolate; float: left; margin-right: 5px"></div>
                                                    <div style="float: left; color: #666666; padding: 9px;">
                                                        Informative checklist no action required.
                                                    </div>
                                                </td>
                                                <td style="font-size: 10px">
                                                    <div style="height: 30px; width: 30px; background-color: SlateBlue; float: left; margin-right: 5px"></div>
                                                    <div style="float: left; color: #666666; padding: 9px;">
                                                        Actionable checklist.
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:HiddenField ID="hdnTitle" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <%-- </div>--%>

            <div class="modal fade" id="modalShortNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 450px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 450px;">
                            <asp:UpdatePanel ID="upshortnotice" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="margin-bottom: 4px; margin-left: 95px">
                                        <asp:Label runat="server" Text="Do you want to conduct meeting by short notice?" ID="Label1" Style="color: #999; margin-left: -72px"></asp:Label>
                                    </div>

                                    <div style="margin-bottom: 4px; margin-left: 95px">
                                        <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div>
                                        <div style="margin: 5px; margin-left: 165px;">
                                            <asp:RadioButtonList ID="rbtnShortnotice" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                OnSelectedIndexChanged="rbtnShortnotice_SelectedIndexChanged">
                                                <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                                <asp:ListItem Text="No" Value="0" />
                                            </asp:RadioButtonList>
                                        </div>        
                                        <div style="margin-top: 25px; margin-left: 70px;" id="divDays" runat="server">
                                            <asp:Label ID="lbl" Text="Short Notice Days" Style="color: #999;" Width="125px" runat="server"></asp:Label>
                                            <%-- <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" Text ="(-)" Width="16px" runat="server"></asp:Label>--%>
                                            <asp:TextBox ID="txtShortNoticeDays" Width="80px" runat="server" MaxLength="2"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                TargetControlID="txtShortNoticeDays" />
                                        </div>
                                        <div style="margin-top: 50px; margin-left: 125px">
                                            <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="ComplianceValidationGroup" ID="btnSaveShortNotice" OnClick="btnSaveShortNotice_Click" />
                                            <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnShortClose" />
                                            <%--OnClick="btnShortClose_Click"--%>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalClosure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 680px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 680px;">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div>
                                        <asp:ValidationSummary runat="server" CssClass="vdsummary" ForeColor="Red"
                                            ValidationGroup="ComplianceValidationGroup1" />
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                            ValidationGroup="ComplianceValidationGroup1" ForeColor="Red" Display="None" />
                                        <asp:Label runat="server" ID="lblImsg1" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div style="margin-bottom: 4px; margin-left: 95px">
                                        <asp:Label runat="server" Text="Do you want to stop compliance schedule?" ID="Label2" Style="color: #999; margin-left: -72px"></asp:Label>
                                    </div>
                                    <div style="margin-bottom: 4px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:Label ID="Label6" Text="Schedule From Date" Style="color: #999;" Width="135px" runat="server"></asp:Label>
                                        <asp:DropDownList runat="server" ID="ddlScheduleDates" Style="width: 190px;" AutoPostBack="true"
                                            DataTextFormatString="{0:dd/MMM/yyyy}">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select schedule from date."
                                            ControlToValidate="ddlScheduleDates" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 4px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <asp:Label ID="Label5" Text="Remark" Style="color: #999;" Width="135px" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtClosureRemark" TextMode="MultiLine" Width="420px" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Remark can not be empty."
                                            ControlToValidate="txtClosureRemark" runat="server" ValidationGroup="ComplianceValidationGroup1"
                                            Display="None" />
                                    </div>
                                    <div style="margin-top: 50px; margin-left: 125px">
                                        <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="ComplianceValidationGroup1" ID="btnClosure" OnClick="btnClosure_Click" />
                                        <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnCloseClosure" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalInternalCompliance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 700px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 620px;">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="margin-bottom: 4px; margin-left: 95px">
                                        <asp:Label runat="server" ID="lblImsg" ForeColor="Red"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="False"
                                            ValidationGroup="InternalComplianceValidationGroup" Display="None" />
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="InternalComplianceValidationGroup" />
                                    </div>
                                    <div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                Internal Compliance
                                            </label>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList runat="server" ID="ddlInternalCompliance" CssClass="form-control" Style="padding: 0px; margin: 0px; width: 400px;" AutoPostBack="true" />
                                                    <asp:CompareValidator ErrorMessage="Please select internal Compliance." ControlToValidate="ddlInternalCompliance" ID="vaInternalCompliance"
                                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="InternalComplianceValidationGroup"
                                                        Display="None" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                Days
                                            </label>
                                            <asp:TextBox ID="txtDays" Width="80px" runat="server" MaxLength="2"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                                TargetControlID="txtDays" />
                                            <asp:RequiredFieldValidator ID="rfvDays" ErrorMessage="Please enter Days."
                                                ControlToValidate="txtDays" runat="server" ValidationGroup="InternalComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                                OneTime/Recurring
                                            </label>
                                            <asp:RadioButtonList ID="rbtInternalOccurance" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem Text="Recurring" Value="0" />
                                                <asp:ListItem Text="OneTime" Value="1" Selected="True" />
                                            </asp:RadioButtonList>
                                        </div>
                                        <div style="margin-top: 50px; margin-left: 215px">
                                            <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="InternalComplianceValidationGroup" ID="btnInternalComplianceAdd" OnClick="btnInternalComplianceAdd_Click" />
                                            <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnInternal" />
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
