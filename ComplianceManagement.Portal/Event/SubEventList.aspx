﻿<%@ Page Title="Sub Events" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="SubEventList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.SubEventList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upSubEventList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="center">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddSubEvent" OnClick="btnAddSubEvent_Click" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                            RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                    runat="server" Style="text-decoration: none; color: Black" />
                            </ItemTemplate>
                            <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                            <SeparatorStyle Font-Size="12" />
                            <SeparatorTemplate>
                                &gt;
                            </SeparatorTemplate>
                        </asp:DataList>
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdSubEventList" AutoGenerateColumns="false" GridLines="Vertical"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdSubEventList_RowCreated"
                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdSubEventList_Sorting"
                Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdSubEventList_RowCommand" OnRowDataBound="grdSubEventList_RowDataBound"
                OnPageIndexChanging="grdSubEventList_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="SubEventName" HeaderText="Sub Event Name" SortExpression="SubEventName" />
                    <asp:BoundField DataField="Name" HeaderText="Event Name" SortExpression="Name" />
                    <%-- <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Center" SortExpression="CreatedOn" >
                        <ItemTemplate>
                            <%# ((DateTime)Eval("CreatedOn")).ToString("dd-MMM-yy HH:mm")%>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Entity">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtViewChields" runat="server" CommandName="VIEW_CHILDREN" CommandArgument='<%# Eval("ID") %>'>sub-events</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_SUBEVENT" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Sub-entity" title="Edit Sub-entity" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_SUBEVENT" CommandArgument='<%# Eval("ID") %>'
                                OnClientClick="return confirm('This will also delete all the sub-events associated with current sub event. Are you certain you want to delete this sub event?');"><img src="../Images/delete_icon.png" alt="Disable Sub-entity" title="Disable Sub-entity" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divSubEventDialog">
        <asp:UpdatePanel ID="upSubEvents" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary"
                            ValidationGroup="SubEventValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="SubEventValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:Literal ID="litCustomer" runat="server" />
                    </div>
                    <div id="divParent" runat="server" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Parent</label>
                        <asp:Literal ID="litParent" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 390px;" MaxLength="500" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="SubEventValidationGroup" Display="None" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="SubEventValidationGroup"
                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&')(:;,_ .-]*$"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 260px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="SubEventValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divSubEventDialog').dialog('close');" />
                    </div>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divSubEventDialog').dialog({
                height: 400,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Sub Event Information",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });
    </script>
</asp:Content>
