﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Objects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventParentChildRelationship : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Multiview1.ActiveViewIndex = 0;
                BindEvents(1);
                txtParentEvent.Attributes.Add("readonly", "readonly");
            }
        }

        private void BindParentEvent(int CompanyType)
        {
            try
            {
                rptParentEvent.DataSource = CustomerBranchManagement.GetEventExceptOne(Convert.ToInt32(ddlEvents.SelectedValue),CompanyType);
                rptParentEvent.DataBind();

                foreach (RepeaterItem aItem in rptParentEvent.Items)
                {
                    CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");

                    if (!chkParentEvent.Checked)
                    {
                        chkParentEvent.Checked = true;
                    }
                }
                CheckBox ParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                ParentEventSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCompliance()
        {
            try
            {
                rptCompliance.DataSource = CustomerBranchManagement.GetCompliances();
                rptCompliance.DataBind();

                foreach (RepeaterItem aItem in rptCompliance.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");

                    if (!chkCompliance.Checked)
                    {
                        chkCompliance.Checked = true;
                    }
                }
                CheckBox ComplianceSelectAll = (CheckBox)rptCompliance.Controls[0].Controls[0].FindControl("complianceSelectAll");
                ComplianceSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindParentAllEvent()
        {
            try
            {
                rptParentEvent.DataSource = CustomerBranchManagement.GetEvent(0);
                rptParentEvent.DataBind();

                foreach (RepeaterItem aItem in rptParentEvent.Items)
                {
                    CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");

                    if (!chkParentEvent.Checked)
                    {
                        chkParentEvent.Checked = true;
                    }
                }
                CheckBox ParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                ParentEventSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSubEvent(int CompanyType)
        {
            try
            {
                rptParentEvent.DataSource = CustomerBranchManagement.GetEventExceptOne(Convert.ToInt32(ddlEvents.SelectedValue), CompanyType);
                rptParentEvent.DataBind();

                foreach (RepeaterItem aItem in rptParentEvent.Items)
                {
                    CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");

                    if (!chkParentEvent.Checked)
                    {
                        chkParentEvent.Checked = true;
                    }
                }
                CheckBox ParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                ParentEventSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindSubAllEvent()
        {
            try
            {
                rptParentEvent.DataSource = CustomerBranchManagement.GetEvent(1);
                rptParentEvent.DataBind();

                foreach (RepeaterItem aItem in rptParentEvent.Items)
                {
                    CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");

                    if (!chkParentEvent.Checked)
                    {
                        chkParentEvent.Checked = true;
                    }
                }
                CheckBox ParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                ParentEventSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEvents(int CompanyType)
        {
            try
            {
                ddlEvents.DataTextField = "Name";
                ddlEvents.DataValueField = "ID";

                ddlEvents.DataSource = EventManagement.GetEvent(CompanyType);
                ddlEvents.DataBind();
                ddlEvents.Items.Insert(0, new ListItem("< Select Event >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEventCompliance(int CompanyType)
        {
            try
            {
                ddlEventCompliance.DataTextField = "Name";
                ddlEventCompliance.DataValueField = "ID";

                ddlEventCompliance.DataSource = EventManagement.GetEvent(CompanyType);
                ddlEventCompliance.DataBind();
                ddlEventCompliance.Items.Insert(0, new ListItem("< Select Event>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubEvents(int CompanyType)
        {
            try
            {
                ddlEvents.DataTextField = "Name";
                ddlEvents.DataValueField = "ID";

                ddlEvents.DataSource = EventManagement.GetEvent(CompanyType);
                ddlEvents.DataBind();
                ddlEvents.Items.Insert(0, new ListItem("< Select Event>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> vGetParentEventMappedIDs = new List<int>();
            bool Intermediateflag = Business.ComplianceManagement.CheckIntermediateEvent(Convert.ToInt32(ddlEvents.SelectedValue));  
            if(Intermediateflag == true)
            {
                chkIntermediate.Checked = true;
                if (rblRelationship.SelectedValue.Equals("0"))
                {
                    BindParentEvent(1);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetIntermediateEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 1);
                }
                else if (rblRelationship.SelectedValue.Equals("1"))
                {
                    BindParentEvent(2);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetIntermediateEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 2);
                }
                else if (rblRelationship.SelectedValue.Equals("2"))
                {
                    BindParentEvent(3);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetIntermediateEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 3);
                }
                else if (rblRelationship.SelectedValue.Equals("3"))
                {
                    BindParentEvent(4);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetIntermediateEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 4);
                }
            }
            else
            {
                chkIntermediate.Checked = false;
                if (rblRelationship.SelectedValue.Equals("0"))
                {
                    BindParentEvent(1);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetEventSubEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 1);
                }
                else if (rblRelationship.SelectedValue.Equals("1"))
                {
                    BindParentEvent(2);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetEventSubEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 2);
                }
                else if (rblRelationship.SelectedValue.Equals("2"))
                {
                    BindParentEvent(3);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetEventSubEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 3);
                }
                else if (rblRelationship.SelectedValue.Equals("3"))
                {
                    BindParentEvent(4);
                    vGetParentEventMappedIDs = Business.ComplianceManagement.GetEventSubEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), "PP", 4);
                }
            }
   
            txtParentEvent.Text = "< Select >";
            foreach (RepeaterItem aItem in rptParentEvent.Items)
            {
                CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");
                chkParentEvent.Checked = false;
                CheckBox chkParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                chkParentEventSelectAll.Checked = false;
            }
            foreach (RepeaterItem aItem in rptParentEvent.Items)
            {
                CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");
                chkParentEvent.Checked = false;
                CheckBox ParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("ParentEventSelectAll");
             
                for (int i = 0; i <= vGetParentEventMappedIDs.Count - 1; i++)
                {
                    if (((Label)aItem.FindControl("lblParentEventID")).Text.Trim() == vGetParentEventMappedIDs[i].ToString())
                    {
                        chkParentEvent.Checked = true;
                    }
                }
                if ((rptParentEvent.Items.Count) == (vGetParentEventMappedIDs.Count))
                {
                    ParentEventSelectAll.Checked = true;
                }
                else
                {
                    ParentEventSelectAll.Checked = false;
                }
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }


        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtParentEvent.Text = "< Select Event >";
            chkIntermediate.Checked = false;
            foreach (RepeaterItem aItem in rptParentEvent.Items)
            {
                CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");
                chkParentEvent.Checked = false;
                CheckBox chkParentEventSelectAll = (CheckBox)rptParentEvent.Controls[0].Controls[0].FindControl("parentEventSelectAll");
                chkParentEventSelectAll.Checked = false;
            }
            if (rblRelationship.SelectedValue.Equals("4"))
            {
                Multiview1.ActiveViewIndex = 1;
            }
            else
            {
                Multiview1.ActiveViewIndex = 0;
            }

            rptParentEvent.DataSource = null;
            rptParentEvent.DataBind();
            txtParentEvent.Text = "< Select >";

            if (rblRelationship.SelectedValue.Equals("0"))
            {
                BindEvents(1);
            }
            else if (rblRelationship.SelectedValue.Equals("1"))
            {
                BindEvents(2); 
            }
            else if (rblRelationship.SelectedValue.Equals("2"))
            {
                BindEvents(3); 
            }
            else if (rblRelationship.SelectedValue.Equals("3"))
            {
                BindEvents(4); 
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeParentEventList", string.Format("initializeJQueryUI('{0}', 'dvParentEvent');", txtParentEvent.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideEvent", "$(\"#dvParentEvent\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComplianceEventList", string.Format("initializeJQueryUI('{0}', 'dvComplianceEvent');", txtCompliance.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideComplianceEvent", "$(\"#dvComplianceEvent\").hide(\"blind\", null, 5, function () { });", true);
                
              }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ParentEventIds = new List<int>();
                string EventType = "";
                if (rblRelationship.SelectedValue.Equals("0"))
                    EventType = "PP";
                if (rblRelationship.SelectedValue.Equals("1"))
                    EventType = "PP";
                if (rblRelationship.SelectedValue.Equals("2"))
                    EventType = "PP";
                if (rblRelationship.SelectedValue.Equals("3"))
                    EventType = "PP";

                Business.ComplianceManagement.UpdateEventSubEventMappedID(Convert.ToInt32(ddlEvents.SelectedValue), EventType);
                foreach (RepeaterItem aItem in rptParentEvent.Items)
                {
                    CheckBox chkParentEvent = (CheckBox)aItem.FindControl("chkParentEvent");
                    if (chkParentEvent.Checked)
                    {
                        if (chkIntermediate.Checked == true)
                        {
                            if (rblRelationship.SelectedValue.Equals("0"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    //SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IntermediateEventID  = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 1,
                                    IntermediateFlag =true,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("1"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    //SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IntermediateEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 2,
                                    IntermediateFlag = true,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("2"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    //SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IntermediateEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 3,
                                    IntermediateFlag = true,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("3"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    //SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IntermediateEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 4,
                                    IntermediateFlag = true,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                        }
                        else
                        {
                            if (rblRelationship.SelectedValue.Equals("0"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 1,
                                    IntermediateFlag = false,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("1"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 2,
                                    IntermediateFlag = false,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("2"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 3,
                                    IntermediateFlag = false,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                            if (rblRelationship.SelectedValue.Equals("3"))
                            {
                                ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()));
                                EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                                {
                                    ParentEventID = Convert.ToInt32(ddlEvents.SelectedValue),
                                    SubEventID = Convert.ToInt32(((Label)aItem.FindControl("lblParentEventID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "PP",
                                    Type = 4,
                                    IntermediateFlag = false,
                                };
                                Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                            }
                        }
                    }
                }
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Save Successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

        }

        protected void ddlEventCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> vGetComplianceMappedIDs = new List<int>();

            BindCompliance();
            vGetComplianceMappedIDs = Business.ComplianceManagement.GetEventComplianceMappedID(Convert.ToInt32(ddlEventCompliance.SelectedValue), "PC");

            txtCompliance.Text = "< Select >";
            foreach (RepeaterItem aItem in rptCompliance.Items)
            {
                CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                chkCompliance.Checked = false;
                CheckBox chkComplianceSelectAll = (CheckBox)rptCompliance.Controls[0].Controls[0].FindControl("complianceSelectAll");
                chkComplianceSelectAll.Checked = false;
            }
            foreach (RepeaterItem aItem in rptCompliance.Items)
            {
                CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                chkCompliance.Checked = false;
                CheckBox complianceSelectAll = (CheckBox)rptCompliance.Controls[0].Controls[0].FindControl("complianceSelectAll");

                for (int i = 0; i <= vGetComplianceMappedIDs.Count - 1; i++)
                {
                    if (((Label)aItem.FindControl("lblComplianceID")).Text.Trim() == vGetComplianceMappedIDs[i].ToString())
                    {
                        chkCompliance.Checked = true;
                    }
                }
                if ((rptCompliance.Items.Count) == (vGetComplianceMappedIDs.Count))
                {
                    complianceSelectAll.Checked = true;
                }
                else
                {
                    complianceSelectAll.Checked = false;
                }
            }
        }

        protected void btnSaveCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ParentEventIds = new List<int>();
                string EventType = "";
                if (rblRelationship.SelectedValue.Equals("4"))
                    EventType = "PC";

                Business.ComplianceManagement.UpdateComplianceMappedID(Convert.ToInt32(ddlEventCompliance.SelectedValue), EventType);
                foreach (RepeaterItem aItem in rptCompliance.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                    if (chkCompliance.Checked)
                    {
                        if (rblRelationship.SelectedValue.Equals("4"))
                        {
                            ParentEventIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblComplianceID")).Text.Trim()));
                            EventSubEventMapping eventSubEventMapping = new EventSubEventMapping()
                            {
                                ParentEventID = Convert.ToInt32(ddlEventCompliance.SelectedValue),
                                ComplianceID = Convert.ToInt32(((Label)aItem.FindControl("lblComplianceID")).Text.Trim()),
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                EventType = "PC",
                            };
                            Business.ComplianceManagement.CreateEventSubEventMapping(eventSubEventMapping);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}