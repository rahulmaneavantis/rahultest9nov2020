﻿<%@ Page Title="Compliance List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" EnableEventValidation="true"
    CodeBehind="EditEventComplianceList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EditEventComplianceList"  Culture="en-GB"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 504px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(document).tooltip();
        });

        $(document).ready(function () {
            if (document.getElementById('BodyContent_saveopo').value == "true") {

                $('#divComplianceDetailsDialog').dialog({
                    height: 670,
                    width: 800,
                    autoOpen: false,
                    draggable: true,
                    title: "Compliance Details",
                    open: function (type, data) {
                        $(this).parent().appendTo("form");
                    }
                });
                newfun();
            }

         

        });
        function newfun() {

            $("#divComplianceDetailsDialog").dialog('open');

        }

        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        var validFilesTypes = ["exe", "bat", "dll"];
        function ValidateFile() {

            var label = document.getElementById("<%=Label2.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

    </script>
    <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:DropDownList runat="server" ID="ddlAct1" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                          OnSelectedIndexChanged="ddlAct1_SelectedIndexChanged"  CssClass="txtbox" AutoPostBack="true" />

                    </td>
                    <td>
                        <asp:RadioButton ID="rdFunctionBased" Visible="false" Text="Function Based" AutoPostBack="true" Width="110px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdFunctionBased_CheckedChanged" />
                    </td>
                    <td>
                        <asp:RadioButton ID="rdChecklist" Visible="false" Text="Checklist" AutoPostBack="true" Width="100px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdChecklist_CheckedChanged" />
                    </td>
                    <td style="width: 25%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right" style="width: 70px">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddCompliance" OnClick="btnAddCompliance_Click" Visible="false" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Sections" HeaderText="Sections" ItemStyle-Width="200px" SortExpression="Sections" />
                        <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="140px" SortExpression="UploadDocument">
                            <ItemTemplate>
                                <%# Convert.ToBoolean(Eval("UploadDocument")) ? "Yes" : "No" %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Risk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="Risk" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# ComplianceActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandArgument='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtComplianceLog" runat="server" CommandName="Log_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/view_details_icon.png" alt="Compliance Log" title="Compliance Log" /></asp:LinkButton>

                                <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                                <asp:LinkButton ID="lnkStatus" runat="server" Visible='<%# ButtonDisplayComplianceActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandName="STATUS" CommandArgument='<%# Eval("ID") %>'><img src="../Images/change_status_icon.png" alt="Status change"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divComplianceDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlAct" Style="padding: 0px; margin: 0px; height: 22px; width: 690px;"
                            CssClass="txtbox" />
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Act Name."
                            ControlToValidate="ddlAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Section(s) / Rule(s)</label>
                        <asp:TextBox runat="server" ID="tbxSections" Style="height: 16px; width: 690px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Section(s) / Rule(s) can not be empty."
                            ControlToValidate="tbxSections" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescription" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 690px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Short Description can not be empty."
                            ControlToValidate="txtShortDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="height: 50px; width: 690px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Description can not be empty."
                            ControlToValidate="tbxDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Change Type?</label>
                        
                        <asp:RadioButtonList ID="rbtChangeType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                             OnSelectedIndexChanged="rbtChangeType_SelectedIndexChanged">
                            <asp:ListItem>Temporary</asp:ListItem>
                            <asp:ListItem>Permanent</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div id="divtemp" runat="server" visible="false">
                        <div style="margin-bottom: 7px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Change Start Date</label>
                            <asp:TextBox runat="server" ID="txtChangeStartDate" Style="height: 16px; width: 250px;" />
                        </div>
                        <div style="margin-bottom: 7px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Change End Date</label>
                            <asp:TextBox runat="server" ID="txtChangeEndDate" Style="height: 16px; width: 250px;" />
                        </div>

                            <asp:CompareValidator ID="CompareValidator14" ValidationGroup = "ComplianceValidationGroup" ForeColor = "Red" runat="server" 
                            ControlToValidate = "txtChangeStartDate" ControlToCompare = "txtChangeEndDate" Operator = "LessThan" Type = "Date"
                            ErrorMessage="Start date must be less than End date."></asp:CompareValidator>

                    </div>
                    <div id="divPerma" runat="server" visible="false">
                        <div style="margin-bottom: 7px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                *</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Change Effective Date</label>
                            <asp:TextBox runat="server" ID="txtChangeEffectiveDate" Style="height: 16px; width: 250px;" />
                        </div>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Industry
                        </label>
                        <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 0px; margin: 0px; height: 22px; width: 690px;"
                            CssClass="txtbox" />
                        <div style="width: 70.5%; margin-left: 210px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvIndustry">

                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>

                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Legal Entity Type
                        </label>
                        <asp:TextBox runat="server" ID="txtEntityType" Style="padding: 0px; margin: 0px; height: 22px; width: 690px;"
                            CssClass="txtbox" />
                        <div style="width: 70.5%; margin-left: 210px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvEntityType">

                            <asp:Repeater ID="rptEntityType" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="EntityTypeSelectAll" Text="Select All" runat="server" onclick="checkAllET(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeaterEntityType" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                          
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkEntityType" runat="server" onclick="UncheckHeaderET();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblEntityTypeID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblEntityTypeName" runat="server" Text='<%# Eval("EntityTypeName")%>' ToolTip='<%# Eval("EntityTypeName") %>'></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>

                            </asp:Repeater>

                        </div>

                    </div>

                    <div style="margin-bottom: 7px">
                      
                        <asp:DropDownList runat="server" Visible="false" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 545px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                            <asp:ListItem Text="Function based" Value="0" />
                            <asp:ListItem Text="Checklist" Value="1" />
                            <asp:ListItem Text="Time Based" Value="2" />
                        </asp:DropDownList>
                       
                    </div>
                    <div style="margin-bottom: 7px" id="divChecklist" visible="false" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Checklist Type</label>
                        <asp:DropDownList runat="server" ID="ddlChklstType" Style="padding: 0px; margin: 0px; height: 22px; width: 545px;"
                            CssClass="txtbox" AutoPostBack="true">
                          
                            <asp:ListItem Text="One Time based Checklist" Value="0" />
                            <asp:ListItem Text="Function based Checklist" Value="1" />
                            <asp:ListItem Text="Time Based Checklist" Value="2" />
                          
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Select Compliance Type."
                            ControlToValidate="ddlComplianceType" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" id="divOneTime" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            One Time Date</label>
                        <asp:TextBox runat="server" ID="tbxOnetimeduedate" Style="height: 16px; width: 390px;" AutoPostBack="true" />
                        <asp:RequiredFieldValidator ID="rfvOnetimeduedate" ErrorMessage="Please enter One time Due Date."
                            ControlToValidate="tbxOnetimeduedate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px" id="divTimebasedTypes" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Timely Based Type</label>
                        <asp:RadioButtonList ID="rbTimebasedTypes" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="rbTimebasedTypes_SelectedIndexChanged">
                            <asp:ListItem Text="Fixed Gap" Value="0" Selected="True" />
                            <asp:ListItem Text="Periodically Based" Value="1" />
                        </asp:RadioButtonList>
                    </div>
                    <div runat="server" id="divNatureOfCompliance" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Nature Of Compliance</label>
                        <asp:DropDownList runat="server" ID="ddlNatureOfCompliance" OnSelectedIndexChanged="ddlNatureOfCompliance_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                            <asp:ListItem Text="< Select >" Value="-1" />
                            <asp:ListItem Text="Canteen" Value="12" />
                            <asp:ListItem Text="Certificates/Licensing" Value="8" />
                            <asp:ListItem Text="Cleanliness" Value="10" />
                            <asp:ListItem Text="Examination" Value="13" />
                            <asp:ListItem Text="Holiday List" Value="9" />
                            <asp:ListItem Text="Inspection" Value="5" />
                            <asp:ListItem Text="Maintenance" Value="14" />
                            <asp:ListItem Text="Meeting" Value="6" />
                            <asp:ListItem Text="Minimum Wages" Value="11" />
                            <asp:ListItem Text="Notices/Correspondences" Value="15" />
                            <asp:ListItem Text="Payments" Value="1" />
                            <asp:ListItem Text="Registers" Value="7" />
                            <asp:ListItem Text="Report" Value="3" />
                            <asp:ListItem Text="Returns" Value="0" />
                            <asp:ListItem Text="Safety and Welfare" Value="16" />
                            <asp:ListItem Text="Training" Value="4" />
                            <asp:ListItem Text="Others" Value="17" />
                        </asp:DropDownList>
                        <asp:CompareValidator ErrorMessage="Please select Nature of Compliance." ControlToValidate="ddlNatureOfCompliance"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Nature Of Compliance Sub Type</label>
                        <asp:DropDownList runat="server" ID="ddlComplianceSubType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                         <%--  <asp:CompareValidator ErrorMessage="Please select Compliance sub type" ControlToValidate="ddlComplianceSubType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />--%>
                    </div>

                    <div style="margin-bottom: 7px" id="Div1" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Upload Document</label>
                        <asp:RadioButton ID="rdoUploadDoc" Checked="true" Text="Yes" Font-Bold="true" GroupName="radioUplaod" runat="server" />
                        <asp:RadioButton ID="rdoNotUploadDoc" Text="No" Font-Bold="true" GroupName="radioUplaod" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px" id="Div2" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Visible
                        </label>
                        <asp:RadioButton ID="rdoComplianceVisible" Checked="true" Text="Yes" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                        <asp:RadioButton ID="rdoNotComplianceVisible" Text="No" Font-Bold="true" GroupName="radioComplianceVisible" runat="server" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <%--  <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Event Based</label>--%>
                        <asp:CheckBox runat="server" Visible="false" ID="chkEventBased" Checked="true" CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <div id="divEvent" runat="server">
                        <div style="margin-bottom: 7px">
                            <%--    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Events</label>--%>
                            <asp:DropDownList runat="server" ID="ddlEvents" AutoPostBack="true" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" OnSelectedIndexChanged="ddlEvents_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please Select Event." ControlToValidate="ddlEvents" ID="rfEvents"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                Display="None" />
                        </div>
                        <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode" visible="false">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Sub Event</label>
                            <asp:TextBox runat="server" ID="tbxSubEvent" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" Text="< Select Sub Event >" />
                            <div style="margin-left: 200px; position: absolute; z-index: 10" id="divSubevent">
                                <asp:TreeView runat="server" ID="tvSubEvent" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" Height="200px" Width="394px"
                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvSubEvent_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                        <div style="margin-bottom: 7px" id="divEventComplianceType" runat="server">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                Event Compliance Type</label>
                            <asp:RadioButtonList ID="rbEventComplianceType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Mandatory" Value="0" Selected="True" />
                                <asp:ListItem Text="Optional" Value="1" />
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div style="margin-bottom: 7px;" id="divComplianceDueDays" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Compliance Due(Day)</label>
                        <asp:TextBox runat="server" ID="txtEventDueDate" MaxLength="4" Width="80px" />
                        <asp:RegularExpressionValidator ID="rgexEventDueDate" ControlToValidate="txtEventDueDate"
                            runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                        <asp:RequiredFieldValidator ID="rfvEventDue" ErrorMessage="Please enter compliance after due."
                            ControlToValidate="txtEventDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" /><%-- Enabled="false" --%>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" id="divFunctionBased">
                                <div id="divNonEvents" runat="server">

                                    <div style="margin-bottom: 7px" id="divFrequency" runat="server">
                                        <%-- <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                    *</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Frequency</label>--%>
                                        <asp:DropDownList runat="server" Visible="false" ID="ddlFrequency" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                            CssClass="txtbox">
                                        </asp:DropDownList>
                                        <%--  <asp:CompareValidator ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequency" ID="cvfrequency"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" />--%>
                                    </div>
                                    <div style="margin-bottom: 7px" id="vivDueDate" runat="server">
                                        <%-- <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Due Date</label>--%>
                                        <asp:DropDownList runat="server" Visible="false" ID="ddlDueDate" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                            CssClass="txtbox">
                                        </asp:DropDownList>
                                        <%--   <asp:RequiredFieldValidator ID="vaDueDate" ErrorMessage="Please Select Due Date."
                                    ControlToValidate="ddlDueDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                                    Display="None" Enabled="false" />--%>
                                    </div>
                                </div>
                                <div style="margin-bottom: 7px" id="divNonComplianceType1" runat="server">
                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                        Non Compliance Type</label>
                                    <asp:DropDownList runat="server" ID="ddlNonComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlNonComplianceType_SelectedIndexChanged">
                                        <asp:ListItem Text="< Select >" Value="-1" />
                                        <asp:ListItem Text="Both" Value="2" />
                                        <asp:ListItem Text="Monetary" Value="0" />
                                        <asp:ListItem Text="Non-Monetary" Value="1" />
                                    </asp:DropDownList>
                                </div>
                                <div runat="server" id="divMonetary" style="border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 10px">
                                    <div style="margin-bottom: 25px">
                                        <label style="display: block; float: left; font-size: 13px; color: #333; font-weight: bold">
                                            Monetory</label>
                                    </div>
                                    <div style="margin-bottom: 7px; clear: both;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Fixed Minimum</label>
                                        <asp:TextBox runat="server" ID="tbxFixedMinimum" Style="height: 16px; width: 390px;"
                                            MaxFixedMinimum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Fixed minimum can not be empty."
                                            ControlToValidate="tbxFixedMinimum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Fixed minimum is a not valid number."
                                            ControlToValidate="tbxFixedMinimum" Operator="DataTypeCheck" Type="Double" runat="server"
                                            Display="None" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Fixed Maximum</label>
                                        <asp:TextBox runat="server" ID="tbxFixedMaximum" Style="height: 16px; width: 390px;"
                                            MaxFixedMaximum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Fixed maximum can not be empty."
                                            ControlToValidate="tbxFixedMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Fixed maximum is a not valid number."
                                            ControlToValidate="tbxFixedMaximum" Operator="DataTypeCheck" Type="Double" runat="server"
                                            Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CompareValidator ID="CompareValidatormaximum" runat="server" ErrorMessage="Fixed maximum should be greater than fixed minimum." Type="Double"
                                            ControlToCompare="tbxFixedMinimum" ControlToValidate="tbxFixedMaximum" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 200px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <asp:DropDownList runat="server" ID="ddlPerDayMonth" Style="padding: 0px; margin: 0px; margin-left: 200px; height: 22px; width: 390px;"
                                            CssClass="txtbox" AutoPostBack="true">
                                            <asp:ListItem Text="< Select >" Value="-1" />
                                            <asp:ListItem Text="Day" Value="0" />
                                            <asp:ListItem Text="Month" Value="1" />
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please select Day or Month."
                                            InitialValue="-1" ControlToValidate="ddlPerDayMonth" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount Rs.</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPerDay" Style="height: 16px; width: 390px;"
                                            MaxVariableAmountPerDay="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Variable amount can not be empty."
                                            ControlToValidate="tbxVariableAmountPerDay" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Variable amount is a not valid number."
                                            ControlToValidate="tbxVariableAmountPerDay" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (Max)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPerDayMax" Style="height: 16px; width: 390px;"
                                            MaxVariableAmountPerDayMax="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Variable amount (Max) can not be empty."
                                            ControlToValidate="tbxVariableAmountPerDayMax" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Variable amount (max) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPerDayMax" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CompareValidator ID="CompareValidator12" runat="server" ErrorMessage="Variable Amount (Max) should be greater than Variable Amount Rs." Type="Double"
                                            ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (%)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPercent" Style="height: 16px; width: 390px;"
                                            MaxVariableAmountPercent="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Variable amount (%) can not be empty."
                                            ControlToValidate="tbxVariableAmountPercent" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator6" ErrorMessage="Variable amount (%) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPercent" Operator="DataTypeCheck" Type="Double"
                                            runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                            *</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Variable Amount (% Max)</label>
                                        <asp:TextBox runat="server" ID="tbxVariableAmountPercentMaximum" Style="height: 16px; width: 390px;"
                                            MaxVariableAmountPercentMaximum="4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Variable amount (% max) can not be empty."
                                            ControlToValidate="tbxVariableAmountPercentMaximum" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                        <asp:CompareValidator ID="CompareValidator7" ErrorMessage="Variable amount (% max) is a not valid number."
                                            ControlToValidate="tbxVariableAmountPercentMaximum" Operator="DataTypeCheck"
                                            Type="Double" runat="server" Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        <asp:CompareValidator ID="CompareValidator13" runat="server" ErrorMessage="Variable Amount (% Max) should be greater than Variable Amount Variable Amount (%)." Type="Double"
                                            ControlToCompare="tbxVariableAmountPerDay" ControlToValidate="tbxVariableAmountPerDayMax" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                    </div>
                                </div>
                                <div runat="server" id="divNonMonetary" style="border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 10px">
                                    <div style="margin-bottom: 25px">
                                        <label style="display: block; float: left; font-size: 13px; color: #333; font-weight: bold">
                                            Non-Monetory</label>
                                    </div>
                                    <div style="margin-bottom: 7px; clear: both">
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Imprisonment</label>
                                        <asp:CheckBox runat="server" ID="chbImprisonment" CssClass="txtbox" AutoPostBack="true"
                                            OnCheckedChanged="chbImprisonment_CheckedChanged" />
                                    </div>
                                    <div id="divImprisonmentDetails" runat="server" visible="false">
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                Designation</label>
                                            <asp:TextBox runat="server" ID="tbxDesignation" Style="height: 16px; width: 390px;"
                                                MaxDesignation="4" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Designation can not be empty."
                                                ControlToValidate="tbxDesignation" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                Minimum months
                                            </label>
                                            <asp:TextBox runat="server" ID="tbxMinimumYears" Style="height: 16px; width: 390px;"
                                                MaxMinimumYears="4" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Minimum months  can not be empty."
                                                ControlToValidate="tbxMinimumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator9" ErrorMessage="Minimum months  is a not valid number."
                                                ControlToValidate="tbxMinimumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                        </div>
                                        <div style="margin-bottom: 7px">
                                            <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                                Maximum months</label>
                                            <asp:TextBox runat="server" ID="tbxMaximumYears" Style="height: 16px; width: 390px;"
                                                MaxMaximumYears="4" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Maximum months can not be empty."
                                                ControlToValidate="tbxMaximumYears" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                Display="None" />
                                            <asp:CompareValidator ID="CompareValidator10" ErrorMessage="Maximum months is not a valid number."
                                                ControlToValidate="tbxMaximumYears" Operator="DataTypeCheck" Type="Integer" runat="server"
                                                Display="None" ValidationGroup="ComplianceValidationGroup" />
                                            <asp:CompareValidator ID="CompareValidator11" runat="server" ErrorMessage="Maximum year should be greater than minimum year." Type="Double"
                                                ControlToCompare="tbxMinimumYears" ControlToValidate="tbxMaximumYears" Display="None" Operator="GreaterThanEqual" ValidationGroup="ComplianceValidationGroup" />
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Others</label>
                                        <asp:TextBox runat="server" ID="tbxNonComplianceEffects" Style="height: 16px; width: 390px;" />
                                    </div>
                                </div>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Penalty Description</label>
                                <asp:TextBox runat="server" ID="txtPenaltyDescription" TextMode="MultiLine" Style="height: 50px; width: 690px;" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Reference Material Text</label>
                                <asp:TextBox runat="server" ID="txtReferenceMaterial" TextMode="MultiLine" Style="height: 50px; width: 690px;" />
                            </div>

                            <div style="margin-bottom: 7px" id="divReminderType">
                                <%-- <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Reminder Type</label>--%>
                                <asp:RadioButtonList ID="rbReminderType" Visible="false" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                    OnSelectedIndexChanged="rbReminderType_SelectedIndexChanged">
                                    <asp:ListItem Text="Standard" Value="0" Selected="True" />
                                    <asp:ListItem Text="Custom" Value="1" />
                                </asp:RadioButtonList>
                            </div>
                            <div style="margin-bottom: 7px;" id="divForCustome" runat="server" visible="false">
                                <div style="float: left; width: 30%; overflow: hidden; margin-left: 200px;">
                                    <label>
                                        Before(In Days)
                                    </label>
                                    <asp:TextBox runat="server" ID="txtReminderBefore" MaxLength="4" Width="80px" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtReminderBefore"
                                        runat="server" ErrorMessage="Only Numbers allowed" Display="None" ValidationExpression="^-?[0-9]\d*(\.\d+)?$" ValidationGroup="ComplianceValidationGroup" />
                                    <asp:RequiredFieldValidator ID="rfvforcustomeReminder" ErrorMessage="Please enter reminder before value."
                                        ControlToValidate="txtReminderBefore" runat="server" ValidationGroup="ComplianceValidationGroup"
                                        Display="None" Enabled="false" />
                                </div>
                                <div style="overflow: hidden;">
                                    <label>
                                        Gap(In Days)
                                    </label>
                                    <asp:TextBox runat="server" ID="txtReminderGap" MaxLength="4" Width="80px" />
                                </div>

                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                                    *</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Risk Type</label>
                                <asp:DropDownList runat="server" ID="ddlRiskType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox">
                                    <asp:ListItem Text="< Select >" Value="-1" />
                                    <asp:ListItem Text="High" Value="0" />
                                    <asp:ListItem Text="Low" Value="2" />
                                    <asp:ListItem Text="Medium" Value="1" />
                                    <asp:ListItem Text="Critical" Value="3" />

                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator8" ErrorMessage="Please select Risk Type."
                                    ControlToValidate="ddlRiskType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                    ValidationGroup="ComplianceValidationGroup" Display="None" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Send Notification</label>
                                <asp:CheckBox runat="server" ID="chkNotification" CssClass="txtbox" OnCheckedChanged="chkNotification_CheckedChanged" AutoPostBack="true" />
                            </div>                          

                            <div style="margin-bottom: 7px" id="divNotification" runat="server" visible="false">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Notification Remark</label>
                                <asp:TextBox runat="server" ID="txtNotificationRemark" TextMode="MultiLine" Style="height: 50px; width: 690px;" />
                            </div>  
                            <asp:UpdatePanel ID="upFileUploadPanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px" id="dvUploadDoc" runat="server" visible="false">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Upload Document</label>
                                        <asp:CheckBox runat="server" ID="chkDocument" CssClass="txtbox" Checked="true" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                                    </div>
                                    <div style="margin-bottom: 7px" id="dvReqForms" runat="server">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Required Forms</label>
                                        <asp:TextBox runat="server" ID="tbxRequiredForms" Style="height: 16px; width: 690px;" />
                                    </div>
                                    <div style="margin-bottom: 7px" id="Div6" runat="server">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sample Form Link</label>
                                        <asp:TextBox runat="server" ID="txtSampleFormLink" Style="height: 16px; width: 690px;" />
                                    </div>
                                    <div style="margin-bottom: 7px" id="dvSampleForm" runat="server">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                            Sample Form</label>
                                        <asp:Label runat="server" ID="lblSampleForm" CssClass="txtbox" />
                                        <asp:FileUpload runat="server" ID="fuSampleFile" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 210px" id="Div5" runat="server">
                                        <asp:Label ID="Label2" runat="server"></asp:Label>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>


                            <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" OnClientClick="if (!ValidateFile()) return false;" CssClass="button"
                                    ValidationGroup="ComplianceValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divComplianceDetailsDialog').dialog('close');" />
                            </div>

                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                        <%--<label style="width: 208px; display: block; float: left; font-size: 13px; color: red;">Note :: (*) Fields Are Compulsary</label>--%>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="divComplianceStatusDialog">
        <asp:UpdatePanel ID="upComplianceStatusDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetailsStatus_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlStatusAct" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Section(s) / Rule(s)</label>
                        <asp:TextBox runat="server" ID="tbxSectionsStatus" Style="height: 16px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescriptionStatus" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="tbxDescriptionStatus" TextMode="MultiLine" Style="height: 50px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px" id="div3" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Deactivate Date</label>
                        <asp:TextBox runat="server" CssClass="StartDate" ID="txtDeactivateDate" Style="height: 16px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter deactivate Date."
                            ControlToValidate="txtDeactivateDate" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="Div4" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Document Upload</label>
                        <asp:Label runat="server" ID="lblDeactivate" CssClass="txtbox" />
                        <asp:FileUpload runat="server" ID="FileUploadDeactivateDoc" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDeactivateDesc" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Deactivate Description can not be empty."
                            ControlToValidate="txtDeactivateDesc" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 10px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Save" runat="server" ID="btnSaveDeactivate" OnClick="btnSaveDeactivate_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divComplianceStatusDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSaveDeactivate" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divComplianceScheduleDialog">
        <asp:UpdatePanel ID="upComplianceScheduleDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceScheduleDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="border: solid 1px red; background-color: #ffe8eb;"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divStartMonth" style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Year</label>
                        <asp:DropDownList runat="server" ID="ddlStartMonth" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlStartMonth_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Calender Year"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Financial Year"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upSchedulerRepeter" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" style="margin-bottom: 7px; padding-left: 50px">
                                <asp:Repeater runat="server" ID="repComplianceSchedule" OnItemDataBound="repComplianceSchedule_ItemDataBound">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th style="width: 150px; border: 1px solid gray">For Period
                                                </th>
                                                <th style="width: 200px; border: 1px solid gray">Day
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="margin-bottom: 7px">
                                            <tr>
                                                <td align="center" style="border: 1px solid gray">
                                                    <%# Eval("ForMonthName")%>
                                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnForMonth" Value='<%# Eval("ForMonth") %>' />
                                                </td>
                                                <td align="center" style="border: 1px solid gray">
                                                    <asp:DropDownList runat="server" ID="ddlMonths" Style="padding: 0px; margin: 0px; height: 22px; width: 100px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" />
                                                    <asp:DropDownList runat="server" ID="ddlDays" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                                        CssClass="txtbox" DataTextField="Name" DataValueField="ID" />
                                                </td>
                                            </tr>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 142px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveSchedule" OnClick="btnSaveSchedule_Click"
                            CssClass="button" />
                        <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click"
                            CssClass="button" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divComplianceScheduleDialog').dialog('close');" />
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <div id="divComplianceLOGDialog">
        <asp:UpdatePanel ID="upComplianceLogDialog" runat="server" UpdateMode="Conditional" OnLoad="upComplianceLogDialog_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <asp:GridView runat="server" ID="grdComplianceLog" AutoGenerateColumns="false" GridLines="Vertical" 
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" 
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" Width="100%" 
                        Font-Size="12px"   OnPageIndexChanging="grdComplianceLog_PageIndexChanging">
                        <Columns>                            
                            <asp:TemplateField HeaderText="Change Date" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" >
                                <ItemTemplate>
                                                                       
                                        <%# Eval("CreatedOn") != null ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MMM-yyyy") : ""%>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Short Description" ItemStyle-Width="200px" SortExpression="NewShortDescripton">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                        <asp:Label runat="server" Text='<%# Eval("NewShortDescripton") %>' ToolTip='<%# Eval("NewShortDescripton") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Detail Description" ItemStyle-Width="200px" SortExpression="NewDetailedDescription">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                        <asp:Label runat="server" Text='<%# Eval("NewDetailedDescription") %>' ToolTip='<%# Eval("NewDetailedDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="Start Date" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" >
                                <ItemTemplate>
                                                                         
                                           <%# Eval("Startdate") != null ? Convert.ToDateTime(Eval("Startdate")).ToString("dd-MMM-yyyy") : ""%>
                                
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="End Date" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="190px" >
                                <ItemTemplate>
                                                                       
                                          <%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MMM-yyyy") : ""%>
                                
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Effective Date" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="200px" >
                                <ItemTemplate>

                                         <%# Eval("EffectiveDate") != null ? Convert.ToDateTime(Eval("EffectiveDate")).ToString("dd-MMM-yyyy") : ""%>                                        
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:HiddenField ID="saveopo"  runat="server" Value="false"/>
    <script type="text/javascript">
        $(function () {
            $('#divComplianceDetailsDialog').dialog({
                height: 670,
                width: 1000,
                autoOpen: false,
                draggable: true,
                title: "Event Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceStatusDialog').dialog({
                height: 550,
                width: 800,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            $('#divComplianceScheduleDialog').dialog({
                height: 600,
                width: 600,
                autoOpen: false,
                draggable: true,
                title: "Compliance Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });


            $('#divComplianceLOGDialog').dialog({
                height: 500,
                width: 1100,
                autoOpen: false,
                draggable: true,
                title: "Compliance Log Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox() {
            $("#<%= ddlAct.ClientID %>").combobox();
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlNatureOfCompliance.ClientID %>").combobox();
            $("#<%= ddlFrequency.ClientID %>").combobox();
            $("#<%= ddlRiskType.ClientID %>").combobox();
            $("#<%= ddlDueDate.ClientID %>").combobox();
            $("#<%= ddlNonComplianceType.ClientID %>").combobox();
            $("#<%= ddlPerDayMonth.ClientID %>").combobox();
            $("#<%= ddlEvents.ClientID %>").combobox();
            $("#<%= ddlChklstType.ClientID %>").combobox();
            $("#<%= ddlStatusAct.ClientID %>").combobox();
            $("#<%= ddlComplianceSubType.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }




        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function checkAllET(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkEntityType") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderET() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkEntityType']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkEntityType']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='EntityTypeSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= tbxOnetimeduedate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "minDate", startDate);
                }
            });
            if (date != null) {
                $("#<%= tbxOnetimeduedate.ClientID %>").datepicker("option", "defaultDate", date);

            }
        }
         function initializeDatePickerStartDateChanges(date) {
            var startDate = new Date();
            $("#<%= txtChangeStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
               <%-- onClose: function (startDate) {
                    $("#<%= txtChangeStartDate.ClientID %>").datepicker("option", "minDate", startDate);
                }--%>
            });
            if (date != null) {
                $("#<%= txtChangeStartDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
        function initializeDatePickerENDDateChanges(date) {
            var startDate = new Date();
            $("#<%= txtChangeEndDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
              <%--  onClose: function (startDate) {
                    $("#<%= txtChangeEndDate.ClientID %>").datepicker("option", "minDate", startDate);
                }--%>
            });
            if (date != null) {
                $("#<%= txtChangeEndDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
        function initializeDatePickerChangeEffectiveDatChanges(date) {
            var startDate = new Date();
            $("#<%= txtChangeEffectiveDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
               <%-- onClose: function (startDate) {
                    $("#<%= txtChangeEffectiveDate.ClientID %>").datepicker("option", "minDate", startDate);
                }--%>
            });
            if (date != null) {
                $("#<%= txtChangeEffectiveDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
