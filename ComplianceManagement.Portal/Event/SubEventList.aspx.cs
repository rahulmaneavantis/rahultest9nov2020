﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class SubEventList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["eventID"] = Session["eventID"];
                    ViewState["ParentID"] = null;
                    BindEventSubType();
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                        btnAddSubEvent.Visible = true;
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        private void BindEventSubType()
        {
            try
            {
                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }


                dlBreadcrumb.DataSource = EventManagement.GetHierarchy(Convert.ToInt32(ViewState["eventID"]), parentID);
                dlBreadcrumb.DataBind();

                grdSubEventList.DataSource = EventManagement.GetAll(Convert.ToInt32(ViewState["eventID"]), parentID, tbxFilter.Text);
                grdSubEventList.DataBind();
                upSubEventList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdSubEventList.PageIndex = 0;
                BindEventSubType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddSubEvent_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxName.Text = string.Empty;
                PopulateInputForm();

                upSubEvents.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divSubEventDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SubEvent subEvent = new SubEvent()
                {
                    Name = tbxName.Text,
                    EventID = Convert.ToInt32(ViewState["eventID"]),
                    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                    CreatedBy = AuthenticationHelper.UserID,
                    UpdatedBy = AuthenticationHelper.UserID
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    subEvent.ID = Convert.ToInt32(ViewState["subeventID"]);
                }

                if (EventManagement.Exists(subEvent, Convert.ToInt32(ViewState["eventID"])))
                {
                    cvDuplicateEntry.ErrorMessage = "Sub event name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }


                if ((int)ViewState["Mode"] == 0)
                {
                    EventManagement.Create(subEvent);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    EventManagement.Update(subEvent);
                }

                BindEventSubType();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divSubEventDialog\").dialog('close')", true);
                upSubEvents.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int subeventID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_SUBEVENT"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["subeventID"] = subeventID;

                    SubEvent subEvent = EventManagement.GetSubEventByID(subeventID);

                    PopulateInputForm();

                    tbxName.Text = subEvent.Name;

                    upSubEvents.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divSubEventDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_SUBEVENT"))
                {
                    EventManagement.SubEventDelete(subeventID);
                    BindEventSubType();
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {
                        ViewState["ParentID"] = subeventID;
                        BindEventSubType();
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSubEventList.PageIndex = e.NewPageIndex;
                BindEventSubType();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdSubEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                long parentID = -1;
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                }

                var eventCatagory = EventManagement.GetAll(Convert.ToInt32(ViewState["eventID"]), parentID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    eventCatagory = eventCatagory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    eventCatagory = eventCatagory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdSubEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdSubEventList.Columns.IndexOf(field);
                    }
                }

                grdSubEventList.DataSource = eventCatagory;
                grdSubEventList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }


                    BindEventSubType();
                    upSubEventList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                divParent.Visible = ViewState["ParentID"] != null;
                litCustomer.Text = EventManagement.GetByID(Convert.ToInt32(ViewState["eventID"])).Name;

                if (ViewState["ParentID"] != null)
                {
                    litParent.Text = EventManagement.GetSubEventByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSubEventList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        e.Row.Cells[3].Visible = true;
                    }
                    else
                    {
                        e.Row.Cells[3].Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

    }
}