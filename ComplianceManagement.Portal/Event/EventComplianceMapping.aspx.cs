﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventComplianceMapping : System.Web.UI.Page
    {
        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindParentEvent(1);
                BindState();
                BindAct(-1);
                txtactList.Text = "< Act Filter >";
            }
        }

        private void BindAct(int StateID = -1)
        {
            var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
            string cashTimeval = string.Empty;
            string cashActval = string.Empty;
            try
            {
                
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                    
                    
                    if (objlocal == "Local")
                    {
                        cashTimeval = "Event_CHE" + AuthenticationHelper.UserID;
                        cashActval = "Event_CHEAct" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "EventCHE" + AuthenticationHelper.UserID;
                        cashActval = "Event_Act" + AuthenticationHelper.UserID;
                    }
                    List<Act> MasterQuery = new List<Act>();
                    List<Act> MasterQuerylst = new List<Act>();
                    if (CacheHelper.Exists(cashActval))
                    {
                        try
                        {
                            CacheHelper.Get<List<Act>>(cashActval, out MasterQuery);
                            MasterQuerylst = CustomerBranchManagement.BindActEvent(MasterQuery, StateID);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("GET BindAct Event Error exception :" + cashActval);
                            MasterQuerylst = CustomerBranchManagement.BindActEvent(StateID);
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {                    
                        MasterQuery = (from row in entities.Acts
                                       join row1 in entities.Compliances
                                       on row.ID equals row1.ActID
                                       where row.IsDeleted == false
                                       && row1.EventFlag == true || row1.ComplianceType == 1
                                       select row).Distinct().ToList();

                        CacheHelper.Set<List<Act>>(cashActval, MasterQuery);
                        MasterQuerylst = CustomerBranchManagement.BindActEvent(MasterQuery, StateID);                                                
                        if (CacheHelper.Exists(cashTimeval))
                        {
                            CacheHelper.Remove(cashTimeval);
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                        else
                        {
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                    }
                    rptActList.DataSource = MasterQuerylst;
                    rptActList.DataBind();
                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                        chkAct.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog("GET BindAct Event Error exception :" + cashActval);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindEvent(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlEvent.DataTextField = "SubEvent";
                ddlEvent.DataValueField = "SubEventID";

                ddlEvent.DataSource = EventManagement.GetSubEvent(ParentEventID);
                ddlEvent.DataBind();
                ddlEvent.Items.Insert(0, new ListItem("< Select Sub Event >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindParentEvent(int Type)
        {
            var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
            string cashTimeval = string.Empty;
            string cashEventmasterval = string.Empty;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                   
                    if (objlocal == "Local")
                    {
                        cashTimeval = "Event_CHE" + AuthenticationHelper.UserID;
                        cashEventmasterval = "Event_CHMaster" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "EventCHE" + AuthenticationHelper.UserID;
                        cashEventmasterval = "Event_Master" + AuthenticationHelper.UserID;
                    }
                    List<Business.Data.Event> MasterQuery = new List<Business.Data.Event>();
                    List<Business.Data.Event> MasterQuerylst = new List<Business.Data.Event>();
                    if (CacheHelper.Exists(cashEventmasterval))
                    {
                        try
                        {
                            CacheHelper.Get<List<Business.Data.Event>>(cashEventmasterval, out MasterQuery);
                            MasterQuerylst = EventManagement.GetParentEvent(MasterQuery, Type);
                        }
                        catch (Exception ex)
                        {
                            LogLibrary.WriteErrorLog("GET BindParentEvent Event Error exception :" + cashEventmasterval);
                            MasterQuerylst = EventManagement.GetParentEvent(Type);
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    else
                    {

                        MasterQuery = (from row in entities.Events
                                     where row.Visible == "Y"                                     
                                      && row.IsDeleted == false
                                     select row).OrderBy(entry => entry.Name).ToList();

                        MasterQuerylst = EventManagement.GetParentEvent(MasterQuery, Type);
                        CacheHelper.Set<List<Business.Data.Event>>(cashEventmasterval, MasterQuery);

                        if (CacheHelper.Exists(cashTimeval))
                        {
                            CacheHelper.Remove(cashTimeval);
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                        else
                        {
                            CacheHelper.Set(cashTimeval, DateTime.Now);
                        }
                    }
                    
                    ddlParentEvent.DataTextField = "Name";
                    ddlParentEvent.DataValueField = "ID";

                    ddlParentEvent.DataSource = MasterQuerylst;
                    ddlParentEvent.DataBind();
                    ddlParentEvent.Items.Insert(0, new ListItem("< Select Event >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LogLibrary.WriteErrorLog("GET BindParentEvent Event Error exception :" + cashEventmasterval);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindState()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = StateCityManagement.BindStateData();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("Central" , "0"));
                ddlState.Items.Insert(0, new ListItem("< State Filter >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCompliance()
        {
            try
            {
                lstLeft.DataSource = null;
                lstLeft.DataBind();
                List<long> actIds = new List<long>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }

                if (actIds.Count != 0 || tbxFilter.Text !="")
                {
                    List<SP_GetEventBasedCompliance_Result> MasterQuery = new List<SP_GetEventBasedCompliance_Result>();
                    List<SP_GetEventBasedCompliance_Result> MasterQuerylst = new List<SP_GetEventBasedCompliance_Result>();
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    string cashEventval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "Event_CHE" + AuthenticationHelper.UserID;
                        cashEventval = "Event_PSD" + AuthenticationHelper.UserID;                        
                    }
                    else
                    {
                        cashTimeval = "EventCHE" + AuthenticationHelper.UserID;
                        cashEventval = "EventPSD" + AuthenticationHelper.UserID;                        
                    }

                    try
                    {
                        
                        if (CacheHelper.Exists(cashEventval))
                        {
                            try
                            {                                
                                CacheHelper.Get<List<SP_GetEventBasedCompliance_Result>>(cashEventval, out MasterQuery);
                                MasterQuerylst = EventManagement.GetAllCompliances(MasterQuery, tbxFilter.Text, actIds);
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("GET BindCompliance Event Error exception :" + cashEventval);
                                MasterQuerylst = EventManagement.GetAllCompliances(tbxFilter.Text, actIds);
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        else
                        {                            
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                MasterQuery = (from row in entities.SP_GetEventBasedCompliance()
                                              select row).ToList();

                                MasterQuerylst = EventManagement.GetAllCompliances(MasterQuery,tbxFilter.Text, actIds);

                                //CacheHelper.Set<SP_GetEventBasedCompliance_Result>(cashEventval, MasterQuery);
                                CacheHelper.Set<List<SP_GetEventBasedCompliance_Result>>(cashEventval, MasterQuery);

                                if (CacheHelper.Exists(cashTimeval))
                                {
                                    CacheHelper.Remove(cashTimeval);
                                    CacheHelper.Set(cashTimeval, DateTime.Now);
                                }
                                else
                                {
                                    CacheHelper.Set(cashTimeval, DateTime.Now);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogLibrary.WriteErrorLog("KeyExists BindCompliance Event Error exception :" + cashEventval);
                        MasterQuerylst = EventManagement.GetAllCompliances(tbxFilter.Text, actIds);
                    }

                    MasterQuerylst = MasterQuerylst.GroupBy(a => a.ID).Select(a => a.FirstOrDefault()).ToList();

                    List<long> MappedComplianceList = new List<long>();

                    foreach (ListItem item in lstRight.Items)
                    {
                        MappedComplianceList.Add(Convert.ToInt64(item.Value));
                    }

                    MasterQuerylst = MasterQuerylst.ToList().Where(entry => !MappedComplianceList.Contains(entry.ID)).ToList();
                    
                    
                    lstLeft.DataTextField =  "ShortDescription";
                    lstLeft.DataValueField = "ID";

                    lstLeft.DataSource = MasterQuerylst;
                    lstLeft.DataBind();

                    foreach (ListItem item in lstLeft.Items)
                    {
                        item.Attributes["title"] = item.Text;
                    }
                }
                upEvent.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRightMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstLeft.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstLeft.Items.Count; i++)
                    {
                        if (lstLeft.Items[i].Selected)
                        {
                            if (!arraylist1.Contains(lstLeft.Items[i]))
                            {
                                arraylist1.Add(lstLeft.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < arraylist1.Count; i++)
                    {
                        if (!lstRight.Items.Contains(((ListItem)arraylist1[i])))
                        {
                            lstRight.Items.Add(((ListItem)arraylist1[i]));
                        }
                        lstLeft.Items.Remove(((ListItem)arraylist1[i]));
                    }
                    lstRight.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapping compliance to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            
        }

        protected void btnLeftMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstRight.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstRight.Items.Count; i++)
                    {
                        if (lstRight.Items[i].Selected)
                        {
                            if (!arraylist2.Contains(lstRight.Items[i]))
                            {
                                arraylist2.Add(lstRight.Items[i]);
                            }
                        }
                    }

                    for (int i = 0; i < arraylist2.Count; i++)
                    {
                        if (!lstLeft.Items.Contains(((ListItem)arraylist2[i])))
                        {
                            lstLeft.Items.Add(((ListItem)arraylist2[i]));
                        }
                        lstRight.Items.Remove(((ListItem)arraylist2[i]));
                    }
                    lstLeft.SelectedIndex = -1;
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select atleast one mapped compliance to move";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lstLeft.Items.Clear();
                BindAct(-1);
                tbxFilter.Text = "";

                var lst = EventManagement.GetAllMappedCompliances(Convert.ToInt32(ddlEvent.SelectedValue));

                lstRight.DataSource = lst;
                lstRight.DataTextField = "ShortDescription";
                lstRight.DataValueField = "ID";
                lstRight.DataBind();

                foreach (ListItem item in lstRight.Items)
                {
                    item.Attributes["title"] = item.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Business.ComplianceManagement.UpdateComplianceForEventMappedID(Convert.ToInt32(ddlEvent.SelectedValue));

                var EventType = Business.EventManagement.GetEventType(Convert.ToInt32(ddlEvent.SelectedValue)).Type;

                for (int i = 0; i < lstRight.Items.Count; i++)
                {
                    EventMapping eventMapping = new EventMapping()
                    {
                        ComplianceID = Convert.ToInt64(lstRight.Items[i].Value),
                        EventID = Convert.ToInt32(ddlEvent.SelectedValue),
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        Type = Convert.ToInt32(EventType),
                    };
                    Business.ComplianceManagement.CreateEventMapping(eventMapping);
                }

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Save Successfully";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlParentEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlEvent.DataSource = null;
                ddlEvent.DataBind();
                lstLeft.Items.Clear();
                lstRight.Items.Clear();
                tbxFilter.Text = "";
                BindAct(-1);
                if (ddlParentEvent.SelectedValue != "-1")
                {
                    BindEvent(Convert.ToInt32(ddlParentEvent.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliance();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindAct(Convert.ToInt32(ddlState.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstLeft.Items.Clear();
            lstRight.Items.Clear();
            tbxFilter.Text = "";
            BindAct(-1);

            if (rblRelationship.SelectedValue.Equals("0"))
            {
                BindParentEvent(1);
            }
            else if (rblRelationship.SelectedValue.Equals("1"))
            {
                BindParentEvent(2);
            }
            else if (rblRelationship.SelectedValue.Equals("2"))
            {
                BindParentEvent(3);
            }
            else if (rblRelationship.SelectedValue.Equals("3"))
            {
                BindParentEvent(4);
            }
            ddlEvent.DataSource = null;
            ddlEvent.DataBind();

        }

    }
}