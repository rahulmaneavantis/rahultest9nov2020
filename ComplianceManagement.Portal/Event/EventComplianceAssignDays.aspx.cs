﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Objects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventComplianceAssignDays : System.Web.UI.Page
    {
        public bool FlagAutoTrigger = false;
        public bool FlagUpdateAutoTrigger = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("IMPT") || AuthenticationHelper.Role.Equals("RPER") || AuthenticationHelper.Role.Equals("RREV")))
                {
                    btnSave.Visible = true;
                }
                 BindParentEvent();
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();

                if (rblRelationship.SelectedValue== "3")
                {
                    if (ddlState.SelectedValue != "-1")
                    {
                        int stateId = Convert.ToInt32(ddlState.SelectedValue);
                        compliance = compliance.Where(entry => entry.StateID == stateId || entry.StateID == null).ToList();
                    }
                }

                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetEventData(ParentEventID, type, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                //GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                //string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                //var Intermediate = entities.SP_GetIntermediateEvent(ParentEventID, Type).ToList();
                //gv2.DataSource = Intermediate;
                //gv2.DataBind();
            }
        }

        
        protected bool visibleUpdateBtnChildAutoTrigger(int ParentEventID, int IntermediateEventID, int SubEventID, int SequenceID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                if (SequenceID == 1)
                {
                    result = false;
                    if (IntermediateEventID == 0)
                    {
                        FlagUpdateAutoTrigger = true;
                    }
                }
                else
                {
                    result = true;
                    var data = (from row in entities.EventAutoTriggers
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID
                                select row).Distinct().FirstOrDefault();

                    if (data != null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    if (IntermediateEventID != 0)
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleBtnChildAutoTrigger(int ParentEventID, int IntermediateEventID,int SubEventID, int SequenceID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                if (SequenceID == 1)
                {
                    result = false;

                    if (IntermediateEventID == 0)
                    {
                        FlagAutoTrigger = true;
                    }
                }
                else
                {
                    result = true;
                    var data = (from row in entities.EventAutoTriggers
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID
                                select row).Distinct().FirstOrDefault();

                    if (data != null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }

                    if (IntermediateEventID != 0)
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }


        protected bool visibleTextDays(int ComplianceID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                bool result = false;
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID 
                            select row).Distinct().FirstOrDefault();

                if (data.IsFrequencyBased == true || data.ComplianceType == 2)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visiblelblFrequency(int ComplianceID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                bool result = false;
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).Distinct().FirstOrDefault();

                if (data.IsFrequencyBased == true || data.ComplianceType == 2)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected string GetFrequency(int ComplianceID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                string result = "";
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).Distinct().FirstOrDefault();

                if (data.ComplianceType == 0)
                {
                    if (data.Frequency != null)
                    {
                        result = data.Frequency == 0 ? "Monthly" :
                                             data.Frequency == 1 ? "Quarterly" :
                                             data.Frequency == 2 ? "HalfYearly" :
                                             data.Frequency == 3 ? "Annual" :
                                             data.Frequency == 4 ? "FourMonthly" :
                                             data.Frequency == 5 ? "TwoYearly" :
                                             data.Frequency == 6 ? "SevenYearly" :
                                             data.Frequency == 9 ? "Fortnight" :
                                             data.Frequency == 8 ? "Weekly" : "";
                    }
                    else
                    {
                        result = "";
                    }
                }
                else if (data.ComplianceType == 2)
                {
                    result = "TimeBased";
                }
                
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "";
        }

        protected bool visibleUpdateBtnIntermediateAutoTrigger(int ParentEventID, int IntermediateEventID, int SubEventID, int SequenceID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                if (FlagUpdateAutoTrigger == false)
                {
                    result = false;
                    FlagUpdateAutoTrigger = true;
                }
                else
                {
                    result = true;
                    var data = (from row in entities.EventAutoTriggers
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID
                                select row).Distinct().FirstOrDefault();

                    if (data != null)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    //if (IntermediateEventID != 0)
                    //{
                    //    result = false;
                    //}
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleBtnIntermediateAutoTrigger(int ParentEventID, int IntermediateEventID, int SubEventID, int SequenceID)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                if (FlagAutoTrigger == false)
                {
                    result = false;
                    FlagAutoTrigger = true;
                }
                else
                {
                    result = true;
                    var data = (from row in entities.EventAutoTriggers
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID
                                select row).Distinct().FirstOrDefault();
                    if (data != null)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                    //if (IntermediateEventID != 0)
                    //{
                    //    result = false;
                    //}
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void gvChildGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                divAutoTrigger.Visible = false;
                rfvAutoTriggerDays.Enabled = false;

                GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");
                int ParentEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());
                int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);
                var IntermediateEventID = childGrid.Rows[e.RowIndex].FindControl("IntermediateEventID") as Label;
                int intermediateEventID = Convert.ToInt32(IntermediateEventID.Text);

                ViewState["ParentEventID"] = ParentEventID;
                ViewState["IntermediateEventID"] = intermediateEventID;
                ViewState["SubEventID"] = SubEventID;

                var AutoTrigger = EventManagement.GetAutoTrigger(ParentEventID, intermediateEventID, SubEventID);
                chkAutoTrigger.Checked = false;
                txtAutoTriggerDays.Text = "";
                if (AutoTrigger != null)
                {
                    if(AutoTrigger.IsAutoTrigger == true)
                    {
                        divAutoTrigger.Visible = true;
                        rfvAutoTriggerDays.Enabled = true;
                    }
                    chkAutoTrigger.Checked = AutoTrigger.IsAutoTrigger;
                    txtAutoTriggerDays.Text =Convert.ToString(AutoTrigger.AutoTriggerDays);
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divAutoTriggerDialog\").dialog('open')", true);

                //txtShortNoticeDays.Text = "";
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");
                //    int ParentEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());
                //    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);
                //    int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);
                //    var txtDate = childGrid.Rows[e.RowIndex].FindControl("txtgvChildGridDate") as TextBox;
                //    if (txtDate.Text != "")
                //    {
                //        ViewState["interIndex"] = 0;
                //        ViewState["RowIndex"] = e.RowIndex;
                //        ViewState["ParentEventID"] = ParentEventID;
                //        ViewState["IntermediateEventID"] = 0;
                //        ViewState["SubEventID"] = SubEventID;
                //        ViewState["EventScheduledOnID"] = EventScheduledOnID;
                //        GridView gvEvenToCompliance = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvComplianceGrid");
                //        //Check shorter notice Compliance
                //        List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                //        Boolean shorterFlag = false;
                //        List<long> lstShortCompliance = new List<long>();
                //        foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                //        {
                //            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                //            bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                //            if (ExistShorterNotice == true)
                //            {
                //                shorterFlag = true;
                //                lstShortCompliance.Add(ComplinaceID);
                //                //break;
                //            }
                //        }

                //        if (shorterFlag == true)
                //        {
                //            // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#modalShortNotice').modal();", true);
                //            upOptionalCompliances.Update();
                //        }
                //        else
                //        {
                //            var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvChildGridDate") as TextBox;
                //            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //            EventAssignDate eventAssignDate = new EventAssignDate()
                //            {
                //                ParentEventID = ParentEventID,
                //                EventScheduleOnID = EventScheduledOnID,
                //                IntermediateEventID = 0,
                //                SubEventID = SubEventID,
                //                Date = Date,
                //                IsActive = true,
                //                CreatedDate = DateTime.Now,
                //                CreatedBy = Convert.ToInt32(Session["userID"]),
                //                Type = Type,
                //            };
                //            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                //            // Child Compliance 
                //            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                //            {
                //                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                //                int IsInternal = Convert.ToInt32(EvenToCompliancerow.Cells[3].Text);
                //                if (IsInternal == 0) //Statutory
                //                {
                //                    Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                //                    if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                //                    {
                //                        int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);
                //                        Boolean IsComplianceChecklistStatutory = EventManagement.CheckComplianceStatutoryChecklist(ComplinaceID);

                //                        if (IsComplianceChecklistStatutory == true)
                //                        {
                //                            //Change Generate flag Schedule change
                //                            EventManagement.UpdateComplianceInstanceScheduleFlag(ComplinaceID, ComplianceInstanceID);
                //                        }
                //                        int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                //                        Boolean FlgCheck = false;
                //                        FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                //                        if (FlgCheck == false)
                //                        {
                //                            EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                //                        }
                //                        else
                //                        {
                //                            int IncludeDays = 0;
                //                            if (days == 0)
                //                            {
                //                                IncludeDays = 0;
                //                            }
                //                            else
                //                            {
                //                                if (days > 0)
                //                                {
                //                                    IncludeDays = days - 1;
                //                                }
                //                                else
                //                                {
                //                                    IncludeDays = days + 1;
                //                                }
                //                            }
                //                            DateTime UpdatedDate = Date.AddDays(IncludeDays);
                //                            EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                //                            EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                //                        }
                //                    }
                //                }
                //                else if (IsInternal == 1) //Internal
                //                {
                //                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetInternalComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);

                //                    int days = Convert.ToInt32(EvenToCompliancerow.Cells[2].Text);
                //                    Boolean FlgCheck = false;
                //                    FlgCheck = EventManagement.CheckEventInternalComplianceAssigned(ParentEventID, 0, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                //                    if (FlgCheck == false)
                //                    {
                //                        EventManagement.GenerateEventInternalComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                //                    }
                //                    else
                //                    {
                //                        int IncludeDays = 0;
                //                        if (days == 0)
                //                        {
                //                            IncludeDays = 0;
                //                        }
                //                        else
                //                        {
                //                            if (days > 0)
                //                            {
                //                                IncludeDays = days - 1;
                //                            }
                //                            else
                //                            {
                //                                IncludeDays = days + 1;
                //                            }
                //                        }
                //                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                //                        EventManagement.UpdateInternalReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                //                        EventManagement.UpdateInternalComplianceSheduleOnDates(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                //                    }
                //                }
                //            }
                //            BindParentEventData(Convert.ToInt32(ParentEventID));
                //            cvDuplicateEntry.IsValid = false;
                //            cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                //        }
                //    }
                //    else
                //    {
                //        cvDuplicateEntry.IsValid = false;
                //        cvDuplicateEntry.ErrorMessage = "Please enter activated date";
                //    }
                //}
                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                gvParentGrid.DataSource = null;
                gvParentGrid.DataBind();
                BindParentEvent();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                Label lblIntermediateEventID = (Label)e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {
                    GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                    if (rblRelationship.SelectedValue == "3")
                    {
                        if (ddlState.SelectedValue != "-1")
                        {
                            int stateId = Convert.ToInt32(ddlState.SelectedValue);
                            Compliance = Compliance.Where(entry => entry.StateID == stateId || entry.StateID == null).ToList();
                        }
                    }
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
                else
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            int Type = 0;
            if (rblRelationship.SelectedValue == "0")
            {
                Type = 1;
            }
            if (rblRelationship.SelectedValue == "1")
            {
                Type = 2;
            }
            if (rblRelationship.SelectedValue == "2")
            {
                Type = 3;
            }
            if (rblRelationship.SelectedValue == "3")
            {
                Type = 4;
            }

            //Delete all event sequence
            ComplianceDBEntities entities = new ComplianceDBEntities();
            {
                int ParentEventID =Convert.ToInt32(ddlparentEvent.SelectedValue);
                var ids = (from row in entities.EventCompAssignDays
                           where row.ParentEventID == ParentEventID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventCompAssignDay prevmappedids = (from row in entities.EventCompAssignDays
                                                   where row.ID == entry
                                                   select row).FirstOrDefault();
                    entities.EventCompAssignDays.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }

            foreach (GridViewRow Eventrow in gvParentGrid.Rows)
            {
                int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);

                GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;


                //ParentEvent -> compliance
                foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                {
                    int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                    var textbox2 = EvenToCompliancerow.FindControl("txtEventComplianceDays") as TextBox;
                    int days = Convert.ToInt32(textbox2.Text);

                    EventCompAssignDay eventComplianceSequence = new EventCompAssignDay()
                    {
                        ParentEventID = ParentEventID,
                        ComplianceID = ComplinaceID,
                        Days = days,
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = Convert.ToInt32(Session["userID"]),
                        EventType = "",
                        Type = Type,
                    };
                    Business.ComplianceManagement.CreateEventCompAssignDays(eventComplianceSequence);
                }

                //ParentEvent -> SubEvent-> compliance
                GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                {
                    int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);

                    GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;

                    foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);
                        int days = -1;
                        var compliance = Business.ComplianceManagement.GetCompliance(ComplinaceID);

                        if (compliance.IsFrequencyBased == true || compliance.ComplianceType == 2)
                        {
                            days = -1;
                        }
                        else
                        {
                            var textbox1 = SubEventComplincerow.FindControl("txtSubEventComlianceDays") as TextBox;
                             days = Convert.ToInt32(textbox1.Text);
                        }

                        EventCompAssignDay subeventComplianceSequence = new EventCompAssignDay()
                        {
                            ParentEventID = ParentEventID,
                            SubEventID = SubEventID,
                            ComplianceID = ComplinaceID,
                            Days = days,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedBy = Convert.ToInt32(Session["userID"]),
                            EventType = "",
                            Type = Type,
                        };
                        Business.ComplianceManagement.CreateEventCompAssignDays(subeventComplianceSequence);
                    }


                    int IntermediateEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);

                    GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;

                    foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                    {
                        int SubEventID1 = Convert.ToInt32(SubEventrow1.Cells[1].Text);
                        GridView gvSubCompliance1 = SubEventrow1.FindControl("gvIntermediateComplainceGrid") as GridView;
                        foreach (GridViewRow SubEventComplincerow in gvSubCompliance1.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);

                            int days = -1;
                            var compliance = Business.ComplianceManagement.GetCompliance(ComplinaceID);
                            if (compliance.IsFrequencyBased == true || compliance.ComplianceType == 2)
                            {
                                days = -1;
                            }
                            else
                            {
                                var textbox1 = SubEventComplincerow.FindControl("txtIntermediateSubEventComlianceDays") as TextBox;
                                days = Convert.ToInt32(textbox1.Text);
                            }

                            EventCompAssignDay subeventComplianceSequence = new EventCompAssignDay()
                            {
                                ParentEventID = ParentEventID,
                                IntermediateEventID = IntermediateEventID,
                                SubEventID = SubEventID1,
                                ComplianceID = ComplinaceID,
                                Days = days,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                EventType = "",
                                Type = Type,
                            };
                            Business.ComplianceManagement.CreateEventCompAssignDays(subeventComplianceSequence);
                        }
                    }
                }
            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = "Save Successfully";
        }

        private void BindParentEvent()
        {
            try
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }


                if (Type == 4)
                {
                    BindState();
                    divState.Visible = true;
                }
                else
                {
                    ddlState.SelectedValue = "-1";
                    divState.Visible = false;
                }

                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlparentEvent.DataTextField = "Name";
                ddlparentEvent.DataValueField = "ParentEventID";
                ddlparentEvent.DataSource = entities.SP_GetParentEvent(Type).ToList(); ;
                ddlparentEvent.DataBind();
                ddlparentEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindState()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = StateCityManagement.BindStateData();
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("< State Filter >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlparentEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int Type = 0;
                    if (rblRelationship.SelectedValue == "0")
                    {
                        Type = 1;
                    }
                    if (rblRelationship.SelectedValue == "1")
                    {
                        Type = 2;
                    }
                    if (rblRelationship.SelectedValue == "2")
                    {
                        Type = 3;
                    }
                    if (rblRelationship.SelectedValue == "3")
                    {
                        Type = 4;
                    }
                    //GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    //int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;

                    //var IntermediateEventID = gv.Rows[e.Row.RowIndex].FindControl("IntermediateEventID1") as Label;
                    //int intermediateEventID = Convert.ToInt32(IntermediateEventID.Text);

                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    //GridView childGrid1 = (GridView)sender;
                    //int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    //int intermediateEventID = Convert.ToInt32(childGrid1.Rows[e.Row.RowIndex].FindControl("IntermediateEventID1") as Label);

                    //int intermediateEventID = Convert.ToInt32(e.Row.Cells[3].Text);

                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(ddlparentEvent.SelectedValue);
                    var Compliance = entities.SP_GetIntermediateComplianceAssignDays(parentEventID, intermediateEventID, SubEventID, Type).ToList();
                    if (rblRelationship.SelectedValue == "3")
                    {
                        if (ddlState.SelectedValue != "-1")
                        {
                            int stateId = Convert.ToInt32(ddlState.SelectedValue);
                            Compliance = Compliance.Where(entry => entry.StateID == stateId || entry.StateID == null).ToList();
                        }
                    }

                    gv.DataSource = Compliance;
                    gv.DataBind();

                    //GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
                    //int interIndex = masterrow.RowIndex;

                    //GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    //int parentEventID = Convert.ToInt32(intermediateGrid.DataKeys[interIndex].Value.ToString());

                    //GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                    //int SubEventID = Convert.ToInt32(childGrid.Rows[e.Row.RowIndex].Cells[1].Text);

                    //int intermediateEventID = Convert.ToInt32(childGrid.DataKeys[e.Row.RowIndex].Value.ToString());

                   


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void gvIntermediateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        int Type = 0;
        //        if (rblRelationship.SelectedValue == "0")
        //        {
        //            Type = 1;
        //        }
        //        if (rblRelationship.SelectedValue == "1")
        //        {
        //            Type = 2;
        //        }
        //        if (rblRelationship.SelectedValue == "2")
        //        {
        //            Type = 3;
        //        }
        //        if (rblRelationship.SelectedValue == "3")
        //        {
        //            Type = 4;
        //        }

        //        GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
        //        int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
        //        GridView childGrid1 = (GridView)sender;
        //        int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
        //        ComplianceDBEntities entities = new ComplianceDBEntities();
        //        var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
        //        gv.DataSource = Compliance;
        //        gv.DataBind();
        //    }
        //}

        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }
        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);


                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }

        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#D2691E");
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                }
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnSave.Visible = true;
                ddlparentEvent.SelectedValue = "-1";
                gvParentGrid.DataSource = null;
                gvParentGrid.DataBind();
                if (ddlState.SelectedValue !="-1")
                {
                    btnSave.Visible = false;
                    BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkAutoTrigger_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkAutoTrigger.Checked == true)
                {
                    divAutoTrigger.Visible = true;
                    rfvAutoTriggerDays.Enabled = true;
                }
                else
                {
                    divAutoTrigger.Visible = false;
                    rfvAutoTriggerDays.Enabled = false;
                }
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveAutoTrigger_Click(object sender, EventArgs e)
        {
            try
            {
                int AutoTriggerDays= Convert.ToInt32(txtAutoTriggerDays.Text);
                if (chkAutoTrigger.Checked == true)
                {
                    if (AutoTriggerDays > 0)
                    {
                        int ParentEventID = Convert.ToInt32(ViewState["ParentEventID"]);
                        int IntermediateEventID = Convert.ToInt32(ViewState["IntermediateEventID"]);
                        int SubEventID = Convert.ToInt32(ViewState["SubEventID"]);
                        var eventAutoTrigger = EventManagement.GetEventAutoTrigger(ParentEventID, IntermediateEventID, SubEventID);

                        EventAutoTrigger data = new EventAutoTrigger()
                        {
                            ParentEventID = ParentEventID,
                            IntermediateEventID = IntermediateEventID,
                            SubEventID = SubEventID,
                            IsAutoTrigger = chkAutoTrigger.Checked,
                            CreatedOn = DateTime.Now,
                            CreatedBy = AuthenticationHelper.UserID,
                        };

                        if (chkAutoTrigger.Checked == false)
                        {
                            data.AutoTriggerDays = 0;
                        }
                        else
                        {
                            data.AutoTriggerDays = Convert.ToInt32(txtAutoTriggerDays.Text);
                        }

                        if (eventAutoTrigger == null)
                        {
                            Business.EventManagement.CreateEventAutoTrigger(data);
                            lblImsg.Text = "Auto Trigger Added successfully";
                        }
                        else
                        {
                            data.ID = eventAutoTrigger.ID;
                            EventManagement.UpdateEventAutoTrigger(data);
                            lblImsg.Text = "AutoTrigger updated successfully";
                        }
                        BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
                    }
                    else
                    {
                        lblImsg.Text = "Auto Trigger days mustbe greater than zero";
                    }
                }
                else
                {
                    // Auto trigger remove
                    int ParentEventID = Convert.ToInt32(ViewState["ParentEventID"]);
                    int IntermediateEventID = Convert.ToInt32(ViewState["IntermediateEventID"]);
                    int SubEventID = Convert.ToInt32(ViewState["SubEventID"]);
                    var eventAutoTrigger = EventManagement.GetEventAutoTrigger(ParentEventID, IntermediateEventID, SubEventID);

                    EventAutoTrigger data = new EventAutoTrigger()
                    {
                        ParentEventID = ParentEventID,
                        IntermediateEventID = IntermediateEventID,
                        SubEventID = SubEventID,
                        IsAutoTrigger = chkAutoTrigger.Checked,
                        CreatedOn = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID,
                    };

                    if (chkAutoTrigger.Checked == false)
                    {
                        data.AutoTriggerDays = 0;
                    }
                    else
                    {
                        data.AutoTriggerDays = Convert.ToInt32(txtAutoTriggerDays.Text);
                    }

                    if (eventAutoTrigger == null)
                    {
                        Business.EventManagement.CreateEventAutoTrigger(data);
                        lblImsg.Text = "Auto Trigger Added successfully";
                    }
                    else
                    {
                        data.ID = eventAutoTrigger.ID;
                        EventManagement.UpdateEventAutoTrigger(data);
                        lblImsg.Text = "AutoTrigger updated successfully";
                    }
                    BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void gvIntermediateSubEventGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                divAutoTrigger.Visible = false;
                rfvAutoTriggerDays.Enabled = false;

                GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
                int interIndex = masterrow.RowIndex;

                GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                int ParentEventID = Convert.ToInt32(intermediateGrid.DataKeys[interIndex].Value.ToString());

                GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);

                int intermediateEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());

                ViewState["ParentEventID"] = ParentEventID;
                ViewState["IntermediateEventID"] = intermediateEventID;
                ViewState["SubEventID"] = SubEventID;

                var AutoTrigger = EventManagement.GetAutoTrigger(ParentEventID, intermediateEventID, SubEventID);
                chkAutoTrigger.Checked = false;
                txtAutoTriggerDays.Text = "";
                if (AutoTrigger != null)
                {
                    if (AutoTrigger.IsAutoTrigger == true)
                    {
                        divAutoTrigger.Visible = true;
                        rfvAutoTriggerDays.Enabled = true;
                    }
                    chkAutoTrigger.Checked = AutoTrigger.IsAutoTrigger;
                    txtAutoTriggerDays.Text = Convert.ToString(AutoTrigger.AutoTriggerDays);
                }
                UpdatePanel1.Update();
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divAutoTriggerDialog\").dialog('open')", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}