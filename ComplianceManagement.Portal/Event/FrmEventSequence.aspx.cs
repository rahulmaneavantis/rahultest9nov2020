﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Data.SqlClient;
using System.Data.Objects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class FrmEventSequence : System.Web.UI.Page
    {
        //protected static string DataString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindParentEvent();
                //DataString = "";
            }
        }

        private void BindParentEvent()
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                ddlparentEvent.DataTextField = "Name";
                ddlparentEvent.DataValueField = "ParentEventID";

                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                ddlparentEvent.DataSource = entities.SP_GetParentEvent(Type).ToList(); ;
                ddlparentEvent.DataBind();
                ddlparentEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                var compliance = entities.SP_GetParentEventToCompliance(ParentEventID, Type).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetEventData(ParentEventID, type, Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                //GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                //string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                //var Intermediate = entities.SP_GetIntermediateEvent(ParentEventID, Type).ToList();
                //gv2.DataSource = Intermediate;
                //gv2.DataBind();
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                Label lblIntermediateEventID = (Label)e.Row.FindControl("IntermediateEventID");
                if (lblIntermediateEventID.Text == "0")
                {
                    GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetCompliance(Parentid, SubEventID, Type).ToList();
                    if (Compliance.Count() > 0)
                    {
                        gv.DataSource = Compliance;
                        gv.DataBind();
                    }
                }
                else
                {
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                    int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    var Compliance = entities.SP_GetIntermediateSubEvent(IntermediateEventID, Parentid, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //Delete all event sequence

            int Type = 0;
            if (rblRelationship.SelectedValue == "0")
            {
                Type = 1;
            }
            if (rblRelationship.SelectedValue == "1")
            {
                Type = 2;
            }
            if (rblRelationship.SelectedValue == "2")
            {
                Type = 3;
            }
            if (rblRelationship.SelectedValue == "3")
            {
                Type = 4;
            }
            ComplianceDBEntities entities = new ComplianceDBEntities();
            {
                int ParentEvent = Convert.ToInt32(ddlparentEvent.SelectedValue);

                var ids = (from row in entities.EventSequences
                           where row.ParentEventID == ParentEvent
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventSequence prevmappedids = (from row in entities.EventSequences
                                                   where row.ID == entry
                                                   select row).FirstOrDefault();
                    entities.EventSequences.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }

           
            foreach (GridViewRow Eventrow in gvParentGrid.Rows)
            {
                int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                //var textbox = Eventrow.FindControl("txtEventSequence") as TextBox;
                //string ParentEventSequence = Convert.ToString(textbox.Text);
                string ParentEventSequence = "0";
                EventSequence ParentEvent = new EventSequence()
                {
                    ParentEventID = ParentEventID,
                    //SubEventID = SubEventID,
                    //ComplianceID = ComplinaceID,
                    SequenceID = ParentEventSequence,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = Convert.ToInt32(Session["userID"]),
                    EventType = "",
                    Type = Type,
                };
                Business.ComplianceManagement.CreateEventSequence(ParentEvent);

                GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;

                // Direct Compliance 
                foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                {
                    int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);
                    var textbox2 = EvenToCompliancerow.FindControl("txtEventComplianceSequence") as TextBox;
                    string evenToComplianceSequence = Convert.ToString(textbox2.Text);

                    EventSequence eventComplianceSequence = new EventSequence()
                    {
                        ParentEventID = ParentEventID,
                        ComplianceID = ComplinaceID,
                        SequenceID = evenToComplianceSequence,
                        IsActive = true,
                        CreatedDate = DateTime.Now,
                        CreatedBy = Convert.ToInt32(Session["userID"]),
                        EventType = "",
                        Type = Type,
                    };
                    Business.ComplianceManagement.CreateEventSequence(eventComplianceSequence);
                }

              
                GridView gvSubEvent = Eventrow.FindControl("gvChildGrid") as GridView;
                foreach (GridViewRow SubEventrow in gvSubEvent.Rows)
                {
                    Label lblIntermediateEventID = (Label)SubEventrow.FindControl("IntermediateEventID");

                    int IntermediateEventID = Convert.ToInt32(lblIntermediateEventID.Text);
                    if (lblIntermediateEventID.Text == "0")
                    {
                        // Subevent-> Compliance 
                        int SubEventID = Convert.ToInt32(SubEventrow.Cells[1].Text);
                        var textbox3 = SubEventrow.FindControl("txtSubEventSequence") as TextBox;
                        string SubEventSequence = Convert.ToString(textbox3.Text);

                        EventSequence subEventSequence = new EventSequence()
                        {
                            ParentEventID = ParentEventID,
                            SubEventID = SubEventID,
                            SequenceID = SubEventSequence,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedBy = Convert.ToInt32(Session["userID"]),
                            EventType = "",
                            Type = Type,
                        };
                        Business.ComplianceManagement.CreateEventSequence(subEventSequence);

                        GridView gvSubCompliance = SubEventrow.FindControl("gvComplianceGrid") as GridView;

                        foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);

                            var textbox1 = SubEventComplincerow.FindControl("txtSubEventComlianceSequence") as TextBox;
                            string SubEventComlianceSequence = Convert.ToString(textbox1.Text);

                            EventSequence subeventComplianceSequence = new EventSequence()
                            {
                                ParentEventID = ParentEventID,
                                SubEventID = SubEventID,
                                ComplianceID = ComplinaceID,
                                SequenceID = SubEventComlianceSequence,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                EventType = "",
                                Type = Type,
                            };
                            Business.ComplianceManagement.CreateEventSequence(subeventComplianceSequence);
                        }
                    }
                    else 
                    {
                        //Intermediatre-> Subevent-> Compliance
                        //int IntermediateEventID = Convert.ToInt32(intermediateEventrow.Cells[1].Text);

                        //var textbox4 = intermediateEventrow.FindControl("txtIntermediateEventSequence") as TextBox;
                        //string IntermediateEventSequence = Convert.ToString(textbox4.Text);

                        var textbox3 = SubEventrow.FindControl("txtSubEventSequence") as TextBox;
                        string SubEventSequence = Convert.ToString(textbox3.Text);
                        EventSequence intermediateEventSequence = new EventSequence()
                        {
                            ParentEventID = ParentEventID,
                            //SubEventID = SubEventID,
                            IntermediateEventID = IntermediateEventID,
                            SequenceID = SubEventSequence, //IntermediateEventSequence,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedBy = Convert.ToInt32(Session["userID"]),
                            EventType = "",
                            Type = Type,
                        };

                        Business.ComplianceManagement.CreateEventSequence(intermediateEventSequence);

                        GridView gvIntermediateSubEvent = SubEventrow.FindControl("gvIntermediateSubEventGrid") as GridView;

                        foreach (GridViewRow SubEventrow1 in gvIntermediateSubEvent.Rows)
                        {
                            int SubEventID = Convert.ToInt32(SubEventrow1.Cells[1].Text);

                            var textbox1 = SubEventrow1.FindControl("txtIntermediateSubEventSequence") as TextBox;
                            string SubEventSequence1 = Convert.ToString(textbox1.Text);

                            EventSequence subEventSequence = new EventSequence()
                            {
                                ParentEventID = ParentEventID,
                                IntermediateEventID = IntermediateEventID,
                                SubEventID = SubEventID,
                                SequenceID = SubEventSequence1,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                EventType = "",
                                Type = Type,
                            };
                            Business.ComplianceManagement.CreateEventSequence(subEventSequence);

                            GridView gvSubCompliance = SubEventrow1.FindControl("gvIntermediateComplainceGrid") as GridView;

                            foreach (GridViewRow SubEventComplincerow in gvSubCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(SubEventComplincerow.Cells[0].Text);

                                var textbox2 = SubEventComplincerow.FindControl("txtIntermediateComplianceSequence") as TextBox;
                                string SubEventComlianceSequence = Convert.ToString(textbox2.Text);

                                EventSequence subeventComplianceSequence = new EventSequence()
                                {
                                    ParentEventID = ParentEventID,
                                    IntermediateEventID = IntermediateEventID,
                                    SubEventID = SubEventID,
                                    ComplianceID = ComplinaceID,
                                    SequenceID = SubEventComlianceSequence,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    EventType = "",
                                    Type = Type,
                                };
                                Business.ComplianceManagement.CreateEventSequence(subeventComplianceSequence);
                            }
                        }
                    }
                }
            }
            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = "Save Successfully";
        }
        protected void rblRelationship_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                gvParentGrid.DataSource = null;
                gvParentGrid.DataBind();
                BindParentEvent();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        protected void ddlparentEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindParentEventData(Convert.ToInt32(ddlparentEvent.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                int Type = 0;
                if (rblRelationship.SelectedValue == "0")
                {
                    Type = 1;
                }
                if (rblRelationship.SelectedValue == "1")
                {
                    Type = 2;
                }
                if (rblRelationship.SelectedValue == "2")
                {
                    Type = 3;
                }
                if (rblRelationship.SelectedValue == "3")
                {
                    Type = 4;
                }

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentEventSelected(ParentEventID, Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();

                //if (ParentEvent.Count() > 0)
                //{
                //    System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                //    stringbuilder.Append(@"<table id='basic' width='100%' cellpadding='5' cellspacing='5'>");
                //    foreach (var item in ParentEvent)
                //    {
                //        stringbuilder.Append("<tr data-node-id=" + Convert.ToString(ParentEventID) + ">" +
                //              " <td class='locationheadLocationbg'>" + Convert.ToString(ParentEventID) + "</td>");
                //        stringbuilder.Append(" <td class='locationheadLocationbg'>" + Convert.ToString(item.Name) + "</td>");
                //        stringbuilder.Append("</tr>");

                //        //InterMediate and Sub Event
                //        var SubEvent = entities.SP_GetEventData(ParentEventID, "", Type).ToList();
                //        foreach (var item1 in SubEvent)
                //        {
                //            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item1.SubEventID) + " data-node-pid=" + Convert.ToString(ParentEventID) + " >" +
                //                                  " <td class='locationheadLocationbg'>" + Convert.ToString(item1.SubEventID) + "</td>");
                //            stringbuilder.Append(" <td class='locationheadLocationbg'>" + Convert.ToString(item1.Name) + "</td>");
                //            stringbuilder.Append(" <td class='txtbox'><input type='textbox' data-ParentEventId="+ Convert.ToString(item1.ParentEventID) + " data-intermediateEventId=" + Convert.ToString(item1.IntermediateEventID) + "  data-SubEventId=" + Convert.ToString(item1.SubEventID) + " id ='txtsubEvent" + Convert.ToString(item1.SubEventID) + "' runat = 'server' name ='txtsubEvent'" + Convert.ToString(item1.SubEventID) + "'  size='3' maxlength='3'/></td>");
                //            stringbuilder.Append("</tr>");

                //            //SubEvent Compliance
                //            var SubEventCompliance = entities.SP_GetCompliance(ParentEventID, item1.SubEventID, Type).ToList();
                //            foreach (var subEventCompliance in SubEventCompliance)
                //            {
                //                stringbuilder.Append("<tr data-node-id=" + Convert.ToString(subEventCompliance.ComplianceID) + " data-node-pid=" + Convert.ToString(item1.SubEventID) + " >" +
                //                    " <td class='locationheadLocationbg'>" + Convert.ToString(subEventCompliance.ComplianceID) + "</td>");
                //                stringbuilder.Append(" <td class='locationheadLocationbg'>" + Convert.ToString(subEventCompliance.Name) + "</td>");
                //                stringbuilder.Append(" <td class='txtbox'><input type='textbox' data-ParentEventId=" + Convert.ToString(ParentEventID) + " data-SubEventId=" + Convert.ToString(item1.SubEventID) + " data-SubEventComplianceId=" + Convert.ToString(subEventCompliance.ComplianceID) + " id ='txtsubEventCompliance" + Convert.ToString(subEventCompliance.ComplianceID) + "' runat = 'server' name ='txtsubEventCompliance'" + Convert.ToString(subEventCompliance.ComplianceID) + "'  size='3' maxlength='3'/></td>");
                //                stringbuilder.Append("</tr>");
                //            }
                //            //intermediateSubEvent
                //            var intermediateSubEvent = entities.SP_GetIntermediateSubEvent(item1.SubEventID, ParentEventID, Type).ToList();
                //            foreach (var interSubEvent in intermediateSubEvent)
                //            {
                //                stringbuilder.Append("<tr data-node-id=" + Convert.ToString(interSubEvent.SubEventID) + " data-node-pid=" + Convert.ToString(interSubEvent.ParentEventID) + " >" +
                //                    " <td class='locationheadLocationbg'>" + Convert.ToString(interSubEvent.SubEventID) + "</td>");
                //                stringbuilder.Append(" <td class='locationheadLocationbg'>" + Convert.ToString(interSubEvent.Name) + "</td>");
                //                stringbuilder.Append(" <td class='txtbox'><input type='textbox' data-ParentEventId=" + Convert.ToString(ParentEventID) + " data-intermediateEventId=" + Convert.ToString(item1.SubEventID) + " data-subEventId=" + Convert.ToString(interSubEvent.SubEventID) + " id ='txtintermediateSubEvent" + Convert.ToString(interSubEvent.SubEventID) + "' runat = 'server' name ='txtsubEventCompliance'" + Convert.ToString(interSubEvent.SubEventID) + "'  size='3' maxlength='3'/></td>");
                //                stringbuilder.Append("</tr>");
                //                //InterMediatesubEventCompliance Compliance
                //                var InterMediatesubEventCompliance = entities.SP_GetIntermediateCompliance(item.ParentEventID, item1.SubEventID, interSubEvent.SubEventID, Type).ToList();
                //                foreach (var interMediatesubEventCompliance in InterMediatesubEventCompliance)
                //                {
                //                    stringbuilder.Append("<tr data-node-id=" + Convert.ToString(interMediatesubEventCompliance.ComplianceID) + " data-node-pid=" + Convert.ToString(interSubEvent.SubEventID) + " >" +
                //                        " <td class='locationheadLocationbg'>" + Convert.ToString(interMediatesubEventCompliance.ComplianceID) + "</td>");
                //                    stringbuilder.Append(" <td class='locationheadLocationbg'>" + Convert.ToString(interMediatesubEventCompliance.Name) + "</td>");
                //                    stringbuilder.Append(" <td class='txtbox'><input type='textbox' data-ParentEventId=" + Convert.ToString(ParentEventID) + " data-intermediateEventId=" + Convert.ToString(item1.SubEventID) + " data-subEventId=" + Convert.ToString(interSubEvent.SubEventID) + " data-interMediatesubEventComplianceId=" + Convert.ToString(interMediatesubEventCompliance.ComplianceID) + "  id ='txtinterMediatesubEventCompliance" + Convert.ToString(interMediatesubEventCompliance.ComplianceID) + "' runat = 'server' name ='txtsubEventCompliance'" + Convert.ToString(interMediatesubEventCompliance.ComplianceID) + "'  size='3' maxlength='3'/></td>");
                //                    stringbuilder.Append("</tr>");
                //                }
                //            }
                //        }
                //    }
                //    stringbuilder.Append("</table>");
                //    DataString = stringbuilder.ToString().Trim();
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int Type = 0;
                    if (rblRelationship.SelectedValue == "0")
                    {
                        Type = 1;
                    }
                    if (rblRelationship.SelectedValue == "1")
                    {
                        Type = 2;
                    }
                    if (rblRelationship.SelectedValue == "2")
                    {
                        Type = 3;
                    }
                    if (rblRelationship.SelectedValue == "3")
                    {
                        Type = 4;
                    }
                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(ddlparentEvent.SelectedValue);
                    var Compliance = entities.SP_GetIntermediateCompliance(parentEventID,intermediateEventID, SubEventID, Type).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}