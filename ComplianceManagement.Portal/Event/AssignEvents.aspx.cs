﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Configuration;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class AssignEvents : System.Web.UI.Page
    {
        static int type = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "CustomerBranchName";

                BindTreeView(tvFilterLocation);
                BindUsers(ddlFilterUsers);
                //BindReviewer(ddlFilterUsers);
                //Session["EventRoleID"] = 10;
                BindEventClassification();
                //var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                //int EventOwnerRoleID = 0;
                //int EventReviewerRoleID = 0;
                //foreach (var ar in AssignedRoles)
                //{
                //    string name = ar.Name;
                //    if (name.Equals("Event Owner"))
                //    {
                //        EventOwnerRoleID = 10;
                //    }
                //    else if (name.Equals("Event Reviewer"))
                //    {
                //        EventReviewerRoleID = 11;
                //    }
                //}

                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 11)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 0)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //else
                //{
                //    Session["EventRoleID"] = 11;
                //}

                BindEventInstances(10);
            }
        }

        private void BindDepartment()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.Items.Clear();
                var deptdetails = CompDeptManagement.FillDepartment(customerID);
                ddlDepartment.DataSource = deptdetails;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEvents(int type, int CustomerBranchID, int pageIndex)
        {
            try
            {
                grdEventInstance.DataSource = null;
                grdEventInstance.DataBind();
                List<long> actIds = new List<long>();

                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }

                if (actIds.Count != 0)
                {

                    if (pageIndex == null || pageIndex.ToString() == "")
                    {
                        pageIndex = 0;
                    }

                    long EventClassificationID = Convert.ToInt64(ddlEventClassification.SelectedValue);

                    grdEventInstance.DataSource = EventManagement.GetAllEventsWithAssignedDays(type, CustomerBranchID, EventClassificationID, actIds);
                    grdEventInstance.PageIndex = pageIndex;
                    grdEventInstance.DataBind();
                }

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                upEventinstance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCheckdEvents(List<long> EventList)
        {
            try
            {
                ddlEvent.DataTextField = "Name";
                ddlEvent.DataValueField = "ID";

                ddlEvent.DataSource = EventManagement.GetCheckdEvent(EventList);
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, 0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindEventInstances(int EventRoleID, int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                try
                {
                    int branchID = -1;
                    if (tvFilterLocation.SelectedValue != "-1")
                    {
                        branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }

                    int userID = -1;
                    if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                    }

                    if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                    {
                        userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    }

                    string Role = Convert.ToString(AuthenticationHelper.Role);

                    var dataSource = EventManagement.GetAllAssignedEventsForCompanyAdmin(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));

                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.Role).ToList();
                        }
                        //if (ViewState["SortExpression"].ToString() == "StartDate")
                        //{
                        //    dataSource = dataSource.OrderBy(entry => entry.StartDate).ToList();
                        //}
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "CustomerBranchName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.CustomerBranchName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Name")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Name).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "UserName")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.UserName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Role")
                        {
                            dataSource = dataSource.OrderByDescending(entry => entry.Role).ToList();
                        }
                        //if (ViewState["SortExpression"].ToString() == "StartDate")
                        //{
                        //    dataSource = dataSource.OrderByDescending(entry => entry.StartDate).ToList();
                        //}
                        direction = SortDirection.Ascending;
                    }

                    grdEventList.PageIndex = pageIndex;
                    grdEventList.DataSource = dataSource;
                    grdEventList.DataBind();

                    if (isBranchChanged)
                    {
                        if (branchID != -1)
                        {
                            BindUsers(ddlFilterUsers, dataSource.Select(entry => entry.UserID.Value).Distinct().ToList());
                        }
                        else
                        {
                            BindUsers(ddlFilterUsers);
                        }

                        ddlFilterUsers.SelectedValue = "-1";
                    }

                    upEventInstanceList.Update();
                    ForceCloseFilterBranchesTreeView();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue.Length > 0)
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if (ddlFilterUsers.SelectedValue.Length > 0)
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }

                var assignmentList = EventManagement.GetAllAssignedEventsForCompanyAdmin(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }

                grdEventList.DataSource = assignmentList;
                grdEventList.DataBind();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindEventInstances(10, pageIndex: e.NewPageIndex);
        }

        private void BindAct()
        {
            try
            {
                rptActList.DataSource = CustomerBranchManagement.BindAct(); ;
                rptActList.DataBind();

                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                    if (!chkAct.Checked)
                    {
                        chkAct.Checked = true;
                    }
                }
                CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                actSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
            grdEventInstance.PageIndex = 0;
            BindEvents(type, CustomerBranchID, 0);
        }

        protected void upEventinstance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upEventInstanceList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                //SaveCheckedValues();
                //BindEvents(grdEventInstance.PageIndex);
                //PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTreeView(TreeView tvLocation)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvLocation.Nodes.Add(node);
                }

                tvLocation.ExpandAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindEventInstances(10);

        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                //users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindApproverUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                //users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindReviewer(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                ddlEventReviewer.DataTextField = "Name";
                ddlEventReviewer.DataValueField = "ID";
                ddlEventReviewer.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);
                // users.Insert(0, new { ID = -1, Name = ddlUserList == ddlEventReviewer ? "< Select >" : "< All >" });

                ddlEventReviewer.DataSource = users;
                ddlEventReviewer.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEventInstances(10);
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var EventList = EventManagement.GetAllAssignedInstancesByUser(Convert.ToInt32(ddlUsers.SelectedValue), Convert.ToInt64(UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue)).CustomerID));
                var UserEventList = EventList.GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();

                if (EventList.Count > 0)
                {
                    Session["EventLocation"] = null;
                    List<EventLocationProperty> EventLocationList = new List<EventLocationProperty>();
                    foreach (var eve in UserEventList)
                    {
                        var locationList = EventList.Where(entry => entry.ID == eve.ID).GroupBy(entry => entry.CustomerBranchID).Select(en => en.FirstOrDefault()).ToList();
                        string ids = string.Empty;
                        string names = string.Empty;
                        foreach (var stringnodes in locationList)
                        {
                            ids += stringnodes.CustomerBranchID + ",";
                            names += stringnodes.CustomerBranchName + ",";
                        }

                        EventLocationProperty EventLocation = new EventLocationProperty();
                        EventLocation.EventID = Convert.ToInt32(eve.ID);
                        EventLocation.BranchIds = ids;
                        EventLocation.BranchNames = names;
                        EventLocationList.Add(EventLocation);
                    }

                    Session["EventLocation"] = EventLocationList;
                }
                // BindEvents();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                Boolean IsIndustryMapped = EventManagement.CheckIndustryMapping(branchID1);

                if (IsIndustryMapped == true)
                {
                    List<Tuple<int, bool, bool>> eventList = new List<Tuple<int, bool, bool>>();
                    SaveCheckedValues();
                    eventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;
                    List<long> EventList = new List<long>();
                    for (int i = 0; i < eventList.Count; i++)
                    {
                        long EventID = eventList[i].Item1;
                        int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        bool FlgCheck = false;
                        FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                        if (FlgCheck == true)
                        {
                            continue;
                        }
                        EventList.Add(EventID);
                    }

                    //int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                    var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);

                    Session["exceptComplianceIDs"] = exceptComplianceIDs;
                    Session["EventList"] = EventList;
                    if (exceptComplianceIDs.Count > 0)
                    {
                        List<long> EventList1 = new List<long>();
                        EventList1 = (List<long>)Session["EventList"];
                        List<long> EvtList = EventList1.Select(i => (long)i).ToList();
                        BindCheckdEvents(EvtList);

                        grdNoAsignedComplinace.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, exceptComplianceIDs, EventList, branchID1, -1);
                        grdNoAsignedComplinace.DataBind();
                        upNotAssignedCompliances.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('open')", true);
                    }
                    else
                    {
                        List<string> eventlist = new List<string>();
                        for (int i = 0; i < eventList.Count; i++)
                        {
                            List<EventInstance> eventInstanceOwner = new List<EventInstance>();
                            List<EventAssignment> eventAssignmentOwner = new List<EventAssignment>();
                            int EventID = eventList[i].Item1;
                            long userID = Convert.ToInt64(ddlUsers.SelectedValue);
                            long ReviewerID = Convert.ToInt64(ddlEventReviewer.SelectedValue);
                            int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                            bool FlgCheck = false;
                            FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                            if (FlgCheck == true)
                            {
                                continue;
                            }

                            string eventname = EventManagement.GetEventName(EventID);

                            eventlist.Add(eventname);

                            EventInstance inst = new EventInstance();
                            inst.EventID = EventID;
                            inst.CustomerBranchID = branchID;
                            inst.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            inst.IsDeleted = false;
                            inst.StartDate = DateTime.Now;
                            inst.CreatedOn = DateTime.Now;
                            eventInstanceOwner.Add(inst);
                            Business.EventManagement.AddEventInstance(eventInstanceOwner);

                            EventAssignment assig = new EventAssignment();
                            assig.EventInstanceID = inst.ID;
                            assig.UserID = userID;
                            assig.Role = RoleManagement.GetByCode("EO").ID;
                            assig.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            assig.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            assig.UpdatedOn = DateTime.Now;
                            assig.IsDeleted = false;
                            assig.CreatedOn = DateTime.Now;
                            eventAssignmentOwner.Add(assig);

                            Business.EventManagement.AddEventAssignment(eventAssignmentOwner, branchID);

                        }

                        if (eventlist.Count > 0)
                        {
                            int customerID = -1;
                            customerID = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0;
                            string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                            var user = UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue));

                            var EscalationEmail = Properties.Settings.Default.EMailTemplate_AssignedEvents
                                                           .Replace("@User", user.FirstName + ' ' + user.LastName)
                                                           .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                           .Replace("@From", ReplyEmailAddressName);
                            StringBuilder details = new StringBuilder();

                            for (int i = 0; i < eventlist.Count; i++)
                            {
                                details.AppendLine(Properties.Settings.Default.EMailTemplate_AssignedEventsRows
                                    .Replace("@Branch", tbxBranch.Text)
                                    .Replace("@eventName", eventlist[i].ToString()));

                            }
                            string message = EscalationEmail.Replace("@User", "User").Replace("@Details", details.ToString());
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Reminder: List of 'ASSIGNED' events.", message);

                            var StatutoryChecklist = EventManagement.GetStatutoryChecklistCompliance(EventClassification, EventList, branchID1);

                            if(StatutoryChecklist.Count >0)
                            {
                                EventManagement.ChengeFlagGenerateSchedule(StatutoryChecklist, branchID1);
                            }
                        }

                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEventDialog\").dialog('close');", true);
                        BindEventInstances(10);
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please assign industry to selected location before event assignment";
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnDvNotAssignedClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        protected void btnComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divNotAssignedCompliances", "$(\"#divNotAssignedCompliances\").dialog('close')", true);

                lblBranch.Text = tvBranches.SelectedNode.Value.ToString();
                txtLocation.Text = tbxBranch.Text;
                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);
                BindApproverUsers(ddlFilterApprover);
                BindDepartment();
                upCompliance.Update();

                List<long> lstComplaince = new List<long>();
                List<long> EventList = new List<long>();
                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);

                lstComplaince = (List<long>)Session["exceptComplianceIDs"];
                EventList = (List<long>)Session["EventList"];
                int EvetID = Convert.ToInt32(ddlEvent.SelectedValue);
                int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                gvComplianceListAssign.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, lstComplaince, EventList, branchID1, EvetID);
                gvComplianceListAssign.DataBind();
                upCompliance.Update();


                //DateTime date = DateTime.Now;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                // upCompliance.Update();

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divAssignedCompliances", "$(\"#divAssignedCompliances\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        List<Tuple<int, string>> _valueList = new List<Tuple<int, string>>();
        private List<Tuple<int, string>> getCheckedNodes(TreeNodeCollection _parentNodeList)
        {
            foreach (TreeNode item in _parentNodeList)
            {
                if (item.Checked)
                {
                    _valueList.Add(new Tuple<int, string>(Convert.ToInt32(item.Value), item.Text));
                }

                if (item.ChildNodes.Count > 0)
                {
                    getCheckedNodes(item.ChildNodes);
                }
            }
            return _valueList;
        }

        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }
                //Session["EventLocation"] = null;
                ViewState["CHECKED_ITEMS"] = null;
                txtactList.Text = "< Select >";

                ddlFilterPerformer.SelectedValue = "-1";
                ddlFilterReviewer.SelectedValue = "-1";
                ddlFilterApprover.SelectedValue = "-1";
                tbxStartDate.Text = "";

                BindAct();
                BindUsers(ddlUsers);
                BindReviewer(ddlEventReviewer);
                tbxBranch.Text = "< Select >";
                //int CustomerBranchID = -1; // Convert.ToInt32(tvBranches.SelectedNode.Value);
                //BindEvents(type, CustomerBranchID, 0);

                grdEventInstance.DataSource = null;
                grdEventInstance.DataBind();

                ddlUsers.SelectedValue = "-1";
                ddlEventReviewer.SelectedValue = "-1";

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEventDialog\").dialog('open');", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                DateTime date = DateTime.Now;
                //tbxStartDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                //setDateToGridView();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ForceCloseFilterBranchesTreeView();
                upEventinstance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventInstance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();
                grdEventInstance.PageIndex = e.NewPageIndex;
                grdEventInstance.DataBind();
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                BindEvents(type, CustomerBranchID, e.NewPageIndex);
                PopulateCheckedValues();
                //if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
                //{
                //    setDateToGridView();
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<Tuple<int, bool, bool>> EventList = new List<Tuple<int, bool, bool>>();

                int index = -1;
                foreach (GridViewRow gvrow in grdEventInstance.Rows)
                {
                    index = Convert.ToInt32(grdEventInstance.DataKeys[gvrow.RowIndex].Value);

                    CheckBox chkEvent = ((CheckBox)gvrow.FindControl("chkEvent"));
                    //CheckBox chkEventReviewer = ((CheckBox)gvrow.FindControl("chkEvent"));
                    //string startdate = Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString();
                    if (ViewState["CHECKED_ITEMS"] != null)
                        EventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;

                    //  if (chkEvent.Checked || chkEventReviewer.Checked)
                    //{
                    if (chkEvent.Checked)
                    {
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        //EventList.Add(new Tuple<int,string>(index,Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString()));
                        if (data != null)
                        {
                            EventList.Remove(data);
                            EventList.Add(new Tuple<int, bool, bool>(index, chkEvent.Checked, chkEvent.Checked));
                        }
                        else
                        {
                            EventList.Add(new Tuple<int, bool, bool>(index, chkEvent.Checked, chkEvent.Checked));
                        }
                    }
                    else
                    {
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                            EventList.Remove(data);
                    }
                }

                if (EventList != null && EventList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = EventList;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                //List<Tuple<int, bool, bool,string>> EventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool,string>>;
                List<Tuple<int, bool, bool>> EventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;
                if (EventList != null && EventList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdEventInstance.Rows)
                    {
                        int index = Convert.ToInt32(grdEventInstance.DataKeys[gvrow.RowIndex].Value);
                        var data = EventList.Where(entry => entry.Item1 == index).FirstOrDefault();
                        if (data != null)
                        {
                            CheckBox eventchek = (CheckBox)gvrow.FindControl("chkEvent");
                            //CheckBox chkEventReviewer = ((CheckBox)gvrow.FindControl("chkEvent"));
                            //TextBox txtStartDate = (TextBox)gvrow.FindControl("txtStartDate");
                            eventchek.Checked = data.Item2;
                            //chkEventReviewer.Checked = data.Item3;
                            //txtStartDate.Text = data.Item4;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEventInstance__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int branchID = -1;
                    if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                    {
                        branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    }
                    long eventID = Convert.ToInt64(grdEventInstance.DataKeys[e.Row.RowIndex].Values[0].ToString());
                    List<EventAssignment> AssignedEvents = EventManagement.GetAssignedEventsByEvent(eventID, branchID);

                    //DateTime startDate = DateTime.Now;
                    //TextBox txtStartDate = (TextBox)e.Row.FindControl("txtStartDate");
                    //txtStartDate.Text = startDate.ToString("dd-MM-yyyy");
                    CheckBox chkEvent = (CheckBox)e.Row.FindControl("chkEvent");
                    // CheckBox chkEventReviewer = (CheckBox)e.Row.FindControl("chkEventReviewer");

                    if (AssignedEvents != null)
                    {

                        EventAssignment AsOwner = AssignedEvents.Where(entry => entry.Role == 10).FirstOrDefault();
                        EventAssignment AsReviewer = AssignedEvents.Where(entry => entry.Role == 11).FirstOrDefault();

                        if (AsOwner != null)
                        {
                            User user = UserManagement.GetByID(Convert.ToInt32(AsOwner.UserID));
                            chkEvent.Checked = true;
                            chkEvent.Enabled = false;
                            chkEvent.ToolTip = "This Eevent is already assigned to " + user.FirstName + " " + user.LastName + ".";
                        }
                        //if (AsReviewer != null)
                        //{
                        //    User user = UserManagement.GetByID(Convert.ToInt32(AsReviewer.UserID));
                        //    chkEventReviewer.Checked = true;
                        //    chkEventReviewer.Enabled = false;
                        //    chkEventReviewer.ToolTip = "This event is already assigned to " + user.FirstName + " " + user.LastName + ".";
                        //}

                    }

                    //CheckBox chekEventReviewerHeader = (CheckBox)grdEventInstance.HeaderRow.FindControl("chekEventReviewerHeader");
                    // chkEventReviewer.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + chekEventReviewerHeader.ClientID + "')");

                    CheckBox headerchk = (CheckBox)grdEventInstance.HeaderRow.FindControl("chkEventCheck");
                    chkEvent.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "')");

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventInstance_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("LOCATION"))
                {
                    ViewState["EventID"] = e.CommandArgument;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenLocationDialog", "$(\"#divGridRowLocation\").dialog('open');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventInstance_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceCatagoryID = Convert.ToInt32(grdEventInstance.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.GetByType(0, complianceCatagoryID);
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }


                foreach (DataControlField field in grdEventInstance.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdEventInstance.Columns.IndexOf(field);
                    }
                }


                SaveCheckedValues();
                grdEventInstance.DataSource = ComplianceRoleMatrixList;
                grdEventInstance.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventInstance_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                type = EventManagement.GetCompanyType(Convert.ToInt32(tvBranches.SelectedNode.Value));

                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
                TreeviewTextHighlightrd(Convert.ToInt32(tvBranches.SelectedNode.Value));
                int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                grdEventInstance.PageIndex = 0;
                BindEvents(type, CustomerBranchID, 0);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void TreeviewTextHighlightrd(int? branch)
        {
            foreach (TreeNode node in tvBranches.Nodes)
            {
                var list = GetChildren(node);
                List<TreeNode> tn = list.Where(nd => nd.Text.Contains("<div")).ToList();
                if (tn != null)
                {
                    foreach (TreeNode t in tn)
                    {
                        t.Text = Regex.Replace(t.Text, "<.*?>", string.Empty);
                    }

                }

                TreeNode td = null;
                if (branch != null)
                    td = list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault();

                if (td != null)
                    list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault().Text = "<div style=\"font-weight:bold\">" + td.Text + "</div>";
            }

        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForEvent(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tbxBranch.Text = "< Select >";
                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationNonSecretrial()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForNonSecretrialEvent(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tbxBranch.Text = "< Select >";
                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlEventReviewer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void chkEventCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdEventInstance.HeaderRow.FindControl("chkEventCheck");
            foreach (GridViewRow row in grdEventInstance.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkEvent");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
                int branchID = -1;
                if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                {
                    branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }
                long eventID = Convert.ToInt64(grdEventInstance.DataKeys[row.RowIndex].Values[0].ToString());
                List<EventAssignment> AssignedEvents = EventManagement.GetAssignedEventsByEvent(eventID, branchID);
                CheckBox chkEvent = (CheckBox)row.FindControl("chkEvent");
                // CheckBox chkEventReviewer = (CheckBox)e.Row.FindControl("chkEventReviewer");

                if (AssignedEvents != null)
                {

                    EventAssignment AsOwner = AssignedEvents.Where(entry => entry.Role == 10).FirstOrDefault();
                    EventAssignment AsReviewer = AssignedEvents.Where(entry => entry.Role == 11).FirstOrDefault();

                    if (AsOwner != null)
                    {
                        User user = UserManagement.GetByID(Convert.ToInt32(AsOwner.UserID));
                        chkEvent.Checked = true;
                        chkEvent.Enabled = false;
                        chkEvent.ToolTip = "This Eevent is already assigned to " + user.FirstName + " " + user.LastName + ".";
                    }
                }
            }
        }

        protected void grdNoAsignedComplinace_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdNoAsignedComplinace.PageIndex = e.NewPageIndex;
            List<Tuple<int, bool, bool>> eventList = new List<Tuple<int, bool, bool>>();
            SaveCheckedValues();
            eventList = ViewState["CHECKED_ITEMS"] as List<Tuple<int, bool, bool>>;

            List<long> EventList = new List<long>();
            for (int i = 0; i < eventList.Count; i++)
            {
                int EventID = eventList[i].Item1;
                int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                bool FlgCheck = false;
                FlgCheck = EventManagement.CheckEventAssigned(EventID, branchID);

                if (FlgCheck == true)
                {
                    continue;
                }
                EventList.Add(EventID);
            }

            int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
            int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

            var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);
            grdNoAsignedComplinace.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, exceptComplianceIDs, EventList, branchID1, -1);
            grdNoAsignedComplinace.DataBind();
            upNotAssignedCompliances.Update();
        }
        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void SaveComplianceList_Click(object sender, EventArgs e)
        {
            try
            {
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();

                foreach (GridViewRow item in gvComplianceListAssign.Rows)
                {
                    string ComplianceID = (item.FindControl("lblID") as Label).Text;

                    int DepartmentID = -1;
                    if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                    {
                        if (ddlDepartment.SelectedValue != "-1")
                        {
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                        }
                    }

                    CheckBox chkAssign = (CheckBox)item.FindControl("chkCompliance");
                    if (chkAssign.Checked)
                    {
                        ComplianceInstance instance = new ComplianceInstance();
                        instance.ComplianceId = Convert.ToInt64(ComplianceID);
                        instance.CustomerBranchID = Convert.ToInt32(lblBranch.Text);
                        instance.ScheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        if (DepartmentID == -1 || DepartmentID == 0)
                        {
                            instance.DepartmentID = null;
                        }
                        else
                        {
                            instance.DepartmentID = DepartmentID;
                        }

                        if (ddlFilterPerformer.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment = new ComplianceAssignment();
                            assignment.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                            assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                        }
                        if (ddlFilterReviewer.SelectedValue.ToString() != "-1")
                        {

                            ComplianceAssignment assignment1 = new ComplianceAssignment();
                            assignment1.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                            assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                        }

                        if (ddlFilterApprover.SelectedValue.ToString() != "-1")
                        {
                            ComplianceAssignment assignment2 = new ComplianceAssignment();
                            assignment2.UserID = Convert.ToInt32(ddlFilterApprover.SelectedValue);
                            assignment2.RoleID = RoleManagement.GetByCode("APPR").ID;
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment2));
                        }
                    }
                }
                if (assignments.Count != 0)
                {
                    Business.ComplianceManagement.CreateInstancesEventBased(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    GetAssignCompliance(-1);
                    ddlEvent.SelectedValue = "-1";
                    upCompliance.Update();

                    if (gvComplianceListAssign.Rows.Count <= 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignedCompliances\").dialog('close');", true);
                    }
                }

                ddlFilterPerformer.SelectedValue = "-1";
                ddlFilterReviewer.SelectedValue = "-1";
                ddlFilterApprover.SelectedValue = "-1";
                tbxStartDate.Text = "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);
                GetAssignCompliance(EventID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void GetAssignCompliance(int EventID)
        {
            try
            {
                int branchID1 = Convert.ToInt32(tvBranches.SelectedNode.Value);
                var EventList = (List<long>)Session["EventList"];
                int EventClassification = Convert.ToInt32(ddlEventClassification.SelectedValue);

                var exceptComplianceIDs = EventManagement.CheckAllEventComplianceNotAssigned(EventClassification, EventList, branchID1);

                Session["exceptComplianceIDs"] = exceptComplianceIDs;
                var lstComplaince = (List<long>)Session["exceptComplianceIDs"];

                gvComplianceListAssign.DataSource = EventManagement.GetAllNotAssignedComplinceListForEvent(EventClassification, lstComplaince, EventList, branchID1, EventID);
                gvComplianceListAssign.DataBind();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkComplianceCheck_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)gvComplianceListAssign.HeaderRow.FindControl("chkComplianceCheck");
            foreach (GridViewRow row in gvComplianceListAssign.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkCompliance");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }

        private void BindEventClassification()
        {
            try
            {
                ddlEventClassification.DataTextField = "Name";
                ddlEventClassification.DataValueField = "ID";
                ddlEventClassification.DataSource = EventManagement.GetEventClassification();
                ddlEventClassification.DataBind();
                ddlEventClassification.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlEventClassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //int CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                tvBranches.Nodes.Clear();
                if (ddlEventClassification.SelectedValue == "1")
                {
                    BindLocation();
                }
                else
                {
                    BindLocationNonSecretrial();
                }
                //BindEvents(type, CustomerBranchID, 0);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}