﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
   
    public partial class EventDetails : System.Web.UI.Page
    {
        static string eventType;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    int eventId = Convert.ToInt32(Session["eventId"]);
                    int CustomerBranchID = Convert.ToInt32(Session["CustomerBranchID"]);

                    eventType = Convert.ToString(Session["eventType"]);

                    if(eventType == "AssignedEvent")
                    {
                        btn_BackAssigned.Visible = true;
                        btn_BackActivated.Visible = false;
                    }
                    if (eventType == "AciveEvent")
                    {
                        btn_BackActivated.Visible = true;
                        btn_BackAssigned.Visible = false;
                    }
                    lblEventName.Text = getEventName(eventId);

                    BindEventComplianceDetails(eventId, CustomerBranchID);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }

        private string getEventName(int EventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var eventName = (from row in entities.Events
                                 where row.ID == EventID
                                 select row).SingleOrDefault();

                return eventName.Name;
                // UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return "";
        }

        //protected void btnBack_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Session["ViewDetailsBack"] = "true"; 
        //        Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private void BindEventComplianceDetails(int ParentEventID,int CustomerBranchID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                entities.Database.CommandTimeout = 180;

                string EventClassification=Convert.ToString(EventManagement.GetEventClassification(ParentEventID));
                var List = entities.SP_GetEventComplianceDetail(ParentEventID, CustomerBranchID, EventClassification).ToList();
                grdEventDetail.DataSource = List;
                grdEventDetail.DataBind();
                upEventDetail.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}