﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskDocumentList : System.Web.UI.Page
    {
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected static List<Int32> Taskroles;
        protected string Reviewername;
        protected string Performername;
        protected string InternalReviewername;
        protected string InternalPerformername;
        static int CustomerID;
        static int UserRoleID;
        public static string subTaskDocViewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PerformerFlag = false;
                    ReviewerFlag = false;
                    ddlAct.Enabled = true;

                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlDocType.SelectedValue = "2";
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("1"));
                    }
                    else
                    {
                        ddlDocType.SelectedValue = "1";
                    }
                    CustomerID = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                    UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);                   
                    BindStatus();
                    BindLocationFilter();                    
                    Taskroles = TaskManagment.GetAssignedTaskUserRole(AuthenticationHelper.UserID);
                    //bool ismanagement = false;
                    //if (AuthenticationHelper.Role.Contains("MGMT"))
                    //{
                    //    ismanagement = true;
                    //    PerformerFlag = true;
                    //}
                    //if (ddlDocType.SelectedValue == "1") //1--Statutory
                    //{
                    //    ddlAct.ClearSelection();
                    //    BindTypes(ismanagement);
                    //    BindCategories(ismanagement);
                    //}
                    //else if (ddlDocType.SelectedValue == "2") //2--Internal
                    //{
                    //    ddlAct.ClearSelection();
                    //    BindTypesInternal(ismanagement);
                    //    BindCategoriesInternal(ismanagement);
                    //}
                   
                    //BindActList(ismanagement);
                    //BindComplianceSubTypeList(ismanagement);

                    if (Taskroles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (Taskroles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;                        
                    }
                    btnDownload.Visible = false;
                    bool ismanagement = false;
                    if (AuthenticationHelper.Role.Contains("MGMT"))
                    {
                       ismanagement = true;                        
                    }
                    BindPerfRevUserDropDown(ismanagement);
                    btnSearch_Click(sender, e);

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        private void BindActList(bool ismgmt)
        {
            try
            {
                List<Sp_StatuoryTaskAct_Result> ActList = new List<Sp_StatuoryTaskAct_Result>();
                ddlAct.Items.Clear();
                if (ismgmt)
                {
                    ActList = TaskManagment.GetActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"MGMT");
                }
                else
                {
                    ActList = TaskManagment.GetActsByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                }
               
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypesInternal(bool ismgmt)
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                if (ismgmt)
                {
                    var TypleList = TaskManagment.GetAllAssignedInternalType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"MGMT");
                    ddlType.DataSource = TypleList;
                    ddlType.DataBind();
                }
                else
                {
                    var TypleList = TaskManagment.GetAllAssignedInternalType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                    ddlType.DataSource = TypleList;
                    ddlType.DataBind();
                }                
                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypes(bool ismgmt)
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                if (ismgmt)
                {
                    var TypeList = TaskManagment.GetAllAssignedType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"MGMT");
                    ddlType.DataSource = TypeList;
                    ddlType.DataBind();
                }
                else
                {
                    var TypeList = TaskManagment.GetAllAssignedType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                    ddlType.DataSource = TypeList;
                    ddlType.DataBind();
                }

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCategories(bool ismgmt)
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                if (ismgmt)
                {
                    var CatList = TaskManagment.GetAllTaskAssignedCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"MGMT");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                else
                {
                    var CatList = TaskManagment.GetAllTaskAssignedCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();

                }                
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCategoriesInternal(bool ismgmt)
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";            
                if (ismgmt)
                {
                    var CatList = TaskManagment.GetAllTaskAssignedInternalCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"MGMT");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                else
                {
                    var CatList = TaskManagment.GetAllTaskAssignedInternalCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                    ddlCategory.DataSource = CatList;
                    ddlCategory.DataBind();
                }
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindComplianceSubTypeList(bool ismgmt)
        {
            try
            {
                ddlComplianceSubType.Items.Clear();
                List<Sp_StatuoryTaskSubType_Result> ComplianceSubTypeList = new List<Sp_StatuoryTaskSubType_Result>();

                if (ismgmt)
                {
                    ComplianceSubTypeList = TaskManagment.GetSubTypeByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID) , AuthenticationHelper.UserID,"MGMT");
                }
                else
                {
                    ComplianceSubTypeList = TaskManagment.GetSubTypeByUserID(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID,"PR");
                }
                 
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();
                ddlComplianceSubType.Items.Insert(0, new ListItem("Select Sub Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindPerfRevUserDropDown(bool ismgmt)
        {
            try
            {
                int tasktype = -1;
                int roleID = 3;
                roleID = Convert.ToInt32(ViewState["selectedRole"]);
                ddlPerfRevUser.DataSource = null;
                ddlPerfRevUser.DataBind();
                ddlPerfRevUser.DataTextField = "Name";
                ddlPerfRevUser.DataValueField = "ID";
                if (ddlDocType.SelectedValue == "1") //1--Statutory
                {
                    tasktype = 1;
                }
                else if (ddlDocType.SelectedValue == "2") //2--Internal
                {
                    tasktype = 2;
                }
                if (ismgmt)
                {
                    var TypeList = TaskManagment.GetTaskStatutoryPerformerReviewerList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, roleID, tasktype, "MGMT");
                    ddlPerfRevUser.DataSource = TypeList;
                    ddlPerfRevUser.DataBind();
                }
                else
                {
                    var TypeList = TaskManagment.GetTaskStatutoryPerformerReviewerList(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, roleID,tasktype, "PR");
                    ddlPerfRevUser.DataSource = TypeList;
                    ddlPerfRevUser.DataBind();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void FillTaskDocuments()
        {
            try
            {                
                if (AuthenticationHelper.Role.Contains("MGMT"))
                {                   
                    PerformerFlag = true;
                }               
                DocumentFilterNewStatus Status = (DocumentFilterNewStatus) Convert.ToInt16(ddlStatus.SelectedIndex);
                String location = tvFilterLocation.SelectedNode.Text;
                string StringType = "";
                DateTime? DateFrom = null;
                DateTime? DateTo = null;

                if (txtStartDate.Text != "")
                {
                    DateFrom = GetDate(txtStartDate.Text);
                }
                if (txtEndDate.Text != "")
                {
                    DateTo = GetDate(txtEndDate.Text);
                }
                StringType = txtSearchType.Text;
                Session["TotalRows"] = 0;
                if (PerformerFlag)
                {
                    var ComplianceDocs = TaskManagment.GetFilteredTaskDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, Status, location, DateFrom, DateTo, StringType, ddlDocType.SelectedValue);
                    grdPerformerTaskDocument.DataSource = ComplianceDocs;
                    Session["TotalRows"] = ComplianceDocs.Count;
                    grdPerformerTaskDocument.DataBind();
                    grdPerformerTaskDocument.Visible = true;
                    grdReviewerTaskDocument.Visible = false;
                }
                else if (ReviewerFlag)
                {
                    var ComplianceDocs = TaskManagment.GetFilteredTaskDocuments(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 4, Status, location, DateFrom, DateTo, StringType, ddlDocType.SelectedValue);
                    grdReviewerTaskDocument.DataSource = ComplianceDocs;
                    grdReviewerTaskDocument.DataBind();
                    Session["TotalRows"] = ComplianceDocs.Count;

                    grdPerformerTaskDocument.Visible = false;
                    grdReviewerTaskDocument.Visible = true;
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
     
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected string GetPerformer(long TaskScheduledOnID, long TaskInstanceID, byte TaskType)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string result = "";
                result = TaskManagment.GetTaskAssignedUser(customerID, Convert.ToInt32(TaskType), TaskInstanceID, TaskScheduledOnID, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetReviewer(long TaskScheduledOnID, long TaskInstanceID, byte TaskType)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                string result = "";
                result = TaskManagment.GetTaskAssignedUser(customerID, Convert.ToInt32(TaskType), TaskInstanceID, TaskScheduledOnID, 4);
                Reviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }       
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
            FillTaskDocuments();
            ViewState["selectedRole"] = 4;
        }
       
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
            FillTaskDocuments();
            ViewState["selectedRole"] = 3;
        }
        private void BindStatus()
        {
            try
            {
                foreach (DocumentFilterNewStatus r in Enum.GetValues(typeof(DocumentFilterNewStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(DocumentFilterNewStatus), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvFilterLocation.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);
                var LocationList = TaskManagment.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);                    
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDocumentDownload_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", string.Format("initializeDatePicker12(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", "initializeDatePicker12(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bool ismanagement = false;                              
                if (AuthenticationHelper.Role.Contains("MGMT"))
                {
                    ismanagement = true;                    
                }                
                if (ddlDocType.SelectedValue == "1") //1--Statutory
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    BindTypes(ismanagement);
                    BindCategories(ismanagement);                    
                }
                else if (ddlDocType.SelectedValue == "2") //2--Internal
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = false;
                    Panelsubtype.Enabled = false;
                    DivAct.Attributes.Add("disabled", "true");
                    BindTypesInternal(ismanagement);
                    BindCategoriesInternal(ismanagement);                   
                }
                BindPerfRevUserDropDown(ismanagement);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillTaskDocuments();
        }
        private void ClearAdvanceSearchModalControls()
        {
            try
            {
                ddlType.ClearSelection();
                ddlCategory.ClearSelection();
                ddlAct.ClearSelection();
                ddlComplianceSubType.ClearSelection();
                ddlPerfRevUser.ClearSelection();

                txtSearchType.Text = "";
                txtStartDate.Text = "";
                txtEndDate.Text = "";


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceList_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = "";
                divAdvSearch.Visible = false;
                ClearAdvanceSearchModalControls();

                FillTaskDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                int SubCategory = 0;
                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    if (ddlType.SelectedValue != "-1")
                    {
                        type = ddlType.SelectedItem.Text.Length;

                        if (type >= 20)
                            lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                    }
                }
                if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
                {
                    if (ddlCategory.SelectedValue != "-1")
                    {
                        Category = ddlCategory.SelectedItem.Text.Length;

                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Category >= 20)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                        }
                        else
                        {
                            if (Category >= 20)
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    if (ddlAct.SelectedValue != "-1")
                    {
                        Act = ddlAct.SelectedItem.Text.Length;

                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Act >= 30)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                        }
                        else
                        {
                            if (Act >= 30)
                                lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue))
                {
                    if (ddlComplianceSubType.SelectedValue != "-1")
                    {
                        SubCategory = ddlComplianceSubType.SelectedItem.Text.Length;

                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (SubCategory >= 30)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category : </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                        }
                        else
                        {
                            if (SubCategory >= 30)
                                lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                        }
                    }
                }
                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (ddlPerfRevUser.SelectedValue != "-1" && ddlPerfRevUser.SelectedValue != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>User: </b>" + ddlPerfRevUser.SelectedItem.Text;
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type To Search: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                FillTaskDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void CmpSaveCheckedValues()
        {
            try
            {
                ArrayList userdetails = new ArrayList();
                int index = -1;

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        foreach (GridViewRow gvrow in grdPerformerTaskDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfile");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkTask")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }

                    else if (ReviewerFlag)
                    {
                        foreach (GridViewRow gvrow in grdReviewerTaskDocument.Rows)
                        {
                            ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewer");
                            index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                            bool result = ((CheckBox) gvrow.FindControl("chkReviewerTask")).Checked;

                            // Check in the Session
                            if (ViewState["checkedCompliances"] != null)
                                userdetails = (ArrayList) ViewState["checkedCompliances"];
                            if (result)
                            {
                                if (!userdetails.Contains(index))
                                    userdetails.Add(index);
                            }
                            else
                                userdetails.Remove(index);
                        }
                        if (userdetails != null && userdetails.Count > 0)
                            ViewState["checkedCompliances"] = userdetails;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void CmpPopulateCheckedValues()
        {
            try
            {
                ArrayList complianceDetails = (ArrayList) ViewState["checkedCompliances"];

                if (complianceDetails != null && complianceDetails.Count > 0)
                {
                    if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                    {
                        if (PerformerFlag)
                        {
                            foreach (GridViewRow gvrow in grdPerformerTaskDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfile");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkTask");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                        else if (ReviewerFlag)
                        {
                            foreach (GridViewRow gvrow in grdReviewerTaskDocument.Rows)
                            {
                                ImageButton lblDownLoadfile = (ImageButton) gvrow.FindControl("lblDownLoadfileReviewer");
                                int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                                if (complianceDetails.Contains(index))
                                {
                                    CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkReviewerTask");
                                    myCheckBox.Checked = true;
                                }
                            }
                        }
                    }
                    if (complianceDetails.Count > 0)
                    {
                        lblTotalSelected.Text = complianceDetails.Count + " Selected";
                        btnDownload.Visible = true;
                    }
                    else if (complianceDetails.Count == 0)
                    {
                        lblTotalSelected.Text = "";
                        btnDownload.Visible = false;
                    }
                }
                else
                {
                    lblTotalSelected.Text = "";
                    btnDownload.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkTaskHeader_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (PerformerFlag)
                {
                    CheckBox ChkBoxHeader = (CheckBox) grdPerformerTaskDocument.HeaderRow.FindControl("chkTaskHeader");

                    foreach (GridViewRow row in grdPerformerTaskDocument.Rows)
                    {
                        CheckBox chkTask = (CheckBox) row.FindControl("chkTask");

                        if (ChkBoxHeader.Checked)
                            chkTask.Checked = true;
                        else
                            chkTask.Checked = false;
                    }
                }

                else if (ReviewerFlag)
                {
                    CheckBox ChkBoxHeader = (CheckBox) grdReviewerTaskDocument.HeaderRow.FindControl("chkReviewerTaskHeader");

                    foreach (GridViewRow row in grdReviewerTaskDocument.Rows)
                    {
                        CheckBox chkReviewerTask = (CheckBox) row.FindControl("chkReviewerTask");

                        if (ChkBoxHeader.Checked)
                            chkReviewerTask.Checked = true;
                        else
                            chkReviewerTask.Checked = false;
                    }
                }

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void chkTask_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;
                if (PerformerFlag)
                {
                    CheckBox ChkBoxHeader = (CheckBox) grdPerformerTaskDocument.HeaderRow.FindControl("chkTaskHeader");

                    foreach (GridViewRow row in grdPerformerTaskDocument.Rows)
                    {
                        CheckBox chkTask = (CheckBox) row.FindControl("chkTask");

                        if (chkTask.Checked)
                            Count++;
                    }
                    if (Count == grdPerformerTaskDocument.Rows.Count)
                        ChkBoxHeader.Checked = true;
                    else
                        ChkBoxHeader.Checked = false;
                }

                else if (ReviewerFlag)
                {
                    CheckBox ChkBoxHeader = (CheckBox) grdReviewerTaskDocument.HeaderRow.FindControl("chkReviewerTaskHeader");

                    foreach (GridViewRow row in grdReviewerTaskDocument.Rows)
                    {
                        CheckBox chkReviewerTask = (CheckBox) row.FindControl("chkReviewerTask");

                        if (chkReviewerTask.Checked)
                            Count++;
                    }
                    if (Count == grdReviewerTaskDocument.Rows.Count)
                        ChkBoxHeader.Checked = true;
                    else
                        ChkBoxHeader.Checked = false;
                }
                ShowSelectedRecords(sender, e);
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            CmpSaveCheckedValues();

            CmpPopulateCheckedValues();
        }

        public static TaskInstanceTransactionView GetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.TaskInstanceTransactionViews
                                        where row.TaskScheduledOnID == ScheduledOnID
                                        select row).FirstOrDefault();

                if (ScheduleOnData != null)
                {
                    return ScheduleOnData;
                }
                else
                {
                    return null;
                }
               
            }
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList) ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2") 
                                {
                                    var ComplianceData = GetForMonth(ScheduledOnID);
                                    List<GetTaskDocumentView> fileData = TaskManagment.GetTaskRelatedFileData(ScheduledOnID).ToList();// DocumentManagement.GetFileData1(ScheduledOnID);

                                    //craeted subdirectory
                                    //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                                    //string directoryName = ComplianceData.TaskDescription + "/" + ComplianceData.ForMonth;

                                    var CustomerBranch = CustomerBranchManagement.GetByID(ComplianceData.CustomerBranchID).Name; 
                                    string directoryName = CustomerBranch + "/" + ComplianceData.TaskTitle + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }                                                           
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        
                        byte[] data = zipMs.ToArray();                      
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=TaskDocument.zip");
                        Response.BinaryWrite(data);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (!IsValid()) { return; };

                CmpSaveCheckedValues();

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdPerformerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdPerformerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                    else if (ReviewerFlag)
                    {
                        grdReviewerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                //Reload the Grid
                FillTaskDocuments();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (PerformerFlag)
                    {
                        grdPerformerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdPerformerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdReviewerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                //Reload the Grid
                FillTaskDocuments();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        #region Paging

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1" || ddlDocType.SelectedValue == "2")
                {
                    if (!IsValid()) { return; };

                    SelectedPageNo.Text = "1";
                    int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                    if (currentPageNo < GetTotalPagesCount())
                    {
                        SelectedPageNo.Text = (currentPageNo).ToString();
                    }

                    if (PerformerFlag)
                    {
                        grdPerformerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdPerformerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }

                    else if (ReviewerFlag)
                    {
                        grdReviewerTaskDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdReviewerTaskDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                    }
                }
                //Reload the Grid
                FillTaskDocuments();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // throw ex;
            }
        }
        #endregion

        #region  View
        protected void rptViewTaskVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptViewTaskVersion.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                rptViewTaskVersion.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";
                                        //UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptViewTaskVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                
                var scriptManager = ScriptManager.GetCurrent(this.Page);                
                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblViewTaskDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        #endregion

        #region Download
        protected void rptDownloadTaskVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnDownloadTaskVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton) e.Item.FindControl("lblDownloadTaskDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }

        protected void rptDownloadTaskVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                        if (taskDocument.Count <= 0)
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            var documentVersionData = taskDocument.Select(x => new
                            {
                                ID = x.ID,
                                TaskScheduleOnID = x.TaskScheduleOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                FileID = x.FileID,
                                //VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            rptDownloadTaskVersion.DataSource = documentVersionData;
                            rptDownloadTaskVersion.DataBind();

                            //upTaskDocument.Update();
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                            if (taskDocument.Count > 0)
                            {
                                ComplianceZip.AddDirectoryByName(commandArgs[1]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptViewTaskVersion.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";                                        
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Performer Grid Events
        protected void grdPerformerTaskDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArg = e.CommandArgument.ToString().Split(',');
                int taskScheduleOnID = Convert.ToInt32(commandArg[0]);
                int taskTransactionID = Convert.ToInt32(commandArg[1]);
                if (taskScheduleOnID != 0)
                {
                    if (taskTransactionID != 0)
                    {
                        Session["TaskScheduleOnID"] = commandArg[0];
                        Session["TransactionID"] = commandArg[1];
                        if (e.CommandName == "Download")
                        {
                            List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                            if (CMPDocuments != null)
                            {
                                List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetTaskDocumentView entityData = new GetTaskDocumentView();
                                    entityData.Version = "1.0";
                                    entityData.TaskScheduleOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDownloadTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDownloadTaskVersion.DataBind();
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                                }
                                else
                                {
                                    using (ZipFile ComplianceZip = new ZipFile())
                                    {
                                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                        List<GetTaskDocumentView> ComplianceFileData = new List<GetTaskDocumentView>();
                                        List<GetTaskDocumentView> ComplianceDocument = new List<GetTaskDocumentView>();

                                        ComplianceDocument = TaskManagment.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                                        ComplianceFileData = ComplianceDocument;

                                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                        if (ComplianceFileData.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceFileData)
                                            {
                                                string filePath = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                else
                                                {
                                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string[] filename = file.FileName.Split('.');
                                                    string str = filename[0] + i + "." + filename[1];
                                                    if (file.EnType == "M")
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    i++;
                                                }
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        ComplianceZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] data = zipMs.ToArray();

                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskDocument_" + commandArgs[1] + ".zip");
                                        Response.BinaryWrite(data);
                                        Response.Flush();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                                    }
                                }
                            }
                        }
                        else if (e.CommandName == "View")
                        {
                            List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                            if (CMPDocuments != null)
                            {
                                List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetTaskDocumentView entityData = new GetTaskDocumentView();
                                    entityData.Version = "1.0";
                                    entityData.TaskScheduleOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in CMPDocuments)
                                    {
                                        rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version); //+ "_" + lastFragment.Substring(1, 5);
                                        rptViewTaskVersion.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            upTaskDetails1.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdPerformerTaskDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatus");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdPerformerTaskDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpSaveCheckedValues();
            FillTaskDocuments();
            CmpPopulateCheckedValues();
        }
        #endregion

        #region Reviewer Grid Events
        protected void grdReviewerTaskDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    Label lblScheduledOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label) e.Row.FindControl("lblStatusReviewer");

                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;

                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Not Complied")
                            lblStatus.Text = "Rejected";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdReviewerTaskDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArg = e.CommandArgument.ToString().Split(',');
                int taskScheduleOnID = Convert.ToInt32(commandArg[0]);
                int taskTransactionID = Convert.ToInt32(commandArg[1]);
                if (taskScheduleOnID != 0)
                {
                    if (taskTransactionID != 0)
                    {
                        Session["TaskScheduleOnID"] = commandArg[0];
                        Session["TransactionID"] = commandArg[1];
                        if (e.CommandName == "Download")
                        {
                            List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                            if (CMPDocuments != null)
                            {
                                List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetTaskDocumentView entityData = new GetTaskDocumentView();
                                    entityData.Version = "1.0";
                                    entityData.TaskScheduleOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDownloadTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDownloadTaskVersion.DataBind();
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                                }
                                else
                                {
                                    using (ZipFile ComplianceZip = new ZipFile())
                                    {
                                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                        List<GetTaskDocumentView> ComplianceFileData = new List<GetTaskDocumentView>();
                                        List<GetTaskDocumentView> ComplianceDocument = new List<GetTaskDocumentView>();

                                        ComplianceDocument = TaskManagment.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                                        ComplianceFileData = ComplianceDocument;

                                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                        if (ComplianceFileData.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceFileData)
                                            {
                                                string filePath = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                else
                                                {
                                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string[] filename = file.FileName.Split('.');
                                                    string str = filename[0] + i + "." + filename[1];
                                                    if (file.EnType == "M")
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    i++;
                                                }
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        ComplianceZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] data = zipMs.ToArray();

                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskDocument_" + commandArgs[1] + ".zip");
                                        Response.BinaryWrite(data);
                                        Response.Flush();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                                    }
                                }
                            }
                        }
                        else if (e.CommandName == "View")
                        {
                            List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                            if (CMPDocuments != null)
                            {
                                List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetTaskDocumentView entityData = new GetTaskDocumentView();
                                    entityData.Version = "1.0";
                                    entityData.TaskScheduleOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in CMPDocuments)
                                    {
                                        rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version); //+ "_" + lastFragment.Substring(1, 5);
                                        rptViewTaskVersion.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            upTaskDetails1.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
        protected void grdReviewerTaskDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CmpSaveCheckedValues();
            FillTaskDocuments();
            CmpPopulateCheckedValues();
        }
        #endregion
    }
}