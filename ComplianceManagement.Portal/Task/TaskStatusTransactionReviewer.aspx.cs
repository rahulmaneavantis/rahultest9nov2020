﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class TaskStatusTransactionReviewer : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        public static List<SP_TaskInstanceTransactionStatutoryView_Result> MastersTasklist = new List<SP_TaskInstanceTransactionStatutoryView_Result>();
        public static List<SP_TaskInstanceTransactionInternalView_Result> MastersTasklistinternal = new List<SP_TaskInstanceTransactionInternalView_Result>();
        public static List<TaskDocumentsView> MastersTaskDocumentslistinternal = new List<TaskDocumentsView>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["TID"]) && !string.IsNullOrEmpty(Request.QueryString["TSOID"]))
                    {
                        var taskInstanceID = Request.QueryString["TID"];
                        var taskScheduleOnID = Request.QueryString["TSOID"];

                        if (taskInstanceID != "" && taskScheduleOnID != "")
                        {
                            BindTransactionDetails(Convert.ToInt32(taskInstanceID), Convert.ToInt32(taskScheduleOnID));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {
                rdbtnStatus.Items.Clear();

                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (ComplianceStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
                divDated1.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactionDetails(int taskInstanceID, int taskScheduledOnID)
        {          
            //added by rhul on 7 June 2019 For check transaction done or not
            var RTT = TaskManagment.GetCurrentTaskStatus(taskInstanceID, taskScheduledOnID);            
            if (RTT.TaskStatusID == 4 || RTT.TaskStatusID == 5)
            {
                btnSave.Visible = false;
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "This task is already closed.";
            }
            else
            {
                #region 
                btnSave.Visible = true;
                try
                {
                    var taskDetail = TaskManagment.GetTaskDetail(taskScheduledOnID, taskInstanceID, 4); //In Case Performer wee need to show Reviewer Name 

                    if (taskDetail != null)
                    {
                        lblTaskTitle.Text = taskDetail.TaskTitle;
                        lblTaskDesc.Text = taskDetail.TaskDescription;
                        lblTaskCompliance.Text = TaskManagment.GetTaskComplianceDescription(Convert.ToInt32(taskDetail.MainTaskID), taskDetail.TaskType);
                        lblTaskReviewer.Text = taskDetail.User;
                        lblTaskDueDate.Text = Convert.ToDateTime(taskDetail.ScheduledOn).ToString("dd-MMM-yyyy");
                        lblLocation.Text = taskDetail.Branch;
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            lblPeriod1.Text = Business.ComplianceManagement.PeriodReplace(taskDetail.ForMonth);
                            lblPeriod.Text = taskDetail.ForMonth;
                        }
                        else
                        {
                            lblPeriod1.Text = taskDetail.ForMonth;
                            lblPeriod.Text = taskDetail.ForMonth;
                        }

                        tbxDate.Text = string.Empty;
                        tbxRemarks.Text = string.Empty;

                        ViewState["taskInstanceID"] = taskInstanceID;
                        hdnTaskInstanceID.Value = Convert.ToString(taskInstanceID);

                        ViewState["taskScheduleOnID"] = taskScheduledOnID;
                        hdnTaskScheduledOnID.Value = Convert.ToString(taskScheduledOnID);

                        ViewState["ComplianceScheduleOnID"] = taskScheduledOnID;
                        hdnComplianceScheduledOnID.Value = Convert.ToString(taskDetail.ComplianceScheduleOnID);
                        BindTaskTransactionLog(Convert.ToInt32(taskDetail.ComplianceScheduleOnID), taskScheduledOnID);                      
                        if (RTT != null)
                        {
                            BindDocument(taskScheduledOnID);
                            btnSave.Attributes.Remove("disabled");
                            BindStatusList(Convert.ToInt32(RTT.TaskStatusID));
                            if (RTT.TaskStatusID == 1 || RTT.TaskStatusID == 6 || RTT.TaskStatusID == 10 || RTT.TaskStatusID == 13 || RTT.TaskStatusID == 14)
                            {
                                btnSave.Visible = true;
                            }
                            else if (RTT.TaskStatusID != 1)
                            {
                                btnSave.Attributes.Add("disabled", "disabled");
                            }
                            upTaskReviewer.Update();
                        }
                        var showHideButton = BindSubTasks(taskDetail.TaskType, taskDetail.TaskID, taskDetail.ForMonth, taskDetail.RoleID, taskDetail.ComplianceScheduleOnID);
                        btnSave.Enabled = showHideButton;
                        btnReject.Enabled = showHideButton;

                        var taskForm = Business.TaskManagment.GetTaskFormByTaskID(taskDetail.TaskID);
                        string sampleFormPath = string.Empty;

                        if (taskForm != null)
                        {
                            lbDownloadSample.Text = "Download";
                            lbDownloadSample.CommandArgument = taskForm.TaskID.ToString();
                            sampleFormPath = taskForm.FilePath;
                            sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);

                            lblNote.Visible = true;
                            lnkViewSampleForm.Visible = true;
                            lblSlash.Visible = true;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                lblpathsample.Text = sampleFormPath;
                            }
                            else
                            {
                                lblpathsample.Text = sampleFormPath;
                            }
                        }
                        else
                        {
                            lblNote.Visible = false;
                            lblSlash.Visible = false;
                            lnkViewSampleForm.Visible = false;
                            sampleFormPath = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTaskReviewer.IsValid = false;
                    cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }
        private bool BindSubTasks(int taskType, long parentTaskID, string period, int roleID, long? complianceScheduleOnID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (taskType == 1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                            where row.ParentID == parentTaskID
                                            && row.ForMonth == period
                                            && row.RoleID == roleID
                                            && row.ComplianceScheduleOnID == complianceScheduleOnID
                                            select row).ToList();

                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();
                        if (documentData.Count != 0)
                            divTaskSubTask.Visible = true;
                        else
                            divTaskSubTask.Visible = false;

                        var LinkTaskIDList = (from row in entities.SP_TaskLinkingInstanceTransactionStatutoryView(customerID)
                                              where row.TaskID == parentTaskID
                                               && row.ForMonth == period
                                               && row.RoleID == roleID
                                               && row.ComplianceScheduleOnID == complianceScheduleOnID
                                              select row.TasklinkID).ToList();

                        var LinkdocumentData = (from row in entities.SP_TaskLinkingInstanceTransactionStatutoryView(customerID)
                                                where LinkTaskIDList.Contains(row.TaskID)
                                                  && row.ForMonth == period
                                                 && row.RoleID == roleID
                                                  && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                select row).ToList();

                       
                        gridLinkSubTask.DataSource = LinkdocumentData;
                        gridLinkSubTask.DataBind();

                        if (LinkdocumentData.Count != 0)
                            divLinkTaskSubTask.Visible = true;
                        else
                            divLinkTaskSubTask.Visible = false;

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                }

                else if (taskType == 2)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                            //where 
                                            //row.ParentID == parentTaskID 
                                            //&& row.ForMonth == period
                                            //&& row.RoleID == roleID
                                            select row).ToList();

                        var TaskdocumentData = (from row in entities.TaskDocumentsViews
                                                where row.CustomerID == customerID && row.TaskType == 2
                                                select row).ToList();
                        if (TaskdocumentData.Count > 0)
                        {
                            TaskdocumentData = TaskdocumentData.Where(entry => entry.TaskStatusID == 4 || entry.TaskStatusID == 5).ToList();
                            MastersTaskDocumentslistinternal = TaskdocumentData;
                        }
                        MastersTasklistinternal = documentData;

                        documentData = (from row in documentData
                                        where row.ParentID == parentTaskID
                                        && row.ComplianceScheduleOnID == complianceScheduleOnID
                                        && row.RoleID == roleID
                                        select row).ToList();

                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();
                        if (documentData.Count != 0)
                            divTaskSubTask.Visible = true;
                        else
                            divTaskSubTask.Visible = false;
                        var LinkTaskIDList = (from row in entities.SP_TaskLinkingInstanceTransactionInternalView(customerID)
                                              where row.TaskID == parentTaskID
                                              && row.ForMonth == period
                                              && row.RoleID == roleID
                                              select row.TasklinkID).ToList();

                        var LinkdocumentData = (from row in entities.SP_TaskLinkingInstanceTransactionInternalView(customerID)
                                                where LinkTaskIDList.Contains(row.TaskID)
                                                 && row.ForMonth == period
                                                 && row.RoleID == roleID
                                                select row).ToList();

                        gridLinkSubTask.DataSource = LinkdocumentData;
                        gridLinkSubTask.DataBind();
                        if (LinkdocumentData.Count != 0)
                            divLinkTaskSubTask.Visible = true;
                        else
                            divLinkTaskSubTask.Visible = false;
                        if (documentData.Count == documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count())
                            return true;
                        else
                            return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        public void BindDocument(int taskScheduledOnID)
        {
            try
            {
                List<GetTaskDocumentView> taskDocuments = new List<GetTaskDocumentView>();
                List<GetTaskDocumentData> taskDocumentsoutput = new List<GetTaskDocumentData>();
                taskDocumentsoutput.Clear();
                taskDocuments = TaskManagment.GetTaskRelatedFileData(taskScheduledOnID).ToList(); /*.Where(entry => entry.Version.Equals("1.0")*/
                var taskDocumentsNew = taskDocuments.Where(x => x.ISLink == false).ToList();
                var documentVersionData = taskDocumentsNew.Select(x => new
                {
                    ID = x.ID,
                    TaskScheduleOnID = x.TaskScheduleOnID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    Fileid = x.FileID,
                    ISLink = x.ISLink,
                    FilePath = x.FilePath,
                    FileName = x.FileName
                }).GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();

                var documentVersionData1 = taskDocuments.Select(x => new
                {
                    ID = x.ID,
                    TaskScheduleOnID = x.TaskScheduleOnID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    Fileid = x.FileID,
                    ISLink = x.ISLink,
                    FilePath = x.FilePath,
                    FileName = x.FileName
                }).Where(x => x.ISLink == true).ToList();
                //rptComplianceVersion.DataSource = taskDocuments;
                foreach (var item in documentVersionData)
                {
                    taskDocumentsoutput.Add(new GetTaskDocumentData
                    {
                        ID = item.ID,
                        TaskScheduleOnID = item.TaskScheduleOnID,
                        Version = item.Version,
                        VersionDate = item.VersionDate,
                        FileID = item.Fileid,
                        ISLink = item.ISLink,
                        FilePath = item.FilePath,
                        FileName = item.FileName
                    });
                }
                foreach (var item in documentVersionData1)
                {
                    taskDocumentsoutput.Add(new GetTaskDocumentData
                    {
                        ID = item.ID,
                        TaskScheduleOnID = item.TaskScheduleOnID,
                        Version = item.Version,
                        VersionDate = item.VersionDate,
                        FileID = item.Fileid,
                        ISLink = item.ISLink,
                        FilePath = item.FilePath,
                        FileName = item.FileName
                    });
                }                
                rptComplianceVersion.DataSource = taskDocumentsoutput;
                rptComplianceVersion.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public class GetTaskDocumentData
        {
            public Nullable<int> ID { get; set; }
            public int FileID { get; set; }
            public Nullable<long> TaskScheduleOnID { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> VersionDate { get; set; }            
            public Nullable<bool> ISLink { get; set; }
        }
        private void BindTaskTransactionLog(int complianceScheduleOnID, int taskScheduledOnID)
        {
            try
            {
                grdTransactionLogHistory.DataSource = TaskManagment.GetAllTaskTransactionLog(taskScheduledOnID, complianceScheduleOnID);
                grdTransactionLogHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow) (((Control) e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label) row.FindControl("lblTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {                                    
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));                                   
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptTaskCompliancedocument.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptTaskCompliancedocument.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";
                                        UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionLogHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (hdnComplianceScheduledOnID.Value != null && hdnTaskScheduledOnID.Value != null)
                {
                    grdTransactionLogHistory.PageIndex = e.NewPageIndex;
                    BindTaskTransactionLog(Convert.ToInt32(hdnComplianceScheduledOnID.Value), Convert.ToInt32(hdnTaskScheduledOnID.Value));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
                return "";
            }

        }
        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {

                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                    if (taskScheduleOnID != 0)
                    {
                        List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();
                        taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        if (commandArgs[1].Equals("1.0"))
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                            if (taskDocument.Count <= 0)
                            {
                                taskDocument = taskDocument.Where(entry => entry.Version == null).ToList();
                            }
                        }
                        else
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        }

                        if (e.CommandName.Equals("version"))
                        {
                            if (e.CommandName.Equals("version"))
                            {
                                List<GetTaskDocumentData> taskDocumentsoutput = new List<GetTaskDocumentData>();
                                taskDocumentsoutput.Clear();
                                var taskDocumentsNew = taskDocument.Where(x => x.ISLink == false).ToList();
                                var documentVersionData = taskDocumentsNew.Select(x => new
                                {
                                    ID = x.ID,
                                    TaskScheduleOnID = x.TaskScheduleOnID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    Fileid = x.FileID,
                                    ISLink = x.ISLink,
                                    FilePath = x.FilePath,
                                    FileName = x.FileName
                                }).GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                                var documentVersionData1 = taskDocument.Select(x => new
                                {
                                    ID = x.ID,
                                    TaskScheduleOnID = x.TaskScheduleOnID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    Fileid = x.FileID,
                                    ISLink = x.ISLink,
                                    FilePath = x.FilePath,
                                    FileName = x.FileName
                                }).Where(x => x.ISLink == true).ToList();
                                foreach (var item in documentVersionData)
                                {
                                    taskDocumentsoutput.Add(new GetTaskDocumentData
                                    {
                                        ID = item.ID,
                                        TaskScheduleOnID = item.TaskScheduleOnID,
                                        Version = item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.Fileid,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                foreach (var item in documentVersionData1)
                                {
                                    taskDocumentsoutput.Add(new GetTaskDocumentData
                                    {
                                        ID = item.ID,
                                        TaskScheduleOnID = item.TaskScheduleOnID,
                                        Version = item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.Fileid,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                rptComplianceVersion.DataSource = taskDocumentsoutput;
                                rptComplianceVersion.DataBind();
                                //var documentVersionData = taskDocument.Select(x => new
                                //{
                                //    ID = x.ID,
                                //    TaskScheduleOnID = x.TaskScheduleOnID,
                                //    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                //    VersionDate = x.VersionDate,
                                //    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                //    Fileid = x.FileID,
                                //    ISLink = x.ISLink,
                                //    FilePath = x.FilePath,
                                //    FileName = x.FileName
                                //}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                                //rptComplianceVersion.DataSource = documentVersionData;
                                //rptComplianceVersion.DataBind();

                                upTaskDocument.Update();
                            }
                        }
                        else if (e.CommandName.Equals("Download"))
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                                taskDocument = taskDocument.Where(x => x.ISLink == false).ToList();
                                if (taskDocument.Count > 0)
                                {
                                    ComplianceZip.AddDirectoryByName(commandArgs[1]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        else if (e.CommandName.Equals("View"))
                        {
                            #region
                            List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                            taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                            Session["ScheduleOnID"] = taskScheduleOnID;

                            if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                            {
                                taskDocumenttoView = taskDocumenttoView.Where(x => x.ISLink == false).ToList();
                                List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                                if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetTaskDocumentView entityData = new GetTaskDocumentView();
                                    entityData.Version = "271.0";
                                    entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptTaskCompliancedocument.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptTaskCompliancedocument.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblTaskpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblInternalpathDownload");


                LinkButton lblDownLoadfile = (LinkButton) e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton) e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
        }

        protected void rptTaskCompliancedocument_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblTaskDocumentView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptTaskCompliancedocument_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {                    
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();                       
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;                       
                        taskDocumenttoView = taskDocumenttoView.Where(entry=>entry.FileID== taskFileidID).ToList();
                        
                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptTaskCompliancedocument.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                rptTaskCompliancedocument.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {                                  
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";
                                        UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        protected void upTaskReviewer_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerReviewer", "initializeDatePicker();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        { 
            //added by rhul on 7 June 2019 For check transaction done or not
            var TIID = Convert.ToInt32(hdnTaskInstanceID.Value);
            var TSOID = Convert.ToInt32(hdnTaskScheduledOnID.Value);
            var RTT = TaskManagment.GetCurrentTaskStatus(TIID, TSOID);

            if (RTT.TaskStatusID == 4 || RTT.TaskStatusID == 5)
            {
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "This task already performed.";
            }
            else
            {
                #region Save Code
                try
                {
                    int StatusID = 0;
                    if (lblStatus.Visible)
                        StatusID = Convert.ToInt32(rdbtnStatus.SelectedValue);
                    else
                        StatusID = Convert.ToInt32(TaskManagment.GetClosedTransaction(Convert.ToInt32(hdnComplianceScheduledOnID.Value)).ComplianceStatusID);

                    TaskTransaction transaction = new TaskTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnID.Value),
                        TaskScheduleOnID = Convert.ToInt64(hdnTaskScheduledOnID.Value),
                        TaskInstanceId = Convert.ToInt64(hdnTaskInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = StatusID,
                        StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text,
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                    if (sucess)
                    {
                        cvTaskReviewer.IsValid = false;
                        cvTaskReviewer.ErrorMessage = "Save Sucessfully";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTaskReviewer.IsValid = false;
                    cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        { 
            //added by rhul on 7 June 2019 For check transaction done or not
            var TIID = Convert.ToInt32(hdnTaskInstanceID.Value);
            var TSOID = Convert.ToInt32(hdnTaskScheduledOnID.Value);
            var RTT = TaskManagment.GetCurrentTaskStatus(TIID, TSOID);            
            if (RTT.TaskStatusID == 6 || RTT.TaskStatusID == 8)
            {
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "This task already rejected.";
            }
            else
            {
                #region Save Code
                try
                {
                    TaskTransaction transaction = new TaskTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnID.Value),
                        TaskScheduleOnID = Convert.ToInt64(hdnTaskScheduledOnID.Value),
                        TaskInstanceId = Convert.ToInt64(hdnTaskInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 6,
                        StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text,
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                    if (sucess)
                    {
                        cvTaskReviewer.IsValid = false;
                        cvTaskReviewer.ErrorMessage = "Save Sucessfully";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTaskReviewer.IsValid = false;
                    cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview1");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");
                    Label lblTaskID = (Label)e.Row.FindControl("lblTaskID");                    
                    Label lblCbranchId = (Label)e.Row.FindControl("lblCbranchId");
                    Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                    Label lblsubtaskDocuments = (Label)e.Row.FindControl("lblsubtaskDocuments");
                    Image chkDocument = (Image)e.Row.FindControl("chkDocument");

                    int taskId = Convert.ToInt32(lblTaskID.Text);
                    int CbranchId = Convert.ToInt32(lblCbranchId.Text);
                    if (MastersTasklist.Count > 0)
                    {
                        var documentData = (from row in MastersTasklist
                                            where row.ParentID == (long?)taskId
                                            select row).ToList();                        
                        string strDocumentSubtasks = "";
                        foreach (var item in documentData)
                        {                            
                            strDocumentSubtasks += "<br/>" + item.TaskTitle;
                        }                        
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }
                    else if (MastersTasklistinternal.Count > 0)
                    {
                        var documentData = (from row in MastersTasklistinternal
                                            where row.ParentID == (long?)taskId
                                             && row.ForMonth == lblForMonth.Text &&
                                             row.CustomerBranchID == (long?)CbranchId
                                             && row.RoleID == 4
                                            select row).ToList();

                        string strDocumentSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";                        
                        int DocCounter = 0;
                        foreach (var item in documentData)
                        {
                            string disp = "none";
                            if (item.UserID == AuthenticationHelper.UserID && (item.TaskStatusID == 1 || item.TaskStatusID == 8))
                            {
                                disp = "block";
                            }
                            #region Document
                            if (MastersTaskDocumentslistinternal.Count > 0)
                            {
                                var taskdocumentData = (from row in MastersTaskDocumentslistinternal
                                                        where row.ForMonth == item.ForMonth &&
                                                         row.CustomerBranchID == item.CustomerBranchID
                                                         && row.TaskScheduleOnID == item.TaskScheduledOnID
                                                        select row).ToList();
                                if (taskdocumentData.Count > 0)
                                {
                                    DocCounter = 1;
                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                    strDocumentSubtasks += "</div>";
                                }
                            }
                            #endregion

                            if (1 == 1)
                            {
                                
                                var documentData1 = (from row in MastersTasklistinternal
                                                     where                                                      
                                                      row.ForMonth == lblForMonth.Text &&
                                                      row.CustomerBranchID == (long?)CbranchId
                                                      && row.RoleID == 4
                                                       && row.ParentID == item.TaskID
                                                     select row).ToList();
                                foreach (var item1 in documentData1)
                                {
                                    disp = "none";
                                    if (item1.UserID == AuthenticationHelper.UserID && (item1.TaskStatusID == 1 || item1.TaskStatusID == 8))
                                    {
                                        disp = "block";
                                    }
                                    //style='display:"+ disp + "'
                                    #region Document1
                                    if (MastersTaskDocumentslistinternal.Count > 0)
                                    {
                                        var taskdocumentData1 = (from row in MastersTaskDocumentslistinternal
                                                                 where row.ForMonth == item1.ForMonth &&
                                                                  row.CustomerBranchID == item1.CustomerBranchID
                                                                  && row.TaskScheduleOnID == item1.TaskScheduledOnID
                                                                 select row).ToList();
                                        if (taskdocumentData1.Count > 0)
                                        {
                                            DocCounter = 1;
                                            strDocumentSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item1.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                            strDocumentSubtasks += "</div>";
                                        }
                                    }
                                    #endregion
                                    
                                    var documentData2 = (from row in MastersTasklistinternal
                                                         where                                                           
                                                           row.ForMonth == lblForMonth.Text &&
                                                          row.CustomerBranchID == (long?)CbranchId
                                                          && row.RoleID ==  4
                                                           && row.ParentID == item1.TaskID
                                                         select row).ToList();
                                    foreach (var item2 in documentData2)
                                    {
                                        disp = "none";
                                        if (item2.UserID == AuthenticationHelper.UserID && (item2.TaskStatusID == 1 || item2.TaskStatusID == 8))
                                        {
                                            disp = "block";
                                        }
                                        //style='display:"+ disp + "'
                                        #region Document2
                                        if (MastersTaskDocumentslistinternal.Count > 0)
                                        {
                                            var taskdocumentData2 = (from row in MastersTaskDocumentslistinternal
                                                                     where row.ForMonth == item2.ForMonth &&
                                                                      row.CustomerBranchID == item2.CustomerBranchID
                                                                      && row.TaskScheduleOnID == item2.TaskScheduledOnID
                                                                     select row).ToList();
                                            if (taskdocumentData2.Count > 0)
                                            {
                                                strDocumentSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                strDocumentSubtasks += "</div>";
                                            }
                                        }
                                        #endregion                                        
                                        var documentData3 = (from row in MastersTasklistinternal
                                                             where                                                               
                                                               row.ForMonth == lblForMonth.Text &&
                                                              row.CustomerBranchID == (long?)CbranchId
                                                              && row.RoleID == 4
                                                               && row.ParentID == item2.TaskID
                                                             select row).ToList();
                                        foreach (var item3 in documentData3)
                                        {
                                            disp = "none";
                                            if (item3.UserID == AuthenticationHelper.UserID && (item3.TaskStatusID == 1 || item3.TaskStatusID == 8))
                                            {
                                                disp = "block";
                                            }
                                            //style='display:"+ disp + "'
                                            #region Document3
                                            if (MastersTaskDocumentslistinternal.Count > 0)
                                            {
                                                var taskdocumentData3 = (from row in MastersTaskDocumentslistinternal
                                                                         where row.ForMonth == item3.ForMonth &&
                                                                          row.CustomerBranchID == item3.CustomerBranchID
                                                                          && row.TaskScheduleOnID == item3.TaskScheduledOnID
                                                                         select row).ToList();
                                                if (taskdocumentData3.Count > 0)
                                                {
                                                    DocCounter = 1;
                                                    strDocumentSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item3.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button'  onclick ='downloadTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                    strDocumentSubtasks += "</div>";
                                                }                                                
                                            }
                                            #endregion                                            
                                        }                                        
                                    }                                    
                                }                                
                            }                            
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                        if (DocCounter == 0)
                        {
                            chkDocument.Visible = false;
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }


                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridLinkSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label)row.FindControl("lblLinkTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (eachFile.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptTaskCompliancedocument.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptTaskCompliancedocument.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        subTaskDocViewPath = FileName;
                                        subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                        lblMessage.Text = "";
                                        UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskReviewer.IsValid = false;
                cvTaskReviewer.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void gridLinkSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblLinkStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblLinkSlashReview1");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnLinkSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnLinkSubTaskDocDownload");
                    //Label lblIsTaskClose = (Label)e.Row.FindControl("lblLinkIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                //if (lblIsTaskClose.Text == "True")
                                //{
                                //    btnSubTaskDocDownload.Visible = false;
                                //    lblSlashReview.Visible = false;
                                //    btnSubTaskDocView.Visible = false;
                                //}
                                //else
                                //{
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                //}
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.TaskManagment.GetTaskFormByTaskID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                if (file != null)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.FileName);
                    Response.BinaryWrite(file.FileData); // create the file
                    Response.Flush(); // send it to the client to download

                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}