﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Task
{
    public partial class PerformerTaskDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.TaskApplicable != "1")
                {
                    Response.Redirect("~/Common/Dashboard.aspx", false);
                }
                else
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";


                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlTaskType.SelectedValue = "2";
                        ddlTaskType.Items.Remove(ddlTaskType.Items.FindByValue("1"));
                    }

                    BindTaskDropDown();
                    BindTasks();

                    BindAct();

                    //BindLocationFilter();
                    BindUsers(ddlPerformer);
                    BindUsers(ddlReviewer);
                    BindTaskTypeDropDown();
                    divStatutory.Visible = true;
                    divInternal.Visible = false;
                    txtTask.Attributes.Add("readonly", "readonly");

                }
            }
        }

        private void BindTaskTypeDropDown()
        {
            try
            {
                ddlAllTaskType.DataTextField = "Name";
                ddlAllTaskType.DataValueField = "ID";

                ddlAllTaskType.DataSource = Business.TaskManagment.GetTaskTypeData();
                ddlAllTaskType.DataBind();
                ddlAllTaskType.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlAllTaskType.Items.Add(new ListItem("Add New", "0"));
                lblAddTaskType.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindSubTaskTypeDropDown(int TaskType)
        {
            try
            {
                if (TaskType == -1)
                {
                    ddlSubTaskType.DataSource = Business.TaskManagment.GetAllSubTaskType(-1); 
                    ddlSubTaskType.DataBind();
                    lblAddSubTask.Visible = false;
                    upTaskDetails.Update();
                }
                else
                {
                    ddlSubTaskType.DataTextField = "Name";
                    ddlSubTaskType.DataValueField = "ID";
                    ddlSubTaskType.DataSource = Business.TaskManagment.GetAllSubTaskType(TaskType);
                    ddlSubTaskType.DataBind();
                    ddlSubTaskType.Items.Insert(0, new ListItem("< Select >", "-1"));
                    ddlSubTaskType.Items.Add(new ListItem("Add New", "0"));
                    lblAddSubTask.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };
                grdTask.PageSize = int.Parse(((DropDownList)sender).SelectedValue);
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindTasks();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindDropDownTasks(long? ParentID, long? TaskID = null)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int Userid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //var taskQuery = (from row in entities.Tasks
                //                 where row.Isdeleted == false
                //                 && row.CustomerID == customerID
                //                 && row.ParentID == ParentID
                //                 && row.ID != TaskID
                //                 && row.CreatedBy == Userid
                //                 select new
                //                 {
                //                     row.ID,
                //                     row.Title,
                //                     row.TaskType,
                //                 });

                var taskQuerymapping = (from row in entities.TaskComplianceMappings
                                        where row.TaskID == TaskID
                                        select row.ComplianceID
                                        ).FirstOrDefault();


                var taskQuery = (from row in entities.Tasks
                                 join row1 in entities.TaskComplianceMappings on row.ID equals row1.TaskID
                                 where row.Isdeleted == false
                                 && row.ID != TaskID
                                 && row.CreatedBy == Userid
                                 && row.CustomerID == customerID
                                 && row1.ComplianceID.Equals(taskQuerymapping)
                                 select new
                                 {
                                     row.ID,
                                     row.Title,
                                     row.TaskType,
                                 });

                var task = taskQuery.ToList();

                if (rdoStatutory.Checked == true)
                {
                    task = task.Where(entry => entry.TaskType == 1).ToList();
                }
                else
                {
                    task = task.Where(entry => entry.TaskType == 2).ToList();
                }

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    task = task.Where(entry => entry.Title == tbxFilter.Text).ToList();
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderBy(entry => entry.Title).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderByDescending(entry => entry.Title).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                rptTask.DataSource = task;
                rptTask.DataBind();

                foreach (RepeaterItem aItem in rptTask.Items)
                {
                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                    chkCompliance.Checked = false;
                    CheckBox chkTaskSelectAll = (CheckBox)rptTask.Controls[0].Controls[0].FindControl("TaskSelectAll");
                    chkTaskSelectAll.Checked = false;
                }

                upTaskList.Update();
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                //txtFName.Text = string.Empty;
                ClearSelection();
                txtTask.Text = "< Select Task >";
                BindDropDownTasks(null);
                rdoStatutory.Checked = true;
                ViewState["Mode"] = 0;
                lblErrorMassage.Text = string.Empty;
                upTaskDetails.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divTaskDetailsDialog\").dialog('open')", true);
                //upTaskList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BindLocationFilterRahul(int ComplianceType, List<long> ComplianceList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    tvFilterLocationTask.Nodes.Clear();
                    List<int> LocationList = new List<int>();
                    if (ComplianceType == 1)
                    {
                        LocationList = TaskManagment.GetTaskComplianceMappedLocations(ComplianceList, customerID);
                    }
                    else
                    {
                        LocationList = TaskManagment.GetTaskInternalComplianceMappedLocations(ComplianceList, customerID);
                    }

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.ShowCheckBox = false;
                    node.Selected = true;                    
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        if (!LocationList.Contains(item.ID))
                        {
                            node.ShowCheckBox = false;
                        }
                        CustomerBranchManagement.TaskBindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);
                    }
                    tvFilterLocation.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(int ComplianceType, List<long> ComplianceList)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchyRestrictLocationForTask(ComplianceType, ComplianceList, customerID);

                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //private void BindLocationFilter()
        //{
        //    try
        //    {
        //        long customerID = -1;
        //        customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
        //        tvFilterLocation.Nodes.Clear();
        //        var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);
        //        TreeNode node = new TreeNode();
        //        foreach (var item in bracnhes)
        //        {
        //            node = new TreeNode(item.Name, item.ID.ToString());
        //            node.SelectAction = TreeNodeSelectAction.Expand;
        //            CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
        //            tvFilterLocation.Nodes.Add(node);
        //        }
        //        tvFilterLocation.CollapseAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnAddTask_Click(object sender, EventArgs e)
        {
            try
            {
                txtTask.Text = "< Select Task >";
                BindDropDownTasks(null);

                showSampleFormDetails(0);
                //ClearSelection();
                //rdoStatutory.Checked = true;
                //ViewState["Mode"] = 0;
                //lblErrorMassage.Text = string.Empty;
                //upTaskDetails.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divTaskDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void BindAct()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = TaskManagment.BindAct(AuthenticationHelper.UserID);
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCompliance(int ActID)
        {
            try
            {
                ddlCompliance.Items.Clear();

                ddlCompliance.DataTextField = "ShortDescription";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = TaskManagment.GetComplianceForTask(AuthenticationHelper.UserID, ActID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select Compliance >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);// UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }



        private void ClearSelection()
        {
            txtTaskTitle.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtDueDays.Text = string.Empty;
            txtComplianceDueDays.Text = string.Empty;
            txtComplianceType.Text = string.Empty;
            rdoStatutory.Checked = true;

            ddlAct.ClearSelection();
            ddlCompliance.ClearSelection();

            ddlPerformer.ClearSelection();
            ddlReviewer.ClearSelection();

            ddlAct.SelectedValue = "-1";
            ddlCompliance.SelectedValue = "-1";

            ddlInternalCompliance.SelectedValue = "-1";
            ddlPerformer.SelectedValue = "-1";
            ddlReviewer.SelectedValue = "-1";
            ddlAllTaskType.SelectedValue = "-1";
            BindSubTaskTypeDropDown(Convert.ToInt32(ddlAllTaskType.SelectedValue));
            ddlSubTaskType.SelectedValue = "-1";
            ddlIsAfter.SelectedValue = "False";
        }

        private void BindInternalCompliance()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                long UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                ddlInternalCompliance.Items.Clear();

                ddlInternalCompliance.DataTextField = "IShortDescription";
                ddlInternalCompliance.DataValueField = "ID";

                ddlInternalCompliance.DataSource = TaskManagment.GetInternalComplianceForPerformer(customerID, UserID);
                ddlInternalCompliance.DataBind();

                ddlInternalCompliance.Items.Insert(0, new ListItem("< Select Compliance >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindTasks();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdTask.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdTask.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindTasks();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upTaskDetails_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            //}

            try
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeTask", string.Format("initializeJQueryUI('{0}', 'dvTask');", txtTask.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTask", "$(\"#dvTask\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
            {
                if (ddlAct.SelectedValue != "-1")
                {
                    BindCompliance(Convert.ToInt32(ddlAct.SelectedValue));
                }
                else
                    BindCompliance(0);
            }
            else
                BindCompliance(0);
        }
        public static void ClearTreeViewSelection(TreeView tree)
        {
            foreach (TreeNode node in tree.Nodes)
            {
                if (node.Checked && node.ChildNodes.Count == 0) // if (node.Checked)
                {
                    node.Checked = false;
                }

                foreach (TreeNode tn in node.ChildNodes)
                {
                    if (tn.Checked && tn.ChildNodes.Count == 0) //if (tn.Checked)
                    {
                        tn.Checked = false;
                    }

                    if (tn.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < tn.ChildNodes.Count; i++)
                        {
                            tn.ChildNodes[i].Checked = false;
                        }
                    }
                }
            }
        }

        protected void grdTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int TaskID = Convert.ToInt32(e.CommandArgument);
                ViewState["TaskID"] = TaskID;
                ddlPerformer.ClearSelection();
                ddlReviewer.ClearSelection();
                tbxStartDate.Text = "";
                ClearTreeViewSelection(tvFilterLocation);

                tbxFilterLocationTask.Text = "Select Task Applicable Location";
                BindDropDownTasks(null, TaskID);
                if (TaskID != 0)
                {
                    if (e.CommandName.Equals("EDIT_TASK"))
                    {
                        ViewState["Mode"] = 1;
                        ViewState["TaskID"] = TaskID;

                        lblErrorMassage.Text = "";

                        var task = Business.TaskManagment.GetByTaskDetails(TaskID);
                        if (task != null)
                        {
                            txtTaskTitle.Text = task.Title;
                            txtDescription.Text = task.Description;
                            txtDueDays.Text = task.DueDays.ToString();

                            TxtConditionalMessage.Text = task.Message;
                            txtYesMessage.Text = task.Yesmessage;
                            txtNoMessage.Text = task.Nomessage;

                            Boolean IsTaskConditional;
                            if (task.IsYesNo != null)
                            {
                                if (task.IsYesNo == true)
                                    IsTaskConditional = true;
                                else
                                    IsTaskConditional = false;
                            }
                            else
                            {
                                IsTaskConditional = false;
                            }
                            ChkIsTaskConditional.Checked = IsTaskConditional;

                            if (task.IsYes != null && task.IsNo != null)
                            {
                                if (task.IsYes == true)
                                    rbTrueCondtion.SelectedValue = "0";
                                else
                                    rbTrueCondtion.SelectedValue = "1";
                            }
                            if (task.IsBothYesNo != null && task.IsBothYesNo != null)
                            {
                                if (task.IsBothYesNo == true)
                                    rbBothMessage.SelectedValue = "0";
                                else
                                    rbBothMessage.SelectedValue = "1";
                            }
                            if (task.TaskType == 1)
                            {

                                divStatutory.Visible = true;
                                divInternal.Visible = false;
                                rdoStatutory.Checked = true;

                                if (task.ActFilter != null || task.ActFilter != "")
                                {
                                    string actList = task.ActFilter;
                                    ddlAct.SelectedValue = actList;

                                    BindCompliance(Convert.ToInt32(actList));

                                    var ComplianceID = Business.TaskManagment.GetTaskCompliaceMappingbyTaskID(TaskID);

                                    if (ComplianceID != null)
                                    {
                                        if (ddlCompliance.Items.FindByValue(ComplianceID[0].ToString()) != null)
                                            ddlCompliance.SelectedValue = Convert.ToString(ComplianceID[0]);
                                    }

                                    var complaince = TaskManagment.GetComplianceDetails(Convert.ToInt32(ComplianceID[0]));
                                    if (complaince.ComplianceType == 0)
                                    {
                                        txtComplianceType.Text = "Function based";
                                    }
                                    if (complaince.ComplianceType == 1)
                                    {
                                        txtComplianceType.Text = "Checklist";
                                    }
                                    else if (complaince.ComplianceType == 2)
                                    {
                                        txtComplianceType.Text = "Time Based";
                                    }
                                    txtComplianceDueDays.Text = Convert.ToString(complaince.DueDate);
                                    ddlAllTaskType.SelectedValue = Convert.ToString(task.TaskTypeID);
                                    ddlAllTaskType_SelectedIndexChanged(null, null);
                                    ddlSubTaskType.SelectedValue = Convert.ToString(task.SubTaskID);
                                    ddlIsAfter.SelectedValue = Convert.ToString(task.IsAfter);
                                }
                            }
                            else if (task.TaskType == 2)
                            {
                                divInternal.Visible = true;
                                divStatutory.Visible = false;

                                rdoInternal.Checked = true;

                                BindInternalCompliance();

                                var IntComplianceID = Business.TaskManagment.GetTaskCompliaceMappingbyTaskID(TaskID);

                                if (IntComplianceID != null)
                                {
                                    if (ddlInternalCompliance.Items.FindByValue(IntComplianceID[0].ToString()) != null)
                                        ddlInternalCompliance.SelectedValue = Convert.ToString(IntComplianceID[0]);
                                }

                                var complaince = TaskManagment.GetInternalComplianceDetails(Convert.ToInt32(IntComplianceID[0]));
                                if (complaince.IComplianceType == 0)
                                {
                                    txtComplianceType.Text = "Function based";
                                }
                                if (complaince.IComplianceType == 1)
                                {
                                    txtComplianceType.Text = "Checklist";
                                }
                                else if (complaince.IComplianceType == 2)
                                {
                                    txtComplianceType.Text = "Time Based";
                                }
                                txtComplianceDueDays.Text = Convert.ToString(complaince.IDueDate);
                                ddlAllTaskType.SelectedValue = Convert.ToString(task.TaskTypeID);
                                ddlAllTaskType_SelectedIndexChanged(null, null);
                                ddlSubTaskType.SelectedValue = Convert.ToString(task.SubTaskID);
                                ddlIsAfter.SelectedValue = Convert.ToString(task.IsAfter);
                            }
                        }

                        txtTask.Text = "< Select >";
                        var vGetCmplianceMappedIDs = Business.ComplianceManagement.GetTaskLinkingID(TaskID);
                        foreach (RepeaterItem aItem in rptTask.Items)
                        {
                            CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                            chkCompliance.Checked = false;
                            CheckBox ComplianceSelectAll = (CheckBox)rptTask.Controls[0].Controls[0].FindControl("TaskSelectAll");

                            for (int i = 0; i <= vGetCmplianceMappedIDs.Count - 1; i++)
                            {
                                if (((Label)aItem.FindControl("lblTaskID")).Text.Trim() == vGetCmplianceMappedIDs[i].ToString())
                                {
                                    chkCompliance.Checked = true;
                                }
                            }
                            if ((rptTask.Items.Count) == (vGetCmplianceMappedIDs.Count))
                            {
                                ComplianceSelectAll.Checked = true;
                            }
                            else
                            {
                                ComplianceSelectAll.Checked = false;
                            }
                        }

                        //Added
                        showSampleFormDetails(TaskID);

                        upTaskDetails.Update();
                        // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divTaskDetailsDialog\").dialog('open')", true);
                        
                    }

                    else if (e.CommandName.Equals("SHOW_ASSIGNMENT"))
                    {
                        Session["TaskType"] = TaskManagment.GetTaskType(TaskID);
                        bool? IsTaskAfter = TaskManagment.GetTaskBeforeAfter(TaskID);
                        var complianceList = TaskManagment.GetTaskMappingComplianceList(TaskID);
                        if (IsTaskAfter != null)
                        {
                            Session["IsTaskAfter"] = IsTaskAfter;
                        }
                        else
                        {
                            Session["IsTaskAfter"] = 0;
                        }

                        //BindLocationFilter(Convert.ToInt32(Session["TaskType"]), complianceList);
                        BindLocationFilterRahul(Convert.ToInt32(Session["TaskType"]), complianceList);

                        BindLocationFilterTask();

                        BindAssignment(TaskID);

                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterTask", string.Format("initializeJQueryUI('{0}', 'divFilterLocationTask');", tbxFilterLocationTask.ClientID), true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterTask", "$(\"#divFilterLocationTask\").hide(\"blind\", null, 500, function () { });", true);

                        upTaskAssignmentDetails.Update();
                        //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divAssignmentDetailsDialog\").dialog('open')", true);

                    }
                    else if (e.CommandName.Equals("VIEW_TASKS"))
                    {
                        Session["TaskID"] = TaskID;
                        Session["MainTaskID"] = TaskID;
                        Session["ParentID"] = null;
                        Session["DueDays"] = TaskManagment.GetDueDays(TaskID);
                        Session["TaskType"] = TaskManagment.GetTaskType(TaskID);
                        Session["CustomerID"] = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        //Response.Redirect("~/Task/SubTaskDetails.aspx?ParentID=" + TaskID, false);
                        Response.Redirect("~/Task/PerformerSubTaskDetails.aspx", false);
                    }
                    else if (e.CommandName.Equals("DELETE_TASK"))
                    {
                        bool CheckTaskAssinged = false;
                        bool CheckSubTaskDeleted = false;
                        CheckTaskAssinged = TaskManagment.GetTaskAssignedData(TaskID);
                        CheckSubTaskDeleted = TaskManagment.CheckSubtaskDataDeleted(TaskID);
                        if (CheckTaskAssinged)
                        {
                            if (CheckSubTaskDeleted)
                            {
                                TaskManagment.DeleteTaskRecord(TaskID);
                                CvMainTask.IsValid = false;
                                CvMainTask.ErrorMessage = "Task Deleted Successfully";
                                BindTasks();
                            }
                            else
                            {
                                CvMainTask.IsValid = false;
                                CvMainTask.ErrorMessage = "First delete subtask.";
                            }
                        }
                        else
                        {
                            CvMainTask.IsValid = false;
                            CvMainTask.ErrorMessage = "Task can not delete, Task already assigned";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindLocationFilterTask()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                tvFilterLocationTask.Nodes.Clear();
                var bracnhes = TaskManagment.GetAllHierarchySatutory(customerID);

                TreeNode node = new TreeNode();
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagementRisk.BindBranchesHierarchy(node, item);
                    tvFilterLocationTask.Nodes.Add(node);
                }
                tvFilterLocationTask.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTasks()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskQuery = (from row in entities.Tasks
                                 where row.Isdeleted == false
                                 && row.CustomerID == customerID
                                 && row.ParentID == null
                                 && row.CreatedBy == AuthenticationHelper.UserID
                                 select new
                                 {
                                     row.ID,
                                     row.Title,
                                     row.Description,
                                     row.TaskType,
                                     row.DueDays,
                                     TaskTypeName = row.TaskType == 1 ? "Statutory" :
                                     row.TaskType == 2 ? "Internal" : ""
                                 });

                var task = taskQuery.ToList();

                if (!string.IsNullOrEmpty(ddlTask.SelectedValue))
                {
                    if (ddlTask.SelectedValue != "-1")
                    {
                        task = task.Where(entry => entry.ID == Convert.ToInt32(ddlTask.SelectedValue)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(ddlTaskType.SelectedValue))
                {
                    if (ddlTaskType.SelectedValue != "-1")
                    {
                        task = task.Where(entry => entry.TaskType == Convert.ToInt32(ddlTaskType.SelectedValue)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    task = task.Where(entry => entry.Title == tbxFilter.Text).ToList();
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderBy(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        task = task.OrderBy(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "TaskTypeName")
                    {
                        task = task.OrderBy(entry => entry.TaskTypeName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        task = task.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        task = task.OrderByDescending(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        task = task.OrderByDescending(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "TaskTypeName")
                    {
                        task = task.OrderByDescending(entry => entry.TaskTypeName).ToList();
                    }

                    direction = SortDirection.Ascending;
                }

                grdTask.DataSource = task;
                Session["TotalRows"] = task.Count;
                grdTask.DataBind();
                GetPageDisplaySummary();
                upTaskList.Update();
            }
        }

        private void BindAssignment(int TaskID)
        {
            GrdAssigment.DataSource = null;
            GrdAssigment.DataBind();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskQuery = (from row in entities.TaskAssignedInstancesViews
                                 where row.TaskID == TaskID
                                 select new
                                 {
                                     row.TaskAssignmentID
                                        ,
                                     row.TaskInstanceID
                                         ,
                                     row.CustomerBranchID
                                         ,
                                     row.Branch
                                        ,
                                     row.TaskCustomerBranchID
                                        ,
                                     row.TaskCustomerBranchName
                                         ,
                                     row.Role
                                         ,
                                     row.User
                                     ,
                                     row.UserID
                                        ,
                                     row.ScheduledOn
                                 });

                //var taskQuery = (from row in entities.TaskAssignedInstancesViews
                //                 where row.TaskID == TaskID
                //                 select row).ToList();

                var task = taskQuery.OrderBy(entry => entry.TaskInstanceID).ToList();
                //var task = taskQuery.ToList();

                GrdAssigment.DataSource = task;
                GrdAssigment.DataBind();

                upTaskAssignmentDetails.Update();
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTask.PageIndex = e.NewPageIndex;

                BindTasks();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdTask_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdTask_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {

        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Validation

                bool CheckActValidateSuccess = false;
                bool ComplValidateSuccess = false;

                bool saveSuccess = false;

                List<int> ActID = new List<int>();
                List<int> compID = new List<int>();

                if (rdoStatutory.Checked == true)
                {
                    #region Statutory
                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    {
                        if (ddlAct.SelectedValue != "-1")
                        {
                            CheckActValidateSuccess = true;
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Please Select Act.";
                            return;
                        }
                    }
                    else
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Please Select Act.";
                        return;
                    }

                    //Statutory Compliance
                    if (CheckActValidateSuccess)
                    {
                        if (!string.IsNullOrEmpty(ddlCompliance.SelectedValue))
                        {
                            if (ddlCompliance.SelectedValue != "-1")
                            {
                                long complianceID = Convert.ToInt32(ddlCompliance.SelectedValue);

                                var comFreq = TaskManagment.GetComplianceFrequency(complianceID);

                                if (comFreq != null)
                                {
                                    if (comFreq != 0)
                                        ComplValidateSuccess = true;
                                    else
                                    {
                                        if (ddlIsAfter.SelectedValue == "False")
                                        {
                                            if (Convert.ToInt32(txtDueDays.Text) <= 10)
                                            {
                                                ComplValidateSuccess = true;
                                            }
                                            else
                                            {
                                                CustomValidator1.IsValid = false;
                                                CustomValidator1.ErrorMessage = "Due Days can not be more than 10 for monthly compliance.";
                                                ComplValidateSuccess = false;
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            ComplValidateSuccess = true;
                                        }
                                    }
                                }
                                else
                                    ComplValidateSuccess = true;
                            }
                            else
                            {
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Please Select Compliance.";
                                return;
                            }
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Please Select Compliance.";
                            return;
                        }
                    }
                    #endregion
                }

                if (rdoInternal.Checked == true)
                {
                    #region Internal
                    if (!string.IsNullOrEmpty(ddlInternalCompliance.SelectedValue))
                    {
                        if (ddlInternalCompliance.SelectedValue != "-1")
                        {
                            long InternalComplianceID = Convert.ToInt32(ddlInternalCompliance.SelectedValue);

                            var comFreq = TaskManagment.GetInternalComplianceFrequency(InternalComplianceID);

                            if (comFreq != null)
                            {
                                if (comFreq != 0)
                                    ComplValidateSuccess = true;
                                else
                                {
                                    if (ddlIsAfter.SelectedValue == "False")
                                    {
                                        if (Convert.ToInt32(txtDueDays.Text) <= 10)
                                        {
                                            ComplValidateSuccess = true;
                                        }
                                        else
                                        {
                                            CustomValidator1.IsValid = false;
                                            CustomValidator1.ErrorMessage = "Due Days can not be more than 10 for monthly compliance.";
                                            ComplValidateSuccess = false;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ComplValidateSuccess = true;
                                    }
                                }
                            }
                            else
                                ComplValidateSuccess = true;

                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Please Select Internal Compliance.";
                            return;
                        }
                    }
                    else
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Please Select Internal Compliance.";
                        return;
                    }
                    #endregion
                }
                List<string> lstErrorMsg = new List<string>();

                if (fuSampleFile.PostedFile.FileName != "")
                {
                    if (fuSampleFile.PostedFiles.Count > 0)
                    {
                        string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                        string ext = System.IO.Path.GetExtension(fuSampleFile.PostedFile.FileName);
                        if (ext == "")
                        {
                            lstErrorMsg.Add("Invalid file error. System does not support uploaded file.Please upload another file.");
                        }
                        else
                        {
                            for (int i = 0; i < validFileTypes.Length; i++)
                            {
                                if (ext == "." + validFileTypes[i])
                                {
                                    lstErrorMsg.Add("Invalid file error. System does not support uploaded file.Please upload another file.");
                                    break;
                                }
                            }
                            for (int i = 0; i < fuSampleFile.PostedFiles.Count; i++)
                            {
                                HttpPostedFile uploadfile = fuSampleFile.PostedFiles[i];
                                if (uploadfile.ContentLength > 0)
                                {
                                }
                                else
                                {
                                    lstErrorMsg.Add("Please do not upload virus file or blank file =>" + uploadfile.FileName);
                                }
                            }
                        }
                    }
                }
                if (lstErrorMsg.Count > 0)
                {
                    ComplValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, CustomValidator1);                   
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "scriptTaskDialogOpen", "fopenpopup()", true);
                }
                else
                    ComplValidateSuccess = true;

                #endregion

                int TaskTypeID = -1;
                int SubTaskID = -1;
                if (!string.IsNullOrEmpty(ddlAllTaskType.SelectedValue))
                {
                    TaskTypeID = Convert.ToInt32(ddlAllTaskType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlSubTaskType.SelectedValue))
                {
                    SubTaskID = Convert.ToInt32(ddlSubTaskType.SelectedValue);
                }

                Boolean IsYesNo;
                if (ChkIsTaskConditional.Checked == true)
                    IsYesNo = true;
                else
                    IsYesNo = false;

                Boolean IsYes;
                Boolean IsNo;
                if (rbTrueCondtion.SelectedValue == "0")
                {
                    IsYes = true;
                    IsNo = false;
                }
                else
                {
                    IsYes = false;
                    IsNo = true;
                }

                Boolean IsBothMessage;
                if (rbBothMessage.SelectedValue == "0")
                {
                    IsBothMessage = true;
                }
                else
                {
                    IsBothMessage = false;
                }

                if (ComplValidateSuccess == true)
                {
                    Business.Data.Task task = new Business.Data.Task()
                    {
                        Title = txtTaskTitle.Text.Trim(),
                        Description = txtDescription.Text.Trim(),
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        IsActive = true,
                        Isdeleted = false,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedDate = Convert.ToDateTime(DateTime.Now),
                        UpdatedDate = Convert.ToDateTime(DateTime.Now),
                        DueDays = Convert.ToInt32(txtDueDays.Text),
                        ActualDueDays = Convert.ToInt32(txtDueDays.Text),
                        TaskTypeID = TaskTypeID,
                        SubTaskID = SubTaskID,
                        IsYesNo = IsYesNo,
                        Message = TxtConditionalMessage.Text.Trim(),
                        IsYes = IsYes,
                        IsNo = IsNo,
                        Yesmessage = txtYesMessage.Text.Trim(),
                        Nomessage = txtNoMessage.Text.Trim(),
                        IsBothYesNo = IsBothMessage
                    };

                    if (ddlIsAfter.SelectedValue != "-1")
                    {
                        if (ddlIsAfter.SelectedValue == "False")
                        {
                            task.IsAfter = false;
                        }
                        else if (ddlIsAfter.SelectedValue == "True")
                        {
                            task.IsAfter = true;
                        }
                        else
                        {
                            task.IsAfter = null;
                        }
                    }

                    if (rdoStatutory.Checked == true)
                    {
                        task.TaskType = 1;
                    }
                    else if (rdoInternal.Checked == true)
                    {
                        task.TaskType = 2;
                    }

                    if ((int) ViewState["Mode"] == 1)
                    {
                        task.ID = Convert.ToInt32(ViewState["TaskID"]);
                    }

                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    {
                        if (ddlAct.SelectedValue != "-1")
                        {
                            task.ActFilter = ddlAct.SelectedValue;
                        }
                    }

                    //Save Task
                    if ((int) ViewState["Mode"] == 0)
                    {
                        if (Business.TaskManagment.TaskNameExistOrnot(task))
                        {
                            if (Business.TaskManagment.CreateTask(task))
                            {
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Task Saved Successfully.";
                                saveSuccess = true;
                            }
                            else
                            {
                                CustomValidator1.IsValid = false;
                                CustomValidator1.ErrorMessage = "Something went wrong, try again.";
                                saveSuccess = false;
                            }
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Task with same title already exists.";
                            saveSuccess = false;
                        }
                    }//Task Update
                    else if ((int) ViewState["Mode"] == 1)
                    {
                        task.UpdatedBy = AuthenticationHelper.UserID;
                        task.UpdatedDate = DateTime.Now;

                        if (Business.TaskManagment.TaskNameExistOrnot(task))
                        {
                            Boolean Check = Business.TaskManagment.CheckTaskScheduleOnPresentFromTask(task.ID);
                            if (Check == false)
                            {
                                if (Business.TaskManagment.UpdateTask(task))
                                {
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Task Updated Successfully.";
                                    saveSuccess = true;
                                }
                                else
                                {
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Something went wrong, try again.";
                                    saveSuccess = false;
                                }
                            }
                            else
                            {
                                if (Business.TaskManagment.UpdateMappingTask(task))
                                {
                                    CustomValidator1.IsValid = false;
                                    CustomValidator1.ErrorMessage = "Task Updated Successfully.";
                                    saveSuccess = true;
                                }
                                //CustomValidator1.IsValid = false;
                                //CustomValidator1.ErrorMessage = "Task alredy scheduled, you can not update task";
                            }
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Task with same title already exists.";
                            saveSuccess = false;
                        }
                    }

                    if (saveSuccess)
                    {
                        // Task Linking
                        List<int> ParentTaskIds = new List<int>();
                        Business.ComplianceManagement.UpdateTaskMappedID(Convert.ToInt32(task.ID));
                        foreach (RepeaterItem aItem in rptTask.Items)
                        {
                            CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkTask");
                            if (chkCompliance.Checked)
                            {
                                ParentTaskIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblTaskID")).Text.Trim()));
                                TaskLinking taskLinking = new TaskLinking()
                                {
                                    TaskLinkID = Convert.ToInt32(((Label)aItem.FindControl("lblTaskID")).Text.Trim()),
                                    TaskID = Convert.ToInt32(task.ID),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                Business.ComplianceManagement.CreateTaskMapping(taskLinking);
                            }
                        }

                        //Remove Task Compliance Mapping
                        Business.TaskManagment.UpdateTaskComplianceMappedID(Convert.ToInt32(task.ID));

                        // Create/Update Task Compliance Mapping

                        //int TaskIDbyName = TaskManagment.GetTaskIDByName(txtTaskTitle.Text.Trim());
                        int ComplianceID = -1;
                        int InternalcompID = -1;

                        if (rdoStatutory.Checked == true)// Add Compliance to Task
                        {
                            if (CheckActValidateSuccess)
                            {
                                if (!string.IsNullOrEmpty(ddlCompliance.SelectedValue))
                                {
                                    if (ddlCompliance.SelectedValue != "-1")
                                    {
                                        ComplianceID = Convert.ToInt32(ddlCompliance.SelectedValue);
                                    }
                                }
                            }
                            TaskComplianceMapping TasktMapping = new TaskComplianceMapping()
                            {
                                ComplianceID = ComplianceID,
                                TaskID = task.ID,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            };

                            Business.TaskManagment.CreateTaskComplianceMapping(TasktMapping);
                        }
                        if (rdoInternal.Checked == true)// Add Internal Compliance to Task
                        {
                            if (!string.IsNullOrEmpty(ddlInternalCompliance.SelectedValue))
                            {
                                if (ddlInternalCompliance.SelectedValue != "-1")
                                {
                                    InternalcompID = Convert.ToInt32(ddlInternalCompliance.SelectedValue);
                                }
                            }

                            TaskComplianceMapping TasktMapping = new TaskComplianceMapping()
                            {
                                ComplianceID = InternalcompID,
                                TaskID = task.ID,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            };

                            Business.TaskManagment.CreateTaskComplianceMapping(TasktMapping);
                        }

                        //Upload Task Sample Form                        
                        if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                        {
                            string directoryPath = string.Empty;
                            string filePath = string.Empty;

                            directoryPath = Server.MapPath("~/TaskFiles/" + task.ID);

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            filePath = directoryPath + "/" + fuSampleFile.FileName;

                            fuSampleFile.SaveAs(filePath);

                            TaskForm newform = new TaskForm()
                            {
                                TaskID = task.ID,
                                FileName = fuSampleFile.FileName,
                                FileData = fuSampleFile.FileBytes,
                                FilePath = "~/TaskFiles/" + task.ID + "/" + fuSampleFile.FileName,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            };
                                                        
                            bool uploadSuccess = TaskManagment.CreateUpdateTaskForm(newform);

                            if (uploadSuccess)
                            {
                                //Added
                                showSampleFormDetails(task.ID);
                            }
                        }
                    }
                    BindTasks();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "scriptTaskDialogOpen", "fopenpopup()", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rdoStatutory_CheckedChanged(object sender, EventArgs e)
        {
            txtComplianceDueDays.Text = string.Empty;
            txtComplianceType.Text = string.Empty;

            if (rdoStatutory.Checked == true)
            {
                divStatutory.Visible = true;
                divInternal.Visible = false;
                txtDueDays.Text = string.Empty;
                rdoStatutory.Checked = true;
            }
            else if (rdoInternal.Checked == true)
            {
                divInternal.Visible = true;
                divStatutory.Visible = false;
                txtDueDays.Text = string.Empty;
                rdoInternal.Checked = true;
                BindInternalCompliance();
            }

            upTaskDetails.Update();
        }

        protected void btnSaveTaskAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    bool validationSuccess = false;
                    bool saveSuccess = false;

                    List<long> selectedLocationList = new List<long>();

                    if (!String.IsNullOrEmpty(ddlPerformer.SelectedValue) && !String.IsNullOrEmpty(ddlReviewer.SelectedValue))
                    {
                        if (ddlPerformer.SelectedValue != "-1" && ddlReviewer.SelectedValue != "-1")
                        {
                            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                            {
                                selectedLocationList = RetrieveNodes(this.tvFilterLocation.Nodes[i], selectedLocationList);
                            }

                            if (selectedLocationList.Count > 0)
                                validationSuccess = true;
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Select Location to Assign.";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Select Performer/Reviewer to Assign.";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Performer/Reviewer to Assign.";
                    }

                    if (validationSuccess)
                    {
                        int TaskbranchID = CustomerBranchManagement.GetByName(tbxFilterLocationTask.Text.Trim(), Convert.ToInt32(AuthenticationHelper.CustomerID)).ID;

                        bool Email = false;
                        string listCustomer = ConfigurationManager.AppSettings["TaskAssignmentEmailCustomerID"];
                        string[] listCust = new string[] { };
                        if (!string.IsNullOrEmpty(listCustomer))
                            listCust = listCustomer.Split(',');

                        if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                            Email = true;
                        else
                            Email = false;

                        selectedLocationList.ForEach(eachSelectedBranch =>
                        {
                            saveSuccess = SaveAssignment(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(eachSelectedBranch), TaskbranchID, Convert.ToInt32(ddlPerformer.SelectedValue), Convert.ToInt32(ddlReviewer.SelectedValue), Convert.ToBoolean(ViewState["IsAfter"]), Email);
                        });

                        if (saveSuccess)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Task Assigned to User Successfully.";
                            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, try again.";
                        }

                        BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
                    }
                }
                upTaskAssignmentDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected List<long> RetrieveNodes(TreeNode node, List<long> locationList)
        {
            if (node.Checked) // if (node.Checked)
            {
                if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    locationList.Add(Convert.ToInt32(node.Value));
            }
            //if (node.Checked && node.ChildNodes.Count == 0) // if (node.Checked)
            //{
            //    if (!locationList.Contains(Convert.ToInt32(node.Value)))
            //        locationList.Add(Convert.ToInt32(node.Value));
            //}

            foreach (TreeNode tn in node.ChildNodes)
            {
                if (tn.Checked) //if (tn.Checked) && tn.ChildNodes.Count == 0
                {
                    if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                        locationList.Add(Convert.ToInt32(tn.Value));
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        RetrieveNodes(tn.ChildNodes[i], locationList);
                    }
                }
            }

            return locationList;
        }
        private bool SaveAssignment(int taskID, int branchID,int TaskbranchID, int PerformerID, int ReviewerID,Boolean IsAfter,bool Email)
        {
            try
            {
                List<Tuple<TaskInstance, TaskAssignment>> assignments = new List<Tuple<TaskInstance, TaskAssignment>>();

                TaskInstance instance = new TaskInstance();
                instance.TaskId = taskID;
                instance.CustomerBranchID = branchID;
                instance.TaskCustomerBranchID = TaskbranchID;
                
                instance.ScheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                TaskAssignment assignment = new TaskAssignment();
                assignment.UserID = PerformerID;
                assignment.RoleID = 3;
                assignments.Add(new Tuple<TaskInstance, TaskAssignment>(instance, assignment));

                TaskAssignment assignment1 = new TaskAssignment();
                assignment1.UserID = ReviewerID;
                assignment1.RoleID = 4;
                assignments.Add(new Tuple<TaskInstance, TaskAssignment>(instance, assignment1));

                if (assignments.Count != 0)
                {
                    Business.TaskManagment.CreateTaskInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, Convert.ToInt32(Session["TaskType"]), IsAfter);

                    if (Email)
                    {
                        try
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                TaskAssignment_EmailDetail eml = new TaskAssignment_EmailDetail();
                                eml.ComplianceLocationID = branchID;
                                eml.ReportingLocationID = TaskbranchID;
                                eml.PerformerID = PerformerID;
                                eml.ReviewerID = ReviewerID;
                                eml.TaskID = taskID;
                                eml.StartDate = DateTime.Now;
                                eml.EmailFlag = false;
                                eml.CreatedBy = AuthenticationHelper.UserID;
                                eml.CreatedOn = DateTime.Now;
                                entities.TaskAssignment_EmailDetail.Add(eml);
                                entities.SaveChanges();
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
                return false;
            }
        }

        protected void upTaskAssignmentDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterTask", string.Format("initializeJQueryUI('{0}', 'divFilterLocationTask');", tbxFilterLocationTask.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterTask", "$(\"#divFilterLocationTask\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlTaskType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTasks();
        }

        protected void ddlTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTasks();
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindTasks();
        }

        private void BindTaskDropDown()
        {
            try
            {
                ddlTask.DataTextField = "Title";
                ddlTask.DataValueField = "ID";

                ddlTask.DataSource = Business.TaskManagment.GetAllTask(AuthenticationHelper.UserID,Convert.ToInt32(AuthenticationHelper.CustomerID));
                ddlTask.DataBind();
                ddlTask.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void GrdAssigment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArg = e.CommandArgument.ToString().Split(',');
                int AssignID = Convert.ToInt32(commandArg[0]);
                int TaskInstanceID = Convert.ToInt32(commandArg[1]);

                if (e.CommandName.Equals("DELETE_TASKASSIGNMENT"))
                {
                    Business.TaskManagment.DeleteTaskAssignment(AssignID, TaskInstanceID);
                    BindTasks();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void GrdAssigment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList dp = (DropDownList) e.Row.FindControl("ddlUserList");
                    BindUsers(dp);
                    dp.SelectedValue = GrdAssigment.DataKeys[e.Row.RowIndex].Values[1].ToString();
                }
            }
        }

        protected void GrdAssigment_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GrdAssigment.EditIndex = e.NewEditIndex;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void GrdAssigment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrdAssigment.EditIndex = -1;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void GrdAssigment_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int AssignmentID = Convert.ToInt32(GrdAssigment.DataKeys[e.RowIndex].Values[0].ToString());

            Boolean Check = Business.TaskManagment.CheckTaskScheduleOnPresent(AssignmentID);
            if (Check == false)
            {
                DropDownList ddl = (DropDownList) GrdAssigment.Rows[e.RowIndex].FindControl("ddlUserList");
                Business.TaskManagment.UpdateAssignedUserTAsk(AssignmentID, Convert.ToInt32(ddl.SelectedValue));

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "User updated successfully";
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Task allredy scheduled, you can not change user";
            }

            GrdAssigment.EditIndex = -1;
            BindAssignment(Convert.ToInt32(ViewState["TaskID"]));
        }

        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdoStatutory.Checked == true)
                {
                    var complaince = TaskManagment.GetComplianceDetails(Convert.ToInt32(ddlCompliance.SelectedValue));
                    if (complaince.ComplianceType == 0)
                    {
                        txtComplianceType.Text = "Function based";
                    }
                    if (complaince.ComplianceType == 1)
                    {
                        txtComplianceType.Text = "Checklist";
                    }
                    else if (complaince.ComplianceType == 2)
                    {
                        txtComplianceType.Text = "Time Based";
                    }
                    txtComplianceDueDays.Text = Convert.ToString(complaince.DueDate);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }

        }

        protected void ddlInternalCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdoInternal.Checked == true)
                {
                    var complaince = TaskManagment.GetInternalComplianceDetails(Convert.ToInt32(ddlInternalCompliance.SelectedValue));
                    if (complaince.IComplianceType == 0)
                    {
                        txtComplianceType.Text = "Function based";
                    }
                    if (complaince.IComplianceType == 1)
                    {
                        txtComplianceType.Text = "Checklist";
                    }
                    else if (complaince.IComplianceType == 2)
                    {
                        txtComplianceType.Text = "Time Based";
                    }
                    txtComplianceDueDays.Text = Convert.ToString(complaince.IDueDate);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }

        }

        protected void ddlAllTaskType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAllTaskType.SelectedValue != "-1")
            {
                if (ddlAllTaskType.SelectedValue == "0")
                {
                    lblAddTaskType.Visible = true;
                    lblAddSubTask.Visible = false;
                    BindSubTaskTypeDropDown(-1);
                    upTaskDetails.Update();
                }
                else
                {
                    lblAddTaskType.Visible = false;
                    lblAddSubTask.Visible = false;
                    BindSubTaskTypeDropDown(Convert.ToInt32(ddlAllTaskType.SelectedValue));
                }
            }
        }

        protected void lblAddTaskType_Click(object sender, EventArgs e)
        {
            tbxTaskType.Text = "";
            CuValAddTaskType.ErrorMessage = "";
            UpdatePanel4.Update();
        }

        protected void lblAddSubTask_Click(object sender, EventArgs e)
        {
            tbxSubTaskType.Text = "";
            CuValSubTaskType.ErrorMessage = "";
            UpdatePanel5.Update();
        }

        protected void UpdatePanel4_Load(object sender, EventArgs e)
        {

        }

        protected void UpdatePanel5_Load(object sender, EventArgs e)
        {

        }

        protected void btnTaskSubType_Click(object sender, EventArgs e)
        {
            try
            {
                TaskSubType objtask = new TaskSubType();
                if (!string.IsNullOrEmpty(tbxSubTaskType.Text))
                {
                    objtask.Name = tbxSubTaskType.Text;
                    objtask.CreatedOn = DateTime.Now;
                }
                if (!string.IsNullOrEmpty(ddlAllTaskType.SelectedValue))
                {
                    if (ddlAllTaskType.SelectedValue != "-1")
                    {
                        objtask.TaskTypeID = Convert.ToInt32(ddlAllTaskType.SelectedValue);
                        if (objtask != null)
                        {
                            if (TaskManagment.IsExistSubTaskType(objtask))
                            {
                                TaskManagment.CreateSubTaskType(objtask);
                                BindSubTaskTypeDropDown(Convert.ToInt32(ddlAllTaskType.SelectedValue));
                                upTaskDetails.Update();
                                CuValSubTaskType.IsValid = false;
                                CuValSubTaskType.ErrorMessage = "Subtask type added successfully";
                                tbxSubTaskType.Text = "";
                            }
                            else
                            {
                                CuValSubTaskType.IsValid = false;
                                CuValSubTaskType.ErrorMessage = "Subtask type already exist";
                            }
                        }
                        else
                        {
                            CuValSubTaskType.IsValid = false;
                            CuValSubTaskType.ErrorMessage = "TaskType should not be emty";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CuValSubTaskType.IsValid = false;
                CuValSubTaskType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnAddTaskType_Click(object sender, EventArgs e)
        {
            try
            {
                TaskType objtask = new TaskType();
                if (!string.IsNullOrEmpty(tbxTaskType.Text))
                {
                    objtask.Name = tbxTaskType.Text;
                    objtask.CreatedOn = DateTime.Now;
                }
                if (objtask != null)
                {
                    if (TaskManagment.IsExistTaskType(objtask))
                    {
                        TaskManagment.CreateTaskType(objtask);
                        BindTaskTypeDropDown();
                        upTaskDetails.Update();
                        CuValAddTaskType.IsValid = false;
                        CuValAddTaskType.ErrorMessage = "Task type added successfully";
                        tbxTaskType.Text = "";
                    }
                    else
                    {
                        CuValAddTaskType.IsValid = false;
                        CuValAddTaskType.ErrorMessage = "Task type already exist";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CuValAddTaskType.IsValid = false;
                CuValAddTaskType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlSubTaskType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubTaskType.SelectedValue != "-1")
            {
                if (ddlSubTaskType.SelectedValue == "0")
                {
                    lblAddSubTask.Visible = true;
                }
                else
                {
                    lblAddSubTask.Visible = false;
                }
            }
        }

        protected void btnTaskRepeater_Click(object sender, EventArgs e)
        {

        }

        protected void showSampleFormDetails(long taskID)
        {
            if (taskID != 0)
            {
                var taskForm = Business.TaskManagment.GetTaskFormByTaskID(taskID);

                if (taskForm != null)
                {
                    if (taskForm != null)
                    {
                        string fileName = taskForm.FileName;
                        string ext = Path.GetExtension(fileName);
                        //lblSampleForm.Text = taskForm.FileName.Substring(0, 10) + "..." + ext;
                        lblSampleForm.Text = fileName;
                        lblSampleForm.ToolTip = taskForm.FileName;
                    }
                    else
                    {
                        lblSampleForm.Text = "No Form Uploaded";
                        lblSampleForm.ToolTip = "No Sample Form Uploaded";
                    }
                }
                else
                {
                    lblSampleForm.Text = "No Form Uploaded";
                    lblSampleForm.ToolTip = "No Sample Form Uploaded";
                }
            }
            else
            {
                lblSampleForm.Text = "No Form Uploaded";
                lblSampleForm.ToolTip = "No Sample Form Uploaded";
            }
        }

        protected void tvFilterLocationTask_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocationTask.Text = tvFilterLocationTask.SelectedNode.Text;
        }

        protected void linkbutton_onclick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Controls/frmUpcomingCompliancessNew.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}