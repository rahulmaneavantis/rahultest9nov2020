﻿
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class PaidAmountController : Controller
    {
       
        // GET: PaidAmount
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult getAllExapanses()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int empId = Convert.ToInt32(Session["userID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                var expanseList = entities.TM_GetExpenseDetails(empId, "PDGA").ToList();
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        [HttpGet]
        public ActionResult getAllStatus()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                List<int> vl = new List<int>();
                vl.Add(5);
                vl.Add(7);
                entities.Configuration.ProxyCreationEnabled = false;
                var cust = entities.SalesBillStatusTypes.Where(entry => vl.Contains(entry.ID)).Select(model => new { model.ID, model.StatusType }).ToList();
                return Json(cust, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getAllfinanceApprovels(int? tId)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int rId = Convert.ToInt32(Session["UserID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                List<EmployeeTimeSheet> timesheet = new List<EmployeeTimeSheet>();
                var expanseList = (from usr in entities.TM_PaidExpenseDetails(tId)
                                   select usr).ToList();
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        [HttpPost]
        public ActionResult SaveApprovals(ManagerApprovalEntry TimeSheets)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (SaleBillApprovalTransactoin time in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        var status = entities.SalesExpensesTransactionDetails.Where(y => y.TransactionID == time.SalesExpensesTransactionID).FirstOrDefault();

                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();

                      
                        emp.TransactionDate = DateTime.Now;
                        emp.SalesExpensesTransactionID = time.SalesExpensesTransactionID;
                        emp.SalesExpensesTransactionDetailID = time.SalesExpensesTransactionDetailID;
                        emp.ApprovalFlag = "F";
                        emp.ActionTypeID = time.ActionTypeID;
                        emp.ApprovallevelID = empId;
                        emp.Comments = time.Comments;
                        
                        emp.StatusID = 7;
                        entities.SaleBillApprovalTransactoins.Add(emp);
                        entities.SaveChanges();

                        var totaldetailID = entities.SalesExpensesTransactionDetails.Where(x => x.TransactionID == time.SalesExpensesTransactionID && x.IsDeleted == false).ToList().Count;
                        var totalRecentTransactionbycurrentUser = entities.RecentExpenseTransactionViews.Where(x => x.TransactionID == time.SalesExpensesTransactionID 
                        && x.CreatedBy == empId && x.StatusID==7).ToList();                       
                        if (totaldetailID == totalRecentTransactionbycurrentUser.Count())
                        {
                            var billstatus = entities.SalesExpensesTransactions.Where(x => x.TransactionID == time.SalesExpensesTransactionID).FirstOrDefault();
                            billstatus.BillStatusTypeID = 7;
                            status.ActionTypeID = 1;
                        }                                               
                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                result = "Approval  has been Processed Successfully!";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}