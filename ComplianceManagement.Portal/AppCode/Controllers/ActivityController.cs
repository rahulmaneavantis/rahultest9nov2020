﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract  ;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class ActivityController : Controller
    {
        private PracticeEntities db = new PracticeEntities();

        // GET: Activity
        public ActionResult Activities()
        {
            return View();
        }
        public ActionResult KednoActivites()
        {
            return View();
        }
        //Fetching all Activity details from database
        public JsonResult getAll()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var activityList = db.TM_GetActivityDetails().ToList();
          
            
            var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
            JsonResult.MaxJsonLength = int.MaxValue;
            return JsonResult;
        }

        //<-- Get Activity by ID -->
        public JsonResult getActivityByNo(string id)
        {
            try
            {
                int no = Convert.ToInt32(id);
                var activityList = db.TM_GetActivityDetails().ToList();

                activityList = activityList.Where(entry => entry.ID == no).ToList();
                return Json(activityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exp)
            {
                LoggerMessage.InsertLog(exp, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Json("Error in getting record !", JsonRequestBehavior.AllowGet);
            }
        }
        //<--Activity record insertion code -->  
        public string AddActivity(Activity act)
        {
            bool isExist = db.Activities.Where(x => x.ActivityName == act.ActivityName).Count() > 0 ? true : false;
            if (act != null && isExist==false)
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        Activity activ = new Activity();
                        activ.ID = act.ID;
                        activ.ActivityName = act.ActivityName;
                        activ.ProductID = act.ProductID;
                        activ.IsDeleted = false;
                        db.Activities.Add(activ);
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        return ex.InnerException.ToString();
                    }

                    return "Activity added Successfully";
                }

            }
            else
            {
                return "Addition of Activity unsucessfull as Act Name already Exists !";
            }
        }

      
        //<-Activity  record updation code -->
        public string UpdateActivity(Activity act)
        {
            bool isExist = db.Activities.Where(x => x.ActivityName == act.ActivityName).Count() > 0 ? true : false;
            if (act != null && isExist == false)
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        int no = Convert.ToInt32(act.ID);
                        var activ = db.Activities.Where(x => x.ID == no).FirstOrDefault();
                        activ.ID = act.ID;
                        activ.ActivityName = act.ActivityName;
                        activ.ProductID = act.ProductID;
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        return ex.InnerException.ToString();
                    }
                    return "Activity Updated Successfully";

                }
            }
            else
            {
                return "Invalid Activity";
            }
        }
        //Delete Activity  by Id
        public string DeleteActivity(string Id)
        {
            if (!String.IsNullOrEmpty(Id))
            {
                int aId = Int32.Parse(Id);
                var _act = db.Activities.Find(aId);
                bool isExist = db.EmployeeTimeSheets.Where(x => x.ActivityID == aId).Count() > 0 ? true : false;
                if (_act != null && isExist == false)
                {
                    try
                    {
                        using (var dbContextTransaction = db.Database.BeginTransaction())
                        {
                            try
                            {
                                _act.IsDeleted = true;
                                db.SaveChanges();
                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        return "Selected Activity record deleted sucessfully";
                    }
                    catch (Exception)
                    {
                        return "Activity details not found";
                    }
                }
                else
                {
                    return "Activity details is in use";
                }
            }
            else
            {
                return "Invalid operation";
            }
        }
        //Fetching Product Value from Database
        
        [HttpGet]
        public ActionResult GetProductName()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var role = db.TM_GetProductDetails().Select(model => new { model.Id, model.Name }).ToList();
            return Json(role, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckExistance(string activityname)
        {
            bool isExist = db.Activities.Where(x => x.ActivityName == activityname && x.IsDeleted==false).Count() > 0 ? true : false;
            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

    }
}