﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class SalesController : Controller
    {
  
        // GET: Sales
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult getreportingmanager()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int empId = Convert.ToInt32(Session["userID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                var expanseList = entities.TM_GetReportingManager(empId).ToList();              
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        ////Fetching all User details from database
        public JsonResult getAllExapanses()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int empId = Convert.ToInt32(Session["userID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                var expanseList = entities.TM_GetExpenseDetails(empId, "EXPE").ToList();                
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public ActionResult ExpansesView()
        {
            return View();
        }
        public ActionResult testdemo()
        {
            return View();
        }
        public ActionResult UserExpansesView()
        {
            return View();
        }

        //Edit Expanses based upon Id

        [HttpPost]
        public JsonResult Edit(int? tId)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                SalesExpensesTransaction expanse = new SalesExpensesTransaction();
                var expenses = (from o in entities.SalesExpensesTransactions
                                where o.TransactionID == tId && o.IsDeleted == false
                                select new
                                {
                                    TransactionID = o.TransactionID,
                                    CustomerID = o.CustomerID,
                                    Narration = o.Narration,
                                    IsRecoverable = o.IsRecoverable,
                                    BillStatusTypeID = o.BillStatusTypeID,
                                }).FirstOrDefault();
                expanse = new SalesExpensesTransaction
                {
                    TransactionID = expenses.TransactionID,
                    CustomerID = expenses.CustomerID,
                    Narration = expenses.Narration,
                    IsRecoverable = expenses.IsRecoverable,
                    BillStatusTypeID = expenses.BillStatusTypeID,
                };
                List<SalesExpensesTransactionDetailRahul> expansedetails = new List<SalesExpensesTransactionDetailRahul>();
                var expansedetail = (from i in entities.TM_GetLatestExpenseDetails(tId)
                                     select new SalesExpensesTransactionDetailRahul
                                     {
                                         TransactionID = i.TransactionDetailID,
                                         TransactionDetailID = i.TransactionDetailID,
                                         SalesExpID = i.SalesExpID,
                                         Amount = i.Amount,
                                         IsProofDoc = i.IsProofDoc,
                                         TranscationDetailDate = i.TranscationDetailDate,
                                         Remark = i.Remark,
                                         salesTransactionID = i.salesTransactionID,
                                         Comments = i.Comments,
                                         StatusID = i.StatusID,
                                         Status = i.Status,
                                     }).ToList();
                foreach (var detail in expansedetail)
                {
                    expansedetails.Add(new SalesExpensesTransactionDetailRahul
                    {
                        TransactionID = detail.TransactionID,
                        TransactionDetailID = detail.TransactionDetailID,
                        SalesExpID = detail.SalesExpID,
                        TranscationDetailDate = detail.TranscationDetailDate,
                        Amount = detail.Amount,
                        IsProofDoc = detail.IsProofDoc,
                        Remark = detail.Remark,
                        salesTransactionID = detail.salesTransactionID,
                        Comments = detail.Comments,
                        StatusID = detail.StatusID,
                        Status = detail.Status,
                    });
                }
                List<SalesExpensesTransactionDocument> expansedocuments = new List<SalesExpensesTransactionDocument>();
                foreach (var item in expansedetail)
                {
                    var expansedocument = (from i in entities.SalesExpensesTransactionDocuments
                                           where i.TransactionDetailID == item.TransactionDetailID
                                           select new
                                           {
                                               TransactionDetailID = i.TransactionDetailID,
                                               FileName = i.FileName,
                                               DocumentTransactionID = i.DocumentTransactionID,
                                           }).ToList();
                    foreach (var subitem in expansedocument)
                    {
                        expansedocuments.Add(new SalesExpensesTransactionDocument { TransactionDetailID = subitem.TransactionDetailID, FileName = subitem.FileName, DocumentTransactionID = subitem.DocumentTransactionID });
                    }
                }

                ExpenseDetail details = new ExpenseDetail();
                details.Expenses = expanse;
                details.ExpensesDetails = expansedetails;
                details.ExpensesDocuments = expansedocuments;
                return Json(details, JsonRequestBehavior.AllowGet);
            }
        }
        //Save or update values to database
        [HttpPost]
        public ActionResult SaveSalesDetails()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                string result = "Error! User Expanses Is Not Complete!";
                int empId = Convert.ToInt32(Session["userID"]);

                SalesDetailsEntry SDE = JsonConvert.DeserializeObject<SalesDetailsEntry>(Request.Form["details"]);
                var traId = entities.SalesExpensesTransactions.Where(x => x.TransactionID == SDE.TransactionID).FirstOrDefault();
                if (traId == null)
                {
                    #region Save Code
                    try
                    {
                        var fianId = entities.TM_GetFinancialyearID(DateTime.Now).ToList();
                        foreach (var f in fianId)
                        {
                            var Tserial = entities.TM_SalesDocTransactionserialNumber(f).FirstOrDefault();
                            string directoryPath = Server.MapPath("~/ExpenseDocument/" + Tserial);
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(directoryPath);
                            }
                            //Save the Files.
                            for (int i = 0; i < Request.Files.Count; i++)
                            {
                                HttpPostedFileBase postedFile = Request.Files[i];
                                string fileName = Path.GetFileName(postedFile.FileName);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                postedFile.SaveAs(Server.MapPath(finalPath));
                            }


                            // Save to database.
                            SalesDetailsEntry expanses = JsonConvert.DeserializeObject<SalesDetailsEntry>(Request.Form["details"]);
                            SalesExpensesTransaction tex = new SalesExpensesTransaction();
                            int tid = Convert.ToInt32(expanses.TransactionID);
                            int custid = Convert.ToInt32(expanses.CustomerID);
                            tex.TransactionID = tid;
                            tex.TransactionDate = DateTime.Now.Date;
                            tex.CustomerID = custid;
                            tex.TransactionSerial = Tserial;
                            tex.EmployeeID = empId;
                            tex.IsRecoverable = expanses.IsRecoverable;
                            tex.FinancialYearID = f;
                            tex.BillStatusTypeID = expanses.BillStatusTypeID;
                            tex.Narration = expanses.Narration;
                            tex.CreatedBy = empId;
                            tex.CreatedDate = DateTime.Now;
                            entities.SalesExpensesTransactions.Add(tex);
                            entities.SaveChanges();

                            int set = tex.TransactionID;
                            if (expanses.details != null)
                            {
                                foreach (var sd in expanses.details)
                                {
                                    SalesExpensesTransactionDetail tdetails = new SalesExpensesTransactionDetail();
                                    tdetails.TransactionID = set;
                                    int tdId = Convert.ToInt32(sd.TransactionDetailID);
                                    tdetails.TransactionDetailID = tdId;
                                    tdetails.TranscationDetailDate = (sd.TranscationDetailDate);
                                    tdetails.Amount = sd.Amount;
                                    tdetails.IsProofDoc = sd.IsProofDoc;
                                    tdetails.SalesExpID = sd.SalesExpID;
                                    tdetails.CreatedBy = empId;
                                    tdetails.CreatedDate = DateTime.Now;
                                    tdetails.ActionTypeID = 3;
                                    tdetails.Remark = sd.Remark;
                                    entities.SalesExpensesTransactionDetails.Add(tdetails);
                                    entities.SaveChanges();

                                    int tdId1 = tdetails.TransactionDetailID;

                                    SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                    int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                    emp.TransactionDate = DateTime.Now;
                                    emp.SalesExpensesTransactionID = set;
                                    emp.SalesExpensesTransactionDetailID = tdId1;
                                    emp.ApprovalFlag = "E";
                                    emp.ActionTypeID = 3;
                                    emp.ApprovallevelID = empId;
                                    emp.Comments = sd.Remark;
                                    emp.StatusID = expanses.BillStatusTypeID;
                                    emp.TransactionID = sbatID;
                                    entities.SaleBillApprovalTransactoins.Add(emp);
                                    entities.SaveChanges();


                                    if (sd.Names != null)
                                    {
                                        for (int j = 0; j < sd.Names.Length; j++)
                                        {
                                            SalesExpensesTransactionDocument d = new SalesExpensesTransactionDocument();
                                            d.TransactionDetailID = tdId1;
                                            string finalPath = Path.Combine(directoryPath, sd.Names[j]);
                                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            d.FileName = finalPath;
                                            d.CreatedBy = empId;
                                            d.CreatedDate = DateTime.Now;
                                            entities.SalesExpensesTransactionDocuments.Add(d);
                                        }
                                    }
                                }
                            }
                            entities.SaveChanges();
                            result = "User Expenses added Successfully!";
                        }

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    return Json(result, JsonRequestBehavior.AllowGet);
                    #endregion
                }
                //update values to database
                else
                {
                    #region Update Code
                    try
                    {
                        string directoryPath = Server.MapPath("~/ExpenseDocument/" + traId.TransactionSerial);
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }
                        //Save the Files.
                        for (int i = 0; i < Request.Files.Count; i++)
                        {
                            HttpPostedFileBase postedFile = Request.Files[i];
                            string fileName = Path.GetFileName(postedFile.FileName);
                            string finalPath = Path.Combine(directoryPath, fileName);
                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            postedFile.SaveAs(Server.MapPath(finalPath));
                        }

                        int tid = Convert.ToInt32(SDE.TransactionID);
                        int custid = Convert.ToInt32(SDE.CustomerID);

                        SalesExpensesTransaction tex = new SalesExpensesTransaction();
                        tex.TransactionID = tid;
                        tex.TransactionDate = traId.TransactionDate;
                        tex.CustomerID = custid;
                        tex.EmployeeID = empId;
                        tex.TransactionSerial = traId.TransactionSerial;
                        if (traId.BillStatusTypeID == 6 && SDE.BillStatusTypeID == 2)
                        {
                            tex.BillStatusTypeID = 3;
                        }
                        else if(traId.BillStatusTypeID == 3)
                        {
                            tex.BillStatusTypeID = 3;
                        }
                        else if (traId.BillStatusTypeID == 5)
                        {
                            tex.BillStatusTypeID = 5;
                        }
                        else
                        {
                            tex.BillStatusTypeID = SDE.BillStatusTypeID;
                        }

                        tex.IsRecoverable = SDE.IsRecoverable;
                        tex.FinancialYearID = traId.FinancialYearID;
                        tex.Narration = SDE.Narration;
                        tex.UpdatedBy = empId;
                        tex.UpdatedDate = DateTime.Now;
                        UpdateSalesExpensesTransaction(tex);

                        int set = tex.TransactionID;
                        if (SDE.details != null)
                        {
                            foreach (var sd in SDE.details)
                            {
                                var SETDdetailsId = entities.SalesExpensesTransactionDetails.Where(x => x.TransactionDetailID == sd.TransactionDetailID).FirstOrDefault();
                                if (SETDdetailsId == null)
                                {
                                    int tdId = Convert.ToInt32(sd.TransactionDetailID);
                                    SalesExpensesTransactionDetail SETDdetails = new SalesExpensesTransactionDetail();
                                    SETDdetails.TransactionID = set;
                                    SETDdetails.TransactionDetailID = tdId;
                                    SETDdetails.TranscationDetailDate = DateTime.Now;
                                    SETDdetails.Amount = sd.Amount;
                                    SETDdetails.IsProofDoc = sd.IsProofDoc;
                                    SETDdetails.SalesExpID = sd.SalesExpID;
                                    SETDdetails.ActionTypeID = 3;
                                    SETDdetails.CreatedBy = empId;
                                    SETDdetails.CreatedDate = DateTime.Now;
                                    SETDdetails.Remark = sd.Remark;
                                    entities.SalesExpensesTransactionDetails.Add(SETDdetails);
                                    entities.SaveChanges();
                                    int tdetaiId = SETDdetails.TransactionDetailID;


                                    SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                    int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                    emp.TransactionDate = DateTime.Now;
                                    emp.SalesExpensesTransactionID = set;
                                    emp.SalesExpensesTransactionDetailID = tdetaiId;
                                    emp.ApprovalFlag = "E";
                                    emp.ActionTypeID = 3;
                                    emp.ApprovallevelID = empId;
                                    emp.Comments = sd.Remark;
                                    emp.StatusID = tex.BillStatusTypeID;
                                    emp.TransactionID = sbatID;
                                    entities.SaleBillApprovalTransactoins.Add(emp);
                                    entities.SaveChanges();


                                    if (sd.Names != null)
                                    {
                                        for (int j = 0; j < sd.Names.Length; j++)
                                        {
                                            SalesExpensesTransactionDocument d = new SalesExpensesTransactionDocument();
                                            d.TransactionDetailID = tdetaiId;
                                            string finalPath = Path.Combine(directoryPath, sd.Names[j]);
                                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            d.FileName = finalPath;
                                            d.CreatedBy = empId;
                                            d.CreatedDate = DateTime.Now;
                                            entities.SalesExpensesTransactionDocuments.Add(d);
                                            entities.SaveChanges();
                                        }
                                    }

                                }
                                else
                                {
                                    int tdId = Convert.ToInt32(sd.TransactionDetailID);
                                    SalesExpensesTransactionDetail tdetails = new SalesExpensesTransactionDetail();
                                    tdetails.TransactionID = set;
                                    tdetails.TransactionDetailID = SETDdetailsId.TransactionDetailID;
                                    tdetails.TranscationDetailDate = sd.TranscationDetailDate;
                                    tdetails.Amount = sd.Amount;
                                    tdetails.IsProofDoc = sd.IsProofDoc;
                                    tdetails.SalesExpID = sd.SalesExpID;
                                    tdetails.CreatedBy = empId;
                                    tdetails.CreatedDate = SETDdetailsId.CreatedDate;
                                    tdetails.UpdatedBy = empId;
                                    tdetails.ActionTypeID = 3;
                                    tdetails.UpdatedDate = DateTime.Now;
                                    tdetails.Remark = sd.Remark;
                                    UpdateSalesExpensesTransactionDetail(tdetails);

                                    if (sd.StatusID == 1)
                                    {
                                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                        int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                        emp.TransactionDate = DateTime.Now;
                                        emp.SalesExpensesTransactionID = set;
                                        emp.SalesExpensesTransactionDetailID = SETDdetailsId.TransactionDetailID;
                                        emp.ApprovalFlag = "E";
                                        emp.ActionTypeID = 3;
                                        emp.ApprovallevelID = empId;
                                        emp.Comments = sd.Remark;
                                        emp.StatusID = tex.BillStatusTypeID;
                                        emp.TransactionID = sbatID;
                                        UpdateSaleBillApprovalTransactoin(emp);
                                    }
                                    else if (sd.StatusID == 6)
                                    {
                                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                        int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                        emp.TransactionDate = DateTime.Now;
                                        emp.SalesExpensesTransactionID = set;
                                        emp.SalesExpensesTransactionDetailID = SETDdetailsId.TransactionDetailID;
                                        emp.ApprovalFlag = "E";
                                        emp.ActionTypeID = 3;
                                        emp.ApprovallevelID = empId;
                                        emp.Comments = sd.Remark;
                                        emp.StatusID = tex.BillStatusTypeID;
                                        emp.TransactionID = sbatID;
                                        entities.SaleBillApprovalTransactoins.Add(emp);
                                        entities.SaveChanges();
                                    }
                                    else if (sd.StatusID == 5)
                                    {
                                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                        int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                        emp.TransactionDate = DateTime.Now;
                                        emp.SalesExpensesTransactionID = set;
                                        emp.SalesExpensesTransactionDetailID = SETDdetailsId.TransactionDetailID;
                                        emp.ApprovalFlag = "E";
                                        emp.ActionTypeID = 3;
                                        emp.ApprovallevelID = empId;
                                        emp.Comments = sd.Remark;
                                        emp.StatusID = 5;
                                        emp.TransactionID = sbatID;
                                        entities.SaleBillApprovalTransactoins.Add(emp);
                                        entities.SaveChanges();
                                    }
                                    else if (sd.StatusID != 3)
                                    {
                                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                                        int sbatID = Convert.ToInt32(sd.salesTransactionID);
                                        emp.TransactionDate = DateTime.Now;
                                        emp.SalesExpensesTransactionID = set;
                                        emp.SalesExpensesTransactionDetailID = SETDdetailsId.TransactionDetailID;
                                        emp.ApprovalFlag = "E";
                                        emp.ActionTypeID = 3;
                                        emp.ApprovallevelID = empId;
                                        emp.Comments = sd.Remark;
                                        emp.StatusID = tex.BillStatusTypeID;
                                        emp.TransactionID = sbatID;
                                        entities.SaleBillApprovalTransactoins.Add(emp);
                                        entities.SaveChanges();
                                    }
                                   


                                    if (sd.Names != null)
                                    {
                                        for (int j = 0; j < sd.Names.Length; j++)
                                        {
                                            if (sd.Names[j] != null)
                                            {
                                                SalesExpensesTransactionDocument d = new SalesExpensesTransactionDocument();
                                                d.TransactionDetailID = tdId;
                                                string finalPath = Path.Combine(directoryPath, sd.Names[j]);
                                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                d.FileName = finalPath;
                                                d.UpdatedBy = empId;
                                                d.UpdatedDate = DateTime.Now;
                                                entities.SalesExpensesTransactionDocuments.Add(d);
                                                entities.SaveChanges();
                                            }
                                        }
                                    }

                                }
                            }
                            entities.SaveChanges();
                            result = "User Expenses has been Updated Successfully!";
                        }

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    return Json(result, JsonRequestBehavior.AllowGet);
                    #endregion
                }
            }
        }

        public void UpdateSaleBillApprovalTransactoin(SaleBillApprovalTransactoin SBAT)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    SaleBillApprovalTransactoin SETDToUpdate = (from row in entities.SaleBillApprovalTransactoins
                                                                where row.TransactionID == SBAT.TransactionID
                                                                select row).FirstOrDefault();

                    if (SETDToUpdate != null)
                    {
                        SETDToUpdate.Comments = SBAT.Comments;
                        SETDToUpdate.StatusID = SBAT.StatusID;                     
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void UpdateSalesExpensesTransactionDetail(SalesExpensesTransactionDetail SETD)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    SalesExpensesTransactionDetail SETDToUpdate = (from row in entities.SalesExpensesTransactionDetails
                                                                   where row.TransactionDetailID == SETD.TransactionDetailID
                                                                   select row).FirstOrDefault();

                    if (SETDToUpdate != null)
                    {                     
                        SETDToUpdate.TranscationDetailDate = SETD.TranscationDetailDate;
                        SETDToUpdate.Amount = SETD.Amount;
                        SETDToUpdate.IsProofDoc = SETD.IsProofDoc;
                        SETDToUpdate.Remark = SETD.Remark + "";
                        SETDToUpdate.SalesExpID = SETD.SalesExpID;
                        SETDToUpdate.UpdatedBy = SETD.UpdatedBy;
                        SETDToUpdate.ActionTypeID = 3;
                        SETDToUpdate.UpdatedDate = DateTime.Now;                     
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void UpdateSalesExpensesTransaction(SalesExpensesTransaction SET)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    SalesExpensesTransaction SETToUpdate = (from row in entities.SalesExpensesTransactions
                                                            where row.TransactionID == SET.TransactionID
                                                            select row).FirstOrDefault();

                    if (SETToUpdate != null)
                    {
                        SETToUpdate.TransactionDate = SET.TransactionDate;
                        SETToUpdate.CustomerID = SET.CustomerID;
                        SETToUpdate.EmployeeID = SET.EmployeeID;
                        SETToUpdate.TransactionSerial = SET.TransactionSerial;
                        SETToUpdate.BillStatusTypeID = SET.BillStatusTypeID;
                        SETToUpdate.IsRecoverable = SET.IsRecoverable;
                        SETToUpdate.FinancialYearID = SET.FinancialYearID;
                        SETToUpdate.Narration = SET.Narration;                                                
                        SETToUpdate.UpdatedBy = SET.UpdatedBy;
                        SETToUpdate.UpdatedDate = DateTime.Now;                                              
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //Delete records based upon TranscationId
        [HttpPost]
        public JsonResult DeleteEmployeesalesexpanse(int? tId)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                SalesExpensesTransaction SETToUpdate = (from row in entities.SalesExpensesTransactions
                                                        where row.TransactionID == tId
                                                        select row).FirstOrDefault();

                if (SETToUpdate != null)
                {
                    SETToUpdate.IsDeleted = true;                    
                    entities.SaveChanges();
                }
            }          
            return Json("Expense Details has been deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        //Delete records based upon TranscationdetailId
        [HttpPost]
        public ActionResult Delete(List<SalesExpensesTransactionDetail> items)
        {
            string result = "Error! User expanses Entry Is Not Complete!";
            using (PracticeEntities entities = new PracticeEntities())
            {

                try
                {
                    if (items != null)
                    {
                        foreach (SalesExpensesTransactionDetail setting in items)
                        {
                            if (setting.TransactionDetailID != 0)
                            {
                                SalesExpensesTransactionDetail SETToUpdate = (from row in entities.SalesExpensesTransactionDetails
                                                                              where row.TransactionDetailID == setting.TransactionDetailID
                                                                              select row).FirstOrDefault();

                                if (SETToUpdate != null)
                                {
                                    SETToUpdate.IsDeleted = true;
                                    SETToUpdate.UpdatedBy = setting.UpdatedBy;
                                    SETToUpdate.UpdatedDate = DateTime.Now;
                                    entities.SaveChanges();

                                    var totaldetailID = entities.SalesExpensesTransactionDetails.Where(x => x.TransactionID == SETToUpdate.TransactionID && x.IsDeleted == false).ToList().Count;
                                    var totalRecentTransactionbycurrentUser = entities.RecentExpenseTransactionViews.Where(x => x.TransactionID == SETToUpdate.TransactionID).ToList();

                                    if (totaldetailID == totalRecentTransactionbycurrentUser.Count())
                                    {
                                        var billstatus = entities.SalesExpensesTransactions.Where(x => x.TransactionID == SETToUpdate.TransactionID).FirstOrDefault();
                                        var chekcommanstatus = totalRecentTransactionbycurrentUser.Select(a => a.Status).Distinct().ToList();
                                        if (chekcommanstatus.Count == 1)
                                        {
                                            var aa = totalRecentTransactionbycurrentUser.Select(a => a.StatusID).FirstOrDefault();
                                            if (aa == 3)
                                            {
                                                billstatus.BillStatusTypeID = 3;
                                                entities.SaveChanges();
                                            }
                                            else if (aa == 5)
                                            {
                                                billstatus.BillStatusTypeID = 5;
                                                entities.SaveChanges();
                                            }
                                            else
                                            {
                                                billstatus.BillStatusTypeID = aa;
                                                entities.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage_TM.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                result = "User expanses has been Deleted Successfully !";

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteNew(int ID)
        {
            string result = "Error! User expanses Entry Is Not Complete!";
           
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Get Expense Type from database
        public ActionResult GetExpanseType()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;                
                var exp = entities.SalesExpenses.Select(model => new { EXPID = model.ID, EXPName = model.ExpName }).ToList();           
                return Json(exp, JsonRequestBehavior.AllowGet);
            }
        }


        //Get Expense Type from database
        public ActionResult GetExpanseTypes()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var exp = entities.SalesExpenses.Select(model => new { model.ID, model.ExpName }).ToList();
                return Json(exp, JsonRequestBehavior.AllowGet);
            }
        }
        //Get Finance Year from database
        public ActionResult GetFiancialYear()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var fin = entities.SalesFinancialYears.Select(model => new { model.ID, model.FinancialYear }).ToList();
                return Json(fin, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Multiplefileupload()
        {
            return View();
        }
        public ActionResult OrderDocumentsFileupload()
        {
            return View();
        }

        [HttpGet]
        public ActionResult getAllCustomer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var cust = entities.Customers.Select(model => new { model.ID, model.Name }).ToList();
                return Json(cust, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult getAllStatus()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var cust = entities.SalesBillStatusTypes.Where(entry => entry.ID != 6).Select(model => new { model.ID, model.StatusType }).ToList();
                return Json(cust, JsonRequestBehavior.AllowGet);
            }
        }
    }
}