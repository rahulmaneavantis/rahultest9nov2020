﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class FinanceRecoveryController : Controller
    {
        private PracticeEntities db = new PracticeEntities();
        // GET: FinanceRecovery
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexNew()
        {
            return View();
        }
        public JsonResult getAllExpenseRecoveryDetails()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                //int empId = Convert.ToInt32(Session["userID"]);
                db.Configuration.ProxyCreationEnabled = false;
                var expanseRecoveryList = entities.TM_GetExpenseRecoveryDetails().ToList();
                var JsonResult = Json(expanseRecoveryList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        [HttpGet]
        public static bool GetInvID(long TransactionID)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                var AllDocument = (from data in entities.tbl_PushToInvoice
                                   where data.TransactionID == TransactionID
                                   select data).ToList();

                if (AllDocument.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;

                }
               
            }
        }

       
        [HttpPost]
        public ActionResult SaveInvoice(InvoiceEntry TransactionIDs)
        {

            using (PracticeEntities entities = new PracticeEntities())
            {
                try
                {
                    List<long> TransactionIDList = new List<long>();
                    foreach (var a in TransactionIDs.TransactionID)
                    {
                        
                        string[] split = a.TransactionIDList.Split(',');
                        if (split.Length > 0)
                        {
                            for (int rs = 0; rs < split.Length; rs++)
                            {
                                TransactionIDList.Add(Convert.ToInt32(split[rs].Trim()));
                            }
                        }
                    }
                    if (TransactionIDList.Count > 0)
                    {

                        TransactionIDList.ForEach(EachID =>
                        {
                        var IsExist = GetInvID(Convert.ToInt32(EachID));
                            if (!IsExist)
                            {
                                
                                tbl_PushToInvoice inv = new tbl_PushToInvoice()
                                {
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = Convert.ToString(AuthenticationHelper.UserID),
                                    TransactionID = EachID
                                };

                                CreateInvData(inv);
                                
                            }

                        });
                        
                    }
                }
                catch (Exception ex)
                {
                   // dbContextTransaction.Rollback();
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }

            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public static bool CreateInvData(tbl_PushToInvoice objInv)
        {
            try
            {
                using (PracticeEntities entities = new PracticeEntities())
                {
                    entities.tbl_PushToInvoice.Add(objInv);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {   
                return false;
            }
        }

    }
}