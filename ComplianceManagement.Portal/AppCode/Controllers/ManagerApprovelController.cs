﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class ManagerApprovelController : Controller
    {     
        // GET: ManagerApprovel
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexNew()
        {
            return View();
        }

        public JsonResult getAllExapanses()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int empId = Convert.ToInt32(Session["userID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                var expanseList = entities.TM_GetExpenseDetails(empId, "FMGA").ToList();
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        //public JsonResult getAllmanagerApprovels(DateTime FDate, DateTime TDate)

        public JsonResult getAllmanagerApprovels1(int? tId)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                int rId = Convert.ToInt32(Session["UserID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                List<EmployeeTimeSheet> timesheet = new List<EmployeeTimeSheet>();
                var expanseList = (from usr in entities.TM_GetFirstApprovalDetails(tId)
                                   select usr).ToList();
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        public JsonResult getAllmanagerApprovels(string FDate, string TDate)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                DateTime fdate;
                DateTime tdate;
                try
                {
                    fdate = DateTimeExtensions.GetDate(Convert.ToDateTime(FDate).ToString("dd/MM/yyyy"));
                    tdate = DateTimeExtensions.GetDate(Convert.ToDateTime(TDate).ToString("dd/MM/yyyy"));
                }
                catch (Exception ex)
                {

                    throw;
                }

                int rId = Convert.ToInt32(Session["UserID"]);
                entities.Configuration.ProxyCreationEnabled = false;
                List<EmployeeTimeSheet> timesheet = new List<EmployeeTimeSheet>();              
                var expanseList = (from usr in entities.TM_GetFirstApprovalData(rId, fdate, tdate)
                                   where (usr.TransactionDate) >= fdate && (usr.TransactionDate) <= tdate
                                   select new
                                   {
                                       usr.TransactionID,
                                       usr.TransactionDate,
                                       usr.TransactionDetailID,
                                       usr.EmpID,
                                       usr.ExpenseType,
                                       usr.Expenses,
                                       usr.Customer,
                                       usr.Amount,
                                       usr.FileName,
                                       usr.ApprovallevelID,
                                       usr.Flag,
                                       usr.DocumentTransactionID,
                                       usr.EmpName,
                                       usr.TransactionSerial,
                                       usr.Remark
                                   }).ToList();
                var JsonResult = Json(expanseList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        [HttpPost]
        public ActionResult SaveApprovals(ManagerApprovalEntry TimeSheets)
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                var val = string.Empty;
                try
                {
                    foreach (SaleBillApprovalTransactoin time in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        var status = entities.SalesExpensesTransactionDetails.Where(y => y.TransactionID == time.SalesExpensesTransactionID).FirstOrDefault();

                        SaleBillApprovalTransactoin emp = new SaleBillApprovalTransactoin();
                        emp.TransactionDate = DateTime.Now;
                        emp.SalesExpensesTransactionID = time.SalesExpensesTransactionID;
                        emp.SalesExpensesTransactionDetailID = time.SalesExpensesTransactionDetailID;
                        emp.ApprovalFlag = "M";
                        emp.ActionTypeID = time.ActionTypeID;
                        emp.ApprovallevelID = empId;
                        emp.Comments = time.Comments;
                        emp.TransactionID = time.TransactionID;

                        if (time.ActionTypeID == 1)
                        {
                            emp.StatusID = 3;
                            status.ActionTypeID = 1;
                            val = "Approved";
                        }
                        if (time.ActionTypeID == 2)
                        {
                            emp.StatusID = 4;
                            status.ActionTypeID = 2;
                            val = "Rejected";
                        }
                        entities.SaleBillApprovalTransactoins.Add(emp);
                        entities.SaveChanges();


                        var totaldetailID = entities.SalesExpensesTransactionDetails.Where(x => x.TransactionID == time.SalesExpensesTransactionID && x.IsDeleted==false).ToList().Count;

                        var totalRecentTransactionbycurrentUser = entities.RecentExpenseTransactionViews.Where(x => x.TransactionID == time.SalesExpensesTransactionID && x.CreatedBy == empId).ToList();

                        if (totaldetailID == totalRecentTransactionbycurrentUser.Count())
                        {
                            var billstatus = entities.SalesExpensesTransactions.Where(x => x.TransactionID == time.SalesExpensesTransactionID).FirstOrDefault();
                            var chekcommanstatus = totalRecentTransactionbycurrentUser.Select(a => a.Status).Distinct().ToList();
                            if (chekcommanstatus.Count == 1)
                            {
                                var aa = totalRecentTransactionbycurrentUser.Select(a => a.StatusID).FirstOrDefault();
                                if (aa == 4)
                                {
                                    billstatus.BillStatusTypeID = aa;
                                }
                                else
                                {
                                    billstatus.BillStatusTypeID = 3;
                                }
                            }
                            else
                            {
                                billstatus.BillStatusTypeID = 4;
                            }
                        }
                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                if (!string.IsNullOrEmpty(val))
                {
                    if (val == "Rejected")
                    {
                        result = "Rejection has been Processed Successfully!";
                    }
                    else
                    {
                        result = "Approval has been Processed Successfully!";
                    }
                }
                else
                {
                    result = "Approval has been Processed Successfully!";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }


        //Get Expense Type from database
        public ActionResult GetStatus()
        {
            using (PracticeEntities entities = new PracticeEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var exp = entities.SalesBillActionTypes.Select(model => new { model.ID, model.ActionName }).ToList();
                return Json(exp, JsonRequestBehavior.AllowGet);
            }
        }
    }
}