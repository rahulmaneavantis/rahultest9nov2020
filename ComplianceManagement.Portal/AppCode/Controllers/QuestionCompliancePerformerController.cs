﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class QuestionCompliancePerformerController : Controller
    {
        // GET: QuestionCompliancePerformer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ComplianceApplicabilityPreformerQuestions()
        {
            return View();
        }

        #region  Un Taggged Question  Compliance Mapping
        [HttpGet]
        public ActionResult GetActAgrup()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
              
                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACTGROUP", "PER")                           
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetAct(int groupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACT", "PER")                         
                            where A.ActGroupID == groupid
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

              
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAllCompliances(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_GetActCompliancedetails(actid)
                                    select usr).ToList();

                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        [HttpGet]
        public ActionResult GetQuestionsDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //IsQuestionType
                List<int> d = new List<int>();
                d.Add(1);
                d.Add(3);
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionMasters.Where(x => d.Contains((int)x.IsQuestionType) && x.IsActive == false).Select(model => new { model.ID, model.QuestionName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
      

        [HttpGet]
        public ActionResult GetCategorizationDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.mst_categorization.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetSubCategorizationDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.mst_Subcategorization.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name, model.categorid }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        public class LinkedToALicenseType
        {
            public long LID { get; set; }
            public string LName { get; set; }
            public int LinkedID { get; set; }
        }

        [HttpGet]
        public ActionResult GetLicenseTypesDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<LinkedToALicenseType> expansedetails = new List<LinkedToALicenseType>();
                var expansedetail = entities.Lic_tbl_LicenseType_Master.Where(x => x.IsDeleted == false).Select(model => new { model.ID, model.Name }).ToList();
                foreach (var detail in expansedetail)
                {
                    expansedetails.Add(new LinkedToALicenseType
                    {
                        LID = detail.ID,
                        LName = detail.Name,
                        LinkedID = 1,
                    });
                }
                LinkedToALicenseType cc = new LinkedToALicenseType();
                cc.LID = 0;
                cc.LName = "No";
                cc.LinkedID = 2;
                expansedetails.Add(cc);
                return Json(expansedetails, JsonRequestBehavior.AllowGet);
            }
        }

    
        public string AddQuestionActComplianceMapping(QAC_Mapping detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.ActID;
                        Qmst.ComplianceId = detail.ComplianceID;
                        Qmst.QuestionID = detail.QuestionID;
                        Qmst.ControllId = detail.ControlId;
                        Qmst.AnswerTOQ = detail.ServiceID;
                        Qmst.AnswerValue = detail.AnswerValue;
                        if (detail.ControlId == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerSingle;
                        }
                        else if (detail.ControlId == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerMultiple;
                        }

                        Qmst.StatusId = 2;
                        Qmst.AnsweRangeMin = detail.min;
                        Qmst.AnsweRangeMax = detail.max;

                        Qmst.IslicenseID = detail.IslicenseID;
                        Qmst.LicenseTypeID = detail.LicenseTypeID;
                        Qmst.CategoryID = detail.CategoryID;
                        Qmst.SubCategoryID = detail.SubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        if (QuestionACt_Compliance_MappingExists(Qmst))
                        {
                            result = "Question name already exists";
                        }
                        else
                        {
                            entities.Questions_ActComplianceMapping.Add(Qmst);
                            entities.SaveChanges();
                            var QuestionID = Qmst.Id;

                            result = "Details Saved Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }

        public bool QuestionACt_Compliance_MappingExists(Questions_ActComplianceMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActComplianceMapping
                             where row.IsActive == false
                                  && row.ComplianceId == customer.ComplianceId
                                  && row.QuestionID == customer.QuestionID
                                  && row.ControllId == customer.ControllId
                             select row);

                if (customer.Id > 0)
                {
                    query = query.Where(entry => entry.Id != customer.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        [HttpPost]
        public ActionResult SaveApprovals(QACMappingEntry TimeSheets)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QAC_Mapping detail in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.ActID;
                        Qmst.ComplianceId = detail.ComplianceID;
                        Qmst.QuestionID = detail.QuestionID;
                        Qmst.ControllId = detail.ControlId;
                        Qmst.AnswerTOQ = detail.ServiceID;
                        Qmst.AnswerValue = detail.AnswerValue;
                        Qmst.StatusId = 2;
                        if (detail.ControlId == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerSingle;
                        }
                        else if (detail.ControlId == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.AnswerMultiple;
                        }
                        Qmst.AnsweRangeMin = detail.min;
                        Qmst.AnsweRangeMax = detail.max;
                        Qmst.IslicenseID = detail.IslicenseID;
                        Qmst.LicenseTypeID = detail.LicenseTypeID;
                        Qmst.CategoryID = detail.CategoryID;
                        Qmst.SubCategoryID = detail.SubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        entities.Questions_ActComplianceMapping.Add(Qmst);
                        entities.SaveChanges();
                    }
                    result = "Details Saved Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region Tagged  Question  Compliance Mapping
        public JsonResult getAllTaggedCompliances(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.sp_TaggedGetActCompliancedetails(actid)
                                    select usr).ToList();

                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        public string UpdateTaggedQuestion(QAC_MappingUpdate detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var query = (from row in entities.Questions_ActComplianceMapping
                                     where row.IsActive == false
                                          && row.Id == detail.TaggedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.TaggedActID;
                            query.ComplianceId = detail.TaggedComplianceID;
                            query.QuestionID = detail.TaggedQuestionID;
                            query.ControllId = detail.TaggedControllID;
                            query.AnswerTOQ = detail.TaggedServiceID;
                            query.AnswerValue = detail.TaggedAnswer;
                            if (detail.TaggedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerSingle;
                            }
                            else if (detail.TaggedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerMultiple;
                            }
                            query.StatusId = 2;
                            query.AnsweRangeMin = detail.TaggedAnsweRangeMin;
                            query.AnsweRangeMax = detail.TaggedAnsweRangeMax;

                            query.IslicenseID = detail.TaggedIslicenseID;
                            query.LicenseTypeID = detail.TaggedLicenseTypeID;
                            query.CategoryID = detail.TaggedCategoryID;
                            query.SubCategoryID = detail.TaggedSubCategoryID;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            entities.SaveChanges();
                            result = "Details Updated Successfully!";
                            dbContextTransaction.Commit();
                        }
                        else
                        {
                            result = "Question Not Found!";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        [HttpPost]
        public ActionResult BulkUpdateTaggedQuestion(QACMappingEntryUpdate bupdate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QAC_MappingUpdate detail in bupdate.BulkUpdates)
                    {
                        var query = (from row in entities.Questions_ActComplianceMapping
                                     where row.IsActive == false
                                          && row.Id == detail.TaggedQACMMappingID
                                     select row).FirstOrDefault();
                        if (query != null)
                        {
                            query.ActId = detail.TaggedActID;
                            query.ComplianceId = detail.TaggedComplianceID;
                            query.QuestionID = detail.TaggedQuestionID;
                            query.ControllId = detail.TaggedControllID;
                            query.AnswerTOQ = detail.TaggedServiceID;
                            query.AnswerValue = detail.TaggedAnswer;
                            query.StatusId = 2;
                            if (detail.TaggedControllID == 1)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerSingle;
                            }
                            else if (detail.TaggedControllID == 2)
                            {
                                query.AnsweSingleMulti = detail.TaggedAnswerMultiple;
                            }
                            query.AnsweRangeMin = detail.TaggedAnsweRangeMin;
                            query.AnsweRangeMax = detail.TaggedAnsweRangeMax;
                            query.IslicenseID = detail.TaggedIslicenseID;
                            query.LicenseTypeID = detail.TaggedLicenseTypeID;
                            query.CategoryID = detail.TaggedCategoryID;
                            query.SubCategoryID = detail.TaggedSubCategoryID;
                            query.UpdatedOn = DateTime.Now;
                            query.Updatedby = AuthenticationHelper.UserID;
                            entities.SaveChanges();
                        }
                    }
                    result = "Details Updated Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}