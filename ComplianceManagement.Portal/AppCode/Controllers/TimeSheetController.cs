﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class TimeSheetController : Controller
    {
        private PracticeEntities db = new PracticeEntities();

        public int Id { get; private set; }

        // GET: TimeSheet
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult KendoView()
        {
            return View();
        }
        public ActionResult TimesheetView()
        {
            return View();
        }
        public ActionResult TimesheetViewReport()
        {
            return View();
        }
      
        public JsonResult getAll(string FDate, string TDate)
        {
            DateTime aaa;
            DateTime bbb;
            try
            {
                aaa = DateTimeExtensions.GetDate(Convert.ToDateTime(FDate).ToString("dd/MM/yyyy"));
                bbb = DateTimeExtensions.GetDate(Convert.ToDateTime(TDate).ToString("dd/MM/yyyy"));
            }
            catch (Exception ex)
            {
                throw;
            }

            int empId = Convert.ToInt32(Session["userID"]);
            int roleId = Convert.ToInt32(Session["RoleID"]);
            db.Configuration.ProxyCreationEnabled = false;
            var activityList = db.TM_getTimeSheetAll(empId, aaa.Date, bbb.Date).ToList();

         
            var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
            JsonResult.MaxJsonLength = int.MaxValue;
            return JsonResult;
        }
        public JsonResult getAllTimesheetData()
        {
            int empId = Convert.ToInt32(Session["userID"]);            
            var TimesheetData = db.TM_GetTimeSheetData(empId).ToList(); 
            var JsonResult = Json(TimesheetData, JsonRequestBehavior.AllowGet);
            JsonResult.MaxJsonLength = int.MaxValue;
            return JsonResult;
        }
        public ActionResult SaveTimeSheets(TimeSheetEntry TimeSheets)
        {
            string result = "Error! Time sheet Is Not Complete!";
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (EmployeeTimeSheet time in TimeSheets.timesheets)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        var timedetail = db.EmployeeTimeSheets.Where(x => x.TimeSheetID == time.TimeSheetID).FirstOrDefault();
                        if (timedetail == null)
                        {

                            EmployeeTimeSheet emp = new EmployeeTimeSheet();
                            emp.TimeSheetID = time.TimeSheetID;
                            emp.CustomerID = time.CustomerID;
                            emp.ProjectID = time.ProjectID;
                            emp.ActivityID = time.ActivityID;
                            emp.Date = time.Date.Date;
                            emp.Duration = time.Duration;
                            emp.Description = time.Description;
                            emp.EmployeeID = empId;
                            emp.IsInternalOrCustomer = time.IsInternalOrCustomer;
                            emp.IsDeleted = false;
                            emp.legalentityid = time.legalentityid;
                            db.EmployeeTimeSheets.Add(emp);

                        }
                        else
                        {
                            EmployeeTimeSheet emp = new EmployeeTimeSheet();
                            emp.TimeSheetID = time.TimeSheetID;
                            emp.CustomerID = time.CustomerID;
                            emp.ProjectID = time.ProjectID;
                            emp.ActivityID = time.ActivityID;
                            emp.Duration = time.Duration;
                            emp.Date = time.Date;
                            emp.Description = time.Description;
                            emp.EmployeeID = empId;
                            emp.IsInternalOrCustomer = time.IsInternalOrCustomer;
                            emp.IsDeleted = false;
                            db.Entry(timedetail).CurrentValues.SetValues(emp);
                        }

                        db.SaveChanges();
                    }

                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                result = "Time Entry has been updated Successfully!";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Fetching Customer Value from Database
        [HttpGet]
        public ActionResult GetCustomerName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //int CLI_Id = Convert.ToInt32(Session["CLI_Id"]);
                db.Configuration.ProxyCreationEnabled = false;
                var cust = entities.Customers.Select(model => new { model.ID, model.Name }).ToList();
                return Json(cust, JsonRequestBehavior.AllowGet);
            }
        }

        //Fetching Customer Value from Database
        [HttpGet]      
        public ActionResult GetProductName()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var role = db.TM_GetProductDetails().Select(model => new { model.Id, model.Name }).ToList();
            return Json(role, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProjectName(int CustId)
        {
            Session["CustId1"] = CustId;           
            var products = db.TM_GetProductDetails().Select(model => new { model.Id, model.Name }).ToList();
            //var products = db.TM_GetProductDetailscust(CustId).ToList();
            return Json(products);
        }

        public JsonResult GetBranchName(int CustId)
        {
            Session["CustId1"] = CustId;
            var products = db.TM_GetBranches(CustId,"NA").ToList();
            //var products = db.TM_GetProductDetailscust(CustId).ToList();
            return Json(products);
        }
        public ActionResult GetActivitysNames(int produId)
        {            
            db.Configuration.ProxyCreationEnabled = false;
            var act = db.TM_GetActivityDetails().ToList().Where(x => x.ProductID == produId).ToList();
            return Json(act, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetActivitysName()
        {            
            db.Configuration.ProxyCreationEnabled = false;          
            var activityList = db.TM_GetActivityDetails().ToList();
            return Json(activityList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllActivityName()
        {            
            db.Configuration.ProxyCreationEnabled = false;
            var act = db.Activities.Select(model => new { model.ID, model.ActivityName }).ToList();            
            return Json(act, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        public ActionResult Delete(List<EmployeeTimeSheet> items)
        {
            string result = "Error! Timesheet Entry Is Not Complete!";

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (EmployeeTimeSheet setting in items)
                    {
                        if (setting.TimeSheetID != 0)
                        {
                            db.EmployeeTimeSheets.Remove((from i in db.EmployeeTimeSheets where i.TimeSheetID == setting.TimeSheetID select i).FirstOrDefault());
                            db.SaveChanges();
                        }
                    }
                    db.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                result = "Time Entry has been Deleted Successfully !";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        ////Edit Timesheet  details based upon Id
        [HttpPost]
        public JsonResult getTimesheetByNo(int? id)
        {
            var emptimes = db.TM_TimeSheetByID(id).ToList();
            var products = db.TM_GetProductDetails().ToList();
            var activityList = db.TM_GetActivityDetails().ToList();


            //List<TM_GetBranches_Result> branchList = new List<TM_GetBranches_Result>();
            var branchList = db.TM_GetBranches(-1, "ALL").ToList();
            var clist = db.TM_GetCustomers(-1).ToList();
            List<EmployeeTimeSheet> timesheet = new List<EmployeeTimeSheet>();
           
            foreach (var emps in emptimes)
            {
                timesheet.Add(new EmployeeTimeSheet { TimeSheetID = emps.TimeSheetID, Date = emps.Date, Duration = emps.Duration,
                    CustomerID = emps.CustomerID, ProjectID = emps.ProjectID, ActivityID = emps.ActivityID,
                    legalentityid=emps.legalentityid,
                    Description = emps.Description });
            }
            List<ComplianceManagement.Business.Data.Customer> custList = new List<ComplianceManagement.Business.Data.Customer>();
            foreach (var customer in clist)
            {
                custList.Add(new ComplianceManagement.Business.Data.Customer { ID = customer.ID, Name = customer.Name });
            }

            List<ComplianceManagement.Business.Data.Product> productList = new List<ComplianceManagement.Business.Data.Product>();
            foreach (var product in products)
            {
                productList.Add(new ComplianceManagement.Business.Data.Product { Id = product.Id, Name = product.Name });
            }

            List<Activity> AList = new List<Activity>();
            foreach (var item in emptimes)
            {
                var activityLista = activityList.Where(entry => entry.ProductID == item.ProjectID).ToList();
                foreach (var product in activityLista)
                {
                    AList.Add(new Activity { ID = product.ID, ActivityName = product.ActivityName });
                }
            }

            List<ComplianceManagement.Business.Data.CustomerBranch> masterBList = new List<ComplianceManagement.Business.Data.CustomerBranch>();
            foreach (var item in emptimes)
            {
                var activityLista = branchList.Where(entry => entry.CustomerID == item.CustomerID).ToList();
                foreach (var product in activityLista)
                {
                    masterBList.Add(new ComplianceManagement.Business.Data.CustomerBranch { ID = product.ID, Name = product.Name });
                }
            }
            Timesheets details = new Timesheets();
            details.Timesheets1 = timesheet;
            details.customers = custList;
            details.products = productList;
            details.activity = AList;
            details.branchlist = masterBList;
            return Json(details, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //public JsonResult getTimesheetByNo1(DateTime FDate, DateTime TDate)
        public JsonResult getTimesheetByNo1(string FDate, string TDate)
        {
            Timesheets details = new Timesheets();
            try
            {

                DateTime aaa;
                DateTime bbb;
                try
                {
                    aaa = DateTimeExtensions.GetDate(Convert.ToDateTime(FDate).ToString("dd/MM/yyyy"));
                    bbb = DateTimeExtensions.GetDate(Convert.ToDateTime(TDate).ToString("dd/MM/yyyy"));
                }
                catch (Exception ex)
                {

                    throw;
                }

                int empId = Convert.ToInt32(Session["userID"]);
                int roleId = Convert.ToInt32(Session["RoleID"]);
                List<EmployeeTimeSheet> timesheet = new List<EmployeeTimeSheet>();


                var emptimes = db.TM_getTimeSheetAll(empId, aaa.Date, bbb.Date).ToList();
                var products = db.TM_GetProductDetails().ToList();
                var activityList = db.TM_GetActivityDetails().ToList();
                var branchList = db.TM_GetBranches(-1, "ALL").ToList();
                var clist = db.TM_GetCustomers(-1).ToList();

                for (DateTime i = aaa.Date; i <= bbb.Date; i = i.Date.AddDays(1))
                {
                    List<DateTime> dates = emptimes.Select(x => x.Date).ToList();
                    if (dates.Contains(i))
                    {
                        var emps1 = emptimes.Where(x => x.Date.Date == i.Date.Date).ToList();
                        foreach (var emps in emps1)
                        {

                            timesheet.Add(new EmployeeTimeSheet { TimeSheetID = emps.TimeSheetID, Date = i.Date, Duration = emps.Duration,
                                CustomerID = emps.CustomerID, ProjectID = emps.ProjectID, ActivityID = emps.ActivityID,
                                legalentityid = emps.legalentityid,
                                Description = emps.Description });
                        }
                    }
                    else
                    {
                        var aaaaa = DateTimeExtensions.GetDate(Convert.ToDateTime(i.Date).ToString("dd/MM/yyyy"));
                        timesheet.Add(new EmployeeTimeSheet { TimeSheetID = 0, Date = aaaaa, Duration = 0 });
                    }
                }

                List<ComplianceManagement.Business.Data.Customer> custList = new List<ComplianceManagement.Business.Data.Customer>();
                foreach (var customer in clist)
                {
                    custList.Add(new ComplianceManagement.Business.Data.Customer { ID = customer.ID, Name = customer.Name });
                }

                List<ComplianceManagement.Business.Data.Product> productList = new List<ComplianceManagement.Business.Data.Product>();            
                foreach (var product in products)
                {
                    productList.Add(new ComplianceManagement.Business.Data.Product { Id = product.Id, Name = product.Name });
                }

                List<Activity> AList = new List<Activity>();
                foreach (var item in emptimes)
                {
                    var activityLista = activityList.Where(entry => entry.ProductID == item.ProjectID).ToList();
                    foreach (var product in activityLista)
                    {
                        AList.Add(new Activity { ID = product.ID, ActivityName = product.ActivityName });
                    }
                }

                List<ComplianceManagement.Business.Data.CustomerBranch> masterBList = new List<ComplianceManagement.Business.Data.CustomerBranch>();
                foreach (var item in emptimes)
                {
                    var activityLista = branchList.Where(entry => entry.CustomerID == item.CustomerID).ToList();
                    foreach (var product in activityLista)
                    {
                        masterBList.Add(new ComplianceManagement.Business.Data.CustomerBranch { ID = product.ID, Name = product.Name });
                    }
                }
                details.Timesheets1 = timesheet;
                details.customers = custList;
                details.products = productList;
                details.activity = AList;
                details.branchlist = masterBList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return Json(details, JsonRequestBehavior.AllowGet);
        }

        //Delete EmployeeTimeSheet  by Id
        public string DeleteEmployeeTimeSheet(string Id)
        {
            if (!String.IsNullOrEmpty(Id))
            {
                try
                {
                    int tmId = Int32.Parse(Id);
                    var _time = db.EmployeeTimeSheets.Find(tmId);
                    if (_time != null)
                    {
                        using (var dbContextTransaction = db.Database.BeginTransaction())
                        {
                            try
                            {
                                _time.IsDeleted = true;
                                db.SaveChanges();                               
                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                return ex.InnerException.ToString();
                            }
                        }
                        return "Selected EmployeeTimeSheet record deleted sucessfully";
                    }
                    else
                    {
                        return "EmployeeTimeSheet details not found";
                    }
                }
                catch (Exception)
                {
                    return "EmployeeTimeSheet details not found";
                }
            }
            else
            {
                return "Invalid operation";
            }
        }
    }
    }