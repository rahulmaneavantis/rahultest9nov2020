﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class TimeSheetEntry
    {
        public List<EmployeeTimeSheet> timesheets { get; set; }
    }

    public class InvoiceEntry
    {
        public List<Transaction> TransactionID { get; set; }
    }
    public class Transaction
    {
        public string TransactionIDList { get; set; }
    }
}