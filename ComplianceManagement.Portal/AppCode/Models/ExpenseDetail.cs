﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class ExpenseDetail
    {
        public SalesExpensesTransaction Expenses { get; set; }
        public List<SalesExpensesTransactionDetailRahul> ExpensesDetails { get; set; }
        public List<SalesExpensesTransactionDocument> ExpensesDocuments { get; set; }
    }
}