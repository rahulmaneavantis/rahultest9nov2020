﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class RLCS_WriteLog
    {
        public static string WriteLog(List<string> Errors, string ExcelName, out string file)
        {
            try
            {
                //errorLogFilename = "";

                // errorLogFilename = @"\RLCS_ErrorLog_"+ ExcelName + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";               

                string logDirectoryPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RLCS_LogFile"]);

                string dateFolder = Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");

                string directoryPath = logDirectoryPath + "\\" + dateFolder;
                string errorLogFilename =  @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                // errorLogFilename = dateFolder + @"\ErrorLog_" + ExcelName + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                file = dateFolder + errorLogFilename;
               string  filePath = directoryPath + errorLogFilename;

                if (Errors != null && Errors.Count > 0)
                {
                    using (StreamWriter stwriter = new StreamWriter(filePath, true))
                    {
                        stwriter.WriteLine("-------------------Error Log Start-----------as on " + DateTime.Now.ToString("hh:mm tt"));
                        stwriter.WriteLine("Excel Name :" + ExcelName);
                        for (int i = 0; i < Errors.Count; i++)
                        {
                            stwriter.WriteLine(Errors[i].ToString());
                        }
                        Errors = new List<string>();
                        stwriter.WriteLine("-------------------End----------------------------");
                    }
                }
                return file;

            }
            catch (Exception ex)
            {
                file = "";
                return file;
            }
        }

    }
}