﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.ComponentModel.DataAnnotations;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class ComplianceCategoryModels
    {
      public  ComplianceCategoryModels()
        {
            CategoriesList= new List<ComplianceCategoryModels>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public List<ComplianceCategoryModels> CategoriesList { get; set; }
    }

    public class ComplianceActs
    {
      public  ComplianceActs()
        {
            category = new ComplianceCategoryModels();
            Acts = new ActViewModel();
            ComplianceTypes = new ComplianceTypeModel();
            ActMappingModelList = new ActMappingModel();
            SectionMappingModel = new SectionMappingModel();
            ComplianceModel = new ComplianceModel();
        }
        public int FrequencyId { get; set; }
    public List<string> Lst { get; set; }
       public ActViewModel Acts { get; set; }
        public bool Message { get; set; }
        public string draw { get; set; }
         public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public ComplianceCategoryModels category { get; set; }
        public ComplianceTypeModel ComplianceTypes { get; set; }
        public SectionMappingModel SectionMappingModel { get; set; }
        public ActMappingModel ActMappingModelList { get; set; }
        public ComplianceModel ComplianceModel { get; set;}
    }



    public class SectionMappingModel
    {
        public SectionMappingModel()
        {

            SectionList = new List<SectionMappingModel>();
            category = new ComplianceCategoryModels();
           // Acts = new ActViewModel();
            ComplianceTypes = new ComplianceTypeModel();
            ActMappingModelList = new ActMappingModel();
            ComplianceModel = new ComplianceModel();
        }

        [Required(ErrorMessage = "Please select Acts from below table.")]
        public string HiddenAct_ID { get; set; }
        public Nullable<long> AVACOM_ComplianceID { get; set; }
        public string SM_ActID { get; set; }
        [Required(ErrorMessage = "Please select Section from dropdown.")]
        public string SM_SectionID { get; set; }
        public string SM_SectionName { get; set; }
        public string SM_RecordType { get; set; }
        public string SM_RecordName { get; set; }
        public string SM_Frequency { get; set; }
        public string SM_Description { get; set; }
        public string SM_Status { get; set; }
        public string SM_Risk { get; set; }
        public int FrequencyId { get; set; }
        public List<string> Lst { get; set; }
        public bool Message { get; set; }
        public string draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<SectionMappingModel> SectionList { get; set; }
        //public ActViewModel Acts { get; set; }
        public ComplianceCategoryModels category { get; set; }
        public ComplianceTypeModel ComplianceTypes { get; set; }
        public ComplianceModel ComplianceModel { get; set; }
        public ActMappingModel ActMappingModelList { get; set; }
    }
    public class ActViewModel
    {
        public ActView Actview { get; set; }
      public ActViewModel()
        {
            ActList = new List<ActView>();
            Actview = new ActView();
        }
        public List<ActView> ActList { get; set; }
        //public IPagedList<ActView> ActList { get; set; }
    }
    public class ComplianceTypeModel
    {
       public ComplianceType Types { get; set; }
        public ComplianceTypeModel()
        {
            ComplianceTypeList = new List<ComplianceType>();
                    
            Types = new ComplianceType();
        }
        public List<ComplianceType> ComplianceTypeList { get; set; }
    }

    public class ComplianceModel
    {
      public  ComplianceModel()
        {
            ComplianceList = new List<ComplianceView>();
            ComplianceListNew = new List<ComplianceModel>();
        }
        // public Compliance Compliance { get; set; }
        public int ActID { get; set; }
        public string ActName { get; set; }
        public string Description { get; set; }
        public string Sections { get; set; }
        public byte ComplianceType { get; set; }
        public Nullable<bool> UploadDocument { get; set; }
        public string RequiredForms { get; set; }
        public Nullable<byte> Frequency { get; set; }
        public Nullable<byte> RiskType { get; set; }
        public Nullable<int> ComplianceCategoryId { get; set; }
        public Nullable<int> ComplianceTypeId { get; set; }
     
        public string Status { get; set; }
        public string FrquencyName { get; set; }
        public string Risk { get; set; }
        public string UploadName { get; set; }
        public string StatusName { get; set; }
        public List<ComplianceView> ComplianceList { get; set; }
        public List<ComplianceModel> ComplianceListNew { get; set; }
    }
    
    public class ActMappingModel
    { 
       [Required(ErrorMessage = "Please select Acts from below table.")]
        public string HiddenAct_ID { get; set; }
        [Required(ErrorMessage = "Please select Acts from dropdown.")]
        public string AVACOM_ActID { get; set; }
        public string AM_ActID { get; set; }
        public string AM_ActName { get; set; }
        public string AM_ActGroup { get; set; }
        public string AM_StateID { get; set; }
        public string AM_Status { get; set; }
     
        public ActMappingModel()
        {
            RLCS_Act_MappingList = new List<ActMappingModel>();
           
        }
        public List<ActMappingModel> RLCS_Act_MappingList { get; set; }
    }

    
}