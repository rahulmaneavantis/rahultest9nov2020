﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class SalesDetailsEntry
    {
        public int? TransactionID { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public Nullable<int> FinancialYearID { get; set; }
        public bool IsRecoverable { get; set; }
        public int? CustomerID { get; set; }
        public int EmployeeID { get; set; }
        public string Narration { get; set; }
        public Nullable<int> BillStatusTypeID { get; set; }        
        public List<Class_SalesExpensesTransactionDetail> details { get; set; }

        public string[] Names;
    }
}