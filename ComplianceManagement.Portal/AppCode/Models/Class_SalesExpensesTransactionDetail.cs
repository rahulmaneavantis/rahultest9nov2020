﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
  
    public class Class_SalesExpensesTransactionDetail
    {
        public int? TransactionDetailID;
        public int? TransactionID;
        public int SalesExpID;
        public Nullable<int> Amount;
        public string Remark;
        public int? salesTransactionID;
        public int? StatusID;
        public string Status;
        public Nullable<System.DateTime> TranscationDetailDate { get; set; }

        public Nullable<bool> IsProofDoc;
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string[] Names;

        public List<SalesExpensesTransactionDocument> documents { get; set; }

    }

    public class SalesExpensesTransactionDetailRahul
    {
        public int? TransactionID;
        public int? TransactionDetailID;
        public int SalesExpID;
        public Nullable<int> Amount;
        public Nullable<bool> IsProofDoc;
        public Nullable<System.DateTime> TranscationDetailDate { get; set; }
        public string Remark;
        public string Comments;
        public int? salesTransactionID;
        public int? StatusID;
        public string Status;


    }
}