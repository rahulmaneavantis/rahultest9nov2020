﻿
using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class Timesheets
    {
        public List<EmployeeTimeSheet> Timesheets1 { get; set; }
        public List<ComplianceManagement.Business.Data.Customer> customers { get; set; }
        public List<Activity> activity { get; set; }
        public List<ComplianceManagement.Business.Data.CustomerBranch> branchlist { get; set; }
        public List<Business.Data.Product> products
        {
            get; set;
        }
    }
}