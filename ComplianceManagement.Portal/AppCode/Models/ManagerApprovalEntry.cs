﻿using com.VirtuosoITech.ComplianceManagement.Business.DataPract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class ManagerApprovalEntry
    {
        public List<SaleBillApprovalTransactoin> Approvals { get; set; }
    }

}