﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCaseType : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CaseTypeId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CaseTypeId"]);
                        tbl_CaseType Objcases = LitigationCourtAndCaseType.GetLegalCaseTypeDetailByID(ID, CustomerID);
                        tbxCaseType.Text = Objcases.CaseType;
                        ViewState["Mode"] = 1;
                        ViewState["CaseID"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_CaseType objCase = new tbl_CaseType()
                {
                    CaseType = tbxCaseType.Text.Trim(),
                    CustomerID=(int) CustomerID
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCase.ID = Convert.ToInt32(ViewState["CaseID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationCourtAndCaseType.CheckLegalCaseTypeExist(objCase))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Case/Notice type already exists.";
                    }
                    else
                    {
                        objCase.CreatedBy = AuthenticationHelper.UserID;
                        objCase.CreatedOn = DateTime.Now;
                        objCase.IsDeleted = false;
                        LitigationCourtAndCaseType.CreateLegalCaseTypeDetails(objCase);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Case/Notice type Save Successfully.";
                        tbxCaseType.Text = string.Empty;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    objCase.UpdatedBy = AuthenticationHelper.UserID;
                    objCase.UpdatedOn = DateTime.Now;
                    LitigationCourtAndCaseType.UpdateLegalCaseTypeDetails(objCase);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Case/Notice Category Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}