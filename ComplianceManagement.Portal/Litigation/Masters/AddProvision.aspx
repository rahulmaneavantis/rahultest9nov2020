﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProvision.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddProvision" %>
 

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript">
        function CloseMe() {
          
        }
        function RefreshParent() {
            
        
                    window.parent.location.href = window.parent.location.href;
          
        }
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationAddcases" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 120px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Title</label>
                            <asp:TextBox runat="server" ID="tbxtitle" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Provision title can not be empty." ControlToValidate="tbxtitle"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </div>
                          <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 120px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Details</label>
                            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtDetails" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Provision details can not be empty." ControlToValidate="txtDetails"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; text-align: center; margin-top: 10px">
                            <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="PromotorValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />
                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
