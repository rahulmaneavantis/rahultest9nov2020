﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddAct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxAct.Text != "")
                {
                    bool success = true;
                    int newActID = 0;
                    Act_Litigation newAct = new Act_Litigation()
                    {
                        Name = tbxAct.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        ComplianceCategoryId = 12,
                        ComplianceTypeId = 2,
                        IsDeleted = false,
                        CreatedOn = DateTime.Now,
                        UpdatedOn = DateTime.Now,
                        ProductCode = "L",
                    };

                    if (LitigationLaw.Exists(newAct))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Act with same name already exists.";
                        success = false;
                        return;
                    }

                    if (success)
                    {
                        newActID = ActManagement.CreateAct(newAct);

                        if (newActID != 0)
                        {
                            cvDeptPopup.IsValid = false;
                            cvDeptPopup.ErrorMessage = "Act Save Successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}