﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class Authorities : System.Web.UI.Page
    {
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 5;
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                this.MasterPageFile = "~/LitigationMaster.Master";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>)HttpContext.Current.Cache[key];
                //var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                BindData();
                flag = false;
                bindPageNumber();
                hideAddControls();
            }

        }

        public void hideAddControls()
        {

            if (authpage != null)
            {
                if (authpage.Addval == false)
                {
                    btnAddPromotor.Visible = false;
                }
                else
                {
                    btnAddPromotor.Visible = true;
                }
            }

        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void BindData()
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objProv = (from row in entities.Authorityofthestates
                                   where row.CustomerId==CustomerID
                                    select row).OrderBy(entry => entry.Name).ToList();

                    flag = true;

                    Session["TotalRows"] = null;
                    if (objProv.Count > 0)
                    {
                        grdCourtMaster.DataSource = objProv;
                        Session["TotalRows"] = objProv.Count;
                        grdCourtMaster.DataBind();
                    }
                    else
                    {
                        grdCourtMaster.DataSource = objProv;
                        grdCourtMaster.DataBind();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCourtMaster.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCourtMaster.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAddCourtPopUp('" + ID + "');", true);
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCourtMaster.PageIndex = chkSelectedPage - 1;
            grdCourtMaster.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindData();
            ShowGridDetail();
        }

        protected void grdCourtMaster_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int courtID = 0;

                if (e.CommandName.Equals("EDIT_Court"))
                {
                    courtID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CourtID"] = courtID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAddCourtPopUp('" + courtID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_Court"))
                {
                       courtID = Convert.ToInt32(e.CommandArgument);

                     
                        Authority.DeleteAuthority(courtID, CustomerID);
                        BindData();
                        bindPageNumber();
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindData(); bindPageNumber();
        }

        protected void grdCourtMaster_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var courtList = Authority.GetAllAuthority((int)CustomerID);

                if (courtList.Count > 0)
                    courtList = courtList.OrderBy(entry => entry.Name).ToList();
 

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    courtList = courtList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    courtList = courtList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdCourtMaster.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCourtMaster.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;

                if (courtList.Count > 0)
                {
                    grdCourtMaster.DataSource = courtList;
                    Session["TotalRows"] = courtList.Count;
                    grdCourtMaster.DataBind();
                }
                else
                {
                    grdCourtMaster.DataSource = courtList;
                    grdCourtMaster.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCourtMaster_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCourtMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (authpage != null)
                {
                    if (authpage.Deleteval == false)
                    {
                        LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lbtcourMdelete");
                        LinkButton2.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton2 = (LinkButton)e.Row.FindControl("lbtcourMdelete");
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Modify == false)
                    {
                        LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lbtcourMedit");
                        LinkButton1.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("lbtcourMedit");
                        LinkButton1.Visible = true;
                    }
                    if (authpage.Deleteval == false && authpage.Modify == false)
                    {
                        grdCourtMaster.Columns[7].Visible = false;
                    }
                }
            }
        }
    }
}