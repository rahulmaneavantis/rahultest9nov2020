﻿<%@ Page Title="Department Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="DepartmentMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.DepartmentMaster" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        function openPopup() {
            $('#divDepartmentDialog').modal('show');
        }
       
        var removeMyIFrame = function () {
            $('#MyiFrame').remove();
        }

        function OpenDepartmentPopup(DepartmentID) {
            
            $('#AddDepartmentPopUp').modal('show');
            $('#ContentPlaceHolder1_IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + DepartmentID);
        }

        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            location.reload();
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('Department Master');
            fhead('Department Master');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  

    <asp:UpdatePanel ID="upDepartmentMaster" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12">
                        <section class="panel">    
                            
                         <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPageValidationGroup" />
                            <asp:CustomValidator ID="cvPage" runat="server" EnableClientScript="False"
                                ValidationGroup="DepartmentPageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                         </div>   
                                         
                         <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" style="width:23%">
                                    <p style="color: #999; margin-top: 5px;">Show</p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                             
                            <div class="col-md-10 colpadding0" style="margin-top: 5px; text-align:right">
                                <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server"
                                    ID="btnAddDepartment" OnClick="btnAddDepartment_Click" />
                            </div>
                        </div>

                         <div style="margin-bottom: 4px">           
                            <asp:GridView runat="server" ID="grdDepartment" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdDepartment_RowDataBound"
                               PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" ShowHeaderWhenEmpty="true"
                                 DataKeyNames="ID" OnRowCommand="grdDepartment_RowCommand">
                                <Columns>
                                     <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                     <ItemTemplate>
                                                      <%#Container.DataItemIndex+1 %>
                                     </ItemTemplate>
                                     </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Department" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90%" />                        
                                    <asp:TemplateField  HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Department"
                                                CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Observation" title="Edit Department Details" /></asp:LinkButton>
                                            <asp:LinkButton Visible="false" ID="LinkButton2" runat="server" CommandName="DELETE_Department"
                                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Department Details?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Department Details" title="Delete Department Details" /></asp:LinkButton>
                                        </ItemTemplate>                            
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />  
                                 <PagerSettings Visible="false" />             
                               <EmptyDataTemplate>
                                   No Record Found
                               </EmptyDataTemplate>
                            </asp:GridView>                     
                        </div>

                         <div class="col-md-12 colpadding0">
                            <div class="col-md-10 colpadding0"></div>
                            <div class="col-md-2 colpadding0" style="float:right;">
                                <div style="float:left;width:50%">
                                    <p class="clsPageNo">Page</p>
                                </div>
                                <div style="float:left;width:50%">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px" >                                   
                                    </asp:DropDownListChosen>
                                </div>
                            </div>   
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />                 
                        </div>                                
                       </section>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divDepartmentDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="DepartmentPopup">
                        <asp:UpdatePanel ID="upDepartment" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPopupValidationGroup" />
                                        <asp:CustomValidator ID="cvDeptPopup" runat="server" EnableClientScript="False"
                                            ValidationGroup="DepartmentPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            Department</label>
                                        <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Department Name can not be empty." ControlToValidate="txtFName"
                                            runat="server" ValidationGroup="DepartmentPopupValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="DepartmentPopupValidationGroup"
                                            ErrorMessage="Please enter a valid location" ControlToValidate="txtFName"
                                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px; text-align: center">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="DepartmentPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px"></div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 40%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="250px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    
</asp:Content>
