﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCourt.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddCourt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
             
    <!--external css-->
   <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

  
</head>
<body style="background-color:white">
    <form id="form1" runat="server">
        <div>
            <div id="AddCourtType">
                <asp:ScriptManager ID="LitigationAddCourt" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                      <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CourtPopupValidationGroup" />
                            <asp:CustomValidator ID="cvAddCourtPopup1" runat="server" EnableClientScript="False"
                                ValidationGroup="CourtPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Court Type</label>
                            <asp:TextBox runat="server" ID="tbxCourtType" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Court Type Name can not be empty." ControlToValidate="tbxCourtType"
                                runat="server" ValidationGroup="CourtPopupValidationGroup" Display="None" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CourtPopupValidationGroup"
                                ErrorMessage="Please enter a valid location" ControlToValidate="tbxCourtType"
                                ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                        </div>
                        <div style="margin-bottom: 7px; text-align: center">
                            <asp:Button Text="Save" runat="server" ID="Button1" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                ValidationGroup="CourtPopupValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" OnClick="btnCancel_Click" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />


                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
      <script type="text/javascript">
        

        function CloseMe() {
            
            window.parent.CloseMeCourtType();
        }
        function RefreshParent() {
            var checkPath = window.parent.location.href.toLowerCase();
            if (checkPath.toLowerCase().indexOf("casedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('casedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
            if (checkPath.toLowerCase().indexOf("noticedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('noticedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
        }
    </script>
</body>
</html>
