﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class DepartmentMaster : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";

                BindDepartmentList();
                bindPageNumber();
                //GetPageDisplaySummary();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdDepartment.PageIndex = chkSelectedPage - 1;

                //SelectedPageNo.Text = (chkSelectedPage).ToString();
                grdDepartment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindDepartmentList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdDepartment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        private void BindDepartmentList()
        {
            try
            {
                var DepartmentMasterList = CompDeptManagement.GetAllDepartmentMasterList(CustomerID);

                if (DepartmentMasterList.Count > 0)
                    DepartmentMasterList = DepartmentMasterList.OrderBy(entry => entry.Name).ToList();

                grdDepartment.DataSource = DepartmentMasterList;
                Session["TotalRows"] = DepartmentMasterList.Count;
                grdDepartment.DataBind();

                upDepartmentMaster.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Department objDeptcompliance = new Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID=(int) CustomerID
                };
                mst_Department objDeptaudit = new mst_Department()
                {
                    Name = txtFName.Text.Trim(),
                    IsDeleted = false,
                    CustomerID = (int) CustomerID
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    objDeptcompliance.ID = Convert.ToInt32(ViewState["DeptID"]);
                    objDeptaudit.ID = Convert.ToInt32(ViewState["DeptID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMaster(objDeptcompliance);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Saved Successfully";
                        txtFName.Text = string.Empty;
                    }

                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists";
                    }
                    else
                    {
                        CompDeptManagement.CreateDepartmentMasterAudit(objDeptaudit);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Saved Successfully";
                        txtFName.Text = string.Empty;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (CompDeptManagement.DepartmentExists(objDeptcompliance))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMaster(objDeptcompliance);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Updated Successfully";                        
                    }
                    if (CompDeptManagement.DepartmentExistsAudit(objDeptaudit))
                    {
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Already Exists";
                    }
                    else
                    {
                        CompDeptManagement.UpdateDepartmentMasterAudit(objDeptaudit);
                        cvDeptPopup.IsValid = false;
                        cvDeptPopup.ErrorMessage = "Department Updated Successfully";
                    }
                }
                BindDepartmentList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdDepartment.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDeptPopup.IsValid = false;
                cvDeptPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void btnAddDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                txtFName.Text = string.Empty;
                upDepartment.Update();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDepartmentPopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdDepartment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int DeptID = 0;

                if (e.CommandName.Equals("EDIT_Department"))
                {
                    DeptID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = DeptID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDepartmentPopup('" + DeptID + "');", true);
                    Department RPD = CompDeptManagement.DepartmentMasterGetByID(DeptID, CustomerID);//,CustomerID
                    txtFName.Text = RPD.Name;
                    upDepartment.Update();
                }
                else if (e.CommandName.Equals("DELETE_Department"))
                {
                    DeptID = Convert.ToInt32(e.CommandArgument);
                    CompDeptManagement.DeleteDepartmentMaster(DeptID, CustomerID);
                    CompDeptManagement.DeleteDepartmentMasterAudit(DeptID, CustomerID);
                    BindDepartmentList();
                    bindPageNumber();                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdDepartment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
               
                BindDepartmentList();
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdDepartment.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }
        
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }      

    }
}