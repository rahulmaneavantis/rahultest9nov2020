﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class EmailListLitigation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEmails();
            }
        }

        private void BindEmails()
        {
            try
            {
                grdUserReminder.DataSource = Business.ComplianceManagement.GetEmails(AuthenticationHelper.UserID).ToList();
                grdUserReminder.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}