﻿<%@ Page Title="My Reports" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="MyReportLitigation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports.MyReportLitigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Reports');
        });

        function fopenpopup() {
            $('#divResponsePopUp').modal('show');
        }

        function ShowDialog(noticeCaseInstanceID, type, HistoryFlag) {            
            if (type != '') {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '550px');
                $('.modal-dialog').css('width', '90%');

                if (type == 'C') {
                    $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + noticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
                }
                else if (type == 'N') {
                    $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + noticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
                }
            }
        };

        function OpenMoreReports() {
            window.location.href = '../Reports/litigationreports.aspx';
        };

        $(document).on("click", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="MyReportValidationGroup" />
                        <asp:CustomValidator ID="cvMyReport" runat="server" EnableClientScript="False"
                            ValidationGroup="MyReportValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%;">
                            <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select Type" class="form-control" Width="90%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                                <asp:ListItem Text="Notice" Value="N" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Case" Value="C"></asp:ListItem>
                                <asp:ListItem Text="Task" Value="T"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>

                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%;">
                            <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select Status" class="form-control" Width="90%">
                                  <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Open" Selected="True" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                <%--<asp:ListItem Text="Settled" Value="4"></asp:ListItem>--%>
                            </asp:DropDownListChosen>
                        </div>

                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%;">
                            <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select Status" class="form-control" Width="90%">
                                <asp:ListItem Text="All" Value="B"></asp:ListItem>
                                <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                                <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 14%;">
                            <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                DataPlaceHolder="Select Department" class="form-control" Width="90%" />
                        </div>

                        <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                            <ContentTemplate>
                                <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                                    <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/Branch/Location" autocomplete="off" CssClass="clsDropDownTextBox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 12%;">
                             <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select Financial Year" class="form-control" Width="90%">                         
                            </asp:DropDownListChosen>
                            </div>
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%; display:none">
                            <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                DataPlaceHolder="Select Opponent" class="form-control" Width="90%" />
                        </div>

                        <div class="col-md-3 colpadding0 entrycount" style="text-align: left; width: 10%;">
                            <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="LinkButton1" 
                                OnClick="lnkBtnApplyFilter_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Apply"/>
                        </div>

                        <div class="col-md-3 colpadding0 entrycount" style="text-align: right; width: 7%; margin-left: -4%;">
                            <%--<asp:Button Text="Export to Excel" runat="server" ID="btnExport" CssClass="btn btn-primary" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" />--%>
                            <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                            </asp:LinkButton>
                        </div>

                        <div class="col-md-3 colpadding0 entrycount" style="float: right; margin-right: -2%; width: 10%;">
                            <asp:Button Text="More Reports" runat="server" ID="btnMoreReport" CssClass="btn btn-primary" Width="100%" Style="margin-top: 5px; margin-left: -20%;" OnClientClick="OpenMoreReports()" data-toggle="tooltip" ToolTip="More Reports"/>
                            <%--<i class='fa fa-file-excel-o' aria-hidden='true'></i>--%>
                        </div>
                    </div>

                    <div class="clearfix"></div>


                    <div style="margin-bottom: 4px">
                        <asp:GridView runat="server" ID="grdMyReport" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                            PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="NoticeCaseInstanceID"
                            OnRowCommand="grdMyReport_RowCommand" OnSorting="grdMyReport_Sorting" OnRowCreated="grdMyReport_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderText="Sr" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <%--  <asp:TemplateField HeaderText="FY" ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="center" SortExpression="FYName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("FYName") %>' ToolTip='<%# Eval("FYName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="center" SortExpression="TypeName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeName") %>' ToolTip='<%# Eval("TypeName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Title" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="Title">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="center" SortExpression="NoticeCaseDesc">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeCaseDesc") %>' ToolTip='<%# Eval("NoticeCaseDesc") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="PartyName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                            <asp:Label runat="server" data-toggle="tooltip" Width="100%" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="center" SortExpression="BranchName">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 170px">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="Department">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Department") %>' ToolTip='<%# Eval("Department") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Jurisdiction" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center" SortExpression="Jurisdiction" Visible="false">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Jurisdiction") %>' ToolTip='<%# Eval("Jurisdiction") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="lblViewResponce" runat="server" ImageUrl="~/Images/View-icon-new.png" CommandName="Responce" data-toggle="tooltip" data-placement="bottom"
                                                    CommandArgument='<%# Eval("NoticeCaseInstanceID")+ ","+ Eval("TypeCaseNotice") %>' ToolTip="Show Details"></asp:ImageButton>                                                
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerSettings Visible="false" />
                            <PagerTemplate>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-8 colpadding0">
                            <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2 colpadding0">
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="5" />
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 colpadding0" style="float: right;">
                            <div style="float: left; width: 60%">
                                <p class="clsPageNo">Page</p>
                            </div>
                            <div style="float: left; width: 40%">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="100%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>
                        </div>
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeCaseModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%--Response PopUp--%>
    <div class="modal fade" id="divResponsePopUp" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel1_Load">
                        <ContentTemplate>
                            <div>
                                <asp:GridView runat="server" ID="GirdResponsePopUp" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    PageSize="15" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="NoticeCaseInstanceID">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderText="Sr" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField HeaderText="Response/Hearing Date" DataField="ResponseDate" ItemStyle-Width="10%" DataFormatString="{0:dd/MM/yyyy}" />

                                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                    <asp:Label runat="server" data-toggle="tooltip" Width="180px" data-placement="bottom" Text='<%# Eval("Description") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Created By" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                    <asp:Label runat="server" data-toggle="tooltip" Width="100px" data-placement="bottom" Text='<%# Eval("CreatedBy") %>' ToolTip='<%# Eval("CreatedBy") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                    <asp:Label runat="server" data-toggle="tooltip" Width="150px" data-placement="bottom" Text='<%# Eval("Remark") %>' ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
