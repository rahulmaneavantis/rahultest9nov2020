﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports
{
    public partial class AddMisreport : System.Web.UI.Page
    {
        public string fromeDate;
        public string Todate;
        public string Location;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fromedate"]) || !string.IsNullOrEmpty(Request.QueryString["TODate"]) || !string.IsNullOrEmpty(Request.QueryString["Location"]))
            {
                fromeDate = Request.QueryString["fromedate"].ToString();
                Todate = Request.QueryString["TODate"].ToString();
                Location = Request.QueryString["Location"];

            }
            if (!IsPostBack)
            {
                BindGridForMisReportFiels();

            }
        }

        private void BindGridForMisReportFiels()
        {
            try
            {
                List<Temp_MisReort> lstMISReport = new List<Temp_MisReort>();
                lstMISReport = LitigationManagement.GetMISFielsDetails();

                if (lstMISReport != null && lstMISReport.Count > 0)
                {
                    GrdMisrepportField.DataSource = lstMISReport;
                    GrdMisrepportField.DataBind();
                    Session["TotalRows"] = lstMISReport.Count;
                }
                else
                {
                    GrdMisrepportField.DataSource = new string[] { };
                    GrdMisrepportField.DataBind(); //Bind that empty source      
                    Session["TotalRows"] = lstMISReport.Count;

                }
            }
            catch (Exception ex)
            {

            }
        }

        private bool ReportconfigExist(long customerID, string ReportName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CheckVal = (from row in entities.tbl_Litigation_ReportConfig
                                where row.CustomerID == customerID && row.IsActive == true
                                && row.ReportName == ReportName
                                select row).FirstOrDefault();

                if (CheckVal != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            List<string> checkedlist = new List<string>();
            checkedlist.Clear();
            for (int i = 0; i < GrdMisrepportField.Rows.Count; i++)
            {
                CheckBox CbMISIsfield = (CheckBox)GrdMisrepportField.Rows[i].Cells[0].FindControl("CbMISIsfield");
                if (CbMISIsfield.Checked == false)
                {
                    Label lblMISFieldName = (Label)GrdMisrepportField.Rows[i].Cells[1].FindControl("lblMISFieldName");
                    string MisfieldVal = lblMISFieldName.Text.Trim();
                    checkedlist.Add(MisfieldVal);
                }
            }


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    string LatestHearingOnly = "MisReportWithLatestHearing";
                    if (ReportconfigExist(AuthenticationHelper.CustomerID, LatestHearingOnly))
                    {
                        #region Emami only

                        entities.Database.CommandTimeout = 180;
                        var MisReport = (entities.sp_GetMisReports_LatestHearing(Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                        var MisReport1 = (entities.SP_CaseSummary(Convert.ToInt32(AuthenticationHelper.CustomerID)).OrderByDescending(x => x.Perticulars)).ToList();

                        var provision = (from row in entities.Provisions select row).ToList();

                        var authorities = (from row in entities.Authorityofthestates select row).ToList();

                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                        {
                            var listcaseassigned = (from row in entities.tbl_LegalCaseAssignment
                                                    where row.RoleID == 3 && row.UserID == AuthenticationHelper.UserID
                                                    select row.CaseInstanceID).ToList();

                            MisReport = MisReport.Where(entry => (listcaseassigned.Contains(entry.CaseInstanceID)) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.CaseCreatedBy == AuthenticationHelper.UserID)).ToList();
                        }
                        else // In case of MGMT or CADMN 
                        {
                            MisReport = MisReport.Where(entry => entry.RoleID == 3).ToList();
                        }

                        if (MisReport.Count > 0)
                        {
                            MisReport = (from g in MisReport
                                         group g by new
                                         {
                                             g.CaseType,
                                             g.Case_Open_Date,
                                             g.StateName,
                                             g.Court_Case_No,
                                             g.Case_Title,
                                             g.Type_of_Case,
                                             g.Opponent,
                                             g.Court,
                                             g.Judge,
                                             g.Case_Detail_Description,
                                             g.Entity_Location_Branch,
                                             g.Claim_Amt,
                                             g.Expense_Till_Date,
                                             g.Owner,
                                             g.Law_Firm,
                                             g.Status,
                                             g.Status_Updated_On,
                                             g.CaseInstanceID,
                                             g.Status_Remark,
                                             g.ProvisioninBook,
                                             g.Taxdemand,
                                             g.interest,
                                             g.FY,
                                             g.Penalty,
                                             g.HearingDate,
                                             g.HearingDescription,
                                             g.NextHearingDate,
                                             g.internalcaseno,
                                             g.amountpaid,
                                             g.SLNo,
                                             g.Total,

                                         } into GCS
                                         select new sp_GetMisReports_LatestHearing_Result()
                                         {
                                             CaseType = GCS.Key.CaseType,
                                             Case_Open_Date = GCS.Key.Case_Open_Date,
                                             StateName = GCS.Key.StateName,
                                             Court_Case_No = GCS.Key.Court_Case_No,
                                             Case_Title = GCS.Key.Case_Title,
                                             Type_of_Case = GCS.Key.Type_of_Case,
                                             Opponent = GCS.Key.Opponent,
                                             Court = GCS.Key.Court,
                                             Judge = GCS.Key.Judge,
                                             Case_Detail_Description = GCS.Key.Case_Detail_Description,
                                             Entity_Location_Branch = GCS.Key.Entity_Location_Branch,
                                             Claim_Amt = GCS.Key.Claim_Amt,
                                             Expense_Till_Date = GCS.Key.Expense_Till_Date,
                                             Owner = GCS.Key.Owner,
                                             Law_Firm = GCS.Key.Law_Firm,
                                             Status = GCS.Key.Status,
                                             FY = GCS.Key.FY,
                                             Status_Remark = GCS.Key.Status_Remark,
                                             Penalty = GCS.Key.Penalty,
                                             Taxdemand = GCS.Key.Taxdemand,
                                             interest = GCS.Key.interest,
                                             ProvisioninBook = GCS.Key.ProvisioninBook,
                                             Status_Updated_On = GCS.Key.Status_Updated_On,
                                             CaseInstanceID = GCS.Key.CaseInstanceID,
                                             HearingDate = GCS.Key.HearingDate,
                                             HearingDescription=GCS.Key.HearingDescription,
                                             NextHearingDate = GCS.Key.NextHearingDate,
                                             internalcaseno = GCS.Key.internalcaseno,
                                             amountpaid = GCS.Key.amountpaid,
                                             Total = GCS.Key.Total,
                                             SLNo = GCS.Key.SLNo
                                         }).ToList();

                            if (!string.IsNullOrEmpty(Location) && Location != "-1" && Location != "null")
                            {
                                MisReport = MisReport.Where(entry => entry.Entity_Location_Branch == Location).ToList();
                            }
                            //HearingDate
                            if (!string.IsNullOrEmpty(fromeDate) && fromeDate != "null" && fromeDate != "undefined")
                            {
                                MisReport = MisReport.Where(entry => entry.Case_Open_Date >= DateTimeExtensions.GetDate(fromeDate)).ToList();
                            }
                            if (!string.IsNullOrEmpty(Todate) && Todate != "null" && fromeDate != "undefined")
                            {
                                MisReport = MisReport.Where(entry => entry.Case_Open_Date <= DateTimeExtensions.GetDate(Todate)).ToList();
                            }
                        }

                        DataTable table = new DataTable();
                        table.Columns.Add("SLNO");
                        table.Columns.Add("Court_Case_No");
                        table.Columns.Add("internalcaseno");
                        table.Columns.Add("StateName");
                        table.Columns.Add("FY");
                        table.Columns.Add("Case_Detail_Description");
                        table.Columns.Add("Taxdemand");
                        table.Columns.Add("interest");
                        table.Columns.Add("Penalty");
                        table.Columns.Add("Total");
                        table.Columns.Add("amountpaid");
                        table.Columns.Add("Court");
                        table.Columns.Add("Law_Firm");
                        table.Columns.Add("HearingDate");
                        table.Columns.Add("HearingDescription");
                        table.Columns.Add("NextHearingDate");
                        table.Columns.Add("Expense_Till_Date");
                        table.Columns.Add("ProvisioninBook");
                        table.Columns.Add("Status_Remark");
                        table.Columns.Add("CaseType");
                        table.Columns.Add("Case_Open_Date");
                        table.Columns.Add("Status");
                        table.Columns.Add("Case_Title");
                        table.Columns.Add("Type_of_Case");
                        table.Columns.Add("Opponent");
                        table.Columns.Add("Judge");
                        table.Columns.Add("Entity_Location_Branch");
                        table.Columns.Add("Owner");
                        table.Columns.Add("Status_Updated_On");



                        DataTable table1 = new DataTable();
                        table1.Columns.Add("SLNO", typeof(string));
                        table1.Columns.Add("Perticulars", typeof(string));
                        table1.Columns.Add("DisputedAmt", typeof(decimal));
                        table1.Columns.Add("Interest", typeof(decimal));
                        table1.Columns.Add("Penalty", typeof(decimal));
                        table1.Columns.Add("Total", typeof(decimal));
                        table1.Columns.Add("AmountPaid", typeof(decimal));
                        table1.Columns.Add("Favourable", typeof(string));
                        table1.Columns.Add("Unfavourable", typeof(string));
                        table1.Columns.Add("Expense", typeof(string));

                        DataTable tblprovision = new DataTable();

                        tblprovision.Columns.Add("title", typeof(string));
                        tblprovision.Columns.Add("details", typeof(string));

                        DataTable tblAuthority = new DataTable();
                        tblAuthority.Columns.Add("designation", typeof(string));
                        tblAuthority.Columns.Add("Name", typeof(string));


                        int Count = 0;

                        foreach (var item in MisReport)
                        {
                            Count++;

                            table.Rows.Add(item.SLNo, item.Court_Case_No, item.internalcaseno, item.StateName, item.FY, item.Case_Detail_Description, item.Taxdemand, item.interest, item.Penalty, item.Total, item.amountpaid, item.Court, item.Law_Firm, item.HearingDate, item.HearingDescription, item.NextHearingDate, item.Expense_Till_Date, item.ProvisioninBook, item.Status_Remark, item.CaseType, item.Case_Open_Date, item.Status, item.Case_Title, item.Type_of_Case, item.Opponent, item.Judge, item.Entity_Location_Branch, item.Owner, item.Status_Updated_On);
                        }


                        foreach (var item in MisReport1)
                        {
                            Count++;
                            table1.Rows.Add(Count, item.Perticulars, item.DisputedAmt, item.Interest, item.Penalty, item.Total, item.AmountPaid, item.Favourable, item.Unfavourable, item.Expense);
                        }

                        Count = 0;
                        foreach (var item in provision)
                        {
                            Count++;
                            tblprovision.Rows.Add(item.Title, item.Details);
                        }

                        Count = 0;
                        foreach (var item in authorities)
                        {
                            Count++;
                            tblAuthority.Rows.Add(item.Designation, item.Name);
                        }

                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("MIS");
                        ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Summary");

                        ExcelWorksheet exWorkSheetprovision = exportPackge.Workbook.Worksheets.Add("Provision");
                        ExcelWorksheet exWorkSheetAuthority = exportPackge.Workbook.Worksheets.Add("Authority");

                        DataView view = new System.Data.DataView(table);
                        DataView view1 = new System.Data.DataView(table1);
                        DataView viewprovision1 = new System.Data.DataView(tblprovision);
                        DataView viewauthority1 = new System.Data.DataView(tblAuthority);
                        DataTable ExcelData = null;
                        DataTable ExcelData1 = null;
                        DataTable ExcelDataprovision = null;
                        DataTable ExcelDataAuthority = null;
                        //for mis main report
                        ExcelData = view.ToTable("Selected", false, "SLNO", "Court_Case_No", "internalcaseno", "StateName", "FY", "Case_Detail_Description", "Taxdemand", "interest", "Penalty", "Total", "amountpaid", "Court", "Law_Firm", "HearingDate", "HearingDescription", "NextHearingDate", "Expense_Till_Date", "ProvisioninBook", "Status_Remark", "CaseType", "Case_Open_Date", "Status", "Case_Title", "Type_of_Case", "Opponent", "Judge", "Entity_Location_Branch", "Owner", "Status_Updated_On");
                        //for case Summary
                        ExcelData1 = view1.ToTable("Selected", false, "SLNO", "Perticulars", "DisputedAmt", "Interest", "Penalty", "Total", "AmountPaid", "Favourable", "Unfavourable", "Expense");

                        ExcelDataprovision = viewprovision1.ToTable("Selected", false, "Title", "Details");
                        ExcelDataAuthority = viewauthority1.ToTable("Selected", false, "Name","Designation");


                        for (int i = 0; i < checkedlist.Count; i++)
                        {
                            ExcelData.Columns.Remove(checkedlist[i]);
                        }

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item["HearingDate"])))
                            {
                                item["HearingDate"] = Convert.ToDateTime(item["HearingDate"]).ToString("dd-MMM-yyyy");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(item["NextHearingDate"])))
                            {
                                item["NextHearingDate"] = Convert.ToDateTime(item["NextHearingDate"]).ToString("dd-MMM-yyyy");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(item["Case_Open_Date"])))
                            {
                                item["Case_Open_Date"] = Convert.ToDateTime(item["Case_Open_Date"]).ToString("dd-MMM-yyyy");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(item["Status_Updated_On"])))
                            {
                                item["Status_Updated_On"] = Convert.ToDateTime(item["Status_Updated_On"]).ToString("dd-MMM-yyyy");
                            }
                        }
                        //  ExcelData.Columns.Remove("CaseInstanceID");
                        //exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        //exWorkSheet1.Row(1).Style.Font.Bold = true;
                        //exWorkSheet1.Row(1).Style.Font.Size = 12;
                        //exWorkSheet1.Row(1).Height = 15;

                        //exWorkSheet1.Row(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Row(1).Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        //exWorkSheet1.Cells.AutoFitColumns(0.00, 50.00);

                        exWorkSheet1.Cells["A1"].Value = "SL NO";
                        exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["A1"].Style.WrapText = true;
                        exWorkSheet1.Cells["A1"].AutoFitColumns(10);
                        exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["B1"].Value = "Court Case No.";
                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["B1"].Style.WrapText = true;
                        exWorkSheet1.Cells["B1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheet1.Cells["C1"].Value = "Internal case file no.";
                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["C1"].Style.WrapText = true;
                        exWorkSheet1.Cells["C1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["D1"].Value = "State Name";
                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["D1"].Style.WrapText = true;
                        exWorkSheet1.Cells["D1"].AutoFitColumns(25);
                        exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["E1"].Value = "F.Y.";
                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["E1"].Style.WrapText = true;
                        exWorkSheet1.Cells["E1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["F1"].Value = "Case details description";
                        exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["F1"].Style.WrapText = true;
                        exWorkSheet1.Cells["F1"].AutoFitColumns(50);
                        exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["G1"].Value = "Tax Demand";
                        exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["G1"].Style.WrapText = true;
                        exWorkSheet1.Cells["G1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["H1"].Value = "Interest";
                        exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["H1"].Style.WrapText = true;
                        exWorkSheet1.Cells["H1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["I1"].Value = "Penalty";
                        exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["I1"].Style.WrapText = true;
                        exWorkSheet1.Cells["I1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["J1"].Value = "Total(Claim Amt.)";
                        exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["J1"].Style.WrapText = true;
                        exWorkSheet1.Cells["J1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["K1"].Value = "Amount Paid";
                        exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["K1"].Style.WrapText = true;
                        exWorkSheet1.Cells["K1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["L1"].Value = "Court";
                        exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["L1"].Style.WrapText = true;
                        exWorkSheet1.Cells["L1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["M1"].Value = "Law Firm";
                        exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["M1"].Style.WrapText = true;
                        exWorkSheet1.Cells["M1"].AutoFitColumns(25);
                        exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["N1"].Value = "Date of hearing";
                        exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["N1"].Style.WrapText = true;
                        exWorkSheet1.Cells["N1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["O1"].Value = "Hearing description";
                        exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["O1"].Style.WrapText = true;
                        exWorkSheet1.Cells["O1"].AutoFitColumns(30);
                        exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["P1"].Value = "Next hearing date";
                        exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["P1"].Style.WrapText = true;
                        exWorkSheet1.Cells["P1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["Q1"].Value = "Exp till date";
                        exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["Q1"].Style.WrapText = true;
                        exWorkSheet1.Cells["Q1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheet1.Cells["R1"].Value = "Provision in Book";
                        exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["R1"].Style.WrapText = true;
                        exWorkSheet1.Cells["R1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["S1"].Value = "Status Remark";
                        exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["S1"].Style.WrapText = true;
                        exWorkSheet1.Cells["S1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["T1"].Value = "Case Type";
                        exWorkSheet1.Cells["T1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["T1"].Style.WrapText = true;
                        exWorkSheet1.Cells["T1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["U1"].Value = "Case Open date";
                        exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["U1"].Style.WrapText = true;
                        exWorkSheet1.Cells["U1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheet1.Cells["V1"].Value = "Status";
                        exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["V1"].Style.WrapText = true;
                        exWorkSheet1.Cells["V1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["W1"].Value = "Case Title";
                        exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["W1"].Style.WrapText = true;
                        exWorkSheet1.Cells["W1"].AutoFitColumns(30);
                        exWorkSheet1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["X1"].Value = "Type of Case";
                        exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["X1"].Style.WrapText = true;
                        exWorkSheet1.Cells["X1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["Y1"].Value = "Opponent";
                        exWorkSheet1.Cells["Y1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["Y1"].Style.WrapText = true;
                        exWorkSheet1.Cells["Y1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["Z1"].Value = "Judge";
                        exWorkSheet1.Cells["Z1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["Z1"].Style.WrapText = true;
                        exWorkSheet1.Cells["Z1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["AA1"].Value = "Entity_Location_Branch";
                        exWorkSheet1.Cells["AA1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["AA1"].Style.WrapText = true;
                        exWorkSheet1.Cells["AA1"].AutoFitColumns(25);
                        exWorkSheet1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["AB1"].Value = "Owner";
                        exWorkSheet1.Cells["AB1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["AB1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["AB1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["AB1"].Style.WrapText = true;
                        exWorkSheet1.Cells["AB1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["AB1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["AB1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet1.Cells["AC1"].Value = "Status_Updated_On";
                        exWorkSheet1.Cells["AC1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["AC1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["AC1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheet1.Cells["AC1"].Style.WrapText = true;
                        exWorkSheet1.Cells["AC1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["AC1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["AC1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        
                        #region MISCASE
                        //exWorkSheet1.Cells["A1"].Value = "Sr NO";
                        //exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["A1"].AutoFitColumns(10);
                        //exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["B1"].Value = "Case Type";
                        //exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["B1"].AutoFitColumns(10);
                        //exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["C1"].Value = "Case Open Date";
                        //exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["C1"].AutoFitColumns(10);
                        ////exWorkSheet1.Cells[1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                        //exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["D1"].Value = "Case Status";
                        //exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["D1"].AutoFitColumns(10);
                        ////exWorkSheet1.CellsD1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                        //exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["E1"].Value = "Status Remark";
                        //exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["E1"].AutoFitColumns(10);
                        ////exWorkSheet1.CellsE1A3"].Style.Numberformat.Format = "dd-MMM-yyyy";
                        //exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["F1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["F1"].Value = "Court Case No";
                        //exWorkSheet1.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["G1"].Value = "Case Title";
                        //exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["G1"].AutoFitColumns(30);
                        //exWorkSheet1.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["H1"].Value = "Type of Case";
                        //exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["H1"].AutoFitColumns(20);
                        //exWorkSheet1.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["I1"].Value = "Opponent";
                        //exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["I1"].AutoFitColumns(35);
                        ////exWorkSheet1.CellsI1C5"].Style.Numberformat.Format = "dd-MM-yyyy";
                        //exWorkSheet1.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["J1"].Value = "Court";
                        //exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["J1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["K1"].Value = "Judge";
                        //exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["K1"].AutoFitColumns(20);
                        //exWorkSheet1.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["L1"].Value = "Case Detail Description";
                        //exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["L1"].AutoFitColumns(40);
                        //exWorkSheet1.Cells["L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["L1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["L1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["L1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["M1"].Value = "State Name";
                        //exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["M1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["M1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["M1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["M1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["N1"].Value = "Entity/Location/Branch";
                        //exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["N1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["N1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["N1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["N1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["N1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["O1"].Value = "Claim Amt";
                        //exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["O1"].AutoFitColumns(20);
                        //exWorkSheet1.Cells["O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["O1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["P1"].Value = "Hearing Date";
                        //exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["P1"].AutoFitColumns(10);
                        //exWorkSheet1.Cells["P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["P1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["P1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["P1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["Q1"].Value = "Hearing Description";
                        //exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["Q1"].AutoFitColumns(35);
                        //exWorkSheet1.Cells["Q1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["Q1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["R1"].Value = "Hearing Remark";
                        //exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["R1"].AutoFitColumns(20);
                        //exWorkSheet1.Cells["R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["R1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["R1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["S1"].Value = "Next Hearing Date";
                        //exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["S1"].AutoFitColumns(10);
                        //exWorkSheet1.Cells["S1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["S1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["T1"].Value = "Expense Till Date";
                        //exWorkSheet1.Cells["T1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["T1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["T1"].AutoFitColumns(10);
                        ////exWorkSheet1.CellsT"M5"].Style.Numberformat.Format = "dd-MM-yyyy";
                        //exWorkSheet1.Cells["T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["T1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["U1"].Value = "Owner";
                        //exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["U1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["U1"].AutoFitColumns(20);
                        //exWorkSheet1.Cells["U1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["U1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["U1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["U1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["V1"].Value = "Law Firm";
                        //exWorkSheet1.Cells["V1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["V1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["V1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["V1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["W1"].Value = "Status UpdatedOn";
                        //exWorkSheet1.Cells["W1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["W1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["W1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["W1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["X1"].Value = "Period";
                        //exWorkSheet1.Cells["X1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["X1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["X1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["X1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        //exWorkSheet1.Cells["X1"].Style.Font.Bold = true;

                        //exWorkSheet1.Cells["Y1"].Value = "Tax Demand";
                        //exWorkSheet1.Cells["Y1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["Y1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["Y1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["Y1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["Z1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["Z1"].Value = "Interest";
                        //exWorkSheet1.Cells["Z1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["Z1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["Z1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["Z1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["AA1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["AA1"].Value = "Penalty";
                        //exWorkSheet1.Cells["AA1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["AA1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["AA1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["AA1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["AA1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["AA1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        //exWorkSheet1.Cells["AB1"].Style.Font.Bold = true;
                        //exWorkSheet1.Cells["AB1"].Value = "Provision on Book";
                        //exWorkSheet1.Cells["AB1"].Style.Font.Size = 12;
                        //exWorkSheet1.Cells["AB1"].AutoFitColumns(25);
                        //exWorkSheet1.Cells["AB1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet1.Cells["AB1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet1.Cells["AB1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet1.Cells["AB1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        #endregion MISCASE

                        #region Case Summary
                        //exWorkSheet2.Cells["A6"].LoadFromDataTable(ExcelData1, true);
                        int rowIndex = 7;
                        int ColIndex = 1;
                        decimal DisputedAmt = 0;
                        decimal Interest = 0;
                        decimal Penalty = 0;
                        decimal Total = 0;
                        decimal AmountPaid = 0;
                        decimal Favourable = 0;
                        decimal Unfavourable = 0;
                        decimal Expences = 0;
                        string perticulars = string.Empty;

                        for (int k = 0; k < MisReport1.Count;)
                        {
                            //foreach (var items in MisReport1)
                            //{
                            if (perticulars.Contains(MisReport1[k].Perticulars))
                            {
                                k++;
                            }


                            else
                            {
                                exWorkSheet2.Cells[rowIndex, ColIndex].Value = MisReport1[k].Perticulars;
                                exWorkSheet2.Cells[rowIndex, ColIndex].Style.Font.Bold = true;
                                exWorkSheet2.Cells[rowIndex, ColIndex].Style.Font.Size = 12;
                                exWorkSheet2.Cells.AutoFitColumns();
                                perticulars = MisReport1[k].Perticulars;

                                var UserMasterQuery = (from row in MisReport1
                                                       where row.Perticulars == MisReport1[k].Perticulars
                                                       select row).ToList();
                                rowIndex++;

                                for (int j = 0; j < UserMasterQuery.Count; j++)
                                {

                                    exWorkSheet2.Cells[rowIndex, ColIndex].Value = UserMasterQuery[j].StateName;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 1].Value = UserMasterQuery[j].DisputedAmt;
                                    DisputedAmt += UserMasterQuery[j].DisputedAmt;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 2].Value = UserMasterQuery[j].Interest;
                                    Interest += UserMasterQuery[j].Interest;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 3].Value = UserMasterQuery[j].Penalty;
                                    Penalty += UserMasterQuery[j].Penalty;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 4].Value = UserMasterQuery[j].Total;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.Font.Bold = true;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.Font.Size = 12;

                                    Total += UserMasterQuery[j].Total;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 5].Value = UserMasterQuery[j].AmountPaid;
                                    AmountPaid += UserMasterQuery[j].AmountPaid;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 6].Value = UserMasterQuery[j].Favourable;
                                    Favourable += UserMasterQuery[j].Favourable;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 7].Value = UserMasterQuery[j].Unfavourable;
                                    Unfavourable += UserMasterQuery[j].Unfavourable;
                                    exWorkSheet2.Cells[rowIndex, ColIndex + 8].Value = UserMasterQuery[j].Expense;
                                    Expences += UserMasterQuery[j].Expense;
                                    rowIndex += 1;
                                }
                            }

                        }
                        ////}
                        exWorkSheet2.Cells[rowIndex, ColIndex].Value = "Total";
                        exWorkSheet2.Cells[rowIndex, ColIndex].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 1].Value = DisputedAmt;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 1].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 1].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 1].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex+1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 2].Value = Interest;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 2].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 2].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 2].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheet2.Cells[rowIndex, ColIndex + 3].Value = Penalty;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 3].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 3].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 3].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheet2.Cells[rowIndex, ColIndex + 4].Value = Total;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 5].Value = AmountPaid;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 5].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 5].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 5].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 6].Value = Favourable;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 6].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 6].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 6].Style.Font.Size = 12;
                        ////exWorkSheet2.Cells[rowIndex, ColIndex + 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ////exWorkSheet2.Cells[rowIndex, ColIndex + 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 7].Value = Unfavourable;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 7].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 7].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 7].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells[rowIndex, ColIndex + 8].Value = Expences;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 8].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 8].Style.Font.Bold = true;
                        exWorkSheet2.Cells[rowIndex, ColIndex + 8].Style.Font.Size = 12;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //exWorkSheet2.Cells[rowIndex, ColIndex + 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        //Headers of Case Summary
                        exWorkSheet2.Cells["A1:I1"].Merge = true;
                        exWorkSheet2.Cells["A1:I1"].Style.WrapText = true;
                        exWorkSheet2.Cells["A1:I1"].Value = "EMAMI LIMITED";
                        exWorkSheet2.Cells["A1:I1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A1:I1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A1:I1"].AutoFitColumns(20);
                        exWorkSheet2.Cells["A1:I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["A1:I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells["A2:I2"].Merge = true;
                        exWorkSheet2.Cells["A2:I2"].Style.WrapText = true;
                        exWorkSheet2.Cells["A2:I2"].Value = "VAT, SALES TAX, ENTRY TAX & MISC. CASES AS ON " + DateTime.Now.ToString("dd-MMM-yyyy") + "";
                        exWorkSheet2.Cells["A2:I2"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A2:I2"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A2:I2"].AutoFitColumns(20);
                        exWorkSheet2.Cells["A2:I2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["A2:I2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheet2.Cells["A5:A6"].Merge = true;
                        exWorkSheet2.Cells["A5:A6"].Style.WrapText = true;
                        exWorkSheet2.Cells["A5:A6"].Value = "Particulars";
                        exWorkSheet2.Cells["A5:A6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A5:A6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A5:A6"].AutoFitColumns(20);
                        exWorkSheet2.Cells["A5:A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["A5:A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["A5:A6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["A5:A6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["B5:B6"].Merge = true;
                        exWorkSheet2.Cells["B5:B6"].Style.WrapText = true;
                        exWorkSheet2.Cells["B5:B6"].Value = "Disputed Amount";
                        exWorkSheet2.Cells["B5:B6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["B5:B6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["B5:B6"].AutoFitColumns(15);
                        exWorkSheet2.Cells["B5:B6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["B5:B6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["B5:B6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["B5:B6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["C5:C6"].Merge = true;
                        exWorkSheet2.Cells["C5:C6"].Style.WrapText = true;
                        exWorkSheet2.Cells["C5:C6"].Value = "Interest";
                        exWorkSheet2.Cells["C5:C6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["C5:C6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["C5:C6"].AutoFitColumns(10);
                        exWorkSheet2.Cells["C5:C6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["C5:C6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["C5:C6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["C5:C6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["D5:D6"].Merge = true;
                        exWorkSheet2.Cells["D5:D6"].Style.WrapText = true;
                        exWorkSheet2.Cells["D5:D6"].Value = "Penalty";
                        exWorkSheet2.Cells["D5:D6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["D5:D6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["D5:D6"].AutoFitColumns(10);
                        exWorkSheet2.Cells["D5:D6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["D5:D6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        ////exWorkSheet2.Cells["D5:D6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["D5:D6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["E5:E6"].Merge = true;
                        exWorkSheet2.Cells["E5:E6"].Style.WrapText = true;
                        exWorkSheet2.Cells["E5:E6"].Value = "Total";
                        exWorkSheet2.Cells["E5:E6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["E5:E6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["E5:E6"].AutoFitColumns(25);
                        exWorkSheet2.Cells["E5:E6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["E5:E6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["E5:E6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["E5:E6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["F5:F6"].Merge = true;
                        exWorkSheet2.Cells["F5:F6"].Style.WrapText = true;
                        exWorkSheet2.Cells["F5:F6"].Value = "Amount Paid";
                        exWorkSheet2.Cells["F5:F6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["F5:F6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["F5:F6"].AutoFitColumns(30);
                        exWorkSheet2.Cells["F5:F6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["F5:F6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["F5:F6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["F5:F6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["G5:G6"].Merge = true;
                        exWorkSheet2.Cells["G5:G6"].Style.WrapText = true;
                        exWorkSheet2.Cells["G5:G6"].Value = "Favourable";
                        exWorkSheet2.Cells["G5:G6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["G5:G6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["G5:G6"].AutoFitColumns(20);
                        exWorkSheet2.Cells["G5:G6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["G5:G6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["G5:G6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["G5:G6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["H5:H6"].Merge = true;
                        exWorkSheet2.Cells["H5:H6"].Style.WrapText = true;
                        exWorkSheet2.Cells["H5:H6"].Value = "Unfavourable";
                        exWorkSheet2.Cells["H5:H6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["H5:H6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["H5:H6"].AutoFitColumns(20);
                        exWorkSheet2.Cells["H5:H6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["H5:H6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["H5:H6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ////exWorkSheet2.Cells["H5:H6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet2.Cells["I5:I6"].Merge = true;
                        exWorkSheet2.Cells["I5:I6"].Style.WrapText = true;
                        exWorkSheet2.Cells["I5:I6"].Value = "Expense / Provision made in A/c";
                        exWorkSheet2.Cells["I5:I6"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["I5:I6"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["I5:I6"].AutoFitColumns(20);
                        exWorkSheet2.Cells["I5:I6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet2.Cells["I5:I6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //exWorkSheet2.Cells["I5:I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //exWorkSheet2.Cells["I5:I6"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        #endregion Case Summary

                        #region provions


                        exWorkSheetprovision.Cells["A1"].LoadFromDataTable(ExcelDataprovision, true);
                        exWorkSheetprovision.Cells["A1"].Value = "Title";
                        exWorkSheetprovision.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheetprovision.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheetprovision.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheetprovision.Cells["A1"].Style.WrapText = true;
                        exWorkSheetprovision.Cells["A1"].AutoFitColumns(25);
                        exWorkSheetprovision.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheetprovision.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        

                        exWorkSheetprovision.Cells["B1"].Value = "Details";
                        exWorkSheetprovision.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheetprovision.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheetprovision.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheetprovision.Cells["B1"].Style.WrapText = true;
                        exWorkSheetprovision.Cells["B1"].AutoFitColumns(40);
                        exWorkSheetprovision.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheetprovision.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        
                        exWorkSheetAuthority.Cells["A1:B1"].Value = "Details of Authorities of the State";
                        exWorkSheetAuthority.Cells["A1:B1"].Style.Font.Bold = true;
                        exWorkSheetAuthority.Cells["A1:B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheetAuthority.Cells["A1:B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheetAuthority.Cells["A1:B1"].Style.WrapText = true;
                        exWorkSheetAuthority.Cells["A1:B1"].Merge = true;
                        exWorkSheetAuthority.Cells["A1:B1"].Style.WrapText = true;
                        exWorkSheetAuthority.Cells["A1:B1"].AutoFitColumns(80);
                        exWorkSheetAuthority.Cells["A1:B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheetAuthority.Cells["A1:B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        exWorkSheetAuthority.Cells["A2"].Value = "Name"; 
                        exWorkSheetAuthority.Cells["A2"].LoadFromDataTable(ExcelDataAuthority,true);
                        exWorkSheetAuthority.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheetAuthority.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheetAuthority.Cells["A2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheetAuthority.Cells["A2"].Style.WrapText = true;
                        exWorkSheetAuthority.Cells["A2"].AutoFitColumns(30);
                        exWorkSheetAuthority.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheetAuthority.Cells["A2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        exWorkSheetAuthority.Cells["B2"].Value = "Designation";
                        exWorkSheetAuthority.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheetAuthority.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheetAuthority.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));
                        exWorkSheetAuthority.Cells["B2"].Style.WrapText = true;
                        exWorkSheetAuthority.Cells["B2"].AutoFitColumns(50);
                        exWorkSheetAuthority.Cells["B2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheetAuthority.Cells["B2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        #endregion


                        int countP = Convert.ToInt32(ExcelDataprovision.Rows.Count) + 1;
                        using (ExcelRange col = exWorkSheetprovision.Cells[1, 1, countP, 2])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }

                        int countA = Convert.ToInt32(ExcelDataAuthority.Rows.Count) + 2;
                        using (ExcelRange col = exWorkSheetAuthority.Cells[1, 1, countA, 2])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }


                        using (ExcelRange col = exWorkSheet2.Cells[5, 1, rowIndex, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            //col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }

                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                        int rows = 29 - checkedlist.Count();

                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, count, rows])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            
                        }
                        #endregion
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = "MIS-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName
                                                                                                                     //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            //}
            //else
            //{
            //    cvMyReport.IsValid = false;
            //    cvMyReport.ErrorMessage = "Please Select At Least one MIS Field.";
            //}
        }

    }
}



