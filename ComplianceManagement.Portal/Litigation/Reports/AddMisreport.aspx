﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddMisreport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports.AddMisreport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <script>
        function CloseMe() {

            window.parent.CloseMyADDMisReport();
            RefreshParent();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }


        function CheckAll(Checkbox) {
            var GrdMisrepportField = document.getElementById("<%=GrdMisrepportField.ClientID %>");
            for (i = 1; i < GrdMisrepportField.rows.length; i++) {
                GrdMisrepportField.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
            }
        }

    </script>
    <style>
        label {
    color: black;
}
    </style>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">

        <asp:ScriptManager ID="LitigationMISreport" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="MISreport" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
             
                <div class="row colpadding0">
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="MyReportValidationGroup" />
                        <asp:CustomValidator ID="cvMyReport" runat="server" EnableClientScript="False"
                            ValidationGroup="MyReportValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                </div>
                <div class="row colpadding0" style="overflow-y: hidden;display:none;" >
                        <asp:CheckBox runat="server" Checked="true" ID="CourtCaseNo"  Text="Court Case No." ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true"  ID="InternalCaseNo" Text="Internal case file no." ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true"  ID="State" Text="State Name" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true"  ID="FY" Text="F.Y." ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true"  ID="details" Text="Case details description" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true"  ID="taxdemand" Text="Tax Demand" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="interest" Text="Interest" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="penalty" Text="Penalty" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="total" Text="Total(Claim Amt.)" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="amountpaid" Text="Amount Paid" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="court" Text="Court" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="lawfirm" Text="Law Firm" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="dateofhearing" Text="Date of hearing" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="hearingdesc" Text="Hearing description" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="nexthearing" Text="Next hearing date" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="expense" Text="Exp till date" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="provision" Text="Provision in Book" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="statusremark" Text="Status Remark" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="casetype" Text="Case Type" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="caseopendate" Text="Case Open date" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="status" Text="Status" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="casetitle" Text="case Title" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="typeofcase" Text="Type of Case" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="opponent" Text="Opponent" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="judge" Text="Judge" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="entity" Text="Entity_Location_Branch" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="owner" Text="Owner" ></asp:CheckBox>
                        <asp:CheckBox runat="server" Checked="true" ID="statusupdateon" Text="Status_Updated_on" ></asp:CheckBox>

                    <asp:GridView Visible="false" ID="GrdMisrepportField" runat="server" AutoGenerateColumns="False"
                        AutoPostBack="true" CssClass="table" ShowHeader="true" GridLines="none"
                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">

                        <Columns>

                            <asp:TemplateField HeaderText="Checked/Uncheked Fields">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkHeader" runat="server" Checked="true" onclick="CheckAll(this)" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox ID="CbMISIsfield" runat="server" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CbMISIsfield" runat="server" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MIS_Report Fields_Name">

                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ID="lblMISFieldName"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle Font-Bold="true" Font-Underline="true" />
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />

                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>

                </div>
                <%--<asp:Button ID="Button1" runat="server" onclick="Button1_Click"   
        Text="Save to Database" />  --%>
        </div>
        <div class="row colpadding0">
            <div class="col-md-12 colpadding0 text-center">
                <asp:Button Text="Download" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="btn btn-primary" OnClientClick="CloseMe()" />
            </div>
        </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
