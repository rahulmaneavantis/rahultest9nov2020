﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="NoticeDetailPageNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.NoticeDetailPageNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">
        //$(function () {
        //    $('[id*=ddlLawFirm]').multiselect({
        //        includeSelectAllOption: true,
        //        numberDisplayed: 1,
        //        buttonWidth: '70%'
        //    });
        //});

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtNoticeDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });

            $(function () {
                $('input[id*=tbxResponseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });

            $(function () {
                $('input[id*=tbxTaskDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });

            $(function () {
                $('input[id*=tbxNoticeCloseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });

            $(function () {
                $('input[id*=tbxPaymentDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });


        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function hideDivBranch() {

            $('#divBranches').hide("blind", null, 500, function () { });
        }

        $(document).ready(function () {

            $("#<%=tbxBranch.ClientID %>").unbind('click');

            $("#<%=tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });

        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {

            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function ddlActChange() {

            var selectedActID = $("#<%=ddlAct.ClientID %>").val();
            if (selectedActID != null) {
                if (selectedActID == 0) {
                    $("#lnkShowAddNewActModal").show();
                }
                else {
                    $("#lnkShowAddNewActModal").hide();
                }
            }
        }

        function ddlPartyChange() {

            var selectedPartyID = $("#<%=ddlParty.ClientID %>").val();
            if (selectedPartyID != null) {
                if (selectedPartyID == 0) {
                    $("#lnkShowAddNewPartyModal").show();
                }
                else {
                    $("#lnkShowAddNewPartyModal>").hide();
                }
            }
        }

        function ddlNoticeCategoryChange() {

            var selectedNoticeCatID = $("#<%=ddlNoticeCategory.ClientID %>").val();
            if (selectedNoticeCatID != null) {
                if (selectedNoticeCatID == 0) {
                    $("#lnkAddNewNoticeCategoryModal").show();
                }
                else {
                    $("#lnkAddNewNoticeCategoryModal").hide();
                }
            }
        }

        function ddlDepartmentChange() {

            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

         $(document).ready(function () {
             ddlStatusChange();
        });

        function ddlStatusChange() {
            
            var selectedStatusID = $("#<%=ddlNoticeStatus.ClientID %>").val();
             if (selectedStatusID != null) {
                 if (selectedStatusID == 3) {
                     $("#divClosureDetail").show();
                     $("#divClosureRemark").show();
                     $("#divConvertToCase").show();
                 }
                 else {
                     $("#divClosureDetail").hide();
                     $("#divClosureRemark").hide();
                     $("#divConvertToCase").hide();
                 }
             }
         }

         function ddlTaskUserChange() {

            var selectedTaskUserID = $("#<%=ddlTaskUser.ClientID %>").val();
            if (selectedTaskUserID != null) {
                if (selectedTaskUserID == 0) {
                    $("#lnkShowAddNewOwnerModal").show();
                }
                else {
                    $("#lnkShowAddNewOwnerModal>").hide();
                }
            }
        }
                
         function openNoticeModal() {
             $('#divAddNoticeModal').modal('show');
         }

         function openAddNewActModal() {
             $('#divAddNewActModal').modal('show');
         }
        
     <%-- $(document).on("click", "<%=upNoticePopup.ClientID %>", function (event) {
            
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvBranches') > -1) {
                    $("#divBranches").show();
                } else {
                    $("#divBranches").hide();
                }
            }
            else if (event.target.id != "<%=tbxBranch.ClientID %>") {
                $("#divBranches").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvBranches') > -1) {
                $("#divBranches").show();
            } else if (event.target.id == "<%=tbxBranch.ClientID %>") {
                 $("#<%=tbxBranch.ClientID %>").unbind('click');

                 $("#<%=tbxBranch.ClientID %>").click(function () {
                     $("#divBranches").toggle("blind", null, 500, function () { });
                 });
             }
        }); --%>

        function validateForm() {
            
            if ($('#<%=tbxRefNo.ClientID %>').val() == '') {
                $('#<%=tbxRefNo.ClientID %>').css('border-color', 'red');
            }
            else {
                $('#tbxRefNo').css('border-color', '');
            }
        }

        //My Code Start
        <%--function frefilldept(){
            document.getElementById('<%= btnfilldept.ClientID %>').click();
         }--%>

        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenAddActPopup() {
            $('#AddActPopUp').modal('show');
            $('#IframeAct').attr('src', "../../Litigation/Masters/AddAct.aspx");
        }

        function OpenPartyDetailsPopup() {
            $('#AddPartyPopUp').modal('show');
            $('#IframeParty').attr('src', "../../Litigation/Masters/AddPartyDetails.aspx");
        }

        function OpenCategoryTypePopup() {
            var a = '';
            $('#AddCategoryType').modal('show');
            $('#IframeCategoryType').attr('src', "../../Litigation/Masters/AddCaseType.aspx?CaseTypeId=" + a);
        }

        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "../../Litigation/Masters/AddUser.aspx");
        }

        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            document.getElementById('<%= lnkBtnDept.ClientID %>').click();
        }

        function ClosePopAct() {
            $('#AddActPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAct.ClientID %>').click();
        }

        function ClosePopParty() {
            $('#AddPartyPopUp').modal('hide');
            document.getElementById('<%= lnkBtnParty.ClientID %>').click();
        }

        function CloseCaseTypePopUp() {
            $('#AddCategoryType').modal('hide');
            document.getElementById('<%= lnkBtnCategory.ClientID %>').click();
        }
                
        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAssignUser.ClientID %>').click();
        }
        
        function CloseNoticeDetailPage() {
            window.parent.ClosePopNoticeDetialPage();
        }

    </script>

    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        .Shorter {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 100%;
            display:block;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            imgExpandCollapse();
        });

        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {
            $("[src*=collapse]").on('click', function () {
                
                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {
                
                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }


        function ChangeRowColor(rowID) {
            
            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            //alert(color);

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            //alert(oldColor);

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }
        
    </script>

    <%--<script type="text/javascript">
        $(document).ready(function () {
            $("[src*=collapse]").on('click', function () {
                
                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {
                
                $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                $(this).attr("src", "/Images/collapse.png");
            });
        });
    </script>--%>

  <script type="text/javascript">
     
      function divExpandCollapse(divname) {
          
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {               
                div.style.display = "inline";
                img.src = "/Images/remove.png";
            } else {
                div.style.display = "none";
                img.src = "/Images/add.png";
            }
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <%-- <div class="row Dashboard-white-widget">
        <div class="dashboard">--%>
            <input type="hidden" id="hiddenColor" style="display:none;"  />

        <div style="background-color: #f7f7f7;">
            <header class="panel-heading tab-bg-primary" style="background: none !important; margin-bottom: 10px;">
                <ul class="nav nav-tabs">
                    <li class="active" id="liNoticeDetail" runat="server">
                        <asp:LinkButton ID="lnkNoticeDetail" OnClick="lnkNoticeDetail_Click" runat="server" Style="background-color: #f7f7f7;">Notice Summary</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeTask" runat="server">
                        <asp:LinkButton ID="lnkNoticeTask" OnClick="lnkNoticeTask_Click" runat="server" Style="background-color: #f7f7f7;">Task/Activity</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeResponse" runat="server">
                        <asp:LinkButton ID="lnkNoticeResponse" OnClick="lnkNoticeResponse_Click" runat="server" Style="background-color: #f7f7f7;">Response</asp:LinkButton>
                    </li>
                    <li class="" id="liNoticeStatusPayment" runat="server">
                        <asp:LinkButton ID="lnkNoticeStatusPayment" OnClick="lnkNoticeStatusPayment_Click" runat="server" Style="background-color: #f7f7f7;">Status/Payment</asp:LinkButton>
                    </li>
                </ul>
            </header>

            <div class="clearfix" style="height: 20px;"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <asp:MultiView ID="MainView" runat="server">
                <asp:View ID="firstTabNoticeSummary" runat="server">
                    <div style="width: 100%; float: left; margin-bottom: 15px">
                        <div class="container">

                            <div style="margin-bottom: 7px">
                                <asp:ValidationSummary ID="VSNoticePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="NoticePopUpValidationGroup" />
                                <asp:CustomValidator ID="cvNoticePopUp" runat="server" EnableClientScript="False"
                                    ValidationGroup="NoticePopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </div>

                            <div id="divNoticeDetails" class="row Dashboard-white-widget">
                                <!--NoticeDetail Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Notice Detail">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                <a>
                                                    <h2></h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivNoticeDetails" class="panel-collapse collapse in">
                                            <div class="panel-body">

                                                <asp:Panel ID="pnlNotice" runat="server">

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Type</label>
                                                            <%--<div class="radio radiobuttonlist">--%>
                                                                <asp:RadioButtonList ID="rbNoticeInOutType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Inward" Value="I" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Outward" Value="O"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            <%--</div>--%>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Dated</label>
                                                            <asp:TextBox runat="server" ID="txtNoticeDate" autocomplete="off" Style="width: 70%; background-color: #fff; cursor: pointer;"
                                                                CssClass="form-control" MaxLength="100" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Notice Date can not be empty."
                                                                ControlToValidate="txtNoticeDate" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reference No.</label>
                                                            <asp:TextBox runat="server" ID="tbxRefNo" Style="width: 70%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="rfvRefNo" ErrorMessage="Reference No. can not be empty."
                                                                ControlToValidate="tbxRefNo" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Opposition/Party</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlParty" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    DataPlaceHolder="Select Opposition/Party" CssClass="form-control" Width="100%" onchange="ddlPartyChange()" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Party"
                                                                    ControlToValidate="ddlParty" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <img id="lnkShowAddNewPartyModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenPartyDetailsPopup()" alt="Add New Party" title="Add New Party" />
                                                                <asp:LinkButton ID="lnkBtnParty" OnClick="lnkBtnParty_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Act</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlAct" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" DataPlaceHolder="Select Act" Width="100%" onchange="ddlActChange()" />
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged"--%>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act or Select 'Not Applicable'"
                                                                    ControlToValidate="ddlAct" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <img id="lnkShowAddNewActModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddActPopup()" alt="Add New Party" title="Add New Party" />
                                                                <asp:LinkButton ID="lnkBtnAct" OnClick="lnkBtnAct_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Under Section</label>
                                                            <asp:TextBox runat="server" ID="tbxSection" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Category</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlNoticeCategory" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Notice Category" onchange="ddlNoticeCategoryChange()">
                                                                </asp:DropDownListChosen>
                                                                <asp:RequiredFieldValidator ID="rfvNoticeCategory" ErrorMessage="Please Select Notice Category"
                                                                    ControlToValidate="ddlNoticeCategory" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <img id="lnkAddNewNoticeCategoryModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCategoryTypePopup()" alt="Add New Party" title="Add New Party" />
                                                                <asp:LinkButton ID="lnkBtnCategory" OnClick="lnkBtnCategory_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Title</label>
                                                            <asp:TextBox runat="server" ID="tbxTitle" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty."
                                                                ControlToValidate="tbxTitle" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Description</label>
                                                            <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="width: 85%;" CssClass="form-control" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                                ControlToValidate="tbxDescription" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Entity/Location</label>
                                                            
                                                            <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 70%; background-color: #fff; cursor: pointer;"
                                                                autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" ReadOnly="true" />
                                                            <%--onclick="txtclick()"--%>
                                                            <div style="margin-left: 28%; position: absolute; z-index: 10;    width: 70%;" id="divBranches">
                                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                    Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                </asp:TreeView>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                ControlToValidate="tbxBranch" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Department</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    DataPlaceHolder="Select Department" class="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department"
                                                                    ControlToValidate="ddlDepartment" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <img id="lnkAddNewDepartmentModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                                                <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Owner</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlOwner" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="70%" />
                                                            <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                ControlToValidate="ddlOwner" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Winning Prospect</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlNoticeRisk" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="70%">
                                                                <%-- <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>--%>
                                                                <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Claimed Amount</label>
                                                            <asp:TextBox runat="server" ID="tbxClaimedAmt" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            <asp:CompareValidator ID="cvClaimedAmt" runat="server" ControlToValidate="tbxClaimedAmt" ErrorMessage="Only Numbers in Claimed Amount."
                                                                ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Probable Amount</label>
                                                            <asp:TextBox runat="server" ID="tbxProbableAmt" Style="width: 70%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                            <asp:CompareValidator ID="cvProbableAmt" runat="server" ControlToValidate="tbxProbableAmt" ErrorMessage="Only Numbers in Probable Amount."
                                                                ValidationGroup="NoticePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Upload Documents</label>
                                                            <div style="width: 100%;">
                                                                <div style="width: 50%; float: left;">
                                                                    <asp:FileUpload ID="NoticeFileUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93;" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </asp:Panel>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <asp:UpdatePanel ID="upNoticeDocUploadPopup" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdNoticeDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                    OnRowCommand="grdNoticeDocuments_RowCommand" OnRowDataBound="grdNoticeDocuments_RowDataBound" OnPageIndexChanging="grdNoticeDocuments_OnPageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Document" ItemStyle-Width="40%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                    <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                    <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("ID")%>' CommandName="DownloadNoticeDoc"
                                                                                            ID="lnkBtnDownLoadNoticeDoc" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download"  /> <%--width="15" height="15"--%>
                                                                                        </asp:LinkButton>

                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                            AutoPostBack="true" CommandName="DeleteNoticeDoc"
                                                                                            OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                            ID="lnkBtnDeleteNoticeDoc" runat="server">
                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete"  /> <%--width="15" height="15"--%>
                                                                                        </asp:LinkButton>

                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDownLoadNoticeDoc" />
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDeleteNoticeDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <%--<PagerTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </PagerTemplate>--%>
                                                                    <EmptyDataTemplate>
                                                                        No Records Found.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--NoticeDetail Panel End-->
                            </div>

                            <div id="divNoticeAssignmentDetails" class="row Dashboard-white-widget">
                                <!--NoticeAssignment Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Notice Detail">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeAssignmentDetails">
                                                <a>
                                                    <h2>Assignment Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseDivNoticeAssignmentDetails">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivNoticeAssignmentDetails" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <asp:Panel ID="pnlNoticeAssignment" runat="server">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Lawyer Firm</label>
                                                            <asp:DropDownListChosen ID="ddlLawFirm" CssClass="form-control" AutoPostBack="true" runat="server" DataPlaceHolder="Select User" Width="70%"
                                                                AllowSingleDeselect="false"></asp:DropDownListChosen>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Assigned To</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlPerformer" DataPlaceHolder="Select Performer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="70%" />
                                                        </div>
                                                    </div>

                                                    <div class="row" style="display: none;">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reviewer</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlReviewer" DataPlaceHolder="Select Reviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                CssClass="form-control" Width="70%" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--NoticeAssignment Panel End-->
                            </div>

                            <div class="form-group col-md-12" style="text-align: center;">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSaveNotice_Click"
                                    ValidationGroup="NoticePopUpValidationGroup" OnClientClick="validateForm();"></asp:Button>
                                 <asp:Button Text="Clear" runat="server" ID="btnClearNoticeDetail" CssClass="btn btn-primary" OnClick="btnClearNoticeControls_Click" />
                                 <asp:Button Text="Edit Details" runat="server" ID="btnEditNoticeDetail" CssClass="btn btn-primary" OnClick="btnEditNoticeControls_Click" />
                            </div>

                            <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                        </div>
                    </div>
                </asp:View>

                <asp:View ID="secondTabTask" runat="server">
                    <div style="width: 100%; float: left; margin-bottom: 15px">
                        <div class="container">

                            <div id="secondTabAccordion">

                                <div class="row Dashboard-white-widget">
                                    <!--Action Log Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Notice Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskLogs">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivTaskLogs" class="panel-collapse collapse in">
                                               
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:Panel ID="pnlTask" runat="server">
                                                            <div class="row">
                                                                <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="NoticePopUpTaskValidationGroup" />
                                                                <asp:CustomValidator ID="cvNoticePopUpTask" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Task Title</label>
                                                                    <asp:TextBox ID="tbxTaskTitle" runat="server" CssClass="form-control" Width="70%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvTaskTitle" ErrorMessage="Provide Task Title"
                                                                        ControlToValidate="tbxTaskTitle" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Due Date</label>
                                                                    <asp:TextBox ID="tbxTaskDueDate" runat="server" CssClass="form-control" Width="35%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvTaskDueDate" ErrorMessage="Provide Due Date"
                                                                        ControlToValidate="tbxTaskDueDate" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Lawyer Firm</label>
                                                                    <asp:DropDownListChosen ID="ddlTaskLawyerList" CssClass="form-control" runat="server" Width="70%" 
                                                                        OnSelectedIndexChanged="ddlTaskLawyerList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownListChosen>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Assigned To</label>
                                                                    <div style="float: left; width: 60%">
                                                                        <asp:DropDownListChosen runat="server" ID="ddlTaskUser" DataPlaceHolder="Select User" Onchange="ddlTaskUserChange()" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                            CssClass="form-control" Width="100%" />
                                                                        <asp:RequiredFieldValidator ID="rfvTaskUser" ErrorMessage="Please Select User to Assign Task"
                                                                            ControlToValidate="ddlTaskUser" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                    </div>
                                                                    <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                        <img id="lnkShowAddNewOwnerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddUserDetailPop()" alt="Add New User" title="Add New User" />
                                                                        <asp:LinkButton ID="lnkBtnAssignUser" OnClick="lnkAddNewUser_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Priority</label>
                                                                    <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" DataPlaceHolder="Select Task Priority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        CssClass="form-control" Width="70%">
                                                                        <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                    </asp:DropDownListChosen>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Description</label>
                                                                    <asp:TextBox ID="tbxTaskDesc" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvTaskDesc" ErrorMessage="Provide Task Description"
                                                                        ControlToValidate="tbxTaskDesc" runat="server" ValidationGroup="NoticePopUpTaskValidationGroup" Display="None" />
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                    <asp:TextBox ID="tbxTaskRemark" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                    <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Upload Document</label>
                                                                    <div style="width: 100%;">
                                                                        <div style="width: 50%; float: left;">
                                                                            <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93 !important; margin-bottom: 15px;" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-12" style="text-align: center;">
                                                                    <asp:Button Text="Save" runat="server" ID="btnTaskSave" CssClass="btn btn-primary" OnClick="btnTaskSave_Click"
                                                                        ValidationGroup="NoticePopUpTaskValidationGroup"></asp:Button>
                                                                    <asp:Button Text="Clear" runat="server" ID="btnTaskClear" CssClass="btn btn-primary" OnClick="btnClearTask_Click" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="upNoticeTaskActivity" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                            OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                            PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right" OnRowCreated="grdTaskActivity_RowCreated"
                                                                            OnPageIndexChanging="grdTaskActivity_OnPageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%--<img id="imgdiv<%# Eval("TaskID") %>" class="sample" src="/Images/add.png" alt="Show Responses" style="cursor: pointer" />--%>
                                                                                        <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                                                        <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                            <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table"
                                                                                                Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                 OnRowDataBound="grdTaskResponseLog_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                       <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                                                                    ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>                                                                                                 

                                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                                <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                </asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:LinkButton
                                                                                                                        CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                        ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                    </asp:LinkButton>

                                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                                        AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                                        ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                                                    </asp:LinkButton>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                                <EmptyDataTemplate>
                                                                                                    No Response Submitted yet.
                                                                                                </EmptyDataTemplate>
                                                                                            </asp:GridView>
                                                                                        </asp:Panel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: auto">
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                            <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' CssClass="btn btn-primary"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to close this Task?');"
                                                                                                        AutoPostBack="true" CommandName="CloseTask" Text="Close Task"
                                                                                                        ID="lnkBtnCloseTask" runat="server">
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="TaskReminder"
                                                                                                        ID="lnkBtnTaskReminder" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/send_icon.png")%>' alt="Send Reminder" title="Send Reminder"  /> <%--width="15" height="15" CssClass="btn btn-primary"--%>
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteTask"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Task Detail?');"
                                                                                                        ID="lnkBtnDeleteTask" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete" />
                                                                                                    </asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnCloseTask" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteTask" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />

                                                                            <EmptyDataTemplate>
                                                                                No Records Found.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!--Task/Activity Panel End-->
                                </div>

                            </div>
                        </div>
                    </div>
                </asp:View>

                <asp:View ID="thirdTabResponse" runat="server">
                    <div style="width: 100%; float: left; margin-bottom: 15px">
                        <div class="container">

                            <div class="row Dashboard-white-widget">
                                <!--Response Detail Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Notice Detail">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivResponseLogs">
                                                <a>
                                                    <h2></h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivResponseLogs">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivResponseLogs" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="container">
                                                    <asp:Panel ID="pnlResponse" runat="server">
                                                        <div class="row">
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="NoticePopUpResponseValidationGroup" />
                                                            <asp:CustomValidator ID="cvNoticePopUpResponse" runat="server" EnableClientScript="False"
                                                                ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Responded On</label>
                                                                <asp:TextBox ID="tbxResponseDate" runat="server" CssClass="form-control" Width="30%"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvRespDate" ErrorMessage="Provide Response Date." runat="server"
                                                                    ControlToValidate="tbxResponseDate" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Delivery Mode</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlRespBy" DataPlaceHolder="Select Responded By"
                                                                    AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="30%">
                                                                    <asp:ListItem Text="Courier" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Post" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Other" Value="0"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                                <asp:RequiredFieldValidator ID="rfvRespBy" ErrorMessage="Select Delivery Mode (i.e. Courier/ Post/ Other)." runat="server"
                                                                    ControlToValidate="ddlRespBy" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">Courier Company/Post Detail</label>
                                                                <%--width: 0%;--%>
                                                                <asp:TextBox ID="tbxRespThrough" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvRespThrough" ErrorMessage="Provide Responded through (i.e. Courier Company/ Post Office Detail)." runat="server"
                                                                    ControlToValidate="tbxRespThrough" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Ref/Tracking No.</label>
                                                                <asp:TextBox ID="tbxRespRefNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvRespRefNo" ErrorMessage="Provide Reference/ Courier/ Post Tracking Number, Put 'NA' if not available."
                                                                    runat="server" ControlToValidate="tbxRespRefNo" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">Description</label>
                                                                <asp:TextBox ID="tbxResponseDesc" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvRespDesc" ErrorMessage="Provide Response Description"
                                                                    runat="server" ControlToValidate="tbxResponseDesc" ValidationGroup="NoticePopUpResponseValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 20%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                <asp:TextBox ID="tbxResponseRemark" runat="server" CssClass="form-control" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 20%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Upload Response Document</label>
                                                                <div style="width: 100%;">
                                                                    <div style="width: 50%; float: left;">
                                                                        <asp:FileUpload ID="fuResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12" style="text-align: center;">
                                                                <asp:Button Text="Save" runat="server" ID="btnSaveResponse" CssClass="btn btn-primary" OnClick="btnSaveResponse_Click"
                                                                    ValidationGroup="NoticePopUpResponseValidationGroup"></asp:Button>
                                                                <asp:Button Text="Clear" runat="server" ID="btnResponseClear" CssClass="btn btn-primary" OnClick="btnClearResponse_Click" />
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <asp:UpdatePanel ID="upResponseDocUpload" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView runat="server" ID="grdResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                        OnPageIndexChanging="grdResponseLog_OnPageIndexChanging" OnRowCommand="grdResponseLog_RowCommand" OnRowDataBound="grdResponseLog_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Responded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblResponseDate" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ResponseDate") %>'
                                                                                            Text='<%# Eval("ResponseDate") != DBNull.Value ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                        <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%" HeaderStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowNoticeResponseDocCount((long)Eval("NoticeInstanceID"),(long)Eval("ID")) %>'>
                                                                                        </asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="upResDocDelete" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton
                                                                                                CommandArgument='<%# Eval("ID")+","+ Eval("NoticeInstanceID")%>' CommandName="DownloadResponseDoc"
                                                                                                ID="lnkBtnDownLoadResponseDoc" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                AutoPostBack="true" CommandName="DeleteResponse"
                                                                                                OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                ID="lnkBtnDeleteResponse" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDownLoadResponseDoc" />
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDeleteResponse" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <PagerTemplate>
                                                                            <%-- <table style="display: none">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>--%>
                                                                        </PagerTemplate>
                                                                        <EmptyDataTemplate>
                                                                            No Previous Response Records Found.
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Response Detail Panel End-->
                            </div>


                        </div>
                    </div>
                </asp:View>

                <asp:View ID="fourthTabStatusPayment" runat="server">
                    <div style="width: 100%; float: left; margin-bottom: 15px">
                        <div class="container">
                            <div class="row Dashboard-white-widget">
                                <!--Status Log Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View/Edit Notice Status">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivStatusLogs">
                                                <a>
                                                    <h2>Status</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivStatusLogs">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivStatusLogs" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                
                                                <div style="margin-bottom: 7px">
                                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="NoticePopUpStatusValidationGroup" />
                                                    <asp:CustomValidator ID="cvNoticeStatus" runat="server" EnableClientScript="False"
                                                        ValidationGroup="NoticePopUpStatusValidationGroup" Display="None" />
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Notice Status</label>
                                                        <asp:DropDownListChosen runat="server" ID="ddlNoticeStatus" DataPlaceHolder="Select Status"  AllowSingleDeselect="false" DisableSearchThreshold="5" 
                                                            CssClass="form-control" Width="40%" onchange="ddlStatusChange()">
                                                            <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="In Progress" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Close" Value="3"></asp:ListItem>
                                                        </asp:DropDownListChosen>
                                                    </div>

                                                    <div class="form-group col-md-6" id="divClosureDetail">
                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Close Date</label>
                                                        <asp:TextBox runat="server" ID="tbxNoticeCloseDate" autocomplete="off" Style="width: 40%; background-color: #fff; cursor: pointer;"
                                                            CssClass="form-control" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12" id="divClosureRemark">
                                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                        <asp:TextBox ID="tbxCloseRemark" runat="server" CssClass="form-control" Width="85%" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12" style="text-align: center; margin: auto; width: 30%">
                                                        <div style="float: left;">
                                                            <asp:Button Text="Save" runat="server" ID="btnSaveStatus" CssClass="btn btn-primary" OnClick="btnSaveStatus_Click"
                                                                ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                        </div>
                                                        <div id="divConvertToCase" style="float: left; margin-left: 1%">
                                                            <asp:Button Text="Save & Convert to Case" runat="server" ID="btnSaveConvertCase" CssClass="btn btn-primary" 
                                                                OnClick="btnSaveConvertToCase_Click" ValidationGroup="NoticePopUpStatusValidationGroup"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Status Log Panel End-->
                            </div>

                            <div class="row Dashboard-white-widget">
                                <!--Payment Log Panel Start-->
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Payment Detail">
                                            <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivPaymentLog">
                                                <a>
                                                    <h2>Payment Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivPaymentLog">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseDivPaymentLog" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div style="margin-bottom: 7px">
                                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="NoticePopUpPaymentLogValidationGroup" />
                                                    <asp:CustomValidator ID="cvNoticePayment" runat="server" EnableClientScript="true"
                                                        ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                </div>

                                                <div class="form-group col-md-12">

                                                    <asp:UpdatePanel ID="upNoticePayment" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView runat="server" ID="grdNoticePayment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                OnRowCommand="grdNoticePayment_RowCommand" OnRowDataBound="grdNoticePayment_RowDataBound" PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                OnPageIndexChanging="grdNoticePayment_OnPageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Date" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate") != null ? Convert.ToDateTime(Eval("PaymentDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="tbxPaymentDate" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvPaymentDate" ErrorMessage="Provide Payment Date." runat="server"
                                                                                ControlToValidate="tbxPaymentDate" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Type" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PaymentType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:DropDownListChosen runat="server" ID="ddlPaymentType" DataPlaceHolder="Payment Type"  AllowSingleDeselect="false" DisableSearchThreshold="5" 
                                                                                CssClass="form-control" Width="100%"></asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Select Payment Type."
                                                                                ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Amount" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="tbxAmount" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvAmount" ErrorMessage="Provide Payment Amount."
                                                                                ControlToValidate="tbxAmount" runat="server" ValidationGroup="NoticePopUpPaymentLogValidationGroup" Display="None" />
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="30%" FooterStyle-Width="30%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPaymentRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="tbxPaymentRemark" runat="server" class="form-control"></asp:TextBox>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%"
                                                                        FooterStyle-Width="10%" FooterStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                            AutoPostBack="true" CommandName="DeletePayment"
                                                                                            OnClientClick="return confirm('Are you sure!! You want to Delete this Payment Detail?');"
                                                                                            ID="lnkBtnDeletePayment" runat="server">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" 
                                                                                                title="Delete" />
                                                                                        </asp:LinkButton>
                                                                                        <%--"~/Images/delete_icon.png"--%>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeletePayment" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:Button CssClass="btn btn-primary" ID="btnPaymentSave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="NoticePopUpPaymentLogValidationGroup" OnClick="btnPaymentSave_Click"></asp:Button>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <RowStyle CssClass="clsROWgrid" />
                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                <PagerTemplate>
                                                                    <%--<table style="display: none">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                </td>
                                                                            </tr>
                                                                        </table>--%>
                                                                </PagerTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Records Found.
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Payment Log Panel End-->
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

            <%--Department Popup--%>
            <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                            <%--onclick="frefilldept()"--%>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Act Popup--%>
            <div class="modal fade" id="AddActPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAct" frameborder="0" runat="server" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Party Details--%>
            <div class="modal fade" id="AddPartyPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="550px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Legal Notice category--%>
            <div class="modal fade" id="AddCategoryType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCategoryType" frameborder="0" runat="server" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

             <%--Add User--%>
            <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      <%--  </div>
    </div>--%>
</asp:Content>
