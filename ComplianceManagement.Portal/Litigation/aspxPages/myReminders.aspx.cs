﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class myReminders : System.Web.UI.Page
    {
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                flag = false;
                BindGrid();
                bindPageNumber();
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdReminders.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid(); bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdReminders.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            grdReminders.PageIndex = chkSelectedPage - 1;
            grdReminders.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid(); ShowGridDetail();
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                long customerID = -1;
                //customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int priorityID = -1;
                int partyID = -1;
                int deptID = -1;
                int Status = 1;
                string Type = string.Empty;

                if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    var branchList = new List<int>();

                    ddlTitle.Items.Clear();

                    if (ddlTypePage.SelectedValue == "1") //1--Notice
                    {
                        var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
                        if (lstNoticeDetails.Count > 0)
                        {
                            ddlTitle.DataTextField = "NoticeTitle";
                            ddlTitle.DataValueField = "NoticeInstanceID";
                            ddlTitle.DataSource = lstNoticeDetails;
                            ddlTitle.DataBind();
                            ddlTitle.Items.Insert(0, new ListItem("ALL", "-1"));
                        }
                        else
                        {
                            ddlTitle.Items.Insert(0, new ListItem("No Notice Record Found", "-1"));
                        }
                    }
                    else if (ddlTypePage.SelectedValue == "2") //2--Case
                    {
                        var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
                        if (lstCaseDetails.Count > 0)
                        {
                            ddlTitle.DataTextField = "CaseTitle";
                            ddlTitle.DataValueField = "CaseInstanceID";
                            ddlTitle.DataSource = lstCaseDetails;
                            ddlTitle.DataBind();
                            ddlTitle.Items.Insert(0, new ListItem("ALL", "-1"));
                        }
                        else
                        {
                            ddlTitle.Items.Insert(0, new ListItem("No Case Record Found", "-1"));
                        }
                    }
                    else if (ddlTypePage.SelectedValue == "3") //3--Task
                    {
                        var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, Status, Type, AuthenticationHelper.CustomerID);

                        if (lstTaskDetails.Count > 0)
                        {
                            lstTaskDetails = lstTaskDetails.Where(entry => entry.StatusID != 3).ToList(); //Filter Closed Tasks

                            ddlTitle.DataTextField = "TaskTitle";
                            ddlTitle.DataValueField = "TaskID";
                            ddlTitle.DataSource = lstTaskDetails;
                            ddlTitle.DataBind();
                            ddlTitle.Items.Insert(0, new ListItem("ALL", "-1"));
                        }
                        else
                        {
                            ddlTitle.Items.Insert(0, new ListItem("No Task Record Found", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddReminderClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowReminderDialog(" + 0 + ");", true);
        }

        protected void lnkEditReminder_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    int ReminderID = Convert.ToInt32(btn.CommandArgument);

                    if (ReminderID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowReminderDialog(" + ReminderID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorPage.IsValid = false;
                cvErrorPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int reminderType = 0; //0-All, 1-Notice, 2-Case, 3-Task
                int instanceID = -1;

                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    reminderType = Convert.ToInt32(ddlTypePage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlTitle.SelectedValue) && ddlTitle.SelectedValue != "-1")
                {
                    instanceID = Convert.ToInt32(ddlTitle.SelectedValue);
                }

                var lstReminderDetails = LitigationManagement.GetAssignedReminderList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, reminderType, instanceID);
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstReminderDetails = lstReminderDetails.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstReminderDetails = lstReminderDetails.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;

                Session["TotalRows"] = null;
                if (lstReminderDetails.Count > 0)
                {
                    grdReminders.DataSource = lstReminderDetails;
                    grdReminders.DataBind();
                    Session["TotalRows"] = lstReminderDetails.Count;
                }
                else
                {
                    grdReminders.DataSource = lstReminderDetails;
                    grdReminders.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdReminders_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int reminderID = 0;
                if (e.CommandName.Equals("DELETE_Reminder"))
                {
                    reminderID = Convert.ToInt32(e.CommandArgument);
                    bool deleteSuccess = false;
                    if (reminderID != 0)
                    {
                        deleteSuccess = LitigationManagement.DeleteLitigationReminderByID(reminderID);

                        BindGrid();

                        if (deleteSuccess)
                        {
                            cvErrorPage.IsValid = false;
                            cvErrorPage.ErrorMessage = "Reminder Deleted Successfully";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorPage.IsValid = false;
                cvErrorPage.ErrorMessage = "Server Error Occured. Please try again";
            }
        }

        protected void grdReminders_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatus = e.Row.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text != "" && lblStatus.Text != "Pending")
                    {
                        LinkButton lnkEditReminder = e.Row.FindControl("lnkEditReminder") as LinkButton;
                        LinkButton lnkDeleteReminder = e.Row.FindControl("lnkDeleteReminder") as LinkButton;

                        if (lnkEditReminder != null)
                            lnkEditReminder.Visible = false;

                        if (lnkDeleteReminder != null)
                            lnkDeleteReminder.Visible = false;
                    }
                }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string ShowReminderType(string Type)
        {
            try
            {
                if (Type != "")
                {
                    if (Type == "N")
                        return "Notice";
                    else if (Type == "C")
                        return "Case";
                    else if (Type == "T")
                        return "Task";
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdReminders_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int reminderType = 0; //0-All, 1-Notice, 2-Case, 3-Task
                int instanceID = -1;

                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    reminderType = Convert.ToInt32(ddlTypePage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlTitle.SelectedValue) && ddlTitle.SelectedValue != "-1")
                {
                    instanceID = Convert.ToInt32(ddlTitle.SelectedValue);
                }

                var lstReminderDetails = LitigationManagement.GetAssignedReminderList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, reminderType, instanceID);

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstReminderDetails = lstReminderDetails.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Descending;
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstReminderDetails = lstReminderDetails.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdReminders.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdReminders.Columns.IndexOf(field);
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                ViewState["SortExpression"] = e.SortExpression;
                if (lstReminderDetails.Count > 0)
                {
                    grdReminders.DataSource = lstReminderDetails;
                    grdReminders.DataBind();
                    Session["TotalRows"] = lstReminderDetails.Count;
                }
                else
                {
                    grdReminders.DataSource = lstReminderDetails;
                    grdReminders.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdReminders_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
    }
}