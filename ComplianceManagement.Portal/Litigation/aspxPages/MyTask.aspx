﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTask.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.MyTask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <style type="text/css">
        .taskDetail-form {
            /* max-width: 350px; */
            margin: 100px auto 0;
            background: #f9f9f9;
        }

        .label{
            text-align: left;
            background:none !important;
            font-size: 13px;
            color:#8e8e93;
            font-weight: normal;
        }

        /*ul {
            padding-left: 0px;
            text-align: justify;
        }*/
    </style>

    <script type="text/javascript">
        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
    </script>
</head>
<body>
        
     <div class="container">

         <form runat="server" name="login" id="Form2" autocomplete="off">
              <asp:ScriptManager ID="ScriptManager1" runat="server" />
             <div class="login-form">
                 <asp:Panel ID="panelOTP" runat="server" DefaultButton="btnOTP">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                         <ContentTemplate>
                             <div class="col-md-12 login-form-head">
                                 <p class="login-img">                                   
                                     <img src="/Images/avantil-logo.png" />
                                 </p>
                             </div>
                             
                             <div class="login-wrap" id="divOTP" runat="server" visible="false">
                                 <span style="font-family: 'Roboto',san-serif; color: #555555; font-size: 13.5px;">One Time Password has been sent to
                                <br />
                                     Your registered email -
                                <asp:Label ID="email" Font-Bold="true" runat="server"></asp:Label>
                                     <br />
                                     Your registered Phone No -
                                <asp:Label ID="mobileno" Font-Bold="true" runat="server"></asp:Label>
                                     <br />
                                     (Please note that the OTP is valid till
                                <asp:Label ID="Time" runat="server"></asp:Label>
                                     IST)</span>

                                 <div class="clearfix" style="height: 10px"></div>
                                 <div class="clearfix" style="height: 10px"></div>

                                 <h1 style="font-size: 15px;">Please Enter One Time Password (OTP)</h1>

                                 <div class="input-group">
                                     <span class="input-group-addon"></span>
                                     <asp:TextBox ID="txtOTP" CssClass="form-control" runat="server" placeholder="Enter OTP" />
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtOTP" runat="server" Display="None"
                                         ErrorMessage="Only Numbers allowed in OTP" ValidationExpression="\d+" ValidationGroup="OTPValidationGroup"></asp:RegularExpressionValidator>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Required OTP."
                                         ControlToValidate="txtOTP" runat="server" ValidationGroup="OTPValidationGroup" />
                                 </div>

                                 <asp:Label ID="lblmsgotp" runat="server" ForeColor="Red" CssClass="pull-left" Text=""></asp:Label>
                                 <asp:Button ID="btnOTP" CssClass="btn btn-primary btn-lg btn-block" Text="Verify OTP" OnClick="btnOTP_Click" runat="server" ValidationGroup="OTPValidationGroup"></asp:Button>

                                 <div class="clearfix" style="height: 10px"></div>
                             </div>

                             <div class="col-md-12 login-form-head">
                                 <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" ValidationGroup="OTPValidationGroup" />
                                 <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" ValidationGroup="OTPValidationGroup" />
                             </div>

                         </ContentTemplate>
                         <Triggers>
                             <asp:PostBackTrigger ControlID="btnOTP" />
                         </Triggers>
                     </asp:UpdatePanel>
                 </asp:Panel>
             </div>

             <%--</form>
         <form runat="server" class="taskDetail-form" name="taskDetail" autocomplete="off">--%>

            <%-- <div class="taskDetail-form">--%>
                 <asp:Panel ID="panelTaskDetail" runat="server" Visible="false">
                     <div class="col-md-12" style="text-align: center;">
                         <p class="login-img">                             
                             <img src="/Images/avantil-logo.png" />
                         </p>
                     </div>

                     <div class="row" style="background: none;">
                         
                             <div class="row Dashboard-white-widget">
                                 <!--Task Detail Panel Start-->
                                 <div class="col-lg-12 col-md-12">
                                     <div class="panel panel-default">

                                         <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Task Detail">
                                             <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                                 <a>
                                                     <h2>Task Detail</h2>
                                                 </a>
                                                 <div class="panel-actions">
                                                     <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskDetail">
                                                         <i class="fa fa-chevron-up"></i>
                                                     </a>
                                                 </div>
                                             </div>
                                         </div>

                                         <div id="collapseDivTaskDetail" class="panel-collapse collapse in">
                                             <div class="panel-body">
                                                 <div class="container">
                                                     <div class="row">
                                                         <div class="form-group col-md-6">
                                                             <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                             <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Task Title</label>
                                                            <%-- <asp:Label ID="lblTaskTitle" runat="server" style="width:70%;" CssClass="label" maximunsize="70%" autosize="true"></asp:Label>--%>
                                                              <asp:TextBox ID="lblTaskTitle" runat="server" CssClass="form-control" Width="70%" TextMode="MultiLine" 
                                                                  BorderWidth="0" BorderStyle="None" Wrap="true"></asp:TextBox>
                                                         </div>

                                                         <div class="form-group col-md-6">
                                                             <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                             <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Due Date</label>
                                                             <asp:Label ID="lblTaskDueDate" runat="server" Width="70%" CssClass="label"></asp:Label>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-6">
                                                             <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                             <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Assigned By</label>
                                                             <asp:Label ID="lblAssignBy" runat="server" Width="70%" CssClass="label"></asp:Label>
                                                         </div>

                                                         <div class="form-group col-md-6">
                                                             <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                             <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Priority</label>
                                                             <asp:Label ID="lblPriority" runat="server" Width="70%" CssClass="label"></asp:Label>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Task Description</label>
                                                             <%--<asp:Label ID="lblTaskDesc" runat="server" style="width:85.5%;" maximunsize="85.5%" autosize="true" CssClass="label"></asp:Label>--%>
                                                              <asp:TextBox ID="lblTaskDesc" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine" 
                                                                  BorderWidth="0" BorderStyle="None" Wrap="true"></asp:TextBox>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Remark</label>
                                                             <asp:Label ID="lblTaskRemark" runat="server" Width="85.5%" CssClass="label"></asp:Label>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Uploaded Document</label>

                                                             <asp:UpdatePanel runat="server" ID="aa1a" UpdateMode="Conditional">
                                                                 <ContentTemplate>
                                                                     <asp:Label ID="lblTaskDocuments" runat="server" CssClass="label"></asp:Label>
                                                                     <asp:LinkButton ID="lnkBtnTaskDocuments" runat="server" OnClick="btnDownloadTaskDoc_Click"
                                                                         data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Download">
                                                                         <img src='/Images/download_icon_new.png' alt="Download" title="Download Documents" />                                                                          
                                                                     </asp:LinkButton>
                                                                 </ContentTemplate>
                                                                 <Triggers>
                                                                     <asp:PostBackTrigger ControlID="lnkBtnTaskDocuments" />
                                                                 </Triggers>
                                                             </asp:UpdatePanel>

                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <!--Task Detail Panel  End-->
                             </div>
                             <div class="row Dashboard-white-widget">
                                 <!--Task-Response Panel Start-->
                                 <div class="col-lg-12 col-md-12">
                                     <div class="panel panel-default">

                                         <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="Click to View Response Detail">
                                             <div class="panel-heading" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                                 <a>
                                                     <h2>Response</h2>
                                                 </a>
                                                 <div class="panel-actions">
                                                     <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#secondTabAccordion" href="#collapseDivTaskResponse">
                                                         <i class="fa fa-chevron-up"></i>
                                                     </a>
                                                 </div>
                                             </div>
                                         </div>

                                         <div id="collapseDivTaskResponse" class="panel-collapse collapse in">
                                             <div class="panel-body">
                                                 <div class="container">
                                                     <div class="row">
                                                         <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                             ValidationGroup="TaskResponseValidationGroup" />
                                                         <asp:CustomValidator ID="cvTaskResponse" runat="server" EnableClientScript="False"
                                                             ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Description</label>
                                                             <asp:TextBox ID="tbxTaskResDesc" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="rfvTaskResDesc" ErrorMessage="Provide Response Description"
                                                                 ControlToValidate="tbxTaskResDesc" runat="server" ValidationGroup="TaskResponseValidationGroup" Display="None" />
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Remark</label>
                                                             <asp:TextBox ID="tbxTaskResRemark" runat="server" CssClass="form-control" Width="85.5%"></asp:TextBox>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">
                                                             <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                             <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333; font-weight: bold;">Upload Document</label>
                                                             <div style="width: 100%;">
                                                                 <div style="width: 50%; float: left;">
                                                                     <asp:FileUpload ID="fuTaskResponseDocUpload" runat="server" AllowMultiple="true" Style="color: #8e8e93; margin-bottom: 15px;" />
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12" style="text-align: center;">
                                                             <asp:Button Text="Save" runat="server" ID="btnSaveTaskResponse" CssClass="btn btn-primary" OnClick="btnSaveTaskResponse_Click"
                                                                 ValidationGroup="TaskResponseValidationGroup"></asp:Button>
                                                             <asp:Button Text="Clear" runat="server" ID="btnTaskResponseClear" CssClass="btn btn-primary" OnClick="btnClearTaskResponse_Click" />
                                                         </div>
                                                     </div>

                                                     <div class="row">
                                                         <div class="form-group col-md-12">

                                                             <asp:UpdatePanel ID="upTaskResponseDocUpload" runat="server">
                                                                 <ContentTemplate>
                                                                     <asp:GridView runat="server" ID="grdTaskResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                         GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                         PagerSettings-Position="Top" PagerStyle-HorizontalAlign="Right"
                                                                         OnPageIndexChanging="grdTaskResponseLog_OnPageIndexChanging" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                         <Columns>
                                                                             <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                 <ItemTemplate>
                                                                                     <%#Container.DataItemIndex+1 %>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Responded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblResponseDate" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ResponseDate") %>'
                                                                                             Text='<%# Eval("ResponseDate") != DBNull.Value ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" HeaderStyle-Width="30%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                         <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Remark" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                         <asp:Label ID="lblResRemark" runat="server" Text='<%# Eval("Remark") %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%" Visible="false">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                             data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                 <ItemTemplate>
                                                                                     <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                         <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                         </asp:Label>
                                                                                     </div>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                             <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                 <ItemTemplate>
                                                                                     <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                         <ContentTemplate>
                                                                                             <asp:LinkButton
                                                                                                 CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                 ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                             </asp:LinkButton>

                                                                                             <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                 AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                 OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                 ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                             </asp:LinkButton>
                                                                                         </ContentTemplate>
                                                                                         <Triggers>
                                                                                             <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                             <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                         </Triggers>
                                                                                     </asp:UpdatePanel>
                                                                                 </ItemTemplate>
                                                                             </asp:TemplateField>

                                                                         </Columns>
                                                                         <RowStyle CssClass="clsROWgrid" />
                                                                         <HeaderStyle CssClass="clsheadergrid" />
                                                                         <EmptyDataTemplate>
                                                                             No Records Found
                                                                         </EmptyDataTemplate>
                                                                     </asp:GridView>
                                                                 </ContentTemplate>
                                                             </asp:UpdatePanel>

                                                         </div>
                                                     </div>

                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <!--Task-Response Panel End-->
                             </div>
                        
                     </div>

                 </asp:Panel>
            <%-- </div>--%>
         </form>
    </div>
    
</body>
</html>
