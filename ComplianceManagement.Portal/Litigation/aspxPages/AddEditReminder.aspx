﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditReminder.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.AddEditReminder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtRemindOn]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });
        }

        function CloseMe() {
            window.parent.CloseMyReminderPopup();
        }
    </script>
</head>
<body style="background: none !important;">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="upReminder" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="vsReminder" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="reminderValidationGroup" />
                            <asp:CustomValidator ID="cvReminderError" runat="server" EnableClientScript="False"
                                ValidationGroup="reminderValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Select Type</label>
                            <asp:DropDownListChosen runat="server" ID="ddlTypePopup" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select Type" class="form-control" Width="65%" OnSelectedIndexChanged="ddlTypePopup_SelectedIndexChanged">
                                <asp:ListItem Text="Case" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Notice" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Task" Value="3"></asp:ListItem>
                            </asp:DropDownListChosen>
                            <asp:RequiredFieldValidator ID="rfvTypePopup" ErrorMessage="Select Type (i.e. Case/ Notice/ Task)" ControlToValidate="ddlTypePopup"
                                runat="server" ValidationGroup="reminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Select Title</label>
                            <asp:DropDownListChosen runat="server" ID="ddlTitlePopup" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                                DataPlaceHolder="Select" class="form-control" Width="65%">
                            </asp:DropDownListChosen>
                            <asp:RequiredFieldValidator ID="rfvTitlePopup" ErrorMessage="Select Title (i.e. Case/ Notice/ Task Title)" ControlToValidate="ddlTitlePopup"
                                runat="server" ValidationGroup="reminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Reminder
                            </label>
                            <asp:TextBox runat="server" ID="txtReminderTitle" CssClass="form-control" Style="width: 65%;" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="rfvReminderTitle" ErrorMessage="Provide Reminder Title" ControlToValidate="txtReminderTitle"
                                runat="server" ValidationGroup="reminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Description</label>
                            <asp:TextBox runat="server" ID="txtReminderDesc" CssClass="form-control" Style="width: 65%;" autocomplete="off" />
                            <asp:RequiredFieldValidator ID="rfvReminderDesc" ErrorMessage="Provide Reminder Description" ControlToValidate="txtReminderDesc"
                                runat="server" ValidationGroup="reminderValidationGroup" Display="None" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Remark</label>
                            <asp:TextBox runat="server" ID="txtRemark" CssClass="form-control" Style="width: 65%;" autocomplete="off" />
                        </div>

                        <div style="margin-bottom: 7px; margin-top: 10px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Remind On</label>
                            <div class="input-group date" style="width: 65%">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar" style="padding: 3px !important;"></span>
                            </span>
                            <asp:TextBox runat="server" placeholder="Due Date" class="form-control" ID="txtRemindOn" />
                        </div>
                            <%--<asp:TextBox runat="server" ID="txtRemindOn" CssClass="form-control" Style="width: 20%;" autocomplete="off" />--%>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Provide Reminder Date" ControlToValidate="txtRemindOn"
                                runat="server" ValidationGroup="reminderValidationGroup" Display="None" />
                        </div>
                        <div style="text-align: center; margin-bottom: 7px; text-align: center; margin-top: 10px">
                            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" CausesValidation="true"
                                ValidationGroup="reminderValidationGroup" OnClick="btnSave_Click" />
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe()" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
