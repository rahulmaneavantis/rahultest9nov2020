﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class NoticeDetailPageNew : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindAct();
                    BindParty();
                    BindCustomerBranches();
                    BindDepartment();
                    BindUsers();
                    BindLawyer();
                    BindNoticeCaseType();

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var noticeInstanceID = Request.QueryString["AccessID"];
                        if (noticeInstanceID != "")
                        {
                            ViewState["noticeInstanceID"] = noticeInstanceID;

                            if (Convert.ToInt32(noticeInstanceID) == 0)
                            {
                                liNoticeResponse.Visible = false;
                                liNoticeTask.Visible = false;
                                liNoticeStatusPayment.Visible = false;

                                btnAddNotice_Click(sender, e);  //Add Detail 
                            }
                            else
                            {
                                liNoticeResponse.Visible = true;
                                liNoticeTask.Visible = true;
                                liNoticeStatusPayment.Visible = true;

                                btnEditNotice_Click(sender, e); //Edit Detail
                            }

                            lnkNoticeDetail_Click(sender, e);
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                //Show Hide Grid Control - Enable/Disable Form Controls
                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        enableDisableNoticePopUpControls(false);
                    else
                        enableDisableNoticePopUpControls(true);
                }
                else
                    enableDisableNoticePopUpControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindAct()
        {
            var obj = LitigationLaw.GetAllAct();

            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";

            ddlAct.DataSource = obj;
            ddlAct.DataBind();

            ddlAct.Items.Add(new ListItem("Add New", "0"));
        }

        private void BindNoticeCaseType()
        {
            try
            {
                var lstNoticeCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlNoticeCategory.DataTextField = "CaseType";
                ddlNoticeCategory.DataValueField = "ID";

                ddlNoticeCategory.DataSource = lstNoticeCaseType;
                ddlNoticeCategory.DataBind();

                ddlNoticeCategory.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

                // var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);

                List<NameValueHierarchy> branches;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                }
                else
                {
                    branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                }
                if (branches.Count > 0)
                {
                    branch = branches[0];
                }
                tbxBranch.Text = "Select Entity/Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();

            ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }

        public void BindLawyer()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlLawFirm.DataTextField = "Name";
            ddlLawFirm.DataValueField = "ID";

            ddlLawFirm.DataSource = obj;
            ddlLawFirm.DataBind();

            //Task Assignment - For User Filter
            ddlTaskLawyerList.DataTextField = "Name";
            ddlTaskLawyerList.DataValueField = "ID";

            ddlTaskLawyerList.DataSource = obj;
            ddlTaskLawyerList.DataBind();

            ddlTaskLawyerList.Items.Insert(0, new ListItem("Select", "0"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Modal Pop-up
            ddlParty.DataTextField = "Name";
            ddlParty.DataValueField = "ID";

            ddlParty.DataSource = obj;
            ddlParty.DataBind();

            ddlParty.Items.Add(new ListItem("Add New", "0"));
        }

        private void BindPaymentType(DropDownList ddl)
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                ddl.DataValueField = "ID";
                ddl.DataTextField = "TypeName";

                ddl.DataSource = PaymentMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstUsers = LitigationUserManagement.GetLitigationUsers(customerID, 0);

                ddlOwner.DataValueField = "ID";
                ddlOwner.DataTextField = "Name";
                ddlOwner.DataSource = lstUsers;
                ddlOwner.DataBind();

                ddlPerformer.DataValueField = "ID";
                ddlPerformer.DataTextField = "Name";
                ddlPerformer.DataSource = lstUsers;
                ddlPerformer.DataBind();

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = lstUsers;
                ddlReviewer.DataBind();

                ddlTaskUser.DataValueField = "ID";
                ddlTaskUser.DataTextField = "Name";
                ddlTaskUser.DataSource = lstUsers;
                ddlTaskUser.DataBind();

                ddlTaskUser.Items.Add(new ListItem("Add New", "0"));

                HttpContext.Current.Cache.Insert("userRecords", lstUsers);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindNoticeRelatedDocuments(int noticeInstanceID)
        {
            try
            {
                List<Sp_Litigation_CaseDocument_Result> lstNoticeDocs = new List<Sp_Litigation_CaseDocument_Result>();

                lstNoticeDocs = NoticeManagement.GetNoticeDocumentMapping(noticeInstanceID,"N", AuthenticationHelper.CustomerID);
                if (lstNoticeDocs.Count > 0)
                {
                    lstNoticeDocs = (from g in lstNoticeDocs
                                     group g by new
                               {
                                   g.ID,
                                   g.DocType,
                                   g.FileName,
                                   g.Version,
                                   g.CreatedByText,
                                   g.CreatedOn
                               } into GCS
                               select new Sp_Litigation_CaseDocument_Result()
                               {
                                   ID = GCS.Key.ID,
                                   DocType = GCS.Key.DocType,
                                   FileName = GCS.Key.FileName,
                                   Version = GCS.Key.Version,
                                   CreatedByText = GCS.Key.CreatedByText,
                                   CreatedOn = GCS.Key.CreatedOn,
                               }).ToList();
                }
                if (lstNoticeDocs != null && lstNoticeDocs.Count > 0)
                {
                    grdNoticeDocuments.DataSource = lstNoticeDocs;
                    grdNoticeDocuments.DataBind();
                }
                else
                {
                    grdNoticeDocuments.DataSource = null;
                    grdNoticeDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindNoticeResponses(int noticeInstanceID)
        {
            try
            {
                List<tbl_LegalNoticeResponse> lstNoticeResponses = new List<tbl_LegalNoticeResponse>();

                lstNoticeResponses = NoticeManagement.GetNoticeResponseDetails(noticeInstanceID);

                if (lstNoticeResponses != null && lstNoticeResponses.Count > 0)
                {
                    grdResponseLog.DataSource = lstNoticeResponses;
                    grdResponseLog.DataBind();
                }
                else
                {
                    grdResponseLog.DataSource = null;
                    grdResponseLog.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTasks(int noticeInstanceID)
        {
            try
            {
                var lstNoticeTasks = LitigationTaskManagement.GetTaskDetails(noticeInstanceID, "N");

                grdTaskActivity.DataSource = lstNoticeTasks;
                grdTaskActivity.DataBind();

                lstNoticeTasks.Clear();
                lstNoticeTasks = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlTaskLawyerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                List<object> lstUsers = new List<object>();

                if (!string.IsNullOrEmpty(ddlTaskLawyerList.SelectedValue) && ddlTaskLawyerList.SelectedValue != "0")
                    lstUsers = LitigationUserManagement.GetLitigationUsersByLawyerID(customerID, Convert.ToInt32(ddlTaskLawyerList.SelectedValue));
                else
                    lstUsers = LitigationUserManagement.GetLitigationUsers(customerID, 0);

                ddlTaskUser.Items.Clear();
                ddlTaskUser.DataValueField = "ID";
                ddlTaskUser.DataTextField = "Name";
                ddlTaskUser.DataSource = lstUsers;
                ddlTaskUser.DataBind();

                ddlTaskUser.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvNoticePopUpTask.IsValid = false;
                                cvNoticePopUpTask.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindNoticePayments(int noticeInstanceID)
        {
            try
            {
                List<tbl_NoticeCasePayment> lstNoticePayments = new List<tbl_NoticeCasePayment>();

                lstNoticePayments = NoticeManagement.GetNoticeCasePaymentDetails(noticeInstanceID, "N");

                if (lstNoticePayments != null && lstNoticePayments.Count > 0)
                {
                    grdNoticePayment.DataSource = lstNoticePayments;
                    grdNoticePayment.DataBind();
                }
                else
                {
                    tbl_NoticeCasePayment obj = new tbl_NoticeCasePayment(); //initialize empty class that may contain properties
                    lstNoticePayments.Add(obj); //Add empty object to list

                    grdNoticePayment.DataSource = lstNoticePayments; /*Assign datasource to create one row with default values for the class you have*/
                    grdNoticePayment.DataBind(); //Bind that empty source                    

                    //To Hide row
                    grdNoticePayment.Rows[0].Visible = false;
                    grdNoticePayment.Rows[0].Controls.Clear();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClearNoticeControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearNoticeControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditNoticeControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableNoticeControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnNoticeUpload_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Processid"])) && !string.IsNullOrEmpty(Convert.ToString(ViewState["financialyear"])) && !string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])) && !string.IsNullOrEmpty(Convert.ToString(ViewState["customerbranchid"])))
            //    //{
            //    var aaa = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, ddlFinancialYear.SelectedItem.Text, ddlPeriod.SelectedValue, Convert.ToInt32(ddlFilterLocation.SelectedValue), Convert.ToInt32(ViewState["ATBDID"]), Convert.ToInt32(ViewState["VerticalID"]));

            //    Tran_AnnxetureUpload AnnxetureUpload = new Tran_AnnxetureUpload()
            //    {
            //        ProcessId = Convert.ToInt32(aaa.ProcessId),
            //        ATBDId = Convert.ToInt32(ViewState["ATBDID"]),
            //        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue),
            //        FinancialYear = ddlFinancialYear.SelectedItem.Text,
            //        Period = ddlPeriod.SelectedValue,
            //        VerticalID = Convert.ToInt32(ViewState["VerticalID"]),
            //        IsDeleted = false,
            //    };

            //    if (AnnexureFileUpload.HasFile)
            //    {
            //        HttpFileCollection fileCollection1 = Request.Files;
            //        if (fileCollection1.Count > 0)
            //        {
            //            List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();
            //            int customerID = -1;
            //            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            //            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            //            string directoryPath1 = "";
            //            if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1 && Convert.ToInt32(aaa.ProcessId) != -1 && !string.IsNullOrEmpty(Convert.ToString(ddlFinancialYear.SelectedItem.Text)) && !string.IsNullOrEmpty(Convert.ToString(ddlPeriod.SelectedValue)))
            //            {
            //                directoryPath1 = Server.MapPath("~/AuditClosureAnnuxureDocument/" + customerID + "/"
            //                    + Convert.ToInt32(ddlFilterLocation.SelectedValue) + "/" + Convert.ToInt32(ViewState["VerticalID"]) + "/" + Convert.ToInt32(aaa.ProcessId) + "/"
            //                    + Convert.ToString(ddlFinancialYear.SelectedItem.Text) + "/" + Convert.ToString(ddlPeriod.SelectedValue) + "/"
            //                    + Convert.ToInt32(ViewState["ATBDId"]) + "/1.0");
            //            }
            //            DocumentManagement.CreateDirectory(directoryPath1);
            //            for (int i = 0; i < fileCollection1.Count; i++)
            //            {
            //                HttpPostedFile uploadfile1 = fileCollection1[i];
            //                string[] keys1 = fileCollection1.Keys[i].Split('$');
            //                String fileName1 = "";
            //                if (keys1[keys1.Count() - 1].Equals("AnnexureFileUpload"))
            //                {
            //                    fileName1 = uploadfile1.FileName;
            //                }
            //                Guid fileKey1 = Guid.NewGuid();
            //                string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadfile1.FileName));
            //                Stream fs = uploadfile1.InputStream;
            //                BinaryReader br = new BinaryReader(fs);
            //                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            //                Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
            //                if (uploadfile1.ContentLength > 0)
            //                {
            //                    AnnxetureUpload.FileName = fileName1;
            //                    AnnxetureUpload.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
            //                    AnnxetureUpload.FileKey = fileKey1.ToString();
            //                    AnnxetureUpload.Version = "1.0";
            //                    AnnxetureUpload.VersionDate = DateTime.UtcNow;
            //                    AnnxetureUpload.CreatedDate = DateTime.Today.Date;

            //                    AnnxetureUpload.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            //                    DocumentManagement.SaveDocFiles(Filelist1);
            //                }
            //            }
            //        }
            //        bool Success1 = false;
            //        if (!RiskCategoryManagement.Tran_AnnxetureUploadResultExists(AnnxetureUpload))
            //        {
            //            Success1 = RiskCategoryManagement.CreateTran_AnnxetureUploadResult(AnnxetureUpload);
            //            if (Success1 == true)
            //            {
            //                int chkATBDID = Convert.ToInt32(ViewState["ATBDID"]);
            //                BindDocumentAnnxeture(Convert.ToString(ddlPeriod.SelectedValue), Convert.ToString(ddlFinancialYear.SelectedItem.Text), chkATBDID, Convert.ToInt32(aaa.ProcessId), Convert.ToInt32(ddlFilterLocation.SelectedValue), Convert.ToInt32(ViewState["VerticalID"]));
            //                cvDuplicateEntry.IsValid = false;
            //                cvDuplicateEntry.ErrorMessage = "Uploaded File Successfully";
            //            }
            //            else
            //            {
            //                cvDuplicateEntry.IsValid = false;
            //                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //            }
            //        }
            //        else
            //        {
            //            cvDuplicateEntry.IsValid = false;
            //            cvDuplicateEntry.ErrorMessage = "Same Name Allready Exists..";
            //        }
            //    }
            //    //}
            //    //else
            //    //{
            //    //    cvDuplicateEntry.IsValid = false;
            //    //    cvDuplicateEntry.ErrorMessage = "Please Select Audit Closure Details..";
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void btnAddNotice_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                //BindAct();
                //BindParty();
                //BindCustomerBranches();
                //BindDepartment();
                //BindUsers();
                //BindLawyer();

                enableDisableNoticeControls(true);

                enableDisableNoticePopUpControls(true);

                clearNoticeControls();

                btnSave.Enabled = true;

                btnSave.Visible = true;
                btnSave.Text = "Save";
                btnClearNoticeDetail.Visible = true;
                btnEditNoticeDetail.Visible = false;

                grdNoticeDocuments.DataSource = null;
                grdNoticeDocuments.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditNotice_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    if (noticeInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;

                        var noticeRecord = NoticeManagement.GetNoticeByID(noticeInstanceID);

                        if (noticeRecord != null)
                        {
                            enableDisableNoticeControls(false);

                            btnSave.Visible = true;
                            btnSave.Text = "Update";
                            btnClearNoticeDetail.Visible = false;
                            btnEditNoticeDetail.Visible = true;

                            if (noticeRecord.NoticeType != null)
                            {
                                if (noticeRecord.NoticeType.ToString() == "I")
                                    rbNoticeInOutType.SelectedValue = "I";
                                else if (noticeRecord.NoticeType.ToString() == "O")
                                    rbNoticeInOutType.SelectedValue = "O";
                            }

                            tbxRefNo.Text = noticeRecord.RefNo;

                            if (noticeRecord.NoticeDate != null)
                                txtNoticeDate.Text = Convert.ToDateTime(noticeRecord.NoticeDate).ToString("dd-MM-yyyy");

                            //if (noticeRecord.PartyID != null)
                            //{
                            //    ddlParty.ClearSelection();

                            //    if (ddlParty.Items.FindByValue(noticeRecord.PartyID.ToString()) != null)
                            //        ddlParty.SelectedValue = noticeRecord.PartyID.ToString();
                            //}

                            //if (noticeRecord.ActID != null)
                            //{
                            //    ddlAct.ClearSelection();

                            //    if (ddlAct.Items.FindByValue(noticeRecord.ActID.ToString()) != null)
                            //        ddlAct.SelectedValue = noticeRecord.ActID.ToString();
                            //}

                            tbxSection.Text = noticeRecord.Section;

                            if (noticeRecord.NoticeCategoryID != null)
                            {
                                ddlNoticeCategory.ClearSelection();

                                if (ddlNoticeCategory.Items.FindByValue(noticeRecord.NoticeCategoryID.ToString()) != null)
                                    ddlNoticeCategory.SelectedValue = noticeRecord.NoticeCategoryID.ToString();
                            }

                            tbxTitle.Text = noticeRecord.NoticeTitle;
                            tbxDescription.Text = noticeRecord.NoticeDetailDesc;

                            if (noticeRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == noticeRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == noticeRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(noticeRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = noticeRecord.DepartmentID.ToString();

                            if (noticeRecord.OwnerID != null)
                            {
                                ddlOwner.ClearSelection();

                                if (ddlOwner.Items.FindByValue(noticeRecord.OwnerID.ToString()) != null)
                                    ddlOwner.SelectedValue = noticeRecord.OwnerID.ToString();
                            }

                            if (noticeRecord.NoticeRiskID != null)
                            {
                                ddlNoticeRisk.ClearSelection();

                                if (ddlNoticeRisk.Items.FindByValue(noticeRecord.NoticeRiskID.ToString()) != null)
                                    ddlNoticeRisk.SelectedValue = noticeRecord.NoticeRiskID.ToString();
                            }

                            tbxClaimedAmt.Text = noticeRecord.ClaimAmt.ToString();
                            tbxProbableAmt.Text = noticeRecord.ProbableAmt.ToString();

                            //Get Lawyer Mapping
                            var lstNoticeLawyer = NoticeManagement.GetNoticeLawyerMapping(noticeInstanceID);

                            if (lstNoticeLawyer != null)
                            {
                                ddlLawFirm.ClearSelection();
                                ddlLawFirm.SelectedValue = Convert.ToString(lstNoticeLawyer.LawyerID);
                            }

                            //Get Notice Assignment
                            var lstNoticeAssignment = NoticeManagement.GetNoticeAssignment(noticeInstanceID);

                            if (lstNoticeAssignment.Count > 0)
                            {
                                ddlPerformer.ClearSelection();
                                ddlReviewer.ClearSelection();

                                foreach (var eachAssignmentRecord in lstNoticeAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 4)
                                    {
                                        if (ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //Notice Status 
                            var StatusDetails = NoticeManagement.GetNoticeStatusDetail(noticeInstanceID);

                            if (StatusDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.TxnStatusID)))
                                {
                                    ddlNoticeStatus.ClearSelection();

                                    if (ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()) != null)
                                        ddlNoticeStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()).Selected = true;

                                    ViewState["noticeStatus"] = Convert.ToInt32(StatusDetails.TxnStatusID);

                                    //if (Convert.ToInt32(StatusDetails.TxnStatusID) == 3)
                                    //    enableDisableNoticePopUpControls(false);
                                    //else
                                    //    enableDisableNoticePopUpControls(true);
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.CloseDate)))
                                {
                                    tbxNoticeCloseDate.Text = Convert.ToDateTime(StatusDetails.CloseDate).ToString("dd-MM-yyyy");
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.ClosureRemark)))
                                {
                                    tbxCloseRemark.Text = StatusDetails.ClosureRemark;
                                }
                            }
                            //Notice Status Log--End

                            //Bind Notice Related Documents
                            BindNoticeRelatedDocuments(noticeInstanceID);

                            //Bind Notice Response Details
                            BindNoticeResponses(noticeInstanceID);

                            //Bind Notice Action Details
                            BindTasks(noticeInstanceID);

                            //Bind Notice Payment Details
                            BindNoticePayments(noticeInstanceID);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveNotice_Click(object sender, EventArgs e)
        {
            try
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;

                #region Data Validation

                if (rbNoticeInOutType.SelectedValue != "")
                {
                    if (tbxRefNo.Text != "")
                    {
                        if (txtNoticeDate.Text != "")
                        {
                            if (ddlParty.SelectedValue != "" && ddlParty.SelectedValue != "-1")
                            {
                                if (ddlAct.SelectedValue != "" && ddlParty.SelectedValue != "-1")
                                {
                                    if (tbxTitle.Text != "")
                                    {
                                        if (tbxDescription.Text != "")
                                        {
                                            if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                                            {
                                                if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                                                {
                                                    if (ddlOwner.SelectedValue != "" && ddlOwner.SelectedValue != "-1")
                                                    {
                                                        if (ddlPerformer.SelectedValue != "" && ddlPerformer.SelectedValue != "-1")
                                                        {
                                                            int selectedLawyerCount = 0;

                                                            foreach (ListItem eachlawyer in ddlLawFirm.Items)
                                                            {
                                                                if (eachlawyer.Selected)
                                                                    selectedLawyerCount++;
                                                            }


                                                            //for (int i = 0; i < ddlMappingLawyer.Items.Count; i++)
                                                            //{
                                                            //    if (ddlMappingLawyer.Items[i].Selected)
                                                            //    {
                                                            //        selectedLawyerCount++;
                                                            //    }
                                                            //}

                                                            if (selectedLawyerCount > 0)
                                                            {
                                                                formValidateSuccess = true;
                                                            }
                                                            else
                                                            {
                                                                cvNoticePopUp.IsValid = false;
                                                                cvNoticePopUp.ErrorMessage = "Select Lawyer.";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvNoticePopUp.IsValid = false;
                                                            cvNoticePopUp.ErrorMessage = "Select Performer to Assign.";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        cvNoticePopUp.IsValid = false;
                                                        cvNoticePopUp.ErrorMessage = "Select Owner.";
                                                    }
                                                }
                                                else
                                                {
                                                    cvNoticePopUp.IsValid = false;
                                                    cvNoticePopUp.ErrorMessage = "Select Department.";
                                                }
                                            }
                                            else
                                            {
                                                cvNoticePopUp.IsValid = false;
                                                cvNoticePopUp.ErrorMessage = "Select Entity/Location.";
                                            }
                                        }
                                        else
                                        {
                                            cvNoticePopUp.IsValid = false;
                                            cvNoticePopUp.ErrorMessage = "Please Provide Notice Description.";
                                        }
                                    }
                                    else
                                    {
                                        cvNoticePopUp.IsValid = false;
                                        cvNoticePopUp.ErrorMessage = "Please Provide Notice Title.";
                                    }
                                }
                                else
                                {
                                    cvNoticePopUp.IsValid = false;
                                    cvNoticePopUp.ErrorMessage = "Select Act.";
                                }
                            }
                            else
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Select Party.";
                            }
                        }
                        else
                        {
                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Provide Notice Date.";
                        }
                    }
                    else
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Provide Reference No.";
                    }
                }
                else
                {
                    cvNoticePopUp.IsValid = false;
                    cvNoticePopUp.ErrorMessage = "Select Notice Type.";
                }

                #endregion

                #region Save/Edit Code

                if (formValidateSuccess)
                {
                    long newNoticeID = 0;

                    List<int> lstNoticeLawyer = new List<int>();

                    tbl_LegalNoticeInstance newNotice = new tbl_LegalNoticeInstance()
                    {
                        IsDeleted = false,
                        NoticeType = rbNoticeInOutType.SelectedValue,
                        RefNo = tbxRefNo.Text.Trim(),
                        NoticeDate = DateTimeExtensions.GetDate(txtNoticeDate.Text),
                        Section = tbxSection.Text.Trim(),
                        NoticeCategoryID = Convert.ToInt32(ddlNoticeCategory.SelectedValue),
                        NoticeTitle = tbxTitle.Text.Trim(),
                        NoticeDetailDesc = tbxDescription.Text.Trim(),
                        CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                        DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                        OwnerID = Convert.ToInt32(ddlOwner.SelectedValue),
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID,
                    };

                    if (tbxClaimedAmt.Text != "")
                        newNotice.ClaimAmt = Convert.ToDecimal(tbxClaimedAmt.Text.Trim());

                    if (tbxProbableAmt.Text != "")
                        newNotice.ProbableAmt = Convert.ToDecimal(tbxProbableAmt.Text.Trim());

                    if (ddlNoticeRisk.SelectedValue != "" && ddlNoticeRisk.SelectedValue != "-1")
                        newNotice.NoticeRiskID = Convert.ToInt32(ddlNoticeRisk.SelectedValue);

                    if (ddlPerformer.SelectedValue != "" && ddlPerformer.SelectedValue != "-1")
                        newNotice.AssignmentType = 1;

                    if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                        newNotice.AssignmentType = 2;

                    if ((int) ViewState["Mode"] == 0)
                    {
                        if (!NoticeManagement.ExistsNotice(newNotice.NoticeTitle, 0))
                        {
                            newNoticeID = NoticeManagement.CreateNotice(newNotice);

                            if (newNoticeID > 0)
                                saveSuccess = true;
                        }
                        else
                        {
                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Notice with Same Title already Exists.";
                            return;
                        }

                        if (saveSuccess)
                        {
                            //Notice Status Transaction
                            #region Status Transaction
                            tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                            {
                                NoticeInstanceID = newNoticeID,
                                StatusID = 1,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                                saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);

                            #endregion

                            //Lawyer Mapping
                            #region Lawyer Mapping

                            tbl_LegalNoticeLawyerMapping objNoticeLawyerMapping = new tbl_LegalNoticeLawyerMapping()
                            {
                                NoticeInstanceID = newNoticeID,
                                IsActive = true,
                                LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            saveSuccess = NoticeManagement.CreateNoticeLawyerMapping(objNoticeLawyerMapping);

                            #endregion

                            //User Assignment
                            #region User Assignment
                            tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                            {
                                AssignmentType = newNotice.AssignmentType,
                                NoticeInstanceID = newNoticeID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,

                                UserID = Convert.ToInt32(ddlPerformer.SelectedValue),
                                RoleID = 3,
                            };

                            if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);

                            if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                            {
                                tbl_LegalNoticeAssignment newReviewerAssignment = new tbl_LegalNoticeAssignment()
                                {
                                    AssignmentType = newNotice.AssignmentType,
                                    NoticeInstanceID = newNoticeID,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,

                                    UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                    RoleID = 4,
                                };

                                if (!NoticeManagement.ExistNoticeAssignment(newReviewerAssignment))
                                    saveSuccess = NoticeManagement.CreateNoticeAssignment(newReviewerAssignment);
                            }

                            #endregion 

                            //Upload Document
                            #region Upload Document

                            if (NoticeFileUpload.HasFiles)
                            {
                                tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = Convert.ToInt32(newNoticeID),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocType = "N"
                                };

                                HttpFileCollection fileCollection = Request.Files;


                                if (fileCollection.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                    int customerID = -1;
                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                    string directoryPath = "";
                                    String fileName = "";

                                    if (newNoticeID > 0)
                                    {
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("NoticeFileUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                objNoticeDoc.FileName = fileName;

                                                //Get Document Version
                                                var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                                caseDocVersion++;
                                                objNoticeDoc.Version = caseDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newNoticeID) + "/NoticeDocument/" + objNoticeDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objNoticeDoc.FileKey = fileKey1.ToString();
                                                objNoticeDoc.VersionDate = DateTime.Now;
                                                objNoticeDoc.CreatedOn = DateTime.Now;
                                                objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                                fileList.Clear();
                                            }

                                        }//End For Each                                    
                                    }
                                }
                            }

                            #endregion
                        }

                        if (saveSuccess)
                        {
                            #region Mail
                            try
                            {
                                if (newNotice != null)
                                {
                                    User User = UserManagement.GetByID(Convert.ToInt32(ddlPerformer.SelectedValue));

                                    if (User != null)
                                    {
                                        if (User.Email != null && User.Email != "")
                                        {
                                            string NoticePriority = string.Empty;

                                            if (newNotice.NoticeRiskID != null)
                                            {
                                                if (newNotice.NoticeRiskID == 1)
                                                    NoticePriority = "High";
                                                else if (newNotice.NoticeRiskID == 2)
                                                    NoticePriority = "Medium";
                                                else if (newNotice.NoticeRiskID == 3)
                                                    NoticePriority = "Low";
                                            }
                                            string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                            string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_NoticeAssignment
                                                                    .Replace("@User", username)
                                                                    .Replace("@NoticeTitle", newNotice.NoticeTitle)
                                                                    .Replace("@NoticeDetailDesc", newNotice.NoticeDetailDesc)
                                                                    .Replace("@Priority", NoticePriority)
                                                                    .Replace("@AccessURL", accessURL) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                                    .Replace("@From", "Team Admin")
                                                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Litigation Notification-Notice Assigned", message);
                                            var UseID = CaseManagement.GetUserRole(Convert.ToInt32(ddlPerformer.SelectedValue));

                                            tbl_LitigationReminderLog objRemind = new tbl_LitigationReminderLog()
                                            {
                                                UserID = Convert.ToInt32(ddlPerformer.SelectedValue),
                                                Role = Convert.ToInt32(UseID),
                                                TriggerType = "Litigation Notice assigned Reminder",
                                                TriggerDate = DateTime.Now
                                            };

                                            CaseManagement.SaveLitigationReminderMail(objRemind);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            #endregion

                            cvNoticePopUp.IsValid = false;
                            cvNoticePopUp.ErrorMessage = "Notice Created Successfully.";

                            liNoticeResponse.Visible = true;
                            liNoticeTask.Visible = true;
                            liNoticeStatusPayment.Visible = true;

                            ViewState["Mode"] = 1;
                            ViewState["noticeInstanceID"] = newNoticeID;

                            enableDisableNoticeControls(false);
                        }
                    }//Add Code End                                       

                    else if ((int) ViewState["Mode"] == 1)
                    {
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            newNoticeID = Convert.ToInt32(ViewState["noticeInstanceID"]); //Selected Notice ID
                            newNotice.ID = newNoticeID;

                            saveSuccess = NoticeManagement.UpdateNotice(newNotice);

                            if (saveSuccess)
                            {
                                //Notice Status Transaction
                                #region Status Transaction
                                tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                                {
                                    NoticeInstanceID = newNoticeID,
                                    StatusID = 1,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    UserID = AuthenticationHelper.UserID,
                                    RoleID = 3,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                                {
                                    saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusRecord);
                                    saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);
                                }

                                #endregion

                                //Lawyer Mapping
                                #region Lawyer Mapping

                                List<tbl_LegalNoticeLawyerMapping> lstObjNoticeMapping = new List<tbl_LegalNoticeLawyerMapping>();

                                saveSuccess = NoticeManagement.DeActiveExistingNoticeLawyerMapping(newNoticeID);

                                if (saveSuccess)
                                {
                                    tbl_LegalNoticeLawyerMapping objNoticeLawyerMapping = new tbl_LegalNoticeLawyerMapping()
                                    {
                                        NoticeInstanceID = newNoticeID,
                                        IsActive = true,
                                        LawyerID = Convert.ToInt32(ddlLawFirm.SelectedValue),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };

                                    saveSuccess = NoticeManagement.UpdateNoticeLawyerMapping(objNoticeLawyerMapping);
                                }

                                #endregion

                                //User Assignment ---Able to Edit User Assignment Only After Re-Assign
                                #region User Assignment                               

                                //tbl_LegalNoticeAssignment newAssignment = new tbl_LegalNoticeAssignment()
                                //{
                                //    AssignmentType = newNotice.AssignmentType,
                                //    NoticeInstanceID = newNoticeID,
                                //    IsActive = true,
                                //    CreatedBy = AuthenticationHelper.UserID,
                                //    UpdatedBy = AuthenticationHelper.UserID,

                                //    UserID = Convert.ToInt32(ddlPerformer.SelectedValue),
                                //    RoleID = 3,
                                //};

                                //if (!NoticeManagement.ExistNoticeAssignment(newAssignment))
                                //    saveSuccess = NoticeManagement.CreateNoticeAssignment(newAssignment);

                                //if (ddlReviewer.SelectedValue != "" && ddlReviewer.SelectedValue != "-1")
                                //{
                                //    tbl_LegalNoticeAssignment newReviewerAssignment = new tbl_LegalNoticeAssignment()
                                //    {
                                //        AssignmentType = newNotice.AssignmentType,
                                //        NoticeInstanceID = newNoticeID,
                                //        IsActive = true,
                                //        CreatedBy = AuthenticationHelper.UserID,
                                //        UpdatedBy = AuthenticationHelper.UserID,

                                //        UserID = Convert.ToInt32(ddlReviewer.SelectedValue),
                                //        RoleID = 4,
                                //    };

                                //    if (!NoticeManagement.ExistNoticeAssignment(newReviewerAssignment))
                                //        saveSuccess = NoticeManagement.CreateNoticeAssignment(newReviewerAssignment);
                                //}

                                #endregion

                                //Upload Document
                                #region Upload Document

                                if (NoticeFileUpload.HasFiles)
                                {
                                    tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                    {
                                        NoticeCaseInstanceID = Convert.ToInt32(newNoticeID),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        IsDeleted = false,
                                        DocType = "N",
                                    };

                                    HttpFileCollection fileCollection = Request.Files;

                                    if (fileCollection.Count > 0)
                                    {
                                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                        int customerID = -1;
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string directoryPath = "";
                                        String fileName = "";

                                        if (newNoticeID > 0)
                                        {
                                            for (int i = 0; i < fileCollection.Count; i++)
                                            {
                                                HttpPostedFile uploadedFile = fileCollection[i];

                                                if (uploadedFile.ContentLength > 0)
                                                {
                                                    string[] keys1 = fileCollection.Keys[i].Split('$');

                                                    if (keys1[keys1.Count() - 1].Equals("NoticeFileUpload"))
                                                    {
                                                        fileName = uploadedFile.FileName;
                                                    }

                                                    objNoticeDoc.FileName = fileName;

                                                    //Get Document Version
                                                    var noticeDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                                    noticeDocVersion++;
                                                    objNoticeDoc.Version = noticeDocVersion + ".0";

                                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newNoticeID) + "/NoticeDocument/" + objNoticeDoc.Version);

                                                    if (!Directory.Exists(directoryPath))
                                                        Directory.CreateDirectory(directoryPath);

                                                    Guid fileKey1 = Guid.NewGuid();
                                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                    Stream fs = uploadedFile.InputStream;
                                                    BinaryReader br = new BinaryReader(fs);
                                                    Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                                                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                    objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                    objNoticeDoc.FileKey = fileKey1.ToString();
                                                    objNoticeDoc.VersionDate = DateTime.Now;
                                                    objNoticeDoc.CreatedOn = DateTime.Now;
                                                    objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                    DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                    saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                                    fileList.Clear();
                                                }

                                            }//End For Each                                    
                                        }
                                    }

                                    if (saveSuccess)
                                    {
                                        BindNoticeRelatedDocuments(Convert.ToInt32(newNoticeID));
                                    }
                                }

                                #endregion
                            }

                            if (saveSuccess)
                            {
                                cvNoticePopUp.IsValid = false;
                                cvNoticePopUp.ErrorMessage = "Notice Details Updated Successfully.";

                                enableDisableNoticeControls(false);
                            }
                        }
                    }//Edit Code End
                }

                #endregion

                #region Previous Sushant Code
                //if ((int)ViewState["Mode"] == 1)
                //{
                //    objlawyer.ID = Convert.ToInt32(ViewState["LawyerID"]);//Need to change DeptID
                //}

                //if ((int)ViewState["Mode"] == 0)
                //{
                //    LitigationManagement.CreateLitigationAssignment(objlawyer);
                //    LawyerTypeList.Clear();
                //    for (int i = 0; i < ddlMappingLawyer.Items.Count; i++)
                //    {
                //        if (ddlMappingLawyer.Items[i].Selected)
                //        {
                //            LawyerTypeList.Add(Convert.ToInt32(ddlMappingLawyer.Items[i].Value));
                //        }
                //    }
                //    LitiAssignment_Mapping objAssignment = new LitiAssignment_Mapping();
                //    var LawyerID = LitigationManagement.GetLitiAssignmentID(objlawyer);
                //    foreach (var aItem in LawyerTypeList)
                //    {
                //        objAssignment.LawyerID = aItem;
                //        objAssignment.Liti_AssignmentID = LawyerID.ID;
                //        objAssignment.IsActive = true;
                //        LitigationManagement.CreateLitiAssignmentMapping(objAssignment);
                //    }
                //    cvNoticePopUp.IsValid = false;
                //    cvNoticePopUp.ErrorMessage = "Litigation Assingment Saved Successfully";
                //    tbxTitle.Text = string.Empty;
                //    tbxDescription.Text = string.Empty;

                //    ddlMappingLawyer.Items.Clear();                    
                //    //ddlCity.SelectedIndex = -1;
                //    ddlDepartment.SelectedIndex = -1;                    
                //}

                //else if ((int)ViewState["Mode"] == 1)
                //{
                //    LitigationManagement.UpdateLitigationAssignment(objlawyer);
                //    LawyerTypeList.Clear();
                //    for (int i = 0; i < ddlMappingLawyer.Items.Count; i++)
                //    {
                //        if (ddlMappingLawyer.Items[i].Selected)
                //        {
                //            LawyerTypeList.Add(Convert.ToInt32(ddlMappingLawyer.Items[i].Value));
                //        }
                //    }
                //    LitiAssignment_Mapping objMap = new LitiAssignment_Mapping();
                //    objMap.Liti_AssignmentID = objlawyer.ID;
                //    LitigationManagement.UpdateLitAssignmentMappingAllFalse(objMap);
                //    foreach (var aItem in LawyerTypeList)
                //    {
                //        objMap.Liti_AssignmentID = aItem;
                //        objMap.LawyerID = objlawyer.ID;
                //        objMap.IsActive = true;
                //        if (LitigationManagement.CheckLitiAssignmentExist(objMap))
                //        {
                //            LitigationManagement.UpdateLitigationMappingData(objMap);
                //        }
                //        else
                //        {
                //            LitigationManagement.CreateLitiAssignmentMapping(objMap);
                //        }
                //    }
                //    cvNoticePopUp.IsValid = false;
                //    cvNoticePopUp.ErrorMessage = "Litigation Assingment Updated Successfully";
                //}

                #endregion

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnTaskSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;

                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    if (tbxTaskTitle.Text != "")
                    {
                        if (tbxTaskDueDate.Text != "")
                        {
                            if (!String.IsNullOrEmpty(ddlTaskUser.SelectedValue))
                            {
                                if (ddlTaskUser.SelectedValue != "0")
                                {
                                    if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                                    {
                                        if (tbxTaskDesc.Text != "")
                                        {
                                            validateData = true;
                                        }
                                        else
                                        {
                                            cvNoticePopUpTask.IsValid = false;
                                            cvNoticePopUpTask.ErrorMessage = "Provide Task Description.";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvNoticePopUpTask.IsValid = false;
                                        cvNoticePopUpTask.ErrorMessage = "Select Task Priority.";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvNoticePopUpTask.IsValid = false;
                                    cvNoticePopUpTask.ErrorMessage = "Select User to Assign Task.";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePopUpTask.IsValid = false;
                                cvNoticePopUpTask.ErrorMessage = "Select User to Assign Task.";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePopUpTask.IsValid = false;
                            cvNoticePopUpTask.ErrorMessage = "Provide Due Date.";
                            return;
                        }
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Provide Task Title.";
                        return;
                    }

                    tbl_TaskScheduleOn newRecord = new tbl_TaskScheduleOn();

                    if (validateData)
                    {
                        newRecord.IsActive = true;
                        newRecord.TaskType = "N";
                        newRecord.NoticeCaseInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        newRecord.ScheduleOnDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);
                        newRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                        newRecord.TaskDesc = tbxTaskDesc.Text.Trim();

                        newRecord.StatusID = 1;
                        newRecord.CreatedBy = AuthenticationHelper.UserID;
                        newRecord.CreatedByText = AuthenticationHelper.User;
                        newRecord.LinkCreatedOn = DateTime.Now;
                        newRecord.URLExpired = false;

                        if (!String.IsNullOrEmpty(ddlTaskUser.SelectedValue))
                            newRecord.AssignTo = Convert.ToInt32(ddlTaskUser.SelectedValue);

                        if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                            newRecord.PriorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);

                        if (tbxTaskRemark.Text != "")
                            newRecord.Remark = tbxTaskRemark.Text.Trim();

                        if (!LitigationTaskManagement.ExistNoticeCaseTaskTitle(tbxTaskTitle.Text.Trim(), "N", (long)newRecord.NoticeCaseInstanceID,customerID))
                            saveSuccess = LitigationTaskManagement.CreateTask(newRecord);
                        else
                        {
                            saveSuccess = false;
                            cvNoticePopUpTask.IsValid = false;
                            cvNoticePopUpTask.ErrorMessage = "Task with same title already exists.";
                            tbxTaskTitle.Focus();
                            return;
                        }

                        if (saveSuccess)
                        {
                            //Save Task Related Uploaded Documents
                            #region Upload Document

                            if (fuTaskDocUpload.HasFiles)
                            {
                                tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = newRecord.NoticeCaseInstanceID,
                                    DocTypeInstanceID = newRecord.ID, //TaskID
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocType = "NT"
                                };

                                HttpFileCollection fileCollection = Request.Files;

                                if (fileCollection.Count > 0)
                                {
                                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                    string directoryPath = "";
                                    String fileName = "";

                                    if (newRecord.ID > 0)
                                    {
                                        for (int i = 0; i < fileCollection.Count; i++)
                                        {
                                            HttpPostedFile uploadedFile = fileCollection[i];

                                            if (uploadedFile.ContentLength > 0)
                                            {
                                                string[] keys1 = fileCollection.Keys[i].Split('$');

                                                if (keys1[keys1.Count() - 1].Equals("fuTaskDocUpload"))
                                                {
                                                    fileName = uploadedFile.FileName;
                                                }

                                                objNoticeDoc.FileName = fileName;

                                                //Get Document Version
                                                var taskDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                                taskDocVersion++;
                                                objNoticeDoc.Version = taskDocVersion + ".0";

                                                directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + Convert.ToInt32(newRecord.NoticeCaseInstanceID) + "/Task/" + Convert.ToInt32(newRecord.ID) + "/" + objNoticeDoc.Version);

                                                if (!Directory.Exists(directoryPath))
                                                    Directory.CreateDirectory(directoryPath);

                                                Guid fileKey1 = Guid.NewGuid();
                                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                                Stream fs = uploadedFile.InputStream;
                                                BinaryReader br = new BinaryReader(fs);
                                                Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                                objNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                objNoticeDoc.FileKey = fileKey1.ToString();
                                                objNoticeDoc.VersionDate = DateTime.Now;
                                                objNoticeDoc.CreatedOn = DateTime.Now;
                                                objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                                DocumentManagement.Litigation_SaveDocFiles(fileList);
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);

                                                fileList.Clear();
                                            }

                                        }//End For Each                                    
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    if (saveSuccess)
                    {
                        string accessURL = string.Empty;

                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Task Save Successfully.";

                        //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                        if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(newRecord.AssignTo), customerID))
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                CryptographyManagement.Encrypt(newRecord.ID.ToString()) +
                                "&NID=" + CryptographyManagement.Encrypt(newRecord.NoticeCaseInstanceID.ToString());
                        else
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);

                        saveSuccess = SendTaskAssignmentMail(newRecord, accessURL, AuthenticationHelper.User);

                        if (saveSuccess)
                        {
                            cvNoticePopUpTask.ErrorMessage = "Task Save Successfully. An Email containing task detail and access URL to provide response sent to assignee.";

                            newRecord.AccessURL = accessURL;
                            saveSuccess = LitigationTaskManagement.UpdateTaskAccessURL(newRecord.ID, newRecord);
                        }

                        clearTaskControls();

                        //Re-Bind Notice Task Details
                        BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void btnActionSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        TextBox tbxNextAction = (TextBox)grdTaskActivity.FooterRow.FindControl("tbxNextAction");
        //        TextBox tbxDueOn = (TextBox)grdTaskActivity.FooterRow.FindControl("tbxDueOn");
        //        TextBox tbxActionRemark = (TextBox)grdTaskActivity.FooterRow.FindControl("tbxActionRemark");
        //        CheckBox chkAlert = (CheckBox)grdTaskActivity.FooterRow.FindControl("chkAlert");

        //        if (ViewState["noticeInstanceID"] != null)
        //        {
        //            if (tbxNextAction != null && tbxDueOn != null && tbxActionRemark != null && chkAlert != null)
        //            {
        //                bool validateData = false;
        //                bool saveSuccess = false;

        //                if (tbxNextAction.Text != "")
        //                {
        //                    if (tbxDueOn.Text != "")
        //                    {
        //                        validateData = true;
        //                    }
        //                }

        //                if (validateData)
        //                {
        //                    tbl_LegalNoticeTaskScheduleOn newRecord = new tbl_LegalNoticeTaskScheduleOn()
        //                    {
        //                        IsActive = true,
        //                        NoticeInstanceID=Convert.ToInt32(ViewState["noticeInstanceID"]),
        //                        ScheduleOnDate=DateTimeExtensions.GetDate(tbxDueOn.Text),
        //                        NextAction=tbxNextAction.Text,
        //                        CreatedBy=AuthenticationHelper.UserID,                                
        //                    };

        //                    if (chkAlert.Checked)
        //                        newRecord.Alert = true;
        //                    else
        //                        newRecord.Alert = false;

        //                    if (tbxActionRemark.Text != "")
        //                        newRecord.Remark = tbxActionRemark.Text;

        //                    saveSuccess = NoticeManagement.CreateNoticeNextActionLog(newRecord);
        //                }

        //                if(saveSuccess)
        //                {
        //                    cvNoticePopUpTask.IsValid = false;
        //                    cvNoticePopUpTask.ErrorMessage = "Action Detail Save Successfully.";

        //                    //Re-Bind Notice Action Log Details
        //                    BindNoticeActions(Convert.ToInt32(ViewState["noticeInstanceID"]));
        //                }
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnSaveResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    long newResponseID = 0;
                    long noticeInstanceID = 0;
                    noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                    if (tbxResponseDate.Text != "")
                    {
                        if (!String.IsNullOrEmpty(ddlRespBy.SelectedValue))
                        {
                            if (tbxRespThrough.Text != "")
                            {
                                if (tbxRespRefNo.Text != "")
                                {
                                    if (tbxResponseDesc.Text != "")
                                    {
                                        validateData = true;
                                    }
                                    else
                                    {
                                        cvNoticePopUpResponse.IsValid = false;
                                        cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response Description.";
                                        return;
                                    }
                                }
                                else
                                {
                                    cvNoticePopUpResponse.IsValid = false;
                                    cvNoticePopUpResponse.ErrorMessage = "Provide Reference/ Courier/ Post Traking Number, Put 'NA' if not available.";
                                    return;
                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response sent through (i.e. Courier Company/ Post Office Detail)";
                                return;
                            }
                        }
                        else
                        {
                            cvNoticePopUpResponse.IsValid = false;
                            cvNoticePopUpResponse.ErrorMessage = "Select Response sent by Courier/Post/Other.";
                            return;
                        }
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Provide Notice Response Date.";
                        return;
                    }

                    if (validateData)
                    {
                        tbl_LegalNoticeResponse newRecord = new tbl_LegalNoticeResponse()
                        {
                            IsActive = true,
                            NoticeInstanceID = noticeInstanceID,
                            ResponseDate = DateTimeExtensions.GetDate(tbxResponseDate.Text),
                            RespondedBy = Convert.ToInt32(ddlRespBy.SelectedValue),
                            ResponseThrough = tbxRespThrough.Text,
                            ResponseRefNo = tbxRespRefNo.Text,
                            Description = tbxResponseDesc.Text,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                        };

                        if (tbxResponseRemark.Text != "")
                            newRecord.Remark = tbxResponseRemark.Text;

                        newResponseID = NoticeManagement.CreateNoticeResponseLog(newRecord);

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    if (saveSuccess)
                    {
                        //Save Notice Response Uploaded Documents
                        #region Upload Document

                        if (fuResponseDocUpload.HasFiles)
                        {
                            tbl_LitigationFileData objResponseDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = noticeInstanceID,
                                DocTypeInstanceID = newResponseID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = "NR"
                            };

                            HttpFileCollection fileCollection = Request.Files;

                            if (fileCollection.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                                string directoryPath = "";
                                String fileName = "";

                                if (newResponseID > 0)
                                {
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        int customerID = -1;
                                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        HttpPostedFile uploadedFile = fileCollection[i];

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            string[] keys1 = fileCollection.Keys[i].Split('$');

                                            if (keys1[keys1.Count() - 1].Equals("fuResponseDocUpload"))
                                            {
                                                fileName = uploadedFile.FileName;
                                            }

                                            objResponseDoc.FileName = fileName;

                                            //Get Document Version
                                            var responseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objResponseDoc);

                                            responseDocVersion++;
                                            objResponseDoc.Version = responseDocVersion + ".0";

                                            directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Notice/" + noticeInstanceID + "/Response/" + objResponseDoc.Version);

                                            if (!Directory.Exists(directoryPath))
                                                Directory.CreateDirectory(directoryPath);

                                            Guid fileKey1 = Guid.NewGuid();
                                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                            Stream fs = uploadedFile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                                            fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                            objResponseDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objResponseDoc.FileKey = fileKey1.ToString();
                                            objResponseDoc.VersionDate = DateTime.Now;
                                            objResponseDoc.CreatedOn = DateTime.Now;
                                            objResponseDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(fileList);
                                            saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objResponseDoc);

                                            fileList.Clear();
                                        }

                                    }//End For Each                                    
                                }
                            }
                        }

                        #endregion

                        //Update Status
                        #region Status Transaction

                        tbl_LegalNoticeStatusTransaction newStatusRecord = new tbl_LegalNoticeStatusTransaction()
                        {
                            NoticeInstanceID = noticeInstanceID,
                            StatusID = 2,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            IsDeleted = false,
                            UserID = AuthenticationHelper.UserID,
                            RoleID = 3,
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };

                        if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusRecord))
                        {
                            saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusRecord);
                            saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusRecord);
                        }

                        #endregion

                        if (saveSuccess)
                        {
                            clearResponseControls();

                            cvNoticePopUpResponse.IsValid = false;
                            cvNoticePopUpResponse.ErrorMessage = "Response Detail Save Successfully.";
                        }

                        //Re-Bind Notice Action Log Details
                        BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveStatus_Click(object sender, EventArgs e)
        {
            try
            {

                bool saveSuccess = false;

                if (ViewState["noticeInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlNoticeStatus.SelectedValue))
                    {
                        long noticeInstanceID = 0;
                        noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                        if (noticeInstanceID != 0)
                        {
                            int selectedStatusID = Convert.ToInt32(ddlNoticeStatus.SelectedValue);

                            //Status Transaction Record - Which will Create or Update on Each Status Move
                            tbl_LegalNoticeStatusTransaction newStatusTxnRecord = new tbl_LegalNoticeStatusTransaction()
                            {
                                NoticeInstanceID = noticeInstanceID,
                                StatusID = selectedStatusID,
                                StatusChangeOn = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,
                                UserID = AuthenticationHelper.UserID,
                                RoleID = 3,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            //Status Record - i.e. Notice Closure Record; Only Active on Notice Close otherwise DeActive 
                            tbl_LegalNoticeStatus newStatusRecord = new tbl_LegalNoticeStatus()
                            {
                                NoticeInstanceID = noticeInstanceID,
                                StatusID = selectedStatusID,
                                CloseDate = DateTime.Now,
                                IsActive = true,
                                IsDeleted = false,

                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };

                            if (tbxCloseRemark.Text != "")
                                newStatusRecord.ClosureRemark = tbxCloseRemark.Text;

                            if (ddlNoticeStatus.SelectedValue != "3") //Open or In Progress
                            {
                                if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                {
                                    saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                                    saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                                }
                                else
                                    saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);

                                //If Exists Notice Closure Record then DeActive it
                                if (NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                    saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                            }
                            else if (ddlNoticeStatus.SelectedValue == "3") //Close
                            {
                                if (tbxNoticeCloseDate.Text != "")
                                {
                                    //Create or Update Status Transaction Records
                                    if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                    {
                                        saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                                        saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                                    }
                                    else
                                        saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);

                                    //Create or Update Status Record
                                    if (!NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                        saveSuccess = NoticeManagement.CreateNoticeStatus(newStatusRecord);
                                    else
                                        saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);

                                }
                                else
                                {
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Please Provide Close Date.";
                                    return;
                                }
                            }

                            if (saveSuccess)
                            {
                                ViewState["noticeStatus"] = selectedStatusID;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveConvertToCase_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                if (ViewState["noticeInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlNoticeStatus.SelectedValue))
                    {
                        if (ddlNoticeStatus.SelectedValue == "3") //Status - Closed
                        {
                            long noticeInstanceID = 0;
                            noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);

                            if (noticeInstanceID != 0)
                            {
                                //STATUS UPDATE
                                #region Status UPDATE ---To Closed

                                int selectedStatusID = Convert.ToInt32(ddlNoticeStatus.SelectedValue);

                                //Status Transaction Record - Which will Create or Update on Each Status Move
                                tbl_LegalNoticeStatusTransaction newStatusTxnRecord = new tbl_LegalNoticeStatusTransaction()
                                {
                                    NoticeInstanceID = noticeInstanceID,
                                    StatusID = selectedStatusID,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    UserID = AuthenticationHelper.UserID,
                                    RoleID = 3,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                //Status Record - i.e. Notice Closure Record; Only Active on Notice Close otherwise DeActive 
                                tbl_LegalNoticeStatus newStatusRecord = new tbl_LegalNoticeStatus()
                                {
                                    NoticeInstanceID = noticeInstanceID,
                                    StatusID = selectedStatusID,
                                    CloseDate = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,

                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                if (tbxCloseRemark.Text != "")
                                    newStatusRecord.ClosureRemark = tbxCloseRemark.Text;


                                if (tbxNoticeCloseDate.Text != "")
                                {
                                    //Create or Update Status Transaction Records
                                    if (!NoticeManagement.ExistNoticeStatusTransaction(newStatusTxnRecord))
                                    {
                                        saveSuccess = NoticeManagement.DeActiveNoticeStatusTransaction(newStatusTxnRecord);
                                        saveSuccess = NoticeManagement.CreateNoticeStatusTransaction(newStatusTxnRecord);
                                    }
                                    else
                                        saveSuccess = NoticeManagement.UpdateNoticeStatusTransaction(newStatusTxnRecord);

                                    //Create or Update Status Record
                                    if (!NoticeManagement.ExistNoticeStatus(newStatusRecord))
                                        saveSuccess = NoticeManagement.CreateNoticeStatus(newStatusRecord);
                                    else
                                        saveSuccess = NoticeManagement.UpdateNoticeStatus(newStatusRecord);
                                }
                                else
                                {
                                    saveSuccess = false;
                                    cvNoticeStatus.IsValid = false;
                                    cvNoticeStatus.ErrorMessage = "Please Provide Close Date.";
                                    return;
                                }
                                #endregion

                                #region Notice-Case Mapping
                                if (saveSuccess)
                                {
                                    var noticeDetail = NoticeManagement.GetNoticeByID(Convert.ToInt32(noticeInstanceID));

                                    if (noticeDetail != null)
                                    {
                                        long NewCaseID = 0;

                                        //New Case Record
                                        tbl_LegalCaseInstance NewCaseRecord = new tbl_LegalCaseInstance()
                                        {
                                            IsDeleted = false,
                                            CaseType = noticeDetail.NoticeType,
                                            //CaseRefNo = tbxRefNo.Text.Trim(),
                                            //OpenDate = DateTimeExtensions.GetDate(txtCaseDate.Text),
                                            Section = noticeDetail.Section,
                                            CaseCategoryID = noticeDetail.NoticeCategoryID,
                                            CaseTitle = noticeDetail.NoticeTitle,
                                            CaseDetailDesc = noticeDetail.NoticeDetailDesc,
                                            CustomerBranchID = noticeDetail.CustomerBranchID,
                                            DepartmentID = noticeDetail.DepartmentID,
                                            OwnerID = noticeDetail.OwnerID,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            ClaimAmt = noticeDetail.ClaimAmt,
                                            ProbableAmt = noticeDetail.ProbableAmt,
                                            AssignmentType = noticeDetail.AssignmentType,
                                            CaseRiskID = noticeDetail.NoticeRiskID
                                        };

                                        NewCaseID = CaseManagement.CreateCase(NewCaseRecord);

                                        if (NewCaseID > 0)
                                            saveSuccess = true;

                                        if (saveSuccess)
                                        {
                                            //Case Status Transaction --Open
                                            #region Status Transaction
                                            tbl_LegalCaseStatusTransaction newCaseStatusRecord = new tbl_LegalCaseStatusTransaction()
                                            {
                                                CaseInstanceID = NewCaseID,
                                                StatusID = 1,
                                                StatusChangeOn = DateTime.Now,
                                                IsActive = true,
                                                IsDeleted = false,
                                                UserID = AuthenticationHelper.UserID,
                                                RoleID = 3,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            if (!CaseManagement.ExistCaseStatusTransaction(newCaseStatusRecord))
                                                saveSuccess = CaseManagement.CreateCaseStatusTransaction(newCaseStatusRecord);

                                            #endregion

                                            //Lawyer Mapping
                                            #region Lawyer Mapping

                                            var noticeLawyerMapping = NoticeManagement.GetNoticeLawyerMapping(Convert.ToInt32(noticeInstanceID));

                                            tbl_LegalCaseLawyerMapping objCaseLawyerMapping = new tbl_LegalCaseLawyerMapping()
                                            {
                                                CaseInstanceID = NewCaseID,
                                                IsActive = true,
                                                LawyerID = Convert.ToInt32(noticeLawyerMapping.LawyerID),
                                                CreatedBy = AuthenticationHelper.UserID,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = CaseManagement.CreateCaseLawyerMapping(objCaseLawyerMapping);
                                            
                                            #endregion

                                            //User Assignment
                                            #region User Assignment

                                            //Get Notice User Assignment
                                            var lstNoticeUserAssignment = NoticeManagement.GetNoticeAssignment(Convert.ToInt32(noticeInstanceID));

                                            if (lstNoticeUserAssignment.Count > 0)
                                            {
                                                lstNoticeUserAssignment.ForEach(eachNoticeAssignedUserRecord =>
                                                {
                                                    tbl_LegalCaseAssignment newAssignment = new tbl_LegalCaseAssignment()
                                                    {
                                                        AssignmentType = eachNoticeAssignedUserRecord.AssignmentType,
                                                        CaseInstanceID = NewCaseID,
                                                        IsActive = true,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        UpdatedBy = AuthenticationHelper.UserID,

                                                        UserID = eachNoticeAssignedUserRecord.UserID,
                                                        RoleID = eachNoticeAssignedUserRecord.RoleID,
                                                    };

                                                    if (!CaseManagement.ExistCaseAssignment(newAssignment))
                                                        saveSuccess = CaseManagement.CreateCaseAssignment(newAssignment);
                                                });
                                            }

                                            #endregion
                                        }

                                        if (saveSuccess)
                                        {
                                            cvNoticeStatus.IsValid = false;
                                            cvNoticeStatus.ErrorMessage = "Notice convert to Case Successfully. Please See and Update the details(if required) Under Case List.";
                                        }
                                    }
                                }

                                #endregion

                            }
                        }
                        else
                        {
                            cvNoticeStatus.IsValid = false;
                            cvNoticeStatus.ErrorMessage = "Selected Status must be Closed.";
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox) grdNoticePayment.FooterRow.FindControl("tbxPaymentDate");
                DropDownList ddlPaymentType = (DropDownList) grdNoticePayment.FooterRow.FindControl("ddlPaymentType");
                TextBox tbxAmount = (TextBox) grdNoticePayment.FooterRow.FindControl("tbxAmount");
                TextBox tbxPaymentRemark = (TextBox) grdNoticePayment.FooterRow.FindControl("tbxPaymentRemark");

                if (ViewState["noticeInstanceID"] != null)
                {
                    if (tbxPaymentDate != null && ddlPaymentType != null && tbxAmount != null && tbxPaymentRemark != null)
                    {
                        bool validateData = false;
                        bool saveSuccess = false;

                        if (tbxPaymentDate.Text != "")
                        {
                            if (!String.IsNullOrEmpty(ddlPaymentType.SelectedValue))
                            {
                                if (tbxAmount.Text != "")
                                {
                                    if (tbxPaymentRemark.Text != "")
                                    {
                                        try
                                        {
                                            Convert.ToDecimal(tbxAmount.Text);
                                            validateData = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            validateData = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (validateData)
                        {
                            tbl_NoticeCasePayment newRecord = new tbl_NoticeCasePayment()
                            {
                                NoticeOrCase = "N",
                                IsActive = true,
                                NoticeOrCaseInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]),
                                PaymentDate = DateTimeExtensions.GetDate(tbxPaymentDate.Text),
                                PaymentID =Convert.ToInt32(ddlPaymentType.SelectedValue),
                                Amount = Convert.ToDecimal(tbxAmount.Text),
                                CreatedBy = AuthenticationHelper.UserID,
                            };

                            if (tbxPaymentRemark.Text != "")
                                newRecord.Remark = tbxPaymentRemark.Text;

                            saveSuccess = NoticeManagement.CreateNoticePaymentLog(newRecord);
                        }

                        if (saveSuccess)
                        {
                            cvNoticePayment.IsValid = false;
                            cvNoticePayment.ErrorMessage = "Payment Detail Save Successfully.";

                            //Re-Bind Notice Payment Log Details
                            BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlAct.SelectedValue != "-1")
        //        {
        //            if (ddlAct.SelectedValue == "0")
        //                lnkShowAddNewActModal.Visible = true;
        //            else
        //                lnkShowAddNewActModal.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (taskRecord != null)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));

                    if (User != null)
                    {
                        if (User.Email != null && User.Email != "")
                        {
                            string taskPriority = string.Empty;

                            if (taskRecord.PriorityID != null)
                            {
                                if (taskRecord.PriorityID == 1)
                                    taskPriority = "High";
                                else if (taskRecord.PriorityID == 2)
                                    taskPriority = "Medium";
                                else if (taskRecord.PriorityID == 3)
                                    taskPriority = "Low";
                            }

                            string assignedToUserName = string.Format("{0} {1}", User.FirstName, User.LastName);

                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                                                    .Replace("@User", assignedToUserName)
                                                    .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                    .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                    .Replace("@Priority", taskPriority)
                                                    .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                    .Replace("@AssignedBy", assignedBy)
                                                    .Replace("@Remark", taskRecord.Remark)
                                                    .Replace("@AccessURL", accessURL)
                                                    .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Litigation Notification-Task Assigned", message);

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public void clearTaskControls()
        {
            try
            {
                tbxTaskTitle.Text = "";
                tbxTaskDueDate.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskRemark.Text = "";
                ddlTaskPriority.ClearSelection();
                ddlTaskUser.ClearSelection();

                fuTaskDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearResponseControls()
        {
            try
            {
                tbxResponseDate.Text = "";
                ddlRespBy.ClearSelection();
                tbxRespThrough.Text = "";
                tbxRespRefNo.Text = "";
                tbxResponseDesc.Text = "";
                tbxResponseRemark.Text = "";

                fuResponseDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearNoticeControls()
        {
            try
            {
                txtNoticeDate.Text = "";
                tbxRefNo.Text = "";
                ddlParty.ClearSelection();
                ddlAct.ClearSelection();
                tbxSection.Text = "";
                ddlNoticeCategory.ClearSelection();
                tbxTitle.Text = "";
                tbxDescription.Text = "";
                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                //tvBranches.SelectedNode.Selected = false;
                ddlDepartment.ClearSelection();
                ddlOwner.ClearSelection();
                ddlNoticeRisk.ClearSelection();
                tbxClaimedAmt.Text = "";
                tbxProbableAmt.Text = "";

                NoticeFileUpload.Attributes.Clear();

                ddlLawFirm.ClearSelection();

                ddlPerformer.ClearSelection();
                ddlReviewer.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableNoticeControls(bool flag)
        {
            try
            {
                rbNoticeInOutType.Enabled = flag;
                txtNoticeDate.Enabled = flag;
                tbxRefNo.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlAct.Enabled = flag;
                tbxSection.Enabled = flag;
                ddlNoticeCategory.Enabled = flag;
                tbxTitle.Enabled = flag;
                tbxDescription.Enabled = flag;
                tbxBranch.Enabled = flag;
                //tvBranches.SelectedNode.Selected = false;
                ddlDepartment.Enabled = flag;
                ddlOwner.Enabled = flag;
                ddlNoticeRisk.Enabled = flag;
                tbxClaimedAmt.Enabled = flag;
                tbxProbableAmt.Enabled = flag;

                NoticeFileUpload.Enabled = flag;

                ddlLawFirm.Enabled = flag;

                ddlPerformer.Enabled = flag;
                ddlReviewer.Enabled = flag;

                btnSave.Enabled = flag;
                btnClearNoticeDetail.Enabled = flag;

                if (flag)
                {
                    ddlPerformer.Attributes.Remove("disabled");
                    ddlReviewer.Attributes.Remove("disabled");

                    btnSave.Attributes.Remove("disabled");
                    btnClearNoticeDetail.Attributes.Remove("disabled");
                }
                else
                {
                    ddlPerformer.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");

                    btnSave.Attributes.Add("disabled", "disabled");
                    btnClearNoticeDetail.Attributes.Add("disabled", "disabled");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {

                tbxTaskTitle.Enabled = flag;
                tbxTaskDueDate.Enabled = flag;
                ddlTaskUser.Enabled = flag;
                ddlTaskPriority.Enabled = flag;
                tbxTaskDesc.Enabled = flag;
                tbxTaskRemark.Enabled = flag;
                fuTaskDocUpload.Enabled = flag;

                btnTaskSave.Enabled = flag;
                btnTaskClear.Enabled = flag;

                grdTaskActivity.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableNoticePopUpControls(bool flag)
        {
            try
            {
                pnlNotice.Enabled = flag;
                pnlNoticeAssignment.Enabled = flag;
                pnlTask.Enabled = flag;
                pnlResponse.Enabled = flag;

                btnSave.Enabled = flag;
                btnEditNoticeDetail.Enabled = flag;

                grdTaskActivity.Columns[7].Visible = flag;

                grdNoticePayment.ShowFooter = flag;
                grdNoticePayment.Columns[5].Visible = flag;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadNoticeDocument(int noticeFileID)
        {
            try
            {
                var file = NoticeManagement.GetNoticeDocumentByID(noticeFileID);
                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteNoticeFile(int noticeFileID)
        {
            try
            {
                if (noticeFileID != 0)
                {
                    if (NoticeManagement.DeleteNoticeDocument(noticeFileID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Document Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUp.IsValid = false;
                        cvNoticePopUp.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteNoticeResponse(int noticeResponseID)
        {
            try
            {
                if (noticeResponseID != 0)
                {
                    //Delete Response with Documents
                    if (NoticeManagement.DeleteNoticeResponseLog(noticeResponseID, AuthenticationHelper.UserID))
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Document Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpResponse.IsValid = false;
                        cvNoticePopUpResponse.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpResponse.IsValid = false;
                cvNoticePopUpResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteTask(int taskID,long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Task Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePopUpTask.IsValid = false;
                        cvNoticePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUpTask.IsValid = false;
                cvNoticePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeletePaymentLog(int noticePaymentID)
        {
            try
            {
                if (noticePaymentID != 0)
                {
                    if (NoticeManagement.DeleteNoticePaymentLog(noticePaymentID, AuthenticationHelper.UserID))
                    {
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Action Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvNoticePayment.IsValid = false;
                        cvNoticePayment.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePayment.IsValid = false;
                cvNoticePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdNoticeDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadNoticeDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadNoticeDoc");

            if (lnkBtnDownLoadNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadNoticeDoc);
            }

            LinkButton lnkBtnDeleteNoticeDoc = (LinkButton) e.Row.FindControl("lnkBtnDeleteNoticeDoc");
            if (lnkBtnDeleteNoticeDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteNoticeDoc);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteNoticeDoc.Visible = false;
                    else
                        lnkBtnDeleteNoticeDoc.Visible = true;
                }
            }
        }

        protected void grdNoticeDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadNoticeDoc"))
                    {
                        DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteNoticeDoc"))
                    {
                        DeleteNoticeFile(Convert.ToInt32(e.CommandArgument));

                        //Bind Notice Related Documents
                        if (ViewState["noticeInstanceID"] != null)
                            BindNoticeRelatedDocuments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdNoticeDocuments_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticeDocuments.PageIndex = e.NewPageIndex;
                    BindNoticeRelatedDocuments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton) e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton) e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                if (lblTaskStatus != null)
                {
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                BindTaskResponses(taskID, gvTaskResponses);
                            }
                        }
                    }
                    else
                    {
                        HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;
                        //Image imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as Image;
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }


                    //Hide Close Task Button
                    LinkButton lnkBtnCloseTask = e.Row.FindControl("lnkBtnCloseTask") as LinkButton;
                    if (lnkBtnCloseTask != null)
                    {
                        if (lblTaskStatus.Text != "" && lblTaskStatus.Text == "Closed")
                            lnkBtnCloseTask.Visible = false;
                        else
                            lnkBtnCloseTask.Visible = true;
                    }
                }
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Related Documents
                    BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);

                    //ScriptManager.RegisterClientScriptBlock(Me.GridView1, Me.GetType(), "myScript", "alert('Done with paging');", True)
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["noticeInstanceID"] != null)
                    {
                        int noticeInstanceID = Convert.ToInt32(ViewState["noticeInstanceID"]);
                        int taskID = Convert.ToInt32(e.CommandArgument);

                        if (e.CommandName.Equals("DeleteTask"))
                        {
                            DeleteTask(taskID,AuthenticationHelper.CustomerID);

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("CloseTask"))
                        {
                            //Update Task Status to Closed and Expire URL
                            LitigationTaskManagement.UpdateTaskStatus(taskID, 3, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID); //Status 3 - Closed

                            //Re-Bind Notice Related Documents
                            BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                        else if (e.CommandName.Equals("TaskReminder")) //Send Reminder or Re-generate URL
                        {
                            if (taskID != 0)
                            {
                                string accessURL = string.Empty;
                                bool sendSuccess = false;

                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                    CryptographyManagement.Encrypt(taskID.ToString()) +
                                    "&NID=" + CryptographyManagement.Encrypt(noticeInstanceID.ToString());

                                //Get Task Record
                                var taskRecord = LitigationTaskManagement.GetTaskDetailByTaskID(noticeInstanceID, taskID, "N", AuthenticationHelper.CustomerID);

                                if (taskRecord != null)
                                {
                                    sendSuccess = SendTaskAssignmentMail(taskRecord, accessURL, AuthenticationHelper.User);

                                    if (sendSuccess)
                                    {
                                        cvNoticePopUpTask.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";

                                        taskRecord.AccessURL = accessURL;
                                        taskRecord.UpdatedBy = AuthenticationHelper.UserID;
                                        sendSuccess = LitigationTaskManagement.UpdateTaskAccessURL(taskRecord.ID, taskRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        protected void grdResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdResponseLog.PageIndex = e.NewPageIndex;

                    BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);
                        int NoticeInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && NoticeInstanceID != 0)
                        {
                            var lstResponseDocument = NoticeManagement.GetNoticeResponseDocuments(NoticeInstanceID, responseID, "NR");

                            if (lstResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=NoticeResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvNoticePopUpResponse.IsValid = false;
                                cvNoticePopUpResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }

                            DownloadNoticeDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("DeleteResponse"))
                    {
                        DeleteNoticeResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID 

                        //Bind Notice Responses
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadResponseDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadResponseDoc");

            if (lnkBtnDownLoadResponseDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadResponseDoc);
            }

            LinkButton lnkBtnDeleteResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteResponse");
            if (lnkBtnDeleteResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteResponse);

                if (ViewState["noticeStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["noticeStatus"]) == 3)
                        lnkBtnDeleteResponse.Visible = false;
                    else
                        lnkBtnDeleteResponse.Visible = true;
                }
            }
        }

        protected void grdNoticePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton) e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlPaymentType = (DropDownList) e.Row.FindControl("ddlPaymentType");

                if (ddlPaymentType != null)
                    BindPaymentType(ddlPaymentType);
            }
        }

        protected void grdNoticePayment_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["noticeInstanceID"] != null)
                {
                    grdNoticePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Payments
                    BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        DeletePaymentLog(Convert.ToInt32(e.CommandArgument));

                        //Re-Bind Notice Payments
                        if (ViewState["noticeInstanceID"] != null)
                        {
                            BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                //ScriptManager.RegisterStartupScript(this.upNoticePopup, this.upNoticePopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvNoticePopUp.IsValid = false;
                cvNoticePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkNoticeDetail_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "active");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
        }

        protected void lnkNoticeTask_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "active");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindTasks(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeResponse_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "active");
            liNoticeStatusPayment.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticeResponses(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        protected void lnkNoticeStatusPayment_Click(object sender, EventArgs e)
        {
            liNoticeDetail.Attributes.Add("class", "");
            liNoticeTask.Attributes.Add("class", "");
            liNoticeResponse.Attributes.Add("class", "");
            liNoticeStatusPayment.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 3;

            if (ViewState["noticeInstanceID"] != null)
            {
                BindNoticePayments(Convert.ToInt32(ViewState["noticeInstanceID"]));
            }
        }

        public string ShowNoticeResponseDocCount(long noticeInstanceID, long noticeResponseID)
        {
            try
            {
                var docCount = NoticeManagement.GetNoticeResponseDocuments(noticeInstanceID, noticeResponseID, "NR").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        // New code
        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lnkBtnParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lnkBtnCategory_Click(object sender, EventArgs e)
        {
            BindNoticeCaseType();
        }

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

    }
}