﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="myRemindersNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.myRemindersNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />

    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
           .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height:30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
              line-height: 14px;
        }
        .k-grid tbody tr {
            height: 38px;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Reminders');
            $('#dd2').hide();

            BindTypeofUser();
            Bindgrid();

            $(document).on("click", "#grid tbody tr .ob-deleteuser", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    debugger;
                    $.ajax({
                        type: 'POST',
                        url: "<% =Path%>Litigation/Delete_Litigation_ReminderByID?reminderID=" + item.ReminderID,
                        dataType: "json",
                         beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#grid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });
        });

        var record = 0;
        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/kendomyReminder?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&reminderType=' + $("#dropdownlist1").val() + '&instanceID=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/kendomyReminder?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&reminderType=' + $("#dropdownlist1").val() +'&instanceID=-1'
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    if (this.ReminderStatus == 0) { template: "#=pending" }
                },
                columns: [{
                    title: "Sr",
                    template: "#= ++record #",
                    width: 50
                },
                {
                    field: "Type", title: 'Type', template: "#if(Type == 'C') {#<div>Case</div>#}if(Type == 'T') {#<div>Task</div>#} if(Type == 'N') {#<div>Notice</div>#}#",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "ReminderTitle", title: 'Reminder',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "Description", title: 'Description',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "15%",
                },
                {
                    field: "Remark", title: 'Remark',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "RemindOn", title: 'Remind On',
                    template: "#= kendo.toString(kendo.parseDate(RemindOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "ReminderStatus", title: 'Status', template: "#if(ReminderStatus==0){#<div>pending</div>#}#",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        extra: false,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },

                {
                    command: [
                        { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                        { name: "edit1", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-deleteuser" }], title: "Action", lock: true, width: "15%;",// width: 150,
                }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Delete";
                }
            });
            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                $('#divShowReminderDialog').modal('show');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=" + item.ReminderID)

            });
        }

        function BindTypeofUser() {

            $("#dropdownlist1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    Bindgrid();
                    showdropdown();
                },
                dataSource: [
                    { text: "All", value: "0" },
                    { text: "Notice", value: "1" },
                    { text: "case", value: "2" },
                    { text: "Task", value: "3" },

                ]
            });
        }

        function showdropdown() {
            if ($("#dropdownlist1").val() == 1 || $("#dropdownlist1").val() == 2 || $("#dropdownlist1").val() == 3) {
                $('#dd2').show();
                $("#dropdownlist2").kendoDropDownList({
                    filter: "startswith",
                    autoClose: false,
                    autoWidth: true,
                    dataTextField: "Name",
                    dataValueField: "ID",
                    optionLabel: "Select",
                    change: function (e) {
                        var values = this.value();
                        if (values != "" && values != null) {
                            var filter = { logic: "or", filters: [] };
                            filter.filters.push({
                                field: "InstanceID", operator: "eq", value: parseInt(values)
                            });
                            var dataSource = $("#grid").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }
                        else {
                            $("#grid").data("kendoGrid").dataSource.filter({});
                        }
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Litigation/kendomyReminderFilterList?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&roleID=3&ddltype=' + $("#dropdownlist1").val(),
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>Litigation/kendomyReminderFilterList?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&roleID=3&ddltype=' + $("#dropdownlist1").val()

                        }
                    }
                });
            }
            if ($("#dropdownlist1").val() == 0) {
                $('#dd2').hide();
            }
        }

        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }

        function AddnewTest(e) {
            $('#divShowReminderDialog').modal('show');
            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=0")
            e.preventDefault();
            return true;
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <div class="row">
        <div class="col-lg-12 col-md-12" style="margin-top: 13px;">
            <div class="col-lg-3 col-md-3" style="margin-left:-19px;">
                <input id="dropdownlist1" style="width: 100%;" />
            </div>
            <div id="dd2" class="col-lg-3 col-md-3">
                <input id="dropdownlist2" style="width: 100%;" />
            </div>
            
             <div class="col-lg-3 col-md-3">
            </div>
             <div class="col-lg-3 col-md-3" style="float: right;margin-right: -159px;">
                 <button id="Addnew" style="height: 35px; width:100px;" onclick="AddnewTest(event)" class="btn btn-search">Add New</button>
                 </div>
               
        </div>
    </div>

    <div class="row" style="padding-top: 12px;">
        <div id="grid" style="border:none;margin-left:10px;margin-right:10px;"></div>
    </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
