﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CaseListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.CaseListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />


    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <style type="text/css">
           .div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }
        .k-grid-content {
            min-height: 394px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }
      
        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height:30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
              line-height: 14px;
        }
        .k-grid tbody tr {
            height: 38px;
        }
       
    </style>
    <title></title>

    <script type="text/javascript">

        function ReadQuerySt(e) {
            try {
                for (hu = window.location.search.substring(1), gy = hu.split('&'), i = 0; i < gy.length; i++)
                    if (ft = gy[i].split('='), ft[0].toLowerCase() == e.toLowerCase()) return ft[1];
                return ''
            } catch (t) {
                return ''
            }
        }

        $(document).ready(function () {
            bindFY();
            bindCalendarYear();
            BindStatus();
            BindType();
            //if (document.getElementById('CustomerId').value == "5")//89
            if (document.getElementById('CustomerId').value=="5" && ReadQuerySt('CT')=='')
            { 
                $("#dropdownType").data("kendoDropDownList").value('2');
                $("#dropdownType1").data("kendoDropDownList").value('2');
            }   
            BindLawyers();
            BindPeriod();
            BindCategory();
            BindOpponent();
            BindBranch();
            BindCourt();
            BindWinLoseType();
            BindDepartment();
            bindNoticeType();
            Bindgrid();
            BindgridPopup();

            var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
            if ($("#dropdownType1").val() == "1") {
                CourtDropDown.wrapper.hide();
            }
            else {
                CourtDropDown.wrapper.show();
            }

            $("#Startdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            function BindLawyers() {
                $("#dropdownLawyers").kendoDropDownTree({
                    placeholder: "Opposition Lawyer",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: true,
                    checkAllTemplate: "Select All",
                    autoWidth: true,
                    dataTextField: "LawyerName",
                    dataValueField: "LawyertID",
                    change: function () {

                        fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: "<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>"
                        },
                    }
                });
            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });


        });

        function onChange() {
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);

            }

        }


        function BindWinLoseType() {
            $("#dropdownWinLose").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "TypeName",
                dataValueField: "ID",
                optionLabel:"Result",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>"
                    },
                }
            });

        }

        function bindNoticeType() {
            $("#NoticeType").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //FilterAll();
                    fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
            $("#NoticeType1").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //FilterAllAdvanced();
                    fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
        }

        function BindDepartment() {
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                //dataValueField: "DepartmentID",
                change: function () {
                    //FilterAll();
                    fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });

            $("#dropdownDept1").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                change: function () {
                    //FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });
        }

        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    //FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                //checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    //FilterAllAdvanced();
                    fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function BindStatus() {
            $("#dropdownStatus").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" }
                ]
            });

            $("#dropdownStatus1").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //BindgridPopup();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" }
                ]
            });

            $("#dropdownStatus").data("kendoDropDownList").value(<% =StatusFlagID%>);

            $("#dropdownStatus1").data("kendoDropDownList").value(<% =StatusFlagID%>);
        }

        function BindType() {
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownType").val() == "3") {
                        window.location.href = "../aspxPages/TaskListNew.aspx";
                    }
                    else {
                        Bindgrid();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" }
                ]
            });

            $("#dropdownType1").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
                    if ($("#dropdownType1").val() == "1") {
                        CourtDropDown.wrapper.hide();
                    }
                    else {
                        CourtDropDown.wrapper.show();
                    }
                    if ($("#dropdownType1").val() == "3") {
                        window.location.href = "../aspxPages/TaskListNew.aspx";
                    }
                    else {
                        BindgridPopup();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" }
                ]
            });
            $("#dropdownType").data("kendoDropDownList").value('<% =CNTtype%>');

            $("#dropdownType1").data("kendoDropDownList").value('<% =CNTtype%>');
        }

        function BindOpponent() {
            $("#dropdownOpponent").kendoDropDownTree({
                placeholder: "Opponent",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function () {

                    fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/LegalCaseParty?CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/LegalCaseParty?CustomerID=<% =CustId%>"
                    },
                }
            });
        }

        function BindCourt() {
            $("#dropdownCourtWise").kendoDropDownTree({
                placeholder: "Court",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CourtName",
                dataValueField: "ID",
                change: function (e) {

                    fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>"
                    }
                }
            });
        }

        function BindCategory() {
            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                autoClose: false,
                change: function (e) {
                    //FilterAll();
                    fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                    }
                }
            });

            $("#dropdownCategory1").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                change: function (e) {
                    //FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                     }
                 }
             });

        }

        function BindPeriod() {

            $("#dropdownPastData1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    // BindgridPopup();
                    $("#dropdownFY1").data("kendoDropDownList").select(0);
                    $('#Startdatepicker').val('');
                    $('#Lastdatepicker').val('');
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        function bindCalendarYear() {
            $("#dropdownCalYear").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear").val() != 0) {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "Year - 2021", value: "2021" },
                    { text: "Year - 2020", value: "2020" },
                    { text: "Year - 2019", value: "2019" },
                    { text: "Year - 2018", value: "2018" },
                    { text: "Year - 2017", value: "2017" },
                    { text: "Year - 2016", value: "2016" },
                    { text: "Year - 2015", value: "2015" },
                    { text: "Year - 2014", value: "2014" },
                    { text: "Year - 2013", value: "2013" },
                    { text: "Year - 2012", value: "2012" }

                ]
            });

            $("#dropdownCalYear1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear1").val() != 0) {
                        $("#dropdownFY1").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "Year - 2021", value: "2021" },
                    { text: "Year - 2020", value: "2020" },
                    { text: "Year - 2019", value: "2019" },
                    { text: "Year - 2018", value: "2018" },
                    { text: "Year - 2017", value: "2017" },
                    { text: "Year - 2016", value: "2016" },
                    { text: "Year - 2015", value: "2015" },
                    { text: "Year - 2014", value: "2014" },
                    { text: "Year - 2013", value: "2013" },
                    { text: "Year - 2012", value: "2012" }

                ]
            });
        }

        function bindFY() {
            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    $("#dropdownCalYear").data("kendoDropDownList").select(0);
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });

            $("#dropdownFY1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY1").val() != 0) {
                        $("#dropdownPastData1").data("kendoDropDownList").select(4);
                        $('#Startdatepicker').val('');
                        $('#Lastdatepicker').val('');
                        $("#dropdownCalYear1").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });
        }

        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp');//Department
            }
            if (div == 'filtersstoryboardNoticeType') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }

            if (div == 'filtersstoryCategoryboard') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
            fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
            fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
        };

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#NoticeType").data("kendoDropDownTree").value([]);
            $("#dropdownCategory").data("kendoDropDownTree").value([]);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownCalYear").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fCreateStoryBoard1(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept1') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp');//Department
            }
            if (div == 'filtersstoryboardNoticeType1') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }
            if (div == 'filtersstoryCategoryboard1') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            if (div == 'filtersstoryCourtboard') {
                $('#' + div).append('Court &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Court
            }
            if (div == 'filtersstoryboardOpponent') {
                $('#' + div).append('Opponent &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//opponent
            }
            if (div == 'filtersstoryboardLawyer') {
                $('#' + div).append('Lawyer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Lawyer
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory1(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory1(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory1(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
            fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
            fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
            fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
            fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
            fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
        };

        function ClearAllFilter(e) {
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownDept1").data("kendoDropDownTree").value([]);
            $("#NoticeType1").data("kendoDropDownTree").value([]);
            $("#dropdownCategory1").data("kendoDropDownTree").value([]);
            $("#dropdownCourtWise").data("kendoDropDownTree").value([]);
            $("#dropdownOpponent").data("kendoDropDownTree").value([]);
            $("#dropdownLawyers").data("kendoDropDownTree").value([]);
            $("#dropdownWinLose").data("kendoDropDownList").select(0);
            $("#dropdownPastData1").data("kendoDropDownList").select(4);
            $("#dropdownFY1").data("kendoDropDownList").select(0);
            $("#dropdownCalYear1").data("kendoDropDownList").select(0);
            document.getElementById('txtSearch').value = '';
            $('#Startdatepicker').val('');
            $('#Lastdatepicker').val('');
            BindgridPopup();
            e.preventDefault();
        }

        function FilterAllAdvanced() {
            var locationlist = $("#dropdowntree1").data("kendoDropDownTree")._values;

            //Department details
            var Departmentlist = $("#dropdownDept1").data("kendoDropDownTree")._values;

            var NoticeTypedetails = $("#NoticeType1").data("kendoDropDownTree")._values;

            //Category details
            var Categorylist8 = $("#dropdownCategory1").data("kendoDropDownTree")._values;

            //Court details
            var Courtlist7 = $("#dropdownCourtWise").data("kendoDropDownTree")._values;

            //Oppnent details
            var Oppnentlist4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;

            //Lawyers details
            var Lawyerlist = $("#dropdownLawyers").data("kendoDropDownTree")._values;

            var FilterForAllSearch = document.getElementById('txtSearch').value;
            
            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || Courtlist7.length > 0
                || Oppnentlist4.length > 0
                || Lawyerlist.length > 0
                || FilterForAllSearch != ""
                || ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "")
                || ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY1").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "") {
                    var WLFilter = { logic: "or", filters: [] };

                    WLFilter.filters.push({
                        field: "WLResult", operator: "eq", value: $("#dropdownWinLose").val()
                    });

                    finalSelectedfilter.filters.push(WLFilter);
                }

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }

                if (NoticeTypedetails.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType1").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType1").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }

                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (Courtlist7.length > 0) {
                    if ($("#dropdownType1").val() == "2") {
                        var CourtFilter = { logic: "or", filters: [] };

                        $.each(Courtlist7, function (i, v) {
                            CourtFilter.filters.push({
                                field: "CourtID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(CourtFilter);
                    }
                }


                if (Oppnentlist4.length > 0) {
                    var OppnentFilter = { logic: "or", filters: [] };

                    $.each(Oppnentlist4, function (i, v) {
                        OppnentFilter.filters.push({
                            field: "PartyID", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(OppnentFilter);
                }

                if (Lawyerlist.length > 0) {
                    var lawyerFilter = { logic: "or", filters: [] };

                    $.each(Lawyerlist, function (i, v) {
                        lawyerFilter.filters.push({
                            field: " Opposition", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(lawyerFilter);
                }
                if (FilterForAllSearch != "" && FilterForAllSearch != undefined) {
                    var AllFilter = { logic: "or", filters: [] };

                    AllFilter.filters.push({
                        field: "Department", operator: "contains", value: FilterForAllSearch
                    });

                    AllFilter.filters.push({
                        field: "BranchName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "Category", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "NoticeCaseDesc", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "TypeName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CaseRefNo", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "PartyName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "RefNo", operator: "contains", value: FilterForAllSearch
                    });
                    finalSelectedfilter.filters.push(AllFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }

        }

        function FilterAll()
        {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;

            //Department details
            var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;


            var NoticeTypedetails = $("#NoticeType").data("kendoDropDownTree")._values;

            var Categorylist8 = $("#dropdownCategory").data("kendoDropDownTree")._values;

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != ""))
            {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "")
                {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0)
                {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Departmentlist.length > 0)
                {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }
                if (NoticeTypedetails.length > 0)
                {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }
                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }
        var record = 0;
        var caserefno = 0;
        var LitigationType = 0;
        function Bindgrid()
        {
            if ($("#dropdownType").val() == 1) {
                caserefno = "RefNo";
                trueorfalse = true;
                LitigationType="Notice"
            }
            else {
                caserefno = "CaseRefNo";
                trueorfalse = false;
                LitigationType="Case"
            }
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
                         
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {                       
                        <%--     read: '<% =Path%>Litigation/MyCaseWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData").val() + '&FY=0&NoticeStatus=' + $("#dropdownStatus").val() + '&CaseType=' + $("#dropdownCaseType").val() + '&SD=&LastDate=&DisputedAmount=' + $("#dropdownDisputedAmount").val(),
                  --%>
                        read: {
                            url: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=All&StatusFlag=' + $("#dropdownType").val() + '&FY=0&StatusType=' + $("#dropdownStatus").val() + '&CourtId=&CategoryID=&StartDateDetail=&EndDateDetail=&CY=' + $("#dropdownCalYear").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=All&StatusFlag=' + $("#dropdownType").val() + '&FY=0&StatusType=' + $("#dropdownStatus").val() + '&CourtId=&CategoryID=&StartDateDetail=&EndDateDetail=&CY=' + $("#dropdownCalYear").val() + '&WLResult=-1&AssingnedLawyer=-1',
                    },
                    schema: {
                        data: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBoundAdvanced,
                columns: [
                    {
                        title: "Sr",
                        template: "#= ++record #",
                        width: 35
                    },
                    {
                        field: "BranchName", title: 'Location',
                        width: "17.7%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        width:"12%",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "17%",
                    },
                    {
                        field: "Title", title: 'Title',
                        type: "string",
                        width: "16%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseStage", title: 'Stage',
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden:true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },/* {field:"FYName",title:"Financial Year"},*/
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit"},
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                        ], title: "Action", lock: true, width: 150,
                    }
                ]
            });

            function OnGridDataBoundAdvanced(e) {

                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {

                    var currentUid = gridData[i].uid;
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (<% =flagDelVal%> == 0) {
                         var deleteButton = $(currentRow).find(".ob-delete");
                         deleteButton.hide();
                     }
                 }
             }

             $("#grid").kendoTooltip({
                 filter: "td:nth-child(2)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid").kendoTooltip({
                 filter: "td:nth-child(3)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid").kendoTooltip({
                 filter: "td:nth-child(4)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid").kendoTooltip({
                 filter: "td:nth-child(5)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid").kendoTooltip({
                 filter: "td:nth-child(6)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid").kendoTooltip({
                 filter: ".k-grid-edit",
                 content: function (e) {
                     return "Edit Case Details";
                 }
            });
             $("#grid").kendoTooltip({
                 filter: ".k-grid-delete",
                 content: function (e) {
                     return "delete Case";
                 }
             });


            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog').modal('show');
                    $('#showdetails').attr('width', '100%');
                    $('#showdetails').attr('height', '550px');
                    $('.modal-dialog').css('width', '100%');

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);
                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);
                    }
                }
                return true;
            });

        }


        var trueorfalse = false;
       function BindgridPopup()
         {

             if ($("#dropdownType1").val() == 1) {
                 caserefno = "RefNo";
                 trueorfalse = true;
                 LitigationType = "Notice";
             }
             else {
                 caserefno = "CaseRefNo";
                 trueorfalse = false;
                 LitigationType = "Case";
             }

             var setStartDate = '';
             var setEndDate = '';

             //var list4 = $("#dropdownLawyers").data("kendoDropDownTree")._values;
             //var Lawyerdetails = [];
             //if (list4.length > 0) {
             //    $.each(list4, function (i, v) {
             //        Lawyerdetails.push(v);
             //    });
             //}

             if ($("#Startdatepicker").val()!=undefined) {
                 //setStartDate = kendo.toString($("#Startdatepicker").val(), "dd/MM/yyyy");
                 setStartDate = $("#Startdatepicker").val();
             }
             if ($("#Lastdatepicker").val()!=undefined) {
                 //setEndDate = kendo.toString($("#Lastdatepicker").val(), "dd/MM/yyyy");
                 setEndDate = $("#Lastdatepicker").val();
             }   

            var grid = $('#grid1').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid1').empty();
                         
            var grid = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData1").val() + '&StatusFlag=' + $("#dropdownType1").val() + '&FY=0&StatusType=' + $("#dropdownStatus1").val() + '&StartDateDetail=' + setStartDate + '&EndDateDetail=' + setEndDate + '&CY=' + $("#dropdownCalYear1").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData1").val() + '&StatusFlag=' + $("#dropdownType1").val() + '&FY=0&StatusType=' + $("#dropdownStatus1").val() + '&StartDateDetail=' + setStartDate + '&EndDateDetail=' + setEndDate + '&CY=' + $("#dropdownCalYear1").val() + '&WLResult=-1&AssingnedLawyer=-1',
                    },
                    schema: {
                        data: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBoundAdvanced,
                columns: [
                    {
                        title: "Sr",
                        template: "#= ++record #",
                        width: 35
                    },
                    {
                        field: "BranchName", title: 'Location',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseStage", title: 'Stage',
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                              { name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit1" },
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete1" }
                        ], title: "Action", lock: true,// width: 150,
                    }
                ]
            });
           function OnGridDataBoundAdvanced(e) {

               var grid = $("#grid1").data("kendoGrid");
               var gridData = grid.dataSource.view();
               for (var i = 0; i < gridData.length; i++) {

                   var currentUid = gridData[i].uid;
                   var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                   if (<% =flagDelVal%> == 0) {
                       var deleteButton = $(currentRow).find(".ob-delete1");
                       deleteButton.hide();
                   }
               }
           }
         
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(2)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(3)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(4)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(5)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: "td:nth-child(6)", //this filter selects the second column's cells
                 position: "down",
                 content: function (e) {
                     var content = e.target.context.textContent;
                     return content;
                 }
             }).data("kendoTooltip");
             $("#grid1").kendoTooltip({
                 filter: ".k-grid-edit",
                 content: function (e) {
                     return "Edit Case Details";
                 }
             });
             $("#grid1").kendoTooltip({
                 filter: ".k-grid-delete",
                 content: function (e) {
                     return "delete Case";
                 }
             });


           
             $(document).on("click", "#grid1 tbody tr .ob-edit1", function (e) {
                 var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                 if (item.TypeCaseNotice != '') {
                     $('#divShowDialog').modal('show');
                     $('#showdetails').attr('width', '100%');
                     $('#showdetails').attr('height', '550px');
                     $('.modal-dialog').css('width', '100%');

                     if (item.TypeCaseNotice == 'C') {
                         $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);

                     }
                     else if (item.TypeCaseNotice == 'N') {
                          $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID);
                     }
                 }
                 return true;
             });
        }

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace');


            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: '<% =Path%>Litigation/Delete_CaseNotice_WorkSpace?UserID=<% =UId%>&CaseInstanceID=' + item.NoticeCaseInstanceID + '&Type=' + item.TypeCaseNotice,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#grid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });
            
          
            $(document).on("click", "#grid1 tbody tr .ob-delete1", function (e)
            {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: '<% =Path%>Litigation/Delete_CaseNotice_WorkSpace?UserID=<% =UId%>&CaseInstanceID=' + item.CaseInstanceID + '&Type=' + item.TypeCaseNotice,
                        dataType: "json",
                          beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            grid = $("#grid1").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });


        });

        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });
            $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

     
        function exportReportMain(e) {
            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });

            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;

            $.ajax({
                type: "GET",
                url: '' + PathName + '//LitigationExportReport/NoticeCaseWorkSpaceReport',
                data: {
                    UserId: UId,
                    CustomerID: customerId,
                    FlagIsApp: FlagIsApp,
                    MonthId: 'All',
                    StatusFlag: $("#dropdownType").val(),
                    FY: $("#dropdownFY").val(),
                    StatusType: $("#dropdownStatus").val(),
                    StartDateDetail: '',
                    EndDateDetail: '',
                    Dept: JSON.stringify(Deptdetails),
                    NoticeType: JSON.stringify(NoticeTypedetails),
                    location: JSON.stringify(locationsdetails),
                    Category: JSON.stringify(CategoryID),
                    CY: $("#dropdownCalYear").val(),
                    CourtID: "-1",
                    Oppnent: "-1",
                    Lawyers: "-1",
                    Result: "-1",
                    AllSearch: ""
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function exportReportAdvanced(e) {

            //location details
            var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept1").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType1").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory1").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });

            //CourtID details
            var Courtlist = $("#dropdownCourtWise").data("kendoDropDownTree")._values;
            var CourtDetailID = [];
            $.each(Courtlist, function (i, v) {
                CourtDetailID.push(v);
            });

            //Oppnent details
            var list4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;
            var Oppnentdetails = [];
            $.each(list4, function (i, v) {
                Oppnentdetails.push(v);
            });

            //Lawyer details
            var list4 = $("#dropdownLawyers").data("kendoDropDownTree")._values;
            var Lawyerdetails = [];
            $.each(list4, function (i, v) {
                Lawyerdetails.push(v);
            });

            var WlResult = $("#dropdownWinLose").val();
            if (WlResult == "") {
                WlResult = -1;
            }
            else {
                WlResult = $("#dropdownWinLose").val();
            }


            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;
            var FilterForAll = document.getElementById('txtSearch').value;


            $.ajax({
                type: "GET",
                url: '' + PathName + '//LitigationExportReport/NoticeCaseWorkSpaceReport',
                data: {
                    UserId: UId,
                    CustomerID: customerId,
                    FlagIsApp: FlagIsApp,
                    MonthId: $("#dropdownPastData1").val(),
                    StatusFlag: $("#dropdownType1").val(),
                    FY: $("#dropdownFY1").val(),
                    StatusType: $("#dropdownStatus1").val(),
                    StartDateDetail: $("#Startdatepicker").val(),
                    EndDateDetail: $("#Lastdatepicker").val(),
                    Dept: JSON.stringify(Deptdetails),
                    NoticeType: JSON.stringify(NoticeTypedetails),
                    location: JSON.stringify(locationsdetails),
                    Category: JSON.stringify(CategoryID),
                    CY: $("#dropdownCalYear1").val(),
                    CourtID: JSON.stringify(CourtDetailID),
                    Oppnent: JSON.stringify(Oppnentdetails),
                    Lawyers: JSON.stringify(Lawyerdetails),
                    Result: WlResult,
                    AllSearch: FilterForAll,
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function OpenAddNewNotice(e) {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '550px');
                $('.modal-dialog').css('width', '100%');

            if ($("#dropdownType").val()=="2") {
                    $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0");

                }
            else if ($("#dropdownType").val() == '1') {
                    $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=0");
                }
            e.preventDefault();
            return false;
        }

        function ClosePopNoticeDetialPage() 
        { 
            $('#divShowDialog').modal('hide');
            $('#showdetails').attr('src', "../../Common/blank.html");
            //Bindgrid();
            //FilterAll();
     
            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();
           
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');            
            fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
            fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
            fCreateStoryBoard('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
            fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
          
            //e.preventDefault();
        }

        function ApplyBtnAdvancedFilter(e) {
            BindgridPopup();
            FilterAllAdvanced();
        }
        function ApplyBtnMainFilter(e) {
            Bindgrid();
            FilterAll();
            e.preventDefault();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="col-lg-8 col-md-8">
                <input id="Path" type="hidden" value="<% =Path%>" />
                <input id="CustomerId" type="hidden" value="<% =CustId%>" />
                <input id="UId" type="hidden" value="<% =UId%>" />
                <input id="FlagDetail" type="hidden" value="<% =FlagIsApp%>" />
            </div>
           
            </div>
    </div>
      <div class="row" style="padding-bottom: 2px;margin-left: 10px;padding-top: 2px;">
           <input id="dropdownType" data-placeholder="Type"  style="width:169px;"/>   
          <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 289px;"/> 
          <input id="dropdownDept" data-placeholder="Type" style="width:246px;"/>
           <input id="dropdownFY" data-placeholder="Financial Year" style="width:177px;text-align:center;"/>  
           <input id="dropdownCalYear" data-placeholder="Calendar Year" style="width:195px;text-align:center;"/>  
           </div>
         
         
            <div class="row" style="margin-right: 13px;margin-top: 2px;">
            <div class="toolbar"> 
                <input id="dropdownStatus" data-placeholder="Status" style="width:168px;margin-left: 11px;"/>
                     <input id="dropdownCategory" data-placeholder="Category Of Case" style="width: 289px;"/>  
                      <input id="NoticeType" data-placeholder="Risk" style="width: 246px;margin-right: 3px;"/>
                      <button id="exportReport" onclick="exportReportMain(event)"  class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png);background-repeat: no-repeat;width:35px;height:30px;background-color:white;border: none;margin-left: -3px;"></button>        
                      <button id="ApplyBtnMain" style="height: 31px;width: 100px;" onclick="ApplyBtnMainFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                      <button id="AdavanceSearch" style="height: 32px;width: 153px;margin-right: 0.8px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
                     <button id="AddNewNotice" Class="btn btn-primary" style="float: right;width: 75px;margin-left: 2px;" runat="server" onclick="OpenAddNewNotice(event)">
              <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</button> 
                     </div>
            </div>

            <div class="row" style="margin-left: 10px;margin-top:2px">
                    <div class="col-md-12 colpadding0">
                           <button id="ClearfilterMain" style="float: right;height: 26px;margin-top: 3px;width: 115px;margin-right: 11px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                      </div>
                    </div>
             
         
        <div class="row" style="padding-bottom: 2px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboard">&nbsp;&nbsp;</div>
        <div class="row" style="padding-bottom: 2px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardDept">&nbsp;&nbsp;</div>
        <div class="row" style="padding-bottom: 2px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardNoticeType">&nbsp;&nbsp;</div>
        <div class="row" style="padding-bottom: 2px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryCategoryboard">&nbsp;&nbsp;</div>
      
       
           <div class="row" style="padding-top: 2px;">
        <div id="grid" style="border: none;margin-left: 10px;margin-right: 10px;margin-top: 5px;"></div>
    </div>

           <div id="divAdvanceSearchModel" style="padding-top: 5px; z-index: 999">
               
               <div class="row" style="margin-left: 10px; margin-top:10px">
                    <div class="col-md-12 colpadding0">
                            <input id="dropdownType1" data-placeholder="Type" style="width: 15%;margin-left: -4px;"/>    
                            <input id="dropdownStatus1" data-placeholder="Status" style="width: 13.5%;"/>
                            <input id="dropdownWinLose" style="width: 20%;"/>
                            <input id="NoticeType1" data-placeholder="Calendar Year" style="width: 15%;"/> 
                                 <input id="dropdownFY1" data-placeholder="Finance Year" style="width: 15%;"/>
                              <input id="dropdownPastData1" style="width: 19.8%;margin-left: 1px;" />
                        </div>
                   </div>
               <div class="row" style="margin-left: 10px;margin-top:5px" >
                    <div class="col-md-12 colpadding0">
                              <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 28.5%;margin-left:-3px;" />
                         <input id="dropdownDept1" data-placeholder="Department" style="width: 20%;"/>
                               <input id="dropdownOpponent" style="width: 15%;"/>
                             <input id="dropdownCalYear1" style="width: 15.3%;"/>
                                <input id="Startdatepicker" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 10%;"/>
                             <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 9.5%;"/>
                        </div>
                   </div>
               <div class="row" style="margin-left: 10px;margin-top:5px">
                    <div class="col-md-12 colpadding0">
                            <input id="dropdownCourtWise" style="width: 28.5%;margin-left:-3px"/>
                            <input id="dropdownLawyers" style="width: 20%;"/>    
                            <input id="txtSearch" type="text" style="width: 15%;" onkeydown="return (event.keyCode!=13);" class="k-textbox" placeholder="Type to Search " />
                            <input id="dropdownCategory1" data-placeholder="Category Of Case" style="width: 15.5%;"/>  
                            <button id="exportAdvanced" onclick="exportReportAdvanced(event)"  class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:39px; height:30px; background-color:white;border: none;"></button>        
                            <button id="ApplyBtnAdvanced" style="height: 23px;width: 76px;" onclick="ApplyBtnAdvancedFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                            <button id="Clearfilter" style="float: right;height: 23px;width: 90px;margin-right: 8px;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                    </div>
                   </div>
           
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboard1">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardNoticeType1">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardDept1">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryCategoryboard1">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryCourtboard">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardOpponent">&nbsp;&nbsp;</div>
                <div class="row" style="padding-top: 4px;margin-left: 10px;font-size: 12px;display:none;color: #535b6a;" id="filtersstoryboardLawyer">&nbsp;&nbsp;</div>
               
                <div class="row" style="padding-top: 8px;">
                <div id="grid1" style="border: none;margin-left: 7px;margin-right: 7px;"></div>
                </div>
    </div>
 
           <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Case Details</label>
                  <button id="btnAddEditcase" type="button" class="close" data-dismiss="modal"  aria-hidden="true"  onclick="ClosePopNoticeDetialPage();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>



