﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class TaskList : System.Web.UI.Page
    {
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                flag = false;
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "Open")
                    {
                        ViewState["Status"] = 1;

                        if (ddlTaskStatus.Items.FindByValue("1") != null)
                        {
                            ddlTaskStatus.ClearSelection();
                            ddlTaskStatus.Items.FindByValue("1").Selected = true;
                        }
                    }

                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["Status"] = 3;

                        if (ddlTaskStatus.Items.FindByValue("3") != null)
                        {
                            ddlTaskStatus.ClearSelection();
                            ddlTaskStatus.Items.FindByValue("3").Selected = true;
                        }
                    }
                }

                BindGrid(); bindPageNumber();
            }
        }

        public void BindGrid()
        {
            try
            {
                int priorityID = -1;
                int partyID = -1;
                int deptID = -1;
                int taskStatus = -1;
                string taskType = string.Empty;

                if (!string.IsNullOrEmpty(ddlTaskStatus.SelectedValue))
                {
                    taskStatus = Convert.ToInt32(ddlTaskStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlTaskTypePage.SelectedValue))
                {
                    taskType = ddlTaskTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                {
                    if (ddlTaskPriority.SelectedValue != "0")
                    {
                        priorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }
                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, taskStatus, taskType, AuthenticationHelper.CustomerID);

               // lstTaskDetails = lstTaskDetails.Where(entry => entry.NoticeCaseInstanceID == 0).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                {
                    lstTaskDetails = lstTaskDetails.Where(entry => entry.TaskTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.TaskDesc.Contains((tbxtypeTofilter.Text).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstTaskDetails = lstTaskDetails.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstTaskDetails = lstTaskDetails.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstTaskDetails.Count > 0)
                {
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
                else
                {
                    grdTaskActivity.DataSource = null;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }

                lstTaskDetails.Clear();
                lstTaskDetails = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    int taskID = Convert.ToInt32(commandArgs[0]);
                    int noticeCaseInstanceID = Convert.ToInt32(commandArgs[1]);

                    if (taskID != 0)//&& noticeCaseInstanceID != null
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + taskID + "," + noticeCaseInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid(); bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdTaskActivity.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue == "1")
                        Response.Redirect("~/Litigation/aspxPages/NoticeList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "2")
                        Response.Redirect("~/Litigation/aspxPages/CaseList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "3")
                        Response.Redirect("~/Litigation/aspxPages/TaskList.aspx", false);

                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Page Number-Bottom

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());

            grdTaskActivity.PageIndex = chkSelectedPage - 1;
            grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid(); ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        protected void grdTaskActivity_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int priorityID = -1;
                int partyID = -1;
                int deptID = -1;
                int taskStatus = -1;
                string taskType = string.Empty;

                if (!string.IsNullOrEmpty(ddlTaskStatus.SelectedValue))
                {
                    taskStatus = Convert.ToInt32(ddlTaskStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlTaskTypePage.SelectedValue))
                {
                    taskType = ddlTaskTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                {
                    if (ddlTaskPriority.SelectedValue != "0")
                    {
                        priorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, taskStatus, taskType, AuthenticationHelper.CustomerID);
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                {
                    lstTaskDetails = lstTaskDetails.Where(entry => entry.TaskTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.TaskDesc.Contains((tbxtypeTofilter.Text).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    lstTaskDetails = lstTaskDetails.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    ViewState["Direction"] = "Ascending";
                }
                else
                {
                    lstTaskDetails = lstTaskDetails.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    ViewState["Direction"] = "Descending";
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdTaskActivity.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdTaskActivity.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (lstTaskDetails.Count > 0)
                {
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
                else
                {
                    grdTaskActivity.DataSource = null;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void lnkAdd_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptAddTask", "ShowAddTaskModel(" + 0 + "," + 0 + ");", true);
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });


                    // string TypeValue = commandArgs[2];

                    if (e.CommandName.Equals("DELETE_Task"))
                    {
                        int taskID = Convert.ToInt32(commandArgs[0]);
                        int NoticeCaseInstanceID = Convert.ToInt32(commandArgs[1]);
                        DeleteTask(taskID, AuthenticationHelper.CustomerID);
                        BindGrid();
                    }
                    else if (e.CommandName.Equals("Edit_Task"))
                    {
                        int taskID = Convert.ToInt32(commandArgs[0]);
                        int NoticeCaseInstanceID = Convert.ToInt32(commandArgs[1]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptAddTask1", "ShowAddTaskModel(" + NoticeCaseInstanceID + "," + taskID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void DeleteTask(int taskID, long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        LitigationManagement.CreateAuditLog("T", 0, "tbl_TaskScheduleOn", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Deleted", true);
                        cvTaskPage.IsValid = false;
                        cvTaskPage.ErrorMessage = "Task Detail Deleted Successfully.";
                        BindGrid(); ShowGridDetail();
                    }
                    else
                    {
                        cvTaskPage.IsValid = false;
                        cvTaskPage.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskPage.IsValid = false;
                cvTaskPage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCreatedBy = (e.Row.FindControl("lblCreatedBy") as Label);
                Label lblNoticeCaseInstanceID = (e.Row.FindControl("lblNoticeCaseInstanceID") as Label);
                LinkButton lnkEditTask = (e.Row.FindControl("lnkEditTask") as LinkButton);
                LinkButton lnkDeleteTask = (e.Row.FindControl("lnkDeleteTask") as LinkButton);
                if (!string.IsNullOrEmpty(lblCreatedBy.Text.Trim()))
                {
                    if (AuthenticationHelper.UserID == Convert.ToInt64(lblCreatedBy.Text.Trim()))
                    {
                        if ((lblNoticeCaseInstanceID.Text.Trim()) == "0")
                        {
                            lnkEditTask.Visible = true;
                            lnkDeleteTask.Visible = true;
                        }
                        else
                        {
                            lnkEditTask.Visible = false;
                            lnkDeleteTask.Visible = false;
                        }
                    }
                    else
                    {
                        lnkEditTask.Visible = false;
                        lnkDeleteTask.Visible = false;
                    }
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                int priorityID = -1;
                int partyID = -1;
                int deptID = -1;
                int taskStatus = -1;
                string taskType = string.Empty;

                if (!string.IsNullOrEmpty(ddlTaskStatus.SelectedValue))
                {
                    taskStatus = Convert.ToInt32(ddlTaskStatus.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlTaskTypePage.SelectedValue))
                {
                    taskType = ddlTaskTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                {
                    if (ddlTaskPriority.SelectedValue != "0")
                    {
                        priorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }
                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, taskStatus, taskType, AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text.Trim()))
                {
                    lstTaskDetails = lstTaskDetails.Where(entry => entry.TaskTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.TaskDesc.Contains((tbxtypeTofilter.Text).Trim())).ToList();
                }

                #region excel report
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    int Count = 0;
                    DataTable table = new DataTable();
                    table.Columns.Add("SrNo", typeof(string));
                    table.Columns.Add("NoticeOrCaseTitle", typeof(string));
                    table.Columns.Add("TaskType", typeof(string));
                    table.Columns.Add("TaskTitle", typeof(string));
                    table.Columns.Add("Priority", typeof(string));
                    table.Columns.Add("TaskDesc", typeof(string));
                    table.Columns.Add("Status", typeof(string));
                    table.Columns.Add("ScheduleOnDate", typeof(string));
                    table.Columns.Add("CreatedByText", typeof(string));
                    table.Columns.Add("AssignToName", typeof(string));
                    table.Columns.Add("Remark", typeof(string));

                    foreach (var item in lstTaskDetails)
                    {
                        ++Count;
                        table.Rows.Add(Count, item.NoticeOrCaseTitle, item.TaskType, item.TaskTitle, item.Priority, item.TaskDesc,item.Status,item.ScheduleOnDate,item.CreatedByText,item.AssignToName,item.Remark);
                    }

                    ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Task Report");
                    DataView view = new System.Data.DataView(table);
                    DataTable ExcelData = null;
                    ExcelData = view.ToTable("Selected", false, "SrNo", "NoticeOrCaseTitle", "TaskType", "TaskTitle", "Priority", "TaskDesc", "Status", "ScheduleOnDate", "CreatedByText", "AssignToName", "Remark");

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(item["ScheduleOnDate"])))
                        {
                            item["ScheduleOnDate"] = Convert.ToDateTime(item["ScheduleOnDate"]).ToString("dd-MMM-yyyy");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(item["TaskType"])))
                        {
                            if (Convert.ToString(item["TaskType"])=="N")
                            {
                                item["TaskType"] = "Notice";
                            }
                            if (Convert.ToString(item["TaskType"]) == "C")
                            {
                                item["TaskType"] = "Case";
                            }
                            if (Convert.ToString(item["TaskType"]) == "T")
                            {
                                item["TaskType"] = "Individual Task";
                            }
                        }
                    }

                    #region Lawyer Performance Report
                    exWorkSheet4.Cells["A1"].LoadFromDataTable(ExcelData, true);
                    exWorkSheet4.Cells["A1"].Value = "Sr No.";
                    exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["A1"].AutoFitColumns(10);
                    exWorkSheet4.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["B1"].Value = "Notice/Case Title";
                    exWorkSheet4.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["B1"].AutoFitColumns(30);
                    exWorkSheet4.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["C1"].Value = "Task Type";
                    exWorkSheet4.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["C1"].AutoFitColumns(20);
                    exWorkSheet4.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["D1"].AutoFitColumns(40);
                    exWorkSheet4.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["D1"].Value = "Task Title";
                    exWorkSheet4.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["E1"].Value = "Priority";
                    exWorkSheet4.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["E1"].AutoFitColumns(15);
                    exWorkSheet4.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["F1"].Value = "TaskDesc";
                    exWorkSheet4.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["F1"].AutoFitColumns(50);
                    exWorkSheet4.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                                                                                                                     //Rating
                    exWorkSheet4.Cells["G1"].Value = "Status";
                    exWorkSheet4.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["G1"].AutoFitColumns(10);
                    exWorkSheet4.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["H1"].Value = "ScheduleOn Date";
                    exWorkSheet4.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["H1"].AutoFitColumns(10);
                    exWorkSheet4.Cells["H1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["H1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["I1"].Value = "Createdby";
                    exWorkSheet4.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["I1"].AutoFitColumns(20);
                    exWorkSheet4.Cells["I1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["I1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["J1"].Value = "Assign To";
                    exWorkSheet4.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["J1"].AutoFitColumns(20);
                    exWorkSheet4.Cells["J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["J1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["J1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet4.Cells["K1"].Value = "Remark";
                    exWorkSheet4.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet4.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet4.Cells["K1"].AutoFitColumns(15);
                    exWorkSheet4.Cells["K1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet4.Cells["K1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet4.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet4.Cells["K1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    #endregion

                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                    using (ExcelRange col = exWorkSheet4.Cells[1, 1, count, 11])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string ReportName = "TaskReport-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + ReportName);//locatioName
                                                                                                                    //Response.AddHeader("content-disposition", "attachment;filename=MIS_Report.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}