﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class CaseListNew : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        protected static int StatusFlagID;
        protected static string CNTtype;
        protected int flagDelVal;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = AuthenticationHelper.Role;
            StatusFlagID = -1;
            CNTtype = "1";
            flagDelVal = 1;

            Page.Header.DataBind();

            if (!String.IsNullOrEmpty(Request.QueryString["CT"]))
            {
                if (Request.QueryString["CT"].Equals("N"))
                {
                    CNTtype = "1";
                }
                if (Request.QueryString["CT"].Equals("C"))
                {
                    CNTtype = "2";
                }
                if (Request.QueryString["CT"].Equals("T"))
                {
                    CNTtype = "3";
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
            {
                string InputFilter = Request.QueryString["Status"];
                if (InputFilter.Equals("Open"))
                {
                    StatusFlagID = 1;
                }
                else if (InputFilter.Equals("Closed"))
                {
                    StatusFlagID = 3;
                }
                else
                {
                    StatusFlagID = -1;
                }
            }
            if (AuthenticationHelper.Role.Equals("CADMN"))
            {
                flagDelVal = 1;
            }
            else
            {
                flagDelVal = 0;
            }

        }
    }
}