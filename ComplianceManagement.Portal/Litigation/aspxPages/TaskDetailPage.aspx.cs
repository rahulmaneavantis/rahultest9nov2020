﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class TaskDetailPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["NID"]))
                    {
                        var taskID = Request.QueryString["TaskID"];
                        var NID = Request.QueryString["NID"];

                        if (taskID != "" && NID != "")
                        {
                            ViewState["TaskID"] = taskID;
                            ViewState["NoticeCaseInstanceID"] = NID;
                            btnCloseTask.Visible = false;
                            showTaskDetail(Convert.ToInt32(taskID), Convert.ToInt32(NID));

                            //Re-Bind Responses Log Details
                            //BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }
                }
            }

            //Show Hide Grid Control - Enable/Disable Form Controls
            if (ViewState["taskStatus"] != null)
            {
                if (Convert.ToInt32(ViewState["taskStatus"]) == 3)
                    enableDisableTaskControls(false);
                else
                    enableDisableTaskControls(true);
            }
            else
                enableDisableTaskControls(true);
            if (!string.IsNullOrEmpty(Request.QueryString["TaskSta"]))
            {
                var taskStatus = Request.QueryString["TaskSta"];
                if (taskStatus == "3")
                {
                    enableDisableTaskControls(false);
                    btnMoreDetails.Visible = false;
                }
            }
        }

        public void BindTaskResponses(int taskID)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                grdTaskResponseLog.DataSource = lstTaskResponses;
                grdTaskResponseLog.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void clearTaskResponseControls()
        {
            try
            {
                tbxTaskResDesc.Text = "";
                tbxTaskResRemark.Text = "";

                fuTaskResponseDocUpload.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                pnlTaskDetail.Enabled = flag;
                pnlTaskResponse.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    bool validateData = false;
                    bool saveSuccess = false;
                    long newResponseID = 0;
                    long taskID = 0;
                    taskID = Convert.ToInt32(ViewState["TaskID"]);

                    int userID = 0;

                    if (taskID != 0)
                    {
                        userID = AuthenticationHelper.UserID;

                        if (userID != 0)
                        {
                            if (tbxTaskResDesc.Text != "")
                            {
                                validateData = true;
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Provide Response Description.";
                                return;
                            }
                        }
                    }

                    if (validateData)
                    {
                        tbl_TaskResponse newRecord = new tbl_TaskResponse()
                        {
                            IsActive = true,
                            TaskID = taskID,
                            ResponseDate = DateTime.Now,
                            Description = tbxTaskResDesc.Text,
                            CreatedBy = userID,
                            UserID = userID
                        };

                        if (tbxTaskResRemark.Text != "")
                            newRecord.Remark = tbxTaskResRemark.Text;

                        newResponseID = LitigationTaskManagement.CreateTaskResponseLog(newRecord);

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    if (saveSuccess)
                    {
                        //Save Notice Response Uploaded Documents
                        #region Upload Document

                        if (fuTaskResponseDocUpload.HasFiles)
                        {
                            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = taskID,
                                DocTypeInstanceID = newResponseID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = "TR"
                            };

                            HttpFileCollection fileCollection1 = Request.Files;

                            if (fileCollection1.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = 0;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                string directoryPath1 = "";

                                if (newResponseID > 0)
                                {
                                    directoryPath1 = Server.MapPath("~/LitigationDocuments/" + customerID + "/TaskResponseDocs/" + Convert.ToInt32(newResponseID));

                                    DocumentManagement.CreateDirectory(directoryPath1);

                                    for (int i = 0; i < fileCollection1.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection1[i];
                                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                                        String fileName1 = "";
                                        if (keys1[keys1.Count() - 1].Equals("fuTaskResponseDocUpload"))
                                        {
                                            fileName1 = uploadedFile.FileName;
                                        }
                                        Guid fileKey1 = Guid.NewGuid();
                                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                        Stream fs = uploadedFile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32) fs.Length);
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            objNoticeDoc.FileName = fileName1;
                                            objNoticeDoc.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objNoticeDoc.FileKey = fileKey1.ToString();
                                            objNoticeDoc.Version = "1.0";
                                            objNoticeDoc.VersionDate = DateTime.UtcNow;
                                            objNoticeDoc.CreatedOn = DateTime.Now.Date;
                                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                            if (!NoticeManagement.ExistsNoticeDocumentMapping(objNoticeDoc))
                                            {
                                                DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);
                                            }
                                        }
                                    }//End For Each                                    
                                }
                            }
                        }

                        #endregion                        

                        if (saveSuccess)
                        {
                            //Update Task Status to Submitted
                            saveSuccess = LitigationTaskManagement.UpdateTaskStatus(taskID, 2, userID, AuthenticationHelper.CustomerID); //Status 2 - Submitted

                            clearTaskResponseControls();

                            cvTaskResponse.IsValid = false;
                            cvTaskResponse.ErrorMessage = "Response Detail Save Successfully.";
                        }

                        //Re-Bind Responses Log Details
                        BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    grdTaskResponseLog.PageIndex = e.NewPageIndex;

                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        var lstTaskDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);


                        if (lstTaskDocument != null)
                        {
                            List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                            if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                entityData.Version = "1.0";
                                entityData.NoticeCaseInstanceID = taskID;
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in lstTaskDocument)
                                {
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        DocumentPath = FileName;

                                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                        lblMessage.Text = "";

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                    }
                                    break;
                                }
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["taskStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["taskStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskDocCount()
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    if (taskID != 0 || noticeCaseInstanceID != 0)
                    {
                        var docCount = LitigationTaskManagement.GetTaskDocumentsCount(taskID, noticeCaseInstanceID);

                        if (docCount == 0)
                            return "No Documents";
                        else if (docCount == 1)
                            return "1 File";
                        else if (docCount > 1)
                            return docCount + " Files";
                        else
                            return "";
                    }
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void showTaskDetail(int taskID, int noticeCaseInstanceID)
        {
            try
            {
                var taskDetail = LitigationTaskManagement.GetNoticeCaseTaskViewDetails(noticeCaseInstanceID, taskID);
                LblTaskType.Text = string.Empty;
                if (taskDetail != null)
                {
                    if (taskDetail.CreatedBy == AuthenticationHelper.UserID)
                    {
                        btnCloseTask.Visible = true;
                    }
                    lblNameTag.Visible = true;
                    if (string.IsNullOrEmpty(taskDetail.NoticeOrCaseTitle))
                    {
                        lblNameTag.Visible = false;
                    }
                    lblNoticeOrCaseTitle.Text = taskDetail.NoticeOrCaseTitle;
                    lblTaskTitle.Text = taskDetail.TaskTitle;
                    lblTaskDueDate.Text = taskDetail.ScheduleOnDate.ToString("dd-MM-yyyy");
                    lblAssignBy.Text = taskDetail.CreatedByText;
                    lblPriority.Text = taskDetail.Priority;
                    lblTaskDesc.Text = taskDetail.TaskDesc;
                    lblTaskRemark.Text = taskDetail.Remark;
                    LblTaskType.Text = taskDetail.TaskType;
                    var docCount = ShowTaskDocCount();

                    if (taskDetail.TaskType.Trim() == "T")
                    {
                        btnMoreDetails.Visible = false;
                    }
                    else
                    {
                        btnMoreDetails.Visible = true;
                    }
                    lblTaskDocuments.Text = docCount;

                    if (docCount != "No Documents")
                    {
                        lnkBtnTaskDocuments.Visible = true;
                        lblTaskDocView.Visible = true;
                    }
                    else
                    {
                        lnkBtnTaskDocuments.Visible = false;
                        lblTaskDocView.Visible = false;
                    }

                    ViewState["taskStatus"] = Convert.ToInt32(taskDetail.StatusID);

                    //Bind Task Responses Log Details
                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnDownloadTaskDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                    if (lstTaskDocument.Count > 0)
                    {
                        using (ZipFile responseDocZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in lstTaskDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    //string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];

                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            responseDocZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                        if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (AllinOneDocumentList.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            DocumentPath = FileName;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            lblMessage.Text = "";
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void lblTaskDocView_Click(object sender, EventArgs e)
        {
            if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
            {
                long taskID = Convert.ToInt32(ViewState["TaskID"]);
                long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                if (lstTaskDocument != null)
                {
                    List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                    if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                        entityData.Version = "1.0";
                        entityData.NoticeCaseInstanceID = noticeCaseInstanceID;
                        entitiesData.Add(entityData);
                    }

                    if (entitiesData.Count > 0)
                    {
                        foreach (var file in lstTaskDocument)
                        {
                            rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptDocmentVersionView.DataBind();
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;

                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                lblMessage.Text = "";

                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                            break;
                        }
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                    }
                }
            }
        }

        protected void btnCloseTask_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
            {
                long TaskID = Convert.ToInt64(Request.QueryString["TaskID"]);
                long CustomerID = AuthenticationHelper.CustomerID;
                UpdateTaskStatus(TaskID, 3, CustomerID);
                enableDisableTaskControls(false);
            }
        }

        public static bool UpdateTaskStatus(long taskID, int statusID, long CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID && x.CustomerID == CustomerID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.StatusID = statusID;
                        taskToUpdate.UpdatedBy = AuthenticationHelper.UserID;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        if (statusID == 3)
                            taskToUpdate.URLExpired = true;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void btnMoreDetails_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["NID"]))
            {
                if (!string.IsNullOrEmpty(LblTaskType.Text.Trim()))
                {
                    if (LblTaskType.Text.Trim() == "N")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + ",'N');", true);
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "OpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + "," +"Notice"+  ");", true);
                    }
                    if (LblTaskType.Text.Trim() == "C")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + ",'C');", true);
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "OpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + "," + "Case" + ");", true);
                    }
                }
            }
        }
    }
}