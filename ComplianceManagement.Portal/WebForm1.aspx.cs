﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class WebForm1 : System.Web.UI.Page, System.Web.UI.ICallbackEventHandler
    {
        public string GetCallbackResult()
        {
            return CallBackData;
        }
        string CallBackData;
        public void RaiseCallbackEvent(string eventArgument)
        {
            switch (eventArgument)
            {
                case "aa":
                    CallBackData = "A";
                    break;
                default:
                    CallBackData = "aA";
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string callbackeve = Page.ClientScript.GetCallbackEventReference(this, "evtArgs", "GetCallFromServer", "");
            string clientScript = "function CallServer(evtArgs){" + callbackeve + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer", clientScript, true);
            }

        //string CallBackData;
        //public string GetCallBackResult()
        //{
        //    throw new NotImplementedException();
        //}
        //public void RaisCallBackEvent(string eventArgument)
        //{
        //    switch (eventArgument)
        //    {
        //        case "aa":
        //            CallBackData = "A";
        //            break;
        //        default:
        //            CallBackData = "aA";
        //            break;
        //    }
        //}
    }
   
}