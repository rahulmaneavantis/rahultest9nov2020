﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class Compliance : System.Web.UI.MasterPage
    {
        protected string LastLoginDate;
        protected string CustomerName;
        protected bool showCADMNTOManagement;
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
                
        //        Page.Header.DataBind();
        //        if (Session["LastLoginTime"] != null)
        //        {
        //            LastLoginDate = Session["LastLoginTime"].ToString();
        //        }

        //        if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT"))
        //        {
        //            if (AuthenticationHelper.UserID != -1)
        //                CustomerName = CustomerManagement.GetByID(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)).Name;
        //        }
                

        //        // added by sudarshan for menu bar
        //        if (Session["ChangePassword"] != null)
        //        {
        //            if (Convert.ToBoolean(Session["ChangePassword"]) != true)
        //            {
        //                if (AuthenticationHelper.Role.Equals("SADMN"))
        //                {                          
        //                    CMPServicesSiteMap.SiteMapProvider = "SuperAdminProvoider";
        //                }
        //                else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.IComplilanceApplicable.Equals("1"))
        //                {
        //                    CMPServicesSiteMap.SiteMapProvider = "AdminInternalProvoider";
        //                }
        //                else if (AuthenticationHelper.Role.Equals("CADMN"))
        //                {
        //                    CMPServicesSiteMap.SiteMapProvider = "AdminProvoider";
        //                }
        //                else if (AuthenticationHelper.Role.Equals("MGMT"))
        //                {
        //                    CMPServicesSiteMap.SiteMapProvider = "MangementProvoider";
        //                }
        //                else if (AuthenticationHelper.Role.Equals("IMPT"))
        //                {
        //                    CMPServicesSiteMap.SiteMapProvider = "CompanyAdminStructureProvoider";
        //                }
        //                else
        //                {
        //                    if (AuthenticationHelper.IComplilanceApplicable.Equals("1"))
        //                    {
        //                        CMPServicesSiteMap.SiteMapProvider = "NonAdminInternalProvoider";
        //                    }
        //                    else
        //                    {
        //                        CMPServicesSiteMap.SiteMapProvider = "NonAdminProvoider";
        //                    }
                            
        //                }
        //            }
        //            else
        //            {
        //                CMPServicesSiteMap.SiteMapProvider = null;
        //                CMPMenuBar.Visible = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Page.Header.DataBind();
                if (Session["LastLoginTime"] != null)
                {
                    LastLoginDate = Session["LastLoginTime"].ToString();
                }

                if (!AuthenticationHelper.Role.Equals("SADMN") && !AuthenticationHelper.Role.Equals("IMPT") && !AuthenticationHelper.Role.Equals("UPDT") && !AuthenticationHelper.Role.Equals("RPER") && !AuthenticationHelper.Role.Equals("RREV") && !AuthenticationHelper.Role.Equals("SPADM"))
                {
                    //if (AuthenticationHelper.UserID != -1)
                    //    CustomerName = CustomerManagement.GetByID(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)).Name;

                    if (AuthenticationHelper.UserID != -1)
                        CustomerName = CustomerManagement.GetByID(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)).Name;
                }
                showCADMNTOManagement = UserManagement.CheckIsServiceProviderID(AuthenticationHelper.UserID);
                // added by sudarshan for menu bar
                if (Session["ChangePassword"] != null)
                {
                    if (Convert.ToBoolean(Session["ChangePassword"]) != true)
                    {
                        if (AuthenticationHelper.Role.Equals("SADMN"))
                        {
                            if (AuthenticationHelper.Role.Equals("SADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "SuperAdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("SADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//Audit
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AuditAdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("SADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl
                            {
                                CMPServicesSiteMap.SiteMapProvider = "InternalControlAdminProvoider";
                            }
                        }                       
                        else if (AuthenticationHelper.Role.Equals("IMPT"))
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var CityList = (from row in entities.QuestionReviewers
                                                where row.Userid == AuthenticationHelper.UserID
                                                select row).FirstOrDefault();


                                if (CityList != null)
                                {
                                    CMPServicesSiteMap.SiteMapProvider = "ImplementationReviewerProvoider";
                                }
                                else
                                {

                                    if (AuthenticationHelper.Role.Equals("IMPT") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "CompanyAdminStructureProvoider";
                                    }
                                    else if (AuthenticationHelper.Role.Equals("IMPT") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//Audit
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "CompanyAdminStructureProvoider";
                                    }
                                    else if (AuthenticationHelper.Role.Equals("IMPT") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "CompanyAdminStructureProvoider";
                                    }

                                }
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("UPDT"))
                        {
                            if (AuthenticationHelper.Role.Equals("UPDT") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "LegalUpdateProvoider";
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("RPER"))
                        {
                            if (AuthenticationHelper.Role.Equals("RPER") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "ResearchPerformerProvoider";
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("RREV"))
                        {
                            if (AuthenticationHelper.Role.Equals("RREV") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "ResearchReviewerProvoider";
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("CADMN"))
                        {

                            if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.IComplilanceApplicable.Equals("1") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AdminInternalProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//Audit
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AuditAdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl
                            {
                                CMPServicesSiteMap.SiteMapProvider = "InternalControlAdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("R"))//Audit
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AuditAdminProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("CADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl
                            {
                                CMPServicesSiteMap.SiteMapProvider = "InternalControlAdminProvoider";
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("MGMT") && showCADMNTOManagement)
                        {
                            CMPServicesSiteMap.SiteMapProvider = "AdminProvoider";
                        }
                        else if (AuthenticationHelper.Role.Equals("MGMT"))
                        {
                            if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "MangementProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.ProductApplicableLogin.Equals("A"))//Audit
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AuditManagementProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("MGMT") && AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl
                            {
                                CMPServicesSiteMap.SiteMapProvider = "InternalControlManagementProvoider";
                            }
                        }
                        else if (AuthenticationHelper.Role.Equals("SPADM"))
                        {
                            if (AuthenticationHelper.Role.Equals("SPADM") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "ServiceProviderAdmin";
                            }                            
                        }
                        else if (AuthenticationHelper.Role.Equals("DADMN"))
                        {
                            if (AuthenticationHelper.Role.Equals("DADMN") && AuthenticationHelper.IComplilanceApplicable.Equals("1") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AdminInternalProvoider";
                            }
                            else if (AuthenticationHelper.Role.Equals("DADMN") && AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                CMPServicesSiteMap.SiteMapProvider = "AdminProvoider";
                            }
                        }
                        else
                        {
                            if (AuthenticationHelper.ProductApplicableLogin.Equals("C"))//compliance
                            {
                                if (AuthenticationHelper.IComplilanceApplicable.Equals("1"))
                                {
                                    CMPServicesSiteMap.SiteMapProvider = "NonAdminInternalProvoider";
                                }
                                else
                                {
                                    CMPServicesSiteMap.SiteMapProvider = "NonAdminProvoider";
                                }
                            }
                            else if (AuthenticationHelper.ProductApplicableLogin.Equals("A"))//Audit                            
                            {
                                var PersonResp = CustomerManagementRisk.GetPersonResponsibleid(AuthenticationHelper.UserID);
                                var DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                                var a = CustomerManagementRisk.GetAssignedRolesICFR(AuthenticationHelper.UserID);

                                if (PersonResp != 0 && DepartmentHead != null)  //Function Head and Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPersonRespProvider";
                                    }
                                }

                                else if (PersonResp != 0)  // Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditPersonRespProvider";
                                    }
                                }
                                else if (DepartmentHead != null) //Function Head
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadBothProvider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadPerformerProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadReviewerProvider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditFunctionHeadProvider";
                                    }
                                }
                                else
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminBothProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "AuditNonAdminPerformerProvoider";
                                    }
                                }
                            }
                            else if (AuthenticationHelper.ProductApplicableLogin.Equals("I"))//InternalControl                            
                            {
                                var PersonResp = CustomerManagementRisk.GetInternalPersonResponsibleid(AuthenticationHelper.UserID);
                                var AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                                var DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(AuthenticationHelper.UserID);
                                var a = CustomerManagementRisk.GetAssignedRolesARS(AuthenticationHelper.UserID);

                                if (AuditHeadOrManager != null)
                                {
                                    if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
                                    {
                                        if (a.Contains(2))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditMgrCompanyAdminProvoider";
                                        }
                                        else if (a.Contains(3) && a.Contains(4))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerReviewerProvoider";
                                        }
                                        else if (a.Contains(3))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerPerformerProvoider";
                                        }
                                        else if (a.Contains(4))
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerReviewerProvoider";
                                        }
                                        else
                                        {
                                            CMPServicesSiteMap.SiteMapProvider = "InternalAuditManagerProvider";
                                        }
                                    }
                                    else if (AuditHeadOrManager == "AH")
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditHeadProvoider";
                                    }
                                }
                                else if (DepartmentHead != null)//Departmet Head
                                {

                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadBothProvoider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadPerformerProvoider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalAuditDepartmentHeadProvoider";
                                    }
                                }
                                else if (PersonResp != 0)  // Person Responsible
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "IInternalContorlPersonResponsibleBothProvoider";
                                    }
                                    else if (a.Contains(3))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsiblePerformerProvoider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleReviewerProvoider";
                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalContorlPersonResponsibleProvoider";
                                    }
                                }

                                else
                                {
                                    if (a.Contains(3) && a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminBothProvider";
                                    }
                                    else if (a.Contains(4))
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminReviewerProvoider";

                                    }
                                    else
                                    {
                                        CMPServicesSiteMap.SiteMapProvider = "InternalControlNonAdminPerformerProvoider";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        CMPServicesSiteMap.SiteMapProvider = null;
                        CMPMenuBar.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtLogout_Click(object sender, EventArgs e)
        {
            try
            {

                //Session["ShowAlertForLogin"] = "false";
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();

                //added by sudarshan to login same user multiple time
                //if ((Convert.ToInt32(Application[AuthenticationHelper.UserID.ToString()]) - 1) == 0)
                //{
                //    Application[AuthenticationHelper.UserID.ToString()] = null;
                //}
                //else
                //{
                //    Application[AuthenticationHelper.UserID.ToString()] = Convert.ToInt32(Application[AuthenticationHelper.UserID.ToString()]) - 1;
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected override void OnPreRender(EventArgs e)
            {
                //Response.Headers.Add("X-Content-Type-Options", "nosniff");
                //Response.Headers.Add("X-XSS-Protection", "1");
                //base.OnPreRender(e);
            }
    }
}