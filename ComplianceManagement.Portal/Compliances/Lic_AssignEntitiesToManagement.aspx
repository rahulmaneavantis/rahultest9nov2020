﻿<%@ Page Title="Assign Entities" Language="C#" MasterPageFile="~/Compliance.Master"
     AutoEventWireup="true" CodeBehind="Lic_AssignEntitiesToManagement.aspx.cs"
     Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Lic_AssignEntitiesToManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div id="FilterLocationdiv" runat="server" style="float: left; margin-left: 210px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10;" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true"  Height="200px" Width="390px" 
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>
                <div runat="server" id="divFilterUsers" style="margin-left: 20px; margin-top: 5px; float: left;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div style="float: right; margin-top: 10px; margin-right: 15px">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddComplianceType" OnClick="btnAddComplianceType_Click" />
                </div>
                <br />
                <br />
                <br />

                <asp:GridView runat="server" ID="grdAssignEntities" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAssignEntities_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdAssignEntities_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdAssignEntities_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="License Type Name" SortExpression="LicenseType">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("LicenseType") %>' ToolTip='<%# Eval("LicenseType") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divAssignEntitiesDialog">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList ID="ddlUsers" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select User." ControlToValidate="ddlUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 150px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"   
                                        Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                                <asp:CompareValidator ID="CompareValidator2" ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                   License Type</label>
                                <asp:DropDownList runat="server" ID="ddlLicenseType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="false">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select license type." ControlToValidate="ddlLicenseType"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 256px; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignEntitiesDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 64px; width: 260px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#divAssignEntitiesDialog').dialog({
                height: 450,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Assign Entities to Management",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });

        function initializeCombobox() {
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
             $("#<%= ddlUsers.ClientID %>").combobox();
             $("#<%= ddlComplianceCatagory.ClientID %>").combobox();

         }


         function initializeJQueryUI(textBoxID, divID) {

             $("#" + textBoxID).unbind('click');

             $("#" + textBoxID).click(function () {
                 $("#" + divID).toggle("blind", null, 500, function () { });
             });
         }


    </script>
</asp:Content>
