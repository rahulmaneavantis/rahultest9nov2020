﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Check_List_Reports_Performer : System.Web.UI.Page
    {        
        public static string role;
        protected static string queryStringFlag = "";
        protected static bool IsNotCompiled;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    // STT Change- Add Status
                    IsNotCompiled = false;
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    long CustId = AuthenticationHelper.CustomerID;
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == CustId.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }
                    role = Request.QueryString["role"];  
                   
                    BindStatus();
                    BindTypes();
                    BindCategories();
                    BindActList();
                    BindLocationFilter();
                    queryStringFlag = "A";
                    FillData(role, queryStringFlag);

                    if (role == "Performer")
                    {
                        liPerformer.Text = "Performer";
                    }
                    else
                    {
                        liPerformer.Text = "Reviewer";
                    }

                    GetPageDisplaySummary();
                    if (tvFilterLocationPerformer.SelectedValue != "-1")
                        Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                    else
                        Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;

                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        private void BindLocationFilter()
        {
            try
            {

                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(AuthenticationHelper.CustomerID));
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.Role, "S");
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPerformer.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPerformer.Nodes.Add(node);
                }
                tvFilterLocationPerformer.CollapseAll();
                divFilterLocationPerformer.Style.Add("display", "none");
                tvFilterLocationPerformer_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocationPerformer_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPerformer.Text = tvFilterLocationPerformer.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindCategories()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                var CatList = ActManagement.GetAllAssignedCategory(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypeList = ActManagement.GetAllAssignedType(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();
                int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
                int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindStatus()
        {
            try
            {
                if (IsNotCompiled == true)
                {
                    foreach (CheckListCannedReportPerformerAddedNotCompliedStatus r in Enum.GetValues(typeof(CheckListCannedReportPerformerAddedNotCompliedStatus)))
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CheckListCannedReportPerformerAddedNotCompliedStatus), r), r.ToString());
                        ddlStatus.Items.Add(item);
                    }
                }
                else
                {
                    foreach (CheckListCannedReportPerformer r in Enum.GetValues(typeof(CheckListCannedReportPerformer)))
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(CheckListCannedReportPerformer), r), r.ToString());
                        ddlStatus.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private void BindComplianceType()
        {
            try
            {
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                ddlType.DataSource = CustomerManagement.GetComplainceType();
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceCategory()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = CustomerManagement.GetComplainceCategory();
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindAct()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = CustomerManagement.GetAct();
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {              
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPerformer", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPerformer", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected string GetPerformer(int compliancectatusid, long scheduledonid, long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetCheckListUserName(compliancectatusid, scheduledonid, complianceinstanceid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
                return "";
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    if (queryStringFlag == "A")
                    {
                        #region All Checklist Performer
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CheckListCannedReport");
                        DataTable ExcelData = null;
                        string filter = "";
                        String LocationName = String.Empty;

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Session["LocationName"].ToString() != "")
                            LocationName = Session["LocationName"].ToString();

                        DataView view = new System.Data.DataView(GetGrid());

                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ShortDescription", "Description", "ShortForm", "ForMonth", "ScheduledOn", "EventName", "EventNature", "Risk", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "Remarks", "SequenceID");

                        ExcelData.Columns.Add("RiskName");
                        ExcelData.Columns.Add("Performer");

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Performer"] = GetPerformer(Convert.ToInt32(item["ComplianceStatusID"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()), Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                            if (item["Risk"].ToString() == "0")
                                item["RiskName"] = "High";
                            else if (item["Risk"].ToString() == "1")
                                item["RiskName"] = "Medium";
                            else if (item["Risk"].ToString() == "2")
                                item["RiskName"] = "Low";
                            else if (item["Risk"].ToString() == "3")
                                item["RiskName"] = "Critical";

                            if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "ClosedTimely")
                                item["Status"] = "ClosedTimely";
                            //else if (item["Status"].ToString() == "NotApplicable")
                            //    item["Status"] = "Not Applicable";
                            else if (item["Status"].ToString() == "Not Applicable")
                                item["Status"] = "Not Applicable";
                        }

                        ExcelData.Columns.Remove("ComplianceStatusID");
                        ExcelData.Columns.Remove("ScheduledOnID");
                        ExcelData.Columns.Remove("ComplianceInstanceID");
                        ExcelData.Columns.Remove("Risk");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of Checklist Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A5"].Value = "ComplianceID";
                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["C5"].Value = "Act";
                        exWorkSheet.Cells["D5"].Value = "Category Name";
                        exWorkSheet.Cells["E5"].Value = "Short Description";
                        exWorkSheet.Cells["F5"].Value = "Detail Description";
                        exWorkSheet.Cells["G5"].Value = "Short Form";
                        exWorkSheet.Cells["H5"].Value = "Period";
                        exWorkSheet.Cells["I5"].Value = "Due Date";
                        exWorkSheet.Cells["J5"].Value = "Event Name";
                        exWorkSheet.Cells["K5"].Value = "Event Nature";
                        exWorkSheet.Cells["L5"].Value = "Status";
                        exWorkSheet.Cells["M5"].Value = "Remark";
                        exWorkSheet.Cells["N5"].Value = "Label";
                        exWorkSheet.Cells["O5"].Value = "Risk";
                        exWorkSheet.Cells["P5"].Value = "Performer";

                        //Heading
                        if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                        {
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                            int colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                            exWorkSheet.Column(colIdx).Width = 25;

                            colIdx = ExcelData.Columns.IndexOf("ComplianceCategoryName") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("ShortDescription") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                            exWorkSheet.Column(colIdx).Width = 100;


                            colIdx = ExcelData.Columns.IndexOf("ShortForm") + 1;
                            exWorkSheet.Column(colIdx).Width = 30;

                            colIdx = ExcelData.Columns.IndexOf("ForMonth") + 1;
                            exWorkSheet.Column(colIdx).Width = 10;

                            colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;

                            //colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            //exWorkSheet.Column(colIdx).Width = 15;

                            colIdx = ExcelData.Columns.IndexOf("EventName") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("EventNature") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Performer") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Risk") + 1;
                            exWorkSheet.Column(colIdx).Width = 10;

                            colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Remarks") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;

                            colIdx = ExcelData.Columns.IndexOf("SequenceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 16])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col[5, 2, 5 + ExcelData.Rows.Count, 16].Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Checklist_CannedReport.xlsx");

                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        #endregion
                    }
                    if (queryStringFlag == "E")
                    {
                        #region All Checklist EventBased
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CheckListCannedReport");
                        DataTable ExcelData = null;
                        string filter = "";
                        String LocationName = String.Empty;

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Session["LocationName"].ToString() != "")
                            LocationName = Session["LocationName"].ToString();

                        DataView view = new System.Data.DataView(GetGrid());

                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ShortDescription", "Description", "ShortForm", "ScheduledOn", "EventName", "EventNature", "Risk", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "Remarks", "SequenceID");

                        ExcelData.Columns.Add("RiskName");
                        ExcelData.Columns.Add("Performer");

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Performer"] = GetPerformer(Convert.ToInt32(item["ComplianceStatusID"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()), Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                            if (item["Risk"].ToString() == "0")
                                item["RiskName"] = "High";
                            else if (item["Risk"].ToString() == "1")
                                item["RiskName"] = "Medium";
                            else if (item["Risk"].ToString() == "2")
                                item["RiskName"] = "Low";
                            else if (item["Risk"].ToString() == "3")
                                item["RiskName"] = "Critical";

                            if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "ClosedTimely")
                                item["Status"] = "ClosedTimely";
                            else if (item["Status"].ToString() == "Not Applicable")
                                item["Status"] = "Not Applicable";
                        }

                        ExcelData.Columns.Remove("ComplianceStatusID");
                        ExcelData.Columns.Remove("ScheduledOnID");
                        ExcelData.Columns.Remove("ComplianceInstanceID");
                        ExcelData.Columns.Remove("Risk");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of Checklist Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A5"].Value = "ComplianceID";
                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["C5"].Value = "Act";
                        exWorkSheet.Cells["D5"].Value = "Category Name";
                        exWorkSheet.Cells["E5"].Value = "Short Description";
                        exWorkSheet.Cells["F5"].Value = "Detail Description";
                        exWorkSheet.Cells["G5"].Value = "Short Form";
                        exWorkSheet.Cells["H5"].Value = "Due Date";
                        exWorkSheet.Cells["I5"].Value = "Event Name";
                        exWorkSheet.Cells["J5"].Value = "Event Nature";
                        exWorkSheet.Cells["K5"].Value = "Status";
                        exWorkSheet.Cells["L5"].Value = "Remark";
                        exWorkSheet.Cells["M5"].Value = "Label";
                        exWorkSheet.Cells["N5"].Value = "Risk";
                        exWorkSheet.Cells["O5"].Value = "Performer";
                      
                        //Heading
                        if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                        {
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                            int colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                            exWorkSheet.Column(colIdx).Width = 25;

                            colIdx = ExcelData.Columns.IndexOf("ComplianceCategoryName") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("ShortDescription") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                            exWorkSheet.Column(colIdx).Width = 100;

                            colIdx = ExcelData.Columns.IndexOf("ShortForm") + 1;
                            exWorkSheet.Column(colIdx).Width = 30;

                            colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;

                            //colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            //exWorkSheet.Column(colIdx).Width = 15;

                            colIdx = ExcelData.Columns.IndexOf("EventName") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("EventNature") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Performer") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Risk") + 1;
                            exWorkSheet.Column(colIdx).Width = 10;

                            colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Remarks") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("SequenceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 15])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col[5, 2, 5 + ExcelData.Rows.Count, 15].Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Checklist_CannedReport.xlsx");

                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        #endregion
                    }
                    if (queryStringFlag == "S")
                    {
                        #region Statutory Checklist Performer
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CheckListCannedReport");
                        DataTable ExcelData = null;
                        string filter = "";
                        String LocationName = String.Empty;

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Session["LocationName"].ToString() != "")
                            LocationName = Session["LocationName"].ToString();

                        DataView view = new System.Data.DataView(GetGrid());

                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ShortDescription", "Description", "ShortForm", "ForMonth", "ScheduledOn", "Risk", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "Remarks", "SequenceID");

                        ExcelData.Columns.Add("RiskName");
                        ExcelData.Columns.Add("Performer");

                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["Performer"] = GetPerformer(Convert.ToInt32(item["ComplianceStatusID"].ToString()), Convert.ToInt32(item["ScheduledOnID"].ToString()), Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                            if (item["Risk"].ToString() == "0")
                                item["RiskName"] = "High";
                            else if (item["Risk"].ToString() == "1")
                                item["RiskName"] = "Medium";
                            else if (item["Risk"].ToString() == "2")
                                item["RiskName"] = "Low";
                            else if (item["Risk"].ToString() == "3")
                                item["RiskName"] = "Critical";

                            if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                item["Status"] = "Upcoming";
                            else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                item["Status"] = "Overdue";
                            else if (item["Status"].ToString() == "ClosedTimely")
                                item["Status"] = "ClosedTimely";
                            else if (item["Status"].ToString() == "Not Applicable")
                                item["Status"] = "Not Applicable";
                        }

                        ExcelData.Columns.Remove("ComplianceStatusID");
                        ExcelData.Columns.Remove("ScheduledOnID");
                        ExcelData.Columns.Remove("ComplianceInstanceID");
                        ExcelData.Columns.Remove("Risk");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of Checklist Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A5"].Value = "ComplianceID";
                        exWorkSheet.Cells["B5"].Value = "Location";
                        exWorkSheet.Cells["C5"].Value = "Act";
                        exWorkSheet.Cells["D5"].Value = "Category Name";
                        exWorkSheet.Cells["E5"].Value = "Short Description";
                        exWorkSheet.Cells["F5"].Value = "Detail Description";
                        exWorkSheet.Cells["G5"].Value = "Short Form";
                        exWorkSheet.Cells["H5"].Value = "Period";
                        exWorkSheet.Cells["I5"].Value = "Due Date";
                        exWorkSheet.Cells["J5"].Value = "Status";
                        exWorkSheet.Cells["K5"].Value = "Remark";
                        exWorkSheet.Cells["L5"].Value = "Label";
                        exWorkSheet.Cells["M5"].Value = "Risk";
                        exWorkSheet.Cells["N5"].Value = "Performer";
                        //Heading
                        if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                        {
                            exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                            int colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                            exWorkSheet.Column(colIdx).Width = 25;

                            colIdx = ExcelData.Columns.IndexOf("ComplianceCategoryName") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("ShortDescription") + 1;
                            exWorkSheet.Column(colIdx).Width = 35;

                            colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                            exWorkSheet.Column(colIdx).Width = 100;

                            colIdx = ExcelData.Columns.IndexOf("ShortForm") + 1;
                            exWorkSheet.Column(colIdx).Width = 30;

                            colIdx = ExcelData.Columns.IndexOf("ForMonth") + 1;
                            exWorkSheet.Column(colIdx).Width = 10;

                            colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;

                            colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                            exWorkSheet.Column(colIdx).Width = 15;

                            colIdx = ExcelData.Columns.IndexOf("Performer") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Risk") + 1;
                            exWorkSheet.Column(colIdx).Width = 10;

                            colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("Remarks") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;

                            colIdx = ExcelData.Columns.IndexOf("SequenceID") + 1;
                            exWorkSheet.Column(colIdx).Width = 20;
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 14])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col[5, 2, 5 + ExcelData.Rows.Count, 14].Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Checklist_CannedReport.xlsx");

                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "ClosedTimely")
                            e.Row.Cells[6].Text = "ClosedTimely";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CheckListCannedReportPerformer>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DataTable GetGrid()
        {
            FillData(role,queryStringFlag);
            if (role == "Performer")
            { 
            return (grdComplianceTransactions.DataSource as List<SP_GetCheckListCannedReportCompliancesSummary_Result>).ToDataTable();
            }
            else
            {
                return (grdComplianceTransactions.DataSource as List<CheckListInstanceTransactionReviewerView>).ToDataTable();
            }
        }
        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CheckListCannedReportPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {                            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillData(role, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }           
        }
        protected void FillData(string role, string queryStringFlag)
        {
            try
            {

                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CheckListCannedReportPerformerAddedNotCompliedStatus status1 =new CheckListCannedReportPerformerAddedNotCompliedStatus();
                CheckListCannedReportPerformer status = new CheckListCannedReportPerformer(); 
                if (IsNotCompiled == true)
                {
                    status1 = (CheckListCannedReportPerformerAddedNotCompliedStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                }
                else
                {
                    status = (CheckListCannedReportPerformer)Convert.ToInt16(ddlStatus.SelectedIndex);
                }

                int ComplianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (role == "Performer")
                {
                    if(IsNotCompiled == true)
                    {
                        var CheckList = CannedReportManagement.GetCheckListReportDataForPerformerAddedNotCompliedStatus(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Y", risk, status1, location, type, category, ActID, dtfrom, dtTo, ComplianceType, queryStringFlag);
                        grdComplianceTransactions.DataSource = CheckList;
                        Session["TotalRowsPerformer"] = CheckList.Count();
                        grdComplianceTransactions.DataBind();
                    }
                    else
                    {
                        var CheckList = CannedReportManagement.GetCheckListReportDataForPerformerNew(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Y", risk, status, location, type, category, ActID, dtfrom, dtTo, ComplianceType, queryStringFlag);
                        grdComplianceTransactions.DataSource = CheckList;
                        Session["TotalRowsPerformer"] = CheckList.Count();
                        grdComplianceTransactions.DataBind();
                    }

                    if (queryStringFlag == "A")
                    {
                        grdComplianceTransactions.Columns[4].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                        grdComplianceTransactions.Columns[7].Visible = true;
                    }
                    if (queryStringFlag == "E")
                    {
                        grdComplianceTransactions.Columns[4].Visible = false;
                        grdComplianceTransactions.Columns[6].Visible = true;
                        grdComplianceTransactions.Columns[7].Visible = true;
                    }
                    if (queryStringFlag == "S")
                    {
                        grdComplianceTransactions.Columns[4].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = false;
                        grdComplianceTransactions.Columns[7].Visible = false;
                    }
                }
                else
                {
                    if (IsNotCompiled == true)
                    {
                        var CheckList = CannedReportManagement.GetCheckListReportDataForReviewerAddedNotCompliedStatus(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Y", risk, status1, location, type, category, ActID, dtfrom, dtTo, ComplianceType, queryStringFlag);
                        grdComplianceTransactions.DataSource = CheckList;
                        Session["TotalRowsPerformer"] = CheckList.Count();
                        grdComplianceTransactions.DataBind();
                    }
                    else
                    {
                        var CheckList = CannedReportManagement.GetCheckListReportDataForReviewerrNew(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Y", risk, status, location, type, category, ActID, dtfrom, dtTo, ComplianceType, queryStringFlag);
                        grdComplianceTransactions.DataSource = CheckList;
                        Session["TotalRowsPerformer"] = CheckList.Count();
                        grdComplianceTransactions.DataBind();
                    }

                    if (queryStringFlag == "A")
                    {
                        grdComplianceTransactions.Columns[4].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                        grdComplianceTransactions.Columns[7].Visible = true;
                    }
                    if (queryStringFlag == "E")
                    {
                        grdComplianceTransactions.Columns[4].Visible = false;
                        grdComplianceTransactions.Columns[6].Visible = true;
                        grdComplianceTransactions.Columns[7].Visible = true;
                    }
                    if (queryStringFlag == "S")
                    {
                        grdComplianceTransactions.Columns[4].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = false;
                        grdComplianceTransactions.Columns[7].Visible = false;
                    }
                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                divAdvSearch.Visible = false;
                FillData(role, queryStringFlag);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }
        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                grdComplianceTransactions.PageSize = int.Parse(((DropDownList)sender).SelectedValue);
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role, queryStringFlag);

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }              
                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillData(role, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRowsPerformer"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRowsPerformer"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRowsPerformer"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }        
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CheckListCannedReportPerformer status = (CheckListCannedReportPerformer)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int ComplianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (ComplianceType == -1)
                {
                    queryStringFlag = "S";
                }
                else
                {
                    queryStringFlag = "E";
                }
                grdComplianceTransactions.Visible = true;
                FillData(role, queryStringFlag);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }
        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                SelectedPageNo.Text = "1";
                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }
                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }
                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }
                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }
                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                FillData(role, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
    }
}