﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceLicenseList : System.Web.UI.Page
    {
        protected bool flag;
        protected long loggedInUserId = AuthenticationHelper.UserID;
        protected string loggedInUserRoleCode = AuthenticationHelper.Role;
        protected long customerID = 0;
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";        
        protected static string queryStringFlag = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        string status = Request.QueryString["Status"].ToString().Trim();
                        ddlLicenseStatus.SelectedValue = status;
                    }
                    else
                    {
                        ddlLicenseStatus.SelectedValue = "Status";
                    }
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    BindLocationFilter();
                    BindLicenseType();
                    if (roles.Contains(3) && roles.Contains(4))
                    {
                        ClickChangeflag = "P";
                    }
                    else if (roles.Contains(3))
                    {
                        ClickChangeflag = "P";
                    }
                    else if (roles.Contains(4))
                    {
                        ClickChangeflag = "R";
                    }
                    var cnt = LicenseMgmt.GetAssignedroleid(AuthenticationHelper.UserID);
                    if (cnt.Count > 0)
                    {
                        lnkcompliancelink.Visible = true;
                    }
                    else
                    {
                        lnkcompliancelink.Visible = false;
                    }
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
                try
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                BindLicenseType();
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindLicenseType()
        {
            string isstatutoryinternal = "S";
            if (ddlComplianceType.SelectedItem.Text == "Statutory")
            {
                isstatutoryinternal = "S";
            }
            else if (ddlComplianceType.SelectedItem.Text == "Internal")
            {
                isstatutoryinternal = "I";
            }
            List<Sp_BindLicenseType_Result> data = new List<Sp_BindLicenseType_Result>();
            if (AuthenticationHelper.Role.Contains("CADMN"))
            {                
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            else
            {
                data = LicenseTypeMasterManagement.GetLicenseTypeWiseAssinedUser(AuthenticationHelper.UserID, AuthenticationHelper.Role, isstatutoryinternal);
            }
            //Page DropDown
            ddlLicenseType.DataTextField = "Name";
            ddlLicenseType.DataValueField = "ID";

            ddlLicenseType.DataSource = data;
            ddlLicenseType.DataBind();

            ddlLicenseType.Items.Insert(0, new ListItem("< All >", "-1"));
        }
        private void BindLocationFilter()
        {
            try
            {                    
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                var LocationList = new List<int>();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                //tvFilterLocation_SelectedNodeChanged(null, null);
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                tbxFilterLocation.Text = "Select Entity/Location";

                TreeNode node = new TreeNode("All", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvErrorLicenseListPage.IsValid = false;
                //cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvErrorLicenseListPage.IsValid = false;
                //cvErrorLicenseListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }
        protected void ddlLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();

            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLicenseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();

            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
            BindGrid();
            bindPageNumber();
            ShowGridDetail();

            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int deptID = -1;
                string licenseStatus = string.Empty;
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
               
                if (ddlLicenseStatus.SelectedValue != "Status")
                {
                    licenseStatus = ddlLicenseStatus.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();

                string isstatutoryinternal = "S";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                if (AuthenticationHelper.Role == "MGMT")
                {
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                        branchList, deptID, licenseStatus, licenseTypeID, "MGMT", isstatutoryinternal);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    if (roles.Contains(3))
                    {
                        liReviewer.Attributes.Add("class", "");
                        liPerformer.Attributes.Add("class", "active");
                    }
                    else if (roles.Contains(4))
                    {
                        liReviewer.Attributes.Add("class", "active");
                        liPerformer.Attributes.Add("class", "");
                    }

                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "CADMN", isstatutoryinternal);
                    if (ClickChangeflag == "P")
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    else if (ClickChangeflag == "R")
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                    }
                }
                else if (roles.Contains(3) && roles.Contains(4))
                {
                   
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                         branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);

                    if (ClickChangeflag == "P")
                    {
                        liReviewer.Attributes.Add("class", "");
                        liPerformer.Attributes.Add("class", "active");
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    else
                    {
                        liReviewer.Attributes.Add("class", "active");
                        liPerformer.Attributes.Add("class", "");
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                    }
                }
                else if (roles.Contains(3))
                {
                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "active");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                      branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                }
                else if (roles.Contains(4))
                {
                    liReviewer.Attributes.Add("class", "active");
                    liPerformer.Attributes.Add("class", "");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                        branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                }
                else
                {
                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);
                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                Session["TotalRows"] = null;
                if (MasterTransction.Count > 0)
                {
                    flag = true;
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
                MasterTransction.Clear();
                MasterTransction = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }                    
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLicenseList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static int GetInternalAssignedInstanceroleid(int Userid, int ComplianceInstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var recordToUpdate = (from row in entities.InternalComplianceAssignments
                                      where row.InternalComplianceInstanceID == ComplianceInstanceid
                                      && row.UserID == Userid
                                      select row.RoleID).FirstOrDefault();
                return recordToUpdate;
            }
        }
        public static int GetAssignedInstanceroleid(int Userid, int ComplianceInstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var recordToUpdate = (from row in entities.ComplianceAssignments
                                      where row.ComplianceInstanceID == ComplianceInstanceid
                                      && row.UserID == Userid
                                      select row.RoleID).FirstOrDefault();
                return recordToUpdate;
            }
        }
        protected void lnkEditLicense_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                    if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[1])))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(commandArgs[2])))
                        {
                            int ComplianceInstanceID = Convert.ToInt32(commandArgs[1]);
                            long ComplianceScheduleOnID = Convert.ToInt64(commandArgs[2]);
                            int roleid = -1;
                            if (ddlComplianceType.SelectedItem.Text == "Statutory")
                            {
                                 roleid = GetAssignedInstanceroleid(AuthenticationHelper.UserID, ComplianceInstanceID);
                            }
                            else
                            {
                                roleid = GetInternalAssignedInstanceroleid(AuthenticationHelper.UserID, ComplianceInstanceID);                                
                            }

                            if (roleid == 3)
                            {
                                liReviewer.Attributes.Add("class", "");
                                liPerformer.Attributes.Add("class", "active");
                                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogPerformer(" + ComplianceInstanceID + "," + ComplianceScheduleOnID + ");", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogInternalPerformer(" + ComplianceInstanceID + "," + ComplianceScheduleOnID + ");", true);
                                }
                                
                            }
                            else if (roleid == 4)
                            {
                                liReviewer.Attributes.Add("class", "active");
                                liPerformer.Attributes.Add("class", "");
                                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogReviewer(" + ComplianceInstanceID + "," + ComplianceScheduleOnID + ");", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialogInternalReviewer(" + ComplianceInstanceID + "," + ComplianceScheduleOnID + ");", true);
                                }
                            }                            
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected bool CanChangeStatus(long LicenseID, long ComplianceInstanceID, long ComplianceScheduleOnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool result = false;
                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        var statusid = (from row in entities.RecentComplianceTransactionViews
                                        where row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row.ComplianceStatusID).FirstOrDefault();

                        if (statusid != null)
                        {
                            if (ClickChangeflag == "P")
                            {
                                var RoleID = (from row in entities.ComplianceAssignments
                                              where row.ComplianceInstanceID == ComplianceInstanceID
                                              && row.UserID == AuthenticationHelper.UserID
                                              && row.RoleID==3
                                              select row.RoleID).FirstOrDefault();
                                if (RoleID != null)
                                {
                                    if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                                    {
                                        result = true;
                                    }
                                    if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                                    {
                                        result = true;
                                    }
                                }
                            }
                            else if (ClickChangeflag == "R")
                            {
                                var RoleID = (from row in entities.ComplianceAssignments
                                              where row.ComplianceInstanceID == ComplianceInstanceID
                                              && row.UserID == AuthenticationHelper.UserID
                                               && row.RoleID == 4
                                              select row.RoleID).FirstOrDefault();
                                if (RoleID != null)
                                {
                                    if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                                    {
                                        result = true;
                                    }
                                    else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                                    {
                                        result = true;
                                    }
                                }
                            }
                            //var RoleID = (from row in entities.ComplianceAssignments
                            //              where row.ComplianceInstanceID == ComplianceInstanceID
                            //              && row.UserID == AuthenticationHelper.UserID
                            //              select row.RoleID).FirstOrDefault();
                            //if (RoleID != null)
                            //{
                            //    if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                            //    {
                            //        result = true;
                            //    }
                            //    if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                            //    {
                            //        result = true;
                            //    }
                            //    else if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                            //    {
                            //        result = true;
                            //    }
                            //    else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                            //    {
                            //        result = true;
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        var statusid = (from row in entities.RecentInternalComplianceTransactionViews
                                        where row.InternalComplianceScheduledOnID == ComplianceScheduleOnID
                                        select row.ComplianceStatusID).FirstOrDefault();

                        if (statusid != null)
                        {
                            if (ClickChangeflag == "P")
                            {
                                var RoleID = (from row in entities.InternalComplianceAssignments
                                              where row.InternalComplianceInstanceID == ComplianceInstanceID
                                              && row.UserID == AuthenticationHelper.UserID
                                               && row.RoleID == 3
                                              select row.RoleID).FirstOrDefault();
                                if (RoleID != null)
                                {
                                    if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                                    {
                                        result = true;
                                    }
                                    if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                                    {
                                        result = true;
                                    }
                                }
                            }
                            else if (ClickChangeflag == "R")
                            {
                                var RoleID = (from row in entities.InternalComplianceAssignments
                                              where row.InternalComplianceInstanceID == ComplianceInstanceID
                                              && row.UserID == AuthenticationHelper.UserID
                                               && row.RoleID == 4
                                              select row.RoleID).FirstOrDefault();
                                if (RoleID != null)
                                {
                                    if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                                    {
                                        result = true;
                                    }
                                    else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                                    {
                                        result = true;
                                    }
                                }
                            }
                            //var RoleID = (from row in entities.InternalComplianceAssignments
                            //              where row.InternalComplianceInstanceID == ComplianceInstanceID
                            //              && row.UserID == AuthenticationHelper.UserID
                            //              select row.RoleID).FirstOrDefault();
                            //if (RoleID != null)
                            //{
                            //    if (RoleID == 3 && statusid == 1 && ClickChangeflag == "P")
                            //    {
                            //        result = true;
                            //    }
                            //    if (RoleID == 3 && statusid == 6 && ClickChangeflag == "P")
                            //    {
                            //        result = true;
                            //    }
                            //    else if (RoleID == 4 && statusid == 2 && ClickChangeflag == "R")
                            //    {
                            //        result = true;
                            //    }
                            //    else if (RoleID == 4 && statusid == 3 && ClickChangeflag == "R")
                            //    {
                            //        result = true;
                            //    }
                            //}
                        }
                    }                      
                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
      
        protected void grdLicenseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
        }
        protected void grdLicenseList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {               
                long customerID = -1;
                customerID = Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int deptID = -1;
                string licenseStatus = string.Empty;
                long licenseTypeID = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (ddlLicenseStatus.SelectedValue != "Status")
                {
                    licenseStatus = ddlLicenseStatus.SelectedValue;
                }
                if (!string.IsNullOrEmpty(ddlLicenseType.SelectedValue))
                {
                    licenseTypeID = Convert.ToInt32(ddlLicenseType.SelectedValue);
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                List<Lic_SP_MyWorkspaceDetail_Result> MasterTransction = new List<Lic_SP_MyWorkspaceDetail_Result>();
                string isstatutoryinternal = "S";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                if (AuthenticationHelper.Role == "MGMT")
                {
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                        branchList, deptID, licenseStatus, licenseTypeID, "MGMT", isstatutoryinternal);
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    if (roles.Contains(3))
                    {
                        liReviewer.Attributes.Add("class", "");
                        liPerformer.Attributes.Add("class", "active");
                    }
                    else if (roles.Contains(4))
                    {
                        liReviewer.Attributes.Add("class", "active");
                        liPerformer.Attributes.Add("class", "");
                    }

                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "CADMN", isstatutoryinternal);
                    if (ClickChangeflag == "P")
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    else
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                    }
                }
                else if (roles.Contains(3) && roles.Contains(4))
                {
                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "active");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                         branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    if (ClickChangeflag == "P")
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                    }
                    else
                    {
                        MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                    }
                }
                else if (roles.Contains(3))
                {
                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "active");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                      branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                }
                else if (roles.Contains(4))
                {
                    liReviewer.Attributes.Add("class", "active");
                    liPerformer.Attributes.Add("class", "");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                        branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 4).ToList();
                }
                else
                {
                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "");
                    MasterTransction = LicenseMgmt.GetAllLicenseDetials(Convert.ToInt32(customerID), AuthenticationHelper.UserID,
                     branchList, deptID, licenseStatus, licenseTypeID, "PER", isstatutoryinternal);
                    MasterTransction = MasterTransction.Where(entry => entry.RoleID == 3).ToList();
                }
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    MasterTransction = MasterTransction.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    MasterTransction = MasterTransction.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdLicenseList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdLicenseList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                if (MasterTransction.Count > 0)
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = MasterTransction.Count;
                }
                else
                {
                    grdLicenseList.DataSource = MasterTransction;
                    grdLicenseList.DataBind();
                    Session["TotalRows"] = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLicenseList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLicenseList.PageIndex = chkSelectedPage - 1;

            grdLicenseList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
            
            ShowGridDetail();
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
       
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }               
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void lnkcompliancelink_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("~/Controls/frmUpcomingCompliancess.aspx",true);
        //}
        protected void btnPerformer_Click(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setTabActive", "setTabActive('ContentPlaceHolder1_liPerformer');", true);
            ClickChangeflag = "P";
            BindGrid();
            bindPageNumber();
            ShowGridDetail();

        }
        protected void btnReviewer_Click(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "active");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setTabActive", "setTabActive('ContentPlaceHolder1_liReviewer');", true);
            ClickChangeflag = "R";
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
    }
}
