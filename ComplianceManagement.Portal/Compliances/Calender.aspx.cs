﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class Calender : System.Web.UI.Page
    {
        [WebMethod]
        public static List<Compliancecalendar> GetEvents()
        {
            List<Compliancecalendar> events = new List<Compliancecalendar>();
            c_json bus = new c_json();
            int customerid = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            int userid = AuthenticationHelper.UserID;
            DataTable dt = bus.fetch(customerid, userid);
            var x = (from r in dt.AsEnumerable()
                     select r["ScheduledOn"]).Distinct().ToList();

            foreach (var i in x)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                _Event.ScheduledOn = Convert.ToDateTime(i);
                events.Add(_Event);
            }
            return events;
        }
    }

    public partial class Compliancecalendar
    {
        public System.DateTime ScheduledOn { get; set; }
    }

}