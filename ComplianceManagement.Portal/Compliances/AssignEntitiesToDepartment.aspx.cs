﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class AssignEntitiesToDepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;
                BindLocationFilter();
                BindComplianceEntityInstance();
                BindUsers(ddlUsers);
                BindDelLocationFilter();
                BindLocation();
                tbxBranch.Attributes.Add("readonly", "readonly");
                TextBox1.Attributes.Add("readonly", "readonly");

            }


 
        }
        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                    
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";

                ddlUserList.Items.Clear();
                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void Create(EntitiesAssignment_IsDept objEntitiesAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EntitiesAssignment_IsDept.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }
        public static EntitiesAssignment_IsDept SelectEntity(int branchId, int userID, string isSatInt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntitiesAssignmentData = (from row in entities.EntitiesAssignment_IsDept
                                              where row.BranchID == branchId
                                              && row.UserID == userID
                                              && row.IsStatutoryInternal == isSatInt
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int branchId = -1;
                int userID = -1;
                string issatint = string.Empty;
                if (!string.IsNullOrEmpty(ddlUsers.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUsers.SelectedValue);
                }

                if (!string.IsNullOrEmpty(tvBranches.SelectedValue))
                {
                    branchId = Convert.ToInt32(tvBranches.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlstutoryinternal.SelectedValue))
                {
                    issatint = ddlstutoryinternal.SelectedValue;
                }

                if (userID != -1 && branchId != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var customerBranches = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == AuthenticationHelper.CustomerID
                                                select row);
                        foreach (var node in customerBranches)
                        {

                            var data = SelectEntity(Convert.ToInt32(node.ID), userID, issatint);
                            if (data != null)
                            {

                                EntitiesAssignment_IsDept objEntitiesAssignment = new EntitiesAssignment_IsDept();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                                objEntitiesAssignment.IsStatutoryInternal = issatint;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                Create(objEntitiesAssignment);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                            }
                        }
                    }
                }



            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocation()
        {
            try
            {
                int custid = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    custid = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(custid);
                TreeNode node = new TreeNode();

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;

                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstance();
        }
        private void BindComplianceEntityInstance()
        {
            try
            {
                int branchid = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(tvFilterLocation.SelectedValue);

                }
                int userid = -1;
                if ((!string.IsNullOrEmpty(ddlstutoryinternal.SelectedValue)))
                {
                    userid = Convert.ToInt32(ddlstutoryinternal.SelectedValue);


                }
                int custid = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                grdAssignEntities.DataSource = AssignEntityManagement.SelectAllEntities(branchid, userid, custid);
                grdAssignEntities.DataBind();

                upComplianceTypeList.Update();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "server error occured.please try again.";

            }


        }


        private void BindDelLocationFilter()
        {
            try
            {
                int custid = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    custid = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(custid);
                TreeNode node = new TreeNode("All", "-2");
                node.SelectAction = TreeNodeSelectAction.Select;

                TreeView1.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;

                    BindBranchesHierarchy(node, item);
                    TreeView1.Nodes.Add(node);
                }

                TreeView1.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox1.Text = TreeView1.SelectedNode != null ? TreeView1.SelectedNode.Text : "< select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "&(\"#divBranches1\").hide(\"bind\",null,500,function (){});", true);

            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches'); ", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}
