﻿<%@ Page Title="Approve Compliances" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ApproveCompliances.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ApproveCompliances" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function showHide(id) {

            var elem = document.getElementById('d' + id);
            var img = document.getElementById('I' + id);
            if (elem) {
                if (elem.style.display != 'block') {
                    elem.style.display = 'block';
                    elem.style.visibility = 'visible';
                    img.src = img.src.replace("plus", "minus");
                }
                else {
                    elem.style.display = 'none';
                    elem.style.visibility = 'hidden';
                    img.src = img.src.replace("minus", "plus");
                }
            }
            return false;
        }

        function Confirm(category) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure you want approve all compliances of category " + category + "?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="margin: 10px 20px 10px 30px">
        <div style="margin-bottom: 4px">
            <asp:ValidationSummary runat="server" CssClass="vdsummary"
                ValidationGroup="ComplianceInstanceValidationGroup" />
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            <asp:Label ID="lblMessage" runat="server" Style="text-align: center"></asp:Label>
        </div>
        <div id="mainDiv" runat="server">
            <asp:Repeater ID="rptCatagory" runat="server" OnItemDataBound="lstCatagory_ItemDataBound">
                <ItemTemplate>
                    <div style="margin-top: 10px;" class="test">
                        <asp:Label ID="lblCatagoryName" runat="server" Text='<%# Eval("Catagory") %>' Style="font-size: medium; font-weight: bold" Width="250px"></asp:Label>
                        <asp:LinkButton ID="lbtApprover" Text="Approve" CommandArgument='<%# Eval("ComplianceCatagoryID") %>' CommandName='<%# Eval("Catagory") %>'
                            CssClass="button" OnCommand="lbtApprover_Command" runat="server"></asp:LinkButton>

                    </div>
                    <asp:Repeater ID="repActList" runat="server" OnItemDataBound="repActList_ItemDataBound">
                        <ItemTemplate>
                            <div id='h1<%# Eval("ActID") %>' class="header" onclick='showHide(<%# Eval("ActID") %>);' style="margin-left: 15px; margin-top: 10px; margin-bottom: 10px; background-color: #E6EFF7; height: 25px; border: px solid #9FCBEA">
                                <table>
                                    <tr>
                                        <td>
                                            <img id='I<%# Eval("ActID") %>' src="../Images/plus.png" style="width: 15px; height: 15px;" /></td>
                                        <td>
                                            <asp:Label ID="lblActID" runat="server" Text='<%# Eval("ActID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblActName" runat="server" Text='<%# Eval("ActName") %>'></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                            <div id='d<%# Eval("ActID") %>' style="margin-left: 40px; display: none">
                                <asp:Repeater runat="server" ID="rptCompliancesList">
                                    <ItemTemplate>
                                        <table style="border: 1px solid gray; border-collapse: collapse; width: 100%;">
                                            <tr>
                                                <td style="border: 1px solid gray; border-collapse: collapse;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px;">
                                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription")%>'></asp:Label>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>

</asp:Content>

