﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class MyReport : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static bool IsNotCompiled;
        //protected static int RoleDropdDisplay;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Path;
        protected static string CustomerName;
        protected static int PerformerFlagID;
        protected static int ReviewerFlagID;
        protected static int ApproverFlagID;
        protected static int ManagmentFlagID;
        protected static int DepartmentFlagID;
        protected static string RoleKey;
        protected static string Authorization;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            IsNotCompiled = false;
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            CustomerName = GetCustomerName(CustId);
            RoleFlag = 0;
            IsMonthID = "3";
            PerformerFlagID = 0;
            ReviewerFlagID = 0;
            ApproverFlagID = 0;
            ManagmentFlagID = 0;
            DepartmentFlagID = 0;
            
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }

            var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    DepartmentFlagID = 1;
                    RoleKey = "DEPT";
                }
            }

            roles = Session["User_comp_Roles"] as List<int>;//get role for Performer and Reviewer and Approver
            if (roles.Contains(3))
            {
                PerformerFlagID = 1;
                RoleKey = "PRA";
            }
            if (roles.Contains(4))
            {
                ReviewerFlagID = 1;
                RoleKey = "REV";
            }
            if (roles.Contains(6))
            {
                ApproverFlagID = 1;
                RoleKey = "APPR";
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                ManagmentFlagID = 1;
                Falg = "MGMT";
                RoleFlag = 1;
                DisableFalg = false;
                RoleKey = "MGMT";
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                //IsMonthID = "AUD";
                SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");

                Falg = "AUD";
                DisableFalg = true;
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }

            // STT Change- Add Status
            string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
            List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
            if (PenaltyNotDisplayCustomerList.Count > 0)
            {
                foreach (string PList in PenaltyNotDisplayCustomerList)
                {
                    if (PList == CustId.ToString())
                    {
                        IsNotCompiled = true;
                        break;
                    }
                }
            }
        }

        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.CustomerViews
                              where row.ID == CID
                              select row.Name).Single();

                return CName;
            }
        }

    }
}