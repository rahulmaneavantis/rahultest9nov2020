﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CustomerwiseCookies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCustomers();
                BindData();               
            }
        }
        
        private void BindCustomers()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                ddlCustomerList.DataSource = CustomerManagement.GetAll("");
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindData()
        {
            int customerid = -1;
            grdClient.DataSource = null;
            grdClient.DataBind();

            int userID = AuthenticationHelper.UserID;
            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue) && Convert.ToString(ddlCustomerList.SelectedValue) != "-1")
            {
                customerid = Convert.ToInt32(ddlCustomerList.SelectedValue);
            }
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ClientDetails = (from row in entities.CustomerwiseCookies
                                     join row1 in entities.Customers
                                     on row.CustomerID equals row1.ID
                                     where row.IsDelete == false
                                     select new
                                     {
                                         row.ID,
                                         row1.Name,
                                         row.Expiresdays,
                                     }).ToList();
                grdClient.DataSource = ClientDetails;
                grdClient.DataBind();

            }
            upUserList.Update();
        }
        protected void ddlcustomerList_Changed(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CustomerwiseCooky cust = new CustomerwiseCooky()
                    {
                        CustomerID = customerID,
                        Expiresdays = Convert.ToInt32(txtexpiresdays.Text),
                        IsDelete = false,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        cust.ID = Convert.ToInt32(ViewState["CID"]);
                    }
                    bool isExists = Exists(cust);
                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (isExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Customer allready exists.";                            
                            return;
                        }
                        else
                        {
                            entities.CustomerwiseCookies.Add(cust);
                            entities.SaveChanges();
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record saved successfully";
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        cust.UpdatedBy = AuthenticationHelper.UserID;
                        Update(cust);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record update Successfully.";                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Update(CustomerwiseCooky ltlm)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CustomerwiseCooky ltlmToUpdate = (from row in entities.CustomerwiseCookies
                                                      where row.ID == ltlm.ID
                                                      select row).FirstOrDefault();

                    ltlmToUpdate.Expiresdays = ltlm.Expiresdays;
                    ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                    ltlmToUpdate.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();                    
                }
            }
            catch (Exception ex)
            {                
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
        public static bool Exists(CustomerwiseCooky ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerwiseCookies
                             where row.CustomerID == ltlm.CustomerID
                             && row.IsDelete == false
                             select row);
                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static CustomerwiseCooky GetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerdetails = (from row in entities.CustomerwiseCookies
                                       where row.ID == ID
                                       select row).SingleOrDefault();

                return customerdetails;
            }
        }
        protected void grdClient_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Client"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CID"] = ID;                    
                    CustomerwiseCooky cust1 = GetByID(ID);                    
                    ddlCustomerList.SelectedValue = cust1.CustomerID.ToString();
                    txtexpiresdays.Text = cust1.Expiresdays.ToString();
                    upCustomerDetails.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#CustomerDetails\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_Client"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);                    
                    Delete(ID);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Record deleted successfully";                                        
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                upCustomerDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#CustomerDetails\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upUserList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upCustomerDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Delete(int ID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CustomerwiseCooky ltlmToUpdate = (from row in entities.CustomerwiseCookies
                                                      where row.ID == ID
                                                      select row).FirstOrDefault();

                    ltlmToUpdate.UpdatedBy = AuthenticationHelper.UserID;
                    ltlmToUpdate.IsDelete = true;
                    ltlmToUpdate.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}