﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="CustomerwiseCookies.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CustomerwiseCookies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="../Newjs/fullcalendar.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
  <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional" OnLoad="upUserList_Load">
        <ContentTemplate>
            <table width="100%">
                <tr>
                  <td align="right" style="width: 65%; padding-right: 60px;">
                      
                            <asp:LinkButton Text="Add New" runat="server" ID="btnAddAct" OnClick="btnAddCustomer_Click" Visible="true" />
                       
                    </td>
                    </tr>
                </table>
       
      <div runat="server" id="div3" style="margin-bottom: 7px;">
   
            <asp:Panel ID="Panel2" Width="100%" Height="350px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdClient" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%"
                    Font-Size="12px" OnRowCommand="grdClient_RowCommand" DataKeyNames="ID">
                    <%-- OnRowCommand="grdCompliances_RowCommand"--%>
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                        <%-- <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ComplianceID" />--%>
                        <asp:TemplateField HeaderText="Customer Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expires days" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("Expiresdays") %>' ToolTip='<%# Eval("Expiresdays") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="aaa" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lbtEdit" runat="server" CommandName="EDIT_Client" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Client Details"/></asp:LinkButton>
                                        <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Client" CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this compliance?');">
                                    <img src="../Images/delete_icon.png" alt="Delete Client Details"/></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
          </div>
     </ContentTemplate>
</asp:UpdatePanel>

    <div id="CustomerDetails"  style="margin: 5px">
           <asp:UpdatePanel ID="upCustomerDetails" runat="server" UpdateMode="Conditional" OnLoad="upCustomerDetails_Load">
            <ContentTemplate>
         <table align="center" style="padding-top:40px">
             <tr>
                 <td>
                     <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="CustomerValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="CustomerValidationGroup" Display="None" />
                    </div>
                 </td>
             </tr>
             <tr>
                 <td>
                     <div style="margin-bottom: 7px">
                          <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                         <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                             Customer Name</label>
                         <asp:DropDownList ID="ddlCustomerList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlcustomerList_Changed" Style="height:25px;width:200px;"></asp:DropDownList>
                     <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomerList"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CustomerValidationGroup"
                            Display="None" />
                          </div>
                 </td>
             </tr>
             <tr>
                 <td>
                     <div style="margin-bottom: 7px">
                         <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                         <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                             Expires days</label>
                         <asp:TextBox runat="server" ID="txtexpiresdays" Style="height: 25px; width: 200px;"
                             ToolTip="" />
                         <asp:RequiredFieldValidator ErrorMessage="Expires days can not be empty ." ControlToValidate="txtexpiresdays"
                             runat="server" ValidationGroup="CustomerValidationGroup" Display="None" />
                          <asp:CompareValidator ValidationGroup="CustomerValidationGroup" runat="server" ControlToValidate="txtexpiresdays"  Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer"  ErrorMessage="Expires days cannot be less than zero or string" Display="Dynamic" Text="*"/>
                                                          
                     </div>
                 </td>
             </tr>
             <%--<tr>
                 <td>
                     <asp:Button id="btnsubmit" runat="server" Text="Submit" Style="height: 25px; width: 100px;"/>
                 </td>
             </tr>--%>

         </table>
                <div style="padding-left: 150px;padding-top:50px">
                    <asp:Button ID="btnsave" runat="server" Text="Submit" OnClick="btnsave_Click" Style="height: 28px; width: 120px;" ValidationGroup="CustomerValidationGroup" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

        
    

    <script type="text/javascript">
        $(function () {
            $('#CustomerDetails').dialog({
                height: 450,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Client Coookies details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            initializeCombobox();
        });     
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlCustomerList.ClientID %>").combobox();
          
       }
    </script>
</asp:Content>
