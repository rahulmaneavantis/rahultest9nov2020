﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Completed_NotCompleted_ChecklistReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFilters();
                dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }
        }

        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        //public DataTable GetGrid()
        //{
        //    dlFilters_SelectedIndexChanged(null, null);
        //    return (grdComplianceTransactions.DataSource as List<ComplianceInstanceTransactionView>).ToDataTable();
        //}

        //public string GetFilter()
        //{
        //    return Enumerations.GetEnumByID<CannedReportFilterForPerformer>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        //}
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(rblRole.SelectedItem.Text);

                    DataTable ExcelData = null;
                    string filter = "";
                    if (rblRole.SelectedIndex == 0)
                    {
                        DataView view = new System.Data.DataView(GetGrid());
                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "ComplianceCategoryName", "Branch", "Description", "Status", "ScheduledOn");
                        filter = GetFilter();
                    }
                  
              

                   // exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);
                    //Heading
                    if(!(filter.Equals("CategoryByEntity")||filter.Equals("RiskByEntity")))
                    {
                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B4"].Value = "Compliance ID";
                        exWorkSheet.Cells["B4"].AutoFitColumns(15);

                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C4"].Value = "Compliance Category Name";
                        exWorkSheet.Cells["C4"].AutoFitColumns(25);

                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D4"].Value = "Location";
                        exWorkSheet.Cells["D4"].AutoFitColumns(25);

                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E4"].Value = "Description";
                        exWorkSheet.Cells["E4"].AutoFitColumns(25);

                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F4"].Value = "Status";
                        exWorkSheet.Cells["F4"].AutoFitColumns(50);

                        exWorkSheet.Cells["G4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G4"].Value = "ScheduledOn";
                        exWorkSheet.Cells["G4"].AutoFitColumns(50);
                        
                    }
                    exWorkSheet.Cells["C2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C2"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = "CheckListReport_" + rblRole.SelectedItem.Text + "_" + filter;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 15;


                    int colIdx = 0;

                    colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                    exWorkSheet.Column(colIdx).Width = 20;


                    colIdx = ExcelData.Columns.IndexOf("ComplianceCategoryName") + 1;
                    exWorkSheet.Column(colIdx).Width = 20;

                    colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                    exWorkSheet.Column(colIdx).Width = 25;

                    colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                    exWorkSheet.Column(colIdx).Width = 30;


                    colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                    exWorkSheet.Column(colIdx).Width = 20;

                    colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                    exWorkSheet.Column(colIdx).Width = 25;

                    //if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                    //{
                    //    colIdx = ExcelData.Columns.IndexOf("ComCategoryName") + 1;
                    //    exWorkSheet.Column(colIdx).Width = 35;
                    //    colIdx = ExcelData.Columns.IndexOf("ComSubTypeName") + 1;
                    //    exWorkSheet.Column(colIdx).Width = 35;
                    //}
                    //else
                    //{
                    //    colIdx = ExcelData.Columns.IndexOf("IntcomCategoryName") + 1;
                    //    exWorkSheet.Column(colIdx).Width = 35;
                    //}


                    using (ExcelRange col = exWorkSheet.Cells[4,2, 4 + ExcelData.Rows.Count, 7])
                    {
                        col.Style.WrapText = true;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.AutoFitColumns();
                    }
                   


                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + rblRole.SelectedItem.Text + "_Report.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    // Response.End();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindFilters()
        {
            try
            {
                dlFilters.DataSource = Enumerations.GetAll<CheckListReportFilterForPerformerCompletedNotCompleted>();
                dlFilters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckListReportFilterForPerformerCompletedNotCompleted filter = (CheckListReportFilterForPerformerCompletedNotCompleted)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                grdComplianceTransactions.DataSource = CannedReportManagement.GetCheckListReportDataForPerformer(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, "N", filter);
                grdComplianceTransactions.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);
            return (grdComplianceTransactions.DataSource as List<SP_GetCheckListCannedReportCompliancesSummary_Result>).ToDataTable();
        }

        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CheckListReportFilterForPerformerCompletedNotCompleted>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                CheckListReportFilterForPerformerCompletedNotCompleted filter = (CheckListReportFilterForPerformerCompletedNotCompleted)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                var assignmentList = CannedReportManagement.GetCheckListReportDataForPerformer(Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID), AuthenticationHelper.UserID, "N", filter);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
       
    }
}