﻿<%@ Page Title="Revise Compliances" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ReviseCompliancesCA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ReviseCompliancesCA" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value
        {
            background-color: #ccc;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
  <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
  <asp:UpdatePanel ID="upReviseCompliances" runat="server">
        <ContentTemplate>
        <div style="margin-bottom: 4px">
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        </div> 
        <div style="margin-bottom: 7px; margin-left: 10px;margin-top: 10px;margin-right:20px; text-align:right">
         <asp:Button Text="Set Filter" runat="server" ID="btnSetFilter" OnClick="btnSetFilter_Click"
                CssClass="button" ValidationGroup="ReviseComplianceValidationGroup" CausesValidation="false" />
        </div>
        <div style="margin-bottom: 4px">
        <asp:GridView runat="server" ID="grdRviseCompliances" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px"  OnRowCommand="grdRviseCompliances_RowCommand"
            CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnRowDataBound="grdRviseCompliances_RowDataBound"
            OnPageIndexChanging="grdRviseCompliances_PageIndexChanging" Font-Size="12px" DataKeyNames="ScheduledOnID">
            <Columns>
                  <asp:TemplateField HeaderText ="Description" ItemStyle-Height="25px"  HeaderStyle-Height="20px" ItemStyle-Width="200px" SortExpression="Description">
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:600px">
                            <asp:Label ID="lblShortDiscription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
	            </asp:TemplateField>
                <asp:TemplateField HeaderText="Scheduled On" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn" >
                    <ItemTemplate>
                        <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="For Month" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth" >
                    <ItemTemplate>
                        <%# Eval("ForMonth")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"/>
                <asp:TemplateField ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkReviseCompliances" runat="server" CommandName="ReviseCompliance" CommandArgument='<%# Eval("ScheduledOnID") +" , "+ Eval("ComplianceInstanceID") %>'
                            ToolTip="Please Revise your compliances." ><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Revise Compliance" title="Revise Compliance" /></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <pagersettings position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
  <div id="divReviseFilter">
        <asp:UpdatePanel ID="upDownloadList" runat="server" OnLoad="upDownloadList_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" 
                            ValidationGroup="ReviseComplianceValidationGroup" />
                             <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                             ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Type</label>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px;
                            height: 22px;"  CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                     <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Select Category</label>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px;
                            height: 22px; " CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div style="margin-bottom: 7px">
                     <div style="margin-bottom: 7px">
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Act </label>
                                <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px;margin-left: 50px;
                                    width: 390px;" ReadOnly="true" CssClass="txtbox"  Text="< Select >"/>
                                <div style="margin-left: 150px; margin-left: 200px; position:absolute; z-index:50;overflow-y: auto;background:white;border:1px solid gray; height:200px;" id="dvActList" >
                                  <asp:Repeater ID="rptActList" runat="server">
                                        <HeaderTemplate>
                                            <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                                <tr>   
                                                    <td style="width:100px;" ><asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)"/></td>    
                                                    <td style="width:282px;"><asp:Button runat="server" ID="btnRepeater" Text="Ok" style="float:left"  OnClick="btnRefresh_Click"/></td>   
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        <tr>
                                            <td style="width:20px;"><asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width:200px;">
                                             <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:250px; padding-bottom:5px;">
                                            <asp:Label ID="lblActID"  runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                            <asp:Label ID="lblActName"  runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                        </div></td>   
                                            
                                        </tr>
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                   
                                </asp:Repeater>
                                
                                </div>
                             
                            </div>
                    <div runat="server" id="div3" style="margin-bottom: 7px;">
                        <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnRowDataBound="grdComplianceInstances_RowDataBound"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" AllowSorting="true" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" 
                            DataKeyNames="ID"  >
                            <Columns>
                               <asp:TemplateField   ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                <HeaderTemplate >
                                  <asp:CheckBox ID="chkFilterCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdComplianceInstances')" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkFilterCompliances" runat="server" />
                                </ItemTemplate>
                               </asp:TemplateField>
                                <asp:TemplateField HeaderText ="Description" ItemStyle-Width="200px" SortExpression="Description">
                                    <ItemTemplate>
                                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:600px">
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
	                           </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small"/>
                            <pagersettings position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311"  />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div style="margin-bottom: 30px;margin-top: 15px;">
                         <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                           Start Date</label>
                         <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; float:left; width: 200px;" ReadOnly="true"
                         MaxLength="200" />
                         <label style="width: 100px; padding-left: 30px; display: block; float:left; font-size: 13px; color: #333;">
                         End Date</label>
                          <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; float:left; width: 200px;" ReadOnly="true"
                          MaxLength="200" />
                    </div>
                    <div style="margin-bottom: 7px; float: right;margin-top: 30px;">
                        <asp:Button Text="Apply" runat="server" ID="btnApply"  OnClick="btnApply_Click" 
                            CssClass="button" ValidationGroup="ReviseComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClientClick="$('#divReviseFilter').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
  <div id="divReviseCompliance">
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" 
                        ValidationGroup="ReviseComplianceDetailsGroup" />
                    <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                        ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                </div>
                <fieldset style="border-style: solid;border-width: 1px;border-color: gray; margin-top:5px;">
                <legend>Compliance Details</legend>
                <asp:label id="lblShorDescription"  runat="server" style="display: block; float: left; font-size: 13px; color: #333;">
                          </asp:label>
                </fieldset>
               <fieldset style="border-style: solid;border-width: 1px;border-color: gray; margin-top:5px;">
                <legend>Revise Compliance Details</legend>
                     <div style="margin-bottom: 7px">
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Revise Date</label>
                        <asp:TextBox runat="server" ID="tbxDate"  Style="height: 20px;width: 390px;" />
                        <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                    </div>
                     <div style="margin-bottom: 7px">
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Remarks</label>
                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" Style="height: 50px;
                            width: 388px;" />
                            <asp:RequiredFieldValidator ErrorMessage="Please enter remark." ControlToValidate="tbxRemarks"
                            runat="server" ID="RequiredFieldValidator2" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Upload Compliance Document(s)</label>
                        <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" />
                         <asp:label ID="Label2" style="font-size: 11px;margin-left: -45px;" runat="server" text="(only Pdf,Execl,Jpeg and Jpg documents allowed)"></asp:label>
                         <asp:RequiredFieldValidator ErrorMessage="Please select documents for upload." ControlToValidate="fuSampleFile"
                            runat="server" ID="rfvFile" ValidationGroup="ReviseComplianceDetailsGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                        <label style="width: 250px; display: block; float: left; font-size: 13px; color: #333;">
                            Upload Working Files(s)</label>
                        <asp:FileUpload ID="FileUpload1" Multiple="Multiple" runat="server"/>
                        <asp:label ID="Label3" runat="server" style="font-size: 11px;margin-left: -45px;" text="(only Pdf,Execl,Jpeg and Jpg documents allowed)"></asp:label>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 250px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave"  CssClass="button" OnClientClick="if (!ValidateFile()) return false;"
                            ValidationGroup="ReviseComplianceDetailsGroup" OnClick="btnSave_Click"/>
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divReviseCompliance').dialog('close');" />
                    </div>
                    </fieldset>
                    <div style="margin-bottom: 7px; clear: both; margin-top:10px">
                        <asp:GridView runat="server" ID="grdReviseVersionHistory" AutoGenerateColumns="false" AllowSorting="true"
                            AllowPaging="true" PageSize="12" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                            BorderStyle="Solid" OnPageIndexChanging="grdReviseVersionHistory_OnPageIndexChanging" OnRowCreated="grdReviseVersionHistory_RowCreated"
                            BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnSorting="grdReviseVersionHistory_Sorting"
                            DataKeyNames="ID">
                            <Columns>
                                <asp:TemplateField HeaderText="Revise Date" SortExpression="VersionDate" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <%# Eval("VersionDate") != null ? Convert.ToDateTime(Eval("VersionDate")).ToString("dd-MMM-yyyy") : ""%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Version" HeaderText="Version" SortExpression="Version" ItemStyle-Width="10%"/>
                                <asp:BoundField DataField="VersionComment" HeaderText="Remark" SortExpression="VersionComment" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium"/>
                            <pagersettings position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#e1e1e1" />
                            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                No Records Found.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
  

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
   </asp:UpdatePanel>
  </div>
  <script type="text/javascript">
      $(function () {
          $('#divReviseFilter').dialog({
              height: 600,
              width: 900,
              autoOpen: false,
              draggable: true,
              title: "Manage Document Filter",
              open: function (type, data) {
                  $(this).parent().appendTo("form");
              }
          });

          $('#divReviseCompliance').dialog({
              height: 600,
              width: 900,
              autoOpen: false,
              draggable: true,
              title: "Revise Compliance Details",
              open: function (type, data) {
                  $(this).parent().appendTo("form");
              }
          });

      });


      function SelectheaderCheckboxes(headerchk, gridname) {
          var rolecolumn;
          var chkheaderid = headerchk.id.split("_");

          if (gridname == "grdComplianceDocument") {
              gvcheck = document.getElementById("<%=grdRviseCompliances.ClientID %>");
         } else {
             gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
         }

         var i;

         if (headerchk.checked) {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
             }
         }

         else {
             for (i = 0; i < gvcheck.rows.length; i++) {
                 gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
             }
         }
     }

     function Selectchildcheckboxes(header, gridname) {
         var i;
         var count = 0;
         var rolecolumn;
         var gvcheck;
         if (gridname == "grdComplianceDocument") {
             gvcheck = document.getElementById("<%=grdRviseCompliances.ClientID %>");
         } else {
             gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");
         }
         var headerchk = document.getElementById(header);
         var chkheaderid = header.split("_");

         var rowcount = gvcheck.rows.length;

         for (i = 1; i < gvcheck.rows.length - 1; i++) {
             if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                 count++;
             }
         }

         if (count == gvcheck.rows.length - 2) {
             headerchk.checked = true;
         }
         else {
             headerchk.checked = false;
         }
     }

     function initializeCombobox() {

         $("#<%= ddlComplinceCatagory.ClientID %>").combobox();
         $("#<%= ddlFilterComplianceType.ClientID %>").combobox();

     }

     function initializeReviseDate(date) {
         var startDate = new Date();
         $('#<%= tbxDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1
            });

            if (date != null) {
                $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function initializeDatePicker(date) {

            var startDate = new Date();
            $("#<%= txtStartDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             numberOfMonths: 1,
             onClose: function (startDate) {
                 $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
             }
         });

             $("#<%= txtEndDate.ClientID %>").datepicker({
             dateFormat: 'dd-mm-yy',
             defaultDate: startDate,
             numberOfMonths: 1,
             minDate: startDate,

         });


         if (date != null) {
             $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
             $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
         }
     }

     function initializeJQueryUI(textBoxID, divID) {
         $("#" + textBoxID).unbind('click');

         $("#" + textBoxID).click(function () {
             $("#" + divID).toggle("blind", null, 500, function () { });
         });
     }

     function checkAll(cb) {
         var ctrls = document.getElementsByTagName('input');
         for (var i = 0; i < ctrls.length; i++) {
             var cbox = ctrls[i];
             if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                 cbox.checked = cb.checked;
             }
         }
     }

     function UncheckHeader() {
         var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
         var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
         var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
         if (rowCheckBox.length == rowCheckBoxSelected.length) {
             rowCheckBoxHeader[0].checked = true;
         } else {

             rowCheckBoxHeader[0].checked = false;
         }
     }

     var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
     function ValidateFile() {

         var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
            var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }


            for (var i = 0; i < FileUpload1.length; i++) {
                var fileExtension = FileUpload1[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
            }
            return isValidFile;
        }

    </script>
</asp:Content>
