﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ComplianceTrackingSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceTrackingSummary" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div style="float: left; clear: both; font-family: Tahoma; font-size: 13px; background-color: #418CF1;
        height: 35px; width: 100%">
        <div style="float: left; clear: both; margin-top: 5px; margin-left: 10px; font-family: Tahoma;
            font-size: 13px;">
            <asp:HyperLink CssClass="sublink" ID="hpAssignLocation" runat="server" NavigateUrl="AssignCompliance.aspx?Param=location">View Assignments by Location</asp:HyperLink>
            <asp:HyperLink CssClass="sublink" ID="hpAssignUser" runat="server" NavigateUrl="AssignCompliance.aspx?Param=user">View Assignments by User</asp:HyperLink>
            <asp:HyperLink CssClass="sublink" ID="hpAssignCompliance" runat="server" NavigateUrl="AssignCompliance.aspx">Compliance Assignments</asp:HyperLink>
            <asp:HyperLink CssClass="sublink" ID="HpViewCompliance" runat="server" NavigateUrl="ComplianceTransactionList.aspx">View My Compliances</asp:HyperLink>
        </div>
    </div>
    <table border="0" cellpadding="10" cellspacing="10" style="clear: both">
        <tr>
            <td valign="top" style="width: 150px" align="center">
                <asp:Calendar ID="calHighlights" runat="server" SelectionMode="None" ShowNextPrevMonth="false">
                    <SelectedDayStyle />
                </asp:Calendar>
                <br />
                <asp:Calendar ID="calHighlightsNext" runat="server" SelectionMode="None" ShowNextPrevMonth="false">
                    <SelectedDayStyle />
                </asp:Calendar>
                <br />
                <asp:Calendar ID="calHighlightsNext2" runat="server" SelectionMode="None" ShowNextPrevMonth="false">
                    <SelectedDayStyle />
                </asp:Calendar>
            </td>
            <td valign="top" align="center">
                 <table>
                    <tr>
                        <td style="border-right: 1px solid #ccc; border-bottom: 1px solid #ccc">
                            <div style="float: left; clear: both; margin-top: 5px; margin-left: 50px">
                                <asp:Chart ID="chrtMyCompliances" runat="server" Width="300px" Height="300px">
                                    <Titles>
                                        <asp:Title Name="Users" Text="All Compliances" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                                        <%--<asp:Title  TextStyle="Shadow"  Font="Trebuchet MS, 14pt, style=Bold"  ></asp:Title>--%>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                        <td style="border-right: 1px solid #ccc; border-bottom: 1px solid #ccc">
                            <div style="float: left; margin-top: 5px; margin-left: 50px">
                                <asp:Chart ID="chrtCompliances" runat="server" Width="300px" Height="300px">
                                    <Titles>
                                        <asp:Title Text="Compliances as Performer" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                        <td style="border-bottom: 1px solid #ccc">
                            <div style="float: left; margin-top: 5px; margin-left: 50px">
                                <asp:Chart ID="chrtComplianceReview" runat="server" Width="300px" Height="300px">
                                    <Titles>
                                        <asp:Title Text="Compliances as Reviewer" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid #ccc;">
                            <div style="float: left; margin-top: 5px; margin-left: 50px">
                                <asp:Chart ID="chrtComplianceApproval" runat="server" Width="300px" Height="300px">
                                    <Titles>
                                        <asp:Title Text="Compliances as Approver" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                        <td style="border-right: 1px solid #ccc;">
                            <div style="float: left; margin-top: 5px; margin-left: 50px">
                                <asp:Chart ID="chrtByLocation" runat="server" Width="300px" Height="300px">
                                    <Titles>
                                        <asp:Title Name="Users" Text="Compliances by Location" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
