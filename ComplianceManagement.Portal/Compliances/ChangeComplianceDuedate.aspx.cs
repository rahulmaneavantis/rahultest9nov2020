﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ChangeComplianceDuedate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                BindActs();
                BindFinancialYear();
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
        }
        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActManagement.GetAllAssignedActs();
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindFinancialYear()
        {
            try
            {
                ddlFinancialYear.DataTextField = "Name";
                ddlFinancialYear.DataValueField = "ID";
                ddlFinancialYear.DataSource = GetCurrentFinancialYear();
                ddlFinancialYear.DataBind();
                ddlFinancialYear.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindCompliances(long actID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var bcompliances = (from row in entities.Acts
                                join row1 in entities.Compliances
                                on row.ID equals row1.ActID
                                join row2 in entities.ComplianceInstances
                                on row1.ID equals row2.ComplianceId
                                join row3 in entities.ComplianceAssignments
                                on row2.ID equals row3.ComplianceInstanceID
                                where row.IsDeleted == false && row1.IsDeleted == false
                                && row1.ActID == actID && row1.EventFlag == null && row1.Frequency != null
                                orderby row.Name ascending
                                select new { ID = row1.ID, Name = row1.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                    ddlCompliance.DataTextField = "Name";
                    ddlCompliance.DataValueField = "ID";
                    if (bcompliances != null)
                    {
                        ddlCompliance.DataSource = bcompliances;// ActManagement.GetAllAssignedCompliances(actID);
                        ddlCompliance.DataBind();
                    }                   
                    ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public List<object> GetCurrentFinancialYear()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var acts = (from row in entities.Mst_FinancialYear                            
                            select new { ID = row.Id, Name = row.FinancialYear }).OrderBy(entry => entry.Name).ToList<object>();

                return acts;                
            }
        }

        private void BindSchedulePeriods(long ComplianceID,string FinancialYear)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.DataTextField = "Name";
                    ddlPeriod.DataValueField = "ID";
                    if (FinancialYear != null)
                    {
                        int Frequency;
                        DateTime FromFinancialYear;
                        DateTime ToFinancialYear;
                        string[] ss = FinancialYear.Split('-');
                        if (ss.Length > 1)
                        {
                            string dtfrom = Convert.ToString("01" + "-04" + "-" + ss[0]);
                            string dtto = Convert.ToString("31" + "-03" + "-" + ss[1]);

                            var compliances = (from row in entities.Compliances
                                               where row.IsDeleted == false && row.Status == null
                                               && row.ID == ComplianceID
                                               select row).FirstOrDefault();
                            if (compliances != null)
                            {
                                Frequency = (int)compliances.Frequency;

                                if (Frequency == 0)
                                {
                                    #region //Monthly
                                    FromFinancialYear = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    ToFinancialYear = DateTime.ParseExact(dtto, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                    var months = MyFunction(FromFinancialYear, ToFinancialYear);
                                    foreach (var item in months)
                                    {
                                        var spitem = item.Split('=');
                                        if (spitem.Length > 1)
                                        {
                                            ddlPeriod.Items.Add(new ListItem(spitem[1].ToString(), spitem[0]));
                                        }
                                    }
                                    ddlPeriod.DataBind();
                                    ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
                                    #endregion
                                }
                                else if (Frequency == 1)
                                {
                                    #region//Quarterly
                                    var Quarters = GetFinancialQuarter(FinancialYear);
                                    foreach (var item in Quarters)
                                    {
                                        var spitem = item.Split('=');
                                        if (spitem.Length > 1)
                                        {
                                            ddlPeriod.Items.Add(new ListItem(spitem[1].ToString(), spitem[0]));
                                        }
                                    }
                                    ddlPeriod.DataBind();
                                    ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
                                    #endregion
                                }
                                else if (Frequency == 2)
                                {
                                    #region//Half Yearly
                                    List<string> halfyears = new List<string>();
                                    var checkcalOrFinancial = GetComplianceScheduleForMonth(ComplianceID);
                                    if (checkcalOrFinancial.Contains(1))
                                    {
                                        //Calender Year
                                        halfyears = GetHalfYear(FinancialYear, "C");
                                    }
                                    else if (checkcalOrFinancial.Contains(4))
                                    {
                                        //Financial Year
                                        halfyears = GetHalfYear(FinancialYear, "F");
                                    }
                                    if (halfyears.Count > 0)
                                    {
                                        foreach (var item in halfyears)
                                        {
                                            var spitem = item.Split('=');
                                            if (spitem.Length > 1)
                                            {
                                                ddlPeriod.Items.Add(new ListItem(spitem[1].ToString(), spitem[0]));
                                            }
                                        }
                                        ddlPeriod.DataBind();
                                        ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
                                    }
                                    #endregion
                                }
                                else if (Frequency == 3)
                                {
                                    #region//Annually
                                    List<string> annuallys = new List<string>();
                                    var checkcalOrFinancial = GetComplianceScheduleForMonth(ComplianceID);
                                    if (checkcalOrFinancial.Contains(1))
                                    {
                                        //Calender Year
                                        annuallys = GetAnnually(FinancialYear, "C");
                                    }
                                    else if (checkcalOrFinancial.Contains(4))
                                    {
                                        //Financial Year
                                        annuallys = GetAnnually(FinancialYear, "F");
                                    }
                                    if (annuallys.Count > 0)
                                    {
                                        foreach (var item in annuallys)
                                        {
                                            var spitem = item.Split('=');
                                            if (spitem.Length > 1)
                                            {
                                                ddlPeriod.Items.Add(new ListItem(spitem[1].ToString(), spitem[0]));
                                            }
                                        }
                                        ddlPeriod.DataBind();
                                        ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
                                    }
                                    #endregion
                                }
                                else if (Frequency == 4)
                                {
                                    #region//FourMonthly
                                    List<string> fourmonthlys = new List<string>();
                                    var checkcalOrFinancial = GetComplianceScheduleForMonth(ComplianceID);
                                    if (checkcalOrFinancial.Contains(1))
                                    {
                                        //Calender Year
                                        fourmonthlys = GetFourMonthly(FinancialYear, "C");
                                    }
                                    else if (checkcalOrFinancial.Contains(4))
                                    {
                                        //Financial Year
                                        fourmonthlys = GetFourMonthly(FinancialYear, "F");
                                    }
                                    if (fourmonthlys.Count > 0)
                                    {
                                        foreach (var item in fourmonthlys)
                                        {
                                            var spitem = item.Split('=');
                                            if (spitem.Length > 1)
                                            {
                                                ddlPeriod.Items.Add(new ListItem(spitem[1].ToString(), spitem[0]));
                                            }
                                        }
                                        ddlPeriod.DataBind();
                                        ddlPeriod.Items.Insert(0, new ListItem("< Select >", "-1"));
                                    }                                    
                                    #endregion
                                }                               
                            }//compliances                       
                        }//Check Length
                    }//FinancialYear

                }//Using
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        IEnumerable<DateTime> GetDates(DateTime date1, DateTime date2)
        {
            while (date1 <= date2)
            {
                yield return date1;
                date1 = date1.AddMonths(1);
            }            
        }

        public List<string> GetFinancialQuarter(string FinancialYear)
        {
            List<string> quarter = new List<string>();            
            string[] ss = FinancialYear.Split('-');
            if (ss.Length > 1)
            {
                quarter.Add("1="+"Apr " + ss[0].Substring(2, ss[0].Length - 2) + " - Jun " + ss[0].Substring(2, ss[0].Length - 2) + "");
                quarter.Add("4="+"Jul " + ss[0].Substring(2, ss[0].Length - 2) + " - Sep " + ss[0].Substring(2, ss[0].Length - 2) + "");
                quarter.Add("7="+"Oct " + ss[0].Substring(2, ss[0].Length - 2) + " - Dec " + ss[0].Substring(2, ss[0].Length - 2) + "");
                quarter.Add("10="+"Jan " + ss[1].Substring(2, ss[1].Length - 2) + " - Mar " + ss[1].Substring(2, ss[1].Length - 2) + "");                          
            }
            return quarter;            
        }
        public List<string> GetHalfYear(string FinancialYear,string iscalorFinancial)
        {
            List<string> HalfYear = new List<string>();
            string[] ss = FinancialYear.Split('-');
            if (ss.Length > 1)
            {
                if (iscalorFinancial == "C")
                {                    
                    HalfYear.Add("1=" + "Jan " + ss[0].Substring(2, ss[0].Length - 2) + " - Jun " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    HalfYear.Add("7=" + "Jul " + ss[0].Substring(2, ss[0].Length - 2) + " - Dec " + ss[0].Substring(2, ss[0].Length - 2) + "");
                }
                else if (iscalorFinancial == "F")
                {                   
                    HalfYear.Add("4=" + "Apr " + ss[0].Substring(2, ss[0].Length - 2) + " - Sep " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    HalfYear.Add("10=" + "Oct " + ss[0].Substring(2, ss[0].Length - 2) + " - Mar " + ss[1].Substring(2, ss[1].Length - 2) + "");
                }                
            }
            return HalfYear;
        }
        public List<string> GetAnnually(string FinancialYear, string iscalorFinancial)
        {
            List<string> Annually = new List<string>();
            string[] ss = FinancialYear.Split('-');
            if (ss.Length > 1)
            {
                if (iscalorFinancial == "C")
                {                    
                    Annually.Add("1=" + "CY " + ss[0].Substring(2, ss[0].Length - 2) + " - " + ss[1].Substring(2, ss[1].Length - 2) + "");                  
                }
                else if (iscalorFinancial == "F")
                {                    
                    Annually.Add("4=" + "FY " + ss[0].Substring(2, ss[0].Length - 2) + " - " + ss[1].Substring(2, ss[1].Length - 2) + "");                    
                }
            }
            return Annually;
        }
        public List<string> GetFourMonthly(string FinancialYear, string iscalorFinancial)
        {
            List<string> FourMonthly = new List<string>();
            string[] ss = FinancialYear.Split('-');
            if (ss.Length > 1)
            {
                if (iscalorFinancial == "C")
                {                    
                    FourMonthly.Add("1=" + "Jan " + ss[0].Substring(2, ss[0].Length - 2) + " - Apr " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    FourMonthly.Add("5=" + "May " + ss[0].Substring(2, ss[0].Length - 2) + " - Aug " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    FourMonthly.Add("9=" + "Sep " + ss[0].Substring(2, ss[0].Length - 2) + " - Dec " + ss[0].Substring(2, ss[0].Length - 2) + "");
                }
                else if (iscalorFinancial == "F")
                {
                    FourMonthly.Add("4=" + "Jan " + ss[0].Substring(2, ss[0].Length - 2) + " - Apr " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    FourMonthly.Add("8=" + "May " + ss[0].Substring(2, ss[0].Length - 2) + " - Aug " + ss[0].Substring(2, ss[0].Length - 2) + "");
                    FourMonthly.Add("12=" + "Sep " + ss[0].Substring(2, ss[0].Length - 2) + " - Dec " + ss[0].Substring(2, ss[0].Length - 2) + "");
                }
            }
            return FourMonthly;
        }
        
        public List<int> GetComplianceScheduleForMonth(long Complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var cschedule = (from row in entities.ComplianceSchedules
                                 where row.ComplianceID == Complianceid
                                 select row.ForMonth).ToList();

                return cschedule;
            }
        }
        List<string> MyFunction(DateTime date1, DateTime date2)
        {           
            return GetDates(date1, date2).Select(x => x.Month +"="+ x.ToString("MMM yy")).ToList();            
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlAct.SelectedValue) != -1)
            {
                BindCompliances(Convert.ToInt32(ddlAct.SelectedValue));
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(ddlCompliance.SelectedValue) != -1)
            //{
            //    BindSchedulePeriods(Convert.ToInt32(ddlCompliance.SelectedValue),"");
            //}

        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlCompliance.SelectedValue) != -1)
            {
                BindSchedulePeriods(Convert.ToInt32(ddlCompliance.SelectedValue), ddlFinancialYear.SelectedItem.Text);
            }

        }
        
        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Convert.ToInt32(ddlAct.SelectedValue) != -1 && Convert.ToInt32(ddlCompliance.SelectedValue) != -1 && Convert.ToString(ddlPeriod.SelectedItem.Text) != "< Select >")
                {

                    long actid = Convert.ToInt32(ddlAct.SelectedValue);
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                    string Period = Convert.ToString(ddlPeriod.SelectedItem.Text);

                    var ScheduleList = (from row in entities.Acts
                                        join row1 in entities.Compliances
                                        on row.ID equals row1.ActID
                                        join row2 in entities.ComplianceInstances
                                        on row1.ID equals row2.ComplianceId
                                        join row3 in entities.ComplianceAssignments
                                        on row2.ID equals row3.ComplianceInstanceID
                                        join row4 in entities.ComplianceScheduleOns
                                        on row3.ComplianceInstanceID equals row4.ComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false && row1.ActID == actid
                                        && row1.ID == complianced
                                        && row4.ForMonth.Equals(Period)
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                        select new
                                        {
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            ActName = row.Name,                                              
                                            row1.ShortDescription,
                                            row4.ForMonth,
                                            row4.ScheduleOn
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();
                }
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool Exists(Mst_ComplianceScheduleOn MSCO)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Mst_ComplianceScheduleOn
                             where row.ComplianceID== MSCO.ComplianceID
                             && row.ScheduleOnDate==MSCO.ScheduleOnDate
                             && row.ForMonth==MSCO.ForMonth
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }               
            }
        }
        public static void CreateMSCO(Mst_ComplianceScheduleOn MSCO)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Mst_ComplianceScheduleOn.Add(MSCO);
                entities.SaveChanges();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Mst_ComplianceScheduleOn eventData = new Mst_ComplianceScheduleOn()
                    {
                        ComplianceID = Convert.ToInt32(ddlCompliance.SelectedValue),
                        ForMonth = ddlPeriod.SelectedItem.Text,
                        ForPeriod = Convert.ToInt32(ddlPeriod.SelectedValue),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Today.Date,
                        ScheduleOnDate=DateTimeExtensions.GetDate(txtSchedueleDate.Text),
                        IsDeleted=false
                    };
                    if (Exists(eventData))
                    {                    
                        cvDuplicateEntry.ErrorMessage = "Same Compliance Schedule already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        CreateMSCO(eventData);
                    }                    
                    ddlAct.SelectedValue = "-1";
                    ddlCompliance.SelectedValue = "-1";
                    ddlPeriod.SelectedValue = "-1";
                    cvDuplicateEntry.IsValid = false;                    
                    txtSchedueleDate.Text = "";                    
                    cvDuplicateEntry.ErrorMessage = "Schedule Saved successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        
    }
}