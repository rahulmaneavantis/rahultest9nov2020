﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActSummaryDetail : System.Web.UI.Page
    {
        //static bool ReviewerFlag;
        protected static List<Int32> roles;                
        protected static List<SP_Get_ComplianceAssignedbyActClosed_Result> ClosedDetails;                
        protected static List<SP_Get_ComplianceAssignedbyActOverdue_Result> OverdueDetails;                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //ReviewerFlag = false;                                                                           
                    BindLocationFilter();
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    //if (roles.Contains(6))
                    //{
                    //    ReviewerFlag = true;
                    //    ShowReviewer(sender, e);
                    //}
                    btnSearch_Click(sender, e);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        //protected void upDownloadList_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "S";

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //FillComplianceDocuments();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = 0;
                //}
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected string GetOverdueTotals(int ActId, int CustBranchId)
        {
            try
            {
                string result = "";

                if (ActId > 0 && CustBranchId > 0)
                {
                    var output = OverdueDetails.Where(x => x.ActId == ActId && x.CustomerbranchID == CustBranchId).ToList();
                    result = Convert.ToString(output.Count);
                }                                   
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }
        protected string GetClosedTotals(int ActId,int CustBranchId)
        {
            try
            {
                string result = "";

                if (ActId > 0 && CustBranchId > 0)
                {
                    var output = ClosedDetails.Where(x => x.ActId == ActId && x.CustomerbranchID == CustBranchId).ToList();
                    result = Convert.ToString(output.Count);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }
        public void FillComplianceDocuments()
        {
            try
            {
                int RoleId = 7;
                string Role = Convert.ToString(AuthenticationHelper.Role);
                if (Role.Equals("MGMT"))
                {
                    RoleId = 8;
                }
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                String location = tvFilterLocation.SelectedNode.Text;
                Session["TotalRows"] = 0;

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                var dataSource = Business.ComplianceManagement.GetAssignedActDetils(Convert.ToInt32(AuthenticationHelper.UserID), RoleId).ToList();
                if (dataSource.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 180;
                        ClosedDetails = entities.SP_Get_ComplianceAssignedbyActClosed(Convert.ToInt32(AuthenticationHelper.UserID), RoleId, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();

                        entities.Database.CommandTimeout = 180;
                        OverdueDetails = entities.SP_Get_ComplianceAssignedbyActOverdue(Convert.ToInt32(AuthenticationHelper.UserID), RoleId, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();
                    }
                    if (branchID > 0)
                        dataSource = dataSource.Where(x => x.CustomerbranchID == branchID).ToList();
                }
                grdReviewerComplianceDocument.DataSource = dataSource;
                grdReviewerComplianceDocument.DataBind();
                Session["TotalRows"] = dataSource.Count;
                grdReviewerComplianceDocument.Visible = true;
                GetPageDisplaySummary();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                //if (ReviewerFlag)
                //{
                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //}
                //Reload the Grid
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public class ReportExportColumn
        {
            public string ActName { get; set; }
            public string locationName { get; set; }
            public string ComplianceTotal { get; set; }
            public string OverdueCompliance { get; set; }
            public string ClosedCompliance { get; set; }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                List<ReportExportColumn> rpt = new List<ReportExportColumn>();

                int RoleId = 7;
                string Role = Convert.ToString(AuthenticationHelper.Role);
                if (Role.Equals("MGMT"))
                {
                    RoleId = 8;
                }
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                String location = tvFilterLocation.SelectedNode.Text;

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                var dataSource = Business.ComplianceManagement.GetAssignedActDetils(Convert.ToInt32(AuthenticationHelper.UserID), RoleId).ToList();
                if (dataSource.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 180;
                        ClosedDetails = entities.SP_Get_ComplianceAssignedbyActClosed(Convert.ToInt32(AuthenticationHelper.UserID), RoleId, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();

                        entities.Database.CommandTimeout = 180;
                        OverdueDetails = entities.SP_Get_ComplianceAssignedbyActOverdue(Convert.ToInt32(AuthenticationHelper.UserID), RoleId, Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();
                    }
                    if (branchID > 0)
                        dataSource = dataSource.Where(x => x.CustomerbranchID == branchID).ToList();
                }

                if (dataSource.Count > 0)
                {
                    foreach (var item in dataSource)
                    {
                        rpt.Add(new ReportExportColumn
                        {
                            ActName = item.ActName,
                            locationName = item.locationName,
                            ComplianceTotal = item.ComplianceTotal.ToString(),
                            OverdueCompliance = GetOverdueTotals(item.ActID, item.CustomerbranchID),
                            ClosedCompliance = GetClosedTotals(item.ActID, item.CustomerbranchID),
                        });
                    }
                }

                if (rpt.Count > 0)
                {
                    String FileName = "Act Summary Detail";
                    String ReportName = "Act Summary Detail";

                    DataView view = new System.Data.DataView((rpt as List<ReportExportColumn>).ToDataTable());
                    
                    DataTable dataToExport = view.ToTable("Selected", false, "ActName", "locationName", "ComplianceTotal", "OverdueCompliance", "ClosedCompliance");

                    using (ExcelPackage pck = new ExcelPackage())
                    {                        
                        ExcelWorksheet exWorkSheet1 = pck.Workbook.Worksheets.Add(ReportName);
                        exWorkSheet1.Cells["A1"].Value = "Act Name";
                        exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["A1"].AutoFitColumns(30);
                        exWorkSheet1.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet1.Cells["B1"].Value = "Location Name";
                        exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["B1"].AutoFitColumns(30);
                        exWorkSheet1.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet1.Cells["C1"].Value = "Compliance Total";
                        exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["C1"].AutoFitColumns(15);
                        exWorkSheet1.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet1.Cells["D1"].Value = "Overdue Compliance";
                        exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["D1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet1.Cells["E1"].Value = "Closed Compliance";
                        exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet1.Cells["E1"].AutoFitColumns(20);
                        exWorkSheet1.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet1.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                        using (ExcelRange col = exWorkSheet1.Cells[1, 1, 1 + dataToExport.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        exWorkSheet1.Cells["A1"].LoadFromDataTable(dataToExport, true);
                        Byte[] fileBytes = pck.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}