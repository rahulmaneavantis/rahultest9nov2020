﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ChecklistPerform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
                {
                    string[] commandArg = Request.QueryString["ComplianceScheduleID"].ToString().Split(',');

                    int IsStatutory = Convert.ToInt32(Request.QueryString["IsFlag"]);

                    int IsApplicable = Convert.ToInt32(Request.QueryString["IsApplicable"]);

                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        foreach (var gvrow in commandArg)
                        {
                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                                if (IsStatutory == -1 || IsStatutory == 1) //Stautory 
                                {
                                    if (IsApplicable == 1) //Applicable 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.ComplianceManagement.GetComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            ComplianceTransaction transaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = Convert.ToInt32(ComplianceInstanceID),
                                                ComplianceScheduleOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 4,//closed timely
                                                Remarks = "Closed Timely"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.ComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                    else if (IsApplicable == 2) //Not Complied 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.ComplianceManagement.GetComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            ComplianceTransaction transaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = Convert.ToInt32(ComplianceInstanceID),
                                                ComplianceScheduleOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 17,//Not Complied 
                                                Remarks = "Not Complied"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.ComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                    else //Not Applicable 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.ComplianceManagement.GetComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            ComplianceTransaction transaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = Convert.ToInt32(ComplianceInstanceID),
                                                ComplianceScheduleOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 15,
                                                Remarks = "Not Applicable"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.ComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                else if (IsStatutory == 0) //Internal 
                                {
                                    if (IsApplicable == 1) //Applicable 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.InternalComplianceManagement.GetInternalComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = Convert.ToInt32(ComplianceInstanceID),
                                                InternalComplianceScheduledOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 4,//closed timely
                                                Remarks = "Closed Timely"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.InternalComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                    else if (IsApplicable == 2) //Not Complied 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.InternalComplianceManagement.GetInternalComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = Convert.ToInt32(ComplianceInstanceID),
                                                InternalComplianceScheduledOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 17,//Not Complied 
                                                Remarks = "Not Complied"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.InternalComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                    else //Not Applicable 
                                    {
                                        var ComplianceInstanceID = ComplianceManagement.Business.InternalComplianceManagement.GetInternalComplianceInstanceID(ScheduledOnID);
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                            {
                                                InternalComplianceInstanceID = Convert.ToInt32(ComplianceInstanceID),
                                                InternalComplianceScheduledOnID = ScheduledOnID,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                StatusId = 15,
                                                Remarks = "Not Applicable"
                                            };
                                            transaction.Dated = DateTime.Now;
                                            transaction.StatusChangedOn = DateTime.Now;
                                            entities.InternalComplianceTransactions.Add(transaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                        }
                    }
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}