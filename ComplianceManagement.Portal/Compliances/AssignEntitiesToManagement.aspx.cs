﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class AssignEntitiesToManagement : System.Web.UI.Page
    {
        public static List<long> Branchlist = new List<long>();
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;
                BindDitributor();
                //BindLocationFilter();
                BindComplianceEntityInstances();
                BindCategory();
                BindUsers(ddlUsers, ddlFilterUsers, delddluser);                                                
                //BindLocation();
                tbxBranch.Attributes.Add("readonly", "readonly");
                TextBox1.Attributes.Add("readonly", "readonly");
                Branchlist.Clear();
                tbxFilterLocation.Text = "< Select >";               
            }
        }

        private void BindCategory()
        {
            try
            {
                var catagorydetails= ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";

                ddlComplianceCatagory.DataSource = catagorydetails;
                ddlComplianceCatagory.DataBind();

                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlComplianceCatagory.Items.Insert(1, new ListItem("Select All", "All"));


                delddlcomcatagory.DataTextField = "Name";
                delddlcomcatagory.DataValueField = "ID";

                delddlcomcatagory.DataSource = catagorydetails;
                delddlcomcatagory.DataBind();

                delddlcomcatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
                delddlcomcatagory.Items.Insert(1, new ListItem("Select All", "All"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceEntityInstances()
        {
            try
            {
                int branchID = -1;
                int customerID = -1;
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }
                 customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);

                locationList.Clear();
                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<ComplianceAssignmentEntitiesView> masterlist = new List<ComplianceAssignmentEntitiesView>();
                if (locationList.Count>0)
                {
                    masterlist = AssignEntityManagement.SelectAllEntitiesList(userID, customerID, locationList);                    
                }
                else
                {
                    masterlist = AssignEntityManagement.SelectAllEntities(branchID, userID, customerID);
                }
                grdAssignEntities.DataSource = masterlist;
                grdAssignEntities.DataBind();
                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }     
        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox1.Text = TreeView1.SelectedNode != null ? TreeView1.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_LoadDelete(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesDelete", string.Format("initializeJQueryUI('{0}', 'divBranches1');", TextBox1.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

     

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "-1")
                    {
                        tvFilterLocation.Nodes.Clear();
                        customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);                           
                        var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                        foreach (var item in bracnhes)
                        {
                            TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                            node.SelectAction = TreeNodeSelectAction.Expand;
                            BindBranchesHierarchy(node, item);
                            tvFilterLocation.Nodes.Add(node);
                        }
                        tvFilterLocation.CollapseAll();
                    }
                }          
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) || !string.IsNullOrEmpty(delddlCustomer.SelectedValue)  )
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    if (delddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(delddlCustomer.SelectedValue);
                    }
                }
                tvBranches.Nodes.Clear();
                TreeView1.Nodes.Clear();
               
                var bracnhes = CustomerBranchManagement.GetAllHierarchyForMappedLocation(customerID);

                TreeNode firstnode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    firstnode = new TreeNode(item.Name, item.ID.ToString());
                    firstnode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(firstnode, item);
                    tvBranches.Nodes.Add(firstnode);
                }
                tvBranches.CollapseAll();

                TreeNode secondnode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    secondnode = new TreeNode(item.Name, item.ID.ToString());
                    secondnode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(secondnode, item);
                    TreeView1.Nodes.Add(secondnode);
                }
                TreeView1.CollapseAll();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //sandesh code start
        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {                
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        //protected void RetrieveNodes(TreeNode node)
        //{

        //    if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
        //    {
        //        if (!locationList.Contains(Convert.ToInt32(node.Value)))
        //            locationList.Add(Convert.ToInt32(node.Value));
        //    }

        //    foreach (TreeNode tn in node.ChildNodes)
        //    {

        //        if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
        //        {
        //            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
        //                locationList.Add(Convert.ToInt32(tn.Value));

        //        }

        //        if (tn.ChildNodes.Count != 0)
        //        {
        //            for (int i = 0; i < tn.ChildNodes.Count; i++)
        //            {
        //                RetrieveNodes(tn.ChildNodes[i]);
        //            }
        //        }
        //    }
        //}

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != AuthenticationHelper.CustomerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != AuthenticationHelper.CustomerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        //sandesh code End

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }


        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();         
        }
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }


       
        private void BindUsers(DropDownList ddlUserList, DropDownList ddlFilterUserList, DropDownList deleteUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                int complianceProductType = 0;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;                    
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                }
                else if (AuthenticationHelper.Role == "MGMT")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int? ParentID = ICIAManagement.GetParentIDforCustomer(customerID);
                    customerID = Convert.ToInt32(ParentID ?? 0);
                    complianceProductType = AuthenticationHelper.ComplianceProductType;
                }
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";            
                ddlUserList.Items.Clear();

                ddlFilterUserList.DataTextField = "Name";
                ddlFilterUserList.DataValueField = "ID";
                ddlFilterUserList.Items.Clear();

                deleteUserList.DataTextField = "Name";
                deleteUserList.DataValueField = "ID";
                deleteUserList.Items.Clear();
                
                var users = UserManagement.GetAllManagmentUser(customerID, complianceProductType);
                //var filterusers = users;
                //var fdeleteusers = users;

                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                //filterusers.Insert(0, new { ID = -1, Name = ddlFilterUserList == ddlFilterUsers ? "< Select >" : "< All >" });
                ddlFilterUserList.DataSource = users;
                ddlFilterUserList.DataBind();
                
                //fdeleteusers.Insert(0, new { ID = -1, Name = deleteUserList == delddluser ? "< Select >" : "< All >" });                                
                deleteUserList.DataSource = users;
                deleteUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                long customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                if (!ddlComplianceCatagory.SelectedValue.Equals("All")) //Single category
                {
                    //if (branchId == -2)  //All branch
                    //{
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var customerBranches = (from row in entities.CustomerBranches
                                                        where row.IsDeleted == false && row.CustomerID == AuthenticationHelper.CustomerID
                                                        select row);


                       // foreach (var node in customerBranches)
                        //{
                       // foreach (var item in Branchlistloop)
                       foreach (TreeNode node in tvBranches.CheckedNodes)
                        {
                            int CategoryId = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                                    var data = AssignEntityManagement.SelectEntity(Convert.ToInt32(node.Value), userID, CategoryId);

                                    if (data != null)
                                    {
                                       cvDuplicateEntry.IsValid = false;
                                       cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                                       ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                                    }
                                    else
                                    {
                                        EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                                        objEntitiesAssignment.UserID = userID;
                                        objEntitiesAssignment.BranchID = Convert.ToInt32(node.Value);
                                        objEntitiesAssignment.ComplianceCatagoryID = CategoryId;
                                        objEntitiesAssignment.CreatedOn = DateTime.UtcNow;

                                        AssignEntityManagement.Create(objEntitiesAssignment);
                                       
                                    }
                                     ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                }
                            }
                        }

                else
                {

               var CatagoryList = ComplianceCategoryManagement.GetAll();
                List<EntitiesAssignment> assignmentEntities = new List<EntitiesAssignment>();
                //foreach (var item in Branchlistloop)
                foreach (TreeNode node in tvBranches.CheckedNodes)
                {
                    int branchId1 = Convert.ToInt32(node.Value);


                    foreach (ComplianceCategory catagory in CatagoryList)
                    {
                        var data = AssignEntityManagement.SelectEntity(branchId1, userID, catagory.ID);
                        if (!(data != null))
                        {
                            EntitiesAssignment objEntitiesAssignment = new EntitiesAssignment();
                            objEntitiesAssignment.UserID = userID;
                            objEntitiesAssignment.BranchID = Convert.ToInt32(branchId1);
                            objEntitiesAssignment.ComplianceCatagoryID = catagory.ID;
                            objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                            assignmentEntities.Add(objEntitiesAssignment);

                           // AssignEntityManagement.Create(assignmentEntities);
                            AssignEntityManagement.Create(objEntitiesAssignment);
                            }
                        else
                         {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                         }
                    }
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                    //}
                }


                BindComplianceEntityInstances();
               
              
            }
            catch (Exception ex)
            {
               
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
             {                
                lblErrorMassage.Text = "";
                ddlUsers.SelectedValue = "-1";
                ddlComplianceCatagory.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
                UpdatePanel1.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ForceCloseDeleteFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
        }

        protected void btnDeleteComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (TreeView1.SelectedNode != null)
                {
                    TreeView1.SelectedNode.Selected = false;
                }

                Label3.Text = "";
                delddluser.SelectedValue = "-1";
                delddlcomcatagory.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('open');", true);
                TextBox1.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                UpdatePanel3.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();            
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }
                int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                var assignmentList = AssignEntityManagement.SelectAllEntities(branchID, userID, customerID);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdAssignEntities.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                    }
                }
                grdAssignEntities.DataSource = assignmentList;
                grdAssignEntities.DataBind();
                tbxFilterLocation.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                long Uid = -1;
                long CatagoryId = -1;
                Uid = Convert.ToInt32(delddluser.SelectedValue);
                if (!delddlcomcatagory.SelectedItem.Text.Equals("< Select >"))
                {
                    if (delddlcomcatagory.SelectedItem.Text.Equals("Select All"))
                    {
                        //Branch and Category All selected
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            foreach (TreeNode node in TreeView1.CheckedNodes)
                            {
                                var CID = Convert.ToInt32(node.Value);
                                entities.EntitiesAssignments.RemoveRange(entities.EntitiesAssignments.Where(x => x.BranchID == CID && x.UserID == Uid).ToList());
                                entities.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        CatagoryId = Convert.ToInt32(delddlcomcatagory.SelectedValue);
                        //Branch all and Category selected
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var data = (from row in entities.EntitiesAssignments
                                        where row.UserID == Uid
                                        && row.ComplianceCatagoryID == CatagoryId
                                        select row).ToList();
                            foreach (TreeNode node in TreeView1.CheckedNodes)
                            {
                                var CID = Convert.ToInt32(node.Value);
                                entities.EntitiesAssignments.RemoveRange(entities.EntitiesAssignments.Where(x => x.BranchID == CID && x.UserID == Uid && x.ComplianceCatagoryID == CatagoryId).ToList());
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "-1")
                    {
                        if (!string.IsNullOrEmpty(ddlFilterServiceProvider.SelectedValue))
                        {
                            if (ddlFilterServiceProvider.SelectedValue != "-1")
                            {
                                BindLocationFilter();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void ddlFilterServiceProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterServiceProvider.SelectedValue))
                {
                    if (ddlFilterServiceProvider.SelectedValue != "-1")
                    {
                        BindCustomer(Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomer(int distributorID)
        {
            try
            {
                var data = GetAllDistributorCustomer(distributorID);
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = data;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));


                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = data;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                delddlCustomer.DataTextField = "Name";
                delddlCustomer.DataValueField = "ID";
                delddlCustomer.DataSource = data;
                delddlCustomer.DataBind();
                delddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDitributor()
        {
            try
            {
                var data = GetAllDistributor();
                ddlFilterServiceProvider.DataTextField = "Name";
                ddlFilterServiceProvider.DataValueField = "ID";
                ddlFilterServiceProvider.DataSource = data;
                ddlFilterServiceProvider.DataBind();
                ddlFilterServiceProvider.Items.Insert(0, new ListItem("< Select ServiceProvider >", "-1"));


                ddlServiceProvider.DataTextField = "Name";
                ddlServiceProvider.DataValueField = "ID";
                ddlServiceProvider.DataSource = data;
                ddlServiceProvider.DataBind();
                ddlServiceProvider.Items.Insert(0, new ListItem("< Select ServiceProvider >", "-1"));


                delddlServiceProvider.DataTextField = "Name";
                delddlServiceProvider.DataValueField = "ID";
                delddlServiceProvider.DataSource = data;
                delddlServiceProvider.DataBind();
                delddlServiceProvider.Items.Insert(0, new ListItem("< Select ServiceProvider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Customer> GetAllDistributor()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.IsDistributor == true
                                 select row);

                if (customers.Count() > 0)
                {
                    if (AuthenticationHelper.Role == "DADMN")
                    {
                        customers = customers.Where(entry => entry.ID == AuthenticationHelper.CustomerID);
                    }
                    if (AuthenticationHelper.Role == "MGMT")
                    {
                        customers = customers.Where(entry => entry.ID == AuthenticationHelper.CustomerID);
                    }
                    customers = customers.OrderBy(row => row.Name);
                }
                return customers.ToList();
            }
        }
        public static List<Customer> GetAllDistributorCustomer(int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.ParentID == distributorid
                                 select row);


                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }


        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(ddlServiceProvider.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1" && ddlServiceProvider.SelectedValue != "-1")
                    {
                        BindLocation();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void ddlServiceProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlServiceProvider.SelectedValue))
                {
                    if (ddlServiceProvider.SelectedValue != "-1")
                    {
                        BindCustomer(Convert.ToInt32(ddlServiceProvider.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void delddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(delddlCustomer.SelectedValue) && !string.IsNullOrEmpty(delddlServiceProvider.SelectedValue))
                {
                    if (delddlCustomer.SelectedValue != "-1" && delddlServiceProvider.SelectedValue != "-1")
                    {
                        BindLocation();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void delddlServiceProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(delddlServiceProvider.SelectedValue))
                {
                    if (delddlServiceProvider.SelectedValue != "-1")
                    {
                        BindCustomer(Convert.ToInt32(delddlServiceProvider.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}