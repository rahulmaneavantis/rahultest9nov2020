﻿<%@ Page Title="Canned Reports" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="CannedReportsCA.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CannedReportsCA" %>

<%@ Register Src="~/Controls/CannedReportPerformerCA.ascx" TagName="CannedReportPerformerCA"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/CannedReportReviewerCA.ascx" TagName="CannedReportReviewerCA"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/CannedReportApprover.ascx" TagName="CannedReportApprover"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div style="margin: 10px 20px 10px 10px">
    <div>
        Role :
        <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow"
            OnSelectedIndexChanged="rblRole_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Performer" Selected="True" />
            <asp:ListItem Text="Reviewer" />
            <asp:ListItem Text="Approver" />
        </asp:RadioButtonList>
       </div>
       <div style="width:100px;float:left;margin-left: 335px;margin-top: -30px;">   
         <asp:LinkButton runat="server"  ID="lbtnExportExcel" style="margin-top:15px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
          title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
      </div>
     </div>
   
    <vit:CannedReportPerformerCA runat="server" ID="udcCannedReportPerformer" />
    <vit:CannedReportReviewerCA runat="server" ID="udcCannedReportReviewer" Visible="false" />
    <vit:CannedReportApprover runat="server" ID="udcCannedReportApprover" Visible="false" />
    <%--<asp:PlaceHolder runat="server" ID="phCannedReport" />--%>
</asp:Content>
