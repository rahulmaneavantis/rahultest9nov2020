﻿<%@ Page Title="Approve Compliances" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ApproveCompliancesNew.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ApproveCompliancesNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function showHide(id) {

            var elem = document.getElementById('d' + id);
            var img = document.getElementById('I' + id);
            if (elem) {
                if (elem.style.display != 'block') {
                    elem.style.display = 'block';
                    elem.style.visibility = 'visible';
                    img.src = img.src.replace("plus", "minus");
                }
                else {
                    elem.style.display = 'none';
                    elem.style.visibility = 'hidden';
                    img.src = img.src.replace("minus", "plus");
                }
            }
            return false;
        }

        function Confirm(category) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure you want approve all compliances of category " + category + "?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }



    </script>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

    </script>
    
    <style type="text/css">

        .clspenaltysave
        {
            font-weight:bold;
            margin-left :15px;
        }
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

          .Inforamative{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
    tr.Inforamative > td{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
       

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

     /*.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}*/

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div>
        <div style="margin-bottom: 4px">
            <asp:ValidationSummary runat="server" class="alert alert-block alert-danger fade in"
                ValidationGroup="ComplianceInstanceValidationGroup" />
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            <asp:Label ID="lblMessage" runat="server" Style="text-align: center; color: #666666;"></asp:Label>
        </div>
        <div class="row Dashboard-white-widget">
            <div class="dashboard">
                <div class="col-lg-12 col-md-12 ">
                    <section class="panel">
        <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liStatutory" OnClick="liStatutory_Click" runat="server">Statutory</asp:LinkButton>
                                                    </li>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liInternal" OnClick="liInternal_Click" runat="server">Internal</asp:LinkButton>
                                                    </li>
                                        </ul>
                                </header>

        <div id="mainDiv" runat="server">
            <asp:Repeater ID="rptCatagory" runat="server" OnItemDataBound="lstCatagory_ItemDataBound">
                <ItemTemplate>
                    <div style="margin-top: 10px;" class="test">
                        <asp:Label ID="lblCatagoryName" runat="server" Text='<%# Eval("Catagory") %>' Style="font-size: medium; color: #666666;" Width="250px"></asp:Label>
                    </div> 
                    <asp:LinkButton ID="lbtApprover" Text="Approve" CommandArgument='<%# Eval("ComplianceCatagoryID") %>' CommandName='<%# Eval("Catagory") %>'
                            CssClass="btn btn-search" OnCommand="lbtApprover_Command" runat="server" Style="margin-top: -2%; margin-left: 50%;"></asp:LinkButton>
                    <asp:Repeater ID="repActList" runat="server" OnItemDataBound="repActList_ItemDataBound">
                        <ItemTemplate>
                            <div id='h1<%# Eval("ActID") %>' class="header" onclick='showHide(<%# Eval("ActID") %>);' style="margin-left: -5px; margin-top: 10px; min-height: 0px !important; height: 36px; ">
                                <table>
                                    <tr>
                                        <td>
                                            <img id='I<%# Eval("ActID") %>' src="../Images/plus.png" style="width: 15px; height: 15px;" /></td>
                                        <td>
                                            <asp:Label ID="lblActID" runat="server" Text='<%# Eval("ActID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblActName" runat="server" Text='<%# Eval("ActName") %>'></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                            <div id='d<%# Eval("ActID") %>' style="margin-left: 40px; display: none;    margin-top: 3%;">
                                <asp:Repeater runat="server" ID="rptCompliancesList">
                                    <ItemTemplate>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="border-bottom: 1px solid #dddddd; border-top: 1px solid #dddddd;line-height: 3;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px;">
                                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription")%>'></asp:Label>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="rptInternalCategory" runat="server" OnItemDataBound="lstInternalCatagory_ItemDataBound">
                <ItemTemplate>
                    <div style="margin-top: 10px;" class="test">
                        <asp:Label ID="lblCatagoryName" runat="server" Text='<%# Eval("Catagory") %>' Style="font-size: medium; color: #666666;" Width="250px"></asp:Label>
                    </div> 
                    <asp:LinkButton ID="lbtInternalApprover" Text="Approve" CommandArgument='<%# Eval("ComplianceCatagoryID") %>' CommandName='<%# Eval("Catagory") %>'
                            CssClass="btn btn-search" OnCommand="lbtInternalApprover_Command" runat="server" Style="margin-top: -2%; margin-left: 50%;"></asp:LinkButton>
                    <asp:Repeater ID="repCatagoryist" runat="server" OnItemDataBound="repCatagoryist_ItemDataBound">
                        <ItemTemplate>
                            <div id='h1<%# Eval("ComplianceCatagoryID") %>' class="header" onclick='showHide(<%# Eval("ComplianceCatagoryID") %>);' style="margin-left: -5px; margin-top: 10px; min-height: 0px !important; height: 36px; ">
                                <table>
                                    <tr>
                                        <td>
                                            <img id='I<%# Eval("ComplianceCatagoryID") %>' src="../Images/plus.png" style="width: 15px; height: 15px;" /></td>
                                        <td>
                                            <asp:Label ID="lblComplianceCatagoryID" runat="server" Text='<%# Eval("ComplianceCatagoryID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCatagory" runat="server" Text='<%# Eval("Catagory") %>'></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                            <div id='d<%# Eval("ComplianceCatagoryID") %>' style="margin-left: 40px; display: none;    margin-top: 3%;">
                                <asp:Repeater runat="server" ID="rptInternalCompliancesList">
                                    <ItemTemplate>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="border-bottom: 1px solid #dddddd; border-top: 1px solid #dddddd;line-height: 3;">
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px;">
                                                        <asp:Label ID="lblIShortDescription" runat="server" Text='<%# Eval("ShortDescription")%>'></asp:Label>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </div>
             </section>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            setactivemenu('leftApprovermenu');
            fhead('My Approvals');
        });
    </script>
</asp:Content>

