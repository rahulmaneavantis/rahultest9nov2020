﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class HolidayList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceTypes(ddlComplianceType);
                BindComplianceTypes(ddlFilterComplianceType);
                BindStates(ddlState);
                BindStates(ddlFilterState);
                BindGrid();
            }
        }
        private void BindStates(DropDownList ddlStateList)
        {
            try
            {
               
                ddlStateList.DataTextField = "Name";
                ddlStateList.DataValueField = "ID";

                ddlStateList.DataSource = AddressManagement.GetAllStates();
                ddlStateList.DataBind();

                ddlStateList.Items.Insert(0, new ListItem("< Select State >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindComplianceTypes(DropDownList ddlTypeList)
        {
            try
            {
                ddlTypeList.DataTextField = "Name";
                ddlTypeList.DataValueField = "ID";

                ddlTypeList.DataSource = ComplianceTypeManagement.GetAll();
                ddlTypeList.DataBind();

                ddlTypeList.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindGrid()
        {
            int complianceTypeID = -1;
            int stateID = -1;
            string filterText = string.Empty;

            if (!string.IsNullOrEmpty(ddlFilterComplianceType.SelectedValue))
                complianceTypeID = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);

            if (!string.IsNullOrEmpty(ddlFilterState.SelectedValue))
                stateID = Convert.ToInt32(ddlFilterState.SelectedValue);

            if (!string.IsNullOrEmpty(tbxFilter.Text))
                filterText = Convert.ToString(tbxFilter.Text);

            grdHoliday.DataSource = HolidayMasterManagement.GetAllHolidays(complianceTypeID, stateID, filterText);
            grdHoliday.DataBind();
            upHolidayList.Update();
        }
        protected void grdHoliday_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_HOLIDAY"))
                {
                    long holidayID = Convert.ToInt64(e.CommandArgument);

                    ViewState["Mode"] = 1;
                    ViewState["HolidayID"] = holidayID;

                    //changes this to holiday master
                    Business.Data.HolidayMaster holidayRecord = HolidayMasterManagement.GetHolidayByID(holidayID);

                    tbxName.Text = holidayRecord.Name;
                    //tbxName.ToolTip = holidayRecord.Name;

                    //if(holidayRecord.Day > 0)
                    //    tbxDay.Text = Convert.ToString(holidayRecord.Day);

                    //if(holidayRecord.Month > 0)
                    //{
                    //    DateTime dtDate = new DateTime(2000, holidayRecord.Month, 1);                        
                    //    string sMonthFullName = dtDate.ToString("MMMM");
                    //    tbxMonth.Text = Convert.ToString(sMonthFullName);
                    //}

                    if (holidayRecord.Day != null && holidayRecord.Month > 0 && holidayRecord.HYear != null)
                    {
                        DateTime dtDate = new DateTime(holidayRecord.HYear, holidayRecord.Month, holidayRecord.Day);                        
                        tbxDate.Text =   Convert.ToDateTime(dtDate).ToString("dd-MM-yyyy");
                    }
                    ddlComplianceType.SelectedValue = Convert.ToString(holidayRecord.ComplianceTypeID);

                    if (holidayRecord.ComplianceTypeID != -1)
                    {
                        ddlComplianceType.SelectedValue = Convert.ToString(holidayRecord.ComplianceTypeID);
                    }
                    else
                    {
                        ddlComplianceType.SelectedValue = "-1";
                    }

                    ddlComplianceType_SelectedIndexChanged(null, null);

                    if(holidayRecord.StateID != -1 && holidayRecord.StateID != 0)
                    {
                       ddlState.SelectedValue = Convert.ToString(holidayRecord.StateID);
                    }
                    else
                    {
                        ddlState.SelectedValue = "-1";
                    }

                    upHoliday.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divHolidayDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_HOLIDAY"))
                {
                    int holidayID = Convert.ToInt32(e.CommandArgument);
                    HolidayMasterManagement.DeleteHoliday(holidayID,AuthenticationHelper.UserID);
                    upHolidayList.Update();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdHoliday_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdHoliday.PageIndex = e.NewPageIndex;
                BindGrid();             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnAddHoliday_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                ddlComplianceType.SelectedValue = "-1";
                ddlState.SelectedValue = "-1";
                divState.Visible = false;
                cvState.Enabled = false;
                tbxDate.Text = string.Empty;
                //tbxMonth.Text = string.Empty;
                upHoliday.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divHolidayDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdHoliday.PageIndex = 0;
                BindGrid();              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Data.HolidayMaster holiday = new Business.Data.HolidayMaster()
                {
                    Name = Convert.ToString(tbxName.Text),
                    IsDeleted = false,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                };

                if (ddlComplianceType.SelectedValue != "-1")
                    holiday.ComplianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (ddlState.SelectedValue != "-1" && ddlState.SelectedValue != "0")
                    holiday.StateID = Convert.ToInt32(ddlState.SelectedValue);


                if (!string.IsNullOrEmpty(tbxDate.Text))
                {
                    int month = DateTime.ParseExact(Convert.ToString(tbxDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture).Month;

                    holiday.Day = Convert.ToByte(DateTime.ParseExact(Convert.ToString(tbxDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture).Day);
                    if (month > 0)
                        holiday.Month = Convert.ToByte(month);

                    holiday.HYear = DateTime.ParseExact(Convert.ToString(tbxDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture).Year;
                }

                //if (!string.IsNullOrEmpty(tbxDay.Text))
                //    holiday.Day = Convert.ToByte(tbxDay.Text);

                //if(!string.IsNullOrEmpty(tbxMonth.Text))
                //{
                //    int month = DateTime.ParseExact(Convert.ToString(tbxMonth.Text), "MMMM", CultureInfo.CurrentCulture).Month;
                //    if (month > 0)
                //        holiday.Month = Convert.ToByte(month);
                //}             

                if ((int)ViewState["Mode"] == 1)
                {
                    holiday.ID = Convert.ToInt32(ViewState["HolidayID"]);
                }

                if (HolidayMasterManagement.Exists(holiday))
                {
                    cvDuplicateEntry.ErrorMessage = "Holiday name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                bool savesucess = false;

                if ((int)ViewState["Mode"] == 0)
                {
                    savesucess = HolidayMasterManagement.Create(holiday);

                    if (savesucess)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    savesucess = HolidayMasterManagement.Update(holiday);

                    if(savesucess)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                    }
                }

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divHolidayDialog\").dialog('close')", true);
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void upHoliday_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilter.Text = string.Empty;
                BindStates(ddlFilterState);
                BindGrid();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int complianceTypeID = -1;
                string complianceTypeName = string.Empty;

                if (ddlComplianceType.SelectedValue != "-1")
                {
                    complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);

                    complianceTypeName = ComplianceTypeManagement.GetByID(complianceTypeID).Name;

                    if (complianceTypeName.Trim().ToUpper() == "STATE")
                    {
                        divState.Visible = true;
                        cvState.Enabled = true;
                        BindStates(ddlState);
                    }
                    else
                    {
                        divState.Visible = false;
                        cvState.Enabled = false;
                        ddlState.SelectedValue = "-1";
                    }

                   // BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void grdHoliday_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = -1;
                int stateID = -1;
                string filterText = string.Empty;

                if (!string.IsNullOrEmpty(ddlFilterComplianceType.SelectedValue))
                    complianceTypeID = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);

                if (!string.IsNullOrEmpty(ddlFilterState.SelectedValue))
                    stateID = Convert.ToInt32(ddlFilterState.SelectedValue);

                if (string.IsNullOrEmpty(tbxFilter.Text))
                    filterText = Convert.ToString(tbxFilter.Text);

                var holidayList = HolidayMasterManagement.GetAllHolidays(complianceTypeID,stateID,filterText);

                if (direction == SortDirection.Ascending)
                {
                    holidayList = holidayList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    holidayList = holidayList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdHoliday.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdHoliday.Columns.IndexOf(field);
                    }
                }

                grdHoliday.DataSource = holidayList;
                grdHoliday.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdHoliday_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        
    }
}