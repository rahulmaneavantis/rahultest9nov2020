﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
   
    public partial class UserCustomerMapping_Details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        BindDitributor();                        
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }       
        public static List<Customer> GetAllDistributor()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers                                
                                 where row.IsDeleted == false
                                 && row.IsDistributor==true
                                 select row);

                
                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }
        public static List<Customer> GetAllDistributorCustomer(int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.ParentID == distributorid
                                 select row);


                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }
        public static List<Product> GetByProductMapping(int customerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var mappings = (from row in entities.ProductMappings
                                join row1 in entities.Products
                                on row.ProductID equals row1.Id
                                where row.CustomerID == customerId
                                 && row.IsActive == false
                                select row1);

                if (mappings.Count() > 0)
                    mappings = mappings.OrderBy(row => row.Name);
                
                return mappings.Distinct().ToList();
            }
        }
        public static List<IMA_Distributor_CustomerUser_Result> GetDistributorUsers(int distributorid,int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var mappings = entities.IMA_Distributor_CustomerUser(distributorid, customerid).ToList();

                if (mappings.Count() > 0)
                    mappings = mappings.OrderBy(row => row.Name).ToList();

                return mappings.ToList();
            }
        }
        private void BindDitributor()
        {
            try
            {
                var data = GetAllDistributor();
                ddldistributor.DataTextField = "Name";
                ddldistributor.DataValueField = "ID";
                ddldistributor.DataSource = data;
                ddldistributor.DataBind();
                ddldistributor.Items.Insert(0, new ListItem("< Select Distributor >", "-1"));


                ddlDistributorpopup.DataTextField = "Name";
                ddlDistributorpopup.DataValueField = "ID";
                ddlDistributorpopup.DataSource = data;
                ddlDistributorpopup.DataBind();
                ddlDistributorpopup.Items.Insert(0, new ListItem("< Select Distributor >", "-1"));
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomer(int distributorID)
        {
            try
            {
                var data = GetAllDistributorCustomer(distributorID);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = data;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));


                ddlCustomerpopup.DataTextField = "Name";
                ddlCustomerpopup.DataValueField = "ID";
                ddlCustomerpopup.DataSource = data;
                ddlCustomerpopup.DataBind();
                ddlCustomerpopup.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProduct(int customerid)
        {
            try
            {
                var data = GetByProductMapping(customerid);
                ddlproduct.DataTextField = "Name";
                ddlproduct.DataValueField = "ID";
                ddlproduct.DataSource = data;
                ddlproduct.DataBind();
                ddlproduct.Items.Insert(0, new ListItem("< Select Product >", "-1"));


                ddlproductpopup.DataTextField = "Name";
                ddlproductpopup.DataValueField = "ID";
                ddlproductpopup.DataSource = data;
                ddlproductpopup.DataBind();
                ddlproductpopup.Items.Insert(0, new ListItem("< Select Product >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindUsers(int distributorid,int customerid)
        {
            try
            {
                var data = GetDistributorUsers(distributorid,customerid);
                ddlUser.DataTextField = "Name";
                ddlUser.DataValueField = "ID";
                ddlUser.DataSource = data;
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("< Select User >", "-1"));


                ddlUserpopup.DataTextField = "Name";
                ddlUserpopup.DataValueField = "ID";
                ddlUserpopup.DataSource = data;
                ddlUserpopup.DataBind();
                ddlUserpopup.Items.Insert(0, new ListItem("< Select User >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static bool UserCustomerExists(UserCustomerMapping cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.UserCustomerMappings
                             where row.CustomerID==cat.CustomerID
                             && row.UserID==cat.UserID
                             && row.ProductID==cat.ProductID      
                             && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreateUserCustomerMappingMaster(UserCustomerMapping cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.UserCustomerMappings.Add(cat);
                entities.SaveChanges();
            }
        }
        public static void UpdateUserCustomerMappingMaster(UserCustomerMapping cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var deptMasterToUpdate = (from row in entities.UserCustomerMappings
                                                            where row.ID == cat.ID
                                                              && row.IsActive == false

                                                            select row).FirstOrDefault();

                deptMasterToUpdate.UserID = cat.UserID;
                deptMasterToUpdate.CustomerID = cat.CustomerID;
                deptMasterToUpdate.ProductID = cat.ProductID;
                entities.SaveChanges();
            }
        }
        public static UserCustomerMapping UserCustomerMappingGetByID(int id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MsttMaster = (from row in entities.UserCustomerMappings
                                  where row.ID == id && row.IsActive == false
                                  select row).SingleOrDefault();

                return MsttMaster;
            }
        }
        public static void DeleteUserCustomerMappingMaster(int id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var categorizationMastertoDelete = (from row in entities.UserCustomerMappings
                                                    where row.ID == id
                                                    select row).FirstOrDefault();

                categorizationMastertoDelete.IsActive = false;
                entities.SaveChanges();
            }
        }      
        public static List<IMA_BindUserCustomerMpping_Result> GetAllList(int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var mappings = entities.IMA_BindUserCustomerMpping(distributorid).ToList();

                if (mappings.Count() > 0)
                    mappings = mappings.OrderBy(row => row.CustomerName).ToList();

                return mappings.ToList();               
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int distributorid = -1;
                int customerid = -1;
                int Productid = -1;
                int Userid = -1;
                if (!string.IsNullOrEmpty(ddlDistributorpopup.SelectedValue))
                {
                    if (ddlDistributorpopup.SelectedValue != "-1")
                    {
                        distributorid = Convert.ToInt32(ddlDistributorpopup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlCustomerpopup.SelectedValue))
                {
                    if (ddlCustomerpopup.SelectedValue != "-1")
                    {
                        customerid = Convert.ToInt32(ddlCustomerpopup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlproductpopup.SelectedValue))
                {
                    if (ddlproductpopup.SelectedValue != "-1")
                    {
                        Productid = Convert.ToInt32(ddlproductpopup.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlUserpopup.SelectedValue))
                {
                    if (ddlUserpopup.SelectedValue != "-1")
                    {
                        Userid = Convert.ToInt32(ddlUserpopup.SelectedValue);
                    }
                }
                if (distributorid != -1)
                {
                    UserCustomerMapping objcat = new UserCustomerMapping()
                    {
                        UserID = Userid,
                        CustomerID = customerid,
                        IsActive = true,
                        CreatedOn = DateTime.Now,
                        ProductID = Productid,
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        objcat.ID = Convert.ToInt32(ViewState["usercustomerid"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (UserCustomerExists(objcat))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "User Customer Mapping already exists";
                        }
                        else
                        {
                            CreateUserCustomerMappingMaster(objcat);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "User Customer Mapping saved successfully";
                            ddlDistributorpopup.SelectedValue = "-1";
                            ddlCustomerpopup.SelectedValue = "-1";
                            ddlproductpopup.SelectedValue = "-1";
                            ddlUserpopup.SelectedValue = "-1";
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        if (UserCustomerExists(objcat))
                        {
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "User Customer Mapping already exists";
                        }
                        else
                        {
                            UpdateUserCustomerMappingMaster(objcat);
                            CustomModifyDepartment.IsValid = false;
                            CustomModifyDepartment.ErrorMessage = "User Customer Mapping successfully";
                        }
                    }
                    BindGridList();
                    upCategoryList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindGridList()
        {
            try
            {
                int distributorID = -1;
                int customerid = -1;
                int productid = -1;
                int userid = -1;
                if (!string.IsNullOrEmpty(ddldistributor.SelectedValue))
                {
                    if (ddldistributor.SelectedValue != "-1")
                    {
                        distributorID = Convert.ToInt32(ddldistributor.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlproduct.SelectedValue))
                {
                    if (ddlproduct.SelectedValue != "-1")
                    {
                        productid = Convert.ToInt32(ddlproduct.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlUser.SelectedValue))
                {
                    if (ddlUser.SelectedValue != "-1")
                    {
                        userid = Convert.ToInt32(ddlUser.SelectedValue);
                    }
                }
                var CategoryMasterList = GetAllList(distributorID);
                var filter = tbxFilter.Text;
                if (!string.IsNullOrEmpty(filter))
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.CustomerName.ToUpper().Contains(filter.ToUpper()) || entry.Distributorname.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                if (customerid != -1)
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.CustomerID == customerid).ToList();
                }
                if (productid != -1)
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.ProductID == productid).ToList();
                }
                if (userid != -1)
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.UserID == userid).ToList();
                }
                grdCategorization.DataSource = CategoryMasterList;
                Session["TotalRows"] = CategoryMasterList.Count;
                grdCategorization.DataBind();
                upModifycategory.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCategorization_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int Usercustomerid = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Category"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["usercustomerid"] = Usercustomerid;
                    var RPD = UserCustomerMappingGetByID(Usercustomerid);                  
                    if (RPD.CustomerID != -1)
                    {
                        ddlCustomerpopup.SelectedValue = Convert.ToString(RPD.CustomerID);
                    }
                    if (RPD.ProductID != -1)
                    {
                        ddlproductpopup.SelectedValue = Convert.ToString(RPD.ProductID);
                    }
                    if (RPD.UserID != -1)
                    {
                        ddlUserpopup.SelectedValue = Convert.ToString(RPD.UserID);
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyCategory\").dialog('open');", true);
                    upModifycategory.Update();
                }
                else if (e.CommandName.Equals("DELETE_Category"))
                {
                    DeleteUserCustomerMappingMaster(Usercustomerid);
                    BindGridList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddldistributor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddldistributor.SelectedValue))
                {
                    if (ddldistributor.SelectedValue != "1")
                    {
                        BindCustomer(Convert.ToInt32(ddldistributor.SelectedValue));
                        BindGridList();
                    }
                }                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "1")
                    {
                        BindProduct(Convert.ToInt32(ddlCustomer.SelectedValue));

                        if (!string.IsNullOrEmpty(ddldistributor.SelectedValue))
                        {
                            if (ddldistributor.SelectedValue != "1")
                            {
                                BindGridList();
                                BindUsers(Convert.ToInt32(ddldistributor.SelectedValue), Convert.ToInt32(ddlCustomer.SelectedValue));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlproduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGridList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGridList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCategorization.PageIndex = 0;
                BindGridList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCategoryList_Load(object sender, EventArgs e)
        {
        }
        protected void grdCategorization_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCategorization.PageIndex = e.NewPageIndex;
                BindGridList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upModifycategory_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                ddlDistributorpopup.SelectedValue = "-1";
                ddlCustomerpopup.SelectedValue = "-1";
                ddlproductpopup.SelectedValue = "-1";
                ddlUserpopup.SelectedValue = "-1";
                upModifycategory.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyCategory\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlDistributorpopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlDistributorpopup.SelectedValue))
                {
                    if (ddlDistributorpopup.SelectedValue != "1")
                    {
                        BindCustomer(Convert.ToInt32(ddlDistributorpopup.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlCustomerpopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomerpopup.SelectedValue))
                {
                    if (ddlCustomerpopup.SelectedValue != "1")
                    {
                        BindProduct(Convert.ToInt32(ddlCustomerpopup.SelectedValue));

                        if (!string.IsNullOrEmpty(ddlDistributorpopup.SelectedValue))
                        {
                            if (ddlDistributorpopup.SelectedValue != "1")
                            {
                                BindUsers(Convert.ToInt32(ddlDistributorpopup.SelectedValue), Convert.ToInt32(ddlCustomerpopup.SelectedValue));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
    }
}