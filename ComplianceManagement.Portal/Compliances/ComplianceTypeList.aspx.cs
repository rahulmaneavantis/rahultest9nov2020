﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Linq;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceTypeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceTypes();
                if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("RREV")))
                {
                    btnAddComplianceType.Visible = true;
                }
            }
        }

        private void BindComplianceTypes()
        {
            try
            {
                grdComplianceType.DataSource = ComplianceTypeManagement.GetAll(tbxFilter.Text);
                grdComplianceType.DataBind();
                upComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int typeID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_COMPLIANCE_TYPE"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceTypeID"] = typeID;

                    ComplianceType userParameter = ComplianceTypeManagement.GetByID(typeID);

                    tbxName.Text = userParameter.Name;
                    tbxDescription.Text = userParameter.Description;
                    tbxDescription.ToolTip = userParameter.Description;

                    upComplianceType.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);

                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE_TYPE"))
                {
                    if (ComplianceTypeManagement.CanDelete(typeID))
                    {
                        ComplianceTypeManagement.Delete(typeID);
                        BindComplianceTypes();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Compliance type is associated with one or more Acts, can not be deleted.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceType.PageIndex = e.NewPageIndex;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;

                upComplianceType.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceTypeDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceType.PageIndex = 0;
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ComplianceType type = new ComplianceType()
                {
                    Name = tbxName.Text,
                    Description = tbxDescription.Text
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    type.ID = Convert.ToInt32(ViewState["ComplianceTypeID"]);
                }

                if (ComplianceTypeManagement.Exists(type))
                {
                    cvDuplicateEntry.ErrorMessage = "Type name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    ComplianceTypeManagement.Create(type);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ComplianceTypeManagement.Update(type);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceTypeDialog\").dialog('close')", true);
                BindComplianceTypes();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceType = ComplianceTypeManagement.GetAll(tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    complianceType = complianceType.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceType = complianceType.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceType.Columns.IndexOf(field);
                    }
                }

                grdComplianceType.DataSource = complianceType;
                grdComplianceType.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (!AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        e.Row.Cells[2].Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}