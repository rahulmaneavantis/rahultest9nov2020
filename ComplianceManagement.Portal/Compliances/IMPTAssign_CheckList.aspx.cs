﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class IMPTAssign_CheckList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDitributor();                
                BindComplianceCategories();
                BindTypes();                               
                AddFilter();
                tbxFilterLocation.Text = "< Select >";
                txtactList.Text = "< Select >";
            }
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "1")
                    {
                        if (!string.IsNullOrEmpty(ddlFilterServiceProvider.SelectedValue))
                        {
                            if (ddlFilterServiceProvider.SelectedValue != "1")
                            {
                                BindLocationFilter(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                                BindUsers(ddlFilterPerformer, Convert.ToInt32(ddlFilterCustomer.SelectedValue), Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                                BindUsers(ddlFilterReviewer, Convert.ToInt32(ddlFilterCustomer.SelectedValue), Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                                BindUsers(ddlFilterApprover, Convert.ToInt32(ddlFilterCustomer.SelectedValue), Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                                BindDepartment(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                                BindActList(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void ddlFilterServiceProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterServiceProvider.SelectedValue))
                {
                    if (ddlFilterServiceProvider.SelectedValue != "1")
                    {
                        BindCustomer(Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDitributor()
        {
            try
            {
                var data = GetAllDistributor();
                ddlFilterServiceProvider.DataTextField = "Name";
                ddlFilterServiceProvider.DataValueField = "ID";
                ddlFilterServiceProvider.DataSource = data;
                ddlFilterServiceProvider.DataBind();
                ddlFilterServiceProvider.Items.Insert(0, new ListItem("< Select ServiceProvider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Customer> GetAllDistributor()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.IsDistributor == true
                                 select row);


                if (customers.Count() > 0)
                {
                    if (AuthenticationHelper.Role == "DADMN")
                    {
                        customers = customers.Where(entry => entry.ID == AuthenticationHelper.CustomerID);
                    }
                    customers = customers.OrderBy(row => row.Name);
                }

                return customers.ToList();
            }
        }
        public static List<Customer> GetAllDistributorCustomer(int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.ParentID == distributorid
                                 select row);


                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }
        private void BindCustomer(int distributorID)
        {
            try
            {
                var data = GetAllDistributorCustomer(distributorID);
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = data;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindDepartment(int customerID)
        {
            try
            {            
                ddlFilterDepartment.DataTextField = "Name";
                ddlFilterDepartment.DataValueField = "ID";
                ddlFilterDepartment.Items.Clear();
                var deptdetails = CompDeptManagement.FillDepartment(customerID);
                ddlFilterDepartment.DataSource = deptdetails;
                ddlFilterDepartment.DataBind();
                ddlFilterDepartment.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void AddFilter(int pageIndex = 0)
        {
           
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindLocationFilter(int customerID)
        {
            try
            {
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;            
            ConditionsBindComplianceMatrix();
        }

       

        private void BindUsers(DropDownList ddlUserList,int customerID,int  distributorID, List<long> ids = null)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();

                    //var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                    //var query = (from row in entities.Users
                    //             where row.IsDeleted == false && row.IsActive == true
                    //             && row.RoleID != 19 && row.CustomerID == customerID
                    //             select row);


                    //var sequery = (from row in entities.Users
                    //               where row.IsDeleted == false && row.IsActive == true
                    //               && row.RoleID != 19
                    //               && row.CustomerID == distributorID
                    //               select row);

                    //var UserCount = query.Union(sequery).ToList();


                    var query = (from row in entities.Users
                                 where row.IsDeleted == false && row.IsActive == true
                                 && row.RoleID != 19 && row.CustomerID == customerID
                                 select row).ToList();

                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                    var sequery = (from row in entities.Users
                                   where row.IsDeleted == false && row.IsActive == true
                                   && row.RoleID != 19
                                   && row.CustomerID == distributorID
                                   select row).ToList();

                    var users1 = (from row in sequery
                                  select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                    var UserCount = users.Union(users1).ToList();
                    ddlUserList.DataSource = UserCount;
                    ddlUserList.DataBind();

                    ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
              
        
        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
            {
                if (ddlFilterCustomer.SelectedValue != "1")
                {
                    BindActList(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                    grdComplianceRoleMatrix.PageIndex = 0;
                    ConditionsBindComplianceMatrix();
                }
            }
        }

        private void ConditionsBindComplianceMatrix()
        {
            if (tbxFilter.Text.Trim() == "")
            {
                BindComplianceMatrix("N", "N");
            }
            else
            {
                BindComplianceMatrix("Y", "N");
            }
        }

        private void BindComplianceMatrix(string flag, string IFCheckedEvent)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "1")
                    {
                        int customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                        int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                        int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                        int branchID = -1;
                        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        }
                        List<int> actIds = new List<int>();
                        foreach (RepeaterItem aItem in rptActList.Items)
                        {
                            CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                            if (chkAct.Checked)
                            {
                                actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                            }
                        }
                        if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count != 0)
                        {

                            if (flag == "N" && IFCheckedEvent == "N")
                            {
                                grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID, false, branchID, actIds);
                            }
                            else if (flag == "Y" && IFCheckedEvent == "N")
                            {
                                grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID, false, branchID, actIds, tbxFilter.Text.Trim());
                            }
                            grdComplianceRoleMatrix.DataBind();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlComplianceType.DataTextField = "Name";
                ddlComplianceType.DataValueField = "ID";

                ddlComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlComplianceType.DataBind();

                ddlComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActList(int customerID)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);                
                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {                        
                        int branchID = -1;
                        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        }
                        if (branchID != -1)
                        {
                            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                        }
                    }
                }

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.TempGetByTypeCheckList(complianceTypeID, complianceCatagoryID);                
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                    }
                }
                SaveCheckedValues();
                grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                if (chkEvent.Checked == true)
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "Y");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "Y");
                    }
                }
                else
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "N");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "N");
                    }
                }

                //BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
            {
                if (ddlFilterCustomer.SelectedValue != "1")
                {
                    BindActList(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                    grdComplianceRoleMatrix.PageIndex = 0;
                    if (chkEvent.Checked == true)
                    {
                        if (tbxFilter.Text.Trim() == "")
                        {
                            BindComplianceMatrix("N", "Y");
                        }
                        else
                        {
                            BindComplianceMatrix("Y", "Y");
                        }
                    }
                    else
                    {
                        if (tbxFilter.Text.Trim() == "")
                        {
                            BindComplianceMatrix("N", "N");
                        }
                        else
                        {
                            BindComplianceMatrix("Y", "N");
                        }
                    }
                }
            }          
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {           
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {

            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach(GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                 {
                     countCheckedCheckbox = countCheckedCheckbox + 1;                    
                 }
               
            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
       }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "1")
                    {
                        int customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                        var complianceList = new List<ComplianceAsignmentProperties>();
                        SaveCheckedValues();
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                        List<TempAssignmentTableCheckList> Tempassignments = new List<TempAssignmentTableCheckList>();
                        int branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        grdComplianceRoleMatrix.AllowPaging = true;
                        BindComplianceMatrix("N", "N");
                        grdComplianceRoleMatrix.DataBind();
                        int DepartmentID = -1;
                        if (!string.IsNullOrEmpty(ddlFilterDepartment.SelectedValue))
                        {
                            if (ddlFilterDepartment.SelectedValue != "-1")
                            {
                                DepartmentID = Convert.ToInt32(ddlFilterDepartment.SelectedValue);
                            }
                        }
                        if (complianceList != null)
                        {
                            for (int i = 0; i < complianceList.Count; i++)
                            {
                                if (complianceList[i].Performer)
                                {
                                    if (ddlFilterPerformer.SelectedValue != null)
                                    {
                                        TempAssignmentTableCheckList TempAssP = new TempAssignmentTableCheckList();
                                        TempAssP.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssP.CustomerBranchID = branchID;
                                        TempAssP.RoleID = RoleManagement.GetByCode("PERF").ID;
                                        TempAssP.UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue);
                                        TempAssP.IsActive = true;
                                        TempAssP.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssP.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssP.DepartmentID = DepartmentID;
                                        }
                                        Tempassignments.Add(TempAssP);
                                    }
                                    if (ddlFilterReviewer.SelectedValue != null)
                                    {
                                        TempAssignmentTableCheckList TempAssR = new TempAssignmentTableCheckList();
                                        TempAssR.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssR.CustomerBranchID = branchID;
                                        TempAssR.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                        TempAssR.UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue);
                                        TempAssR.IsActive = true;
                                        TempAssR.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssR.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssR.DepartmentID = DepartmentID;
                                        }
                                        Tempassignments.Add(TempAssR);

                                    }
                                    if (ddlFilterApprover.SelectedValue != null)
                                    {
                                        TempAssignmentTableCheckList TempAssA = new TempAssignmentTableCheckList();
                                        TempAssA.ComplianceId = complianceList[i].ComplianceId;
                                        TempAssA.CustomerBranchID = branchID;
                                        TempAssA.RoleID = RoleManagement.GetByCode("APPR").ID;
                                        TempAssA.UserID = Convert.ToInt32(ddlFilterApprover.SelectedValue);
                                        TempAssA.IsActive = true;
                                        TempAssA.CreatedOn = DateTime.Now;
                                        if (DepartmentID == -1 || DepartmentID == 0)
                                        {
                                            TempAssA.DepartmentID = null;
                                        }
                                        else
                                        {
                                            TempAssA.DepartmentID = DepartmentID;
                                        }
                                        Tempassignments.Add(TempAssA);
                                    }
                                }
                            }
                            if (Tempassignments.Count != 0)
                            {
                                Business.ComplianceManagement.AddDetailsTempAssignmentTableCheckList(Tempassignments);
                                ClearSelection();
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                        }

                        ConditionsBindComplianceMatrix();
                        ViewState["CHECKED_ITEMS"] = null;
                    }
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }

        private void ClearSelection()
        {
            tbxFilterLocation.Text = "< Select >";
            ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.SelectedValue = "-1";
            ddlFilterApprover.SelectedValue = "-1";
            ddlComplianceCatagory.SelectedValue = "-1";
            ddlComplianceType.SelectedValue = "-1";
            ddlFilterDepartment.SelectedValue = "-1";
        }

        private void SaveCheckedValues()
        {
            try
            {

                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;                                   
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");                                                  
                            chkPerformer.Checked = rmdata.Performer;                                                    
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}