﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CannedReports : System.Web.UI.Page
    {
        public string Role;
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";
        static int CustomerID;
        static int UserRoleID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Session["Status"] = "";
                    Session["ComplianceType"] = "Statutory";
                    Session["ComplianceTypeReviewer"] = "Statutory";

                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                    if (roles.Contains(3) && roles.Contains(4))
                    {
                        ClickChangeflag = "P";
                        btnPerformer_Click(null, null);
                    }
                    else if (roles.Contains(3))
                    {
                        ClickChangeflag = "P";
                        btnPerformer_Click(null, null);
                    }
                    else if (roles.Contains(4))
                    {
                        ClickChangeflag = "R";
                        btnReviewer_Click(null, null);
                    }
                    else
                    {
                        ClickChangeflag = "P";
                        btnPerformer_Click(null, null);
                    }

                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

                    if (UserRoleID == 8)
                    {
                        reviewerdocuments.Visible = false;
                        performerdocuments.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }


        protected void btnPerformer_Click(object sender, EventArgs e)
        {
            try
            {
                ClickChangeflag = "P";

                liReviewer.Attributes.Add("class", "");
                liPerformer.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");

                reviewerdocuments.Visible = false;
                performerdocuments.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                ClickChangeflag = "R";
                liPerformer.Attributes.Add("class", "");
                liReviewer.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");

                reviewerdocuments.Visible = true;
                performerdocuments.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";

                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                }
                else
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetReviewer(List<Sp_GetCannedReportPerformerAndReviewer_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";

                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(masterListComAssign, complianceinstanceid, 4);
                }
                else
                {
                    result = DashboardManagement.GetUserName(masterListComAssign, complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        
        protected string GetReviewerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (Session["ComplianceType"].ToString() == "Internal")
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 4);
                }
                else
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);          
                return "";
            }
        }

     
        protected string GetPerformer(List<Sp_GetCannedReportPerformerAndReviewer_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(masterListComAssign,complianceinstanceid, 3);
                }
                else
                {
                    result = DashboardManagement.GetUserName(masterListComAssign, complianceinstanceid, 3);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                }
                else
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (Session["ComplianceType"].ToString() == "Internal")
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                }
                else
                {
                    result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        
        protected string GetApprover(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
        protected string GetApprover(List<Sp_GetCannedReportPerformerAndReviewer_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(masterListComAssign,complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }
       
        protected string GetApproverInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }


        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CannedReport");
                    DataTable ExcelData = null;
                    string filter = "";
                    String LocationName = String.Empty;

                    DateTime CurrentDate = DateTime.Today.Date;

                    if (Session["LocationName"].ToString() != "")
                        LocationName = Session["LocationName"].ToString();
                    var masterlstComAssignCustomerWise = DashboardManagement.GetlstComplianceAssignments(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (AuthenticationHelper.Role.Equals("MGMT"))
                    {
                        #region Management
                        if (ClickChangeflag == "P")
                        {
                            #region Managment Performer
                            DataView view = new System.Data.DataView(udcCannedReportPerformer.GetGrid());

                            if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");


                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise,Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                            {

                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                //ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {

                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                //ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApproverInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                            {

                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                //ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }

                            #endregion
                        }
                        else if (ClickChangeflag == "R")
                        {
                            #region Managment Reviewer
                            DataView view = new System.Data.DataView(udcCannedReportReviewer.GetGrid());

                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory")
                            {

                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";


                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";


                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");
                                ExcelData.Columns.Add("Approver");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Approver"] = GetApproverInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }
                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region Performer and reviewer                 
                        if (ClickChangeflag == "P")
                        {
                            #region Performer
                            DataView view = new System.Data.DataView(udcCannedReportPerformer.GetGrid());

                            if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";


                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");

                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Reviewer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Reviewer"] = GetReviewerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            #endregion
                        }
                        else if (ClickChangeflag == "R")
                        {
                            #region Reviewer
                            DataView view = new System.Data.DataView(udcCannedReportReviewer.GetGrid());

                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComplianceCategoryName", "ComSubTypeName", "ShortDescription", "Description", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComSubTypeName");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    //complianceid format change
                                    if (item["InternalComplianceID"] != null && item["InternalComplianceID"] != DBNull.Value)
                                        item["InternalComplianceID"] = Convert.ToString(item["InternalComplianceID"]);

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal CheckList")
                            {
                                ExcelData = view.ToTable("Selected", false, "InternalComplianceID", "Branch", "IntcomCategoryName", "ShortDescription", "ForMonth", "InternalScheduledOn", "CloseDate", "Status", "Risk", "InternalComplianceStatusID", "InternalComplianceInstanceID");
                                ExcelData.Columns.Add("RiskName");
                                ExcelData.Columns.Add("Performer");

                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    item["Performer"] = GetPerformerInternal(Convert.ToInt32(item["InternalComplianceInstanceID"].ToString()));

                                    //complianceid format change
                                    if (item["InternalComplianceID"] != null && item["InternalComplianceID"] != DBNull.Value)
                                        item["InternalComplianceID"] = Convert.ToString(item["InternalComplianceID"]);

                                    if (item["Risk"].ToString() == "0")
                                        item["RiskName"] = "High";
                                    else if (item["Risk"].ToString() == "1")
                                        item["RiskName"] = "Medium";
                                    else if (item["Risk"].ToString() == "2")
                                        item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date >= CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["InternalScheduledOn"]).Date < CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("InternalComplianceStatusID");
                                ExcelData.Columns.Remove("InternalComplianceInstanceID");
                                ExcelData.Columns.Remove("Risk");
                            }
                            #endregion
                        }
                        #endregion
                    }

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                    exWorkSheet.Cells["B1:C1"].Merge = true;
                    exWorkSheet.Cells["B1"].Value = LocationName;

                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Value = "Report Name:";

                    exWorkSheet.Cells["B2:C2"].Merge = true;

                    if (ClickChangeflag == "P")
                    {
                        exWorkSheet.Cells["B2"].Value = "Report of " + Session["ComplianceType"].ToString() + " Compliances";
                    }
                    else
                    {
                        exWorkSheet.Cells["B2"].Value = "Report of " + Session["ComplianceTypeReviewer"].ToString() + " Compliances";
                    }

                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                    exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                    //Load Data From Excel
                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);
                    if (AuthenticationHelper.Role.Equals("MGMT"))
                    {
                        #region Managment
                        if (Session["ComplianceType"].ToString() == "Statutory")
                        {
                            exWorkSheet.Cells["A5"].Value = "ComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Act";
                            exWorkSheet.Cells["D5"].Value = "Category Name";
                            exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                            exWorkSheet.Cells["F5"].Value = "Short Description";
                            exWorkSheet.Cells["G5"].Value = "Detail Description";
                            exWorkSheet.Cells["H5"].Value = "Period";
                            exWorkSheet.Cells["I5"].Value = "Due Date";
                            exWorkSheet.Cells["J5"].Value = "Close Date";
                            exWorkSheet.Cells["K5"].Value = "Status";
                            exWorkSheet.Cells["L5"].Value = "Risk";
                            exWorkSheet.Cells["M5"].Value = "Performer";
                            exWorkSheet.Cells["N5"].Value = "Reviewer";
                            exWorkSheet.Cells["O5"].Value = "Approver";
                            //exWorkSheet.Cells["M5"].Value = "Status";
                        }
                        if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                        {
                            exWorkSheet.Cells["A5"].Value = "ComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Act";
                            exWorkSheet.Cells["D5"].Value = "Category Name";
                            //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                            exWorkSheet.Cells["E5"].Value = "Short Description";
                            exWorkSheet.Cells["F5"].Value = "Detail Description";
                            exWorkSheet.Cells["G5"].Value = "Period";
                            exWorkSheet.Cells["H5"].Value = "Due Date";
                            exWorkSheet.Cells["I5"].Value = "Close Date";
                            exWorkSheet.Cells["J5"].Value = "Status";
                            exWorkSheet.Cells["K5"].Value = "Risk";
                            exWorkSheet.Cells["L5"].Value = "Performer";
                            exWorkSheet.Cells["M5"].Value = "Reviewer";
                            //exWorkSheet.Cells["O5"].Value = "Approver";
                            //exWorkSheet.Cells["M5"].Value = "Status";
                        }
                        if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                        {
                            exWorkSheet.Cells["A5"].Value = "ComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Act";
                            exWorkSheet.Cells["D5"].Value = "Category Name";
                            //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                            exWorkSheet.Cells["E5"].Value = "Short Description";
                            exWorkSheet.Cells["F5"].Value = "Detail Description";
                            exWorkSheet.Cells["G5"].Value = "Event Name";
                            exWorkSheet.Cells["H5"].Value = "Event Nature";
                            //exWorkSheet.Cells["G5"].Value = "Period";
                            exWorkSheet.Cells["I5"].Value = "Due Date";
                            exWorkSheet.Cells["J5"].Value = "Close Date";
                            exWorkSheet.Cells["K5"].Value = "Status";
                            exWorkSheet.Cells["L5"].Value = "Risk";
                            exWorkSheet.Cells["M5"].Value = "Performer";
                            exWorkSheet.Cells["N5"].Value = "Reviewer";
                            //exWorkSheet.Cells["O5"].Value = "Approver";
                            //exWorkSheet.Cells["M5"].Value = "Status";
                        }
                        else if (Session["ComplianceType"].ToString() == "EventBased")
                        {
                            exWorkSheet.Cells["A5"].Value = "ComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Act";
                            exWorkSheet.Cells["D5"].Value = "Category Name";
                            exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                            exWorkSheet.Cells["F5"].Value = "Short Description";
                            exWorkSheet.Cells["G5"].Value = "Detail Description";
                            exWorkSheet.Cells["H5"].Value = "Event Name";
                            exWorkSheet.Cells["I5"].Value = "Event Nature";
                            exWorkSheet.Cells["J5"].Value = "Due Date";
                            exWorkSheet.Cells["K5"].Value = "Close Date";
                            exWorkSheet.Cells["L5"].Value = "Status";
                            exWorkSheet.Cells["M5"].Value = "Risk";
                            exWorkSheet.Cells["N5"].Value = "Performer";
                            exWorkSheet.Cells["O5"].Value = "Reviewer";
                            exWorkSheet.Cells["P5"].Value = "Approver";
                        }
                        else if (Session["ComplianceType"].ToString() == "Internal")
                        {
                            exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Category Name";
                            exWorkSheet.Cells["D5"].Value = "Short Description";
                            // exWorkSheet.Cells["E5"].Value = "Detail Description";
                            exWorkSheet.Cells["E5"].Value = "Period";
                            exWorkSheet.Cells["F5"].Value = "Due Date";
                            exWorkSheet.Cells["G5"].Value = "Close Date";
                            exWorkSheet.Cells["H5"].Value = "Status";
                            exWorkSheet.Cells["I5"].Value = "Risk";
                            exWorkSheet.Cells["J5"].Value = "Performer";
                            exWorkSheet.Cells["K5"].Value = "Reviewer";
                            exWorkSheet.Cells["L5"].Value = "Approver";
                        }
                        else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                        {
                            exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Location";
                            exWorkSheet.Cells["C5"].Value = "Category Name";
                            exWorkSheet.Cells["D5"].Value = "Short Description";
                            // exWorkSheet.Cells["E5"].Value = "Detail Description";
                            exWorkSheet.Cells["E5"].Value = "Period";
                            exWorkSheet.Cells["F5"].Value = "Due Date";
                            exWorkSheet.Cells["G5"].Value = "Close Date";
                            exWorkSheet.Cells["H5"].Value = "Status";
                            exWorkSheet.Cells["I5"].Value = "Risk";
                            exWorkSheet.Cells["J5"].Value = "Performer";
                            exWorkSheet.Cells["K5"].Value = "Reviewer";
                            //exWorkSheet.Cells["L5"].Value = "Approver";
                        }
                        #endregion
                    }
                    else
                    {
                        if (ClickChangeflag == "P")
                        {
                            #region Performer
                            if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["F5"].Value = "Short Description";
                                exWorkSheet.Cells["G5"].Value = "Detail Description";
                                exWorkSheet.Cells["H5"].Value = "Period";
                                exWorkSheet.Cells["I5"].Value = "Due Date";
                                exWorkSheet.Cells["J5"].Value = "Close Date";
                                exWorkSheet.Cells["K5"].Value = "Status";
                                exWorkSheet.Cells["L5"].Value = "Risk";
                                exWorkSheet.Cells["M5"].Value = "Reviewer";
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["F5"].Value = "Short Description";
                                exWorkSheet.Cells["G5"].Value = "Detail Description";
                                exWorkSheet.Cells["H5"].Value = "Event Name";
                                exWorkSheet.Cells["I5"].Value = "Event Nature";
                                exWorkSheet.Cells["J5"].Value = "Due Date";
                                exWorkSheet.Cells["K5"].Value = "Close Date";
                                exWorkSheet.Cells["L5"].Value = "Status";
                                exWorkSheet.Cells["M5"].Value = "Risk";
                                exWorkSheet.Cells["N5"].Value = "Reviewer";
                            }
                            else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["E5"].Value = "Short Description";
                                exWorkSheet.Cells["F5"].Value = "Detail Description";
                                exWorkSheet.Cells["G5"].Value = "Period";
                                exWorkSheet.Cells["H5"].Value = "Due Date";
                                exWorkSheet.Cells["I5"].Value = "Close Date";
                                exWorkSheet.Cells["J5"].Value = "Status";
                                exWorkSheet.Cells["K5"].Value = "Risk";
                                exWorkSheet.Cells["L5"].Value = "Reviewer";
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["E5"].Value = "Short Description";
                                exWorkSheet.Cells["F5"].Value = "Detail Description";
                                exWorkSheet.Cells["G5"].Value = "Event Name";
                                exWorkSheet.Cells["H5"].Value = "Event Nature";
                                //exWorkSheet.Cells["G5"].Value = "Period";
                                exWorkSheet.Cells["I5"].Value = "Due Date";
                                exWorkSheet.Cells["J5"].Value = "Close Date";
                                exWorkSheet.Cells["K5"].Value = "Status";
                                exWorkSheet.Cells["L5"].Value = "Risk";
                                exWorkSheet.Cells["M5"].Value = "Reviewer";
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal")
                            {
                                exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Category Name";
                                exWorkSheet.Cells["D5"].Value = "Short Description";
                                exWorkSheet.Cells["E5"].Value = "Period";
                                //exWorkSheet.Cells["F5"].Value = "Category Name";
                                exWorkSheet.Cells["F5"].Value = "Due Date";
                                exWorkSheet.Cells["G5"].Value = "Close Date";
                                exWorkSheet.Cells["H5"].Value = "Status";
                                exWorkSheet.Cells["I5"].Value = "Risk";
                                exWorkSheet.Cells["J5"].Value = "Reviewer";
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Category Name";
                                exWorkSheet.Cells["D5"].Value = "Short Description";
                                exWorkSheet.Cells["E5"].Value = "Period";
                                //exWorkSheet.Cells["F5"].Value = "Category Name";
                                exWorkSheet.Cells["F5"].Value = "Due Date";
                                exWorkSheet.Cells["G5"].Value = "Close Date";
                                exWorkSheet.Cells["H5"].Value = "Status";
                                exWorkSheet.Cells["I5"].Value = "Risk";
                                exWorkSheet.Cells["J5"].Value = "Reviewer";
                            }
                            #endregion
                        }
                        else if (ClickChangeflag == "R")
                        {
                            #region Reviewer
                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["F5"].Value = "Short Description";
                                exWorkSheet.Cells["G5"].Value = "Detail Description";
                                exWorkSheet.Cells["H5"].Value = "Period";
                                exWorkSheet.Cells["I5"].Value = "Due Date";
                                exWorkSheet.Cells["J5"].Value = "Close Date";
                                exWorkSheet.Cells["K5"].Value = "Status";
                                exWorkSheet.Cells["L5"].Value = "Risk";
                                exWorkSheet.Cells["M5"].Value = "Perfomer";
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["F5"].Value = "Short Description";
                                exWorkSheet.Cells["G5"].Value = "Detail Description";
                                exWorkSheet.Cells["H5"].Value = "Event Name";
                                exWorkSheet.Cells["I5"].Value = "Event Nature";
                                exWorkSheet.Cells["J5"].Value = "Due Date";
                                exWorkSheet.Cells["K5"].Value = "Close Date";
                                exWorkSheet.Cells["L5"].Value = "Status";
                                exWorkSheet.Cells["M5"].Value = "Risk";
                                exWorkSheet.Cells["N5"].Value = "Perfomer";
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["E5"].Value = "Short Description";
                                exWorkSheet.Cells["F5"].Value = "Detail Description";
                                exWorkSheet.Cells["G5"].Value = "Event Name";
                                exWorkSheet.Cells["H5"].Value = "Event Nature";
                                //exWorkSheet.Cells["G5"].Value = "Period";
                                exWorkSheet.Cells["I5"].Value = "Due Date";
                                exWorkSheet.Cells["J5"].Value = "Close Date";
                                exWorkSheet.Cells["K5"].Value = "Status";
                                exWorkSheet.Cells["L5"].Value = "Risk";
                                exWorkSheet.Cells["M5"].Value = "Perfomer";
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "ComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Act";
                                exWorkSheet.Cells["D5"].Value = "Category Name";
                                //exWorkSheet.Cells["E5"].Value = "Sub Category Name";
                                exWorkSheet.Cells["E5"].Value = "Short Description";
                                exWorkSheet.Cells["F5"].Value = "Detail Description";
                                exWorkSheet.Cells["G5"].Value = "Period";
                                exWorkSheet.Cells["H5"].Value = "Due Date";
                                exWorkSheet.Cells["I5"].Value = "Close Date";
                                exWorkSheet.Cells["J5"].Value = "Status";
                                exWorkSheet.Cells["K5"].Value = "Risk";
                                exWorkSheet.Cells["L5"].Value = "Perfomer";
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal")
                            {
                                exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Category Name";
                                exWorkSheet.Cells["D5"].Value = "Short Description";
                                exWorkSheet.Cells["E5"].Value = "Period";
                                exWorkSheet.Cells["F5"].Value = "Due Date";
                                exWorkSheet.Cells["G5"].Value = "Close Date";
                                exWorkSheet.Cells["H5"].Value = "Status";
                                exWorkSheet.Cells["I5"].Value = "Risk";
                                exWorkSheet.Cells["J5"].Value = "Performer";
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal CheckList")
                            {
                                exWorkSheet.Cells["A5"].Value = "InternalComplianceID";
                                exWorkSheet.Cells["B5"].Value = "Location";
                                exWorkSheet.Cells["C5"].Value = "Category Name";
                                exWorkSheet.Cells["D5"].Value = "Short Description";
                                exWorkSheet.Cells["E5"].Value = "Period";
                                exWorkSheet.Cells["F5"].Value = "Due Date";
                                exWorkSheet.Cells["G5"].Value = "Close Date";
                                exWorkSheet.Cells["H5"].Value = "Status";
                                exWorkSheet.Cells["I5"].Value = "Risk";
                                exWorkSheet.Cells["J5"].Value = "Performer";
                            }
                            #endregion
                        }
                    }

                    //Heading
                    #region  Heading
                    if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                    {
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["O5"].Style.Font.Bold = true;

                        int colIdx = 0;


                        if (ClickChangeflag == "P")
                        {
                            if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased" || Session["ComplianceType"].ToString() == "Statutory CheckList" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal" || Session["ComplianceType"].ToString() == "Internal CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("InternalComplianceID") + 1;
                                exWorkSheet.Column(colIdx).Width = 25;
                            }
                        }
                        else if (ClickChangeflag == "R")
                        {
                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory" || Session["ComplianceTypeReviewer"].ToString() == "EventBased" || Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;
                            }
                            else if (Session["ComplianceTypeReviewer"].ToString() == "Internal" || Session["ComplianceTypeReviewer"].ToString() == "Internal CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("InternalComplianceID") + 1;
                                exWorkSheet.Column(colIdx).Width = 25;
                            }
                        }

                        colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                        exWorkSheet.Column(colIdx).Width = 25;

                        if (ClickChangeflag == "P")
                        {
                            if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased" || Session["ComplianceType"].ToString() == "Statutory CheckList" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("ComCategoryName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                                colIdx = ExcelData.Columns.IndexOf("ComSubTypeName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                            }
                            else
                            {
                                colIdx = ExcelData.Columns.IndexOf("IntcomCategoryName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                            }

                        }
                        else if (ClickChangeflag == "R")
                        {
                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory" || Session["ComplianceTypeReviewer"].ToString() == "EventBased" || Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("ComCategoryName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                                colIdx = ExcelData.Columns.IndexOf("ComSubTypeName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                            }
                            else
                            {
                                colIdx = ExcelData.Columns.IndexOf("IntcomCategoryName") + 1;
                                exWorkSheet.Column(colIdx).Width = 35;
                            }
                        }

                        colIdx = ExcelData.Columns.IndexOf("ShortDescription") + 1;
                        exWorkSheet.Column(colIdx).Width = 35;

                        colIdx = ExcelData.Columns.IndexOf("Description") + 1;
                        exWorkSheet.Column(colIdx).Width = 100;

                        colIdx = ExcelData.Columns.IndexOf("ForMonth") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("InternalScheduledOn") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Performer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Reviewer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        if (ClickChangeflag == "P")
                        {
                            if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased" )
                            {
                                colIdx = ExcelData.Columns.IndexOf("Approver") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;
                            }
                        }
                        else if (ClickChangeflag == "R")
                        {
                            if (Session["ComplianceTypeReviewer"].ToString() == "Statutory" || Session["ComplianceTypeReviewer"].ToString() == "EventBased")
                            {
                                colIdx = ExcelData.Columns.IndexOf("Approver") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;
                            }
                        }


                        colIdx = ExcelData.Columns.IndexOf("IPerformer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("IReviewer") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("IApprover") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Risk") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        colIdx = ExcelData.Columns.IndexOf("CloseDate") + 1;
                        exWorkSheet.Column(colIdx).Width = 20;

                        if (ClickChangeflag == "P")
                        {
                            if (Session["ComplianceType"].ToString() == "EventBased" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("EventName") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;

                                colIdx = ExcelData.Columns.IndexOf("EventNature") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;

                            }
                        }
                        else if (ClickChangeflag == "R")
                        {
                            if (Session["ComplianceTypeReviewer"].ToString() == "EventBased" || Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("EventName") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;

                                colIdx = ExcelData.Columns.IndexOf("EventNature") + 1;
                                exWorkSheet.Column(colIdx).Width = 20;
                            }
                        }

                        if (AuthenticationHelper.Role.Equals("MGMT"))
                        {
                            #region Managment
                            if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 15])
                                {
                                    col.Style.WrapText = true;
                                    // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 15].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                {
                                    col.Style.WrapText = true;
                                    // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 14])
                                {
                                    col.Style.WrapText = true;
                                    // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 14].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            else if (Session["ComplianceType"].ToString() == "EventBased")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 16])
                                {
                                    col.Style.WrapText = true;
                                    // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 16].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                {
                                    col.Style.WrapText = true;
                                    //  col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 11])
                                {
                                    col.Style.WrapText = true;
                                    //  col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 11].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            if (ClickChangeflag == "P")
                            {
                                #region Performer
                                if (Session["ComplianceType"].ToString() == "Statutory")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceType"].ToString() == "EventBased")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 14])
                                    {
                                        col.Style.WrapText = true;
                                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 14].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 12])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 12].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceType"].ToString() == "EventBased CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceType"].ToString() == "Internal")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                                    {
                                        col.Style.WrapText = true;
                                        // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 10].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceType"].ToString() == "Internal CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                                    {
                                        col.Style.WrapText = true;
                                        // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 10].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                #endregion
                            }
                            else if (ClickChangeflag == "R")
                            {
                                #region Reviewer
                                if (Session["ComplianceTypeReviewer"].ToString() == "Statutory")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 14])
                                    {
                                        col.Style.WrapText = true;
                                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 14].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceTypeReviewer"].ToString() == "Statutory CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 12])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 12].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceTypeReviewer"].ToString() == "EventBased CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 13])
                                    {
                                        col.Style.WrapText = true;

                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 13].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceTypeReviewer"].ToString() == "Internal")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                                    {
                                        col.Style.WrapText = true;
                                        // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 10].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                else if (Session["ComplianceTypeReviewer"].ToString() == "Internal CheckList")
                                {
                                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                                    {
                                        col.Style.WrapText = true;
                                        // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        //col.AutoFitColumns();

                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        col[5, 2, 5 + ExcelData.Rows.Count, 10].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                }
                                #endregion
                            }
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.Clear();
                        Response.Buffer = true;
                        if (ClickChangeflag == "P")
                            Response.AddHeader("content-disposition", "attachment;filename=" + Session["ComplianceType"].ToString() + "_CannedReport.xlsx");
                        else
                            Response.AddHeader("content-disposition", "attachment;filename=" + Session["ComplianceTypeReviewer"].ToString() + "_CannedReport.xlsx");

                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}