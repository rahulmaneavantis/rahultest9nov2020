﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="ClientDepartmentMapping.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ClientDepartmentMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;            
            padding: 0;
        }
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 450px;
        }
    </style>
      <link href="/NewCSS/contract_custom_style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
           
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterCustomerList_SelectedIndexChanged">
                        </asp:DropDownList>

                            <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select Customer"
                            ControlToValidate="ddlFilterCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    </td>
                    <td>
                      <asp:TextBox runat="server" ID="tbxFilter" Width="190px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td>
                        <%--<asp:LinkButton ID="LinkButton_sampleForm"   class="newlink" Font-Underline="True" OnClick="lbtnExportExcel_Click"  runat="server">Export </asp:LinkButton>                                                --%>

                          <asp:Button Text="Export" runat="server" ID="btnDownload" OnClick="lbtnExportExcel_Click" CssClass="button"/>
                    </td>
                    <td style="width: 20%; padding-right: 20px;" align="right">Filter :
                          <asp:FileUpload ID="FU_Upload" runat="server" />
                    </td>
                    <td>
                       <asp:Button Text="Upload" runat="server" ID="btnUploadSave" OnClick="btnUploadSave_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                    </td>
                    
                </tr>
               
            </table>
              <div style="margin: 5px 5px;">
                    <div style="margin-bottom: 4px">                      
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />

                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                 </div>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdClient" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                         <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ComplianceID" />
                         <asp:BoundField DataField="DepartmentID" HeaderText="DepartmentID" ItemStyle-Width="50px" SortExpression="DepartmentID" />
                        <asp:TemplateField HeaderText="Branch Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="BranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadSave" />
             <asp:PostBackTrigger ControlID="btnDownload" />
            
        </Triggers>
    </asp:UpdatePanel>      
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlFilterCustomer.ClientID %>").combobox();            
        }
    </script>
</asp:Content>
