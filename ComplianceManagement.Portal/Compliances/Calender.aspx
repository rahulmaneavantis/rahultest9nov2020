﻿<%@ Page Title="My Calender" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="Calender.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Calender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" src="assets/js/plugins/fullcalendar/fullcalendar-script.js"></script>--%>
     <style type="text/css">
        .fc-widget-header{width:8% !important;}
        .fc-week0 {
      height: 50px !important; 
        }
        .fc-header-title h2{font-size:14px;font-weight:500;}
        .fc-content {
    clear: both;
    margin-top: 16px;
}
         .fc-event-vert .fc-event-bg, .fc-grid .fc-other-month .fc-day-number {
             opacity: .0;
             filter: alpha(opacity=30);
         }
       </style>
   <script type="text/javascript">
    $(document).ready(function () {
        fhead('My Calender');
        setactivemenu('leftcalendermenu');
    });

 </script>

     <script type="text/javascript">
         //Method for Datetime Conversion start//
         function timeconvert(ds) {
             var D, dtime, T, tz, off,
             dobj = ds.match(/(\d+)|([+-])|(\d{4})/g);
             T = parseInt(dobj[0]);
             tz = dobj[1];
             off = dobj[2];
             if (off) {
                 off = (parseInt(off.substring(0, 2), 10) * 3600000) +
                 (parseInt(off.substring(2), 10) * 60000);
                 if (tz == '-') off *= -1;
             }
             else off = 0;
             return new Date(T += off).toUTCString();
         }
         //Method for Datetime Conversion End//

         // Convert date in dd-mm-yyyy format
         function convert(str) {
             var date = new Date(str),
                 mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                 day = ("0" + date.getDate()).slice(-2);
             hours = ("0" + date.getHours()).slice(-2);
             minutes = ("0" + date.getMinutes()).slice(-2);
             return [date.getFullYear(), mnth, day].join("-");
         }

         $(document).ready(function () {
             var event1 = [];
             var AddItem = [];
             var CheckMores = [];
             $.ajax({
                 type: "POST",
                 contentType: "application/json",
                 data: "{}",
                 url: "Calender.aspx/GetEvents",
                 dataType: "json",
                 success: function (data) {
                     for (var i = 0 ; data.d.length > i; i++) {
                         var objDate = new Date(timeconvert(data.d[i].ScheduledOn));
                    
                         if (1==1) {
                             event1.push({
                                 id: "",
                                 title: "",
                                 start: objDate,
                                 backgroundColor: 'darkgreen',
                                 width:'100%',
                                 borderColor: 'darkgreen',
                                 url: "/Controls/frmUpcomingCompliancess.aspx?ScheduledOn=" + convert(objDate),
                             });
                         }
                     }
                     var date = new Date();
                     var d = date.getDate();
                     var m = date.getMonth();
                     var y = date.getFullYear();

                     $('#calendar0').fullCalendar({
                         header: {
                             left: '',
                             center: 'title',
                             right: ''
                         },
                         month: m - 1,
                         editable: false,
                         contentHeight: 400,
                         droppable: false, // this allows things to be dropped onto the calendar
                         eventLimit: false, // allow "more" link when too many events
                         events: event1,
                         eventRender: function (event, element, view) {
                             if (event.start.getMonth() !== view.start.getMonth()) { return false; }
                         },
                     });
                     $('#calendar2').fullCalendar({
                         header: {
                             left: '',
                             center: 'title',
                             right: ''
                         },
                         month: m + 1,
                         editable: false,
                         contentHeight: 400,
                         droppable: false, // this allows things to be dropped onto the calendar
                         eventLimit: false, // allow "more" link when too many events
                         events: event1,
                         eventRender: function (event, element, view) {
                             if (event.start.getMonth() !== view.start.getMonth()) { return false; }
                         },
                     });
                     $('#calendar1').fullCalendar({
                         header: {
                             left: '',
                             center: 'title',
                             right: ''
                         },

                         editable: false,
                         contentHeight: 400,
                         droppable: false, // this allows things to be dropped onto the calendar
                         eventLimit: false, // allow "more" link when too many events
                         events: event1,
                         eventRender: function (event, element, view) {
                             if (event.start.getMonth() !== view.start.getMonth()) { return false; }
                         },
                     });
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {
                    
                     alert("error2");
                 }
             });
           
            
             $('#myprevbutton').click(function () {
                 $('#calendar0').fullCalendar('prev');
                 $('#calendar1').fullCalendar('prev');
                 $('#calendar2').fullCalendar('prev');
             });
             $('#mynextbutton').click(function () {
                 $('#calendar0').fullCalendar('next');
                 $('#calendar1').fullCalendar('next');
                 $('#calendar2').fullCalendar('next');
             });
         });
        
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="section">
            <div class="divider"></div>
            <div id="full-calendar">
                <div class="row">
                  <div class="col s12 m8 l9 " style="float:right;"><a id="myprevbutton" style="cursor:pointer;"><span class="fc-button-content">&nbsp;◄&nbsp;</span></a><a id="mynextbutton" style="cursor:pointer;"><span class="fc-button-content">&nbsp;►&nbsp;</span></a> </div>
                    <div style="clear:both;height:5px;"></div>
                    <div class="col s12 m8 l9">
                        <div id='calendar0' style="float:left;margin-right:10px;width:32%;"></div>
                          <div id='calendar1' style="float:left;margin-right:10px;width:32%;"></div>
                          <div id='calendar2' style="float:left;width:32%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       <div style="clear:both;height:15px;"></div>
</asp:Content>
