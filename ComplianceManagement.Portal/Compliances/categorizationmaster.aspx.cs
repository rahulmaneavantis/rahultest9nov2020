﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class categorizationmaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        BindCategory();
                        BindCategorizationList();
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindCategory()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = categorization.GetAll("");
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("< Select Category >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                mst_categorization objcat = new mst_categorization()
                {
                    Name = txtFName.Text.Trim(),
                    IsActive = false,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedDate = DateTime.Now,
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    objcat.ID = Convert.ToInt32(ViewState["categoryid"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (categorization.categoryExists(objcat))
                    {
                        CustomModifyDepartment.IsValid = false;
                        CustomModifyDepartment.ErrorMessage = "Category already exists";
                    }
                    else
                    {
                        categorization.CreatecategorizationMaster(objcat);
                        CustomModifyDepartment.IsValid = false;
                        CustomModifyDepartment.ErrorMessage = "Category saved successfully";
                        txtFName.Text = string.Empty;
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (categorization.categoryExists(objcat))
                    {
                        CustomModifyDepartment.IsValid = false;
                        CustomModifyDepartment.ErrorMessage = "Category already exists";
                    }
                    else
                    {
                        categorization.UpdateDepartmentMaster(objcat);
                        CustomModifyDepartment.IsValid = false;
                        CustomModifyDepartment.ErrorMessage = "Category updated successfully";
                    }
                }
                BindCategory();
                BindCategorizationList();
                upCategoryList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCategorizationList()
        {
            try
            {
                var CategoryMasterList = categorization.GetAllcategorizationList();

                

                var filter = tbxFilter.Text;
                if (!string.IsNullOrEmpty(filter))
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.Name.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                int categoryid = -1;
                if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
                {
                    if (ddlCategory.SelectedValue != "-1")
                    {
                        categoryid = Convert.ToInt32(ddlCategory.SelectedValue);
                    }
                }
                if (categoryid != -1)
                {
                    CategoryMasterList = CategoryMasterList.Where(entry => entry.ID == categoryid).ToList();
                }


                grdCategorization.DataSource = CategoryMasterList;
                Session["TotalRows"] = CategoryMasterList.Count;
                grdCategorization.DataBind();
                upModifycategory.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCategorization_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int categoryid = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Category"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["categoryid"] = categoryid;
                    var RPD = categorization.categorizationMasterGetByIDAudit(categoryid);
                    txtFName.Text = RPD.Name;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyCategory\").dialog('open');", true);
                    upModifycategory.Update();
                }
                else if (e.CommandName.Equals("DELETE_Category"))
                {
                    categorization.DeletecategorizationMaster(categoryid);
                    BindCategorizationList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCategorizationList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCategorization.PageIndex = 0;
                BindCategorizationList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCategoryList_Load(object sender, EventArgs e)
        {
        }
        protected void grdCategorization_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCategorization.PageIndex = e.NewPageIndex;
                BindCategorizationList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upModifycategory_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComboboxForAssignmentsDialog", "initializeComboboxForAssignmentsDialog();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                upModifycategory.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenDialog", "$(\"#divModifyCategory\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}