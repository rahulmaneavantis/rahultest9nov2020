﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ClientComplianceDepartmentMappingList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ClientComplianceDepartmentMappingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('#divUsersDialog').dialog({
                height: 380,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Compliance and Department Mapping",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function initializeCombobox(flag) {

            if (flag == 1) {
                $("#<%= ddlCustomer.ClientID %>").combobox();
            }
        }

        function UncheckallOption()
        {
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='DepartmentSelectAll']");
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkDepartment") > -1) {
                    cbox.checked = false;
                }
                rowCheckBoxHeader[0].checked = false;
            }
        }

        function disableCombobox() {

            $(".custom-combobox").attr('disabled', 'disabled');
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkDepartment") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        
    $(document).ready(function () {

        $("#btnRepeater").click(function () {
            $("#dvDept").toggle("blind", null, 500, function () { });
        });
    });

        function initializeJQueryUIDeptDDL() {
            $("#<%= txtDepartment.ClientID %>").unbind('click');

            $("#<%= txtDepartment.ClientID %>").click(function () {
                $("#dvDept").toggle("blind", null, 500, function () { });
            });
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkDepartment']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkDepartment']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='DepartmentSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <table width="100%">
                <tr>
                    <td style="vertical-align: bottom; width: 25%;" >
                        <div id="divCustomerfilter" runat="server" style="margin-left: 10px">
                            <label style="width: 110px; display: block; float: left; font-size: 13px; color: White; margin-bottom: -5px;">
                                Select Customer :</label>
                            <div style="width: 150px; float: left; margin-top: -11px; margin-left: 10px;">
                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                            </div>
                        </div>
                    </td>
                    <td style="width: 25%; padding-right: 60px;">
                        <asp:DropDownList runat="server" ID="ddlcompType" Style="padding: 0px;float: left;margin-bottom: -5px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlcompType_SelectedIndexChanged">
                            <asp:ListItem Text="Statutory" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Internal" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                    </td>
                    <td align="right" style="width: 25%; padding-right: 60px;">
                        <label style="font-size: 13px;">
                            Filter :</label>
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td align="right" class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAddUser" OnClick="btnAddUser_Click"  OnClientClick="UncheckallOption();"/>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                <asp:GridView runat="server" ID="grdMapping" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCreated="grdMapping_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" OnSorting="grdMapping_Sorting"
                    Font-Size="12px" DataKeyNames="CustomerId" OnRowCommand="grdMapping_RowCommand" OnPageIndexChanging="grdMapping_PageIndexChanging"
                    OnRowDataBound="grdMapping_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ComplianceID" />
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName" />
                        <asp:BoundField DataField="ShortDescription" HeaderText="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ShortDescription" />
                        <asp:BoundField DataField="DepartmentName" HeaderText="Department" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="DepartmentName" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUsersDialog">
        <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" OnLoad="upUsers_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="UserValidationGroup" />

                        <asp:CustomValidator ID="cvEmailError" runat="server" EnableClientScript="False"
                            ErrorMessage="Email already exists." ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divCustomer" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true" />
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select customer."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divDepartment" style="margin-bottom: 7px">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                Department</label>
                            <asp:TextBox runat="server" ID="txtDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 160px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvDept" class="dvDeptHideshow">
                                <asp:Repeater ID="rptDepartment" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:CheckBox ID="DepartmentSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClientClick="HidedropdownList()" OnClick="btnRepeater_Click"/></td>
                                                <%--OnClick="btnRefresh_Click" --%>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkDepartment" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblDeptID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblDeptName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>                      
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select department."
                            ControlToValidate="ddlCustomer" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="DivType" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Internal" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Type."
                            ControlToValidate="ddlType" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="UserValidationGroup" Display="None" />
                    </div>
                    <div runat="server" id="divProduct" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Comma Separated Compliance ID:
                        </label>
                        <asp:TextBox runat="server" ID="txtComplianceIDList" TextMode="MultiLine" Style="height: 100px; width: 390px;" />

                        <asp:RegularExpressionValidator ID="regValidator" runat="server"
                            ControlToValidate="txtComplianceIDList" ErrorMessage="Please Enter Valid Comma Separated ComplianceID !"
                            ValidationGroup="UserValidationGroup" Display="None"
                            ValidationExpression="^([0-9\,]+)">Please Enter Valid Compliance ID</asp:RegularExpressionValidator>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtComplianceIDList" Display="None"
                            ValidationGroup="UserValidationGroup"
                            ErrorMessage="please enter comma separated ComplianceID"></asp:RequiredFieldValidator>

                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Compliance ID list can not be empty."
                            ControlToValidate="txtComplianceIDList" runat="server" ValidationGroup="UserValidationGroup"
                            Display="None" />--%>
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="UserValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divUsersDialog').dialog('close');" />
                    </div>
                </div>

                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
