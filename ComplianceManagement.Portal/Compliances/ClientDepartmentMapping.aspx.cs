﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ClientDepartmentMapping : System.Web.UI.Page
    {                  
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                              
                BindCustomersList();                                                                
            }
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ComplianceInstanceUpdate");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdFilterRCMData"]);
                    if (view.Count > 0)
                    {
                        ExcelData = view.ToTable("Selected", false, "ID", "ComplianceId", "BranchName", "ShortDescription", "DepartmentID");
                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "InstanceID";
                        exWorkSheet.Cells["A1"].AutoFitColumns(10);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "ComplianceId";
                        exWorkSheet.Cells["B1"].AutoFitColumns(30);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Branch";
                        exWorkSheet.Cells["C1"].AutoFitColumns(10);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Short Description";
                        exWorkSheet.Cells["D1"].AutoFitColumns(30);

                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "DepartmentID";
                        exWorkSheet.Cells["E1"].AutoFitColumns(10);

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            //Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=ComplianceInstanceExportForUpdate.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindComplianceFilter();
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindComplianceFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerid = -1;
                    if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                    {
                        customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                    }
                    if (customerid != -1)
                    {
                        grdClient.DataSource = null;
                        grdClient.DataBind();
                        var ClientcomplianceDetails = (from row in entities.sp_InstanceDepartmentMapping(customerid)
                                                       select row).ToList();

                        if (ClientcomplianceDetails.Count > 0)
                        {

                            if (!string.IsNullOrEmpty(tbxFilter.Text))
                            {
                                if (CheckInt(tbxFilter.Text))
                                {
                                    int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                    ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.DepartmentID == a).ToList();
                                }
                                else
                                {
                                    ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                                }
                            }
                            grdClient.DataSource = ClientcomplianceDetails;
                            grdClient.DataBind();
                            Session["grdFilterRCMData"] = (grdClient.DataSource as List<sp_InstanceDepartmentMapping_Result>).ToDataTable();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }    
        protected void ddlFilterCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }     
        }                    
        private void BindCustomersList()
        {
            try
            {
                var details= CustomerManagement.GetAll(-1);
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = details;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlFilterCustomer.SelectedIndex = -1;
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }               
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);                                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }                      
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {          
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                        
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {            
        }
       
        #region Upload                
        private bool SheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ComplianceInstanceUpdate"))
                    {
                        if (sheet.Name.Trim().Equals("ComplianceInstanceUpdate") || sheet.Name.Trim().Equals("ComplianceInstanceUpdate") || sheet.Name.Trim().Equals("ComplianceInstanceUpdate"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }             
        public int getActID(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ActID = -1;
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ChecklistComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ActID).FirstOrDefault();
                    ActID = query;
                }
                return ActID;
            }
        }
        public partial class CBCTempTable
        {
            public long ComplianceId { get; set; }            
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

        public bool DepartmentExists(int deptid,int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == deptid
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void btnUploadSave_Click(object sender, EventArgs e)
        {

            int customerid = -1;
            if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
            {
                customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
            }
            if (customerid != -1)
            {
                if (FU_Upload.HasFile)
                {
                    try
                    {
                        string filename = Path.GetFileName(FU_Upload.FileName);
                        FU_Upload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                        FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flag = SheetsExitsts(xlWorkbook, "ComplianceInstanceUpdate");
                                if (flag == true)
                                {                                    
                                    ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["ComplianceInstanceUpdate"];
                                    List<string> errorMessage = new List<string>();
                                    List<string> saveerrorMessage = new List<string>();
                                    List<CBCTempTable> lstTemptable = new List<CBCTempTable>();
                                    //List<ClientBasedCheckListFrequency> CBCheckListFrequency = new List<ClientBasedCheckListFrequency>();

                                    if (xlWorksheetvalidation != null)
                                    {
                                        //ExcelData = view.ToTable("Selected", false, "ID", "BranchName", "ComplianceId", "ShortDescription", "DepartmentID");
                                        if (GetDimensionRows(xlWorksheetvalidation) != 0)
                                        {
                                            int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                                            #region Validations
                                            int valDepartmentID = -1;
                                            int valcomplianceinstanceid = -1;
                                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                            {
                                                valcomplianceinstanceid = -1;
                                                valDepartmentID = -1;

                                                #region 1 ComplianceID
                                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                                {
                                                    valcomplianceinstanceid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim());
                                                }
                                                if (valcomplianceinstanceid == 0 || valcomplianceinstanceid == -1)
                                                {
                                                    errorMessage.Add("Required ComplianceInstanceID at row number-" + rowNum);
                                                }
                                                else
                                                {
                                                    if (!lstTemptable.Any(x => x.ComplianceId == valcomplianceinstanceid))
                                                    {
                                                        CBCTempTable tt = new CBCTempTable();
                                                        tt.ComplianceId = valcomplianceinstanceid;
                                                        lstTemptable.Add(tt);
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Compliance with this ComplianceInstanceID (" + valcomplianceinstanceid + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                                    }
                                                }
                                                #endregion

                                                #region 5 DepartmentID

                                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 5].Text.ToString()))
                                                {
                                                    if (CheckInt(xlWorksheetvalidation.Cells[rowNum, 5].Text.ToString()))
                                                    {
                                                        valDepartmentID = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 5].Text);
                                                    }
                                                }
                                                if (valDepartmentID != -1 || valDepartmentID != 0)
                                                {
                                                    if (DepartmentExists(valDepartmentID, customerid) == false)
                                                    {
                                                        errorMessage.Add("DepartmentID not Defined in the System at row number-" + rowNum);
                                                    }
                                                }
                                                #endregion
                                            }
                                            #endregion

                                            if (errorMessage.Count > 0)
                                            {
                                                ErrorMessages(errorMessage);
                                            }
                                            else
                                            {
                                                #region Save
                                                string noupdatedid = string.Empty;
                                                int DepartmentID = -1;
                                                int complianceinstanceid = -1;
                                                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                                                {
                                                    customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                                                }
                                                
                                                for (int saverowNum = 2; saverowNum <= xlrow2; saverowNum++)
                                                {
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 1].Text.ToString().Trim()))
                                                    {
                                                        complianceinstanceid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 1].Text.Trim());
                                                    }
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 5].Text.ToString().Trim()))
                                                    {
                                                        DepartmentID = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 5].Text.Trim());
                                                    }

                                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                    {
                                                        var ltlmToUpdate = (from row in entities.ComplianceInstances
                                                                            where row.ID == complianceinstanceid                                                                          
                                                                            select row).FirstOrDefault();

                                                        ltlmToUpdate.DepartmentID = DepartmentID;                                                    
                                                        entities.SaveChanges();                                                   
                                                    }
                                                }
                                                if (saveerrorMessage.Count > 0)
                                                {
                                                    ErrorMessages(saveerrorMessage);
                                                }
                                                noupdatedid = string.Empty;
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixCreation'.";
                                }
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                        }
                    }
                    catch (Exception ex)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "please select Customer.";
            }
        }
        #endregion
    }
}