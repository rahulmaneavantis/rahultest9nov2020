﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;
using System.Threading;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Collections;
using System.Data;
using System.IO;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CompanyAdminChList : System.Web.UI.Page
    {
        public string Role;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";
                ViewState["Role"] = Role;
                BindCompliances();
            }
        }

        private void BindCompliances()
        {
            try
            {            
                var Checklist = DashboardManagement.DashboardDataForPerformer_Checklist(AuthenticationHelper.UserID,AuthenticationHelper.CustomerID);

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.ForMonth).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Status")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.Status).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventName")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.EventName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventNature")
                    {
                        Checklist = Checklist.OrderBy(entry => entry.EventNature).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.ForMonth).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Status")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.Status).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventName")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.EventName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventNature")
                    {
                        Checklist = Checklist.OrderByDescending(entry => entry.EventNature).ToList();
                    }
                    direction = SortDirection.Descending;
                    direction = SortDirection.Ascending;
                }
                grdComplianceTransactions.DataSource = Checklist;
                grdComplianceTransactions.DataBind();
            }
               
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //SaveCheckedValues();
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindCompliances();
                //PopulateCheckedValues();
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {                
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> assignmentList = null;               
                assignmentList = DashboardManagement.DashboardDataForPerformer_Checklist(AuthenticationHelper.UserID,AuthenticationHelper.CustomerID);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();

                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    SP_GetCheckListCannedReportCompliancesSummary_Result rowView = (SP_GetCheckListCannedReportCompliancesSummary_Result)e.Row.DataItem;
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    CheckBox chkCompleted = (CheckBox)e.Row.FindControl("chkCompleted");
                    CheckBox chkNotCompleted = (CheckBox)e.Row.FindControl("chkNotCompleted");
                    Label lblEventID = (Label)e.Row.FindControl("lblEventID");
                    LinkButton lnkDownload = (LinkButton)e.Row.FindControl("lnkDownload");
                    Label lblReferenceMaterialText = (Label)e.Row.FindControl("lblReferenceMaterialText");
                    Label lblFilePath = (Label)e.Row.FindControl("lblFilePath");

                    
                    
                    //string strScheduleon = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ScheduledOn"));
                    string CheckDate = "1/1/0001 12:00:00 AM";
                    if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                    {
                        //btn_Edit.Visible = false;

                        lblScheduledOn.Visible = false;
                        chkCompleted.Visible = false;
                        chkNotCompleted.Visible = false;
                        lblEventID.Visible = false;
                        lnkDownload.Visible = false;
                        lblReferenceMaterialText.Visible = false;
                        //lblEventName.Visible = false;
                    }
                    else
                    {
                        lblScheduledOn.Visible = true;
                    }

                    if (rowView.ComplianceInstanceID != -1)
                    {
                        //Label lblImpact = (Label)e.Row.FindControl("lblImpact");
                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");
                                        

                        long Complianceid = Convert.ToInt64(lblComplianceId.Text);
                        
                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }

                        Label lblCheckListTypeID = (Label)e.Row.FindControl("lblCheckListTypeID");
                        Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                        if((lblForMonth.Text == null) || (lblForMonth.Text == ""))
                        {
                            if(lblCheckListTypeID.Text == "0")
                            {
                                lblForMonth.Text = "One Time";
                            }
                            else if(lblCheckListTypeID.Text == "2")
                            {
                                lblForMonth.Text = "Time Based";
                            }
                        }

                        //Label lblReferenceMaterialText = (Label)e.Row.FindControl("lblReferenceMaterialText");
                        //Label lblFilePath = (Label)e.Row.FindControl("lblFilePath");
                        if ((lblFilePath.Text != "") && (lblFilePath.Text != null))
                        {
                            lnkDownload.Visible = true;
                        }
                        else
                        {
                            lnkDownload.Visible = false;
                        }

                        if ((lblReferenceMaterialText.ToolTip != "") && (lblReferenceMaterialText.ToolTip != null))
                        {
                            lblReferenceMaterialText.Visible = true;
                        }
                        else
                        {
                            lblReferenceMaterialText.Visible = false;
                        }


                        

                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }


        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        protected void chkCompletedSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            CheckBox chkNotCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            foreach (GridViewRow row in grdComplianceTransactions.Rows)
            {
                CheckBox chkCompleted = (CheckBox)row.FindControl("chkCompleted");
                CheckBox chkNotCompleted = (CheckBox)row.FindControl("chkNotCompleted");
                if (ChkBoxHeader.Checked == true)
                {
                    chkCompleted.Checked = true;
                    chkNotCompletedSelectAll.Enabled = false;
                    chkNotCompleted.Enabled = false;

                }
                else
                {
                    chkCompleted.Checked = false;
                    chkNotCompletedSelectAll.Enabled = true;
                    chkNotCompleted.Enabled = true;
                }
            }
        }

        protected void chkCompleted_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            CheckBox chkNotCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceTransactions.Rows[i];
                CheckBox chkNotCompleted = (CheckBox)row.FindControl("chkNotCompleted");
                if (((CheckBox)row.FindControl("chkCompleted")).Checked)
                {
                    
                    countCheckedCheckbox = countCheckedCheckbox + 1;
                    chkNotCompleted.Enabled = false;
                }
                else
                {
                    chkNotCompleted.Enabled = true; ;
                }
            }
            if(countCheckedCheckbox == grdComplianceTransactions.Rows.Count -1)
            {
                ChkBoxHeader.Checked = true;
                chkNotCompletedSelectAll.Enabled = false;
            }
            else
            {
                ChkBoxHeader.Checked = false;
                chkNotCompletedSelectAll.Enabled = true;
            }
            
        }

        protected void chkNotCompletedSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkNotCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            CheckBox chkCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            foreach (GridViewRow row in grdComplianceTransactions.Rows)
            {
                CheckBox chkNotCompleted = (CheckBox)row.FindControl("chkNotCompleted");
                CheckBox chkCompleted = (CheckBox)row.FindControl("chkCompleted");
                if (chkNotCompletedSelectAll.Checked == true)
                {
                    chkNotCompleted.Checked = true;
                    chkCompletedSelectAll.Enabled = false;
                    chkCompleted.Enabled = false;
                }
                else
                {
                    chkNotCompleted.Checked = false;
                    chkCompletedSelectAll.Enabled = true;
                    chkCompleted.Enabled = true;
                }
            }
        }

        protected void chkNotCompleted_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkNotCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            CheckBox chkCompletedSelectAll = (CheckBox)grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll"); 
            
            int countCheckedCheckboxNC = 0;
            for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceTransactions.Rows[i];
                CheckBox chkCompleted = (CheckBox)row.FindControl("chkCompleted");
                if (((CheckBox)row.FindControl("chkNotCompleted")).Checked)
                {                 
                    countCheckedCheckboxNC = countCheckedCheckboxNC + 1;
                    chkCompleted.Enabled = false;
                }
                else
                {
                    chkCompleted.Enabled = true; 
                }
            }
            if (countCheckedCheckboxNC == grdComplianceTransactions.Rows.Count -1)
            {
                chkNotCompletedSelectAll.Checked = true;
                chkCompletedSelectAll.Enabled = false;

            }
            else
            {
                chkNotCompletedSelectAll.Checked = false;
                chkCompletedSelectAll.Enabled = true;
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
             try
            {

                for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
                {
                    string status = "1";
                    GridViewRow row = grdComplianceTransactions.Rows[i];
                   
                    CheckBox chkCompleted = (CheckBox)row.FindControl("chkCompleted");
                    CheckBox chkNotCompleted = (CheckBox)row.FindControl("chkNotCompleted");
                    Label lblInstanceID = (Label)row.FindControl("lblInstanceID");
                    Label lblScheduleOnID = (Label)row.FindControl("lblScheduleOnID");
                    if (chkCompleted.Checked)
                    {
                        if (lblScheduleOnID.Text != "")
                        {
                            //status = "4";//closed timely
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = Convert.ToInt32(lblInstanceID.Text),
                                    ComplianceScheduleOnID = Convert.ToInt32(lblScheduleOnID.Text),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = 4,//closed timely
                                    Remarks = "Closed Timely"
                                };
                                transaction.Dated = DateTime.UtcNow;
                                entities.ComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }           
                        }
                    }
                    if (chkNotCompleted.Checked)
                    {
                        if (lblScheduleOnID.Text != "")
                        {
                            //status = "6";//not Complied
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = Convert.ToInt32(lblInstanceID.Text),
                                    ComplianceScheduleOnID = Convert.ToInt32(lblScheduleOnID.Text),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = 6,//not Complied
                                    Remarks = "Not Complied"
                                };
                                transaction.Dated = DateTime.UtcNow;
                                entities.ComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                        }         
                    }
                }
                BindCompliances();
                
             
            }
             catch (Exception ex)
             {
                 LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                 //cvDuplicateEntry.IsValid = false;
                 //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
             }
        }

        //This method is used to save the checkedstate of values
        private void SaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            long index = -1;
        
            foreach (GridViewRow gvrow in grdComplianceTransactions.Rows)
            {
                Label lblScheduledOn = (Label)gvrow.FindControl("lblScheduledOn");
                 string CheckDate = "1/1/0001 12:00:00 AM";
                 if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                 {
                 }
                 else
                 {
                     long aaaa = (long)grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                     index = (long)grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                     bool result = ((CheckBox)gvrow.FindControl("chkCompleted")).Checked;

                     // Check in the Session
                     if (Session["CHECKED_ITEMS"] != null)
                         userdetails = (ArrayList)Session["CHECKED_ITEMS"];
                     if (result)
                     {
                         if (!userdetails.Contains(index))
                             userdetails.Add(index);
                     }
                     else
                         userdetails.Remove(index);
                 }
            }
            if (userdetails != null && userdetails.Count > 0)
                Session["CHECKED_ITEMS"] = userdetails;
        }
        private void PopulateCheckedValues()
        {
            ArrayList userdetails = (ArrayList)Session["CHECKED_ITEMS"];
            if (userdetails != null && userdetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceTransactions.Rows)
                {
                    Label lblScheduledOn = (Label)gvrow.FindControl("lblScheduledOn");
                    string CheckDate = "1/1/0001 12:00:00 AM";
                    if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                    {
                    }
                    else
                    {
                        long index = (long)grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                        if (userdetails.Contains(index))
                        {
                            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompleted");
                            myCheckBox.Checked = true;
                        }
                    }
                }
            }
        }

        //protected void lnkDownload_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        LinkButton lnkbtn = sender as LinkButton;
        //        GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
        //        Label lblFilePath = (Label)gvrow.FindControl("lblFilePath");
        //        Label lblFileName = (Label)gvrow.FindControl("lblFileName");
        //        //string filePath = gvDetails.DataKeys[gvrow.RowIndex].Value.ToString();
        //        string fileName = lblFileName.Text;
        //        string filePath = lblFilePath.Text;
        //        //////Response.ContentType = "image/jpg";
        //        ////Response.ContentType = "doc/docx";    
        //        //////Response.ContentType = "application/octet-stream";
        //        ////Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
        //        ////Response.TransmitFile(Server.MapPath(filePath));
        //        //////Response.End();
        //        ////HttpContext.Current.ApplicationInstance.CompleteRequest();

        //        //string filename = "filename from Database";
        //        //Response.ContentType = "application/octet-stream";
        //       // Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
        //        //string aaa = Server.MapPath(filePath);
        //       // Response.TransmitFile(Server.MapPath(filePath));


        //        //Response.ContentType = "image/jpg";
        //        //Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
        //        //Response.TransmitFile(Server.MapPath(filePath));
        //        ////Response.End();
        //        //HttpContext.Current.ApplicationInstance.CompleteRequest();

        //        Response.Clear();
        //        Response.BufferOutput = false;
        //        Response.ContentType = "application/octet-stream";
        //        //Response.AddHeader(“Content-Length”, fileLength);
               
        //        Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
        //        Response.TransmitFile(filePath);
        //        Response.Flush();

                
               
             
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //cvDuplicateEntry.IsValid = false;
        //        //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                Label lblFilePath = (Label)gvrow.FindControl("lblFilePath");
                Label lblFileName = (Label)gvrow.FindControl("lblFileName");
                //if (lblFileName.Text.EndsWith(".txt"))
                //{
                //    Response.ContentType = "application/txt";
                //}
                //else if (lblFileName.Text.EndsWith(".pdf"))
                //{
                //    Response.ContentType = "application/pdf";
                //}
                //else if (lblFileName.Text.EndsWith(".docx"))
                //{
                //    //Response.ContentType = "application/docx";
                //    Response.ContentType = "application/ms-word";
                //}
                //else
                //{
                //    Response.ContentType = "image/jpg";
                //}
                Response.ContentType = "application/octet-stream";
                string filePath = lblFilePath.Text;
              
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}