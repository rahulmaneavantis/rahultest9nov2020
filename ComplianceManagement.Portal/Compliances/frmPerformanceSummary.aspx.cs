﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class frmPerformanceSummary : System.Web.UI.Page
    {
      
        int checkInternalapplicable = 0;
      
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                lblErrorMessage.Text = string.Empty;
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;

                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    User user = UserManagement.GetByID(userID);

                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }
                    if (checkInternalapplicable == 1)
                    {
                        Tab2.Visible = true;
                    }
                    else if (checkInternalapplicable == 0)
                    {
                        Tab1.Visible = true;
                        Tab2.Visible = false;
                    }

                    Tab1_Click(sender, e);
                }
            }
        }
        public void SetRole()
        {
            try
            {
                var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                foreach (var ar in AssignedRoles)
                {
                    string name = ar.Name;
                    if (name !="Event Owner")
                    {
                        if (name.Equals("Reviewer1") || name.Equals("Reviewer2"))
                        {
                            name = "Reviewer";
                        }


                        ListItem item = new ListItem(name, ar.ID.ToString());
                        ListItem item1 = rblRole.Items.FindByText(name);
                        if (item1 == null)
                        {
                            rblRole.Items.Add(item);
                        }
                    }
                    
                }
                if (AssignedRoles.Count > 0)
                {
                    rblRole.SelectedIndex = 0;
                }               
                if (rblRole.SelectedValue.Equals("4"))
                {
                    ucPerformanceSummaryforReviewer.Visible = true;
                    ucPerformanceSummaryforPerformer.Visible = false;                   
                }
                else
                {
                    ucPerformanceSummaryforReviewer.Visible = false;
                    ucPerformanceSummaryforPerformer.Visible = true;   
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        public void SetRoleForInternal()
        {
            try
            {
                var AssignedRoles = ComplianceManagement.Business.InternalComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                foreach (var ar in AssignedRoles)
                {
                    string name = ar.Name;
                    if (name.Equals("Reviewer1") || name.Equals("Reviewer2"))
                    {
                        name = "Reviewer";
                    }
                 

                    ListItem item = new ListItem(name, ar.ID.ToString());
                    ListItem item1 = rdInternalaRoleList.Items.FindByText(name);
                    if (item1 == null)
                    {
                        rdInternalaRoleList.Items.Add(item);
                    }
                }

                if (AssignedRoles.Count > 0)
                {
                    rdInternalaRoleList.SelectedIndex = 0;
                }
                if (rblRole.SelectedValue.Equals("4"))
                {
                    UcInternalPerformanceSummaryforReviewer.Visible = true;
                    UcInternalPerformanceSummaryforPerformer.Visible = false;
                }
                else
                {
                    UcInternalPerformanceSummaryforReviewer.Visible = false;
                    UcInternalPerformanceSummaryforPerformer.Visible = true;
                }  
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
          
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            SetRole();          
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
            SetRoleForInternal();                       
        }
        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblRole.SelectedValue.Equals("3"))
                {
                    ucPerformanceSummaryforReviewer.Visible = false;
                    ucPerformanceSummaryforPerformer.Visible = true;   
                }
                else if (rblRole.SelectedValue.Equals("4"))
                {
                    ucPerformanceSummaryforReviewer.Visible = true;
                    ucPerformanceSummaryforPerformer.Visible = false;  
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void rdInternalaRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdInternalaRoleList.SelectedValue.Equals("3"))
                {
                    UcInternalPerformanceSummaryforReviewer.Visible = false;
                    UcInternalPerformanceSummaryforPerformer.Visible = true;
                }
                else if (rdInternalaRoleList.SelectedValue.Equals("4"))
                {
                    UcInternalPerformanceSummaryforReviewer.Visible = true;
                    UcInternalPerformanceSummaryforPerformer.Visible = false;                    
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessageInternal.Text = "Server Error Occured. Please try again.";
            }
        }

    }
}