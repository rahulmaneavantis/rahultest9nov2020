﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class EditComplianceListMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {                
                BindCategories();
                BindTypes();
                BindActs();
         
                BindDueDates();
                BindFrequencies();
                BindFilterFrequencies();
                BindNatureOfCompliance();
                //   BindCompliances();
                BindEvents();
                BindIndustry();
                BindEntityType();
                BindDueDays();
                //BindLegalEntityType();
                txtIndustry.Attributes.Add("readonly", "readonly");
                txtEntityType.Attributes.Add("readonly", "readonly");
                //if (AuthenticationHelper.Role.Equals("SADMN"))
                //    btnAddCompliance.Visible = true;                
            }
        }
        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chbImprisonment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                divImprisonmentDetails.Visible = ((CheckBox)sender).Checked;
                if (!divImprisonmentDetails.Visible)
                {
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                //ViewState["ComplianceParameters"] = new List<ComplianceParameter>();
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;

                tbxRequiredForms.Text = tbxDescription.Text = tbxNonComplianceEffects.Text = tbxSections.Text = string.Empty;
                ddlNatureOfCompliance.SelectedIndex = ddlComplianceType.SelectedIndex = ddlAct.SelectedIndex = ddlFrequency.SelectedIndex = ddlDueDate.SelectedIndex = ddlNonComplianceType.SelectedIndex = ddlRiskType.SelectedIndex = ddlEvents.SelectedIndex = 0;
                divComplianceDueDays.Visible = false;
                divEvent.Visible = false;
                tbxSubEvent.Text = "< Select Sub Event >";
                tvSubEvent.Nodes.Clear();
                txtEventDueDate.Text = string.Empty;
                chkDocument.Checked = false;
                lblSampleForm.Text = "< Not selected >";
                ddlNonComplianceType_SelectedIndexChanged(null, null);

                ddlComplianceType_SelectedIndexChanged(null, null);

                rbReminderType.SelectedValue = "0";
                txtReminderBefore.Text = string.Empty;
                txtReminderGap.Text = string.Empty;
                txtPenaltyDescription.Text = string.Empty;
                txtShortDescription.Text = string.Empty;

                txtIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
                txtEntityType.Text = "< Select >";

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    chkEntityType.Checked = false;
                    CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                    EntityTypeSelectAll.Checked = false;
                }

                tbxOnetimeduedate.Text = string.Empty;
                divForCustome.Visible = false;
                divChecklist.Visible = false;
                divOneTime.Visible = false;
                chkEventBased.Checked = false;
                upComplianceDetails.Update();
                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = true;

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
               
                Business.Data.Compliance compliance = new Business.Data.Compliance()
                {
                    ActID = Convert.ToInt32(ddlAct.SelectedValue),
                    Description = tbxDescription.Text,
                    Sections = tbxSections.Text,
                    ShortDescription = txtShortDescription.Text,
                    UploadDocument = chkDocument.Checked,
                    ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                    RequiredForms = tbxRequiredForms.Text,
                    RiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                    PenaltyDescription = txtPenaltyDescription.Text,
                    ReferenceMaterialText = txtReferenceMaterial.Text,
                    //EffectiveDate = dtEffectiveDate,
                    UpdatedOn = DateTime.Now,
                    //DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue)
                };
                if (ddlWeekDueDay.SelectedValue != "-1")
                {
                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                }
                if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2 || compliance.ComplianceType == 3)//function based or time based and one time statutory
                {
                    if (ddlNatureOfCompliance.SelectedValue != "")
                    {
                        if (ddlNatureOfCompliance.SelectedValue != "-1")
                        {
                            compliance.NatureOfCompliance = Convert.ToByte(ddlNatureOfCompliance.SelectedValue);
                        }
                    }                    
                    if (compliance.ComplianceType == 3)//time based
                    {
                        string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                        DateTime dtOneTimeDate = new DateTime();
                        if (Onetimedate != "")
                        {
                            dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            compliance.OneTimeDate = dtOneTimeDate;
                        }
                    }
                    else
                    {
                        compliance.OneTimeDate = null;
                    }
                    if (compliance.ComplianceType == 2)//time based
                    {
                        if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                        {
                            compliance.EventID = null;
                            compliance.Frequency = null;
                            compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                            compliance.SubComplianceType = 0;
                        }
                        else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                        {
                            compliance.EventID = null;
                            compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                            compliance.DueDate = null;
                            compliance.SubComplianceType = 1;
                        }
                        else
                        {
                            compliance.EventID = null;
                            compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                            compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                            compliance.SubComplianceType = 2;
                        }
                    }
                    else
                    {

                        if (chkEventBased.Checked == true)
                        {
                            compliance.Frequency = null;
                            if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                            {
                                compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                            }
                            else
                            {
                                compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                            }

                            compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                            compliance.EventComplianceType = Convert.ToByte(rbEventComplianceType.SelectedValue);
                        }
                        else
                        {
                            compliance.EventID = null;
                            compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                            compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                        }
                    }


                    compliance.NonComplianceType = ddlNonComplianceType.SelectedValue == "-1" ? (byte?)null : Convert.ToByte(ddlNonComplianceType.SelectedValue);
                    compliance.NonComplianceEffects = tbxNonComplianceEffects.Text;

                    compliance.FixedMinimum = tbxFixedMinimum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMinimum.Text) : (double?)null;
                    compliance.FixedMaximum = tbxFixedMaximum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMaximum.Text) : (double?)null;
                    if (ddlPerDayMonth.SelectedValue == "0")
                    {
                        compliance.VariableAmountPerDay = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                        compliance.VariableAmountPerMonth = null;
                    }
                    else
                    {
                        compliance.VariableAmountPerMonth = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                        compliance.VariableAmountPerDay = null;
                    }
                    compliance.VariableAmountPerDayMax = tbxVariableAmountPerDayMax.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDayMax.Text) : (double?)null;
                    compliance.VariableAmountPercent = tbxVariableAmountPercent.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercent.Text) : (double?)null;
                    compliance.VariableAmountPercentMax = tbxVariableAmountPercentMaximum.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercentMaximum.Text) : (double?)null;

                    compliance.Imprisonment = chbImprisonment.Checked;
                    compliance.Designation = tbxDesignation.Text;
                    compliance.MinimumYears = tbxMinimumYears.Text.Length > 0 ? Convert.ToInt32(tbxMinimumYears.Text) : (int?)null;
                    compliance.MaximumYears = tbxMaximumYears.Text.Length > 0 ? Convert.ToInt32(tbxMaximumYears.Text) : (int?)null;
                }
                else
                {
                    compliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);
                    if (chkEventBased.Checked == true)
                    {
                        if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                        {
                            compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                        }
                        else
                        {
                            compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                            compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                        }
                        compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                    }

                    compliance.NatureOfCompliance = null;
                    compliance.NonComplianceType = null;
                    compliance.NonComplianceEffects = null;
                    //------checklist------
                    if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based
                    {
                        if (compliance.CheckListTypeID == 2)//time based
                        {
                            if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                            {
                                compliance.EventID = null;
                                compliance.Frequency = null;
                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                compliance.SubComplianceType = 0;
                            }
                            else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                compliance.DueDate = null;
                                compliance.SubComplianceType = 1;
                            }
                            else
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                compliance.SubComplianceType = 2;
                            }
                        }
                        else
                        {
                            if (chkEventBased.Checked == true)
                            {
                                compliance.Frequency = null;
                                if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                                {
                                    compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                }
                                else
                                {
                                    compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                    compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                                }
                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                compliance.EventComplianceType = Convert.ToByte(rbEventComplianceType.SelectedValue);
                            }
                            else
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                            }
                        }
                    }
                    else//one time checklist
                    {
                        string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                        DateTime dtOneTimeDate = new DateTime();
                        if (Onetimedate != "")
                        {
                            dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            compliance.OneTimeDate = dtOneTimeDate;
                        }
                    }
                }
                
                 
                ComplianceForm form = null;
                if (chkDocument.Checked)
                {
                    if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                    {
                        fuSampleFile.SaveAs(Server.MapPath("~/ComplianceFiles/" + fuSampleFile.FileName));
                        form = new ComplianceForm()
                        {
                            Name = fuSampleFile.FileName,
                            FileData = fuSampleFile.FileBytes,
                            FilePath = "~/ComplianceFiles/" + fuSampleFile.FileName
                        };
                    }
                }
                compliance.ReminderType = Convert.ToByte(rbReminderType.SelectedValue);
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    compliance.ReminderBefore = Convert.ToInt32(txtReminderBefore.Text);
                    compliance.ReminderGap = Convert.ToInt32(txtReminderGap.Text);
                }
                List<ComplianceParameter> parameters = new List<ComplianceParameter>(); //GetComplianceParameters();

                if ((int)ViewState["Mode"] == 1)
                {
                    compliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {

                    #region Create
                    Business.ComplianceManagement.Create(compliance, form);

                    //---------add Industry--------------------------------------------
                    List<int> IndustryIds = new List<int>();
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        if (chkIndustry.Checked)
                        {
                            IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));
                            IndustryMapping IndustryMapping = new IndustryMapping()
                            {
                                ComplianceId = compliance.ID,
                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),
                            };
                            Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                        }
                    }
                    //---------add Legal Entity type--------------------------------------------
                    List<int> EntityTypeIds = new List<int>();
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        if (chkEntityType.Checked)
                        {
                            EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                            LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                            {
                                ComplianceId = compliance.ID,
                                LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),

                            };
                            Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                        }
                    }
                    #endregion
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    Business.ComplianceManagement.EditCompliance(compliance, form, AuthenticationHelper.UserID, AuthenticationHelper.User);
                    
                    /// Add compliance new filed in  ComplianceDetail table SACHIN 04 Aug 2017
                    bool chkexistsComplianceDetail = Business.ComplianceManagement.ExistsComplianceDetail(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceDetail == false)
                    {
                        ComplianceDetail compliancedetail = new ComplianceDetail()
                        {
                            ComplianceID = compliance.ID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };
                        Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);
                    }
                    else
                    {
                        Business.ComplianceManagement.UpdateComplianceDetail(compliance.ID, AuthenticationHelper.UserID);
                    }

                    //---------add Industry--------------------------------------------
                    List<int> IndustryIds = new List<int>();
                    Business.ComplianceManagement.UpdateIndustryMappedID(compliance.ID);
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        if (chkIndustry.Checked)
                        {
                            IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                            IndustryMapping IndustryMapping = new IndustryMapping()
                            {
                                ComplianceId = compliance.ID,
                                IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),
                            };
                            Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                        }
                    }

                    //---------add Legal Entity type--------------------------------------------
                    List<int> EntityTypeIds = new List<int>();
                    Business.ComplianceManagement.UpdateLegalEntityMappedID(compliance.ID);
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        if (chkEntityType.Checked)
                        {
                            EntityTypeIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                            LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                            {
                                ComplianceId = compliance.ID,
                                LegalEntityTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                IsActive = true,
                                EditedDate = DateTime.UtcNow,
                                EditedBy = Convert.ToInt32(Session["userID"]),

                            };
                            Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                        }
                    }


                    // Changes Log Entry
                    if (Session["Compliance"] != null)
                    {
                        Business.Data.Compliance CompliancePrevious = (Business.Data.Compliance)Session["Compliance"];

                        if (CompliancePrevious != null)
                        {
                            try
                            {
                                DateTime? changestartdate = null;
                                DateTime? changeenddate = null;
                                DateTime? changeeffectivedate = null;
                                var istempper = string.Empty;
                                if (rbtChangeType.SelectedItem.Text == "Temporary")
                                {
                                    istempper = "TEM";
                                    changestartdate = DateTime.ParseExact(txtChangeStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//, txtChangeStartDate.Text ;
                                    changeenddate = DateTime.ParseExact(txtChangeEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);//, txtChangeEndDate.Text ;
                                }
                                else if (rbtChangeType.SelectedItem.Text == "Permanent")
                                {
                                    changeeffectivedate = DateTime.ParseExact(txtChangeEffectiveDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture); //txtChangeEffectiveDate.Text ;
                                    istempper = "PER";
                                }

                                ChangeShortDetailedDescription Chsdd = new ChangeShortDetailedDescription()
                                {
                                    ComplianceId = compliance.ID,
                                    IsTemp_Per = istempper,
                                    OldShortdescription = CompliancePrevious.ShortDescription,
                                    OldDetailesdescription = CompliancePrevious.Description,
                                    NewShortDescripton = txtShortDescription.Text,
                                    NewDetailedDescription = tbxDescription.Text,
                                    Startdate = changestartdate,
                                    EndDate = changeenddate,
                                    EffectiveDate = changeeffectivedate,
                                    CreatedOn = DateTime.Now,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    IsProcessed = false,
                                    IsActive = false,
                                };
                                CreateChangeShortDetailedDescription(Chsdd);
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), "ChangeShortDetailedDescription");
                            }
                            if (!CompliancePrevious.Equals(compliance))
                            {
                                List<ChangeLog> ChangeLogList = new List<ChangeLog>();
                                if (CompliancePrevious.ShortDescription != compliance.ShortDescription)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "ShortDescription",
                                        OldValue = CompliancePrevious.ShortDescription,
                                        NewValue = compliance.ShortDescription,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }

                                if (CompliancePrevious.Description != compliance.Description)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "Description",
                                        OldValue = CompliancePrevious.Description,
                                        NewValue = compliance.Description,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }

                                if (CompliancePrevious.Frequency != compliance.Frequency)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "Frequency",
                                        OldValue = CompliancePrevious.Frequency.ToString(),
                                        NewValue = compliance.Frequency.ToString(),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }

                                if (CompliancePrevious.DueDate != compliance.DueDate)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "DueDate",
                                        OldValue = CompliancePrevious.DueDate.ToString(),
                                        NewValue = compliance.DueDate.ToString(),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }

                                if (CompliancePrevious.PenaltyDescription != compliance.PenaltyDescription)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "PenaltyDescription",
                                        OldValue = CompliancePrevious.PenaltyDescription,
                                        NewValue = compliance.PenaltyDescription,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }

                                if (CompliancePrevious.ReferenceMaterialText != compliance.ReferenceMaterialText)
                                {
                                    ChangeLog newLog = new ChangeLog()
                                    {
                                        ComplianceID = compliance.ID,
                                        ChangeType = "ReferenceMaterialText",
                                        OldValue = CompliancePrevious.ReferenceMaterialText,
                                        NewValue = compliance.ReferenceMaterialText,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedOn = DateTime.Now,
                                    };

                                    ChangeLogList.Add(newLog);
                                }
                                Business.ComplianceManagement.CreateChangeLogs(ChangeLogList);
                            }
                        }
                    }

                    //Compliance_Log Table

                    string compliancetype = string.Empty;
                    string Frequency = string.Empty;
                    string duedate = string.Empty;
                 
                    if (ddlFrequency.SelectedValue != null)
                    {
                       Frequency = Convert.ToString(ddlFrequency.SelectedValue);
                    }
                    else
                    {
                        Frequency = "";
                    }
                    if(compliance.ComplianceType==1)
                    {
                        if(compliance.CheckListTypeID != null)
                        {
                            compliance.ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue);
                            compliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);
                            compliancetype = compliance.ComplianceType + " checklist type=" + compliance.CheckListTypeID;
                        }
                    }
                    else
                    {
                        compliance.ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue);
                        compliancetype = Convert.ToString(ddlComplianceType.SelectedValue);
                    }
                   if(ddlDueDate.SelectedValue!=null)
                    {
                        duedate = Convert.ToString(ddlDueDate.SelectedValue);
                    }
                   else
                    {
                        duedate = " ";
                    }
                        var NewData1 = Business.ComplianceManagement.ComplianceNewData(Frequency, duedate, compliancetype);
                        var OldData1 = ViewState["OldData"];

                        Compliance_Log comp = new Compliance_Log()
                        {
                            OldData = Convert.ToString(OldData1),
                            NewData = Convert.ToString(NewData1),
                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                            ComplianceID=compliance.ID

                        };
                        Business.ComplianceManagement.CreateCompliance_Log(comp);
                
                  


                    
                    //----Notification Table

                    if (chkNotification.Checked == true && txtNotificationRemark.Text != "")
                    {
                        long NotificationID = 0;

                        Notification newNotification = new Notification()
                        {
                            ActID = Convert.ToInt32(ddlAct.SelectedValue),
                            ComplianceID = compliance.ID,
                            Type = "Compliance",
                            Remark = txtNotificationRemark.Text,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now.Date,
                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now.Date,
                        };

                        //if (!Business.ComplianceManagement.ExistsNotification(newNotification))
                        //{
                        Business.ComplianceManagement.CreateNotification(newNotification);
                        NotificationID = newNotification.ID;
                        //}
                        //else
                        //{
                        //    NotificationID=Business.ComplianceManagement.UpdateNotification(newNotification);
                        //}


                        if (NotificationID != 0)
                        {
                            //Get All Users Compliance to Assigned 
                            List<long> ListUsers = Business.ComplianceManagement.GetAllUsersAssignedByComplianceID(compliance.ID);
                            ListUsers = ListUsers.Distinct().ToList();
                            //For each User Create Entry in Notification Table
                            ListUsers.ForEach(EachUser =>
                            {
                                UserNotification UNF = new UserNotification()
                                {
                                    NotificationID = NotificationID,
                                    UserID = Convert.ToInt32(EachUser),
                                    IsRead = false,
                                };

                                if (!Business.ComplianceManagement.ExistsUserNotification(UNF))
                                    Business.ComplianceManagement.CreateUserNotification(UNF);
                                else
                                    Business.ComplianceManagement.UpdateNotification(newNotification);
                            });

                            #region Notification
                            //add in EditComplianceListMaster after edit Compliance
                            //code for send notification to mobile                          
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                List<string> details = new List<string>();
                                if (ListUsers.Count > 0)
                                {
                                    details.Clear();
                                    foreach (var item in ListUsers)
                                    {
                                        var deviceIDList = (from row in entities.MaintainDeviceIDs
                                                            where row.Product.Equals("C") && row.UserID == item && row.Status.Equals("Active")
                                                            select row.DeviceToken).FirstOrDefault();

                                        if (!string.IsNullOrEmpty(deviceIDList))
                                        {
                                            details.Add(deviceIDList);
                                        }
                                    }
                                    details = details.Where(x => x != "null").ToList();
                                    string[] arr1 = details.Select(i => i.ToString()).ToArray();
                                    if (arr1.Length > 0)
                                    {
                                        string chk = System.Text.RegularExpressions.Regex.Replace(txtNotificationRemark.Text, "<[^>]*>", "");
                                        NotificationManagement.SendNotificationMobile(arr1, chk, "Compliance Updates");
                                    }
                                    details.Clear();
                                    details = null;
                                }
                            }
                            #endregion

                            #region Research Mail Send
                            var researchemailnotification = Business.ComplianceManagement.GetAllUsersAssignedResearchByComplianceID(compliance.ID);
                            if (researchemailnotification.Count > 0)
                            {
                                foreach (var item in researchemailnotification)
                                {
                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(item.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(item.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ResearchNotification
                                                            .Replace("@User", item.Name)
                                                            .Replace("@ShortDescription", item.ShortDescription)
                                                            .Replace("@Remark", txtNotificationRemark.Text.Trim())
                                                            .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                            .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() =>
                                        {
                                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"],
                             (new string[] { item.Email }).ToList(), null, null, "AVACOM Notification for compliances change.", message);
                                        }).Start();
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceDetailsDialog\").dialog('close')", true);
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        private bool DeepCompare(object obj, object another, List<ChangeLog> ChangeList)
        {
            if (ReferenceEquals(obj, another)) return true;

            if ((obj == null) || (another == null)) return false;

            //Compare two object's class, return false if they are difference
            if (obj.GetType() != another.GetType()) return false;

            var result = true;
            //Get all properties of obj
            //And compare each other
            foreach (var property in obj.GetType().GetProperties())
            {
                var objValue = property.GetValue(obj);
                var anotherValue = property.GetValue(another);
                if (!objValue.Equals(anotherValue))
                {
                    return false;
                    //ChangeLog newLog = new ChangeLog()
                    //{
                    //    ComplianceID= Convert.ToInt32(objValue),
                    //    ChangeType=property.ToString(),
                        


                    //};
                }
                
            }

            return result;
        }
        private String BindRemarks()
        {
            String Remarks = String.Empty;

            if (Session["Compliance"] != null)
            {
                Business.Data.Compliance CompliancePrevious = (Business.Data.Compliance)Session["Compliance"];

                if (CompliancePrevious != null)
                {
                    //ShortDescription
                    if (CompliancePrevious.ShortDescription != "" && txtShortDescription.Text != "")
                    {
                        if (!CompliancePrevious.ShortDescription.ToUpper().Equals(txtShortDescription.Text.ToUpper().Trim()))
                        {
                            if (Remarks != "")
                                Remarks += "<br><b>Compliance Details Changed to-</b>" + txtShortDescription.Text.Trim();
                            else
                                Remarks = "<b>Compliance Details Changed to-</b>" + txtShortDescription.Text.Trim();
                        }
                    }

                    //DueDate
                    if (CompliancePrevious.DueDate.ToString() != ddlDueDate.SelectedItem.Text.Trim())
                    {
                        if (Remarks != "")
                            Remarks += "<br><b>New Due Date is-</b>" + ddlDueDate.SelectedItem.Text.Trim();
                        else
                            Remarks = "<b>New Due Date is-</b>" + ddlDueDate.SelectedItem.Text.Trim();
                    }

                    //PenaltyDescription
                    if (txtPenaltyDescription.Text != "" && CompliancePrevious.PenaltyDescription != "")
                    {
                        if (!CompliancePrevious.PenaltyDescription.ToUpper().Equals(txtPenaltyDescription.Text.ToUpper().Trim()))
                        {
                            if (Remarks != "")
                                Remarks += "<br><b>Penalty Revised to-</b>" + txtPenaltyDescription.Text.Trim();
                            else
                                Remarks = "<b>Penalty Revised to-</b>" + txtPenaltyDescription.Text.Trim();
                        }
                    }

                    //Frequency
                    if (CompliancePrevious.Frequency.ToString() != ddlFrequency.SelectedValue.ToString())
                    {
                        if (Remarks != "")
                            Remarks += "<br><b>Revised Frequency is-</b>" + ddlFrequency.SelectedItem.Text.Trim();
                        else
                            Remarks = "<b>Revised Frequency is-</b>" + ddlFrequency.SelectedItem.Text.Trim();
                    }

                    //ReferenceMaterialText
                    if (txtReferenceMaterial.Text != "" && CompliancePrevious.ReferenceMaterialText != "")
                    {
                        if (!CompliancePrevious.ReferenceMaterialText.ToUpper().Equals(txtReferenceMaterial.Text.ToUpper().Trim()))
                        {
                            if (Remarks != "")
                                Remarks += "<br><b>New Reference Material to-</b>" + txtReferenceMaterial.Text.Trim();
                            else
                                Remarks = "<b>New Reference Material to-</b>" + txtReferenceMaterial.Text.Trim();
                        }
                    }

                    //Description
                    if (tbxDescription.Text != "" && CompliancePrevious.Description != "")
                    {
                        if (!CompliancePrevious.Description.ToUpper().Equals(tbxDescription.Text.ToUpper().Trim()))
                        {
                            if (Remarks != "")
                                Remarks += "<br><b>Compliance Details Changed to-</b>" + tbxDescription.Text.Trim();
                            else
                                Remarks = "<b>Compliance Details Changed to-</b>" + tbxDescription.Text.Trim();
                        }
                    }
                }
            }

            return Remarks;
        }
        //private String BindRemarks()
        //{
        //    String Remarks = String.Empty;

        //    if (Session["Compliance"] != null)
        //    {
        //        Business.Data.Compliance CompliancePrevious = (Business.Data.Compliance)Session["Compliance"];

        //        if (CompliancePrevious != null)
        //        {      
        //            //ShortDescription
        //            if (!CompliancePrevious.ShortDescription.ToUpper().Equals(txtShortDescription.Text.ToUpper().Trim()))
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>Compliance Details Changed to-</b>" + txtShortDescription.Text.Trim();
        //                else
        //                    Remarks = "<b>Compliance Details Changed to-</b>" + txtShortDescription.Text.Trim();
        //            }

        //            //DueDate
        //            if (CompliancePrevious.DueDate.ToString() != ddlDueDate.SelectedItem.Text.Trim())
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>New Due Date is-</b>" + ddlDueDate.SelectedItem.Text.Trim();
        //                else
        //                    Remarks = "<b>New Due Date is-</b>" + ddlDueDate.SelectedItem.Text.Trim();
        //            }

        //            //PenaltyDescription
        //            if (!CompliancePrevious.PenaltyDescription.ToUpper().Equals(txtPenaltyDescription.Text.ToUpper().Trim()))
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>Penalty Revised to-</b>" + txtPenaltyDescription.Text.Trim();
        //                else
        //                    Remarks = "<b>Penalty Revised to-</b>" + txtPenaltyDescription.Text.Trim();
        //            }

        //            //Frequency
        //            if (CompliancePrevious.Frequency.ToString() != ddlFrequency.SelectedValue.ToString())
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>Revised Frequency is-</b>" + ddlFrequency.SelectedItem.Text.Trim();
        //                else
        //                    Remarks = "<b>Revised Frequency is-</b>" + ddlFrequency.SelectedItem.Text.Trim();
        //            }

        //            //ReferenceMaterialText
        //            if (!CompliancePrevious.ReferenceMaterialText.ToUpper().Equals(txtReferenceMaterial.Text.ToUpper().Trim()))
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>New Reference Material to-</b>" + txtReferenceMaterial.Text.Trim();
        //                else
        //                    Remarks = "<b>New Reference Material to-</b>" + txtReferenceMaterial.Text.Trim();
        //            }

        //            //Description
        //            if (!CompliancePrevious.Description.ToUpper().Equals(tbxDescription.Text.ToUpper().Trim()))
        //            {
        //                if (Remarks != "")
        //                    Remarks += "<br><b>Compliance Details Changed to-</b>" + tbxDescription.Text.Trim();
        //                else
        //                    Remarks = "<b>Compliance Details Changed to-</b>" + tbxDescription.Text.Trim();
        //            }
        //        }
        //    }

        //    return Remarks;            
        //}

        private void OpenScheduleInformation(Business.Data.Compliance compliance)
        {
            try
            {
                ViewState["ComplianceID"] = compliance.ID;
                ViewState["Frequency"] = compliance.Frequency.Value;
                if (compliance.DueDate != null)
                {
                    ViewState["Day"] = compliance.DueDate.Value;
                }

                if ((compliance.Frequency.Value == 0 || compliance.Frequency.Value == 1))
                    divStartMonth.Visible = false;
                else
                    divStartMonth.Visible = true;

                var scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
                if (scheduleList.Count == 0)
                {
                    Business.ComplianceManagement.GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType);
                    scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
                }

                int step = 0;
                if (compliance.Frequency.Value == 0)
                    step = 0;
                else if (compliance.Frequency.Value == 1)
                    step = 2;
                else if (compliance.Frequency.Value == 2)
                    step = 5;
                else if (compliance.Frequency.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.Frequency.Value == 0 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();
                //if (compliance.EffectiveDate == null)
                //{
                //    lblEffectivedate.Text = "";
                //    lblEffectivedate.ForeColor = System.Drawing.Color.Red;
                //    lblEffectivedate.Text = "Please Enter Effective Date First";
                //}
                //else
                //{
                //    lblEffectivedate.Text = "";
                //    lblEffectivedate.Text = Convert.ToDateTime(compliance.EffectiveDate).ToString("dd-MMM-yyyy");
                //}
                if (divStartMonth.Visible)
                {                   
                    if (compliance.SubComplianceType == 1)
                    {
                        if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
                            ddlStartMonth.SelectedValue = Convert.ToString(1);
                        else
                            ddlStartMonth.SelectedValue = Convert.ToString(4);
                    }
                    else
                    {
                        ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                    }
                   
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkNotification_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNotification.Checked)
            {
                divNotification.Visible = true;
                txtNotificationRemark.Enabled = true;
            }
            else
            {
                divNotification.Visible = false;
                txtNotificationRemark.Enabled = false;
            }
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });


                    Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlDueDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNotificationRemark.Text = BindRemarks();
        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Frequency frequency = (Frequency)Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                if (frequency == Frequency.FourMonthly)
                                    specialMonth = startMonth + (step * i) + 4;
                                else
                                    specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {
                            if (frequency == Frequency.FourMonthly)
                                specialMonth = 4;
                            else
                                specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == Frequency.Monthly ? ((Month)month).ToString() : ((Month)month).ToString() + " - " + ((Month)((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");


                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindNatureOfCompliance()
        {
            try
            {
                ddlNatureOfCompliance.DataSource = Business.ComplianceManagement.GetAllComplianceNature();
                ddlNatureOfCompliance.DataTextField = "Name";
                ddlNatureOfCompliance.DataValueField = "ID";
                ddlNatureOfCompliance.DataBind();

                ddlNatureOfCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rbtChangeType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (rbtChangeType.SelectedItem.Text== "Temporary")
            {
                divtemp.Visible = true;
                divPerma.Visible = false;
                txtChangeStartDate.Text = "";
                txtChangeEndDate.Text = "";
                txtChangeEffectiveDate.Text = "";
            }
            else if (rbtChangeType.SelectedItem.Text == "Permanent")
            {
                txtChangeStartDate.Text = "";
                txtChangeEndDate.Text = "";
                txtChangeEffectiveDate.Text = "";
                divtemp.Visible = false;
                divPerma.Visible = true;
            }
            else
            {
                txtChangeStartDate.Text = "";
                txtChangeEndDate.Text = "";
                txtChangeEffectiveDate.Text = "";
                divtemp.Visible = false;
                divPerma.Visible = false;
            }           
        }

        protected void BindLogGrid(long ComplianceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    grdComplianceLog.DataSource = null;
                    grdComplianceLog.DataBind();
                    ViewState["ComplianceIdLog"] = ComplianceID;
                    var compliance = (from row in entities.sp_ChangeShortDetailedDescriptionLog(ComplianceID)
                                      select row).ToList();
                    if (compliance.Count > 0)
                    {
                        grdComplianceLog.DataSource = compliance;
                        grdComplianceLog.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            
        }
        protected void grdComplianceLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    var aaa = Convert.ToInt64(ViewState["ComplianceID"]);
                    grdComplianceLog.PageIndex = e.NewPageIndex;
                    BindLogGrid(aaa);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceLogDialog_Load(object sender, EventArgs e)
        {           
        }
        public static void CreateChangeShortDetailedDescription(ChangeShortDetailedDescription CSD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //        public static bool ipExists(Mst_BlockIP deptmaster)
                //{
                //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //    {
                //        var query = (from row in entities.ChangeShortDetailedDescription
                //                     where
                //                     row.IPAddress.ToUpper().Trim() == deptmaster.IPAddress.ToUpper().Trim()
                //                     && row.IPName.Trim() == deptmaster.IPName.Trim()
                //                     && row.IsActive == true
                //                     && row.CustomerID == deptmaster.CustomerID
                //                     select row);
                //        return query.Select(entry => true).FirstOrDefault();
                //    }
                //}s
                if (CSD.Startdate!=null && CSD.EndDate !=null)
                {
                    var query = (from row in entities.ChangeShortDetailedDescriptions
                                 where
                                 row.Startdate == CSD.Startdate
                                 && row.EndDate == (DateTime)CSD.EndDate
                                 select row).FirstOrDefault();
                    if (query ==null)
                    {
                        entities.ChangeShortDetailedDescriptions.Add(CSD);
                        entities.SaveChanges();
                    }

                }
                else
                {
                    entities.ChangeShortDetailedDescriptions.Add(CSD);
                    entities.SaveChanges();
                }
                
            }
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int complianceID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("Log_COMPLIANCE"))
                {
                    ViewState["ComplianceID"] = complianceID;
                    BindLogGrid(complianceID);
                    upComplianceLogDialog.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog3", "$(\"#divComplianceLOGDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    divtemp.Visible = false;
                    divPerma.Visible = false;
                    rbtChangeType.ClearSelection();
                    lblErrorMassage.Text = "";
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    Session["Compliance"] = compliance;
                    var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceID);
                    var vGetIndustryMappedIDs = Business.ComplianceManagement.GetIndustryMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }
                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                        {
                            IndustrySelectAll.Checked = true;
                        }
                        else
                        {
                            IndustrySelectAll.Checked = false;
                        }
                    }

                    var vGetLegalEntityTypeMappedID = Business.ComplianceManagement.GetLegalEntityTypeMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        chkEntityType.Checked = false;
                        CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");

                        for (int i = 0; i <= vGetLegalEntityTypeMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetLegalEntityTypeMappedID[i].ToString())
                            {
                                chkEntityType.Checked = true;
                            }
                        }
                        if ((rptEntityType.Items.Count) == (vGetLegalEntityTypeMappedID.Count))
                        {
                            EntityTypeSelectAll.Checked = true;
                        }
                        else
                        {
                            EntityTypeSelectAll.Checked = false;
                        }
                    }
                    txtIndustry.Text = "< Select >";
                    txtEntityType.Text = "< Select >";
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    ddlAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescription.Text = compliance.ShortDescription;
                    tbxDescription.Text = compliance.Description;
                    tbxSections.Text = compliance.Sections;
                    chkDocument.Checked = compliance.UploadDocument ?? false;
                    ddlComplianceType.SelectedValue = compliance.ComplianceType.ToString();
                    tbxRequiredForms.Text = compliance.RequiredForms;
                    ddlRiskType.SelectedValue = compliance.RiskType.ToString();
                    #region[Compliance_log]
                    string frequency = string.Empty;
                    string compliancetype = string.Empty;
                    string duedate = string.Empty;
                    if (compliance.DueDate != null)
                    {
                        duedate = Convert.ToString(compliance.DueDate);
                    }
                    else
                    {
                        duedate = "";
                    }
                    if (ddlComplianceType.SelectedValue != null)
                    {
                        if (ddlComplianceType.SelectedValue == "1")
                        {
                            ddlComplianceType.SelectedValue = Convert.ToString(compliance.ComplianceType);
                            ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                            compliancetype = compliance.ComplianceType + " checklist type=" + compliance.CheckListTypeID;
                        }
                        else
                        {
                            compliancetype = ddlComplianceType.SelectedValue;
                        }

                    }
                    else
                    {
                        compliancetype = " ";
                    }
                    if (compliance.Frequency != null)
                    {
                        ddlFrequency.SelectedValue = Convert.ToString(compliance.Frequency);
                        frequency = Convert.ToString(compliance.Frequency);
                    }
                    else
                    {
                        frequency = "";

                    }


                    var OldData = Business.ComplianceManagement.ComplianceOldData(frequency, duedate, compliancetype);
                    ViewState["OldData"] = OldData;

                    #endregion
                    if (Convert.ToString(compliance.PenaltyDescription) != null)
                    {
                        txtPenaltyDescription.Text = compliance.PenaltyDescription.ToString();
                    }
                    if (Convert.ToString(compliance.ReferenceMaterialText) != null)
                    {
                        txtReferenceMaterial.Text = compliance.ReferenceMaterialText.ToString();
                    }
                    ddlComplianceType_SelectedIndexChanged(null, null);
                    if (complianceForm != null)
                    {
                        lblSampleForm.Text = complianceForm.Name;
                    }
                    else
                    {
                        lblSampleForm.Text = "< Not selected >";
                    }
                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2 || compliance.ComplianceType == 3)//function based or time based and one time  Statutory
                    {
                        ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();
                        chkEventBased.Checked = false;
                        divEvent.Visible = false;
                        divComplianceDueDays.Visible = false;
                        divNonEvents.Visible = true;
                        //ddlEvents.SelectedIndex = 0;
                        txtEventDueDate.Text = string.Empty;

                        if (ddlComplianceType.SelectedValue == "3")
                        {
                            vivDueDate.Visible = false;
                            tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        else
                        {
                            ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();
                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                            {
                                vivDueDate.Visible = false;
                            }
                            else
                            {
                                vivDueDate.Visible = true;
                            }
                            vivWeekDueDays.Visible = false;
                            if (ddlFrequency.SelectedValue == "8")
                            {
                                ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                vivWeekDueDays.Visible = true;
                            }
                            else
                            {
                                vivWeekDueDays.Visible = false;
                            }
                        }
                        if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                        {
                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                        }
                        else
                        {
                            if (compliance.DueDate != null && compliance.DueDate != -1)
                            {
                                if (compliance.DueDate <= 31)
                                {
                                    ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                }
                                else
                                {
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                            }
                        }

                        if (compliance.ComplianceType == 2)
                        {
                            if (compliance.SubComplianceType == 0)
                            {
                                divNonEvents.Visible = false;
                                divFrequency.Visible = false;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = false;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }
                            else if (compliance.SubComplianceType == 1)
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = false;
                                rgexEventDueDate.Enabled = false;
                            }
                            else
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }
                            rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                        }
                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }
                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);
                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;

                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }

                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;

                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }

                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;
                        }

                        //InitializeDateFilter(compliance.DueDate.HasValue > 0 ? DateTime.ParseExact(compliance.DueDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : DateTime.Now.Date);
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;
                    }
                    else// checklist
                    {

                        if (compliance.NonComplianceType != null)
                        {
                            divMonetary.Visible = compliance.NonComplianceType == 0 || compliance.NonComplianceType == 2;
                            divNonMonetary.Visible = compliance.NonComplianceType == 1 || compliance.NonComplianceType == 2;
                        }
                        else
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }


                        ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                        //------checklist------
                        if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                        {
                            divOneTime.Visible = false;
                            if (compliance.EventID != null)
                            {
                                chkEventBased.Checked = true;
                                divEvent.Visible = true;
                                divComplianceDueDays.Visible = true;
                                divNonEvents.Visible = false;
                                if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                                {
                                    ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                    ddlEvents_SelectedIndexChanged(null, null);

                                    if (compliance.SubEventID != null)
                                    {
                                        divSubEventmode.Visible = true;
                                        SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                        tbxSubEvent.Text = subevent.Name;
                                        SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                    }
                                }

                                if (compliance.DueDate != null)
                                {
                                    txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                                }
                                if (compliance.EventComplianceType != null)
                                {
                                    rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                                }
                            }
                            else
                            {
                                divFunctionBased.Visible = true;
                                divFrequency.Visible = true;
                                divNonComplianceType.Visible = false;

                                chkEventBased.Checked = false;
                                divEvent.Visible = false;
                                divComplianceDueDays.Visible = false;
                                divNonEvents.Visible = true;
                                //ddlEvents.SelectedIndex = 0;
                                txtEventDueDate.Text = string.Empty;
                                ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();

                                if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                                {
                                    ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                }
                                else
                                {
                                    if (compliance.DueDate != null && compliance.DueDate != -1)
                                    {
                                        if (compliance.DueDate <= 31)
                                        {
                                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                        }
                                        else
                                        {
                                            txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                        }
                                    }
                                }


                                //if (compliance.ComplianceType == 2)
                                if (compliance.CheckListTypeID == 2)
                                {
                                    divTimebasedTypes.Visible = true;
                                    if (compliance.SubComplianceType == 0)//fixed gap -- time based
                                    {
                                        divNonEvents.Visible = false;
                                        divFunctionBased.Visible = false;
                                        divNonComplianceType.Visible = false;
                                        divFrequency.Visible = false;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = false;
                                        divComplianceDueDays.Visible = true;
                                        rgexEventDueDate.Enabled = true;
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }
                                    else if (compliance.SubComplianceType == 1)
                                    {
                                        divNonEvents.Visible = true;
                                        divFunctionBased.Visible = true;
                                        divFrequency.Visible = true;
                                        divNonComplianceType.Visible = false;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = true;
                                        divComplianceDueDays.Visible = false;
                                        rgexEventDueDate.Enabled = false;
                                    }
                                    else
                                    {
                                        divNonEvents.Visible = true;
                                        divFrequency.Visible = true;
                                        vivDueDate.Visible = false;
                                        rfvEventDue.Enabled = false;
                                        cvfrequency.Enabled = true;
                                        divComplianceDueDays.Visible = true;
                                        rgexEventDueDate.Enabled = true;
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }

                                    rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                                }
                            }

                        }
                        else//one time checklist
                        {
                            if (compliance.EventID != null)
                            {
                                chkEventBased.Checked = true;
                                divEvent.Visible = true;
                                divComplianceDueDays.Visible = true;
                                divNonEvents.Visible = false;
                                divEventComplianceType.Visible = true;

                                if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                                {
                                    ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                                    ddlEvents_SelectedIndexChanged(null, null);

                                    if (compliance.SubEventID != null)
                                    {
                                        divSubEventmode.Visible = true;
                                        SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                                        tbxSubEvent.Text = subevent.Name;
                                        SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                                    }
                                }

                                if (compliance.DueDate != null)
                                {
                                    txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                                }
                                if (compliance.EventComplianceType != null)
                                {
                                    rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                                }
                            }
                            else
                            {
                                chkEventBased.Checked = false;
                                divEvent.Visible = false;
                                divComplianceDueDays.Visible = false;
                                divNonEvents.Visible = true;
                                ddlEvents.SelectedIndex = 0;
                                txtEventDueDate.Text = string.Empty;

                                divTimebasedTypes.Visible = false;
                                divFunctionBased.Visible = false;
                                divOneTime.Visible = true;

                                tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                                divComplianceDueDays.Visible = false;
                                txtEventDueDate.Text = null;
                            }
                        }
                    }

                    if (Convert.ToString(compliance.ReminderType) == "")
                    {
                        rbReminderType.SelectedValue = "0";
                        divForCustome.Visible = false;
                    }
                    else
                    {
                        rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                        if (compliance.ReminderType == 0)
                        {
                            divForCustome.Visible = false;
                        }
                        else
                        {
                            divForCustome.Visible = true;
                        }
                    }

                    txtReminderBefore.Text = Convert.ToString(compliance.ReminderBefore);
                    txtReminderGap.Text = Convert.ToString(compliance.ReminderGap);

                    upComplianceDetails.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                }

                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(complianceID);
                    if (chkexistsComplianceschedule == false)
                    {
                        Business.ComplianceManagement.Delete(complianceID);
                        BindCompliances();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned,can not deleted.";
                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    OpenScheduleInformation(compliance);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliances();
        }

        protected void txtShortDescription_TextChanged(object sender, EventArgs e)
        {
            txtNotificationRemark.Text = BindRemarks();
        }

        protected void tbxDescription_TextChanged(object sender, EventArgs e)
        {
            txtNotificationRemark.Text = BindRemarks();
        }

        protected void txt_TextChanged(object sender, EventArgs e)
        {
            txtNotificationRemark.Text = BindRemarks();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFunctionBased.Visible = vaDueDate.Enabled = divNatureOfCompliance.Visible = (ddlComplianceType.SelectedValue == "0" || ddlComplianceType.SelectedValue == "2");

                if (ddlComplianceType.SelectedValue == "0")//function based
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = true;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;
                }
                else if (ddlComplianceType.SelectedValue.Equals("2"))//timebased
                {
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = true;
                    divComplianceDueDays.Visible = true;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divNonComplianceType.Visible = true;
                    rbTimebasedTypes.SelectedValue = "0";

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;

                }
                else//checklist
                {
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;


                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = true;
                    ddlChklstType.SelectedValue = "0";
                    divOneTime.Visible = true;

                    //rbReminderType.Items[0].Enabled = false;
                    //rbReminderType.SelectedValue = "1";
                    //divForCustome.Visible = true;
                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = false;
                }

                ddlNonComplianceType_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlChklstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbReminderType.SelectedValue = "0";
            rbReminderType.Enabled = false;
            divOneTime.Visible = true;
            if (ddlChklstType.SelectedValue == "0")//one time
            {

                divOneTime.Visible = true;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = false;
                divNonEvents.Visible = false;
                divNonComplianceType.Visible = false;


                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = false;

            }
            else if (ddlChklstType.SelectedValue.Equals("1"))//function based
            {

                divOneTime.Visible = false;
                divTimebasedTypes.Visible = false;

                divFunctionBased.Visible = true;
                divNonEvents.Visible = true;
                divFrequency.Visible = true;
                vivDueDate.Visible = true;
                divTimebasedTypes.Visible = false;
                divComplianceDueDays.Visible = false;
                divNonComplianceType.Visible = false;



            }
            else if (ddlChklstType.SelectedValue.Equals("2"))//time based
            {

                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;
            }
            else
            {
                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = false;
                //rbReminderType.Items[0].Enabled = true;

            }


        }

        protected void rbTimebasedTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = false;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = true;

                    }


                }
                else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = false;
                    rgexEventDueDate.Enabled = false;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = true;
                        divFrequency.Visible = false;
                        cvfrequency.Enabled = false;
                    }
                }
                else
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    if (chkEventBased.Checked == true)
                    {
                        divComplianceDueDays.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlNonComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divMonetary.Visible = ddlNonComplianceType.SelectedValue == "0" || ddlNonComplianceType.SelectedValue == "2";
                divNonMonetary.Visible = ddlNonComplianceType.SelectedValue == "1" || ddlNonComplianceType.SelectedValue == "2";

                if (!divMonetary.Visible)
                {
                    tbxFixedMinimum.Text = tbxFixedMaximum.Text = tbxVariableAmountPerDay.Text = tbxVariableAmountPerDayMax.Text = tbxVariableAmountPercent.Text = tbxVariableAmountPercentMaximum.Text = string.Empty;
                }

                if (!divNonMonetary.Visible)
                {
                    chbImprisonment.Checked = false;
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                //if (lblEffectivedate.Text == "" || lblEffectivedate.Text == "Please Enter Effective Date First")
                //{
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "Effective Cannot be Blank.";
                //}
                //else
                //{
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {

                    List<ComplianceSchedule> scheduleList = new List<ComplianceSchedule>();

                    for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                    {
                        RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                        HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                        HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                        DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                        DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                        scheduleList.Add(new ComplianceSchedule()
                        {
                            ID = Convert.ToInt32(hdnID.Value),
                            ComplianceID = Convert.ToInt64(ViewState["ComplianceID"]),
                            ForMonth = Convert.ToInt32(hdnForMonth.Value),
                            SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                        });
                    }
                    Business.ComplianceManagement.UpdateComplianceUpdatedOn(Convert.ToInt64(ViewState["ComplianceID"]));
                    Business.ComplianceManagement.UpdateScheduleInformation(scheduleList);

                    //DateTime dEffectiveDate = Convert.ToDateTime(lblEffectivedate.Text.Trim());
                    //Business.ComplianceManagement.ComplianceScheduleOnUpdateAsNotActive(Convert.ToInt64(ViewState["ComplianceID"]), AuthenticationHelper.UserID, dEffectiveDate);                   

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                    upSchedulerRepeter.Update();
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeSubevent", string.Format("initializeJQueryUI('{0}', 'divSubevent');", tbxSubEvent.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeEntityTypeList", string.Format("initializeJQueryUI('{0}', 'dvEntityType');", txtEntityType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideEntityTypeList", "$(\"#dvEntityType\").hide(\"blind\", null, 5, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                //Start Date
                if (DateTime.TryParseExact(txtChangeStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {                    
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerStartDateChanges", string.Format("initializeDatePickerStartDateChanges(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerStartDateChanges", "initializeDatePickerStartDateChanges(null);", true);
                }
                //End Date
                if (DateTime.TryParseExact(txtChangeEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerENDDateChanges", string.Format("initializeDatePickerENDDateChanges(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerENDDateChanges", "initializeDatePickerENDDateChanges(null);", true);
                }

                //Change Effective Date
                if (DateTime.TryParseExact(txtChangeEffectiveDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerChangeEffectiveDatChanges", string.Format("initializeDatePickerChangeEffectiveDatChanges(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePickerChangeEffectiveDatChanges", "initializeDatePickerChangeEffectiveDatChanges(null);", true);
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                //{
                //    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;
                //    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");
                //    AsyncPostBackTrigger apbt = new AsyncPostBackTrigger();
                //    apbt.ControlID = ddlMonths.UniqueID;
                //    upComplianceScheduleDialog.Triggers.Add(apbt);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            //rfvFile.Enabled = chkDocument.Checked;
            if (!chkDocument.Checked)
            {
                lblSampleForm.Text = "< Not selected >";
            }
        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAll1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                        //Parameters = GetParameters(complianceInfo.Value)
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }

                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlComplinceCatagory.DataTextField = "Name";
                ddlComplinceCatagory.DataValueField = "ID";

                ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplinceCatagory.DataBind();

                ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEvents()
        {
            try
            {
                ddlEvents.DataTextField = "Name";
                ddlEvents.DataValueField = "ID";

                ddlEvents.DataSource = EventManagement.GetAllEvents(-1, string.Empty);
                ddlEvents.DataBind();

                ddlEvents.Items.Insert(0, new ListItem("< Select Event>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindFrequencies()
        {
            try
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";


                ddlFrequency.DataSource = Enumerations.GetAll<Frequency>();
                ddlFrequency.DataBind();
                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAct1_SelectedIndexChanged(object sender, EventArgs e)
        {

            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
            {
                BindCompliances();
            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }
        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                var acts = ActManagement.GetAllNVP();
                ddlAct.DataSource = acts;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlAct1.DataTextField = "Name";
                ddlAct1.DataValueField = "ID";

                ddlAct1.DataSource = acts;
                ddlAct1.DataBind();

                ddlAct1.Items.Insert(0, new ListItem("< Select Act >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCompliances()
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAllEditCompliance1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType,Convert.ToInt32(ddlAct1.SelectedValue), tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency));
                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                        //Parameters = GetParameters(complianceInfo.Value)
                    });
                }
                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Comment by rahul on 16 march 2016 from view delete
        //private void BindCompliances()
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        if (rdFunctionBased.Checked)
        //            CmType = 0;
        //        if (rdChecklist.Checked)
        //            CmType = 1;

        //        var compliancesData = Business.ComplianceManagement.GetAll(true, Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.Key.RiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.Key.RiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.Key.RiskType == 2)
        //                risk = "Low";

        //            string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency));
        //            dataSource.Add(new
        //            {
        //                complianceInfo.Key.ID,
        //                complianceInfo.Key.ActName,
        //                complianceInfo.Key.Sections,
        //                complianceInfo.Key.Description,
        //                complianceInfo.Key.ComplianceType,
        //                complianceInfo.Key.EventID,
        //                //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
        //                complianceInfo.Key.UploadDocument,
        //                complianceInfo.Key.RequiredForms,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency != null ? (int)complianceInfo.Key.Frequency : -1)),
        //                complianceInfo.Key.NonComplianceEffects,
        //                Risk = risk,
        //                complianceInfo.Key.ShortDescription,
        //                complianceInfo.Key.SubComplianceType,
        //                complianceInfo.Key.CheckListTypeID
        //                //Parameters = GetParameters(complianceInfo.Value)
        //            });
        //        }
        //        grdCompliances.DataSource = dataSource;
        //        grdCompliances.DataBind();
        //        upCompliancesList.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        #endregion
        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

       
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    LinkButton lbtSchedule = (LinkButton)e.Row.FindControl("LinkButton3");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    lbtSchedule.Visible = false;
                    btnSave.Visible = false;

                    if (AuthenticationHelper.Role.Equals("CADMN"))
                    {
                        lbtEdit.Visible = false;
                        lbtSchedule.Visible = false;
                        lbtDelete.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtSchedule.Visible = true;
                        lbtDelete.Visible = true;
                        btnSave.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("IMPT"))
                    {
                        lbtEdit.Visible = false;
                        lbtSchedule.Visible = false;
                        lbtDelete.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("UPDT"))
                    {
                        lbtEdit.Visible = true;
                        lbtSchedule.Visible = true;
                        lbtDelete.Visible = false;
                        btnSave.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("RPER"))
                    {
                        lbtEdit.Visible = false;
                        lbtSchedule.Visible = false;
                        lbtDelete.Visible = false;
                        btnSave.Visible = false;
                    }
                    if (AuthenticationHelper.Role.Equals("RREV"))
                    {
                        lbtEdit.Visible = true;
                        lbtSchedule.Visible = true;
                        lbtDelete.Visible = true;
                        btnSave.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void chkEventBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                vivDueDate.Visible = !chkEventBased.Checked;
                divEvent.Visible = chkEventBased.Checked;

                divComplianceDueDays.Visible = chkEventBased.Checked;
                cvfrequency.Enabled = !chkEventBased.Checked;
                divFrequency.Visible = !chkEventBased.Checked;
                vaDueDate.Enabled = !chkEventBased.Checked;
                if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divComplianceDueDays.Visible = !chkEventBased.Checked;
                }
                if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divComplianceDueDays.Visible = chkEventBased.Checked;
                    rfvEventDue.Enabled = chkEventBased.Checked;
                    vaDueDate.Enabled = chkEventBased.Checked;
                    cvfrequency.Enabled = !chkEventBased.Checked;
                    divFrequency.Visible = !chkEventBased.Checked;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool ViewSchedule(long? EventID, object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (Convert.ToString(frequency) == "Daily" || Convert.ToString(frequency) == "Weekly")
                {
                    return false;
                }
                else if (EventID != null)
                {
                    return false;
                }
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {
                        return true;

                    }

                }
                //else if (frequency != null)
                //if (Convert.ToInt32(CheckListTypeID) == 1)
                //{
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbReminderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    divForCustome.Visible = true;
                }
                else
                {
                    divForCustome.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation(int eventID)
        {
            try
            {
                //int customerID = -1;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                var subEvents = EventManagement.GetAllHierarchy(eventID);
                if (subEvents[0].Children.Count > 0)
                {
                    divSubEventmode.Visible = true;
                    foreach (var item in subEvents)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvSubEvent.Nodes.Add(node);
                    }
                }
                else
                {
                    divSubEventmode.Visible = false;
                }

                tvSubEvent.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxSubEvent.Text = "< Select Sub Event >";
            tvSubEvent.Nodes.Clear();
            if (Convert.ToInt32(ddlEvents.SelectedValue) != -1)
            {
                divSubEventmode.Visible = true;
                BindLocation(Convert.ToInt32(ddlEvents.SelectedValue));
            }
            else
            {
                divSubEventmode.Visible = false;
            }
        }

        protected void tvSubEvent_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                if (tvSubEvent.SelectedNode.ChildNodes.Count == 0)
                {
                    //divEventComplianceType.Visible = false;
                    tbxSubEvent.Text = tvSubEvent.SelectedNode != null ? Regex.Replace(tvSubEvent.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);
                    rbEventComplianceType.SelectedValue = "0";
                }
                else
                {
                    divEventComplianceType.Visible = true;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").show(\"true\", null, 500, function () { });", true);
                }



            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void TreeViewSelectedNode(string value)
        {
            foreach (TreeNode node in tvSubEvent.Nodes)
            {

                if (node.ChildNodes.Count > 0)
                {
                    foreach (TreeNode child in node.ChildNodes)
                    {
                        if (child.Value == value)
                        {
                            child.Selected = true;
                        }
                    }
                }
                else if (node.Value == value)
                {
                    node.Selected = true;
                }
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
        //private void BindIndustry()
        //{
        //    try
        //    {
        //        chkIndustry.DataTextField = "Name";
        //        chkIndustry.DataValueField = "ID";
        //        chkIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
        //        chkIndustry.DataBind();

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //private void BindLegalEntityType()
        //{
        //    try
        //    {
        //        chkLegalEntityType.DataTextField = "EntityTypeName";
        //        chkLegalEntityType.DataValueField = "ID";
        //        chkLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
        //        chkLegalEntityType.DataBind();

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private void BindIndustry()
        {
            try
            {

                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEntityType()
        {
            try
            {

                rptEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                rptEntityType.DataBind();

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");

                    if (!chkEntityType.Checked)
                    {
                        chkEntityType.Checked = true;
                    }
                }
                CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                EntityTypeSelectAll.Checked = true;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix();
            //if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
            //{
            //    setDateToGridView();
            //}
        }

       




    }
}