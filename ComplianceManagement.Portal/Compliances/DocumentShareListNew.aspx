﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="DocumentShareListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.DocumentShareListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../NewCSS/Document_Drive_Style.css" rel="stylesheet" />
    
    <script type="text/javascript">
         $(document).ready(function () {
         
             fhead('My Documents / Other Critical Documents');
              setactivemenu('DocumentShareListNew');
              //fmaters()              
         });

         $(document).ready(function () {
             $(".notification-row > ul > .dropdown").click(function () { $('.notification-row > ul > .dropdown').addClass('open') });
         });

        //$(document).ready(function () {
        //    $('ul.nav.pull-right.top-menu > .dropdown').click(function () { $(this).addClass('open') });
        //    fhead('My Documents /Critical Document(s)');
        //    setactivemenu('leftdocumentsmenu');
        //});
        $(document).ready(function () {
            FetchUSerDetail();
            $("button.multiselect").on("click", function () {
                $(this).parent().addClass("open");
            });
        });

        function OpenNewPermissionPopup() {
            FetchUSerDetail();
            $('#divOpenPermissionPopup').modal('show');
          
        }
        function fopenpopup() {
            settracknew('Critical Document', 'Create', 'Folder', '')
            $('#divOpenNewFolderPopup').modal('show');
        }
        function fclosepopup() {
            $('#divOpenNewFolderPopup').modal('hide');
        }

        function lnkNewTest() {
            
            $('#lnkDropdown1').addClass('open');
        }


        function FetchUSerDetail() {
            $('[id*=lstBoxUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - Owner(s) selected',
            });
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }

        .folder-size {
            height: 100%;
            width: 30px;
            float: left;
        }

        .table > tbody > tr > th {
            font-weight: bold;
        }

        .clsROWgrid {
            cursor: pointer;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                              <header class="panel-heading tab-bg-primary">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                         
                                        <li class="" id="li1" runat="server">
                                            <asp:LinkButton ID="LinkButton1"  PostBackUrl="~/ComplianceDocument/ComplianceDocumentList.aspx" runat="server" Style="padding-bottom:0px;display:none;">Compliance Document</asp:LinkButton>                                           
                                        </li>
                                         
                                        <li class="active"  id="li2" runat="server">
                                            <asp:LinkButton ID="LinkButton2" runat="server" Style="padding-bottom:0px;display:none;">Other Critical Document</asp:LinkButton>                                        
                                        </li>
                                         
                                    </ul>
                                </header>
                            <div style="clear:both;height:15px;"></div>
                       
                                  
                            <div class="col-lg-12 col-md-12" style="padding-left:0px;">                            
                            <div class="col-md-4" style="padding-left:0px;">   
                                 <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                        PlaceHolder="Type to Search Files, folders(e.g. tags, process and subprocess)" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                                 <div class="col-md-6 deleteandsharediv" style="display:none;float: left;text-align: right;">
                                <img src="../images/sharedrive.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="sharedrive" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to Share Document" onclick="settracknew('Critical Document','Share','Write','')"/>  
                                <img  class="deletedrive"  src="../images/deletedrive.png"  style="width:45px;height: 30px;cursor:pointer;margin-right:5px" onclick="settracknew('Critical Document','Delete','Folder','')" />

                                 </div>
                                <div class="col-md-2" style="float:right;">                                                                        
                            <li id="lnkDropdown1" class="dropdown" style="float:right;list-style: none;">
                                                               <button class="dropdown-toggle" onclick="lnkNewTest();" style="width: 95px;height: 42px;background-image: url(/images/Addnewicon.png);   background-color: white; border: none;" type="button" id="menu1" data-toggle="dropdown">
                             </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="margin: 0px -35px 0;">
                                    <li role="presentation">
                                    <a role="menuitem" tabindex="-1" >
                                    <asp:LinkButton ID="lnkAddNewFolder" runat="server" 
                                    OnClientClick="fopenpopup()" OnClick="lnkAddNewFolder_Click">
                                     New Folder
                                    </asp:LinkButton>
                                    </a>
                                    </li>                                 
                                </ul>
                            </li>
                       
                                </div>
                                 </div>
                              
                            
                            
                           <%-- <div class="col-lg-12 col-md-12" style="padding-left:0px;">                            
                            <div class="col-md-6" style="padding-left:0px;">   
                                 <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                        PlaceHolder="Type to Search Files, folders(e.g. tags, process and subprocess)" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                                 <div class="col-md-9">&nbsp;</div>
                                 </div>  
                             <div style="margin-bottom: 4px" />
                                             
                        <div style="float: right; margin-top: 5px;color:#666">                                                      
                            <li id="lnkDropdown1" class="dropdown" style="float:right;list-style: none;">
                                                               <button class="dropdown-toggle" style="width: 51px;height: 33px;background-image: url(/images/Addnewicon.png);   background-color: white; border: none;" type="button" id="menu1" data-toggle="dropdown">
                             </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="margin: 0px -86px 0;">
                                    <li role="presentation">
                                    <a role="menuitem" tabindex="-1" >
                                    <asp:LinkButton ID="lnkAddNewFolder" runat="server" 
                                    OnClientClick="fopenpopup()" OnClick="lnkAddNewFolder_Click">
                                     New Folder
                                    </asp:LinkButton>
                                    </a>
                                    </li>                                 
                                </ul>
                            </li>
                        </div>
                            
                        <div class="clear:both" style="height: 42px;"></div>
                        <div class="col-lg-12 col-md-12 deleteandsharediv" style="display:none;">
                            <div class="col-md-10">&nbsp;</div>
                            <div class="col-md-2" style="float: right;text-align: right;">   
                                <img src="../images/sharedrive.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="sharedrive" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to Share Document" />                                 
                            <img  class="deletedrive"  src="../images/deletedrive.png"  style="width:45px;height: 30px;cursor:pointer;margin-right:5px" /></div>
                        </div>--%>
                                             
                      
                            
<%--                        <div class="clear:both" style="height: 42px;"></div>
                        <div class="col-lg-12 col-md-12 deleteandsharediv" style="display:none;">
                            <div class="col-md-10">&nbsp;</div>
                            <div class="col-md-2" style="float: right;text-align: right;">                                  
                            
                        </div>
                            </div>--%>
                        <div class="row">
                            <asp:GridView runat="server" ID="grdFolderDetail" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnRowCommand="grdFolderDetail_RowCommand"
                                PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID">
                                <Columns >
                                
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr"  Visible="false">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left" >
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                            <asp:LinkButton ID="LinkButton1" runat="server" ToolTip="go to sub folder" data-toggle="tooltip"
                                            CssClass="foldertogo" CommandName="Goto_Subfolder"
                                            CommandArgument='<%# Eval("ID") %>'>
                                            <img src='<%# ResolveUrl("/Images/Folder Icon1.png")%>' class="folder-size" alt="Edit Details" title="Edit Details" />
                                            </asp:LinkButton> <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owner" >
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">

                                            <asp:TextBox ID="tbxCreatedByValue" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("Createdby") %>'></asp:TextBox>
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" CssClass="ownerdoc" Text='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>' ToolTip='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>'></asp:Label>
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" style="display:none;" CssClass="ownerdocaccesss" Text='<%# UserAccess(Convert.ToString(Eval("ID"))) %>' ToolTip='<%# UserAccess(Convert.ToString(Eval("ID"))) %>'></asp:Label>                                                                                          
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Modified" >
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("UpdatedOn") %>' ToolTip='<%# Eval("UpdatedOn") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="1%">
                                    <ItemTemplate>  
                                        <asp:ImageButton ID="LnkShare" runat="server" CssClass="sharef"  Style="float: left; padding: 0px 2px 0px 0px;display:none;" ImageUrl="~/Images/reset_password_new.png" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>' CommandName="ShareFile" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Share File"></asp:ImageButton>                                    
                                        <asp:ImageButton ID="LnkSubShare" runat="server" CssClass="subsharef"  Style="float: left; padding: 0px 2px 0px 0px;display:none;" ImageUrl="~/Images/reset_password_new.png" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>' CommandName="SubShareFile" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Sub Share File"></asp:ImageButton>
                                        <asp:ImageButton ID="lnkDeleteUpload" runat="server"
                                             Visible="true" ImageUrl="~/Images/delete_icon_new.png" style="display:none;" CssClass="deletef"
                                            data-toggle="tooltip" data-placement="bottom" CommandName="DeleteDoc"
                                            ToolTip="Click to Delete Record"
                                            CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'></asp:ImageButton>                                                                             
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0" style="margin-right: 10px;">
                                    <p style="color: #999; margin-top: 5px; margin-right: 5px;">Show </p>
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5"  />
                                    <asp:ListItem Text="10" Selected="True"/>
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px;"></div>
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-3"></div>
                                <div class="col-md-3"></div>
                                 <div class="col-md-2 colpadding0 table-paging-text" style="width: 25%;">
                                    <p>
                                        Page                                       
                                    </p>
                                </div>
                                <div class="col-md-3 colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                    class="form-control m-bot15" Width="120%" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                        </div>

                        <div class=" col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                             <div class="col-md-6 colpadding0" style="float: right">
                                <div class="table-paging" style="margin-bottom: 10px;">                                    
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-5 colpadding0">
                                    <div class="table-Selecteddownload">
                                        <div class="table-Selecteddownload-text">
                                            <p>
                                                <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                               </div>
                            </div>
                        </div>

                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>




    <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 142px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        New Folder</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                 <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCTemSec">
                                    <asp:Label runat="server" ID="successmsgaCTemSec"></asp:Label>
                                </div>
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup" />
                                    <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:TextBox runat="server" ID="txtFolderName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="Foldermsg" ErrorMessage="Required Name"
                                        ControlToValidate="txtFolderName" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:Button Text="Create" runat="server" ID="btnCreateFolder" CssClass="btn btn-search"
                                        OnClick="btnCreate_Click" ValidationGroup="vsFolderDocumentValidationGroup"></asp:Button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

      <div class="modal fade" id="divOpenPermissionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 62px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Share With Others
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:ValidationSummary ID="vsPermission" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="vsPermissionValidationGroup" />
                            <asp:CustomValidator ID="vsPermissionSet" runat="server" EnableClientScript="False"
                                ValidationGroup="vsPermissionValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group required col-md-12" style="display: block;">
                            <label for="ddlPermission1" class="color-lable">Permission</label>
                            <asp:DropDownListChosen runat="server" ID="ddlPermission1" DataPlaceHolder="Select Permission"
                                AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">                                
                                    <asp:ListItem Text="View" Value="4"></asp:ListItem>
                                <asp:ListItem Text="View & Download" Value="1"></asp:ListItem>
                                <%--<asp:ListItem Text="Write" Value="2"></asp:ListItem>--%>
                                <asp:ListItem Text="Full Control" Value="3"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group required col-md-12">
                            <label for="lstBoxUser" class="color-lable">People</label>
                            <asp:ListBox ID="lstBoxUser" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:Button Text="Done" ID="btnPermission" runat="server" OnClick="btnPermission_Click" CssClass="btn btn-search"></asp:Button>
                        </div>
                    </div>
                     <div class="row">
                        <div class="form-group required col-md-12"  style="overflow-x: auto;max-height: 212px;">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:Repeater ID="myRepeater" runat="server" OnItemCommand="myRepeater_ItemCommand">
                                        <HeaderTemplate>
                                            <div style="font-weight: bold; color: black">Shared with Users</div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="form-group required col-md-12" style="padding-left: 0px;">
                                                    <div class="form-group required col-md-4" style="padding-left: 0px;">
                                                        <asp:Label ID="myLabel" runat="server" Style="color: black; float: left" Text='<%# Eval("UserName") %>' />
                                                    </div>
                                                    <div class="form-group required col-md-1" style="padding-left: 0px;">
                                                        <asp:ImageButton ID="LnkDeletShare" runat="server" CommandName="RemoveShare" CommandArgument='<%# Eval("UserPermissionFileId") +","+ Eval("UserId") +","+ Eval("FileType") %>' Style="float: left; padding: 0px 2px 0px 0px;" ImageUrl="~/Images/delete_icon_new.png" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to UnShare File"></asp:ImageButton>                                                         
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="myRepeater" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        var checkbuttonclick = true;
        $('.clsROWgrid').click(function () {
            setInterval(function () { }, 100);
            debugger;
            var owner = $(this).find('.ownerdoc');
            var owneraccsess = $(this).find('.ownerdocaccesss');
            if (checkbuttonclick==true && ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)')) {
                var fclick = $(this).find('.foldertogo');
                if ($(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                    document.getElementById($(fclick).attr('id')).click();
                }
            } else {
                $('.deletedrive').hide();
                $('.sharedrive').hide();
                $('.subsharedrive').hide();
                if ($(owner).text() == 'me') {
                    $('.deletedrive').show();
                    $('.sharedrive').show();
                    $('.subsharedrive').hide();
                }
                else
                {
                    if ($(owneraccsess).text() == 'Access') {
                        $('.subsharedrive').show();
                    }
                    else {
                        $('.subsharedrive').hide();
                    }
                }
                $('.clsROWgrid').css('background-color', 'white');
                $('.clsROWgrid').removeClass('selectedDocs');
                $('.deleteandsharediv').show();
                $(this).css('background-color', '#f8f8f8');
                $(this).addClass('selectedDocs');
                checkbuttonclick = true;
            }
        });
        $('.deletedrive').on('click', function (event) {            
            checkbuttonclick = false;
            var deletedrive = $('.selectedDocs').find('.deletef');
            var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
            if (r == true) {
                document.getElementById($(deletedrive).attr('id')).click();
            }
            event.stopPropagation();
        });
        $('.sharedrive').on('click', function (event) {
            checkbuttonclick = false;
            var shareddrive = $('.selectedDocs').find('.sharef');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });
        $('.subsharedrive').on('click', function (event) {
            checkbuttonclick = false;
            var shareddrive = $('.selectedDocs').find('.subsharef');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                $('.clsROWgrid').click(function () {                   
                    var owner = $(this).find('.ownerdoc');
                    var owneraccsess = $(this).find('.ownerdocaccesss');
                    if ( checkbuttonclick == true && ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)') ){
                        var fclick = $(this).find('.foldertogo');
                        if ($(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                            document.getElementById($(fclick).attr('id')).click();
                        }
                    } else {
                        $('.deletedrive').hide();
                        $('.sharedrive').hide();
                        $('.subsharedrive').hide();
                        if ($(owner).text() == 'me') {
                            $('.deletedrive').show();
                            $('.sharedrive').show();
                        }
                        else {
                            if ($(owneraccsess).text() == 'Access') {
                                $('.subsharedrive').show();
                            }
                            else {
                                $('.subsharedrive').hide();
                            }
                        }
                        $('.clsROWgrid').css('background-color', 'white');
                        $('.clsROWgrid').removeClass('selectedDocs');
                        $('.deleteandsharediv').show();
                        $(this).css('background-color', '#f8f8f8');
                        $(this).addClass('selectedDocs');
                        checkbuttonclick = true;
                    }
                });
                $('.deletedrive').on('click', function (event) {                 
                    checkbuttonclick = false;
                    var deletedrive = $('.selectedDocs').find('.deletef');
                    var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
                    if (r == true) {
                        document.getElementById($(deletedrive).attr('id')).click();
                    }
                    event.stopPropagation();
                });
                $('.sharedrive').on('click', function (event) {
                    checkbuttonclick = false;
                    var shareddrive = $('.selectedDocs').find('.sharef');
                    document.getElementById($(shareddrive).attr('id')).click();
                    event.stopPropagation();
                });
                $('.subsharedrive').on('click', function (event) {
                    checkbuttonclick = false;
                    var shareddrive = $('.selectedDocs').find('.subsharef');
                    document.getElementById($(shareddrive).attr('id')).click();
                    event.stopPropagation();
                });
            });
        });

    </script>
</asp:Content>




