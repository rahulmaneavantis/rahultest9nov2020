﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ReassignReviewerToPerformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ReassignReviewerToPerformer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;
            var chkheaderid = headerchk.id.split("_");

            var gvcheck = null;
            if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");

            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function SelectheaderICheckboxes(Iheaderchk) {
            var rolecolumn;
            var chkIheaderid = Iheaderchk.id.split("_");

            var Igvcheck = null;
            if (chkIheaderid[1] == "grdInternalComplianceInstances")
                Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");
            else
                Igvcheck = document.getElementById("<%=grdInternalComplianceInstances.ClientID %>");

            var i;

            if (Iheaderchk.checked) {
                for (i = 0; i < Igvcheck.rows.length; i++) {
                    Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                }
            }

            else {
                for (i = 0; i < Igvcheck.rows.length; i++) {
                    Igvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = Iheaderchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = null;
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            if (chkheaderid[1] == "grdComplianceInstances")
                gvcheck = document.getElementById("<%=grdComplianceInstances.ClientID %>");

             var rowcount = gvcheck.rows.length;

             for (i = 1; i < gvcheck.rows.length - 1; i++) {
                 if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                     count++;
                 }
             }

             if (count == gvcheck.rows.length - 2) {
                 headerchk.checked = true;
             }
             else {
                 headerchk.checked = false;
             }
         }

         function initializeJQueryUI(textBoxID, divID) {
             $("#" + textBoxID).unbind('click');

             $("#" + textBoxID).click(function () {
                 $("#" + divID).toggle("blind", null, 500, function () { });
             });
         }

         function Confirm() {
             var confirm_value = document.createElement("INPUT");
             confirm_value.type = "hidden";
             confirm_value.name = "confirm_value";
             var oldUser = $("#<%=litCurrentUser.ClientID%>").html();
            var newUser = $("#<%=ddlNewUsers.ClientID%> option:selected").text();
            if (newUser != "< Select user >") {
                if (confirm("Are you sure you want reassign selected compliances to " + newUser + "?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);
        }

    </script>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div runat="server" id="divModifyAssignment">
        <asp:UpdatePanel ID="upModifyAssignment" runat="server" UpdateMode="Conditional" OnLoad="upModifyAssignment_Load">
            <ContentTemplate>

                <div class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12 ">
                           <section class="panel">

                            <div style="margin-bottom: 4px; float: left; width: 100%">
                                <asp:ValidationSummary runat="server" CssClass="vdsummary"
                                    ValidationGroup="ModifyAsignmentValidationGroup" Visible="false" />
                                <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                    ValidationGroup="ModifyAsignmentValidationGroup" Display="None" />

                                <asp:Label ID="litCurrentUser" runat="server" />
                            </div>
                            <div class="clearfix"></div>
                            <div style="float: right; margin-top: -20px;">
                                <asp:LinkButton runat="server" CssClass="btn btn-search" OnClick="linkbutton_onclick">Back</asp:LinkButton>

                            </div>
                            <div class="col-md-2 colpadding0 entrycount">
                                <div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>

                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>

                                <div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px; margin-left: 5px;">Entries</p>
                                </div>
                            </div>

                            <div class="col-md-9 colpadding0" style="text-align: right; float: right">
                                <div style="float: left; margin-right: 2%;">
                                    <asp:DropDownList runat="server" ID="ddlDocType" class="form-control m-bot15 search-select" Style="width: 105px;"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="Statutory" Value="-1" />
                                        <asp:ListItem Text="Internal" Value="0" />
                                    </asp:DropDownList>
                                </div>

                                <div style="float: left; margin-right: 2%;">
                                    <asp:DropDownList runat="server" ID="ddlComType" class="form-control m-bot15 search-select" Style="width: 105px;">
                                        <asp:ListItem Text="Type" Value="-1" />
                                        <asp:ListItem Text="Event" Value="0" />
                                        <asp:ListItem Text="CheckList" Value="1" />
                                    </asp:DropDownList>
                                </div>

                                <div style="float: left; margin-right: 2%;">
                                    <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 200px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />

                                    <div style="margin-left: 1px; position: absolute; z-index: 10; display: inherit;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="325px" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>

                                <div style="float: left; margin-right: 2%;">
                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" runat="server" Text="Apply" OnClick="btnSearch_Click" />
                                </div>

                                <div style="float: left; margin-right: 2%;">
                                    <asp:TextBox runat="server" placeholder="Type to Filter" class="form-control" Width="220px" ID="txtAssigmnetFilter" MaxLength="50" AutoPostBack="true" OnTextChanged="txtAssigmnetFilter_TextChanged" />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px;"></p>
                            </div>
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px; margin-left: 5px;"></p>
                            </div>
                        </div>

                        <div class="col-md-9 colpadding0" style="float: right; margin-bottom: 10px;">
                            <div style="margin-left: -38px;">
                                <asp:DropDownList runat="server" ID="ddlNewUsers" Style="padding: 0px; margin: 0px; width: 225px;" class="form-control m-bot15"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlNewUsers_SelectedIndexChanged" />
                                <asp:CompareValidator ID="cvNewUsers" ErrorMessage="Select User to Assign Compliance." ForeColor="Red" ControlToValidate="ddlNewUsers" runat="server" ValueToCompare="-1"
                                    Operator="NotEqual" ValidationGroup="ModifyAsignmentValidationGroup" />
                            </div>

                            <div runat="server" id="DivRecordsScrum" style="float: right; color: #999; margin-left: 10px;">

                                <p style="padding-right: 0px !Important;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>

                            </div>
                        </div>

                        <div runat="server" id="div3" style="margin-bottom: 7px;">
                            <asp:Panel ID="Panel1" Width="100%" runat="server">
                                <asp:GridView runat="server" ID="grdComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                                    GridLines="None" CssClass="table" AllowSorting="true" Width="100%"
                                    OnRowCreated="grdComplianceInstances_RowCreated" OnPageIndexChanging="grdComplianceInstances_OnPageIndexChanging"
                                    DataKeyNames="ComplianceInstanceID" OnSorting="grdComplianceInstances_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="UserID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkCompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true" />

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCompliances" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompliances_CheckedChanged" />                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="Branch" HeaderText="Location" />

                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                    <asp:Label ID="lblDescription" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                    <asp:Label ID="lblRole" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="User">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                    <asp:Label ID="lbluser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("user") %>' ToolTip='<%# Eval("user") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="Label">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLabel" runat="server" Text='<%# Eval("Label") %>' ToolTip='<%# Eval("Label") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Compliances Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <asp:GridView runat="server" ID="grdInternalComplianceInstances" AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                                    GridLines="none" AllowSorting="true" OnRowCreated="grdInternalComplianceInstances_RowCreated"
                                    Width="100%" CssClass="table" OnPageIndexChanging="grdInternalComplianceInstances_OnPageIndexChanging"
                                    DataKeyNames="InternalComplianceInstanceID" OnSorting="grdInternalComplianceInstances_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="UserID" SortExpression="UserID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIUserID" runat="server" Text='<%# Eval("UserID") %>' ToolTip='<%# Eval("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkICompliancesHeader" runat="server" OnCheckedChanged="chkCompliancesHeader_CheckedChanged" AutoPostBack="true" />

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkICompliances" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompliances_CheckedChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="Branch" HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-Height="20px" />

                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Role">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                    <asp:Label ID="lblIRole" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Role") %>' ToolTip='<%# Eval("Role") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="User">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px">
                                                    <asp:Label ID="lblIuser" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("user") %>' ToolTip='<%# Eval("user") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="Label">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLabel" runat="server" Text='<%# Eval("Label") %>' ToolTip='<%# Eval("Label") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Compliances Found.
                                    </EmptyDataTemplate>

                                </asp:GridView>
                            </asp:Panel>
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 15px;"></asp:Label>
                                        </p>
                                    </div>
                                    <asp:Button Text="Save" ID="btnSaveAssignment" class="btn btn-search" runat="server" OnClientClick="Confirm();" OnClick="btnSaveAssignment_Click" />

                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />


                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />

                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>

                        <div id="divSelectionType" style="margin-bottom: 7px; margin-left: 142px;" runat="server" visible="false">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <asp:RadioButtonList ID="rbtSelectionType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtSelectionType_SelectedIndexChanged"
                                RepeatDirection="Horizontal" RepeatLayout="Table">
                                <asp:ListItem Text="Statutory" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Internal" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>

                            <div style="float: left; width: 10%; margin-left: 1%;">
                                <asp:CheckBox runat="server" ID="chkEvent" AutoPostBack="true" Style="float: left;" OnCheckedChanged="chkEvent_CheckedChanged" />
                                <label style="width: 100px; display: block; font-size: 13px; color: #333;">
                                    Event Based</label>
                            </div>
                            <div style="float: left; width: 20%; margin-left: 1%;">

                                <asp:CheckBox runat="server" ID="chkCheckList" AutoPostBack="true" Style="float: left;" OnCheckedChanged="chkCheckList_CheckedChanged" />
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    CheckList</label>
                            </div>
                        </div>
                    </div>

                   </section>
                </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
