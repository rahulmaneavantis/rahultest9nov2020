﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class MasterRepositry : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static string RoleFlag;
        protected static string Path;
        protected static int ActID;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                if (!IsPostBack)
                {
                    if (AuthenticationHelper.Role == "EXCT")
                    {
                        RoleFlag = "PRA";
                        var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);
                        if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                        {
                            if ((bool)UserDetails.IsHead)
                            {
                                RoleFlag = "DEPT";
                            }
                        }
                    }
                    else
                    {
                        RoleFlag = AuthenticationHelper.Role;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }           
        }
    }
}