﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class IMPTIntermediateChkList : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDitributor();
            }
        }
        protected void ddlFilterCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "1")
                    {
                        BindLocationFilter(Convert.ToInt32(ddlFilterCustomer.SelectedValue));
                        tbxFilterLocation.Text = "< Select >";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        protected void ddlFilterServiceProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterServiceProvider.SelectedValue))
                {
                    if (ddlFilterServiceProvider.SelectedValue != "1")
                    {
                        BindCustomer(Convert.ToInt32(ddlFilterServiceProvider.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindDitributor()
        {
            try
            {
                var data = GetAllDistributor();
                ddlFilterServiceProvider.DataTextField = "Name";
                ddlFilterServiceProvider.DataValueField = "ID";
                ddlFilterServiceProvider.DataSource = data;
                ddlFilterServiceProvider.DataBind();
                ddlFilterServiceProvider.Items.Insert(0, new ListItem("< Select ServiceProvider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Customer> GetAllDistributor()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.IsDistributor == true
                                 select row);


                if (customers.Count() > 0)
                {
                    if (AuthenticationHelper.Role == "DADMN")
                    {
                        customers = customers.Where(entry => entry.ID == AuthenticationHelper.CustomerID);
                    }
                    customers = customers.OrderBy(row => row.Name);
                }

                return customers.ToList();
            }
        }
        public static List<Customer> GetAllDistributorCustomer(int distributorid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customers = (from row in entities.Customers
                                 where row.IsDeleted == false
                                 && row.ParentID == distributorid
                                 select row);


                if (customers.Count() > 0)
                    customers = customers.OrderBy(row => row.Name);

                return customers.ToList();
            }
        }
        private void BindCustomer(int distributorID)
        {
            try
            {
                var data = GetAllDistributorCustomer(distributorID);
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = data;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindUsers(DropDownList ddlUserList,int customerID, List<long> ids = null)
        {
            try
            {               
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();

                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter(int customerID)
        {
            try
            {
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                int nCustomerBranchID = -1;
                nCustomerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                BindGrid(nCustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //sandesh code start

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {           
                int nCustomerBranchID = -1;
                BindGrid(nCustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void RetrieveNodes(TreeNode node,int customerid)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerid)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i], customerid);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerid)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i], customerid);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }



        //sandesh code end
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindGrid(int CustomerBranchID)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                {
                    if (ddlFilterCustomer.SelectedValue != "1")
                    {
                        string filter = tbxFilter.Text.ToString().Trim();

                        int customerID = -1;
                        customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                        locationList.Clear();

                        for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                        {
                            RetrieveNodes(this.tvFilterLocation.Nodes[i], customerID);
                        }
                        List<SP_TempAssignmentCheckList_Result> dataSource = new List<SP_TempAssignmentCheckList_Result>();
                        if (locationList.Count > 0)
                        {
                            dataSource = Business.ComplianceManagement.GetTempAssignedCheckListDetailslist(customerID, locationList);
                        }
                        else
                        {
                            dataSource = Business.ComplianceManagement.GetTempAssignedCheckListDetails(customerID, CustomerBranchID);
                        }

                        if (!string.IsNullOrEmpty(filter))
                        {
                            if (Business.ComplianceManagement.CheckInt(filter))
                            {
                                int a = Convert.ToInt32(filter.ToUpper());
                                dataSource = dataSource.Where(entry => entry.ComplianceID == a).ToList();
                            }
                            else
                            {
                                dataSource = dataSource.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                            }
                        }

                        grdComplianceRoleMatrix.Visible = true;
                        grdComplianceRoleMatrix.DataSource = dataSource;
                        grdComplianceRoleMatrix.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
       
        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {             
                int nCustomerBranchID = -1;
                BindGrid(nCustomerBranchID);
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue))
                    {
                        if (ddlFilterCustomer.SelectedValue != "1")
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                            DropDownList dp = (DropDownList)e.Row.FindControl("ddlUserList");
                            BindUsers(dp, customerID);
                            dp.SelectedValue = grdComplianceRoleMatrix.DataKeys[e.Row.RowIndex].Values[1].ToString();
                        }
                    }
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
           
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
           
        }

        protected void grdComplianceRoleMatrix_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdComplianceRoleMatrix.EditIndex = e.NewEditIndex;      
            int CustomerBranchID = -1;
            BindGrid(CustomerBranchID);
        }

        protected void grdComplianceRoleMatrix_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdComplianceRoleMatrix.EditIndex = -1;      
            int CustomerBranchID = -1;
            BindGrid(CustomerBranchID);
           
        }

        protected void grdComplianceRoleMatrix_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {           
            int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[e.RowIndex].Values[0].ToString());
            DropDownList ddl = (DropDownList)grdComplianceRoleMatrix.Rows[e.RowIndex].FindControl("ddlUserList");
            Business.ComplianceManagement.UpdateAssignedCheckListUser(TempAssignmentID,Convert.ToInt32(ddl.SelectedValue));
            grdComplianceRoleMatrix.EditIndex = -1;        
            int CustomerBranchID = -1;
            BindGrid(CustomerBranchID);                      
        }

        protected void grdComplianceRoleMatrix_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int TempAssignmentID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[e.RowIndex].Values[0].ToString());
                Business.ComplianceManagement.DeleteTempAssignmentChecklistTable(TempAssignmentID);
                grdComplianceRoleMatrix.EditIndex = -1;          
                int CustomerBranchID = -1;
                BindGrid(CustomerBranchID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;          
            int CustomerBranchID = -1;
            BindGrid(CustomerBranchID);
        }
    }
}