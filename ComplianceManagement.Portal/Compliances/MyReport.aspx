﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MyReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.MyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">  
    

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min1.1.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <style type="text/css">
            .k-grid-content
        {
            min-height:394px !important;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        span.k-icon.k-i-arrow-60-down {
    margin-top: 7px;
}
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

          .myKendoCustomClass {
            z-index:999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #ceced2;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-grouping-header 
        {
           color: #515967;
           font-style: italic;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .k-grouping-header 
        {
            border-right: 1px solid;
            border-left: 1px solid;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        /*.k-grid,
        .k-grid-header-wrap,
        .k-grid-header th,
        .k-grid-content > table > tbody > tr > td {
            border: none;
        }*/
        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }
        /*.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap {
            margin-top: 9px;
        }*/
        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }
    </style>
    <title></title>


    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>


    <script type="text/x-kendo-template" id="template"> 
        <div class=row style="padding-bottom: 4px;">
            <div>   
                    <input id="dropdownlistUserRole" data-placeholder="Role"  style="width:242px;">            
            </div>
        </div>
    <div class=row style="padding-bottom: 4px;">
            <div class="toolbar">               
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width:196px;">            
                <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">                  
                <input id="dropdownlistRisk" data-placeholder="Risk"  style="width: 105px;">                
                <input id="dropdownlistStatus" data-placeholder="Status">
                <input id="dropdownlistTypePastdata" data-placeholder="Status" style="width: 156px;">                               
                <button id="export" onclick="exportReport(event)" data-toggle="tooltip" title="Export to Excel" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>        
                <button id="AdavanceSearch" style="height: 23px;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
             </div>
    </div> 
           
         <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2" style="width: 13.6%;;">
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 10px;">
                            <div id="dvdropdownEventName" style="display:none;"><input id="dropdownEventName" data-placeholder="Event Name" style="width:175px;"></div>          
                        </div>
                        <div class="col-md-2" style="width: 15%;padding-left: 0px;">
                           <div id="dvdropdownEventNature" style="display:none;"><input id="dropdownEventNature" data-placeholder="Event Nature"></div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%;padding-left: 22px;">                             
                             <button id="ClearfilterMain" style="float: right; margin-left: 1%;display:none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>                                         
                             <button id="dvbtndownloadDocumentMain" style="float: right;display:none;" onclick="selectedDocumentMain(event)">Download</button>                                                                         
                        </div>

                    </div>
                </div>
                             
       
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtersstoryboard">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filtertype">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterrisk">&nbsp;</div>
            <div class=row style="padding-bottom: 4px;font-size: 12px;display:none;font-weight:bold;" Id="filterstatus">&nbsp;</div>

    </script>


    <script type="text/javascript">

    
         
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

           
           <%if (Falg == "AUD")%>
           <%{%>

            $('#Startdatepicker').val('<% =SDate%>');
            $('#Lastdatepicker').val('<% =LDate%>');

            $("#Startdatepicker").attr("readonly", true);
            $("#Lastdatepicker").attr("readonly", true);
            $("#dropdownPastData").attr("readonly", true);
            $("#dropdownFY").attr("readonly", true);

            $('#dropdownPastData').val('All');
            $('#dropdownlistTypePastdata').val('All');
           <%}%>


            
            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "95%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                   //"Maximize",
                    "Close"
                ],
                close: onClose
            });

            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });
            function onChange() {

                 if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                    $("#dropdownPastData").data("kendoDropDownList").select(4);
                }

                //$('#filterStartDate').css('display', 'none');
                //$('#Clearfilter').css('display', 'none');
                //$('#filterStartDate').html('');
                //if (kendo.toString(this.value(), 'd') != null) {
                //    $('#filterStartDate').css('display', 'block');
                //    $('#Clearfilter').css('display', 'block');
                //    $('#filterStartDate').append('Start Date:&nbsp;');
                //    $('#filterStartDate').append('<span class="k-button" style="background-color:#1fd9e1; height: 20px;Color:white;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                    
                //}
                //DateFilterCustom();
            }

            function DateFilterCustom() {

                $('input[id=chkAll]').prop('checked', false);                
                $('#dvbtndownloadDocument').css('display', 'none');
                $("#dropdownPastData").data("kendoDropDownList").select(4);
                var setStartDate = $("#Startdatepicker").val();
                var setEndDate = $("#Lastdatepicker").val();
                if (setStartDate != null || setEndDate != null) {
                    //DataBindDaynamicKendoGrid();
                }      
                if (setStartDate != null) {
                    $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);                  
                }
                if (setEndDate != null) {
                    $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);                  
                }  
                FilterAllAdvancedSearch();  

            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });
            function onChange1() {
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                    $("#dropdownPastData").data("kendoDropDownList").select(4);
                }               
            }



            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var grid1 = $("#grid1").kendoGrid({
              
                   dataSource: {

                    transport: {
                        <%-- read:"<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0"
                        --%>
                        read: {
                            url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =RoleKey%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =RoleKey%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0"
                        
                    },
                     schema: {
                         data: function (response) {
                             return response[0].StatustoryEB;
                         },
                         total: function (response) {
                             return response[0].StatustoryEB.length;
                         }
                        },
                    pageSize: 10,
                 
                },

                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                change: onChange,
                columns: [
                 
                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Sections", title: "Section" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "43.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                     
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                           
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                    
                    {
                        field: "Status", title: 'Status',
                      
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    //{
                    //    hidden: true, field: "", title: "Uploaded Date" //type: "date"                        
                      
                    //},
                    //{
                    //    hidden: true, field: "", title: "Type",  
                       
                    //},
                  
                    //{ hidden: true, field: "", title: "Uploaded By" },
                    //{ hidden: true, field: "", title: "Size" },
                    //{ hidden: true, field: "ActID", title: "" },
                    //{ hidden: true, field: "UserID", title: "UserID" },
                    {
                        field: "PerformerDated", title: 'PerformerDated',
                        type: "date",
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ReviewerDated", title: 'ReviewerDated',
                        type: "date",
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ReportName", title: "Report Name" },
                     {
                        field: "ShortForm", title: 'Short Form',
                        width: "21%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" }
                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });
            function onSorting(arg) {
                settracknew('Detailed report', 'Paging', arg.sort.field, '');

            }

            function onPaging(arg) {
                settracknew('Detailed report', 'Paging', arg.page, '');	
            }

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });
           
            $("#grid1").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

              $("#grid1").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


               $("#grid1").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


               $("#grid1").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");


               $("#grid1").kendoTooltip({
                filter: "td:nth-child(8)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

             $("#grid1").kendoTooltip({
                filter: "td:nth-child(9)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");



            var grid = $("#grid").kendoGrid({

                dataSource: {

                    transport: {
                        //read:'<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0'
                         read: {
                            url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =RoleKey%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =RoleKey%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0"                       
                    },
                     schema: {
                            data: function (response) {
                                return response[0].StatustoryEB;
                         },
                           total: function (response) {
                            return response[0].StatustoryEB.length;
                        }
                        },
                    pageSize: 10                   
                },
                  excel:{
                      allPages: true,                   
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                columns: [
                   
                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },
                    { hidden: true, field: "Sections", title: "Section" },
                    {
                        field: "Branch", title: 'Location',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                    
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                    
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            extra: false,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {
                        field: "CloseDate", title: "Close Date",
                        type: "date",
                         hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(CloseDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(CloseDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                     },
                     {
                         field: "PerformerDated", title: 'PerformerDated',                        
                         type: "date",
                         hidden: true,
                         template: "#= kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                         //template: "#= kendo.toString(kendo.parseDate(PerformerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                         filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                    {
                        field: "ReviewerDated", title: 'ReviewerDated',                        
                        type: "date",
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(ReviewerDated, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        width: "34.7%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ReportName", title: "Report Name" },
                    {
                        command: [
                            //{ name: "edit", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit" },
                            //{ name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,   width: "7%;",// width: 150,
                    }
                ]
            });
            function onSorting(arg) {
                settracknew('Detailed report', 'Paging', arg.sort.field, '');

            }
            function onPaging(arg) {
                settracknew('Detailed report', 'Paging', arg.page, '');
            }
          
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

              $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
              $("#grid").kendoTooltip({
                filter: "td:nth-child(4)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
            $("#grid").kendoTooltip({
                filter: "td:nth-child(5)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(6)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(7)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" },                   
                     <%if (CustId == 90)%>
                     <%{%>
                        { text: "All", value: "5" }
                     <%}%>
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Detailed report', 'Filtering', 'Complaince Type', '');	

                }
            });



            function DataBindDaynamicKendoGriddMain() {
               
                $('#dvdropdownEventNature').css('display', 'none');
                $('#dvdropdownEventName').css('display', 'none');
                $("#dropdowntree").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'none');
                $("#grid").data('kendoGrid').dataSource.data([]);
               
                if ($("#dropdownlistComplianceType").val() == -1) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=-1&FY=0' 
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=-1&FY=0',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=-1&FY=0' 
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatustoryEB;
                            },
                             total: function (response) {
                            return response[0].StatustoryEB.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }                                    
                                }
                            }
                        },
                        pageSize: 10                       
                    });
                    var grid = $('#grid').data("kendoGrid");
                //    dataSource.read();
                    grid.setDataSource(dataSource);
                }
               if ($("#dropdownlistComplianceType").val() == 1) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=1&FY=0'                            
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=1&FY=0',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=1&FY=0'                            
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatustoryEB;
                            },
                             total: function (response) {
                            return response[0].StatustoryEB.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                     pageSize: 10,                       
                    });
                    var grid = $('#grid').data("kendoGrid");
               //     dataSource.read();
                    grid.setDataSource(dataSource);
                }
                  if ($("#dropdownlistComplianceType").val() == 0) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=0&FY=0'
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=0&FY=0',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=0&FY=0'
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Internal;
                            },
                             total: function (response) {
                            return response[0].Internal.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid').data("kendoGrid");
                  //  dataSource.read();
                    grid.setDataSource(dataSource);
                }

              if ($("#dropdownlistComplianceType").val() == 3) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                              //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=3&FY=0'                            
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=3&FY=0',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=3&FY=0'                            
                        },
                        schema: {
                            data: function (response) {
                                return response[0].InternalChecklist;
                            },
                             total: function (response) {
                            return response[0].InternalChecklist.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                  if ($("#dropdownlistComplianceType").val() == 2) {
                 var dataSource = new kendo.data.DataSource({
                        transport: {
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=2&FY=0'
                            read: {
                                url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=2&FY=0',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=2&FY=0'
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatChecklist;
                            },
                             total: function (response) {
                            return response[0].StatChecklist.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                 if ($("#dropdownlistComplianceType").val() == 4) {
                 var dataSource = new kendo.data.DataSource({
                        transport: {
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=4&FY=0'
                            read: {
                                url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=4&FY=0',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=4&FY=0'
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatChecklist;
                            },
                             total: function (response) {
                            return response[0].StatChecklist.length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                 if ($("#dropdownlistComplianceType").val() == 5) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=5&FY=0'                            
                         //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=5&FY=0'                            
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole").val() + '&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=5&FY=0',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                        },
                        schema: {
                            data: function (response) {
                              
                                var StatustoryEB1 = response[0].StatustoryEB;
                                var Internal1 = response[0].Internal;
                                var InternalChecklist1 = response[0].InternalChecklist;
                                var StatChecklist1 = response[0].StatChecklist;
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1);
                            },
                             total: function (response) {
                            
                                var StatustoryEB1 = response[0].StatustoryEB;
                                var Internal1 = response[0].Internal;
                                var InternalChecklist1 = response[0].InternalChecklist;
                                var StatChecklist1 = response[0].StatChecklist;
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1).length;
                            },
                                 model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                     pageSize: 10,                       
                    });
                    var grid = $('#grid').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }

                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0);
                if ($("#dropdownlistComplianceType").val() == 1 || $("#dropdownlistComplianceType").val() == 4) {
                    $('#dvdropdownEventNature').css('display', 'block');
                    $('#dvdropdownEventName').css('display', 'block');

                    $("#grid").data("kendoGrid").showColumn(4);
                    $("#grid").data("kendoGrid").showColumn(5);
                    $("#grid").data("kendoGrid").hideColumn(2);
                    $("#grid").data("kendoGrid").hideColumn(7);
                }
                else {
                    $("#grid").data("kendoGrid").showColumn(2);
                    $("#grid").data("kendoGrid").showColumn(7);
                  //  $("#grid").data("kendoGrid").hideColumn(4);
                    $("#grid").data("kendoGrid").hideColumn(5);
                }


                  if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3)//Internal and Internal Checklist
                {                                      
                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                          severFiltering: true,
                          transport: {
                              read: {
                                  url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                                  dataType: "json",
                                  beforeSend: function (request) {
                                      request.setRequestHeader('Authorization', '<% =Authorization%>');
                                  },
                              }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
                }
                else
                {
                   var dataSource12 = new kendo.data.HierarchicalDataSource({
                          severFiltering: true,
                          transport: {
                              read: {
                                  url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                                  dataType: "json",
                                  beforeSend: function (request) {
                                      request.setRequestHeader('Authorization', '<% =Authorization%>');
                                  },
                              }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
                }
            }

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Detailed report', 'Filtering', 'Risk', '');	
                    FilterAllMain();
                    //var filter = { logic: "or", filters: [] };
                    ////  values is an array containing values to be searched
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                    //var dataSource = $("#grid").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAllMain]').prop('checked', false);  
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                     { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Status', '');
                    FilterAllMain();
                    //var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    //add after u get column for filter in API
                    //    filter.filters.push({
                    //        field: "Status", operator: "eq", value: v
                    //    });
                    //});
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
                    //var dataSource = $("#grid").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAllMain]').prop('checked', false);  
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                       { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" },
                    { text: "Not Applicable", value: "Not Applicable" },
                    { text: "In Progress", value: "In Progress" }
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Period', '');
                    //DataBindDaynamicKendoGrid();
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });


            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Detailed report', 'Filtering', 'Period', '');
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
            var evalEventName = 0;

            if ($("#dropdownEventName").val() != '') {
                evalEventName = $("#dropdownEventName").val()
            }
            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    //e.preventDefault();
                    settracknew('Detailed report', 'Filtering', 'Event', '');
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName").val() != '') {
                            evalEventName = $("#dropdownEventName").val()
                        }

                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

         
            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            var evalEventName1 = 0;

            if ($("#dropdownEventName1").val() != '') {
                evalEventName1 = $("#dropdownEventName1").val()
            }
            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName1").val() != '') {
                            evalEventName1 = $("#dropdownEventName1").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });


            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                    }
                }
            });

             $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    settracknew('Detailed report', 'Filtering', 'Role', '');
                },
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                     { text: "Performer", value: "PRA" },
                    <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "REV" },
                    <%}%>
                      <%if (ApproverFlagID  == 1)%>
                    <%{%>
                    { text: "Approver", value: "APPR" },
                    <%}%>
                      <%if (ManagmentFlagID == 1)%>
                    <%{%>
                      { text: "Managment", value: "MGMT" },
                    <%}%>
                     <%if (DepartmentFlagID == 1)%>
                    <%{%>
                      { text: "Department", value: "DEPT" }
                    <%}%>
                ]
            });
            $("#dropdownlistUserRole1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Role', '');
                    //DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                     { text: "Performer", value: "PRA" },
                    <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "REV" },
                    <%}%>
                      <%if (ApproverFlagID  == 1)%>
                    <%{%>
                    { text: "Approver", value: "APPR" },
                    <%}%>
                      <%if (ManagmentFlagID == 1)%>
                    <%{%>
                      { text: "Managment", value: "MGMT" },
                    <%}%>
                      <%if (DepartmentFlagID == 1)%>
                    <%{%>
                      { text: "Department", value: "DEPT" }
                    <%}%>
                ]
            });
           
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: { 
                    checkChildren: true
                },             
                checkAll: true,                
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",              
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Location', '');
                    FilterAllMain();                    
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')                   
                    $('input[id=chkAllMain]').prop('checked', false);  
                      $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

                 function FilterAllMain() {
              
                //location details
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Status details
                var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                var Statusdetails = [];
                $.each(list2, function (i, v) {
                    Statusdetails.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                //risk Details
                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push({
                        field: "Risk", operator: "eq", value: parseInt(v)
                    });
                });


                var dataSource = $("#grid").data("kendoGrid").dataSource;

                if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: locationsdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Riskdetails
                            }
                        ]
                    });
                }

                else {
                      $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }


            function FilterAllAdvancedSearch() {
               
                 //location details
                var locationsdetails = [];                
                if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
                    locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
                }

                //Status details
                var Statusdetails = [];
                if ($("#dropdownlistStatus1").data("kendoDropDownTree") != undefined) {
                    Statusdetails = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                }
                
                //risk Details
                var Riskdetails = [];
                if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
                    Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                }

                //user Details 
                var userdetails = [];
                  <%if (RoleFlag == 1)%>
                   <%{%>
                    if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                        userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
                    }
                <%}%>


                //datefilter
                var datedetails = [];
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                
                var finalSelectedfilter = { logic: "and", filters: [] };
              
                if (locationsdetails.length > 0
                    || Statusdetails.length > 0
                    || Riskdetails.length > 0
                    || userdetails.length > 0
                    || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                    || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
                    || datedetails.length > 0)
                {

                    if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                        var SeqFilter = { logic: "or", filters: [] };
                        SeqFilter.filters.push({
                            field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                        });
                        finalSelectedfilter.filters.push(SeqFilter);
                    }

                    if (locationsdetails.length > 0) {
                        var LocationFilter = { logic: "or", filters: [] };

                        $.each(locationsdetails, function (i, v) {
                            LocationFilter.filters.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(LocationFilter);
                    }

                    if (Statusdetails.length > 0) {
                        var StatusFilter = { logic: "or", filters: [] };

                        $.each(Statusdetails, function (i, v) {
                            StatusFilter.filters.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });

                        finalSelectedfilter.filters.push(StatusFilter);
                    }
                    if (Riskdetails.length > 0) {
                        var RiskFilter = { logic: "or", filters: [] };
                        $.each(Riskdetails, function (i, v) {
                            RiskFilter.filters.push({
                                field: "Risk", operator: "eq", value: parseInt(v)
                            });
                        });
                        finalSelectedfilter.filters.push(RiskFilter);
                    }
                    if (userdetails.length > 0) {
                        var UserFilter = { logic: "or", filters: [] };

                        $.each(userdetails, function (i, v) {
                            UserFilter.filters.push({
                                field: "UserID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(UserFilter);
                    }
                    if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                        var ActFilter = { logic: "or", filters: [] };
                        ActFilter.filters.push({
                            field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                        });
                        finalSelectedfilter.filters.push(ActFilter);
                    }
                    if (datedetails.length > 0) {
                        var DateFilter = { logic: "or", filters: [] };

                        if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                            DateFilter.filters.push({
                                field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                            DateFilter.filters.push({
                                field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                            });
                        }
                        finalSelectedfilter.filters.push(DateFilter);
                    }
                    if (finalSelectedfilter.filters.length > 0) {
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(finalSelectedfilter);
                    }
                    else {
                        $("#grid1").data("kendoGrid").dataSource.filter({});
                    }

                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
               
            }



            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
               checkboxes: { 
                    checkChildren: true
                },
                //checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Location', '');
                    FilterAllAdvancedSearch();
                    //var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);   
                       $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });




            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterAllAdvancedSearch();
                    settracknew('Detailed report', 'Filtering', 'Risk', '');
                    //var filter = { logic: "or", filters: [] };
                    ////  values is an array containing values to be searched
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
                    //CheckFilterClearorNot();
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);  
                       $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                     if ($("#dropdownFY").val() != 0) {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                    }
                    //DataBindDaynamicKendoGrid();                    
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },                    
                    { text: "2020-2021", value: "2020-2021" },
		    { text: "2019-2020", value: "2019-2020" },
		    { text: "2018-2019", value: "2018-2019" },
		    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }  
                ]
            });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    settracknew('Detailed report', 'Filtering', 'User', '');
                    FilterAllAdvancedSearch();
                    //var filter = { logic: "or", filters: [] };
                    ////  values is an array containing values to be searched
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    filter.filters.push({
                    //        field: "UserID", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false); 
                     $('#dvbtndownloadDocument').css('display', 'none');
  
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>"
                    },                   
                }
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Status', '');
                     FilterAllAdvancedSearch();
                    //var filter = { logic: "or", filters: [] };
                    //var values = this.value();
                    //$.each(values, function (i, v) {
                    //    //add after u get column for filter in API
                    //    filter.filters.push({
                    //        field: "Status", operator: "eq", value: v
                    //    });
                    //});
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1')
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false); 
                       $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                       { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" },
                    { text: "Not Applicable", value: "Not Applicable" },
                    { text: "In Progress", value: "In Progress" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({

                autoWidth: true,

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Compliance Type', '');
                    //DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" },
                     <%if (CustId == 90)%>
                     <%{%>
                        { text: "All", value: "5" }
                     <%}%>
                ]
            });
           
            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Sequece', '');
                    FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("900");
                }
            });           

            $("#dropdownType").kendoDropDownTree({
                placeholder: "Select Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "TypeName",
                dataValueField: "TypId",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'type', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownType', 'filterCompType', 'CompType');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindTypesAll?CId=<% =CId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindTypesAll?CId=<% =CId%>"
                    }
                }
            });

            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Select Category",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "CategoryName",
                dataValueField: "CategoryId",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Category', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownCategory', 'filterCategory', 'Category');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindCategoriesAll?CId=<% =CId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindCategoriesAll?CId=<% =CId%>"
                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Act', '');
                     FilterAllAdvancedSearch();
                    //var values = this.value();
                    //if (values != "" && values != null) {
                    //    var filter = { logic: "or", filters: [] };
                    //    filter.filters.push({
                    //        field: "ActID", operator: "eq", value: parseInt(values)
                    //    });
                    //    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //    dataSource.filter(filter);
                    //}
                    //else {
                    //    ClearAllFilter();
                    //}
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("1000");
                }
            });

            $("#dropdownComplianceSubType").kendoDropDownTree({
                placeholder: "Compliance Sub Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Compliance Sub Type', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {
                        //add after u get column for filter in API
                        //filter.filters.push({
                        //    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        //});
                    });
                    fCreateStoryBoard('dropdownComplianceSubType', 'filterCompSubType', 'CompSubType');
                    //var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //dataSource.filter(filter);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>"
                    }
                }
            });

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });

         
            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                settracknew('Detailed report', 'Action', 'Overview', '');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.ReportName);
                return true;
            });

          
            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                settracknew('Detailed report', 'Action', 'Overview', '');
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));

                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID, item.ReportName);
                //$("#divAdvanceSearchModel").data("kendoWindow").close();
                return true;
            });

        });

      
        function ClearAllFilterMain(e) {
           
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');

            $('#dvbtndownloadDocumentMain').css('display', 'none');

            $("#dropdownEventName").data("kendoDropDownList").select(0);
            $("#dropdownEventNature").data("kendoDropDownList").select(0);

            $("#grid").data("kendoGrid").dataSource.filter({});

            $('input[id=chkAllMain]').prop('checked', false); 

            e.preventDefault();
        }

        function ClearAllFilter(e) {

            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);

            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            //$("#dropdownACT").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);

               <%if (RoleFlag == 1)%>
                <%{%>
                     $("#dropdownUser").data("kendoDropDownTree").value([]);
                <%}%>

           

            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#Clearfilter').css('display', 'none');

            $('#dvbtndownloadDocument').css('display', 'none');

            $("#grid1").data("kendoGrid").dataSource.filter({});
            
            
            $('input[id=chkAll]').prop('checked', false);   

            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            //for rebind if any pending filter is present (ADV Grid)
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            //fCreateStoryBoard('dropdownACT', 'filterAct', 'Act');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownFY').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownPastData').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownlistComplianceType1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#Clearfilter').css('display', 'none');
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }

        function DataBindDaynamicKendoGrid() {
           
                $("#grid1").data('kendoGrid').dataSource.data([]);

                if ($("#dropdownFY").val() != "0")
                {
                     $("#dropdownPastData").data("kendoDropDownList").select(4);
                    
                }

                  <%if (RoleFlag == 1)%>
                            <%{%>
                            $("#dropdownUser").data("kendoDropDownTree").value([]);
                            <%}%>
                $('input[id=chkAll]').prop('checked', false);   

                $("#grid1").data("kendoGrid").dataSource.filter({});

                $("#dropdowntree1").data("kendoDropDownTree").value([]);
                // $("#dropdownACT").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
                //$("#dropdownUser").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
                $("#Startdatepicker").data("kendoDatePicker").value(null);
                $("#Lastdatepicker").data("kendoDatePicker").value(null);
                $('#filterStartDate').html('');
                $('#filterLastDate').html('');
                $('#filterStartDate').css('display', 'none');
                $('#filterLastDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#dvbtndownloadDocument').css('display', 'none');

                $("#dvdropdownACT").css('display', 'block');
                if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3)//Internal and Internal Checklist
                {                  
                    $("#dvdropdownACT").css('display', 'none');

                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                          severFiltering: true,
                          transport: {
                              read: {
                                  url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                                  dataType: "json",
                                  beforeSend: function (request) {
                                      request.setRequestHeader('Authorization', '<% =Authorization%>');
                                  },
                              }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
                }
                else
                {
                   var dataSource12 = new kendo.data.HierarchicalDataSource({
                          severFiltering: true,
                          transport: {
                              read: {
                                  url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                                  dataType: "json",
                                  beforeSend: function (request) {
                                      request.setRequestHeader('Authorization', '<% =Authorization%>');
                                  },
                              }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
                }

                if ($("#dropdownlistComplianceType1").val() == 1 || $("#dropdownlistComplianceType1").val() == 4)//event based or event based checklist
                {
                    $('#dvdropdownEventNature1').css('display', 'block');
                    $('#dvdropdownEventName1').css('display', 'block');

                    $("#grid1").data("kendoGrid").showColumn(6);//Event Name
                    $("#grid1").data("kendoGrid").showColumn(7);//Event Nature

                    $("#grid1").data("kendoGrid").hideColumn(4);//Branch
                    $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
                }
                else {
                    $('#dvdropdownEventNature1').css('display', 'none');
                    $('#dvdropdownEventName1').css('display', 'none');

                    $("#grid1").data("kendoGrid").hideColumn(6);//Event Name
                    $("#grid1").data("kendoGrid").hideColumn(7);//Event Nature

                    $("#grid1").data("kendoGrid").showColumn(4);//Branch
                    $("#grid1").data("kendoGrid").showColumn(8);//ForMonth
                }

                  if ($("#dropdownlistComplianceType1").val() == -1) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=-1&FY=' + $("#dropdownFY").val() + '',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=-1&FY='+ $("#dropdownFY").val() +'' 
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=-1&FY='+ $("#dropdownFY").val() +'' 
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatustoryEB;
                            },
                             total: function (response) {
                            return response[0].StatustoryEB.length;
                            },
                            model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }  
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
               if ($("#dropdownlistComplianceType1").val() == 1) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=1&FY=' + $("#dropdownFY").val() + '',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=1&FY='+ $("#dropdownFY").val() +''                          
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=1&FY='+ $("#dropdownFY").val() +''                          
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatustoryEB;
                            },
                             total: function (response) {
                            return response[0].StatustoryEB.length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                  if ($("#dropdownlistComplianceType1").val() == 0) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=0&FY='+ $("#dropdownFY").val() +'',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=0&FY='+ $("#dropdownFY").val() +''
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=0&FY='+ $("#dropdownFY").val() +''
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Internal;
                            },
                             total: function (response) {
                            return response[0].Internal.length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }

              if ($("#dropdownlistComplianceType1").val() == 3) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=3&FY=' + $("#dropdownFY").val() + '',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                              //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=3&FY='+ $("#dropdownFY").val() +''                           
                              //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=3&FY='+ $("#dropdownFY").val() +''                           
                        },
                        schema: {
                            data: function (response) {
                                return response[0].InternalChecklist;
                            },
                             total: function (response) {
                            return response[0].InternalChecklist.length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                  if ($("#dropdownlistComplianceType1").val() == 2) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=2&FY='+ $("#dropdownFY").val() +'',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=2&FY='+ $("#dropdownFY").val() +''
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=2&FY='+ $("#dropdownFY").val() +''
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatChecklist;
                            },
                             total: function (response) {
                            return response[0].StatChecklist.length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                  if ($("#dropdownlistComplianceType1").val() == 4) {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=4&FY='+ $("#dropdownFY").val() +'',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                         //   read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=4&FY='+ $("#dropdownFY").val() +''
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=4&FY='+ $("#dropdownFY").val() +''
                        },
                        schema: {
                            data: function (response) {
                                return response[0].StatChecklist;
                            },
                             total: function (response) {
                            return response[0].StatChecklist.length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }
                if ($("#dropdownlistComplianceType1").val() == 5)
                {
                 var dataSource = new kendo.data.DataSource({
                     transport: {
                         read: {
                             url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=5&FY='+ $("#dropdownFY").val() +'',
                             dataType: "json",
                             beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                          //  read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + $("#dropdownlistUserRole1").val() + '&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=5&FY='+ $("#dropdownFY").val() +''
                            //read: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=5&FY='+ $("#dropdownFY").val() +''
                        },
                        schema: {
                            data: function (response) {
                              
                                var StatustoryEB1 = response[0].StatustoryEB;
                                var Internal1 = response[0].Internal;
                                var InternalChecklist1 = response[0].InternalChecklist;
                                var StatChecklist1 = response[0].StatChecklist;
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1);
                            },
                             total: function (response) {
                               
                                var StatustoryEB1 = response[0].StatustoryEB;
                                var Internal1 = response[0].Internal;
                                var InternalChecklist1 = response[0].InternalChecklist;
                                var StatChecklist1 = response[0].StatChecklist;
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1).length;
                            },
                             model: {                               
                                fields: {                                   
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" },
                                    ReviewerDated: { type: "date" },
                                    PerformerDated: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    //dataSource.read();
                    grid.setDataSource(dataSource);
                }

                <%if (Falg == "AUD")%>
                <%{%>
                    $('#Startdatepicker').val('<% =SDate%>');
                    $('#Lastdatepicker').val('<% =LDate%>');

                    $("#Startdatepicker").attr("readonly", true);
                    $("#Lastdatepicker").attr("readonly", true);
                    $("#dropdownPastData").attr("readonly", true);
                    $("#dropdownFY").attr("readonly", true);

                    $('#dropdownPastData').val('All');
                    $('#dropdownlistTypePastdata').val('All');
            <%}%>

            }

        function ApplyBtnAdvancedFilter(e) {

            var setStartDate = $("#Startdatepicker").val();
            var setEndDate = $("#Lastdatepicker").val();

            DataBindDaynamicKendoGrid();
            var flag = "False";
            if (setStartDate != null && setStartDate != "" && setStartDate != undefined) {
                flag = "True";
                $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
            }
            if (setEndDate != null && setEndDate != "" && setEndDate != undefined) {
                flag = "True";
                $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
            }


            if (flag == "True") {
                var datedetails = [];
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                    });
                }

                var dataSource = $("#grid1").data("kendoGrid").dataSource;

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "and",
                            filters: datedetails
                        }
                    ]
                });
                //FilterAllAdvancedSearch();
            }
            
            $("#dropdownSequence").data("kendoDropDownList").value('');
            if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3)
            {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>'
                    }
                });
                dataSourceSequence.read();
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
            else
            {
                 var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>'
                    }
                });
                dataSourceSequence.read();
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
            }
        }


        function OpenAdvanceSearch(e) {

            var myWindowAdv = $("#divAdvanceSearchModel");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "85%",
                title: "Advanced Search",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    //"Maximize",
                    "Close"
                ],
                close: onClose
            });
            $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }

        function ChangeView() {
            $('#grid1').css('display', 'block');

            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").hideColumn(3);//FileName
           // $("#grid1").data("kendoGrid").hideColumn(4);//comment by gaurav
            $("#grid1").data("kendoGrid").showColumn(4);//Change by Gaurav 

            $("#grid1").data("kendoGrid").showColumn(5);//Branch
            $("#grid1").data("kendoGrid").showColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
            $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
            $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            $("#grid1").data("kendoGrid").showColumn(11);//Status
            $("#grid1").data("kendoGrid").hideColumn(12);//VD
            $("#grid1").data("kendoGrid").hideColumn(13);//type
            $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(16);//Size

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }

        }

        function ChangeListView() {
            $('#grid1').css('display', 'block');
            //  $('#grid2').css('display', 'none');
            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").showColumn(3);//FileName
            $("#grid1").data("kendoGrid").showColumn(4);//V
            $("#grid1").data("kendoGrid").hideColumn(5);//Branch
            $("#grid1").data("kendoGrid").hideColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Scheduleon
            $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
            $("#grid1").data("kendoGrid").hideColumn(9);//Status
            $("#grid1").data("kendoGrid").showColumn(10);//VD
            $("#grid1").data("kendoGrid").hideColumn(11);//type
            $("#grid1").data("kendoGrid").showColumn(12);//Uploaded Date
            $("#grid1").data("kendoGrid").showColumn(13);//Size
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
        }

        function ChangeAuditQView() {
            $('#grid1').css('display', 'none');
            //  $('#grid2').css('display', 'block');            
        }

        function exportReportAdvanced(e) {
           
            e.preventDefault();

            var FlagRole = document.getElementById('FlagDetail').value;
            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var CustomerName = document.getElementById('CustName').value;

            //location details
            var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Status details
            var list2 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
            var Statusdetails = [];
            $.each(list2, function (i, v) {
                Statusdetails.push(v);
            });

            //risk Details
            var Riskdetails = [];
            var list3 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Riskdetails.push(v);
            });

            var RoleFlag = document.getElementById('RoleFlagCHK').value;

            var userdetails = [];
            if (RoleFlag == 1) {
                var list4 = $("#dropdownUser").data("kendoDropDownTree")._values;
                $.each(list4, function (i, v) {
                    userdetails.push(parseInt(v));
                });
            }

            //Act Details
            var Actdetails = [];
            if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                Actdetails.push(parseInt($("#dropdownACT").val()));
            }

            var SequenceDataID = null;
            if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                SequenceDataID = $("#dropdownSequence").val();
            }

            $.ajax({
                type: "GET",
                url: '' + PathName + '//ExportReport/Report',
                data: {
                    UserId: UId, CustomerID: customerId,
                    StatusFlag: $("#dropdownlistComplianceType1").val(), FlagIsApp: $("#dropdownlistUserRole1").val(),
                    MonthId: $("#dropdownPastData").val(), FY: $("#dropdownFY").val(),
                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                    risk: JSON.stringify(Riskdetails), status: JSON.stringify(Statusdetails),
                    userDetail: JSON.stringify(userdetails),
                    actDetail: JSON.stringify(Actdetails),
                    StartDateDetail: $("#Startdatepicker").val(),
                    EndDateDetail: $("#Lastdatepicker").val(),
                    EventName: $("#dropdownEventName1").val(),
                    EventNature: $("#dropdownEventNature1").val(),
                    SequenceID: SequenceDataID
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function exportReport(e) {            
            e.preventDefault();

            var FlagRole = document.getElementById('FlagDetail').value;
            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var CustomerName = document.getElementById('CustName').value;

            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Status details
            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            var Statusdetails = [];
            $.each(list2, function (i, v) {
                Statusdetails.push(v);
            });

            //risk Details
            var Riskdetails = [];
            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Riskdetails.push(v);
            });

            var userdetails = [];
            var Actdetails = [];
            var SequenceDataID = null;
            if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                SequenceDataID = $("#dropdownSequence").val();
            }

            $.ajax({
                type: "GET",
                url: '' + PathName + '//ExportReport/Report',
                data: {
                    UserId: UId, CustomerID: customerId,
                    StatusFlag: $("#dropdownlistComplianceType").val(), FlagIsApp: $("#dropdownlistUserRole").val(),
                    MonthId: $("#dropdownlistTypePastdata").val(), FY: $("#dropdownFY").val(),
                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                    risk: JSON.stringify(Riskdetails), status: JSON.stringify(Statusdetails),
                    userDetail: JSON.stringify(userdetails),
                    actDetail: JSON.stringify(Actdetails),
                    StartDateDetail: '',
                    EndDateDetail: '',
                    EventName: $("#dropdownEventName").val(),
                    EventNature: $("#dropdownEventNature").val(),
                    SequenceID: SequenceDataID
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function OpenOverViewpupMain(scheduledonid, instanceid,ReportName) {
          
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');

            if ($("#dropdownlistComplianceType1").val() == 5) {
                if (ReportName == "Internal Checklist" || ReportName == "Internal") {
                    $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
                else {
                    $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
            }
            if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3) {

                $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }

        function OpenOverViewpup(scheduledonid, instanceid,ReportName) {
         
            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '98%');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
            if ($("#dropdownlistComplianceType").val() == 5) {
                if (ReportName == "Internal Checklist" || ReportName == "Internal") {
                    $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
                else {
                    $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
            }
            if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

                $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");           
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div id="grid" style="border: none;"></div>

        <input id="Path" type="hidden" value="<% =Path%>">
        <input id="CustomerId" type="hidden" value="<% =CustId%>">
        <input id="CustName" type="hidden" value="<% =CustomerName%>">
        <input id="RoleFlagCHK" type="hidden" value="<% =RoleFlag%>">
        <input id="UId" type="hidden" value="<% =UId%>">
        <input id="FlagDetail" type="hidden" value="<% =Falg%>">

        <div>
            <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999">
                  <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;">
                             <input id="dropdownlistUserRole1" data-placeholder="Role"  style="width:242px;">             
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                            <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                        </div>

                        <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                            <input id="Startdatepicker" placeholder="Start Date" CssClass="clsROWgrid" title="startdatepicker" style="width: 100%;"/>
                        </div>
                        <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-left: -9px; margin-top: 7px; margin-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px;">
                            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13%; padding-left: 0px;display:none;">                            
                            <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                        </div>
                        <div class="col-md-4" id="dvdropdownACT" style="width: 29.3%; padding-left: 0px;">
                            <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13.4%; padding-left: 0px;" id="dvdropdownlistStatus1">
                            <%if (RoleFlag == 1)%>
                            <%{%>
                            <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                            <%}%>

                            
                        </div>
                         <div class="col-md-2" style="width: 13%; padding-left: 0px;float: right;">
                            <button id="exportAdvanced" onclick="exportReportAdvanced(event)" data-toggle="tooltip" title="Export to Excel" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width:35px; height:30px; background-color:white;border: none;"></button>        
                              <button id="ApplyBtnAdvanced" style="height: 23px;" onclick="ApplyBtnAdvancedFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                             </div>

                    </div>
                </div>


                <div class="row" style="padding-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" style="width: 20%;padding-left: 1px;">
                               <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>  
                             <input id="dropdownSequence" style="width: 100%;" />
                             <%}%>
                        </div>
                        <div class="col-md-2" style="width: 14.3%;padding-left: 1px;">
                            <div id="dvdropdownEventName1" style="display: none;">
                                <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 196px;" />
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 10%;">
                            <div id="dvdropdownEventNature1" style="display: none;">
                                <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 166px;" />
                            </div>
                        </div>
                        <div class="col-md-2" style="width: 3%;">
                        </div>
                        <div class="col-md-1" style="width: 37%; padding-left: 105px;">
                            <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>
                        </div>

                        <%--   <button id="Clearfilter" style="float: right; margin-left: 1%; display: none;" onclick="ClearAllFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
                            <button id="dvbtndownloadDocument" style="float: right; display: none;" onclick="selectedDocument(event)">Download</button>--%>
                    </div>
                </div>

               

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCompType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCategory">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterAct">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterCompSubType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterLastDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filtersstoryboard1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filtertype1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterrisk1">&nbsp;</div>

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterpstData1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterUser">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterFY">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a;font-weight:bold;" id="filterstatus1">&nbsp;</div>


                <div id="grid1"></div>                
            </div>
            <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            fhead('My Reports / Detailed Report');
            setactivemenu('Myreport');
            fmaters1();
            $("#dropdownlistUserRole").data("kendoDropDownList").value('<% =RoleKey%>');      
            $("#dropdownlistUserRole1").data("kendoDropDownList").value('<% =RoleKey%>');   

            //setactivemenu('ComplianceDocumentList');
            //fmaters()
        });

    </script>
</asp:Content>

