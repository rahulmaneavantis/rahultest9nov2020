﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logger;
using System.Reflection;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Collections.Generic;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActAssignment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                BindCategories(ddlfilterCatagory);
                BindTypes(ddlFilterType);
                BindCustomer();               
                //Bind_CheckList();              
                Tab1_Click(sender, e);
                ViewState["ResearchAssignedActID"] = null;
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:initializeCombobox(); ", true);
               
            }
        }     
        private void BindCustomer()
        {
            try
            {
                var customerlist= Business.ResearchActAssignmentClass.GetCustomer();
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";
                ddlCustomer.DataSource = customerlist;
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("< Select >", "-1"));


                ddlCustomerView.DataTextField = "Name";
                ddlCustomerView.DataValueField = "ID";
                ddlCustomerView.DataSource = customerlist;
                ddlCustomerView.DataBind();
                ddlCustomerView.Items.Insert(0, new ListItem("< Select >", "-1"));

                ddlCustomerModifyAssignment.DataTextField = "Name";
                ddlCustomerModifyAssignment.DataValueField = "ID";
                ddlCustomerModifyAssignment.DataSource = customerlist;
                ddlCustomerModifyAssignment.DataBind();
                ddlCustomerModifyAssignment.Items.Insert(0, new ListItem("< Select >", "-1"));
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTypes(DropDownList ddlList)
        {
            try
            {
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";
                ddlList.DataSource = Business.ResearchActAssignmentClass.GetAllComplianceType();
                ddlList.DataBind();
                ddlList.Items.Insert(0, new ListItem("< Select Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCategories(DropDownList ddlList)
        {
            try
            {
                ddlList.DataTextField = "Name";
                ddlList.DataValueField = "ID";
                ddlList.DataSource = Business.ResearchActAssignmentClass.GetAllComplianceCategory();
                ddlList.DataBind();
                ddlList.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Bind_CheckList() // Method for Binding The Checkbox List  
        {
            int customerID = -1;          
            int categoryid = -1;
            int typeid = -1;
            string filtertext = string.Empty;
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                if (ddlCustomer.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            if (!string.IsNullOrEmpty(ddlfilterCatagory.SelectedValue))
            {
                if (ddlfilterCatagory.SelectedValue !="-1")
                {
                    categoryid = Convert.ToInt32(ddlfilterCatagory.SelectedValue);
                }                
            }
            if (!string.IsNullOrEmpty(ddlFilterType.SelectedValue))
            {
                if (ddlFilterType.SelectedValue != "-1")
                {
                    typeid = Convert.ToInt32(ddlFilterType.SelectedValue);
                }
            }
            ActCheckboxList.Items.Clear();
            ActCheckboxList.DataTextField= "Name";
            ActCheckboxList.DataValueField = "ID";
            ActCheckboxList.DataSource = Business.ResearchActAssignmentClass.GetTotalAct(customerID,categoryid, typeid, tbxFilter.Text);//Set Datasource to CheckBox List  
            ActCheckboxList.DataBind(); // Bind the checkboxList with String List.  
        }
        private void BindGrid()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerView.SelectedValue))
                {
                    if (ddlCustomerView.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerView.SelectedValue);
                    }
                }
                ActCheckboxList.ClearSelection();
                var compliancesData = Business.ResearchActAssignmentClass.GetGridDisplay(customerID, tbxFilterView.Text);
                grdCompliances.DataSource = compliancesData;
                grdCompliances.DataBind();
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                upActListView.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindGridAssignment()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerModifyAssignment.SelectedValue))
                {
                    if (ddlCustomerModifyAssignment.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerModifyAssignment.SelectedValue);
                    }
                }
                int currentuserid = -1;
                if (!string.IsNullOrEmpty(ddlCurrentUser.SelectedValue))
                {
                    if (ddlCurrentUser.SelectedValue != "-1")
                    {
                        currentuserid = Convert.ToInt32(ddlCurrentUser.SelectedValue);
                    }
                }
                grdComplianceInstances.DataSource = null;
                grdComplianceInstances.DataBind();
                if (currentuserid != -1)
                {
                    var compliancesData = Business.ResearchActAssignmentClass.GetGridDisplay(customerID, currentuserid, tbxFilterModifyAssignment.Text);
                    grdComplianceInstances.DataSource = compliancesData;
                    grdComplianceInstances.DataBind();
                    upActListAssignment.Update();
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (ddlCustomer.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                List<object> users = new List<object>();
                users = Business.ResearchActAssignmentClass.GetAllNVP(customerID, null, Flags: true);
                ddlPerformer.DataTextField = "Name";
                ddlPerformer.DataValueField = "ID";
                ddlPerformer.Items.Clear();
                ddlPerformer.DataSource = users;
                ddlPerformer.DataBind();
                ddlPerformer.Items.Insert(0, new ListItem("< Select >", "-1"));               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindNewUsers()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerModifyAssignment.SelectedValue))
                {
                    if (ddlCustomerModifyAssignment.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerModifyAssignment.SelectedValue);
                    }
                }
                List<object> users = new List<object>();
                users = Business.ResearchActAssignmentClass.GetAllNVP(customerID, null, Flags: true);               
                ddlNewUsers.DataTextField = "Name";
                ddlNewUsers.DataValueField = "ID";
                ddlNewUsers.Items.Clear();
                ddlNewUsers.DataSource = users;
                ddlNewUsers.DataBind();
                ddlNewUsers.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCurrentUsers()
        {
            try
            {
                int customerID = -1;
                if (!string.IsNullOrEmpty(ddlCustomerModifyAssignment.SelectedValue))
                {
                    if (ddlCustomerModifyAssignment.SelectedValue != "-1")
                    {
                        customerID = Convert.ToInt32(ddlCustomerModifyAssignment.SelectedValue);
                    }
                }
                List<object> users = new List<object>();
                users = Business.ResearchActAssignmentClass.GetAllAssignedNVP(customerID, null, Flags: true);
                ddlCurrentUser.DataTextField = "Name";
                ddlCurrentUser.DataValueField = "ID";
                ddlCurrentUser.Items.Clear();
                ddlCurrentUser.DataSource = users;
                ddlCurrentUser.DataBind();
                ddlCurrentUser.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlfilterCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Bind_CheckList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void ddlFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Bind_CheckList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Filter
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Bind_CheckList();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilterView_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryView.IsValid = false;
                cvDuplicateEntryView.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilterModifyAssignment_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGridAssignment();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryAssignment.IsValid = false;
                cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Customer 
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryView.IsValid = false;
                cvDuplicateEntryView.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCustomerModifyAssignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindNewUsers();
                BindCurrentUsers();
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:initializeCombobox(); ", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryAssignment.IsValid = false;
                cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {               
                if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    int ActID = Convert.ToInt32(e.CommandArgument);
                    int customerID = -1;
                    if (!string.IsNullOrEmpty(ddlCustomerView.SelectedValue))
                    {
                        if (ddlCustomerView.SelectedValue != "-1")
                        {
                            customerID = Convert.ToInt32(ddlCustomerView.SelectedValue);
                        }
                    }

                    Business.ResearchActAssignmentClass.ActDelete(ActID, customerID);
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }             
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<Research_ActAssignment> Tempassignments = new List<Research_ActAssignment>();
                for (int i = 0; i < ActCheckboxList.Items.Count; i++)
                {
                    if (ActCheckboxList.Items[i].Selected == true)// getting selected value from CheckBox List  
                    {
                        int customerID = -1;
                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                        {
                            if (ddlCustomer.SelectedValue != "-1")
                            {
                                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                            }
                        }                        
                        bool chkexists = Business.ResearchActAssignmentClass.CheckActAssigned(Convert.ToInt32(ActCheckboxList.Items[i].Value), customerID);
                        if (chkexists == false)
                        {                            
                            if (ddlPerformer.SelectedValue != "-1")
                            {
                                if (ddlPerformer.SelectedValue != null)
                                {
                                    Research_ActAssignment TempAssP = new Research_ActAssignment();
                                    TempAssP.ActId = Convert.ToInt32(ActCheckboxList.Items[i].Value);
                                    TempAssP.UserId = Convert.ToInt32(ddlPerformer.SelectedValue);
                                    TempAssP.CustomerId = Convert.ToInt32(ddlCustomer.SelectedValue);
                                    TempAssP.IsActive = false;
                                    TempAssP.CreatedOn = DateTime.UtcNow;
                                    TempAssP.Createdby = AuthenticationHelper.UserID;
                                    Tempassignments.Add(TempAssP);
                                }                                
                            }
                        } //exists end                                              
                    }//checbox selected end
                }//for end

                if (Tempassignments.Count != 0)
                {
                    Business.ResearchActAssignmentClass.AddDetailsTempAssignmentTable(Tempassignments);
                    BindGrid();
                    upActList.Update();
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Act assigned successfully";
                }               
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version.";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }              
        protected void upActList_Load(object sender, EventArgs e)
        {            
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);         
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            ddlCustomer.SelectedValue = "-1";
            ddlPerformer.SelectedValue = "-1";
            ddlfilterCatagory.SelectedValue = "-1";
            ddlFilterType.SelectedValue = "-1";
            tbxFilter.Text = "";
            
            Bind_CheckList();
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:initializeCombobox(); ", true);
            upActList.Update();
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
            ddlCustomerView.SelectedValue = "-1";
            tbxFilterView.Text = "";
            BindGrid();                                                      
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:initializeCombobox(); ", true);
            upActListView.Update();
          
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            MainView.ActiveViewIndex = 2;
            ddlCustomerModifyAssignment.SelectedValue = "-1";
            ddlCurrentUser.SelectedValue = "-1";
            ddlNewUsers.SelectedValue = "-1";            
            tbxFilterModifyAssignment.Text = "";
            BindGridAssignment();
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:initializeCombobox(); ", true);
            upActListAssignment.Update();
           
        }

        private void CheckBoxValueSaved()
        {
            List<Tuple<int, string>> chkList = new List<Tuple<int, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;                                
                if (ViewState["ResearchAssignedActID"] != null)
                    chkList = (List<Tuple<int, string>>)ViewState["ResearchAssignedActID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(3)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, string>(index, "3"));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["ResearchAssignedActID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {

            List<Tuple<int, string>> chkList = (List<Tuple<int, string>>)ViewState["ResearchAssignedActID"];
            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);                    
                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(3)).FirstOrDefault();
                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdComplianceInstances.PageIndex = e.NewPageIndex;
                BindGridAssignment();                
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryAssignment.IsValid = false;
                cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlCurrentUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGridAssignment();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryAssignment.IsValid = false;
                cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {

                CheckBoxValueSaved();
                List<Tuple<int, string>> chkList = (List<Tuple<int, string>>)ViewState["ResearchAssignedActID"];
                if (chkList != null)
                {
                    try
                    {
                        Business.ResearchActAssignmentClass.ReplaceUserForActAssignment(Convert.ToInt32(ddlCurrentUser.SelectedValue), Convert.ToInt32(ddlNewUsers.SelectedValue), chkList);
                        BindNewUsers();
                        BindCurrentUsers();
                        BindGridAssignment();
                        cvDuplicateEntryAssignment.IsValid = false;
                        cvDuplicateEntryAssignment.ErrorMessage = "Re-Assign Sucessfully";
                        upActListAssignment.Update();                       
                        ViewState["ResearchAssignedActID"] = null;
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntryAssignment.IsValid = false;
                        cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                  
                }
                else
                {
                    cvDuplicateEntryAssignment.IsValid = false;
                    cvDuplicateEntryAssignment.ErrorMessage = "Please select at list one compliance for proceed.";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryAssignment.IsValid = false;
                cvDuplicateEntryAssignment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}