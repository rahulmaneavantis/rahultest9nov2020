﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class EventCannedReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "CustomerBranchName";

                //BindFilters();
                BindLocationFilter();
                //dlFilters.SelectedIndex = 0;
                dlFilters_SelectedIndexChanged(null, null);
            }

           
        }

        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            //udcCannedReportPerformer.Visible = udcCannedReportReviewer.Visible = udcCannedReportApprover.Visible = false;

            //switch (rblRole.SelectedIndex)
            //{
            //    case 0:
            //        udcCannedReportPerformer.Visible = true;
            //        break;
            //    case 1:
            //        udcCannedReportReviewer.Visible = true;
            //        break;
            //    case 2:
            //        udcCannedReportApprover.Visible = true;
            //        break;
            //}
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                String FileName = String.Empty;
                FileName = "EventBasedComplianceReport";

                //
                DataTable dataToExport = GetGrid();
                dataToExport.Columns.Remove("Frequency");
                dataToExport.Columns.Remove("BranchID");
                dataToExport.Columns.Remove("Client_Frequency");
                dataToExport.Columns.Remove("Actionable_Informative");
                dataToExport.Columns.Remove("SequenceID");
                dataToExport.Columns.Remove("Department");
                dataToExport.Columns.Remove("DueDate");
                dataToExport.Columns.Remove("IsFyOrCY");
                 
                if (dataToExport == null)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "EmptyDataForExport", " $(function () { alert('No data available for export...'); });", true);
                    return;
                }

                using (ExcelPackage pck = new ExcelPackage())
                {
                    var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("EventBased");
                    ws.Cells["A1"].Style.Font.Bold = true;
                    ws.Cells["A1"].Value = "Customer:";

                    ws.Cells["B1:C1"].Merge = true;
                    ws.Cells["B1"].Value = cname;

                    ws.Cells["A2"].Style.Font.Bold = true;
                    ws.Cells["A2"].Value = "Report Name:";

                    ws.Cells["B2:C2"].Merge = true;
                    ws.Cells["B2"].Value = "Event Based Report";

                    ws.Cells["A3"].Style.Font.Bold = true;
                    ws.Cells["A3"].Value = "Report Generated On:";

                    ws.Cells["B3:C3"].Merge = true;
                    ws.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                    ws.Cells["A5"].Style.Font.Bold = true;
                    ws.Cells["B5"].Style.Font.Bold = true;
                    ws.Cells["C5"].Style.Font.Bold = true;
                    ws.Cells["D5"].Style.Font.Bold = true;
                    ws.Cells["E5"].Style.Font.Bold = true;
                    ws.Cells["F5"].Style.Font.Bold = true;
                    ws.Cells["G5"].Style.Font.Bold = true;
                    ws.Cells["H5"].Style.Font.Bold = true;
                    ws.Cells["I5"].Style.Font.Bold = true;
                    ws.Cells["J5"].Style.Font.Bold = true;
                    ws.Cells["K5"].Style.Font.Bold = true;
                    ws.Cells["L5"].Style.Font.Bold = true;
                    ws.Cells["M5"].Style.Font.Bold = true;
                    ws.Cells["N5"].Style.Font.Bold = true;
                    ws.Cells["O5"].Style.Font.Bold = true;

                    ws.SelectedRange["A5:O5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.SelectedRange["A5:O5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                    using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 15])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.AutoFitColumns();

                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                    using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 1 + dataToExport.Columns.Count])
                    {
                        col.AutoFitColumns(30);
                    }
                    using (ExcelRange col = ws.Cells[5, 15, 5 + dataToExport.Rows.Count, 15])
                    {
                        col[5, 15, 5 + dataToExport.Rows.Count, 15].Style.Numberformat.Format = "dd/MMM/yyyy";
                    }
                    ws.Cells["A5"].LoadFromDataTable(dataToExport, true);
                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedNode.Value != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                var ComplianceList = EventManagement.GetEventComplianceAssignedView(Convert.ToInt32(AuthenticationHelper.CustomerID), branchID);

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        ComplianceList = ComplianceList.OrderBy(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        ComplianceList = ComplianceList.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        ComplianceList = ComplianceList.OrderBy(entry => entry.Branch).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        ComplianceList = ComplianceList.OrderByDescending(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        ComplianceList = ComplianceList.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        ComplianceList = ComplianceList.OrderByDescending(entry => entry.Branch).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdEventCannedReport.DataSource = ComplianceList;
                grdEventCannedReport.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEventCannedReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEventCannedReport.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventCannedReport_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedNode.Value != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                List<Sp_GetStatutoryAssignment_Result> assignmentList = null;

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = EventManagement.GetEventComplianceAssignedView(Convert.ToInt32(AuthenticationHelper.CustomerID), branchID);
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = EventManagement.GetEventComplianceAssignedView(Convert.ToInt32(AuthenticationHelper.CustomerID), branchID);
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdEventCannedReport.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventCannedReport.Columns.IndexOf(field);
                    }
                }
                grdEventCannedReport.DataSource = assignmentList;
                grdEventCannedReport.DataBind();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventCannedReport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation.SelectedNode.Value = "-1";
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            dlFilters_SelectedIndexChanged(null, null);
        }

        public DataTable GetGrid()
        {
            try
            {
                dlFilters_SelectedIndexChanged(null, null);
                DataTable GridData = null;
                    GridData = (grdEventCannedReport.DataSource as List<Sp_GetStatutoryAssignment_Result>).ToDataTable();
                return GridData;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;

        }

    }
}