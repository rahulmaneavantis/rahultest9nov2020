﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class AssignEntitiesToDepartmentcompliance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                btnAddComplianceType.Visible = true;
                BindLocationFilter();         
                BindUsers(ddlUsers, delddluser, ddlFilterUsers);
                BindComplianceEntityInstances();                                                              
                ForceCloseFilterBranchesTreeView();
                tbxBranch.Attributes.Add("readonly", "readonly");
                tbxDelBranch.Attributes.Add("readonly", "readonly");              
            }
        }    
        public static List<object> GetAllDepartmentHeadUser(int customerID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users

                             where row.IsDeleted == false && row.IsActive == true
                             && row.IsHead == true
                             select new
                             {
                                 row.ID,
                                 row.FirstName,
                                 row.LastName,
                                 row.Email,
                                 row.CustomerID
                             });

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }


                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).ToList<object>();

                return users;
            }
        }        
        private void BindUsers(DropDownList ddlUserList, DropDownList delddlUserList,DropDownList ddlFilterUsers, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var users = GetAllDepartmentHeadUser(customerID);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();
                ddlUserList.DataSource = users;
                ddlUserList.DataBind();

                delddlUserList.DataTextField = "Name";
                delddlUserList.DataValueField = "ID";            
                delddlUserList.Items.Clear();
                delddlUserList.DataSource = users;
                delddlUserList.DataBind();

                ddlFilterUsers.DataTextField = "Name";
                ddlFilterUsers.DataValueField = "ID";
                ddlFilterUsers.Items.Clear();
                ddlFilterUsers.DataSource = users;
                ddlFilterUsers.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static void Create(EntitiesAssignment_IsDept objEntitiesAssignment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EntitiesAssignment_IsDept.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }      
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {                
                int userID = -1;
                string issatint = string.Empty;
                if (!string.IsNullOrEmpty(ddlUsers.SelectedValue))
                {
                    userID = Convert.ToInt32(ddlUsers.SelectedValue);
                }            
                if (!string.IsNullOrEmpty(ddlComplianceType.SelectedValue))
                {
                    issatint = ddlComplianceType.SelectedValue;
                }

                if (userID != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (tvBranches.CheckedNodes.Count > 0)
                        {                            
                            foreach (TreeNode node in tvBranches.CheckedNodes)
                            {
                                var data = SelectEntity(Convert.ToInt32(node.Value), userID, issatint);
                                if (data != null)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Entity already assigned to location.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                                }
                                else
                                {
                                    EntitiesAssignment_IsDept objEntitiesAssignment = new EntitiesAssignment_IsDept();
                                    objEntitiesAssignment.UserID = userID;
                                    objEntitiesAssignment.BranchID = Convert.ToInt32(node.Value);
                                    objEntitiesAssignment.IsStatutoryInternal = issatint;
                                    objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                    AssignEntitiesToDepartmentcompliance.Create(objEntitiesAssignment);
                                }
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select location.";
                        }
                    }
                }               
                BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        public static EntitiesAssignment_IsDept SelectEntity(int branchId, int userID, string isSatInt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntitiesAssignmentData = (from row in entities.EntitiesAssignment_IsDept
                                              where row.BranchID == branchId
                                              && row.UserID == userID
                                              && row.IsStatutoryInternal == isSatInt
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
        public static List<ComplianceAssignmentDepartmentEntitiesView> SelectAllEntities(int branchId, int userID, int customerID,string isStatInt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.ComplianceAssignmentDepartmentEntitiesViews
                                                   select row).ToList();

                var branchIds = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false && row.CustomerID == customerID
                                 select row.ID).ToList();


                if (!string.IsNullOrEmpty(isStatInt))
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.IsStatutoryInternal  == isStatInt).ToList();
                    
                }  
                if (branchIds != null)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.BranchID)).ToList();
                }
                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.BranchID == branchId).ToList();
                }
                if (userID != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.userID == userID).ToList();
                }

                return ComplianceTransactionEntity;
            }
        }      
        private void BindComplianceEntityInstances()
        {
            try
            {
                string issatint = string.Empty;
                int branchid = -1;
                int userid = -1;
                int custid = -1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    branchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }               
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userid = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }
                custid = (int)AuthenticationHelper.CustomerID;
                if (!string.IsNullOrEmpty(ddlFilterComplianceType.SelectedValue))
                {
                    issatint = ddlFilterComplianceType.SelectedValue;
                }
                grdAssignEntities.DataSource = SelectAllEntities(branchid, userid, custid, issatint);
                grdAssignEntities.DataBind();
                upComplianceTypeList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "server error occured.please try again.";
            }
        }    
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches'); ", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideAddTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideDeleteTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceDelete_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesDelete", string.Format("initializeJQueryUI('{0}', 'divBranches1');", tbxDelBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }
        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }
        
        protected void btnDeleteComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                BindLocation();
                Label3.Text = "";
                delddluser.SelectedValue = "-1";
                delddlcomtype.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('open');", true);
                tbxDelBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideDeleteTreeView", "$(\"#divBranches1\").hide(\"blind\", null, 5, function () { });", true);
                upComplianceDelete.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                BindLocation();
                lblErrorMassage.Text = "";
                ddlUsers.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideAddTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);                
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string issatint = string.Empty;
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                if (!string.IsNullOrEmpty(ddlComplianceType.SelectedValue))
                {
                    issatint = ddlComplianceType.SelectedValue;
                }

                var assignmentList = SelectAllEntities(branchID, userID, customerID, issatint);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAssignEntities.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                    }
                }
                grdAssignEntities.DataSource = assignmentList;
                grdAssignEntities.DataBind();
                tbxFilterLocation.Text = "< Select Location >";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }


        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

    

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {               
                int userID = -1;
                string issatint = string.Empty;
                if (!string.IsNullOrEmpty(delddluser.SelectedValue))
                {
                    userID = Convert.ToInt32(delddluser.SelectedValue);
                }
                if (!string.IsNullOrEmpty(delddlcomtype.SelectedValue))
                {
                    issatint = delddlcomtype.SelectedValue;
                }                              
                if (userID != -1)
                {
                    //Branch all and Category selected
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (tvdelBranches.CheckedNodes.Count > 0)
                        {
                            foreach (TreeNode node in tvdelBranches.CheckedNodes)
                            {
                                var CID = Convert.ToInt32(node.Value);
                                entities.EntitiesAssignment_IsDept.RemoveRange(entities.EntitiesAssignment_IsDept.Where(x => x.BranchID == CID
                                && x.IsStatutoryInternal == issatint && x.UserID == userID).ToList());
                                entities.SaveChanges();
                            }
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divDeleteEntitiesDialog\").dialog('close');", true);
                        }
                        else
                        {
                            cvDuplicateEntryDelete.IsValid = false;
                            cvDuplicateEntryDelete.ErrorMessage = "Please select location.";
                        }
                    }
                    BindComplianceEntityInstances();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntryDelete.IsValid = false;
                cvDuplicateEntryDelete.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }     
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }
       
        protected void delddluser_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        #region Location
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();               
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                tvBranches.Nodes.Clear();
                TreeNode AddNode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    AddNode = new TreeNode(item.Name, item.ID.ToString());
                    AddNode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(AddNode, item);
                    tvBranches.Nodes.Add(AddNode);
                }
                tvBranches.CollapseAll();

                tvdelBranches.Nodes.Clear();
                TreeNode DelNode = new TreeNode();
                foreach (var item in bracnhes)
                {
                    DelNode = new TreeNode(item.Name, item.ID.ToString());
                    DelNode.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(DelNode, item);
                    tvdelBranches.Nodes.Add(DelNode);
                }
                tvdelBranches.CollapseAll();               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion
    }
}








   









































        
    
